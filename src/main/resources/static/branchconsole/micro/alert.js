/**
 * POST  AlertController addnew
 * This creates a new alert on account
 */
function addnewalert(form) {
    var url = '/alert/addnew';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultalert);
}

/**
 * POST  AlertController remove
 * This removes a alert on account
 */
function notification(id) {
    var url = '/alert/notification';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultalert);
}


/**
 * POST  AlertController remove
 * This removes a alert on account
 */
function notifications() {
    var url = '/alert/notifications';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    alertpost(url);
}


/**
 * POST  AlertController modify
 * This removes a alert on account
 */
function customeralerts(id) {
    var url = '/alert/modify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultalert);
}


function alertpost(url, data, callback){

        $.ajax({
            type:  'POST',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data),
            url: url,
            success: callback,
            error : callback
        });
}
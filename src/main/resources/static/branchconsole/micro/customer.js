/**
 * POST  CustomerController addnew
 * This creates a new customer on account
 */
function addnewcustomer(form) {
    showSpinner("customer");
    var url = '/customer/addnew';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultcustomer);
}

/**
 * POST  CustomerController remove
 * This removes a customer on account
 */
function removecustomer(form) {
    showSpinner("customer");
    var url = '/customer/remove';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultcustomer);
}


/**
 * POST  CustomerController modify
 * This removes a customer on account
 */
function modifycustomer(data) {
    showSpinner("customer");
    var url = '/customer/modify';
    var postCustomerUpdate = new PostCustomerUpdate(data);
    postCustomerUpdate.postdata(url, resultcustomer);
}


/**
 * POST  UpdateFormController addnew
 * This creates a new updateform on account
 */
function addnewcustupdateform(form, data, olddata){
    console.log('data ' + data);
    if(olddata.address.value !== data.address){
        if(form.file_upload.value ==='' || form.file_upload.value === undefined ){
            bootbox.alert("Upload is mandatory for change of address");
            return false;
        }
    }

    //if any of the source doc is in place file should not be empty
    if(data.middlenamesourcedoc === 'on' || data.dobsourcedoc === 'on' || data.addresssourcedoc === 'on'){
        if(form.file_upload.value ==='' || form.file_upload.value === undefined ){
            bootbox.alert("Upload is mandatory; source document is in place");
            return false;
        }

    }



    // alert("source doc ==> " + data.middlenamesourcedoc + ", " + data.dobsourcedoc);
    // return false;
    showSpinner("customer");
    console.log(form.entitycategory.value);
    console.log(data);
    $("#uploadcustomerupdateform").submit(function(){
        var formData = new FormData($(form)[0]);
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.file_upload.value);
        postRequest.appendinputsformdata(formData);

        $.ajax({
            url: '/updateform/addforcust',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });
    $("#uploadcustomerupdateform").submit();
    modifycustomer(data);


}

/**
 * POST  CustomerController modify
 * This removes a customer on account
 */
function modifycustomertwo(form) {
    showSpinner("customer");
    var url = '/customer/modify';
    var postRequest = new PostRequest(form.entityid.value, '', '', '');
    var postBioRequest = new PostBioRequest(form);
    postBioRequest.postdata(url, resultcustomer);
}


/**
 * POST  CustomerController verify
 * This verifies a customer process
 */
function verifycustomer(form) {
    showSpinner("customer");
    var url = '/customer/verify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultcustomer);
}

/**
 * POST  CustomerController cancel
 * This cancels a customer action
 */
function cancelcustomer(form) {
    showSpinner("customer");
    var url = '/customer/cancel';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultcustomer);
}
/**
 * POST  CustomerController decline
 * This declines a customer process
 */
//This method does the declination from the controller with ajax call
function declinecustomer(form) {
    showSpinner("customer");
    if(validatecustomerdecline(form)==true) {
        var url = '/customer/decline';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
        postRequest.postdata(url, resultcustomer);
    }
}



function customeritem(id){
    showSpinner("customer");
    getCustomerRole();
    url = '/customer/getdata';
    var postdata = {
        'id': id
    };
    $( "#customertop" ).load( "/customer/get/"+id );
    // process(url, postdata, itemcustomer);
     hideSpinner("customer");
}


function getCustomerRole() {
    var role;

    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        dataType: 'json',
        url: '/user/role/customer',
        success: function (data) {
            role = data;

            effectCustomerRoles(role)
        },
        error : function (data) {


        }
    });

}

function effectCustomerRoles(role){

    if(!role.modifyCustomer){

        $("#modifyCustomer").prop("disabled", true);

    }if(!role.verifyCustomer){

        $("#verifyCustomer").prop("disabled", true);

    }if(!role.declineCustomer){

        $("#declineCustomer").prop("disabled", true);

    }if(!role.cancelCustomer){

        $("#cancelCustomer").prop("disabled", true);

    }


}


function itemcustomer(data){

    hideSpinner("customer");
    document.getElementById("customertop").innerHTML = customeritempage(data);
}


function showdetailsbeforeupdate(data,olddata){

    html = "<form action='#' class='small-scrollable' method='POST'>" +

        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Customer Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +
        "<div style='' class='col-md-6'>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitycategory' name='entitycategory'/>" +
        "<h5 class='col-md-5'>Title </h5><h5 class='col-md-6'  >"+ data.title.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.id+"'  id='entityid' name='entityid'/>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value=''  id='entitydata' name='entitydata'/>" +
        "<h5 class='col-md-5'>First name: </h5><h5 class='col-md-6'  >"+ data.firstname.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Last name: </h5><h5 class='col-md-6' >"+ data.lastname.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5' >Middle name: </h5><h5 class='col-md-6' >"+ data.middlename.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5' >Date of birth: </h5><h5 class='col-md-6' >"+ data.dateOfBirth.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5' >Mother Maiden name: </h5><h5 class='col-md-6' >"+ data.motherMaidenName.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5' >Phone: </h5><h5 class='col-md-6' >"+ data.phone.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5' >Address: </h5><h5 class='col-md-6' >"+ data.address.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5' >Email: </h5><h5 class='col-md-6' >"+ data.email.value +"</h5>" +
        "</div>" +
        "</div>" +
        "<div style='' class='col-md-6 '>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Old title:</h5><h5 class='col-md-6'  >"+ olddata.oldtitle +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Old firstname</h5><h5 class='col-md-6' >"+ olddata.oldfirstname +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Old lastname:</h5><h5 class='col-md-6' >"+ olddata.oldlastname +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Old middlename:</h5><h5 class='col-md-6' >"+ olddata.oldmiddlename +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Old date of birth:</h5><h5 class='col-md-6' >"+ olddata.olddob +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Old mother\'s maiden name:</h5><h5 class='col-md-6' >"+ olddata.oldmothermaiden +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Old phone number:</h5><h5 class='col-md-6' >"+ olddata.oldphone +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Old address:</h5><h5 class='col-md-6' >"+ olddata.oldaddress +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Old email:</h5><h5 class='col-md-6' >"+ olddata.oldemail +"</h5>" +
        "</div>" +
        "</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>";

    html = html + "<input type='button' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifycustomer(this.form)'/>" +
        "<input type='button' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='declinecustomerpage("+ data.id +")'/>";
    html = html + "</div>" + "</form>";
    hideSpinner("customer");


    document.getElementById("customertop").innerHTML = html;
}

function alertdetails(data){

}
function errorpp(){
}
function loaddetailspage(data,form){
    showSpinner("customer");
    //Need to fetch the old record via ajax now
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({entityid:form.entityid.value}),
        url: '/customer/fetcholdrecord',
        success: getoldrecord,
        error : errorpp
    });
    function getoldrecord(oldrecord){

        showdetailsbeforeupdate(data,oldrecord);
    }


}

function updatecustomer(data){
    hideSpinner("customer");
    document.getElementById("customertop").innerHTML = customerupdatepage(data);
}


function uploadcustomerupdateform(data){

    var form = document.getElementById("updatecustomerform");

    if(form.email.value.trim() === '' || form.email === undefined || form.email.value.trim() === null ){
        document.getElementById('email_label').innerHTML = "<font color=red>Email is required</font>";
        return false;
    }else{document.getElementById('email_label').innerHTML = "<font color=green>Valid</font>";}


    if(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(form.email.value.trim())){
        document.getElementById('email_label').innerHTML = "<font color=red>Email is invalid</font>";
        return false;
    }else{document.getElementById('email_label').innerHTML = "<font color=green>Valid</font>";}

    if(form.firstname.value.trim() === '' || form.firstname === undefined || form.firstname.value.trim() === null){
        document.getElementById('firstname_label').innerHTML = "<font color=red>First name is required</font>";
        return false;
    }else{document.getElementById('firstname_label').innerHTML = "<font color=green>Valid</font>";}

    if(form.lastname.value.trim() === '' || form.lastname === undefined || form.lastname.value.trim() === null){
        document.getElementById('lastname_label').innerHTML = "<font color=red>Last name is required</font>";
        return false;
    }else{document.getElementById('lastname_label').innerHTML = "<font color=green>Valid</font>";}

    if(form.motherMaidenName.value.trim() === '' || form.motherMaidenName === undefined || form.motherMaidenName.value.trim() === null){
        document.getElementById('maidenname_label').innerHTML = "<font color=red>Mother's maiden is required</font>";
        return false;
    }else{document.getElementById('maidenname_label').innerHTML = "<font color=green>Valid</font>";}

    if(form.dob.value.trim() === '' || form.dob === undefined || form.dob.value.trim() === null){
        document.getElementById('dob_label').innerHTML = "<font color=red>Date of birth is required</font>";
        return false;
    }else{document.getElementById('dob_label').innerHTML = "<font color=green>Valid</font>";}
    // else if(form.motherMaidenName.value === '' || form.motherMaidenName === undefined || form.motherMaidenName.value === null){
    //     document.getElementById('maidenname_label').innerHTML = "<font color=red>Mother's maiden name is required</font>";
    //     return false;
    // }
    if(form.phone.value.trim() === '' || form.phone === undefined || form.phone.value.trim() === null){
        document.getElementById('phone_label').innerHTML = "<font color=red>Phone number is required</font>";
        return false;
    }else{
        document.getElementById('phone_label').innerHTML = "<font color=green>Valid</font>";
    }

    if(!/^[0-9]+$/.test(form.phone.value.trim())){
        document.getElementById('phone_label').innerHTML = "<font color=red>Invalid phone number</font>";
        return false;
    }else if(form.phone.value.trim().length > 11){
        document.getElementById('phone_label').innerHTML = "<font color=red>Phone number can't be greater than 11 digits</font>";
        return false;
    }else{
        document.getElementById('phone_label').innerHTML = "<font color=green>Valid</font>";
    }

    if(form.address.value.trim() === '' || form.address === undefined || form.address.value.trim() === null){
        document.getElementById('address_label').innerHTML = "<font color=red>Address is required</font>";
        return false;
    }else{document.getElementById('address_label').innerHTML = "<font color=green>Valid</font>";}

    hideSpinner("customer");
    document.getElementById("customertop").innerHTML = customerupdateformuploadpage(data);
}


function addcustomer(){
    hideSpinner("customer");
    document.getElementById("customertop").innerHTML = customeraddpage(data);
}

//This displays the view for the decline form
function declinecustomerpage(id){
    hideSpinner("customer");
    document.getElementById("customertop").innerHTML = customerdeclinepage(id);
}


function resultcustomer(data){

    hideSpinner("customer");
    document.getElementById("customertop").innerHTML = customerresult(data);
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        url: '/customer/getcustomerbottom',
        success: listcustomers,
        error : listcustomers
    });
}


function showcustomersuploadedform(data){
    showSpinner("customer");
    var postdata = {
            'id': data.updateforms[0].id
    }

    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(postdata),
        url: '/updateform/getdata',
        success: function(result){

            showDocumentPDF(result.accessURL);
            // document.getElementById("customerbottom").innerHTML = customeruploadedformpage(result);
            hideSpinner("customer");
        },
        error : listcustomers
    });




}

function listcustomers(data){
    hideSpinner("customer");
    document.getElementById("customerbottom").innerHTML = customerslistpage(data);

}

function PostCustomerUpdate(data){
    this.searchmodule = $('#module').val();
    this.searchusing = $('#using').val();
    this.searchvalue = $('#value').val();
    this.searchfrom = $('#from').val();
    this.searchto = $('#to').val();
    this.searchfilter = $('#filter').val();
    this.searchfiltervalue = $('#filtervalue').val();
    this.searchsortby = $('#sortby').val();
    this.searchmessage = $('#message').val();
    this.searchpage = ($("#page").val());
    this.searchacctid = $('#acctid').val();
    this.entityid = data.entityid;
    this.entitykey = data.entitykey;
    this.entitycategory = data.entitycategory;
    this.entitydata = data.entitydata;
    this.firstname = data.firstname;
    this.firstnameid = data.firstnameid;
    this.lastnameid = data.lastnameid;
    this.lastname = data.lastname;
    this.middlenameid = data.middlenameid;
    this.middlename = data.middlename;
    this.firstnameid = data.firstnameid;
    this.firstname = data.firstname;
    this.emailid = data.emailid;
    this.email = data.email;
    this.addressid = data.addressid;
    this.address = data.address;
    this.phoneid = data.phoneid;
    this.phone = data.phone;
    this.title = data.title;
    this.titleid = data.titleid;
    this.motherMaidenNameid = data.motherMaidenNameid;
    this.motherMaidenName = data.motherMaidenName;
    this.dobid = data.dobid;
    this.dob = data.dob;
    this.middlenamesourcedoc =  data.middlenamesourcedoc; 
    this.dobsourcedoc = data.dobsourcedoc;
    this.addresssourcedoc = data.addresssourcedoc
}

PostCustomerUpdate.prototype.postdata = function(url, callback){

    var postInput = {
        searchmodule: this.searchmodule,
        searchusing: this.searchusing,
        searchvalue: this.searchvalue,
        searchfrom: this.searchfrom,
        searchto: this.searchto,
        searchfilter: this.searchfilter,
        searchfiltervalue: this.searchfiltervalue,
        searchsortby: this.searchsortby,
        searchmessage: this.searchmessage,
        searchpage: this.searchpage,
        searchacctid: this.searchacctid,
        entityid:this.entityid,
        entitykey:this.entitykey,
        entitycategory:this.entitycategory,
        entitydata:this.entitydata,
        firstname: this.firstname,
        firstnameid: this.firstnameid,
        lastname: this.lastname,
        lastnameid: this.lastnameid,
        middlenameid: this.middlenameid,
        middlename: this.middlename,
        address: this.address,
        addressid: this.addressid,
        emailid: this.emailid,
        email: this.email,
        phoneid: this.phoneid,
        phone: this.phone,
        title: this.title,
        titleid: this.titleid,
        motherMaidenNameid:this.motherMaidenNameid,
        motherMaidenName :this.motherMaidenName,
        dobid:this.dobid,
        dob :this.dob,
        middlenamesourcedoc: this.middlenamesourcedoc, 
        dobsourcedoc : this.dobsourcedoc,
        addresssourcedoc : this.addresssourcedoc
    };

    
    process(url, postInput, callback);
//    post(url, postInput);
};







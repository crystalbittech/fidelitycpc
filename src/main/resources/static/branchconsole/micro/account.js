
/*
 * This does the frontend validation
 */
function validateupdate(form){
    if (form.file_upload.value==null || form.file_upload.value==''){
        document.getElementById("bfile").innerHTML = "<font color=blue>Please pick a document</font>";
        return false;
    }else{document.getElementById("bfile").innerHTML = "<font color=green>Valid</font>";}
    return true;
}
/*
 * This does the frontend validation for declination
 */
function validateaccountdecline(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("adecreason").innerHTML = "<font color=blue>You must provide a reason</font>";
        return false;
    }else{document.getElementById("adecreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("adecremark").innerHTML = "<font color=blue>You must provide a comment</font>";
        return false;
    }else{document.getElementById("adecremark").innerHTML = "<font color=green>Valid</font>";}
    return true;
}
/**
 * POST  AccountController modify
 * This removes a account on account
 */
function modifyaccount(data) {
    showSpinner("account");
    var url = '/account/modify';
    var postAccountUpdate = new PostAccountUpdate(data);
    postAccountUpdate.postdata(url, resultaccount);
}

/**
 * Get account top page
 */
function getaccounttoppage(accountid){

     
    $.ajax({
       type:'GET',
        url:'/account/explodepage/'+accountid+'',
        async:false,
        success:function (data) {
            // alert("account "+accountid);
            hideSpinner("account");
            $("#accounttop").html(data);
        },
    });

}
function ReactivateAccount(id){
    var accountid = $('#acctid').val();
    $.ajax({
        type:'GET',
        url:'/account/updatepage/'+accountid+'',
        async:false,
        success:function (data) {
            $("#accounttop").html(data);
        },
    });
}
function DeactivateAccount(id){
    var accountid = $('#acctid').val();
    $.ajax({
        type:'GET',
        url:'/account/updatepage/'+accountid+'',
        async:false,
        success:function (data) {
            $("#accounttop").html(data);
        },
    });
}

function updateaccountdetails(form) {
    if (validateupdate(form)==true) {
        $("form#upload_form").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, this.file_upload.value, form.entitydata.value);
            postRequest.appendinputsformdata(formData);
            // alert("abajax "+form.entitydata.value);
            $.ajax({
                url: '/account/modify',
                type: 'POST',
                data: formData,
                async: false,
                success: function () {
                    getaccounttoppage(form.entityid.value);
                    return false;
                },
                error: function () {

                },
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });
        $("form#upload_form").submit(e);
        getaccounttoppage(form.entityid.value);
    }else{return false;}
}
/**
 * POST  AccountController modify
 * This removes a account on account
 */
function uploadaccountform(form, data){
    $("#accountupdateformpage").submit(function(){
        var formData = new FormData($(form)[0]);
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.file_upload.value);
        postRequest.appendinputsformdata(formData);
        showSpinner("account");
        $.ajax({
            url: '/updateform/addforacct',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });
    $("#accountupdateformpage").submit();
    modifyaccount(data);


}


/**
 * POST  AccountController verify
 * This verifies a account process
 */
function verifyaccount(id) {
    showSpinner("account");
    var url = '/account/verify';
    var postRequest = new PostRequest(id, '', '', '');
    postRequest.postdata(url, getaccounttoppage(id));
}

/**
 * POST  AccountController verify
 * This verifies a account process
 */
function verifyaccountupdate(id) {
    showSpinner("account");
    var accountid = $('#acctid').val();
    var postdata = {'acctid':accountid};
    $.ajax({
        type:'POST',
        url:'/account/verify',
        data:postdata,
        async:false,
        success:function (data) {
            $("#accounttop").html(data);
        },
    });
    getaccounttoppage(accountid);
}

/**
 * POST  AccountController cancel
 * This cancels a account action
 */
function cancelaccount(data) {
    // showSpinner("account");
    // var url = '/account/cancel';
    // var postRequest = new PostRequest(data.id, '', '', '');
    // postRequest.postdata(url, getaccounttoppage());
    // getaccounttoppage();
    showSpinner("account");
    var accountid = $('#acctid').val();
    var postdata = {acctid:accountid};
    $.ajax({
        type:'POST',
        url:'/account/cancelaccount',
        data:postdata,
        async:false,
        success:function (data) {
            $("#accounttop").html(data);
        },
        error:function (data) {
            console.log(data)
        }
    });
    getaccounttoppage(accountid);
    hideSpinner("account");
}
/**
 * POST  AccountController decline
 * This declines a account process
 */
function declineaccountprocess(form){
           console.log("In!!");
    if(true) {
        showSpinner("account");
        var accountid = $('#acctid').val();
        var postdata = {reason: form.entitykey.value, comment: form.entitydata.value, acctid: accountid};
        $.ajax({
            type: 'POST',
            url: '/account/declineaccount',
            data: postdata,
            async: false,
            success: function (data) {
                $("#accounttop").html(data);
            },
            error:function (data) {
                console.log(data)
            }
        });
        getaccounttoppage(accountid);
        hideSpinner("account");
    }else{return false}
    // showSpinner("account");
    // var url = '/account/decline';
    // var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    // postRequest.postdata(url, getaccounttoppage());
}

function declineaccount(id){
    document.getElementById("accounttop").innerHTML = accountdeclinepage(id);
}

function resultaccount(data){
    // $.ajax({
    //     type:  'POST',
    //     contentType: 'application/json',
    //     dataType: 'json',
    //     data: JSON.stringify(data),
    //
    //     url: '/account/getaccountbottom',
    //     success: reloadaccountbottom,
    //     error : reloadaccountbottom
    // });
    
    document.getElementById("accounttop").innerHTML = accountresultpage(data);
}

function updateAccountSecondary(id,secCategory){
    var value = document.getElementById("secCategory").value;
    $('#secondaryModal').modal('toggle');
    showSpinner("account");
    var data = {id:id,category:value};
    $.ajax({
        type:  'POST',
        data: data,
        async:false,
        url: '/account/update/category',
        success:function (result) {
            // alert("success");
            console.log(result)
            $("#feedback").text("UPDATED SUCCESSFULLY");
            // getaccounttoppage();
            accountResultPage(result);
            // $("#accounttop").html(data);
        },
        error : $("#feedback").text("NOTIFICATION SUCCESSFUL")
    });
}

function getaccount(id){
    showSpinner("account");
  url = '/account/getdata'
  var postdata = {'id': id}
  process(url, postdata, displayaccount);
}


function displayaccount(responsedata){


  var data = {
          id: responsedata.id,
          accountnumber: responsedata.accountNumber,
          accountnameid: responsedata.accountName.id,
          accountnamevalue: responsedata.accountName.value,
          accountnamestatus: responsedata.accountName.status,
          accountscheme: responsedata.accountScheme,
          branchname: responsedata.branch.solDescription,
          branchsol: responsedata.branch.sol,
          accountstatusid: responsedata.accountStatus.id,
          accountstatusvalue: responsedata.accountStatus.value,
          accountstatusstatus: responsedata.accountStatus.status,
          accountbalance:responsedata.accountLastBal,
          channels: {
            verve: false,
            fcmbonline: false,
            fcmbmobile: false
          },
          more:'',
          updateform: responsedata.updateFile,
          updateformUrl:responsedata.updateDocUrl
  }
  document.getElementById("accounttop").innerHTML = accountdisplaypage(data)
}


function reactivateaccount(data){

  data.accountstatusvalue = 'A';
  document.getElementById("accounttop").innerHTML = accountreactivatepage(data)

}
    
function updateaccount(data){

  document.getElementById("accounttop").innerHTML = accountupdatepage(data)
}

function channelsaccount(data){

    var updateaccountformdata = getFormData($("#accountupdatepage"));
    if (!isEmpty(updateaccountformdata)){
      data.accountnamevalue = updateaccountformdata.accountname
    }
    document.getElementById("accounttop").innerHTML = accountchannelspage(data);
    $('#verve').prop('checked', data.channels.verve);
    $('#fcmbonline').prop('checked', data.channels.fcmbonline);
    $('#fcmbmobile').prop('checked', data.channels.fcmbmobile);

}

function updateformaccount(data){

    data.channels.verve = document.getElementById("verve").checked
    data.channels.fcmbonline = document.getElementById("fcmbonline").checked
    data.channels.fcmbmobile = document.getElementById("fcmbmobile").checked

    document.getElementById("accounttop").innerHTML = accountupdateformpage(data);
}


function resultaccount(data){

  document.getElementById("accounttop").innerHTML = accountresultpage(data)
}


/**
 * POST  AccountController get freezes
 * This gets the freezes with the account id
 */
function getfreezes(id){

    url = '/account/freezes'
    var postdata = {'id': id}
    process(url, postdata, explodefreeze);
}

function PostAccountUpdate(data){
    this.searchmodule = $('#module').val();
    this.searchusing = $('#using').val();
    this.searchvalue = $('#value').val();
    this.searchfrom = $('#from').val();
    this.searchto = $('#to').val();
    this.searchfilter = $('#filter').val();
    this.searchfiltervalue = $('#filtervalue').val();
    this.searchsortby = $('#sortby').val();
    this.searchmessage = $('#message').val();
    this.searchpage = ($("#page").val());
    this.searchacctid = $('#acctid').val();
    this.entityid = data.id;
    this.accountnameid = data.accountnameid;
    this.accountname = data.accountnamevalue;
    this.accountstatusid = data.accountstatusid;
    this.accountstatus = data.accountstatusvalue;
}

PostAccountUpdate.prototype.postdata = function(url, callback){

    var postInput = {
        searchmodule: this.searchmodule,
        searchusing: this.searchusing,
        searchvalue: this.searchvalue,
        searchfrom: this.searchfrom,
        searchto: this.searchto,
        searchfilter: this.searchfilter,
        searchfiltervalue: this.searchfiltervalue,
        searchsortby: this.searchsortby,
        searchmessage: this.searchmessage,
        searchpage: this.searchpage,
        searchacctid: this.searchacctid,
        entityid:this.entityid,
        accountnameid: this.accountnameid,
        accountname: this.accountname,
        accountstatusid: this.accountstatusid,
        accountstatus: this.accountstatus
    };

    process(url, postInput, callback);
//    post(url, postInput);
};

function updateAcctNStatus(acctId) {
    $("#mn-spinner").show();
    $("#mn-notify_feedback").text("");
    var mn_status = $('#mn-notifyMsg').find(":selected").val();
    try {
        $.post("/update_acct_notify_status",
            {
                acct_id: acctId,
                mn_status: mn_status
            },
            function (data, status) {
                $("#mn-spinner").hide();
                if (status == "success") {
                    $("#mn-notify_feedback").text("SUCCESSFULLY UPDATED");
                    updated = true;
                }
                else {
                    $("#mn-notify_feedback").text("FAILED; ERROR OCCURED");
                }
            }
        );
    }
    catch(err){
        $("#mn-spinner").hide();
        $("#mn-notify_feedback").text("FAILED; ERROR OCCURED");
    }
}
function accountResultPage(acctId) {
    showSpinner("account");
    html = "<div class='col-md-12'>" +
        "<div class='col-md-12 response-box'><div class='inner-response'>" +
        "<label><h3> Documents update successful</h3></label><p></p>" +
        "</div><div class='col-md-12'>" +
        "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='getaccounttoppage("+ acctId +")'/>" +
        "</div></div></div>";
    document.getElementById("accounttop").innerHTML = html;
    hideSpinner("account");
    return html;
}

function hideMNDialog()
{        $('#modAcctNotifyModal').modal('hide'); }

function openNAcctModifyDialog() {
  $("#mn-spinner").hide();
  $('#modAcctNotifyModal').modal('show');
}

function resizePage(){
//            alert("i am here");
        if  (($('#modulepage').hasClass('col-lg-12'))){
        $("#div1").show();
        $("#modulepage").removeClass('col-lg-12');
        $("#modulepage").addClass('col-lg-8' );
       }
}

function loadFulldet(id){
    $.ajax({
        type:'POST',
        url:'/account/viewFullPage',
        data:{"acctid":id},
        async:false,
        success:function(data){document.getElementById("accounttop").innerHTML = data;console.log("It works");},
        error:function(){console.log("Error occured");}
    })
}
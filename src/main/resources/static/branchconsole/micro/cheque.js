function validatechequemodify(form){
    if(document.getElementById("entitydata").value=="acknowledge" || document.getElementById("entitydata").value=="delay") {
        $("#chequeinfo").css("visibility","hidden");
    }else{
        $("#chequeinfo").css("visibility","visible");
    };

    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("moption").innerHTML = "<font color=blue>You must select an option</font>";
        return false;
    }else{document.getElementById("moption").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitykey.value==null || form.entitykey.value=='' && (document.getElementById("entitydata").value!="acknowledge" && document.getElementById("entitydata").value!="delay")){
        document.getElementById("cbgno").innerHTML = "<font color=blue>You must enter begin number</font>";
        return false;
    }else{document.getElementById("cbgno").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitycategory.value==null || form.entitycategory.value=='' && (document.getElementById("entitydata").value!="acknowledge" && document.getElementById("entitydata").value!="delay")){
        document.getElementById("clvno").innerHTML = "<font color=blue>You must enter number of leaves</font>";
        return false;
    }else{document.getElementById("clvno").innerHTML = "<font color=green>Valid</font>";}
    return true;
}
/*
 * This does the frontend validation
 */
function validatechequebook(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("wbgnno").innerHTML = "<font color=blue>You must enter a begin number</font>";
        return false;
    }else{document.getElementById("wbgnno").innerHTML = "<font color=green>Valid</font>";}
    return true;
}/*
 * This does the frontend validation for declination
 */
function validatechequedecline(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("decreason").innerHTML = "<font color=blue>You must provide a reason</font>";
        return false;
    }else{document.getElementById("decreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("decremark").innerHTML = "<font color=blue>You must provide a comment</font>";
        return false;
    }else{document.getElementById("decremark").innerHTML = "<font color=green>Valid</font>";}
    return true;
}
/**
 * POST  ChequeController addnew
 * This creates a new cheque on account
 */
function addnewcheque(form) {
    if (validatechequebook(form) == true) {
        showSpinner("cheque");
        var url = '/cheque/addnew';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value, form.lienremark.value);
        postRequest.postdata(url, resultcheque);
    }
}
function modifynewcheque(form) {
    if (true) {
        showSpinner("cheque");
        var url = '/cheque/modifynew';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value);
        postRequest.postdata(url, resultcheque);
    }
}

/**
 * POST  ChequeController remove
 * This removes a cheque on account
 */
function removecheque(form) {
    showSpinner("cheque");
    var url = '/cheque/remove';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url,resultcheque);
}


/**
 * POST  ChequeController remove
 * This modifies a cheque on account
 */
function modifycheque(form) {
    if (validatechequemodify(form) == true) {
        showSpinner("cheque");
        var url = '/cheque/modify';

        if (form.entitydata.value == 'acknowledge') {
            url = '/cheque/acknowledge';
        }
        if (form.entitydata.value == 'delay') {
            url = '/cheque/delay';
        }
        if (form.entitydata.value == 'revokeleaves' || form.entitydata.value == 'revokebook') {
            url = '/cheque/revoke';
        }
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
        // postRequest.postdata(url);
        postRequest.postdata(url, resultcheque);

      
            $("form#upload_form").submit(function () {
                // showSpinner("freeze");
                var formData = new FormData($(this)[0]);
                var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value,this.entitydata.value, this.file_upload.value);
                postRequest.appendinputsformdata(formData);
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    async: false,
                    success: resultcheque,
                    error: resultcheque,
                    cache: false,
                    contentType: false,
                    processData: false
                });

                return false;
            });
            $("form#upload_form").submit();
    }
}

/**
 * POST  ChequeController remove
 * This revokes a cheque on account
 */
function revokecheque(form) {
    showSpinner("cheque");
    var url = '/cheque/revoke';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url,resultcheque);
}


/**
 * POST  ChequeController remove
 * This delays a cheque on account
 */
function delaycheque(form) {
    showSpinner("cheque");
    var url = '/cheque/delay';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url,resultcheque);
}


/**
 * POST  ChequeController remove
 * This acknowledges a cheque on account
 */
function acknowledgecheque(form) {
    showSpinner("cheque");
    var url = '/cheque/acknowledge';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url,resultcheque);
}
/**
 * POST  ChequeController remove
 * This verifies a cheque action
 */
function verifycheque(form) {
    // alert(form.entityid.value);
    showSpinner("cheque");
    var url = '/cheque/verify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url,resultcheque);
}
/**
 * POST  ChequeController remove
 * This declines a cheque action
 */
function declinecheque(form) {
    if (validatechequedecline(form) == true){
        showSpinner("cheque");
        var url = '/cheque/decline';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
        postRequest.postdata(url, resultcheque);
    }
}
// This method displays the view for declining when called
function chequedecline(id){
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Cheque decline form</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory'  />" +
        "<div >" +
        "<label for='reason'>Reason </label>" +
        "<input class='form-control bcon-field' type='text' id='entitykey' name='entitykey' onkeyup='validatechequedecline(this.form)'  required />" +
        "<small class='text-muted' id='decreason'>Please enter a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' class='form-control dpicker bcon-field' type='text' onkeyup='validatechequedecline(this.form)' name='entitydata' />" +
        "<small class='text-muted' id='decremark'>please enter a comment</small>" +
        "</div>" +
        "</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='declinecheque(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='chequeitem("+id+")'/>" +
        "</div>" +
        "</form>";
        hideSpinner("cheque");
    document.getElementById("chequetop").innerHTML = html;
}

/**
 * POST  ChequeController cancel
 * This cancels a cheque action
 */
function cancelcheque(form) {
    showSpinner("cheque");
    var url = '/cheque/cancel';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url,resultcheque);
}

function updatefield(form) {
    if(form.entitykey.value.toString().length > 8){
        document.getElementById("wbgnno").innerHTML = "<font color=red>Maximum number required is 8</font>";
        return false;
    }else{
        document.getElementById("wbgnno").innerHTML = "<font color=green>Valid</font>";
    }
    form.entityidd.value = (25 + parseInt(form.entitykey.value))-1;
}

function chequeBomBom(data){
    console.log("ENTERED!")
    var html = "";
    html = html + "<form action='#' method='POST'>";
    html = html+ "<div class='col-md-12'>"+
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitycategory' name='entitycategory'/>" +
        "<input type='hidden' class='form-control bcon-field' type='text' id='entityid' name='entityid' value='"+data.id+"'/>" +
        "<input type='hidden' class='form-control bcon-field' type='text' id='entitydata' name='entitydata' value='"+data.id+"'/>";
    if ((data.status == "BOOK_ACKNOWLEDGED" || data.status == "LEAVES_STOPPED") && data.category == "CHEQUE" && data.expectingForm!="YES") {
        html = html + "<input type='button' id='stopCheque' class='btn btn-danger btnpad pull-right' Value='Destroy' onClick='removecheque(this.form )'/>";
        if (data.status != "BOOK_DESTROYED" && getAccountType().indexOf("OD")>= 0) {
            html = html + "<input type='button' id='modifyCheque' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatecheque(" + JSON.stringify(data) + ")'/>";
        }
    }else if (data.status.indexOf("AWAITING") >= 0) {
        if(document.getElementById("currentUserId").value == data.updatedBy.id) {
            html = html + "<input type='button' id='modifyCheque' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatenewcheque(" + JSON.stringify(data) + ")'/>";
            html = html + "<input type='button' id='cancelCheque' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelcheque(this.form)'/>";
        }
        if (data.updatedBy.branch.sol == document.getElementById("currentUserSol").value) {
            html = html + "<input type='button' id='verifyCheque' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifycheque(this.form)'/>" +
                "<input type='button' id='declineCheque' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='chequedecline(" + data.id + ")'/>";
        }
    } else {
        if (data.status.indexOf("DECLINE") >= 0) {
            if(document.getElementById("currentUserId").value == data.updatedBy.id) {
                html = html + "<input type='button' id='cancelCheque' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelcheque(this.form)'/>";
            }
        }
        if (data.status != "BOOK_DESTROYED" && getAccountType().indexOf("OD")>= 0) {
            html = html + "<input type='button' id='modifyCheque' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatecheque(" + JSON.stringify(data) + ")'/>";
        }
    }
    html= html +"</div></form>";
    $("#chequetop").append(html);
    getChequeRole();
}


function chequeitem(id,accid){
    showSpinner("cheque");
    //START HIGHLIGHTING//
    $(".chequeTable tr").click(function() {
        $("tr").removeClass("highlightcolor");
        $(this).addClass("highlightcolor");
    });
    //END HIGHLIGHTING
    // var selected = $("#chequebottom").hasClass("highlight");
    // $("#chequebottom tr").removeClass("highlight");
    // if(!selected)
    //     $(this).addClass("highlight");
    // showSpinner("cheque");
    url = '/cheque/getdata';

    $( "#chequetop" ).load( "/cheque/get/"+id );

    var postdata = {
        'id': id,
        'accid': accid
    };
    var responsedata =  chequepost(postdata, url);
//    explodefreeze(responsedata);

    hideSpinner("cheque");
}

function chequepost(data, url){
    console.log("IN!")
    // showSpinner("cheque");
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        url: url,
        success: function(data) {
            console.log(data)
            // explodecheque(data);
            chequeBomBom(data)
            hideSpinner("cheque");
        },
        error : function(data) {
              console.log(data)
            return data;
        }
    });
}

function nextp(){
    if(document.getElementById("entitykey").value.toString().length > 8){
        document.getElementById("wbgnno").innerHTML = "<font color=red>Maximum number required is 8</font>";
        return false;
    }
    $(".page1").css('display','none');
    $(".page2").css('display','block');
    $(".pageb").css('display','block');
    $(".pagen").css('display','none');

    /*
    var beginChequeNo = $('#entitykey').val();
    var postdata = {'id': beginChequeNo,'accid': 'mbongo'};
    $.ajax({
        type:'POST',
        url:'/cheque/validateentrynumber',
        contentType: 'application/json',
        async:false,
        data: JSON.stringify(postdata),
        dataType:'json',
        success:function(data){
            console.log("result: " + data);
            if(data == '0'){
                document.getElementById("inventorymessage").innerHTML = "<font color=red>The inventory item is not available for this user</font>";
                document.getElementById("wbgnno").style.display = 'none';
                return false;
            }else{
                document.getElementById("inventorymessage").innerHTML = "<font color=green>Valid</font>";
                $(".page1").css('display','none');
                $(".page2").css('display','block');
                $(".pageb").css('display','block');
                $(".pagen").css('display','none');
            }
        },
        error:function () {console.log('error : ' + data);}
    });
    */
}

function backp(){
    $(".page2").css('display','none');
    $(".page1").css('display','block');
    $(".pageb").css('display','none');
    $(".pagen").css('display','block');
}
$("tr").click(function() {
    var selected = $(this).hasClass("highlight");
    $("tr").removeClass("highlight");
    if(!selected)
        $(this).addClass("highlight");
});

function getAccountType(){
    var postdata = {'id':$('#acctid').val(),'accid':$('#acctid').val()};
    var atype=""
    $.ajax({
        type:'POST',
        url:'/account/accounttype',
        async:false,
        data:postdata,
        dataType:'json',
        success:function(data){atype=data.accountScheme},
        error:function () {console.log("Error occured");}
    });
    return atype;
}
function newcheque(noB){
    if (getacctstatus()=="D" || getacctpndstatus()=="Y"){
        bootbox.alert("Cannot issue booklet to a dormant account or account on PND");
        return false;
    }else{
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>New Booklet</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='1' />" +
        "<input id='entitycategory' name='entitycategory' value='WITHDRAWAL' type='hidden' />" +
        "<div class='page1'>" +
        "<label>Begin number </label> " +
        "<input id='entitykey' name='entitykey' type='number' onkeyup='updatefield(this.form)' onchange='updatefield(this.form)' class='form-control bcon-field' maxlength='8'/> <small id='inventorymessage'></small>" +
        "<small class='text-muted' id='wbgnno'>Please enter begin number</small>" +
        "</div>" +
        "<div class='page1'>" +
        "<label>No of leaves </label> " +
        "<input id='entitydata' name='entitydata' type='number' value='25' disabled='true' class='form-control bcon-field' /> " +
        "<small class='text-muted'>Number defaulted for account</small>" +
        "</div>" +
        "<div class='page2' style='display:none'>" +
        "<label>End number </label> " +
        "<input id='entityidd' name='entityidd' disabled class='form-control bcon-field'/>" +
        "<small class='text-muted'>End number calculated</small>" +
        "</div>" +
        "<div class='page2' style='display:none'>" +
        "<label>Collect charge </label>";
    if(noB==0){
        if(getAccountType()!="OD243" && getAccountType()!="OD255") {
            html = html + "<input id='lienremark' name='lienremark' type='text' value='N' disabled='true' class='form-control bcon-field'/>";
        }else{
            html = html + "<input id='lienremark' name='lienremark' type='text' value='Y' disabled='true' class='form-control bcon-field'/>";
        }
    }else {
        html = html + "<input id='lienremark' name='lienremark' type='text' value='Y' disabled='true' class='form-control bcon-field'/>";
    }
    html = html + "<small class='text-muted'>No charge for first request</small>" + "</div>" + "</div>" + "</fieldset>" + "</div>" +
        "<div class='page2' style='display: none;'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='addnewcheque(this.form)'/>" +
        "</div>" +
        "<div class='pageb' style='display: none;'>" +
        "<input type='button' class='btn btn-success btnpad pull-left' Value='Back' onClick='backp()'/>" +
        "</div>" +
        "<div class='pagen'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Next' onClick='nextp()'/>" +
        "</div>" +
        "</form>";
    hideSpinner("cheque");
    document.getElementById("chequetop").innerHTML = html;
}
}

function getChequeRole() {
    
    var role;
    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        dataType: 'json',
        url: '/user/role/cheque',
        success: function (data) {
            role = data;
            effectChequeRoles(role);
        },
        error : function (data) {


        }
    });

}

function effectChequeRoles(role){

    if(!role.declineCheque){
        $("#declineCheque").css("display", "none");
        $("#declineCheque").prop("disabled", true);

    }if(!role.modifyCheque){
        $("#modifyCheque").css("display","none");
        $("#modifyCheque").prop("disabled", true);
        $("#stopCheque").css("display","none");
        $("#stopCheque").prop("disabled", true);

    }if(!role.verifyCheque){
        $("#verifyCheque").css("display","none");
        $("#verifyCheque").prop("disabled", true);

    }if(!role.cancelCheque){
        $("#cancelCheque").css("display","none");
        $("#cancelCheque").prop("disabled", true);

    }

}

function explodecheque(data) {
    console.log("Entity id is " + data.id);
    alert('I am explode');

    // alert(data.expectingForm);
    if (data == null || data.id == "none") {
        document.getElementById("chequetop").innerHTML = "";
    } else {
        var hh = "";
        $(document).on("click",".openinquire",function(){
            $(".modal-content #chqbgnno").val(data.beginNumber.trim());
            $.ajax({
               type:'GET',
                url:'/cheque/chqlvsstatus/'+data.id+'',
                async:false,
                success:function(data){hh=data;document.getElementById("chqdetails").innerHTML = data},
            });
//            // alert(hh);
            $('#chequeflash').modal('show');
            // document.getElementById("chqdetails").innerHTML = hh;
        });
        $(document).on("click",".openupload",function(){
            $(".modal-content #chqbgnno").val(data.beginNumber.trim());
            $.ajax({
               type:'GET',
                url:'/cheque/chqlvsstatus/'+data.id+'',
                async:false,
                success:function(data){hh=data1;document.getElementById("chqdetails2").innerHTML = data},
            });
            // alert(hh);
            $('#chequeflash2').modal('show');
            // document.getElementById("chqdetails").innerHTML = hh;
        });

        html = "<form action='#' method='POST'>" +
            "<div class='col-md-12'>" +
            "<h6 class='modal-title pull-left' id='myModalLabel'>Cheque Details</h6>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
            "<fieldset class='form-group'>" +
            "<div style=' ' class='col-md-6'>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitycategory' name='entitycategory'/>" +
            "<h5 class='col-md-5'> Number of leavess: </h5><h5 class='col-md-6'  >" + data.value + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' id='entityid' name='entityid' value='"+data.id+"'/>" +
            "<h5 class='col-md-5'>Begin number: </h5><h5 class='col-md-6'  >" + data.beginNumber.trim() + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Status: </h5><h5 class='col-md-6' >" +sentenceCase(data.status).replace("_", " ").replace("_", " ") + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitydata' name='entitydata'/>" +
            "<h5 class='col-md-5' >Request Id: </h5><h5 class='col-md-6' >" + data.id + "</h5>" +
            "</div>" +
            "</div>" +
            "<div style='' class='col-md-6 '>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Update by:</h5><h5 class='col-md-6'  >" + data.updatedBy.username + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Update at:</h5><h5 class='col-md-6' >" + data.updatedBy.branch.solDescription + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Update date:</h5><h5 class='col-md-6' >" + convertJsonDate(data.updatedOn) + "</h5>" +
            "</div>";

            if (data.expectingForm == "YES") {
               html = html + "<div class='col-md-12'>" +
                "<h5 class='col-md-5'>Upload document:</h5><h5 class='col-md-6' >" + convertJsonDate(data.updatedOn) + "</h5>" +
                "</div>";
            }
            html = html + "</div>" +
            "</fieldset>" +
            "</div>" +
                "<div class='col-md-1'></div><div class='col-md-10'>" ;

                   html = html  +  "<a data-toggle='modal' data-id='inquirehere' title='' class='btn btn-md btn-primary openinquire' onclick='openInquireDialog()'>Inquire</a>";


            html = html + "</div>"+
            "<div class='col-md-12'>";
        if ((data.status == "BOOK_ACKNOWLEDGED" || data.status == "LEAVES_STOPPED" || data.status == "BOOK_ISSUED" ) && data.category == "CHEQUE") {
            html = html + "<input type='button' id='stopCheque' class='btn btn-danger btnpad pull-right' Value='Destroy' onClick='removecheque(this.form )'/>";
            if (data.status != "BOOK_DESTROYED" && getAccountType().indexOf("OD")>= 0) {
                html = html + "<input type='button' id='modifyCheque' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatecheque(" + JSON.stringify(data) + ")'/>";
            }
        }else if (data.status.indexOf("AWAITING") >= 0) {
            if(document.getElementById("currentUserId").value == data.updatedBy.id) {
                html = html + "<input type='button' id='modifyCheque' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatenewcheque(" + JSON.stringify(data) + ")'/>";
                html = html + "<input type='button' id='cancelCheque' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelcheque(this.form)'/>";
            }
            if (data.updatedBy.branch.sol == document.getElementById("currentUserSol").value) {
                html = html + "<input type='button' id='verifyCheque' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifycheque(this.form)'/>" +
                    "<input type='button' id='declineCheque' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='chequedecline(" + data.id + ")'/>";
            }
        } else {
            if (data.status.indexOf("DECLINE") >= 0) {
                if(document.getElementById("currentUserId").value == data.updatedBy.id) {
                    html = html + "<input type='button' id='cancelCheque' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelcheque(this.form)'/>";
                        }
                }
            if (data.status != "BOOK_DESTROYED" && getAccountType().indexOf("OD")>= 0) {
                html = html + "<input type='button' id='modifyCheque' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatecheque(" + JSON.stringify(data) + ")'/>";
            }
        }
        html= html +"</div>" +
        "</form>";

        document.getElementById("chequetop").innerHTML = html;
        getChequeRole();
        hideSpinner("cheque");
    }
}
//function openInquireDialog(){
// alert("i dey here");
// $('#chequeflash').modal('show');
// }


function updatecheque(data){
    html = "<form action='#' id='upload_form' enctype='multipart/form-data' method='POST' class='chequeform'>" +
        "<div class='col-md-12'>" +
        "<h3 class='modal-title pull-left' id='myModalLabel'>Cheque management</h3>" +
        "</div>" +
        "<div class=''><div class='col-md-12 cheque-scrollable'>"+
        "<fieldset class='form-group'>"+
        "<div style='float: left; clear: none;'>" + "<input type='hidden' name='entityid' id='entityid' value= '"+data.id+"' />"+
        "<div class=''><select name='entitydata' id='entitydata' required class='form-control' onchange='validatecheque modify(this.form)' >" +
        "<option value='' class='form-control'>--Select--</option>";
    if (data.status=="BOOK_ISSUED" || data.status=="BOOK_DELAYED" || data.status=="DELAY_BOOK_DECLINED" || (data.status=="ACKNW_BOOK_DECLINED")){
        html = html + "<option value='acknowledge' class='form-control'>Release</option>";
    }
    if ((data.status=="BOOK_ISSUED" && data.category=="CHEQUE") || (data.status=="DELAY_BOOK_DECLINED" && data.category=="CHEQUE")){
        html = html + "<option value='delay' class='form-control'>Delay</option>";
    }
    if ((data.status=="BOOK_ACKNOWLEDGED" && data.category=="CHEQUE") || (data.status=="STOP_LEAVES_DECLINED" && data.category=="CHEQUE")){
        html = html + "<option value='stopleaves' class='form-control'>Stop leaves</option>";
        // "<option value='stopbook' class='form-control'>Stop book</option>";
    }
    if ((data.status=="LEAVES_STOPPED" && data.category=="CHEQUE") || (data.status=="REVOKE_LEAVES_DECLINED" && data.category=="CHEQUE")){
        html = html + "<option value='revokeleaves' class='form-control'>Revoke leaves</option>"+
            "<option value='stopleaves' class='form-control'>Stop leaves</option>";
        // "<option value='revokebook' class='form-control'>Revoke book</option>";
    }
    html = html +"</select> " +
        "<small class='text-muted' id='moption'>Please select an action</small></div>"+
            "<div class=''>" +
        "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field'  data-preview-file-type='text' >" +
        "<small class='text-muted' id='fname'>Please select Document</small> " +
        "</div>"+
        "<div id='chequeinfo'><div > " +
        "<label for='begin-number'>Begin number</label> " +
        "<input id='entitykey'  name='entitykey' class='form-control bcon-field entitykey2' type='number' onkeyup='validatechequemodify(this.form)' /> " +
        "<small class='text-muted' id='cbgno'>mandatory</small> " +
        "</div> <div > <label for='leavesnumber'>No of leaves</label> " +
        "<input id='entitycategory' class='form-control bcon-field entitycategory2' type='text'  name='entitycategory' onkeyup='validatechequemodify(this.form)'/> " +
        "<small class='text-muted' id='clvno'>Please enter no of leaves</small> " +
        "</div></div> " +
        "</div> </fieldset></div>"+
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='modifycheque(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='chequeitem(this.form.entityid.value)'/>" +
        "</div></div>" +
        "</form>";
    hideSpinner("cheque");
    document.getElementById("chequetop").innerHTML = html;

}

function updatenewcheque(data){
    html = "<form action='#' method='POST' class='chequeform'>" +
        "<div class='col-md-12'>" +
        "<h3 class='modal-title pull-left' id='myModalLabel'>Cheque management</h3>" +
        "</div>" +
        "<div class='col-md-12'>"+
        "<fieldset class='form-group'>"+
        "<div style='float: left; clear: none;'>" + "<input type='hidden' name='entityid' id='entityid' value= '"+data.id+"' />"+
        "<input id='entitycategory' name='entitycategory' value='WITHDRAWAL' type='hidden' />" ;
    html = html +"</select> " +

        "<div id='chequeinfo'><div > " +
        "<label for='begin-number'>Begin number</label> " +
        "<input id='entitykey'  name='entitykey' class='form-control bcon-field entitykey2' type='number' onkeyup='' /> " +
        "<small class='text-muted' id='cbgno'>mandatory</small> " +
        "</div></div> " +
        "</div> </fieldset></div>"+
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='modifynewcheque(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='chequeitem(this.form.entityid.value)'/>" +
        "</div>" +
        "</form>";
    hideSpinner("cheque");
    document.getElementById("chequetop").innerHTML = html;

}

function resultcheque(data){
    console.log("ZSM ish "+JSON.stringify(data));
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        // "<h4 class='modal-title pull-left' id='myModalLabel'>Cheque response page</h4>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +

        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ data.searchmessage +"' />" +
        "<div>" +
        "<label for='remarks'>"+ data.searchmessage +"</label>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='chequeitem("+data.entityid+","+data.searchacctid+")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("cheque");
    document.getElementById("chequetop").innerHTML = html;
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        url: '/cheque/getchequebottom',
        success: reloadchequebottom,
        error : reloadchequebottom
    });

}
function reloadchequebottom(data){

    htmlbottom = "<table class='table table-hover table-condensed chequeTable'> " +
        "<thead> <tr> <th>Category</th> <th>Begin no.</th> <th>Status</th> " +
        "<th><a type='button'  class='pull-right btn btn-sm' href='javascript:newcheque(25)'> " +
        "<i class='fa fa-plus-circle'></i>&nbsp;New booklet</a></th> </tr> </thead>" +
        "<tbody>";
    for (var i=0;i<data.length;i++){
        if (i==0){
            htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:chequeitem(" +data[i].id+","+data.searchacctid+ ")&apos;'  role='button' class='highlightcolor'>";
        }else{
            htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:chequeitem(" +data[i].id+","+data.searchacctid+ ")&apos;'  role='button'>";
        }
            htmlbottom = htmlbottom + "<td>"+sentenceCase(data[i].category)+"</td>"+
            "<td>"+data[i].beginNumber+"</td>" +
            "<td>"+sentenceCase(data[i].status)+"</td><td></td>" +
            "</tr>";
    }
    htmlbottom = htmlbottom + "</tbody> </table>";
    document.getElementById("chequebottom").innerHTML = htmlbottom;

}

function uploadChequeDocument(form){
    $("form#upload_form").submit(function () {
        // showSpinner("freeze");
        var formData = new FormData($(this)[0]);
        var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value,this.entitydata.value, this.file_upload.value);
        postRequest.appendinputsformdata(formData);
        $.ajax({
            url: '/cheque/uploadinstruction',
            type: 'POST',
            data: formData,
            async: false,
            success: function(data){
                if (data.response=="NO"){
                    document.getElementById("upPage").innerHTML = "Upload successful";
                    chequeitem(form.entityid.value);
                }
                else{
                    document.getElementById("Emessage").innerHTML = "Upload failed";
                }
            },
            error: function (){console.log("Error occured");document.getElementById("Emessage").innerHTML = "Upload failed";
            },
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });
    $("form#upload_form").submit();
}
function validatedocumentdecline(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("dreason").innerHTML = "<font color=blue>You must provide a reason</font>";
        return false;
    }else{document.getElementById("dreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("dcommentt").innerHTML = "<font color=blue>You must provide a comment</font>";
        return false;
    }else{document.getElementById("dcommentt").innerHTML = "<font color=green>Valid</font>";}
    return true;
}
function validatedocument(form){
    // alert(form.entitycategory.checked);
    // alert(document.getElementById('docInplace').checked);
    // alert(document.getElementById('docInplace').value);
    // alert(form.entitycategory.value);
    if(!document.getElementById('docInplace').checked) {
        // $("#dnamee").prop("disabled", false);
        // $("#dfile").prop("disabled", false);
        if (form.entitykey.value == null || form.entitykey.value == '') {
            document.getElementById("dnamee").innerHTML = "<font color=blue>Please enter a correct value</font>";
            return false;
        } else {
            document.getElementById("dnamee").innerHTML = "<font color=green>Valid</font>";
        }
        if (form.file_upload.value == null || form.file_upload.value == '' || form.file_upload.value == undefined) {
            document.getElementById("dfile").innerHTML = "<font color=blue>You must select a file</font>";
            return false;
        } else {
            document.getElementById("dfile").innerHTML = "<font color=green>Valid</font>";
        }
    }else {
        // document.getElementById("dnamee").prop("disabled", true);
        // document.getElementById("dfile").prop("disabled", true);
    }
    return true;
}

function disableOtherField(){
    if(!document.getElementById('docInplace').checked) {
        alert("no");
        document.getElementById("dnamee").prop("disabled", false);
        document.getElementById("dfile").prop("disabled", false);
    }else {
        alert("yes");
        document.getElementById("dnamee").prop("disabled", true);
        document.getElementById("dfile").prop("disabled", true);
    }
    }
function validateadddoc(form){
    if(!document.getElementById('docInplace').checked) {
    if (form.entitycategory.value==null || form.entitycategory.value==''){
        document.getElementById("dcategory").innerHTML = "<font color=blue>Please enter a category amount</font>";
        return false;
    }else{document.getElementById("dcategory").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("dname").innerHTML = "<font color=blue>Type document ref. no </font>";
        return false;
    }else{document.getElementById("dname").innerHTML = "<font color=green>Valid</font>";}
    if (form.file_upload.value==null || form.file_upload.value==''){
        document.getElementById("fname").innerHTML = "<font color=blue>You must provide a document</font>";
        return false;
    }else{document.getElementById("fname").innerHTML = "<font color=green>Valid</font>";}
    }else {
        $document.getElementById("dnamee").prop("disabled", true);
        document.getElementById("dfile").prop("disabled", true);
    }
    return true;
}
/**
 * POST  DocumentController addnew
 * This creates a new document on account
 */
function addnewdocument(form_id){
    if (validateadddoc(form_id)==true) {
        $("form#upload_form").submit(function () {
            showSpinner("document");
            var formData = new FormData($(this)[0]);
            var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value, this.file_upload.value);
            postRequest.appendinputsformdata(formData);
            $.ajax({
                url: '/document/addnew',
                type: 'POST',
                data: formData,
                async: false,
                success: resultdocument,
                error: resultdocument,
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });
        $("form#upload_form").submit();
    }else{return false;}
}


/**
 * POST  DocumentController remove
 * This removes a document on account
 */
function removedocument(form) {
    showSpinner("document");
    var url = '/document/remove';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultdocument);
}


/**
 * POST  DocumentController modify
 * This removes a document on account
 */
function modifydocument(form) {
    if (validatedocument(form)==true) {
        // alert(this.entitycategory.value);
        // alert(this.entityid.value);
        // alert('input[name=docInplace]:checked');
        alert(document.getElementById('docInplace').checked);
        alert(document.getElementById('docInplace').value);
        if(document.getElementById('docInplace').checked) {
            document.getElementById('docInplace').value= "Yes";
        }else{
            document.getElementById('docInplace').value= "No";
        }
        alert(document.getElementById('docInplace').value);
        $("form#upload_form").submit(function () {
            showSpinner("document");
            var formData = new FormData($(this)[0]);
            var postRequest = new PostRequest(this.entityid.value, document.getElementById('docInplace').value, this.entitykey.value, this.file_upload.value);
            postRequest.appendinputsformdata(formData);
            $.ajax({
                url: '/document/modify',
                type: 'POST',
                data: formData,
                async: false,
                success: resultdocument,
                error: resultdocument,
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });
        $("form#upload_form").submit();
    }else{return false;}
}


  function validatedefferal(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("duration").innerHTML = "<font color=red>You must select a duration</font>";
        return false;
    }else{document.getElementById("duration").innerHTML = "<font color=green>Valid</font>";}

    if (form.file_upload.value==null || form.file_upload.value==''){
        document.getElementById("deferraldoc").innerHTML = "<font color=red>You must upload a  document</font>";
        return false;
    }else{document.getElementById("deferraldoc").innerHTML = "<font color=green>Valid</font>";}


      if (form.entitykey.value == 'custom') {
              if (form.lienremark.value==null || form.lienremark.value==''){
        document.getElementById("expdaterem").innerHTML = "<font color=red>You must enter a date</font>";
        return false;
    }else{document.getElementById("expdaterem").innerHTML = "<font color=green>Valid</font>";}


      }



      if (form.entitydata.value==null || form.entitydata.value==''){
          document.getElementById("lienremark").innerHTML = "<font color=red>You must select a deferral remark</font>";
          return false;
      }else{document.getElementById("lienremark").innerHTML = "<font color=green>Valid</font>";}




    if (form.defauthority.value==null || form.defauthority.value==''){
        document.getElementById("deferralauthority").innerHTML = "<font color=red>You must select a deferral authority</font>";
        return false;
    }else{document.getElementById("deferralauthority").innerHTML = "<font color=green>Valid</font>";}

     return true;
 }



function pickDatedefer(){
    $('#expiryDate').datepicker({
        format: 'dd-mm-yyyy',
        todayBtn: "linked",
        startDate: '+0d',
        autoclose: true
    });
}













function deferdocument(form) {

    if (validatedefferal(form) == true) {

        $("form#upload_form").submit(function () {

            var formData = new FormData($(this)[0]);

            var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, this.file_upload.value, form.lienremark.value, form.defauthority.value);
            postRequest.appendinputsformdata(formData);
            $.ajax({
                url: '/document/deferdocument',
                type: 'POST',
                data: formData,
                async: false,
                success: resultdocument,
                error: resultdocument,
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });
        $("form#upload_form").submit();
   }
    else {
        return false;
    }
}

function expdateDisplay(form){
    if (form.entitykey.value == 'one_off') {
        $("#expdate").css("display", "none");
    }
    if (form.entitykey.value == 'custom') {
        $("#expdate").show();
        pickDatedefer();
        $("#expdate").css("display", "block");
    } else {
        $("#expdate").hide();
    }
    if (form.entitykey.value == 'default'){
        $("#expdate").css("display", "none");
    }

    // if (form.entitykey.value==null || form.entitykey.value==''){
    //     document.getElementById("duration").innerHTML = "<font color=red>You must select a duration</font>";
    //     return false;
    // }else{document.getElementById("duration").innerHTML = "<font color=green>Valid</font>";}
}

/**
 * POST  DocumentController verify
 * This verifies a document process
 */
function verifydocument(form) {
    showSpinner("document");
    var url = '/document/verify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultdocument);
}

/**
 * POST  DocumentController cancel
 * This cancels a document action
 */
function canceldocument(form) {
    showSpinner("document");
    var url = '/document/cancel';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultdocument);
}
/**
 * POST  DocumentController decline
 * This declines a document process
 */
//This method does the declination from the controller with ajax call
function declinedocument(form) {
    if(validatedocumentdecline(form)==true) {
        showSpinner("document");
        var url = '/document/decline';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
        postRequest.postdata(url, resultdocument);
    }else{return false}
}

function getDocumentRole() {
    var role;
    showSpinner("document");
    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        dataType: 'json',
        url: '/user/role/document',
        success: function (data) {
            role = data;
            hideSpinner("document");
            effectDocumentRoles(role)
        },
        error : function (data) {


        }
    });

}


function effectDocumentRoles(role){
    if(!role.viewLink){


        $('#viewLink').click(function(e) {
                    e.preventDefault();
                    //do other stuff when a click happens
                    bootbox.alert("Sorry You Are Not priviledged To Perfom This task")
                });

    }if(!role.addDocuments){


        $('#addDocument').click(function(e) {
                    e.preventDefault();
                    //do other stuff when a click happens
                    bootbox.alert("Sorry You Are Not priviledged To Perfom This task")
                });

    }if(!role.modifyDocument){
        $("#modifyDocument").css("display", "none");

        $("#modifyDocument").prop("disabled", true);
        $("#removeDocument").css("display", "none");

        $("#removeDocument").prop("disabled", true);

    }if(!role.verifyDocument){
        $("#verifyDocument").css("display", "none");

        $("#verifyDocument").prop("disabled", true);

    }if(!role.declineDocument){
        $("#declineDocument").css("display", "none");

        $("#declineDocument").prop("disabled", true);

    }if(!role.cancelDocument){
        $("#cancelDocument").css("display", "none");

        $("#cancelDocument").prop("disabled", true);

    }
    

}


function documentitem(id){
    showSpinner("document");
    //START HIGHLIGHTING
    $(".documentTable tr").click(function() {
        $("tr").removeClass("highlightcolor");
        $(this).addClass("highlightcolor");
    });
    //END HIGHLIGHTING
    getDocumentRole();
    url = '/document/getdata'
    var postdata = {
        'id': id
    }
    $( "#documenttop" ).load( "/document/get/"+id );
    //process(url, postdata, explodedocument);
    hideSpinner("document");
}
















function deferdocumentpage(fdata){
    $.ajax({
        type:'POST',
        contentType:'application/json',
        data:JSON.stringify(fdata),
        async:false,
        url:'/document/deferdocumentpage',
        success:function(data){$("#documenttop").html(data)},
        error:function(){  }
    });
}

function explodedocument(data){
    if(data==null || data.id=="none"){document.getElementById("documenttop").innerHTML = "";}else {
        html = "<form action='#' method='POST'>" +
            "<div class='col-md-12'>" +
            "<h6 class='modal-title pull-left' id='myModalLabel'>Document Details</h6>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='" + data.id + "' id='entityid' name='entitid'/>" +
            "<fieldset class='form-group'>" +

            "<div style='' class='col-md-6'>" +
            "<div>" +
            "<input type='hidden' class='form-control bcon-field' type='text' name='entitydata' id='entitydata' name='entitydata'/>" +
            "<input type='hidden' class='form-control bcon-field' type='text' name='entitycategory' id='entitycategory' name='entitycategory'/>" +
            "<h5 class='col-md-5' >Ref. Number: </h5><h5 class='col-md-6' >" + data.value + "</h5>" +
            "</div>" +
            "<div>" +
            "<h5 class='col-md-5'>Status: </h5><div class='col-md-6'><h5>" + sentenceCase(data.status).replace("_", " ").replace("_", " ").replace("_", " ") + "</h5></div>" +
            "</div>" +
            "<div>" +
            "<h5 class='col-md-5'><a role='button' onclick='showDocumentPDF("+ JSON.stringify(data.accessURL)+")'>View Document</a></h5>" +
            "</div>" +
            "</div>" +

            "<div style='' class='col-md-6'>" +
            "<div>" +
            "<h5 class='col-md-5'>Update by:</h5><h5 class='col-md-6'  >" + data.updatedBy.username + "</h5>" +
            "</div>" +
            "<div>" +
            "<h5 class='col-md-5'>Update at:</h5><h5 class='col-md-6' >" + data.updatedBy.branch.solDescription + "</h5>" +
            "</div>" +
            "<div>" +
            "<h5 class='col-md-5'>Update date:</h5><h5 class='col-md-6' >" + convertJsonDate(data.updatedOn) + "</h5>" +
            "</div>" +
            "</div>" +

            "</fieldset>" +
            "</div>" +
            "<div class='col-md-12'>";
        if ((data.status == "NEW_DOCUMENT_OUTSTANDING") || (data.status == "NEW_DOCUMENT_ACTION_REQUIRED")|| (data.status.indexOf("EXPIRED") >= 0)) {
            html = html + "<input type='button' class='btn btn-danger btnpad pull-right' Value='Defer' onClick='deferdocumentpage(" + JSON.stringify(data) + ")'/>";
            html = html + "<input type='button' id='modifyDocument' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatedocument(" + JSON.stringify(data) + ")'/>";
        }
        if (data.status.indexOf("AWAITING") >= 0) {
            if (data.updatedBy.branch.sol == document.getElementById("currentUserSol").value) {
                html = html + "<input type='button' id='verifyDocument' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifydocument(this.form)'/>";
                html = html + "<input type='button' id='declineDocument' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='documentdecline(" + data.id + ")'/>";
            }
            if (document.getElementById("currentUserId").value == data.updatedBy.id.toString()) {
                html = html + "<input type='button' class='btn btn-danger btnpad pull-right' id='cancelDocument' Value='Cancel' onClick='canceldocument(this.form)'/>";
                html = html + "<input type='button' id='modifyDocument' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatedocument(" + JSON.stringify(data) + ")'/>";

            }
        }
        if (data.status.indexOf("DECLINE") >= 0) {
            if (document.getElementById("currentUserId").value == data.updatedBy.id.toString()) {
                html = html + "<input type='button' class='btn btn-danger btnpad pull-right' id='cancelDocument' Value='Cancel' onClick='canceldocument(this.form)'/>";
                html = html + "<input type='button' id='modifyDocument' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatedocument(" + JSON.stringify(data) + ")'/>";

            }
        }
        if (data.status == "DOCUMENT_PRESENT" || data.status == "DEFERRED" || data.status == "VERFIED") {
            html = html + "<input type='button' id='removeDocument' class='btn btn-danger btnpad pull-right' Value='Remove' onClick='removedocument(this.form)'/>";
            html = html + "<input type='button' id='modifyDocument' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatedocument(" + JSON.stringify(data) + ")'/>";
        }


    html = html + "</div>" + "</form>";
    hideSpinner("document");
    document.getElementById("documenttop").innerHTML = html;
    getDocumentRole()
}
}

function newdocument(){
    html = "<form action='#' id='upload_form' enctype='multipart/form-data' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>New Document</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<input type='hidden' name='searchacctid2' value='"+ acctid +"' >"+

        "<input type='hidden' name='entityid' id='entityid' value='1' />" +
        // "<input type='hidden' name='entitycategory' id='entitycategory' value='DRIVERS-LICENSE' />" +
        "<div class='col-md-12'>" +
        "<label for='category'>Document type </label>" +
        "<input id='entitycategory' class='form-control bcon-field' type='text' onkeyup='validateadddoc(this.form)' name='entitycategory' required />" +
        "<small class='text-muted' id='dcategory'>Please enter a document type</small>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<label for='category'>Document No</label>" +
        "<input id='documentname' class='form-control bcon-field' type='text' onkeyup='validateadddoc(this.form)' name='entitykey' required />" +
        "<small class='text-muted' id='dname'>Please enter a document ref. no/description</small>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field' onchange='validateadddoc(this.form)' data-preview-file-type='text' >" +
        "<small class='text-muted' id='fname'>Please select Document</small>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='addnewdocument(this.form)'/>" +
        // "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='documentitem("+id+")' /> " +
        "</div>" +
        "</form>";
    hideSpinner("document");
    document.getElementById("documenttop").innerHTML = html;
}



//This displays the view for the decline form
function documentdecline(id){

    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Document Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        // "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +

        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory'  />" +

        "<div >" +
        "<label for='reason'>Reason </label>" +
        "<input class='form-control bcon-field' type='text' id='entitykey' name='entitykey' onkeyup='validatedocumentdecline(this.form)' required />" +
        "<small class='text-muted' id='dreason'>Please enter a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' class='form-control dpicker bcon-field' type='text' name='entitydata' onkeyup='validatedocumentdecline(this.form)'  />" +
        "<small class='text-muted' id='dcommentt'>please enter a comment</small>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='declinedocument(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='documentitem("+id+")' /> " +
        "</div>" +
        "</form>";
    hideSpinner("document");
    document.getElementById("documenttop").innerHTML = html;
}



function updatedocument(data){
    html = "<form action='#' id='upload_form' enctype='multipart/form-data' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>New Document</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<input type='hidden' name='searchacctid2' value='"+ acctid +"' >"+

        "<input type='hidden' name='entityid' id='entityid' value='"+data.id+"' />" +
        "<input type='hidden' name='' id='' value='"+data.id+"' />" +
        "<input type='hidden' name='entitydata' id='' value='DRIVERS-LICENSE' />" +
        "<div class='col-md-12'>" +
        "<label for='category'>Document No</label>" +
        "<input id='documentname' class='form-control bcon-field' type='text'  name='entitykey' onkeyup='validatedocument(this.form)' required />" +
        "<small class='text-muted' id='dnamee'>Please enter a document name/description</small>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field' onchange='validatedocument(this.form)' data-preview-file-type='text' >" +
        "<small class='text-muted' id='dfile'>Please select Document</small>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<label><input id='docInplace' name='docInplace' type='checkbox' value='inplace'>Document already in place</label>" +
        "</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='buttom' class='btn btn-success btnpad pull-right' Value='Submit' onClick='modifydocument(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='documentitem("+data.id+")' /> " +
        "</div>" +
        "</form>";
    hideSpinner("document");
    document.getElementById("documenttop").innerHTML = html;
}

function resultdocument(data){
    if (data.entityid==null || data.entityid=="" || data.entityid=="none" || data.entityid=="undefined" || data.entityid==undefined){html="Document removed";hideSpinner("document");}else{
        html = "<div class='col-md-12'>" +
        "<div class='col-md-12 response-box'><div class='inner-response'>" +
        "<label><h3>"+ data.searchmessage +"</h3></label><p></p>" +
        "<label><h7>Request Id: "+ data.entityid +"</h7></label>" +
        "</div><div class='col-md-12'>" +
        "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='documentitem("+data.entityid+")'/>" +
        "</div></div></div>";
    hideSpinner("document");
    document.getElementById("documenttop").innerHTML = html;
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        url: '/document/getdocumentbottom',
        success: reloaddocumentbottom,
        error : reloaddocumentbottom
    });
}
}
function reloaddocumentbottom(data){

    htmlbottom = "<table class='table table-hover table-condensed documentTable'> " +
        "<thead> <tr> <th>Category</th> <th>Type</th> <th>Status</th> " +
        "<th><a type='button'  class='pull-right btn btn-sm' href='javascript:newdocument()'> " +
        "<i class='fa fa-plus-circle'></i>&nbsp;New document</a></th> </tr> </thead>" +
        "<tbody>";
    for (var i=0;i<data.length;i++){
        if(i==0){
            htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:documentitem(" +data[i].id+ ")&apos;'  role='button' class='highlightcolor'>";
        }else{
            htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:documentitem(" +data[i].id+ ")&apos;'  role='button'>";
        }
        htmlbottom = htmlbottom + "<td>"+data[i].category+"</td>"+
            "<td>"+"</td>" +
            "<td>"+sentenceCase(data[i].status).replace("_"," ").replace("_"," ").replace("_"," ")+"</td><td>" +
            " <a role='button' onclick='showDocumentPDF("+ JSON.stringify(data.accessURL)+")' class='pull-right btn btn-sm'>"+
            " <i class='fa fa-plus-circle'></i>&nbsp;View</a>" +
            "</td>" +
            "</tr>";
    }
    htmlbottom = htmlbottom + "</tbody> </table>";
    document.getElementById("documentbottom").innerHTML = htmlbottom;
}








/**
 * Created by GBASKI-CODDED on 17/11/2016.
 */


function addDocDecline(){
    var url = "/setting/modify"
    var work = "1"
    var value = document.getElementById("newReason").value;

    var postInput = {work:work,value:value};

    post(url, postInput);
}
function removeDocDecline(value){
    var url = "/setting/modify"
    var work = "2"
    var postInput = {work:work,value:value};

    post(url, postInput);
}

function modifyOneoff(){
    var url = "/setting/modify"
    var work = "3"
    var value = document.getElementById("oneoff").value;

    if(value !== "" || value !== undefined || value !== "0"){
        var postInput = {work:work,value:value};

        post(url, postInput);
    }else{
        bootbox.alert("Sorry, OneOff column has to be greater than 0")
    }


}
function modifyDefault(){
    var url = "/setting/modify"
    var work = "4"
    var value = document.getElementById("default").value;

    if(value != "" || value != undefined || value !="0"){
        var postInput = {work:work,value:value};

        post(url, postInput);
    }else{
        bootbox.alert("Sorry, Default column has to be greater than 0")
    }
}

function addLienCategory(){

    var work = "5";
    var url = "/setting/modify";
    var value = document.getElementById("newLienCat").value;
    var code = document.getElementById("newLienCatCode").value;
    var postInput = {work:work,value:value,code:code};

    post(url, postInput);
}
function removeLienCategory(value){
    var url = "/setting/modify"
    var work = "6"

    var postInput = {work:work,value:value};

    post(url, postInput);
}

function addFreezeType(){
    var url = "/setting/modify";
    var work = "7";
    
    // alert("value is "+value);
    // alert("code is "+code);
    var value = document.getElementById("newFreezeType").value;
    var code =  document.getElementById("newFreezeTypeFinacleDsc").value;

    var postInput = {work:work,value:value,code:code};

    post(url, postInput);
}
function removeFreezeType(value){
    var url = "/setting/modify"
    var work = "8"

    var postInput = {work:work,value:value};

    post(url, postInput);
}

function addFreezeReason(){
    var url = "/setting/modify";
    var work = "9"
    var value = document.getElementById("newFreezeReason").value;
    var code = document.getElementById("newFreezeReasonCode").value;

    var postInput = {work:work,value:value,code:code};

    post(url, postInput);
}

function removeFreezeReason(value){
    var url = "/setting/modify";
    var work = "10"

    var postInput = {work:work,value:value};

    post(url, postInput);
}

function addDeferralAuthority(){
    var url = "/setting/modify";
    var work = "11"
    var value = document.getElementById("newDefAuth").value;
    var postInput = {work:work,value:value};

    post(url, postInput);
}

function removeDeferralAuthority(value){
    var url = "/setting/modify";
    var work = "12"

    var postInput = {work:work,value:value};

    post(url, postInput);
}



function getlienCategory(){

    $.ajax({
        url:'/setting/json/1',
        type:'GET',
        data: '',
        dataType: 'json',
        success: function( json ) {

            $.each(json, function(i, value) {
                $("[name='liencategory']").append($('<option>').text(value).attr('value', value));
            });
        }
    });
    
}

function getFreezeType(){
    
}

function getFreezeReason() {
    
}
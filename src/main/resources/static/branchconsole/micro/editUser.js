/**
 * Created by user on 3/9/2017.
 */

function userSetting(username) {

    var postdata = {'username': username};
    // alert(username);
    $("#userclickid").val(username);
    // $("#selectedUsername").innerHTML = username;
    document.getElementById("selectedUsername").innerHTML= username;
    // alert(JSON.stringify(postdata));
    $.ajax({
            type: 'POST',
            url: '/user/getAllPrivilege',
            data: postdata,
            async: false,
            success: function (data) {
                if (data.branchList!=null && data.branchList!="" && !data.branchList!=undefined ) {
                    var arrr = data.branchList.split(",");
                }
                var rrr=data.role;
                $("#roledata select option").filter(function() {
                    //may want to use $.trim in here
                    // alert("called");
                    return $(this).val() == rrr;
                }).attr('selected', true);
                var bbb = data.branch;
                toggleBranchListEdit();

                $("#branchdata22 select option").filter(function() {
                    //may want to use $.trim in here
                    console.log("called2");
                    return $(this).val() == bbb;
                }).attr('selected', true);
                console.log("i am logging "+data);
                displayPrivilege(data);
                // document.getElementById('useredit').innerHTML = data;
            },
            error: function () {
                console.log("Error occured");
            }
        }

    )};



//submit button on edit user
function adjustUserSetting() {
    // var name = {'username': username};
    var left = [];
    $.each($("#leftValues option"), function(){
        // alert($(this).text());
        left.push($(this).text());
    });
    // alert("left val" +left);

    var right =[];
    $.each($("#rightValues option"),function () {
        //alert($(this).text());
        right.push($(this).text());

    });
    var branch = document.getElementById("otherUser");
    var role = document.getElementById("role2");
    var privNotInUse = document.getElementById("leftValues");
    var currentPriv = document.getElementById("rightValues");
    var enable = $('#isenabled').val();




    var uuu = "user";
    var uuuu = $("#zsmUser").val();
    var selectedValues = [];
    $("#zsmUser :selected").each(function(){
        //alert($(this).val());
        selectedValues.push($(this).val());
    });
    var postdata = {'zsmuser':JSON.stringify(selectedValues),'branch':$("#otherUser").val(),'role':$("#role2").val(),'username':$("#userclickid").val(),'right':right,'left':JSON.stringify(left),'enable':$("#isenabled").val()};
    // alert("left user" +$("#userclickid").val());
    // alert("zsm" +$("#zsmUser").val());
    console.log(postdata)
    $.ajax({
        type:'POST',
        url:'/user/adjustUserSetting',
        async:false,
        data: postdata,
        success:function(data){console.log(data);
        bootbox.alert("Successfully modified User!");
        },
        error:function () {
            console.log("Error here");
        }
    });



}
//end function submit button on edit user

//check box for user enable on edit user
function toggleEnable(){
    if ($("#isenabled").val()=="true"){
        $("#isenabled").val("false");
        $("#isenabled").attr("checked",false);
        document.getElementById("enabledText").innerHTML="User disabled";
    }else{
        $("#isenabled").val("true");
        $("#isenabled").attr("checked",true);
        document.getElementById("enabledText").innerHTML="User enabled";
    }
}
//end user enable on edit user

//function to display priviledges of users on edit user
function displayPrivilege(data) {
    console.log("Enn "+data.isEnabled);
    var is_enabled = data.isEnabled;
    $("#isenabled").val(is_enabled);
    // alert("Enable "+is_enabled);
    if(is_enabled==0){
        document.getElementById("enabledText").innerHTML="User disabled";
        $("#isenabled").val("false");
        $("#isenabled").attr("checked",false);
    }else{
        document.getElementById("enabledText").innerHTML="User enabled";
        $("#isenabled").val("true");
        $("#isenabled").attr("checked",true);
    }



    var privileges = [data.addLien, data.verifyLien, data.declineLien,
        data.cancelLien, data.modifyLien, data.submitLien, data.addFreeze, data.verifyFreeze,
        data.modifyFreeze, data.cancelFreeze, data.declineFreeze,
        data.submitFreeze, data.removeFreeze, data.addDocuments, data.modifyDocument, data.verifyDocument,
        data.cancelDocument, data.declineDocument, data.viewLink, data.submitDocument,data.chooseDocument,
        data.addManagersCheque, data.verifyManagersCheque, data.acceptManagersCheque, data.submitManagersCheque,
        data.removeManagersCheque, data.continueManagersCheque, data.loadManagersCheque, data.modifyAccountDetails,
        data.verfiyAccount, data.declineAccount, data.cancelAccount, data.reactivateAccount, data.newMandate, data.viewMandate,
        data.modifyMandate, data.declineMandate, data.verifyMandate, data.submitMandate,
        data.removeMandate, data.cancelMandate, data.modifyCustomerDetails, data.declineCustomerDetails, data.verifyCustomerDetails,
        data.cancelCustomerDetails, data.modifyCheque, data.verifyCheque, data.cancelCheque, data.requestLetter,
        data.waivecharge, data.verifyCharge, data.declineCharge];
    //alert("status"+data.addLien);

    var privilegeDesp = ["Add Lien", "Verify Lien", "Declien Lien", "Cancel Lien", "Modify Lien", "Submit Lien",
        "Add Freeze", "Verify Freeze", "Modify Freeze", "Cancel Freeze", "Decline Freeze", "Submit Freeze", "Remove Freeze",
        "Add Documents", "Modify Document", "Verify Document", "Cancel Document", "Decline Document", "View Link","Submit Document","Choose Document",
        "Add Managers Cheque", "Verify Managers Cheque", "Accept Managers Cheque",
        "Verify Managers Cheque", "Accept Managers Cheque", "Submit Managers Cheque", "Remove Managers Cheque",
        "Continue Managers Cheque", "Load Managers Cheque", "Modify Account Details", "Verify Account",
        "Decline Account", "Cancel Account", "Reactivate Account", "New Mandate", "View Mandate", "Modify Mandate", "Decline Mandate",
        "Verify Mandate", "Submit Mandate", "Remove Mandate", "Cancel Mandate", "Modify Customer Details",
        "Decline Customer Details", "Verify Customer Details", "Cancel Customer Details", "Modify Cheque", "Verify Cheque",
        "Cancel Cheque", "Request Letter", "Waive Charge", "Verify Charge", "Decline Charge"]




    var sel = document.getElementById('leftValues');
    //alert("left val", +sel);
    var fragment = document.createDocumentFragment();
    privileges.forEach(function (privilege, index) {

        if (privilege == false) {
            var opt = document.createElement('option');
            opt.innerHTML = privilegeDesp[index];
            opt.value = privilege;
            fragment.appendChild(opt);
            //alert(index);
            //alert('description', +opt.innerHTML);
            //alert("privilege", +opt.value);
        }

    });

    sel.appendChild(fragment);
    var priv = document.getElementById('rightValues');
    var frag = document.createDocumentFragment();
    privileges.forEach(function (privilege, index) {
        if (privilege == true) {
            var opt = document.createElement('option');
            opt.innerHTML = privilegeDesp[index];
            opt.value = privilege;
            frag.appendChild(opt);
        }
        ///alert("true privilege", +opt.value);
    });
    priv.appendChild(frag);
}
//end function to display priviledges on edit user


//branch list for users on edit user
function toggleBranchListEdit(){

    var data = {};
    var data1 = {};
    $.ajax({
        type: "POST",
        contentType: 'application/json',
        dataType: 'json',
        url: "/settings/fetchBranchesId",
        data: JSON.stringify(data),
        async: false,
        success: function (data) {
            data1 = data;
        },
        error: function (data) {
            console.log('error occured ==> ' + data);
        }
    });
    var role = $("#role2").val();
    // alert("role "+ role);
    var html = "<select  id='otherUser' class='form-control'>";
    for (i = 0; i < data1.length; i++) {
        html = html + "<option value=" + data1[i].id + ">" + data1[i].solDescription + "</option>";
    }
    html = html + "</select>";

    //create branch for zsms
    var newhtml = "<select  id='zsmUser' multiple='multiple' >";
    for (i = 0; i < data1.length; i++) {
        newhtml = newhtml + "<option value=" + data1[i].id + ">" + data1[i].solDescription + "</option>";
    }
    newhtml = newhtml + "</select>";
    if(role === 'ZONAL_SERVICE_MANAGER'){
        //hide branch for normal users, show branch for zsms
        document.getElementById("branchdata22").innerHTML = newhtml;
        $('#zsmUser').multiselect({
            enableFiltering: true,
            maxHeight: 100,
            numberDisplayed: 1
        });
    }else{
        //hide branch for zsms and show branch normal users
        document.getElementById("branchdata22").innerHTML = html;
    }
}
// end branch list for users on edit user

//function to filter through the list of users(in the table) on edit user
function userSearcher() {
    //alert("hello");
    var input, filter, table, tr, td, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    // alert("tr",tr);
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
//end function to filter through the list of users(in the table) on edit user
$(function()
{
    $(".clickable-row").click(function (event) {
        var idVal = $(this).attr('id');
//                    alert("You click this id: "+idVal);
        $(".clickable-row").removeClass('active');
        $("#" + idVal).addClass('active');
        //window.document.location = $(this).data("href");
    });
})
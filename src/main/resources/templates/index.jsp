<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<link>
    <title>Fidelity CPC</title>

    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">


    <link rel="icon" href="/branchconsole/Fidelity-Icon.png">

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/datatables.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/vendor/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/css/skin-black.css">
    <link href="/vendor/pace/pace.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/vendor/iCheck/all.css">
    <!-- jQuery 2.2.0 -->
    <script src="/vendor/jquery/jquery-2.2.0.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/js/app.min.js"></script>

    <script src="/vendor/iCheck/icheck.min.js"></script>
    <script src="/js/demo.js"></script>
    <script src="/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="/vendor/datatable-bootstrap/css/dataTables.bootstrap.min.css">
    <!--@*<script src="/vendor/datatables/media/js/jquery.dataTables.min.js"></script>*@-->

<!--<link href='routes.Assets.at("css/jquery-ui.css")' rel="stylesheet">-->
    <link href="/css/datepicker.css" rel="stylesheet">
    <link href="/css/bootstrap-multiselect.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

    <script src="/js/ie10-viewport-bug-workaround.js" type="text/javascript"></script>

    <script src="/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
    <script src="/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="/js/plugins/purify.min.js" type="text/javascript"></script>

    <!--<script src="/js/plugins/fileinput.min.js" type="text/javascript"></script>-->

    <script src="/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="/js/jszip.min.js" type="text/javascript"></script>
    <script src="/js/pdfmake.min.js" type="text/javascript"></script>
    <script src="/js/vfs_fonts.js" type="text/javascript"></script>
    <script src="/js/buttons.html5.min.js" type="text/javascript"></script>

    <script src="/js/Chart.min.js" type="text/javascript"></script>
    <script src="/js/highcharts.js" type="text/javascript"></script>
    <script src="/js/exporting.js" type="text/javascript"></script>
    <script src="/js/canvasjs.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap-multiselect.js" type="text/javascript"></script>

<script>
    function searchForAccount(form){
        if(form.searchValue.value == null || form.searchValue.value == ''){
            alert("Please enter valid account number ");
            console.log("Please enter valid account number ");
            $('#acctInfoBox').css("display", "none");
        }
        else {
            var accountSearch = form.searchValue.value;
            var postdata = {"acctid": accountSearch, "docId": "Adesola Tolulope"};
            $.ajax({
                type:"POST",
                contentType : "application/json",
                url:"/search",
                data: JSON.stringify(postdata),
                dataType : 'json',
                success:function (data) {
//                    $("#accounttop").html(data);
                    console.log("Success ==>",data);
                    resultAccountSearch(data);
                },
                error:function (data) {
                    console.log("Error ==>",data);
                }
            });
        }
    }

    function resultAccountSearch(data) {
        html = "<div class='box-header'>" +
            "<h3 class='box-title'>Account Information</h3>" +
            "</div><div class='box-body no-padding'>" +
            "<div class='col-md-6'>" +
            "<div><label for='AccountName'>Account Name: </label> <span>" +data.accountName+ "</span> </div>" +
            "<div><label for='AccountNumber'>Account Number: </label> <span>" +data.accountNumber + "</span> </div>" +
            "<div><label for='AccountBalance'>Account Balance: </label> <span>" +data.accountBalance +"</span></div>" +
            "<div><label for='AccountStatus'>Account Status: </label> <span>" +data.accountStatus + "</span></div>" +
            "<div><label for='AccountOpenDate'>Account Open Date: </label> <span>" +data.accountOpenDate + "</span></div>" +
            "<div><label for='CurrentStatus'>Current Status: </label> <span>" +data.currentStatus + "</span></div>" +
            "</div> <div class='col-md-6'> <input id='entitydata' type='file' class='file form-control bcon-field' name='file_pdf' data-preview-file-type='text' onchange='validateAccountSearch(this.form)'>" +
            "<small class='text-muted' id='filepdf'>Please upload pdf file</small> </div>" +
            "<div class='col-md-12'>" +
            "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onclick='submitAccountInfo(this.form)'/>" +
            "</div></div>";

        document.getElementById("acctInfoBox").innerHTML = html;
        $('#acctInfoBox').css("display", "block");
    }

    function validateAccountSearch(form){
        if (form.searchValue.value == null ||  form.searchValue.value == ''){
            alert("Please enter valid account number ");
            return false;
        }
        if (form.file_pdf.value==null || form.file_pdf.value==""){
            document.getElementById("filepdf").innerHTML = "<font color=blue>You must upload pdf file.</font>";
            return false;
        }else {
            if (form.file_pdf.value == "No file chosen"){
                document.getElementById("filepdf").innerHTML = "<font color=blue>You must upload pdf file.</font>";
                return false;
            }
            else {
                if (!validatePDF(form.file_pdf)) {
                    document.getElementById("filepdf").innerHTML = "<font color=blue>Invalid image file type.</font>";
                    return false;
                } else {
                    document.getElementById("filepdf").innerHTML = "<font color=green> </font>";
                }
            }
        }
        return true;
    }

    function validateImage(fld) {
        if(!/(\.bmp|\.gif|\.jpg|\.jpeg|\.png)$/i.test(fld.value)) {
            alert("Invalid image file type.");
            fld.focus();
            return false;
        }
        return true;
    }

    function validatePDF(fld) {
        if(!/(\.pdf)$/i.test(fld.value)) {
            alert("Invalid upload file type.");
            fld.focus();
            return false;
        }
        return true;
    }
    
    function submitAccountInfo(form) {
        if(!validateAccountSearch(form)){
            alert("Fail to submit form");
        }else {
            var accountSearch = form.searchValue.value;
            var filepdf = form.file_pdf.value;
            var postdata = {"acctid": accountSearch, "uploadFile": filepdf};
            $.ajax({
                type:"POST",
                contentType : "application/json",
                url:"/uploadDoc",
                data: JSON.stringify(postdata),
                dataType : 'json',
                success:function (data) {
//                    $("#accounttop").html(data);
                    console.log("Success ==>",data);
//                    resultAccountSearch(data);
                },
                error:function (data) {
                    console.log("Error ==>",data);
                }
            });
            alert("Form submitted successfully");
        }
    }


    function zoom() {
        document.body.style.zoom = "90%"
    }
</script>

</head>
<body class="skin-black sidebar-mini fixed" onload="zoom()">
 Confirm !!

 <div class="wrapper">
     <!-- Main Header -->
     <header class="main-header">

         <!-- Logo -->
         <a href="/homepage" class="logo">
             <!-- mini logo for sidebar mini 50x50 pixels -->

             <span class="logo-mini"><b><img src="/branchconsole/Fidelity-Icon.png" alt="App Logo" style="margin-top: 0px; width:42px; height: 42px"></b></span>
             <!-- logo for regular state and mobile devices -->
             <span class="logo-lg"><img src="/branchconsole/Fidelity-Icon.png" alt="App Logo" style="margin-top: 0px; width:42px; height: 42px">&nbsp; Fidelity CPC</span>

         </a>

         <!-- Header Navbar -->
         <nav class="navbar navbar-static-top" role="navigation">
             <!-- Sidebar toggle button-->
             <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                 <span class="sr-only">Toggle navigation</span>
             </a>
             <div class="navbar-custom-menu" >
                 <ul class="nav navbar-nav"><!-- User Account: style can be found in dropdown.less -->
                     <li class="dropdown user user-menu">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                             <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                             <span class="hidden-xs">User2</span>
                         </a>
                         <ul class="dropdown-menu">
                             <!-- User image -->
                             <li class="user-header">
                                 <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                 <p>
                                     User2 - Data Inputer
                                     <small>Member since Nov. 2012</small>
                                 </p>
                             </li>
                             <!-- Menu Body -->

                             <!-- Menu Footer-->
                             <li class="user-footer">
                                 <div class="pull-left">
                                     <a href="profile.html" class="btn btn-default btn-flat">Profile</a>
                                 </div>
                                 <div class="pull-right">
                                     <a href="login" class="btn btn-default btn-flat">Sign out</a>
                                 </div>
                             </li>
                         </ul>
                     </li>
                 </ul>
             </div>
         </nav>
     </header>

     <!-- Left side column. contains the logo and sidebar -->
     <aside class="main-sidebar">
         <!-- sidebar: style can be found in sidebar.less -->
         <        class="sidebar">
         <!-- Sidebar user panel -->
         <!-- sidebar menu: : style can be found in sidebar.less -->
         <section>
             <ul class="sidebar-menu">
                 <li class="header">ACCOUNT OPERATIONS</li>
                 <b id="csoUser" style="display: none"><c:out  value="${pageContext.request.remoteUser}"></c:out></b>
                 <sec:authorize access="hasRole('REVIEWER') or hasRole('ADMIN')">
                     <li>
                         <a href="review">
                             <i class="fa fa-pencil-square-o"></i> <span>Review</span>
                         </a>
                     </li>
                 </sec:authorize>
                 <sec:authorize access="hasRole('INPUTER') or hasRole('ADMIN')">
                     <li>
                         <a href="update">
                             <i class="fa  fa-check-square-o"></i> <span>Update</span>
                             <span class="pull-right-container">
                          <small class="label pull-right bg-red">3</small>
                        </span>
                         </a>
                     </li>
                 </sec:authorize>
                 <sec:authorize access="hasRole('VERIFIER') or hasRole('ADMIN')">
                     <li>
                         <a href="verify">
                             <i class="fa fa-check-square"></i> <span>Verify</span>
                             <span class="pull-right-container">
                          <small class="label pull-right bg-blue">17</small>
                        </span>
                         </a>
                     </li>
                 </sec:authorize>
                 <%--<sec:authorize access="hasRole('VISITOR')">--%>
                 <li>
                     <a href="report">
                         <i class="fa fa-bar-chart"></i> <span>Reports</span>
                     </a>
                 </li>
                 <%--</sec:authorize>--%>
                 <sec:authorize access="hasRole('ADMIN')">
                     <li>
                         <a href="umr">
                             <i class="fa fa-user-plus"></i> <span>User Management</span>
                         </a>
                     </li>
                 </sec:authorize>
                 <sec:authorize access="hasRole('CSO')  or hasRole('ADMIN')">
                     <li>
                         <a href="docmgmnt">
                             <i class="fa fa-file-pdf-o"></i> <span>CSO Operations</span>
                         </a>
                     </li>
                 </sec:authorize>

             </ul>
         </section>
         <!-- /.sidebar -->
     </aside>

     <!-- Content Wrapper. Contains page content -->
     <!--Calling flash here-->
     <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">


         </section>

         <!-- Main content -->
         <section class="content" style="">
             <!-- Your Page Content Here -->
             <div class="col-sm-12 col-md-12 col-lg-12" style="height: 600px !important; background-color: white;">

                 <!--@search(title,currentLink)-->
                 <form id="search_form" method="POST">

                     <div class="row" style="margin-left: 200px; padding-top: 50px; padding-bottom: 50px;" id="searcher">
                         <div class="col-lg-10" >
                             <div class="input-group">
                                 <input type="text" class="bcon-search-input form-control" name="searchValue" id="searchvalue" placeholder="Search for Account" >
                                 <span class="input-group-btn">
                                    <button onclick="javascript: searchForAccount(this.form);"  type="button" id="search-btn" class="btn btn-success btn-flat"><i class="fa fa-search"></i></button>
                                 </span>

                            </div>
                         </div>
                     </div>
                     <div class="row col-md-8" style='float: left; clear: none; margin-left: 200px;'>

                         <!-- Account Search Result -->
                         <div class="box" style="height:400px; display:none" id="acctInfoBox">


                         </div>


                     </div>
                 </form>


             </div>


         </section>
         <!-- /.content -->
     </div>
     <!-- /.content-wrapper -->


 </div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<link href="<spring:url value="css/app.css" />" rel="stylesheet"
	type="text/css">
<title>Spring Security Example - ProgrammingFree</title>
</head>
<body class="security-app">
<script>
    function signOut(){
        document.getElementById("logout").click();
    }
</script>
	<div class="details">
		<h2>Spring Security - JDBC Authentication</h2>
		<a href="http://www.programming-free.com/2016/01/spring-security-spring-data-jpa.html" class="button green small">Tutorial</a> 
		<a href="https://github.com/priyadb/SpringSecurityJdbcApp/archive/master.zip"
			class="button red small">Download</a>
	</div>
	<div class="lc-block">n
		<h1>Welcome!</h1>
		<div class="alert-normal">
            <form action="/logout" method="post">
                <input type="submit" id="logout" class="button red big" value="Sign Out" /> <input
                    type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
            <sec:authorize access="hasRole('REVIEWER') or hasRole('ADMIN')">
                <script>
                    window.location.href = "review";
                </script>
            </sec:authorize>
            <sec:authorize access="hasRole('INPUTER') or hasRole('ADMIN')">
                <li>
                    <a href="update">
                        <i class="fa  fa-check-square-o"></i> <span>Update</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-red">3</small>
                        </span>
                    </a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasRole('VERIFIER') or hasRole('ADMIN')">
                <li>
                    <a href="verify">
                        <i class="fa fa-check-square"></i> <span>Verify</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-blue">17</small>
                        </span>
                    </a>
                </li>
            </sec:authorize>
            <%--<sec:authorize access="hasRole('VISITOR')">--%>
            <li>
                <a href="report">
                    <i class="fa fa-bar-chart"></i> <span>Reports</span>
                </a>
            </li>
            <%--</sec:authorize>--%>
            <sec:authorize access="hasRole('ADMIN')">
                <li>
                    <a href="umr">
                        <i class="fa fa-user-plus"></i> <span>User Management</span>
                    </a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasRole('CSO')  or hasRole('ADMIN')">
                <li>
                    <a href="docmgmnt">
                        <i class="fa fa-file-pdf-o"></i> <span>CSO Operations</span>
                    </a>
                </li>
            </sec:authorize>
            <b><c:out value="${pageContext.request.remoteUser}"></c:out></b>
			<%--<sec:authentication property="principal.authorities" var="authorities" />--%>
			<%--<c:forEach items="${authorities}" var="authority" varStatus="vs">--%>
				<%--<p>${authority.authority}</p>--%>
			<%--</c:forEach>--%>
		</div>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<html xmlns:th="http://www.thymeleaf.org">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Fidelity CPC</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
<link rel="icon" href="/branchconsole/Fidelity-Icon.png">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
<script src="/angular/angular.min.js"></script>
<script src="/angular/controller.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-app="angularApp" ng-controller="reportCntrl">
<script>
   function altFilterOption(){
        var filterOptn = $("#filterOptn").val();

        if(filterOptn=="STATUS"){
            $(".statusType").show();;
            $(".dateRange").hide();;
        }else if(filterOptn=="DATE"){
            $(".statusType").hide();;
            $(".dateRange").show();;
        }else{
            $(".statusType").hide();;
            $(".dateRange").hide();;
        }
    }

</script>
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b><img src="/branchconsole/icon.png" alt="App Logo" style="margin-top: 0px; width:42px; height: 42px"></b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="/branchconsole/icon.png" alt="App Logo" style="margin-top: 0px; width:42px; height: 42px">&nbsp; <b>Fidelity  </b>CPC</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav"><!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">USER 1</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    User 1 - CSO
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="profile" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="FidelityCPC" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <%--<        class="sidebar">--%>
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <section>
            <ul class="sidebar-menu"><br>
                <li class="header">ACCOUNT OPERATIONS</li>
                <b id="csoUser" style="display: none"><c:out  value="${pageContext.request.remoteUser}"></c:out></b>
                <%--<sec:authorize access="hasRole('REVIEWER') or hasRole('ADMIN')">--%>
                <%--<li>--%>
                    <%--<a href="review">--%>
                        <%--<i class="fa fa-pencil-square-o"></i> <span>Review</span>--%>
                    <%--</a>--%>
                <%--</li>--%>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('INPUTER') or hasRole('ADMIN')">--%>
                <li>
                    <a href="update">
                        <i class="fa fa-pencil-square-o"></i> <span>Update</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-red">3</small>
                        </span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('VERIFIER') or hasRole('ADMIN')">--%>
                <li>
                    <a href="verify">
                        <i class="fa fa-check-square"></i> <span>Verify</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-blue">17</small>
                        </span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--&lt;%&ndash;<sec:authorize access="hasRole('VISITOR')">&ndash;%&gt;--%>
                <li>
                    <a href="report">
                        <i class="fa fa-bar-chart"></i> <span>Reports</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('ADMIN')">--%>
                <li>
                    <a href="umr">
                        <i class="fa fa-user-plus"></i> <span>User Management</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('CSO')  or hasRole('ADMIN')">--%>
                <li>
                    <a href="docmgmnt">
                        <i class="fa fa-file-pdf-o"></i> <span>CSO Operations</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>

            </ul>
        </section>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Document Workflow Report
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row" id="workFlow">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">

                            <%--<div class="" ng-repeat="x in filerOptns">--%>
                                Filter Using:
                                <select id="filterOptn" onChange="altFilterOption();">
                                    <option value="" selected>--SELECT--</option>
                                    <option value="DATE">DATE</option>
                                    <option value="STATUS">STATUS</option>
                                </select>
                                <select id="filterOptnVal" class="statusType" style="display: none">
                                    <option value="">--Select--</option>
                                    <option value="DEFAULT">DEFAULT</option>
                                    <option value="ADVICED">ADVICED</option>
                                    <option value="REJECTED">REJECTED</option>
                                    <option value="UPDATED">UPDATED</option>
                                    <option value="VERIFIED">VERIFIED</option>
                                </select>
                                <label class="dateRange" style="display: none">From </label>
                                <input class="dateRange" type="date" id="fromDate" style="display: none"/>
                                <label class="dateRange" style="display: none"> To</label>
                                <input class="dateRange" type="date" id="toDate" style="display: none"/>
                            <%--</div>--%>

                            <btn class="btn btn-default bt pull-right" ng-click="clrFilterWorkFlow()" >  Clear</btn>
                            <btn class="btn btn-default bt pull-right" ng-click="filterWorkFlow()" >  Filter</btn>
                            <%--<btn class="btn btn-default bt pull-right" ng-click="addFilterWorkFlow()" >Add Filter</btn>--%>
                            <br><br>

                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Account Number</th>
                                    <%--<th>Doc ID</th>--%>
                                    <th>Acct Opn Date</th>
                                    <th>Document Uploader</th>
                                    <th>Upload Date</th>
                                    <th>Sol ID</th>
                                    <th>Branch Name</th>
                                    <th>Inputer</th>
                                    <th>Date Inputted</th>
                                    <th>Document Verifier</th>
                                    <th>Verified Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="x in acctworkFlow">
                                    <td>{{x.acctNumber}}</td>
                                    <%--<td>{{x.docId}}</td>--%>
                                    <td>{{x.acctOpnDate|date:'dd-MM-yyyy'}}</td>
                                    <td>{{x.docmntUploader}}</td>
                                    <td>{{x.docmntUpldDate|date:'dd-MM-yyyy h:mm a'}}</td>
                                    <td>001</td>
                                    <td>Head Office</td>
                                    <td>{{x.docmntInputter}}</td>
                                    <td>{{x.docInputDate|date:'dd-MM-yyyy h:mm a'}}</td>
                                    <td>{{x.acctVerifier}}</td>
                                    <td>{{x.acctVerifyDate|date:'dd-MM-yyyy h:mm a'}}</td>
                                    <td ng-if = "x.acctStatus == 'UPDATED'"><span class="label label-warning">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'VERIFIED'"><span class="label label-success">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'DEFAULT'"><span class="label label-default">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'REJECTED'"><span class="label label-danger">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'ADVICED'"><span class="label label-default">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'LOCKED'"><span class="label label-default">{{x.acctStatus}}</span></td>
                                    <td><a href="#acctUpdateData" ng-click="fetchAcctWorkFlowHist(x.acctNumber)" class="btn btn-primary">View</a></td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row" id="histTable" style="display:none">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <%--<btn class="btn btn-default bt pull-right" ng-click="addFilterWorkFlow()" >Add Filter</btn>--%>
                            <br><br>
                            <table id="" class="table table-bordered table-striped" >
                                <thead>
                                <tr>
                                    <th>S/N</th>
                                    <th>Account Number</th>
                                    <th>Acct Opn Date</th>
                                    <th>Document Name</th>
                                    <%--<th>Checked By</th>--%>
                                    <th>Note</th>
                                    <th>Time Performed</th>
                                    <th>Status reason</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="x in acctworkFlowHist">
                                    <td>{{$index + 1}}</td>
                                    <td>{{x.acctNumber}}</td>
                                    <td>{{x.acctOpnDate|date:'dd-MM-yyyy h:mm a'}}</td>
                                    <td>{{x.docId}}</td>
                                    <%--<td>{{x.docsSubmitted}}</td>--%>
                                    <td>{{x.note}}</td>
                                    <td>{{x.oprtnDate|date:'dd-MM-yyyy h:mm a'}}</td>
                                    <td>{{x.statusReason}}</td>
                                    <%--<td ng-if = "x.operation == 'UPDATED'"><span class="label label-warning">{{x.acctStatus}}</span></td>--%>
                                    <%--<td ng-if = "x.operation == 'VERIFIED'"><span class="label label-success">{{x.acctStatus}}</span></td>--%>
                                    <%--<td ng-if = "x.operation == 'DEFAULT'"><span class="label label-default">{{x.acctStatus}}</span></td>--%>
                                    <%--<td ng-if = "x.operation == 'REJECTED'"><span class="label label-danger">{{x.acctStatus}}</span></td>--%>
                                    <%--<td ng-if = "x.operation == 'ADVICED'"><span class="label label-default">{{x.acctStatus}}</span></td>--%>
                                    <td ><span class="label label-default">{{x.operation}}</span></td>
                                </tr>
                                </tbody>
                            </table><br>
                            <btn class="btn btn-primary" ng-click="navToList()">Back</btn>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1
        </div>
        <strong>Copyright &copy; 2016 <a href="http://longbridgetechnology.com">Longbridge Technology</a>.</strong>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">

        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</body>
</html>

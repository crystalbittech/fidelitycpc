<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<html xmlns:th="http://www.thymeleaf.org">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title> CPC</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
<link rel="icon" href="/branchconsole/blabla.png">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
<script src="/angular/angular.min.js"></script>
<script src="/angular/controller.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-app="angularApp" ng-controller="verifyCntrl">
<script>
    function checkOption(){
        var optionSelected = document.getElementById("rejectOption").value;
        if(optionSelected!=""){
            $("#rejAlert").hide();;
        }else{
            $("#rejAlert").show();;
        }
    }

    function alternatePhnEmailDiv(){
        var phnEmailOpt = document.getElementById("phnEmailOpt").value;

        if(phnEmailOpt=="Phone"){
            $(".emailDiv").hide();;
            $(".phoneDiv").show();;
        }else{
            $(".emailDiv").show();;
            $(".phoneDiv").hide();;
        }
    }



</script>
<div class="wrapper">

	<header class="main-header">
		<!-- Logo -->
		<a href="../../index2.html" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b><img src="/branchconsole/icon.png" alt="CPC" style="margin-top: 0px; width:42px; height: 42px"></b></span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><img src="/branchconsole/icon.png" style="margin-top: 0px; width:42px; height: 42px">&nbsp; <b>Fidelity  </b>CPC</span>
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>

			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav"><!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
							<span id="loginUser" class="hidden-xs">USER2</span>
						</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header">
								<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

								<p>
									User 1 - CSO
									<small>Member since Nov. 2012</small>
								</p>
							</li>
							<!-- Menu Body -->

							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="profile" class="btn btn-default btn-flat">Profile</a>
								</div>
								<div class="pull-right">
									<a href="FidelityCPC" class="btn btn-default btn-flat">Sign out</a>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<%--<        class="sidebar">--%>
		<!-- Sidebar user panel -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<section>
			<ul class="sidebar-menu"><br>
				<li class="header">ACCOUNT OPERATIONS</li>
				<b id="csoUser" style="display: none"><c:out  value="${pageContext.request.remoteUser}"></c:out></b>
				<%--<sec:authorize access="hasRole('REVIEWER') or hasRole('ADMIN')">--%>
				<%--<li>--%>
				<%--<a href="review">--%>
				<%--<i class="fa fa-pencil-square-o"></i> <span>Review</span>--%>
				<%--</a>--%>
				<%--</li>--%>
				<%--</sec:authorize>--%>
				<%--<sec:authorize access="hasRole('INPUTER') or hasRole('ADMIN')">--%>
				<li class="treeview menu-open">
					<a href="update">
						<i class="fa fa-pencil-square-o"></i> <span>Update</span>
						<span class="pull-right-container">
                          <small class="label pull-right bg-red">3</small>
                        </span>
					</a>
				</li>
				<%--</sec:authorize>--%>
				<%--<sec:authorize access="hasRole('VERIFIER') or hasRole('ADMIN')">--%>
				<li class="treeview menu-open">
					<a href="verify" >
						<i class="fa fa-check-square"></i> <span>Verify</span>
						<span class="pull-right-container">
                          <small class="label pull-right bg-blue">17</small>
                        </span>
					</a>
					<ul class="treeview-menu" style="display: block;">
						<li><a href="" ng-click="showAttDocs()"><i class="fa fa-circle-o"></i>CSO CheckList</a></li>
						<li><a href="" ng-click="showCRM()"><i class="fa fa-circle-o"></i>CRM</a></li>
						<li><a href="" ng-click="showHACM()"><i class="fa fa-circle-o"></i>HACM</a></li>
					</ul>
				</li>
				<%--</sec:authorize>--%>
				<%--&lt;%&ndash;<sec:authorize access="hasRole('VISITOR')">&ndash;%&gt;--%>
				<li>
					<a href="report" >
						<i class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
				</li>
				<%--</sec:authorize>--%>
				<%--<sec:authorize access="hasRole('ADMIN')">--%>
				<li>
					<a href="umr" >
						<i class="fa fa-user-plus"></i> <span>User Management</span>
					</a>
				</li>
				<%--</sec:authorize>--%>
				<%--<sec:authorize access="hasRole('CSO')  or hasRole('ADMIN')">--%>
				<li>
					<a href="docmgmnt" >
						<i class="fa fa-file-pdf-o"></i> <span>CSO Operations</span>
					</a>
				</li>
				<%--</sec:authorize>--%>

			</ul>
		</section>
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Account Update
			</h1>
		</section>

		<!-- Main content -->
		<section class="content" id="acctUpdateList">
			<div class="row">
				<div class="col-xs-12">

					<!-- /.box -->

					<div class="box">
						<!-- /.box-header -->
						<div class="box-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
								<tr>
									<th>Account Number</th>
									<th>Account Open Date</th>
									<%--<th>Document Name</th>--%>
									<th>Uploaded By</th>
									<th>Date Uploaded</th>
									<th>Sol ID</th>
									<th>Sol Name</th>
									<th>Verifier</th>
									<th>Date Submitted</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								<tr ng-repeat="x in workFlowforUpdater">
									<td>{{x.acctNumber}}</td>
									<td>{{x.acctOpnDate|date:'dd-MM-yyyy'}}</td>
									<%--<td>{{x.docId}}</td>--%>
									<td>{{x.docmntUploader}}</td>
									<td>{{x.docmntUpldDate|date:'dd-MM-yyyy h:mm a'}}</td>
									<td>099</td>
									<td>Ajah</td>
									<td>{{x.acctVerifier}}</td>
									<td>{{x.acctVerifyDate|date:'dd-MM-yyyy h:mm a'}}</td>
									<td ng-if = "x.acctStatus == 'UPDATED'"><span class="label label-warning">{{x.acctStatus}}</span></td>
									<td ng-if = "x.acctStatus == 'LOCKED'"><span class="label label-default">{{x.acctStatus}}</span></td>
									<td ng-if = "x.acctStatus == 'UPDATED'"><a href="#acctReview" ng-click="fetchAcctDetails(x)" class="btn btn-warning">View</a></td>
									<td ng-if = "x.acctStatus == 'LOCKED'"><a href="#acctReview" onclick="alert('Presently Being worked on')" class="btn btn-primary" disabled>View</a></td>

								</tr>
								</tbody>
							</table>
						</div>

						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
		<section class="content" id="acctUpdateData" style="display:none" >
			<div class="row">
				<%--{{selectedWorkFlow|json}}--%>
				<div class="col-xs-12 col-md-6">
					<div class="box">
						<div class="box-body">
							<div class="nav-tabs-custom" id="retAcctDiv">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab_5" data-toggle="tab">Basic Data</a></li>
									<li><a href="#tab_6" data-toggle="tab">Identification</a></li>
									<li><a href="#tab_7" data-toggle="tab">Contact</a></li>
									<li><a href="#tab_8" data-toggle="tab">Currency</a></li>
									<li><a href="#tab_9" data-toggle="tab">Demographic</a></li>
									<li><a href="#tab_10" data-toggle="tab">Trade Finance</a></li>
								</ul>
								<div class="tab-content" >
									<div class="tab-pane active" id="tab_5" >
										<div class="box" >
											<div class="box-body" >
												<div class="direct-chat-messages" style="height:285px">
													<div class="form-group">
														<label  class="col-sm-3 control-label">Account Number</label>
														<div class="col-sm-9">
															<input type="text" class="form-control" value="{{acctNumber.acctId }}" readOnly>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">First Name</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.firstName}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.firstName" >
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Last Name</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.lastName}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.lastName" placeholder="Last Name">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Middle Name</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.middleName}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.middleName" placeholder="Middle Name">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">DOB</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.birthDt}}" placeholder="Date of Birth">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Title</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.salutationCode}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.salutationCode" placeholder="Title">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Gender</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.gender" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.gender" ng-options="option for option in appOptions.GenderOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">BVN</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.custBvn}}" ng-model="selectedWorkFlow.retDetails.custBvn" placeholder="BVN">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Segment</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.subSegment}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.subSegment" placeholder="Segment">
															<%--<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.subSegment" ng-model="selectedWorkFlow.retDetails.acctSegment" ng-options="option for option in appOptions.SegmentOptns">--%>
															<%--</select>--%>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Sub-segment</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.subSegment}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.subSegment" placeholder="Sub Segment">
															<%--<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.subSegment" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.subSegment" ng-options="option for option in appOptions.SubSegmentOptns">--%>
															<%--</select>--%>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Availed Trade</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.availedTrade" ng-model="selectedWorkFlow.retDetails.availedTrade" ng-options="option for option in appOptions.availedTradeOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Region</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.region}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.region" placeholder="Region">
															<%--<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.region" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.region" ng-options="option for option in appOptions.acctRegionOptns">--%>
															<%--</select>--%>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Preferred Local</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.prefLocal" ng-model="selectedWorkFlow.retDetails.prefLocal" ng-options="option for option in appOptions.prefLocalOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Enable CRM Alert</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.isSMSBankingEnabled" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.isSMSBankingEnabled" ng-options="option for option in appOptions.dualOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Enable E-Banking</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.isEBankingEnabled" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.isEBankingEnabled" ng-options="option for option in appOptions.dualOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Primary Relation ID</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.manager}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.manager" placeholder="Primary Relation ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Secondary Relation ID</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.secondaryRelatinId}}" ng-model="selectedWorkFlow.retDetails.secondaryRelatinId" placeholder="Secondary Relation ID">
														</div><br>
													</div><br>
												</div><br>
												<!--<div class="box-footer" style="display:block">-->
												<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
												<!--</div>-->
											</div>
											<!-- /.box-body -->
										</div>
									</div>
									<!-- /.tab-pane -->
									<div class="tab-pane" id="tab_6">
										<div class="box" >
											<div class="box-body">
												<div class="direct-chat-messages" style="height:285px">
													<div class="form-group">
														<label  class="col-sm-3 control-label">Document Type</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docTypeCode" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docTypeCode}}" placeholder="Document TypeCode">
															<%--<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docTypeCode" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docTypeCode" ng-options="option for option in appOptions.docTypeOptns">--%>
															<%--</select>--%>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Document Code</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docCode" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docCode}}" placeholder="Document Code">
															<%--<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docCode" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docCode" ng-options="option for option in appOptions.docCodeOptns" >--%>
															<%--</select>--%>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Unique ID</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.refNum" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.refNum}}" placeholder="Unique ID t1">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Place of Issue</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.placeOfIssue" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.placeOfIssue}}" placeholder="Place of Issue">
															<%--<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.placeOfIssue" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.placeOfIssue" ng-options="option for option in appOptions.acctRegionOptns" >--%>
															<%--</select>--%>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Country of Issue</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.countryOfIssue" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.countryOfIssue" ng-options="option for option in appOptions.cntryOptns" >
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Issue date</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docIssueDt|date}}" placeholder="Issue date">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Expiry date</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.entityDocDtls.docExpDt|date}}" placeholder="Expiry date">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Is Preferred</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.isPreferred" ng-model="selectedWorkFlow.fidDocument.isPreferred" ng-options="option for option in appOptions.dualOptns" >
															</select>
														</div><br>
													</div><br>
												</div><br>
												<!--<div class="box-footer" style="display:block">-->
												<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
												<!--</div>-->
											</div>
											<!-- /.box-body -->
										</div>
									</div>
									<!-- /.tab-pane -->
									<div class="tab-pane" id="tab_7">
										<div class="box" >
											<div class="box-body">
												<div class="direct-chat-messages" style="height:230px">
													<div class="box" >
														<div class="box-header with-border">
															<h3 class="box-title">Address</h3>

															<div class="box-tools pull-right">
																<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																	<i class="fa fa-minus"></i></button>
															</div>
														</div>
														<div class="box-body">
															<div class="" ng-repeat=" x in selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.retCustAddrInfo" ng-if="x.prefAddr=='Y'">

																<div class="form-group">
																	<label  class="col-sm-3 control-label">Address Format</label>
																	<div class="col-sm-9 ">
																		<select class="form-control" ng-init="x.prefFormat" ng-model="x.prefFormat" ng-options="option for option in appOptions.addressFormatOptns" >
																		</select>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Address Type</label>
																	<div class="col-sm-9 ">
																		<select class="form-control"  class="form-control" ng-init="x.addrCategory" ng-model="x.addrCategory" ng-options="option for option in appOptions.addressTypeOptns" >
																		</select>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Address Label</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control"  ng-model="x.addressLabel" value="{{x.addressLabel}}" placeholder="Address label">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Address Line 1</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" ng-model="x.addrLine1" value="{{x.addrLine1}}" placeholder="Address Line">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Local Govt.</label>
																	<div class="col-sm-9">
																		<select class="form-control" ng-init="x.town" ng-model="x.town" ng-options="option for option in appOptions.acctRegionOptns" >
																		</select>
																	</div>
																	<%--<div class="col-sm-2 ">--%>
																	<%--<span class="input-group-btn">--%>
																	<%--<button type="submit" ng-click="searchforAcct();" name="search" id="search-btn" class="btn btn-flat bg-green">--%>
																	<%--<i class="fa fa-search"></i>--%>
																	<%--</button>--%>
																	<%--</span>--%>
																	<%--</div>--%>
																	<br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">State</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{x.state}}" ng-model="x.state" placeholder="State">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Country</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{x.country}}" ng-model="x.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">City</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{x.city}}" ng-model="x.city" placeholder="City">
																	</div><br>
																</div><br>
															</div>
															<br>
														</div>
														<!-- /.box-body -->
													</div>

													<div class="box" >
														<div class="box" >
															<div class="box-header with-border">
																<h3 class="box-title">Phone and Email</h3>

																<div class="box-tools pull-right">
																	<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																		<i class="fa fa-minus"></i></button>
																</div>
															</div>
															<div class="box-body">
																<div class="" >
																	<div ng-repeat=" x in selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.phoneEmailInfo" ng-if="x.prefFlag=='Y'">
																		<div ng-if="x.phoneOrEmail=='PHONE'">
																			<label  class="control-label" >Preferred Contact Type</label>
																			<select class="form-control"  ng-init="x.phoneEmailType" ng-model="x.phoneEmailType" ng-options="option for option in appOptions.prefContactType">
																			</select>
																			<br>
																		</div>
																		<div ng-if="x.phoneOrEmail=='EMAIL'">
																			<label  class="control-label" >Preferred Mail Type</label>
																			<select class="form-control"  ng-init="x.phoneEmailType" ng-model="x.phoneEmailType" ng-options="option for option in appOptions.prefMailType">
																			</select>
																			<br>
																		</div>
																	</div>

																	<%--<label  class="control-label">Preferred Mobile Alert Type</label>--%>
																	<%--<select class="form-control" ng-init="selectedWorkFlow.contact.fidPrefPhone_email.prefMobAlertType" ng-model="selectedWorkFlow.contact.fidPrefPhone_email.prefMobAlertType" ng-options="option for option in appOptions.prefMobAlertType">--%>
																	<%--</select><br>--%>
																	<div ng-repeat="x in selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.phoneEmailInfo" >
																		<label ng-if="x.prefFlag=='Y'" class="control-label label-success">Phone or Email {{$index +1}}</label>
																		<label ng-if="x.prefFlag!='Y'" class="control-label">Phone or Email {{$index +1}}</label>
																		<select class="form-control" ng-init="x.phoneOrEmail" ng-model="x.phoneOrEmail" ng-options="option for option in appOptions.phoneEmailOptns">
																		</select><br>
																		<div ng-if="x.phoneOrEmail == 'PHONE'">
																			<label  class="control-label">Type</label>
																			<select class="form-control" ng-init="x.phoneEmailType" ng-model="x.phoneEmailType" ng-options="option for option in appOptions.prefMobAlertType">
																			</select><br>
																			<label  class="control-label">Phone Number</label>
																			<div class="">
																				<input type="text" class="form-control"  value="{{x.phoneNum}}" ng-model="x.phoneNum" placeholder="Phone Number">
																			</div><br>
																			<select class="form-control" ng-init="x.prefFlag" ng-model="x.prefFlag" ng-options="option for option in appOptions.dualOptns">
																			</select><br>
																		</div>
																		<div ng-if="x.phoneOrEmail == 'EMAIL'">
																			<label  class="control-label">Type</label>
																			<select class="form-control" ng-init="x.phoneEmailType" ng-model="x.phoneEmailType" ng-options="option for option in appOptions.prefMailType">
																			</select><br>
																			<label  class="control-label">Email ID</label>
																			<div class="">
																				<input type="text" class="form-control"  value="{{x.phoneEmailInfo}}" ng-model="x.phoneEmailInfo" placeholder="Email ID">
																			</div><br>
																			<select class="form-control" ng-init="x.prefFlag" ng-model="x.prefFlag" ng-options="option for option in appOptions.dualOptns">
																			</select><br>
																		</div>
																	</div>
																</div><br>

															</div>

															<!-- /.box-body -->
														</div>

														<!-- /.box-body -->
													</div>

												</div><br>
												<div class="box-footer">
													<button type="submit" ng-click="addPhoneEmail()" class="btn btn-warning pull-right" >Add Phone_Email</button>
												</div>
											</div>
											<!-- /.box-body -->
										</div>
									</div>

									<div class="tab-pane" id="tab_8">
										<div class="box" >
											<div class="box-body">
												<div class="direct-chat-messages" style="height:230px">
													<div class="box" ng-repeat="x in selectedWorkFlow.fidCrncy">
														<div class="box-header with-border">
															<h3 class="box-title">Currency {{$index+1}}</h3>

															<div class="box-tools pull-right">
																<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																	<i class="fa fa-minus"></i></button>
															</div>
														</div>
														<div class="box-body">
															<div class="form-group">
																<label  class="col-sm-3 control-label">Currency</label>
																<div class="col-sm-9 ">
																	<select class="form-control" ng-init="x.crncy" ng-model="x.crncy" ng-options="option for option in appOptions.crncyOptns">
																	</select>
																</div><br>
															</div><br>
															<div class="form-group">
																<label  class="col-sm-3 control-label">Witholding Tax Pcnt</label>
																<div class="col-sm-9 ">
																	<input type="text" ng-model="x.withTax" value="{{x.withTax}}" class="form-control"  placeholder="Witholding Tax Pcnt">
																</div><br>
															</div><br>
														</div>
														<!-- /.box-body -->
													</div>

												</div><br>
												<div class="box-footer">
													<button type="submit" ng-click="addCurrency();" class="btn btn-warning pull-right" >Add Currency</button>
												</div>
											</div>
											<!-- /.box-body -->
										</div>
									</div>

									<div class="tab-pane" id="tab_9">
										<div class="box" >
											<div class="box-header with-border">
												<div class="box-body">
													<div class="direct-chat-messages" style="height:210px">
														<div class="form-group">
															<label  class="col-sm-3 control-label">Nationality</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.nationality}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.nationality" placeholder="Nationality">
																<%--<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.nationality" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.nationality" ng-options="option for option in appOptions.natinltOptns" >--%>
																<%--</select>--%>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Residence Country</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.residenceCountry}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.residenceCountry" placeholder="Residence Country">
																<%--<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.residenceCountry" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.residenceCountry" ng-options="option for option in appOptions.resdnCntryOptns" >--%>
																<%--</select>--%>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Marital Status</label>
															<div class="col-sm-9 ">
																<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.maritalStatus" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.maritalStatus" ng-options="option for option in appOptions.maritalStat" >
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Employment Type</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.employmentStatus}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.employmentStatus" placeholder="Employment Status">
																<%--<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.employmentStatus" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.employmentStatus" ng-options="option for option in appOptions.emploTypeOptns" >--%>
																<%--</select>--%>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Gross Income</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.taxRateTableCode}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.demographicDtls.taxRateTableCode" placeholder="Gross Income">
															</div><br>
														</div><br>
														<div class="box" ng-repeat="x in selectedWorkFlow.demographic.fidEmploymentHist">
															<div class="box-header with-border">
																<h3 class="box-title">Employment History {{$index+1}}</h3>
																<div class="box-tools pull-right">
																	<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																		<i class="fa fa-minus"></i></button>
																</div>
															</div>
															<div class="box-body">
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Employment Type</label>
																	<div class="col-sm-9 ">
																		<select class="form-control" ng-init="x.employType" ng-model="x.employType" ng-options="option for option in appOptions.emploTypeOptns" >
																		</select>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Occupation</label>
																	<div class="col-sm-9 ">
																		<select class="form-control" ng-init="x.occupation" ng-model="x.occupation" ng-options="option for option in appOptions.occpTypeOptns" >
																		</select>
																	</div><br>
																</div><br>
															</div>
															<!-- /.box-body -->
														</div>

													</div><br>
													<div class="box-footer">
														<button type="submit" ng-click="addEmployHist();" class="btn btn-warning pull-right" >Add Employment Details</button>
													</div>
												</div>
												<!-- /.box-body -->
											</div>
										</div>
									</div>

									<div class="tab-pane" id="tab_10">
										<div class="box" >
											<div class="box-body">
												<div class="direct-chat-messages" style="height:305px">
													<div class="form-group">
														<label  class="col-sm-3 control-label">Inland Trade Allowed</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.tradeFinDtls.inlandTradeAllowed" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.tradeFinDtls.inlandTradeAllowed" ng-options="option for option in appOptions.dualOptns" >
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Customer Native</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.retCcustInqResponse.retCustInqRs.tradeFinDtls.custNative" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.tradeFinDtls.custNative" ng-options="option for option in appOptions.dualOptns" >
															</select>
														</div><br>
													</div><br>


												</div>
												<!-- /.box-body -->
											</div>
										</div>
									</div>
									<%--<button type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'UPDATED');" class="btn btn-primary">Update</button>--%>
									<%--<button type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'REJECTED')" class="btn btn-danger" >Reject</button>--%>
									<%--<a href="#" style="padding-left: 2%"><i class="fa fa-clipboard">Add Note</i></a>--%>
									<%--<button type="submit" ng-click="navToUpdateList()" class="btn btn-default pull-right" >Cancel</button>--%>

									<!-- /.tab-pane -->
								</div>
							</div>

							<div class="nav-tabs-custom" id="corpAcctDiv">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab_1" data-toggle="tab">Basic Data</a></li>
									<li><a href="#tab_2" data-toggle="tab">Identification</a></li>
									<li><a href="#tab_3" data-toggle="tab">Contact</a></li>
									<li><a href="#tab_4" data-toggle="tab">Currency</a></li>

								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_1" >
										<div class="box" >
											<div class="box-body" >
												<div class="direct-chat-messages" style="height:350px">
													<div class="form-group">
														<label  class="col-sm-3 control-label">Account Number</label>
														<%--{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.lastName}}--%>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="acctNumber.acctId" value="{{acctNumber.acctId }}" placeholder="Account Number" readOnly>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Account Name</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.corporateName" value="{{selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.corporateName}}" placeholder="Account Name">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Legal Entity Type</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.legalentityType" ng-model="selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.legalentityType" ng-options="option for option in appOptions.legalEntityTpeOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Key Contact Person</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.keycontactPersonname" value="{{selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.keycontactPersonname}}" placeholder="Key Contact Person" >
														</div><br>
													</div><br>
													<div class="form-group">

														<label  class="col-sm-3 control-label">Phone Number</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.phoneEmailData[0].phonenolocalcode" value="{{selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.phoneEmailData[0].phonenolocalcode}}" placeholder="Phone Number" >
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Segment</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.corpDetails.acctSegment" ng-model="selectedWorkFlow.corpDetails.acctSegment" ng-options="option for option in appOptions.SegmentOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Sub-segment</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.corpDetails.acctSubsegment" ng-model="selectedWorkFlow.corpDetails.acctSubsegment" ng-options="option for option in appOptions.SubSegmentOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Business Type</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.businessGroup" ng-model="selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.businessGroup" ng-options="option for option in appOptions.bizTypeOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Country of Incorporation</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.corpDetails.cntryofIncorporation" value="{{selectedWorkFlow.corpDetails.cntryofIncorporation}}" placeholder="Country of Incorporation" >
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Place of Operation</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.corpDetails.placeofOperation" value="{{selectedWorkFlow.corpDetails.placeofOperation}}" placeholder="Place of Operation" >
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">RC Number</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.corpDetails.registerationNum" value="{{selectedWorkFlow.corpDetails.registerationNum}}" placeholder="Registeration Number" >
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Incorporation Date</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.corpCustInqResp.corporateCustomerDetails.corpDet.dateOfIncorporation | date }}" placeholder="Incorporation Date">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Region</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.corpDetails.acctRegion" ng-model="selectedWorkFlow.corpDetails.acctRegion" ng-options="option for option in appOptions.acctRegionOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Relationship Type</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.corpDetails.relatnType" ng-model="selectedWorkFlow.corpDetails.relatnType" ng-options="option for option in appOptions.relatntypeOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">BVN</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.corpDetails.bvn" value="{{selectedWorkFlow.corpDetails.bvn}}" placeholder="BVN">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Native Language</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.corpDetails.nativeLang" ng-model="selectedWorkFlow.corpDetails.nativeLang" ng-options="option for option in appOptions.nativeLangOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Availed Trade</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.corpDetails.availedTrade" ng-model="selectedWorkFlow.corpDetails.availedTrade" ng-options="option for option in appOptions.availedTradeOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Primary Relation ID</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.corpDetails.primaryRelatinId" value="{{selectedWorkFlow.corpDetails.primaryRelatinId}}" placeholder="Primary Relation ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Secondary Relation ID</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.corpDetails.secondaryRelatinId" value="{{selectedWorkFlow.corpDetails.secondaryRelatinId}}" placeholder="Secondary Relation ID">
														</div><br>
													</div><br>
												</div><br>
												<!--<div class="box-footer" style="display:block">-->
												<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
												<!--</div>-->
											</div>
											<!-- /.box-body -->
										</div>
									</div>
									<!-- /.tab-pane -->
									<div class="tab-pane" id="tab_2">
										<div class="box" >
											<div class="box-header with-border">
												<h3 class="box-title">Identification Details</h3>
											</div>
											<div class="box-body">
												<div class="direct-chat-messages" style="height:315px">

													<div class="form-group">
														<label  class="col-sm-3 control-label">Document Type</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.docType" ng-model="selectedWorkFlow.fidDocument.docType" ng-options="option for option in appOptions.docTypeOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Document Code</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.docCode" ng-model="selectedWorkFlow.fidDocument.docCode" ng-options="option for option in appOptions.docCodeOptns" >
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Unique ID</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.fidDocument.docId" value="{{selectedWorkFlow.fidDocument.docId}}" placeholder="Unique ID t1">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Place of Issue</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.placeOfIssue" ng-model="selectedWorkFlow.fidDocument.placeOfIssue" ng-options="option for option in appOptions.acctRegionOptns" >
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Country of Issue</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.cntryOfIssue" ng-model="selectedWorkFlow.fidDocument.cntryOfIssue" ng-options="option for option in appOptions.cntryOptns" >
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Issue date</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Issue date">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Expiry date</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.expiryDate|date}}" placeholder="Expiry date">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Is Preferred</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.isPreferred" ng-model="selectedWorkFlow.fidDocument.isPreferred" ng-options="option for option in appOptions.dualOptns" >
															</select>
														</div><br>
													</div><br>
												</div><br>
												<!--<div class="box-footer" style="display:block">-->
												<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
												<!--</div>-->
											</div>
											<!-- /.box-body -->
										</div>
									</div>
									<!-- /.tab-pane -->
									<div class="tab-pane" id="tab_3">
										<div class="box" >
											<div class="box-header with-border">
												<h3 class="box-title">Contact</h3>
											</div>
											<div class="box-body">

												<div class="direct-chat-messages" style="height:297px">
													<div class="box" >
														<div class="box-header with-border">
															<h3 class="box-title">Address</h3>

															<div class="box-tools pull-right">
																<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																	<i class="fa fa-minus"></i></button>
															</div>
														</div>
														<div class="box-body">
															<div class="">

																<div class="form-group">
																	<label  class="col-sm-3 control-label">Address Format</label>
																	<div class="col-sm-9 ">
																		<select class="form-control" >s
																			<option selected>Free-Text</option>
																			<option>Mail</option>
																		</select>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Address Type</label>
																	<div class="col-sm-9 ">
																		<select class="form-control" >s
																			<option selected>Mailing</option>
																			<option>PO Box</option>
																		</select>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Address Label</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control"  placeholder="Address label">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Address Line 1</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control"  placeholder="Address Line">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Local Govt.</label>
																	<div class="col-sm-7">
																		<select class="form-control" >s
																			<option selected>Male</option>
																			<option>Female</option>
																		</select>
																	</div>
																	<div class="col-sm-2 ">
                                                                <span class="input-group-btn">
                                                                    <button type="submit" ng-click="searchforAcct();" name="search" id="search-btn2" class="btn btn-flat bg-green">
                                                                    <i class="fa fa-search"></i>
                                                                    </button>
                                                                </span>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">State</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control"  placeholder="State">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Country</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control"  placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">City</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control"  placeholder="City">
																	</div><br>
																</div><br>
															</div>
															<br>
															<!--<div class="box-footer" style="display:block">-->
															<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
															<!--</div>-->
														</div>
														<!-- /.box-body -->
													</div>

													<div class="box" >
														<div class="box-header with-border">
															<h3 class="box-title">Phone and Email</h3>

															<div class="box-tools pull-right">
																<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																	<i class="fa fa-minus"></i></button>
															</div>
														</div>
														<div class="box-body">
															<div class="">
																<label  class="control-label">Preferred Contact Type</label>
																<select class="form-control" ng-init="selectedWorkFlow.contact.fidPrefPhone_email.prefContactType" ng-model="acctDetails.contact.fidPrefPhone_email.prefContactType" ng-options="option for option in appOptions.prefContactType">
																</select><br>
																<label  class="control-label">Preferred Mail Type</label>
																<select class="form-control" ng-init="selectedWorkFlow.contact.fidPrefPhone_email.prefMailType" ng-model="acctDetails.contact.fidPrefPhone_email.prefMailType" ng-options="option for option in appOptions.prefMailType">
																</select><br>
																<label  class="control-label">Preferred Mobile Alert Type</label>
																<select class="form-control" ng-init="selectedWorkFlow.contact.fidPrefPhone_email.prefMobAlertType" ng-model="acctDetails.contact.fidPrefPhone_email.prefMobAlertType" ng-options="option for option in appOptions.prefMobAlertType">
																</select><br>
																<div ng-repeat="x in selectedWorkFlow.contact.fidPhoneEmails">
																	<label  class="control-label">Phone or Email {{$index +1}}</label>
																	<select class="form-control" ng-init="x.contactBy" ng-model="x.contactBy" ng-options="option for option in appOptions.phoneEmailOptns">
																	</select><br>
																	<div ng-if="x.contactBy == 'PHONE'">
																		<label  class="control-label">Type</label>
																		<select class="form-control" ng-init="x.contactType" ng-model="x.contactType" ng-options="option for option in appOptions.prefMobAlertType">
																		</select><br>
																		<label  class="control-label">Phone Number</label>
																		<div class="">
																			<input type="text" class="form-control"  value="{{x.contactId}}" ng-model="x.contactId" placeholder="Phone Number">
																		</div><br>
																	</div>
																	<div ng-if="x.contactBy == 'EMAIL'">
																		<label  class="control-label">Type</label>
																		<select class="form-control" ng-init="x.contactType" ng-model="x.contactType" ng-options="option for option in appOptions.prefMailType">
																		</select><br>
																		<label  class="control-label">Email ID</label>
																		<div class="">
																			<input type="text" class="form-control"  value="{{x.contactId}}" ng-model="x.contactId" placeholder="Email ID">
																		</div><br>
																	</div>
																</div>
															</div><br>

														</div>

														<!-- /.box-body -->
													</div>

												</div><br>
												<div class="box-footer">
													<button type="submit" ng-click="addPhoneEmail()" class="btn btn-warning pull-right" >Add Phone_Email</button>
												</div>
												<!--<div class="box-footer" style="display:none">-->
												<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
												<!--</div>-->

											</div>
											<!-- /.box-body -->
										</div>
									</div>

									<div class="tab-pane" id="tab_4">
										<div class="box" >
											<div class="box-header with-border">
												<h3 class="box-title">Currency</h3>
											</div>
											<div class="box-body">
												<div class="direct-chat-messages" style="height:297px">
													<div class="box" ng-repeat="x in selectedWorkFlow.fidCrncy">
														<div class="box-header with-border">
															<h3 class="box-title">Currency {{$index+1}}</h3>

															<div class="box-tools pull-right">
																<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																	<i class="fa fa-minus"></i></button>
															</div>
														</div>
														<div class="box-body">
															<div class="form-group">
																<label  class="col-sm-3 control-label">Currency</label>
																<div class="col-sm-9 ">
																	<select class="form-control" ng-init="x.crncy" ng-model="x.crncy" ng-options="option for option in appOptions.crncyOptns">
																	</select>
																</div><br>
															</div><br>
															<div class="form-group">
																<label  class="col-sm-3 control-label">Witholding Tax Pcnt</label>
																<div class="col-sm-9 ">
																	<input type="text" class="form-control" ng-model="x.withTax"  value="{{x.withTax}}" placeholder="Witholding Tax Pcnt">
																</div><br>
															</div><br>
														</div>
														<!-- /.box-body -->
													</div>

												</div><br>
												<div class="box-footer">
													<button type="submit" ng-click="addCurrency();" class="btn btn-warning pull-right" >Add Currency</button>
												</div>
											</div>
											<!-- /.box-body -->
										</div>
									</div>
									<!-- /.tab-pane -->
								</div>
							</div>

							<div class="nav-tabs-custom" id="Hacm" >
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab_11" data-toggle="tab">General</a></li>
									<li><a href="#tab_12" data-toggle="tab">Directoer Related Exposure</a></li>
									<li><a href="#tab_13" data-toggle="tab">MIS Code</a></li>
									<li><a href="#tab_14" data-toggle="tab">Document Details</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_11" >
										<div class="box" >
											<div class="box-body" >
												<div class="direct-chat-messages" style="height:330px">
													<div class="form-group">
														<label  class="col-sm-4 control-label">A/C Short name</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCustAcctResponse.retCustAcctInqRs.retSaleDtls.saleChannelDtls.acctShortName}}" ng-model="selectedWorkFlow.retCustAcctResponse.retCustAcctInqRs.retSaleDtls.saleChannelDtls.acctShortName" readOnly>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">A/C Opening Date</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCustAcctResponse.retCustAcctInqRs.retSaleDtls.saleChannelDtls.acctOpeningDt}}" ng-model="selectedWorkFlow.retCustAcctResponse.retCustAcctInqRs.retSaleDtls.saleChannelDtls.acctOpeningDt" >
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Charge Level Code</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.chargeLevelCode}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.chargeLevelCode" placeholder="Charge Level Code">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Mode of Operation</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCustAcctResponse.retCustAcctInqRs.retSaleDtls.saleChannelDtls.modeOfOperation}}" ng-model="selectedWorkFlow.retCustAcctResponse.retCustAcctInqRs.retSaleDtls.saleChannelDtls.modeOfOperation" placeholder="Mode of Operation">
														</div><br>
													</div><br>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Location Code</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.custDob|date}}" placeholder="Date of Birth">--%>
													<%--</div><br>--%>
													<%--</div><br>--%>
													<div class="form-group">
														<label  class="col-sm-4 control-label">A/C Report Code</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.title}}" ng-model="acctDetails.retDetails.title" placeholder="Title">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Channel Level Code</label>
														<div class="col-sm-8 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.gender" ng-model="acctDetails.retDetails.gender" ng-options="option for option in appOptions.GenderOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Customer Relationship Code</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.relationshipType}}" ng-model="selectedWorkFlow.retCcustInqResponse.retCustInqRs.retCustDtls.relationshipType" placeholder="Relationship Code">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">A/C Manager ID</label>
														<div class="col-sm-8 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.acctSegment" ng-model="acctDetails.retDetails.acctSegment" ng-options="option for option in appOptions.SegmentOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Ledger No.</label>
														<div class="col-sm-8 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.acctSubsegment" ng-model="acctDetails.retDetails.acctSubsegment" ng-options="option for option in appOptions.SubSegmentOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Contact Phone No.</label>
														<div class="col-sm-8 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.availedTrade" ng-model="acctDetails.retDetails.availedTrade" ng-options="option for option in appOptions.availedTradeOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Cash Debit Limit Exception</label>
														<div class="col-sm-8 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.acctRegion" ng-model="acctDetails.retDetails.acctRegion" ng-options="option for option in appOptions.acctRegionOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Cash Credit Limit exception</label>
														<div class="col-sm-8 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.prefLocal" ng-model="acctDetails.retDetails.prefLocal" ng-options="option for option in appOptions.prefLocalOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Closure Exp limit</label>
														<div class="col-sm-8 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.enableCrm" ng-model="acctDetails.retDetails.enableCrm" ng-options="option for option in appOptions.dualOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Clearing exception Limit</label>
														<div class="col-sm-8 ">
															<select class="form-control" ng-init="selectedWorkFlow.retDetails.enableEbanking" ng-model="acctDetails.retDetails.enableEbanking" ng-options="option for option in appOptions.dualOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Transfer Exeption limit(Dr.)</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Transfer Exeption limit(Cr.)</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Pref Language Code</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Name in Pref Language</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-4 control-label">Pref Language Code</label>
														<div class="col-sm-8 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">
														</div><br>
													</div><br>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Relevant staff ID</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.secondaryRelatinId}}" ng-model="acctDetails.retDetails.secondaryRelatinId" placeholder="Secondary Relation ID">--%>
													<%--</div><br>--%>

													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Preferred Lang Code</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">--%>
													<%--</div><br>--%>
													<%--</div><br>--%>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Name in Pref Language</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.secondaryRelatinId}}" ng-model="acctDetails.retDetails.secondaryRelatinId" placeholder="Secondary Relation ID">--%>
													<%--</div><br>--%>
													<%--</div>--%>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Preferential Calendar Base</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">--%>
													<%--</div><br>--%>
													<%--</div><br>--%>
													<%--</div>--%>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Additional Calendar Base</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.secondaryRelatinId}}" ng-model="acctDetails.retDetails.secondaryRelatinId" placeholder="Secondary Relation ID">--%>
													<%--</div><br>--%>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Account Statement</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">--%>
													<%--</div><br>--%>
													<%--</div><br>--%>
													<%--</div>--%>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Statement Frequency</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.secondaryRelatinId}}" ng-model="acctDetails.retDetails.secondaryRelatinId" placeholder="Secondary Relation ID">--%>
													<%--</div><br>--%>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Name in Pref Language</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.secondaryRelatinId}}" ng-model="acctDetails.retDetails.secondaryRelatinId" placeholder="Secondary Relation ID">--%>
													<%--</div><br>--%>
													<%--</div>--%>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Preferential Calendar Base</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">--%>
													<%--</div><br>--%>
													<%--</div><br>--%>
													<%--</div>--%>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Dispatch Mode</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.secondaryRelatinId}}" ng-model="acctDetails.retDetails.secondaryRelatinId" placeholder="Secondary Relation ID">--%>
													<%--</div><br>--%>
													<%--<div class="form-group">--%>
													<%--<label  class="col-sm-4 control-label">Next Print Date</label>--%>
													<%--<div class="col-sm-8 ">--%>
													<%--<input type="date" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">--%>
													<%--</div><br>--%>
													<%--</div><br>--%>
													<%--</div><br>--%>
												</div><br>
												<!--<div class="box-footer" style="display:block">-->
												<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
												<!--</div>-->
											</div>
											<!-- /.box-body -->
										</div>
									</div>
									<!-- /.tab-pane -->
									<div class="tab-pane" id="tab_12">
										<div class="box" >
											<div class="box-body">
												<div class="direct-chat-messages" style="height:330px">
													<div class="form-group">
														<label  class="col-sm-3 control-label">Relation Type</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.docType" ng-model="selectedWorkFlow.fidDocument.docType" ng-options="option for option in appOptions.docTypeOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Relation Code</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.fidDocument.docId" value="{{selectedWorkFlow.fidDocument.docId}}" placeholder="Relation Code">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Statement Freq.</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.docType" ng-model="selectedWorkFlow.fidDocument.docType" ng-options="option for option in appOptions.docTypeOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Next Pass Dt</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" ng-model="selectedWorkFlow.fidDocument.docId" value="{{selectedWorkFlow.fidDocument.docId}}" placeholder="Next Pass Sheet Print">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Dispatch Mode</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.cntryOfIssue" ng-model="selectedWorkFlow.fidDocument.cntryOfIssue" ng-options="option for option in appOptions.cntryOptns" >
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Designation Code</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Issue date">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Paymnt Mssg freq</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="selectedWorkFlow.fidDocument.cntryOfIssue" ng-model="selectedWorkFlow.fidDocument.cntryOfIssue" ng-options="option for option in appOptions.cntryOptns" >
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Nxt Pay Print Dt</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Issue date">
														</div><br>
													</div><br>

													<div class="form-group">
														<label  class="col-sm-3 control-label">CID ID</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Title</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Name</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Address Type</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Address Line 1</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">State</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">City</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Country</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Postal Code</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Phone Nuber Type</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Phone Number</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Email ID Type</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Email ID</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Cif ID">
														</div><br>
													</div><br>
												</div><br>
												<!--<div class="box-footer" style="display:block">-->
												<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
												<!--</div>-->
											</div>
											<!-- /.box-body -->
										</div>
									</div>
									<!-- /.tab-pane -->
									<div class="tab-pane" id="tab_13">
										<div class="box" >
											<div class="box-body">
												<div class="direct-chat-messages" style="height:350px">
													<div class="box" >
														<div class="box-header with-border">
															<h3 class="box-title">Sector Code</h3>

															<div class="box-tools pull-right">
																<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																	<i class="fa fa-minus"></i></button>
															</div>
														</div>
														<div class="box-body">
															<div class="">

																<div class="form-group">
																	<label  class="col-sm-3 control-label">Sector Code</label>
																	<div class="col-sm-9 ">
																		<select class="form-control" ng-init="selectedWorkFlow.contact.fidAddress.addressFormat" ng-model="acctDetails.contact.fidAddress.addressFormat" ng-options="option for option in appOptions.addressFormatOptns" >
																		</select>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">
																		Subsector Code</label>
																	<div class="col-sm-9 ">
																		<select class="form-control" ng-init="selectedWorkFlow.contact.fidAddress.addressType" ng-model="acctDetails.contact.fidAddress.addressType" ng-options="option for option in appOptions.addressTypeOptns" >
																		</select>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Occupation Code</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" ng-model="acctDetails.contact.fidAddress.addressLabel" value="{{selectedWorkFlow.contact.fidAddress.addressLabel}}" placeholder="Address label">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">
																		Borrower Category
																	</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" ng-model="acctDetails.contact.fidAddress.addressLine1" value="{{selectedWorkFlow.contact.fidAddress.addressLine1}}" placeholder="Address Line">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Purpose of Advance</label>
																	<div class="col-sm-7">
																		<select class="form-control" ng-init="selectedWorkFlow.contact.fidAddress.localGovt" ng-model="acctDetails.contact.fidAddress.localGovt" ng-options="option for option in appOptions.acctRegionOptns" >
																		</select>
																	</div>
																	<div class="col-sm-2 ">
                                                                <span class="input-group-btn">
                                                                    <button type="submit" ng-click="searchforAcct();" name="search" id="search-btn3" class="btn btn-flat bg-green">
                                                                    <i class="fa fa-search"></i>
                                                                    </button>
                                                                </span>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">State</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.state}}" ng-model="acctDetails.contact.fidAddress.state" placeholder="State">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Mode of Advance</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Advance Type</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.city}}" ng-model="acctDetails.contact.fidAddress.city" placeholder="City">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Mode of Advance</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Nature of Advance</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.city}}" ng-model="acctDetails.contact.fidAddress.city" placeholder="City">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Gauantee Cover Code</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Industry type</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.city}}" ng-model="acctDetails.contact.fidAddress.city" placeholder="City">
																	</div><br>
																</div><br>
															</div>
															<br>
															<!--<div class="box-footer" style="display:block">-->
															<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
															<!--</div>-->
														</div>
														<!-- /.box-body -->
													</div>


													<div class="box" >
														<div class="box-header with-border">
															<h3 class="box-title">Free Codes</h3>

															<div class="box-tools pull-right">
																<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																	<i class="fa fa-minus"></i></button>
															</div>
														</div>
														<div class="box-body">
															<div class="">

																<div class="form-group">
																	<label  class="col-sm-3 control-label">SBU</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.state}}" ng-model="acctDetails.contact.fidAddress.state" placeholder="State">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">
																		Broker Code</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.state}}" ng-model="acctDetails.contact.fidAddress.state" placeholder="State">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Code 3</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" ng-model="acctDetails.contact.fidAddress.addressLabel" value="{{selectedWorkFlow.contact.fidAddress.addressLabel}}" placeholder="Address label">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">
																		Free Code 4
																	</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" ng-model="acctDetails.contact.fidAddress.addressLine1" value="{{selectedWorkFlow.contact.fidAddress.addressLine1}}" placeholder="Address Line">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Code 5</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.state}}" ng-model="acctDetails.contact.fidAddress.state" placeholder="State">
																	</div><br>
																	<div class="col-sm-2 ">
                                                                <span class="input-group-btn">
                                                                    <button type="submit" ng-click="searchforAcct();" name="search" id="search-btn11" class="btn btn-flat bg-green">
                                                                    <i class="fa fa-search"></i>
                                                                    </button>
                                                                </span>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Code 6</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.state}}" ng-model="acctDetails.contact.fidAddress.state" placeholder="State">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Code 7</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Code 8</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.city}}" ng-model="acctDetails.contact.fidAddress.city" placeholder="City">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free code 9</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Acount Officer</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.city}}" ng-model="acctDetails.contact.fidAddress.city" placeholder="City">
																	</div><br>
																</div><br>
															</div>
															<br>
															<!--<div class="box-footer" style="display:block">-->
															<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
															<!--</div>-->
														</div>
														<!-- /.box-body -->
													</div>

													<%--<button ng-if="selectedWorkFlow.retDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'UPDATED');" class="btn btn-primary">Update</button>--%>
													<%--<button ng-if="selectedWorkFlow.retDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'REJECTED')" class="btn btn-danger" >Reject</button>--%>
													<%--<button ng-if="selectedWorkFlow.corpDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.corpDetails,'UPDATED');" class="btn btn-primary">Update</button>--%>
													<%--<button ng-if="selectedWorkFlow.corpDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.corpDetails,'REJECTED')" class="btn btn-danger" >Reject</button>--%>
													<%--<a href="#" style="padding-left: 2%"><i class="fa fa-clipboard">Add Note</i></a>--%>
													<%--<button type="submit" ng-click="navToUpdateList()" class="btn btn-default pull-right" >Cancel</button>--%>

													<!-- /.tab-pane -->
												</div>
												<!-- /.tab-content -->
											</div>
										</div>


									</div>

									<div class="tab-pane" id="tab_14">
										<div class="box" >
											<div class="box-body">
												<div class="direct-chat-messages" style="height:350px">
													<div class="box" >
														<%--<div class="box-header with-border">--%>
														<%--<h3 class="box-title">Sector Code</h3>--%>

														<%--<div class="box-tools pull-right">--%>
														<%--<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">--%>
														<%--<i class="fa fa-minus"></i></button>--%>
														<%--</div>--%>
														<%--</div>--%>
														<div class="box-body">
															<div class="">
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Document Code</label>
																	<div class="col-sm-9 ">
																		<select class="form-control" ng-init="selectedWorkFlow.contact.fidAddress.addressFormat" ng-model="acctDetails.contact.fidAddress.addressFormat" ng-options="option for option in appOptions.addressFormatOptns" >
																		</select>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">
																		Due Date</label>
																	<div class="col-sm-9 ">
																		<select class="form-control" ng-init="selectedWorkFlow.contact.fidAddress.addressType" ng-model="acctDetails.contact.fidAddress.addressType" ng-options="option for option in appOptions.addressTypeOptns" >
																		</select>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Notes</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" ng-model="acctDetails.contact.fidAddress.addressLabel" value="{{selectedWorkFlow.contact.fidAddress.addressLabel}}" placeholder="Address label">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">
																		Recieved Date
																	</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" ng-model="acctDetails.contact.fidAddress.addressLine1" value="{{selectedWorkFlow.contact.fidAddress.addressLine1}}" placeholder="Address Line">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Expiry Date</label>
																	<div class="col-sm-7">
																		<select class="form-control" ng-init="selectedWorkFlow.contact.fidAddress.localGovt" ng-model="acctDetails.contact.fidAddress.localGovt" ng-options="option for option in appOptions.acctRegionOptns" >
																		</select>
																	</div>
																	<div class="col-sm-2 ">
                                                                <span class="input-group-btn">
                                                                    <button type="submit" ng-click="searchforAcct();" name="search" id="search-btn10" class="btn btn-flat bg-green">
                                                                    <i class="fa fa-search"></i>
                                                                    </button>
                                                                </span>
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Record</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.state}}" ng-model="acctDetails.contact.fidAddress.state" placeholder="State">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Scan Details</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="box-header with-border">
																	<h3 class="box-title">Free Text</h3>
																</div>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 1</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 2</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 3</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 4</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 4</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 5</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 6</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 7</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 8</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 9</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="form-group">
																	<label  class="col-sm-3 control-label">Free Text 10</label>
																	<div class="col-sm-9 ">
																		<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
																	</div><br>
																</div><br>
																<div class="box-footer">
																	<button type="submit" ng-click="addCurrency();" class="btn btn-warning pull-right" >Add Document</button>
																</div>
															</div>
															<br>
															<!--<div class="box-footer" style="display:block">-->
															<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
															<!--</div>-->
														</div>
														<!-- /.box-body -->
													</div>

													<%--<button ng-if="selectedWorkFlow.retDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'UPDATED');" class="btn btn-primary">Update</button>--%>
													<%--<button ng-if="selectedWorkFlow.retDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'REJECTED')" class="btn btn-danger" >Reject</button>--%>
													<%--<button ng-if="selectedWorkFlow.corpDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.corpDetails,'UPDATED');" class="btn btn-primary">Update</button>--%>
													<%--<button ng-if="selectedWorkFlow.corpDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.corpDetails,'REJECTED')" class="btn btn-danger" >Reject</button>--%>
													<%--<a href="#" style="padding-left: 2%"><i class="fa fa-clipboard">Add Note</i></a>--%>
													<%--<button type="submit" ng-click="navToUpdateList()" class="btn btn-default pull-right" >Cancel</button>--%>

													<!-- /.tab-pane -->
												</div>
												<!-- /.tab-content -->
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class=""  id="attchedDocDiv">
								<div id="acctDataBox" class="direct-chat-messages" style="display:block;height:350px">
									<table id="" class="table table-bordered table-striped">
										<thead>
										<tr>
											<th>Document</th>
											<th>Status</th>
										</tr>
										</thead>
										<tbody>
										<tr ng-repeat="x in DocsToSubmit">
											<td><label class="col-md-8">{{x.docType}}</label></td>
											<td ng-if = "x.docVal == 'S'"><span  class="label label-primary">Submitted</span></td>
											<td ng-if = "x.docVal == 'D'"><span  class="label label-warning">Defferred</span></td>
											<td ng-if = "x.docVal == 'N'"><span  class="label label-default">Not Applicable</span></td>
										</tr>
										</tbody>
									</table>
									<br>
								</div>
								<br>
								<div id="noteDiv" class="form-group" style="display:block">
									<label>Note :</label>
									<br>
									<textarea id="noteContent" class="form-control" rows="3" placeholder="" enabled=""></textarea>
								</div>
							</div><br>

							<div>
								<button  type="submit" ng-click="updateDocument(selectedWorkFlow,'VERIFIED');" class="btn btn-primary">Verify</button>
								<button  type="submit" ng-click="updateDocument(selectedWorkFlow,'REJECTED')" class="btn btn-danger" >Reject</button>
								<%--<a href="#" style="padding-left: 2%"><i class="fa fa-clipboard">Add Note</i></a>--%>
								<button type="submit" ng-click="navToUpdateList()" class="btn btn-default pull-right" >Cancel</button>
							</div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>

				<!-- /.row -->

				<div class="col-xs-12 col-md-6" id="docMntDiv">

					<!-- /.box -->

					<div id="pdfBox" class="box " style="display:none" >
						<div class="box-header with-border">
							<h3 class="box-title">Uploaded Document: </h3>
							<small id="docTitle1"></small>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div style="height:370px">
								<embed width="100%" height="100%" name="plugin" id = "docName1" src="">
							</div>


						</div>
					</div>
					<div id="imgBox" class="box "  style="display:none">
						<div class="box-header with-border">
							<h3 class="box-title">Uploaded Document: </h3>
							<small id="docTitle2"></small>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div style="height:370px">
								<!--<embed width="100%" height="100%" name="plugin" id = "docName2" src=" " type="application/pdf" internalinstanceid="4">-->
								<img  width="100%" height="100%" id = "docName2" src="">
							</div>
						</div>
						<!-- /.box-body -->
					</div>
					<div id="rejectDiv" class="form-group" style="display:none">
						<label class="">Reject To</label>
						<select class="rejLabel" id="rejectOption" onChange="checkOption();">
							<option value="">--Select--</option>
							<option value="UPDATER">Updater</option>
							<option value="BRANCH">Branch</option>
						</select>
						<label class="bg-red rejLabel"  id="rejAlert">Please Select</label>
						<br>
						<textarea id="rejectReason" class="form-control" rows="3" placeholder="Type Here" enabled=""></textarea>
					</div>
				</div>
				<!-- /.col -->
			</div>
		</section>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 1
		</div>
		<strong>Copyright &copy; 2016 <a href="http://longbridgetechnology.com">Longbridge Technology</a>.</strong>
	</footer>

	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Create the tabs -->
		<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
			<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
			<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">

		</div>
	</aside>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</body>
</html>

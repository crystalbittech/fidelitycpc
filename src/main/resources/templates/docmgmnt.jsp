<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CPC</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
    <%--<link rel="icon" href="/branchconsole/Fidelity-Icon.png">--%>
    <script src="/angular/angular.min.js"></script>
    <script src="/angular/controller.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini"  ng-app="angularApp" ng-controller="csoCntrl">
<script>
    function setRealField(){
        document.getElementById("acctNum").value = document.getElementById("searcherAcctVal").value;
        document.getElementById("search-btn").click();
    }
    function closeSearcher(){
        $("#searcherDiv").hide();;
    }
    function checkFileType(){
        var docVal = document.getElementById("inputFile").value;
        var acctInFile;
        if(docVal!=null){
            acctInFile = docVal.split(".")[0].replace(/^.*\\/, '');;
            var extsnVal = docVal.split(".")[1];
            if((!/(\pdf)$/i.test(extsnVal)) && (!/(\bmp|\gif|\jpg|\jpeg|\png)$/i.test(extsnVal))){
                alert("Invalid File Type");
                document.getElementById("inputFile").value = "";
                return;
            }
            if(acctInFile.search(/[^a-zA-Z]+/) == 0 && acctInFile.length>10){
                $("#acctNum").val(acctInFile);;
                $("#search-btn").click();;
            }


        }
    }
</script>
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b><img src="/branchconsole/icon.png" alt="CPC" style="margin-top: 0px; width:42px; height: 42px"></b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="/branchconsole/icon.png" style="margin-top: 0px; width:42px; height: 42px">&nbsp; <b>Fidelity  </b>CPC</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav"><!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">USER 1</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    User 1 - CSO
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="profile" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="FidelityCPC" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <section>
            <ul class="sidebar-menu"><br>
                <li class="header">ACCOUNT OPERATIONS</li>
                <b id="csoUser" style="display: none"><c:out  value="${pageContext.request.remoteUser}"></c:out></b>
                <%--<sec:authorize access="hasRole('REVIEWER') or hasRole('ADMIN')">--%>
                <%--<li>--%>
                    <%--<a href="review">--%>
                        <%--<i class="fa fa-pencil-square-o"></i> <span>Review</span>--%>
                    <%--</a>--%>
                <%--</li>--%>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('INPUTER') or hasRole('ADMIN')">--%>
                <li>
                    <a href="update">
                        <i class="fa fa-pencil-square-o"></i> <span>Update</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-red">3</small>
                        </span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('VERIFIER') or hasRole('ADMIN')">--%>
                <li>
                    <a href="verify">
                        <i class="fa fa-check-square"></i> <span>Verify</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-blue">17</small>
                        </span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--&lt;%&ndash;<sec:authorize access="hasRole('VISITOR')">&ndash;%&gt;--%>
                <li>
                    <a href="report">
                        <i class="fa fa-bar-chart"></i> <span>Reports</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('ADMIN')">--%>
                <li>
                    <a href="umr">
                        <i class="fa fa-user-plus"></i> <span>User Management</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('CSO')  or hasRole('ADMIN')">--%>
                <li>
                    <a href="docmgmnt">
                        <i class="fa fa-file-pdf-o"></i> <span>CSO Operations</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>

            </ul>
        </section>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Document Mapping
            </h1>
        </section>

        <!-- Main content -->
        <section class="content" id="docWorkflow">
            <div class="row">
                <div class="col-xs-12">

                    <!-- /.box -->

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Document Name</th>
                                    <th>Date Uploaded</th>
                                    <th>Account Number</th>
                                    <th>Account Opn Date</th>
                                    <th>Status</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="x in workFlowforCSO">
                                    <td>{{$index +1}}</td>
                                    <td>{{x.acctNumber}}</td>
                                    <td>{{x.acctOpnDate|date:'dd-MM-yyyy'}}</td>
                                    <td>{{x.docId}}</td>
                                    <td>{{x.docmntUpldDate|date:'dd-MM-yyyy h:mm a'}}</td>
                                    <td ng-if = "x.acctStatus == 'LOCKED'"><span class="label label-default">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'UPDATED'"><span class="label label-warning">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'VERIFIED'"><span class="label label-success">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'REJECTED'"><span class="label label-danger">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'DEFAULT'"><span class="label label-default">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'DEFAULT' || x.acctStatus == 'REJECTED'"><a href="#mapDocmntView" ng-click="loadacctDetails(x.acctNumber)" class="btn btn-primary">View</a></td>
                                    <td ng-if = "x.acctStatus != 'DEFAULT' && x.acctStatus != 'REJECTED'"><a href="#docWorkflow" onclick="alert('You cannot modify, Record already in Workflow')" class="btn btn-default" disabled>View</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <script>
                            //alert("well");
                            //document.getElementById("docName1").src = "/upldDocd/class3.pdf";
                        </script>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="#mapDocmntView" ng-click="navToMap_refresh()" class="btn btn-primary">Add New</a>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

        <section class="content" id="mapDocmntView" style="display:none">
            <div class="row">
                <div class="box col-xs-12">
                        <%--<div class="box-header with-border">--%>
                            <%--<h3 class="box-title">Account Information</h3>--%>
                        <%--</div>--%>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-xs-8 col-md-8">
                                <div class="input-group" >
                                    <input type="text" name="acctNum"  onClick="closeSearcher();"  id="acctNum"  class="form-control" placeholder="Enter Account Number" style="border-radius:3px">
                                    <span class="input-group-btn">

                                        <button type="submit" ng-click="searchforAcct();" name="search" id="search-btn" class="btn btn-flat bg-green">
                                        <%--<img style="height: 50px;width: 10px" src="loader.png"/>--%>
                                        <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                    <span class="input-group-btn " style="padding-left:15px">
                                        <a href="#docWorkflow" class="btn btn-primary" ng-click="navToDocWorkflow()">View Uploads</a>
                                </span>
                                </div><br>
                                <div id="searcherDiv" style="display:none;width:85%" >
                                    <select multiple onchange="setRealField();" id="searcherAcctVal" style="height:120px" class="form-control col-md-8">
                                        <option ng-repeat="option in wholeRecords" value="{{option.accountid}}">{{option.accountid}}</option>
                                    </select><br><br>
                                </div>
                                <%--<form method="POST" action="/upload" enctype="multipart/form-data" id="searchObstructor">--%>
                                    <div  class="form-group">
                                        <label  class="col-sm-3 control-label">Account Number</label>
                                        <div class="col-sm-9 ">
                                            <input type="text" class="form-control" id="acctNumber" value="{{acctNumber.acctId}}" placeholder="Account Number" readonly>
                                        </div><br>
                                    </div>
                                    <div class="">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Account Name</label>
                                            <div class="col-sm-9 ">
                                                <input type="text" class="form-control"  value="{{acct.acctInqResponse.acctInqRs.custId.personName.name}}" placeholder="Account Name" readonly>
                                            </div><br>
                                        </div>
                                    </div>
                                    <div ng-if="acct.corpAcctInqResp != null" class="form-group">
                                        <label  class="col-sm-3 control-label">Account Type</label>
                                        <div class="col-sm-9 ">
                                            <input type="text" class="form-control" id="acctType" value="{{acct.acctInqResponse.acctInqRs.acctType.schmCode}}" placeholder="Account Type" readonly>
                                        </div><br>
                                    </div>
                                    <div class="form-group">
                                        <label  class="col-sm-3 control-label">Account Balance</label>
                                        <div class="col-sm-9 ">
                                            <input type="text" class="form-control" id="acctBal" value="{{acct.acctInqResponse.acctInqRs.acctBal[0].balAmt.amountValue}}" placeholder="Account Balance" readonly>
                                        </div><br>
                                    </div>
                                    <div  class="">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Account Open date</label>
                                            <div class="col-sm-9 ">
                                                <input type="text" class="form-control"  value="{{acct.acctInqResponse.acctInqRs.acctOpenDt| limitTo:10}}" placeholder="Account Opn date" readonly>
                                            </div><br>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="col-sm-3 control-label">WorkFlow Status</label>
                                        <div class="col-sm-9 ">
                                            <input type="text" class="form-control" id="wkFlowStat" value="{{status.acctStatus}}" placeholder="Workflow Status" readonly>
                                        </div><br>
                                    </div><br>
                                    <div class="form-group">
                                        <label  class="col-sm-3 control-label">Document Name</label>
                                        <div class="col-sm-9 ">
                                            <input  type="file" onChange="checkFileType();" name="uploadedFile" fileread="acct['fileVal']"  required="true" id="inputFile" class="form-control">
                                        </div><br>
                                    </div><br>
                                    <div ng-if = "status.statusReason != '' && status!=null" id="rejectDiv" class="rejectDiv form-group" >
                                        <label  class="col-sm-3 control-label">Reason for Rejection</label>
                                        <div class="col-sm-9 ">
                                            <textarea id="rejectReason" class="form-control" rows="3" placeholder="Reason For Rejection" enabled="">{{status.statusReason}}</textarea>
                                        </div>
                                    </div><br><br><br>
                                    <div id="noteDiv" class="form-group" style="display:none">
                                        <label  class="col-sm-3 control-label" >Note:</label>
                                        <div class="col-sm-9 ">
                                            <textarea id="note" class="form-control" rows="3" placeholder="Type Here" enabled=""></textarea>
                                        </div><br>
                                    </div><br><br><br>
                                    <button class="btn btn-primary" ng-click="mapDocument(acct)" type="submit">Submit</button>
                                    <input type="submit" id="finalSubmit" value="Submit" style="display: none"/>
                                    <a  ng-click = "showNote()" style="padding-left: 2%"><i class="fa fa-clipboard">Add Note</i></a>
                                <%--</form>--%>
                            </div>

                            <div class="col-xs-4 col-md-4 ">
                                <label><h3>Documents</h3></label><br>
                                <div class="direct-chat-messages" style="height:350px">
                                    <div ng-repeat="x in DocsToSubmit">
                                        <label>{{x.docType}}</label><br>
                                        <input type="radio"  ng-model="x.docVal" value="S">Submitted
                                        <input type="radio"  ng-model="x.docVal" value="D">Defferred
                                        <input type="radio"  ng-model="x.docVal" value="N">Not Applicable<br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.12
        </div>
        <strong>Copyright &copy; 2016 <a href="http://longbridgetechnology.com">Longbridge Technology</a>.</strong>
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">

        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</body>
</html>

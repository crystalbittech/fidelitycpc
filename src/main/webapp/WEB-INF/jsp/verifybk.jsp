<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title> CPC</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
	<link rel="icon" href="/branchconsole/Fidelity-Icon.png">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
           folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
	<script src="/angular/angular.min.js"></script>
	<script src="/angular/controller.js"></script>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-app="angularApp" ng-controller="verifyCntrl">
<script>
    function checkOption(){
        var optionSelected = document.getElementById("rejectOption").value;
        if(optionSelected!=""){
            $("#rejAlert").hide();;
        }else{
            $("#rejAlert").show();;
        }
    }
</script>
<div class="wrapper">

	<header class="main-header">
		<!-- Logo -->
		<a href="../../index2.html" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b><img src="/branchconsole/Fidelity-Icon.png" alt="App Logo" style="margin-top: 0px; width:42px; height: 42px"></b></span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><img src="/branchconsole/Fidelity-Icon.png" alt="App Logo" style="margin-top: 0px; width:42px; height: 42px">&nbsp; <b>  </b>CPC</span>
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>

			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav"><!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
							<span id="loginUser" class="hidden-xs">USER2</span>
						</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header">
								<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

								<p>
									User 1 - CSO
									<small>Member since Nov. 2012</small>
								</p>
							</li>
							<!-- Menu Body -->

							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="profile" class="btn btn-default btn-flat">Profile</a>
								</div>
								<div class="pull-right">
									<a href="FidelityCPC" class="btn btn-default btn-flat">Sign out</a>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<%--<        class="sidebar">--%>
		<!-- Sidebar user panel -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<section>
			<ul class="sidebar-menu"><br>
				<li class="header">ACCOUNT OPERATIONS</li>
				<b id="csoUser" style="display: none"><c:out  value="${pageContext.request.remoteUser}"></c:out></b>
				<%--<sec:authorize access="hasRole('REVIEWER') or hasRole('ADMIN')">--%>
				<%--<li>--%>
				<%--<a href="review">--%>
				<%--<i class="fa fa-pencil-square-o"></i> <span>Review</span>--%>
				<%--</a>--%>
				<%--</li>--%>
				<%--</sec:authorize>--%>
				<%--<sec:authorize access="hasRole('INPUTER') or hasRole('ADMIN')">--%>
				<li>
					<a href="update" ng-click="resetLocker('UPDATED')">
						<i class="fa fa-pencil-square-o"></i> <span>Update</span>
						<span class="pull-right-container">
                          <small class="label pull-right bg-red">3</small>
                        </span>
					</a>
				</li>
				<%--</sec:authorize>--%>
				<%--<sec:authorize access="hasRole('VERIFIER') or hasRole('ADMIN')">--%>
				<li>
					<a href="verify" ng-click="resetLocker('UPDATED')">
						<i class="fa fa-check-square"></i> <span>Verify</span>
						<span class="pull-right-container">
                          <small class="label pull-right bg-blue">17</small>
                        </span>
					</a>
					<ul class="treeview-menu" style="display: block;">
						<li><a href="" ng-click="showCRM()"><i class="fa fa-circle-o"></i>CRM</a></li>
						<li><a href="" ng-click="showHACM()"><i class="fa fa-circle-o"></i>HACM</a></li>
					</ul>
				</li>
				<%--</sec:authorize>--%>
				<%--&lt;%&ndash;<sec:authorize access="hasRole('VISITOR')">&ndash;%&gt;--%>
				<li>
					<a href="report" ng-click="resetLocker('UPDATED')">
						<i class="fa fa-bar-chart"></i> <span>Reports</span>
					</a>
				</li>
				<%--</sec:authorize>--%>
				<%--<sec:authorize access="hasRole('ADMIN')">--%>
				<li>
					<a href="umr" ng-click="resetLocker('UPDATED')">
						<i class="fa fa-user-plus"></i> <span>User Management</span>
					</a>
				</li>
				<%--</sec:authorize>--%>
				<%--<sec:authorize access="hasRole('CSO')  or hasRole('ADMIN')">--%>
				<li>
					<a href="docmgmnt" ng-click="resetLocker('UPDATED')">
						<i class="fa fa-file-pdf-o"></i> <span>CSO Operations</span>
					</a>
				</li>
				<%--</sec:authorize>--%>

			</ul>
		</section>
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Account Verification
			</h1>
		</section>

		<!-- Main content -->
		<section class="content" id="acctUpdateList">
			<div class="row">
				<div class="col-xs-12">

					<!-- /.box -->

					<div class="box">
						<!-- /.box-header -->
						<div class="box-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
								<tr>
									<th>Account Number</th>
									<th>Account Open Date</th>
									<th>Document Name</th>
									<th>Date Uploaded</th>
									<th>Sol ID</th>
									<th>Sol Name</th>
									<th>Uploaded By</th>
									<th>Inputer</th>
									<th>Date Modified</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								</thead>
								<tbody>
								<tr ng-repeat="x in workFlowforUpdater">
									<td>{{x.acctNumber}}</td>
									<td>12-may-2017</td>
									<td>{{x.docId}}</td>
									<td>12-may-2017</td>
									<td>001</td>
									<td>Epe</td>
									<td>{{x.docmntUploader}}</td>
									<td>{{x.acctVerifier}}</td>
									<td>12-may-2017</td>
									<td ng-if = "x.acctStatus == 'UPDATED'"><span class="label label-warning">{{x.acctStatus}}</span></td>
									<td ng-if = "x.acctStatus == 'LOCKED'"><span class="label label-default">{{x.acctStatus}}</span></td>
									<td ng-if = "x.acctStatus == 'UPDATED'"><a href="#acctReview" ng-click="fetchAcctDetails(x)" class="btn btn-primary">View</a></td>
									<td ng-if = "x.acctStatus == 'LOCKED'"><a href="#acctReview" onclick="alert('Presently Being worked on')" class="btn btn-primary" disabled>View</a></td>

								</tr>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
		<section class="content" id="acctUpdateData" style="display:none" >
			<div class="row">
				<div class="col-xs-12 col-md-6" id="corpAcctDiv" style="display:none">
					<div class="nav-tabs-custom" >
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_1" data-toggle="tab">Basic Data</a></li>
							<li><a href="#tab_2" data-toggle="tab">Identification</a></li>
							<li><a href="#tab_3" data-toggle="tab">Contact</a></li>
							<li><a href="#tab_4" data-toggle="tab">Currency</a></li>

						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_1" >
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Basic Data</h3>
									</div>
									<div class="box-body" >
										<div class="direct-chat-messages" style="height:350px">
											<div class="form-group">
												<label  class="col-sm-3 control-label">Account Number</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.accountid" value="{{selectedWorkFlow.corpDetails.accountid}}" placeholder="Account Number" readOnly>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Account Name</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.accountName" value="{{selectedWorkFlow.corpDetails.accountName}}" placeholder="Account Name">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Legal Entity Type</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.corpDetails.entityType" ng-model="acctDetails.corpDetails.entityType" ng-options="option for option in appOptions.legalEntityTpeOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Key Contact Person</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.keycontact" value="{{selectedWorkFlow.corpDetails.keycontact}}" placeholder="Account Number" >
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Phone Number</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.phoneNumber" value="{{selectedWorkFlow.corpDetails.phoneNumber}}" placeholder="Account Number" >
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Segment</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.corpDetails.acctSegment" ng-model="acctDetails.corpDetails.acctSegment" ng-options="option for option in appOptions.SegmentOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Sub-segment</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.corpDetails.acctSubsegment" ng-model="acctDetails.corpDetails.acctSubsegment" ng-options="option for option in appOptions.SubSegmentOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Business Type</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.corpDetails.businessType" ng-model="acctDetails.corpDetails.businessType" ng-options="option for option in appOptions.bizTypeOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Country of Incorporation</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.cntryofIncorporation" value="{{selectedWorkFlow.corpDetails.cntryofIncorporation}}" placeholder="Country of Incorporation" >
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Place of Operation</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.placeofOperation" value="{{selectedWorkFlow.corpDetails.placeofOperation}}" placeholder="Place of Operation" >
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">RC Number</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.registerationNum" value="{{selectedWorkFlow.corpDetails.registerationNum}}" placeholder="Registeration Number" >
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Incorporation Date</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.corpDetails.incorpDate | date }}" placeholder="Incorporation Date">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Region</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.corpDetails.acctRegion" ng-model="acctDetails.corpDetails.acctRegion" ng-options="option for option in appOptions.acctRegionOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Relationship Type</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.corpDetails.relatnType" ng-model="acctDetails.corpDetails.relatnType" ng-options="option for option in appOptions.relatntypeOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">BVN</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.bvn" value="{{selectedWorkFlow.corpDetails.bvn}}" placeholder="BVN">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Native Language</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.corpDetails.nativeLang" ng-model="acctDetails.corpDetails.nativeLang" ng-options="option for option in appOptions.nativeLangOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Availed Trade</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.corpDetails.availedTrade" ng-model="acctDetails.corpDetails.availedTrade" ng-options="option for option in appOptions.availedTradeOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Primary Relation ID</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.primaryRelatinId" value="{{selectedWorkFlow.corpDetails.primaryRelatinId}}" placeholder="Primary Relation ID">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Secondary Relation ID</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.corpDetails.secondaryRelatinId" value="{{selectedWorkFlow.corpDetails.secondaryRelatinId}}" placeholder="Secondary Relation ID">
												</div><br>
											</div><br>
										</div><br>
										<!--<div class="box-footer" style="display:block">-->
										<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
										<!--</div>-->
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_2">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Identification Details</h3>
									</div>
									<div class="box-body">
										<div class="direct-chat-messages" style="height:350px">

											<div class="form-group">
												<label  class="col-sm-3 control-label">Document Type</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" >
														<option selected>Passport</option>
														<option>Voter's card</option>
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Document Code</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" >
														<option selected>D10</option>
														<option>D1</option>
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Unique ID</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField"  placeholder="Unique ID">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Place of Issue</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" >
														<option selected>Lagos</option>
														<option>Abuja</option>
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Country of Issue</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" >
														<option selected>Nigeria</option>
														<option>Libya</option>
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Issue date</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField"  placeholder="Issue date">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Expiry date</label>
												<div class="col-sm-9 ">
													<input type="Date" class="form-control disabledField"  placeholder="Expiry date">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Is Preferred</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" >
														<option selected>Y</option>
														<option>N</option>
													</select>
												</div><br>
											</div><br>
										</div><br>
										<!--<div class="box-footer" style="display:block">-->
										<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
										<!--</div>-->
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_3">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Contact</h3>
									</div>
									<div class="box-body">

										<div class="direct-chat-messages" style="height:297px">
											<div class="box" >
												<div class="box-header with-border">
													<h3 class="box-title">Address</h3>

													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
															<i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="box-body">
													<div class="">

														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Format</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >s
																	<option selected>Free-Text</option>
																	<option>Mail</option>
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Type</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >s
																	<option selected>Mailing</option>
																	<option>PO Box</option>
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Label</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField"  placeholder="Address label">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Line 1</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField"  placeholder="Address Line">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Local Govt.</label>
															<div class="col-sm-7">
																<select class="form-control disabledField" >s
																	<option selected>Male</option>
																	<option>Female</option>
																</select>
															</div>
															<div class="col-sm-2 ">
                                                                <span class="input-group-btn">
                                                                    <button type="submit" ng-click="searchforAcct();" name="search" id="search-btn2" class="btn btn-flat bg-green">
                                                                    <i class="fa fa-search"></i>
                                                                    </button>
                                                                </span>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">State</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField"  placeholder="State">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Country</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField"  placeholder="Country">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">City</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField"  placeholder="City">
															</div><br>
														</div><br>
													</div>
													<br>
													<!--<div class="box-footer" style="display:block">-->
													<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
													<!--</div>-->
												</div>
												<!-- /.box-body -->
											</div>

											<div class="box" >
												<div class="box-header with-border">
													<h3 class="box-title">Phone and Email</h3>

													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
															<i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="box-body">
													<div class="">


														<div class="form-group">
															<label  class="col-sm-3 control-label">Preferred Contact Type</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >s
																	<option selected>Cell Phone</option>
																	<option>Email</option>
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Preferred Mail Type</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >
																	<option selected>Communication</option>
																	<option>Instruction</option>
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Preferred Mobile Alert type</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >
																	<option selected>Cell Phone</option>
																	<option>Email</option>
																</select>
															</div><br>
														</div><br>
														<div ng-repeat="x in selectedWorkFlow.contact.fidPrefPhone_email">
															<div class="form-group">
																<label  class="col-sm-3 control-label">Phone or Email {{$index +1}}</label>
																<div class="col-sm-9 ">
																	<select id="phnEmailOpt" class="form-control disabledField" onChange="alternatePhnEmailDiv();">
																		<option value="Phone" selected>Phone</option>
																		<option value="Email">Email</option>
																	</select>
																</div><br>
															</div><br>
															<div class="form-group phoneDiv" >
																<label  class="col-sm-3 control-label">Type</label>
																<div class="col-sm-9 ">
																	<select class="form-control disabledField" >
																		<option selected>Cell Phone</option>
																		<option>Land Line</option>
																	</select>
																</div><br>
															</div><br>
															<div class="form-group phoneDiv" >
																<label  class="col-sm-3 control-label">Phone Number</label>
																<div class="col-sm-9 ">
																	<input type="text" class="form-control disabledField"  placeholder="Phone Number">
																</div><br>
															</div><br>

															<div class="form-group emailDiv" style="display:none">
																<label  class="col-sm-3 control-label">Type</label>
																<div class="col-sm-9 ">
																	<select class="form-control disabledField" >
																		<option selected>Communication</option>
																		<option>Fax</option>
																	</select>
																</div><br>
															</div><br>
															<div class="form-group emailDiv" style="display:none">
																<label  class="col-sm-3 control-label">Email ID</label>
																<div class="col-sm-9 ">
																	<input type="text" class="form-control disabledField"  placeholder="Email Id">
																</div><br>
															</div><br>
														</div>


													</div><br>

												</div>

												<!-- /.box-body -->
											</div>

										</div><br>
										<div class="box-footer">
											<button type="submit" ng-click="addPhoneEmail()" class="btn btn-warning pull-right" >Add Phone_Email</button>
										</div>
										<!--<div class="box-footer" style="display:none">-->
										<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
										<!--</div>-->

									</div>
									<!-- /.box-body -->
								</div>
							</div>

							<div class="tab-pane" id="tab_4">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Currency</h3>
									</div>
									<div class="box-body">
										<div class="direct-chat-messages" style="height:297px">
											<div class="box" ng-repeat="x in selectedWorkFlow.fidCrncy">
												<div class="box-header with-border">
													<h3 class="box-title">Currency {{$index+1}}</h3>

													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
															<i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="box-body">
													<div class="form-group">
														<label  class="col-sm-3 control-label">Currency</label>
														<div class="col-sm-9 ">
															<select class="form-control disabledField" >
																<option selected>NGN</option>
																<option>USD</option>
																<option>GBP</option>
																<option>EUR</option>
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Witholding Tax Pcnt</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control disabledField"  placeholder="Witholding Tax Pcnt">
														</div><br>
													</div><br>
												</div>
												<!-- /.box-body -->
											</div>

										</div><br>
										<div class="box-footer">
											<button type="submit" ng-click="addCurrency();" class="btn btn-warning pull-right" >Add Currency</button>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<button type="submit" ng-click="updateDocument(selectedWorkFlow.corpDetails,'VERIFIED');" class="btn btn-primary">VERIFY</button>
							<button type="submit" ng-click="updateDocument(selectedWorkFlow.corpDetails,'ADVICED')" class="btn btn-danger" >REJECT</button>
							<button type="submit" ng-click="navToUpdateList()" class="btn btn-default pull-right" >Cancel</button>

							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
				</div>
				<div class="col-xs-12 col-md-6" id="retAcctDiv">
					<div class="nav-tabs-custom" >
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_5" data-toggle="tab">Basic Data</a></li>
							<li><a href="#tab_6" data-toggle="tab">Identification</a></li>
							<li><a href="#tab_7" data-toggle="tab">Contact</a></li>
							<li><a href="#tab_8" data-toggle="tab">Currency</a></li>
							<li><a href="#tab_9" data-toggle="tab">Demographic</a></li>
							<li><a href="#tab_10" data-toggle="tab">Trade Finance</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_5" >
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Basic Data</h3>
									</div>
									<div class="box-body" >
										<div class="direct-chat-messages" style="height:315px">

											<div class="form-group">
												<label  class="col-sm-3 control-label">Account Number</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.retDetails.accountid}}" ng-model="acctDetails.retDetails.accountid" readOnly>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">First Name</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.retDetails.firstName}}" ng-model="acctDetails.retDetails.firstName" >
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Last Name</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.retDetails.lastName}}" ng-model="acctDetails.retDetails.lastName" placeholder="Last Name">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Middle Name</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.retDetails.middleName}}" ng-model="acctDetails.retDetails.middleName" placeholder="Middle Name">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">DOB</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.retDetails.custDob|date}}" placeholder="Date of Birth">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Title</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.retDetails.title}}" ng-model="acctDetails.retDetails.title" placeholder="Title">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Gender</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.retDetails.gender" ng-model="acctDetails.retDetails.gender" ng-options="option for option in appOptions.GenderOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">BVN</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.retDetails.custBvn}}" ng-model="acctDetails.retDetails.custBvn" placeholder="BVN">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Segment</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.retDetails.acctSegment" ng-model="acctDetails.retDetails.acctSegment" ng-options="option for option in appOptions.SegmentOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Sub-segment</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.retDetails.acctSubsegment" ng-model="acctDetails.retDetails.acctSubsegment" ng-options="option for option in appOptions.SubSegmentOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Availed Trade</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.retDetails.availedTrade" ng-model="acctDetails.retDetails.availedTrade" ng-options="option for option in appOptions.availedTradeOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Region</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.retDetails.acctRegion" ng-model="acctDetails.retDetails.acctRegion" ng-options="option for option in appOptions.acctRegionOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Preferred Local</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.retDetails.prefLocal" ng-model="acctDetails.retDetails.prefLocal" ng-options="option for option in appOptions.prefLocalOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Enable CRM Alert</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.retDetails.enableCrm" ng-model="acctDetails.retDetails.enableCrm" ng-options="option for option in appOptions.dualOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Enable E-Banking</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.retDetails.enableEbanking" ng-model="acctDetails.retDetails.enableEbanking" ng-options="option for option in appOptions.dualOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Primary Relation ID</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Secondary Relation ID</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.retDetails.secondaryRelatinId}}" ng-model="acctDetails.retDetails.secondaryRelatinId" placeholder="Secondary Relation ID">
												</div><br>
											</div><br>
										</div><br>
										<!--<div class="box-footer" style="display:block">-->
										<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
										<!--</div>-->
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_6">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Identification Details</h3>
									</div>
									<div class="box-body">
										<div class="direct-chat-messages" style="height:315px">

											<div class="form-group">
												<label  class="col-sm-3 control-label">Document Type</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.fidDocument.docType" ng-model="fetchedCustomerDetails.fidDocument.docType" ng-options="option for option in appOptions.docTypeOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Document Code</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.fidDocument.docCode" ng-model="fetchedCustomerDetails.fidDocument.docCode" ng-options="option for option in appOptions.docCodeOptns" >
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Unique ID</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" ng-model="acctDetails.fidDocument.docId" value="{{selectedWorkFlow.fidDocument.docId}}" placeholder="Unique ID">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Place of Issue</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.fidDocument.placeOfIssue" ng-model="fetchedCustomerDetails.fidDocument.placeOfIssue" ng-options="option for option in appOptions.acctRegionOptns" >
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Country of Issue</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.fidDocument.cntryOfIssue" ng-model="fetchedCustomerDetails.fidDocument.cntryOfIssue" ng-options="option for option in appOptions.cntryOptns" >
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Issue date</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Issue date">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Expiry date</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control disabledField" value="{{selectedWorkFlow.fidDocument.expiryDate|date}}" placeholder="Expiry date">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Is Preferred</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" ng-init="selectedWorkFlow.fidDocument.isPreferred" ng-model="fetchedCustomerDetails.fidDocument.isPreferred" ng-options="option for option in appOptions.dualOptns" >
													</select>
												</div><br>
											</div><br>
										</div><br>
										<!--<div class="box-footer" style="display:block">-->
										<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
										<!--</div>-->
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_7">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Contact</h3>
									</div>
									<div class="box-body">

										<div class="direct-chat-messages" style="height:260px">
											<div class="box" >
												<div class="box-header with-border">
													<h3 class="box-title">Address</h3>

													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
															<i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="box-body">
													<div class="">

														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Format</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" ng-init="selectedWorkFlow.contact.fidAddress.addressFormat" ng-model="acctDetails.contact.fidAddress.addressFormat" ng-options="option for option in appOptions.addressFormatOptns" >
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Type</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" ng-init="selectedWorkFlow.contact.fidAddress.addressType" ng-model="acctDetails.contact.fidAddress.addressType" ng-options="option for option in appOptions.addressTypeOptns" >
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Label</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField" ng-model="acctDetails.contact.addressLabel" value="{{selectedWorkFlow.contact.addressLabel}}" placeholder="Address label">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Line 1</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField" ng-model="acctDetails.contact.addressLine1" value="{{selectedWorkFlow.contact.addressLine1}}" placeholder="Address Line">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Local Govt.</label>
															<div class="col-sm-7">
																<select class="form-control disabledField" ng-init="selectedWorkFlow.contact.localGovt" ng-model="acctDetails.contact.localGovt" ng-options="option for option in appOptions.acctRegionOptns" >
																</select>
															</div>
															<div class="col-sm-2 ">
                                                                <span class="input-group-btn">
                                                                    <button type="submit" ng-click="searchforAcct();" name="search" id="search-btn" class="btn btn-flat bg-green">
                                                                    <i class="fa fa-search"></i>
                                                                    </button>
                                                                </span>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">State</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField"  placeholder="State">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Country</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField"  placeholder="Country">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">City</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control disabledField"  placeholder="City">
															</div><br>
														</div><br>
													</div>
													<br>
													<!--<div class="box-footer" style="display:block">-->
													<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
													<!--</div>-->
												</div>
												<!-- /.box-body -->
											</div>

											<div class="box" >
												<div class="box-header with-border">
													<h3 class="box-title">Phone and Email</h3>

													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
															<i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="box-body">
													<div class="">


														<div class="form-group">
															<label  class="col-sm-3 control-label">Preferred Contact Type</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >s
																	<option selected>Cell Phone</option>
																	<option>Email</option>
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Preferred Mail Type</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >
																	<option selected>Communication</option>
																	<option>Instruction</option>
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Preferred Mobile Alert type</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >
																	<option selected>Cell Phone</option>
																	<option>Email</option>
																</select>
															</div><br>
														</div><br>
														<div ng-repeat="x in selectedWorkFlow.contact.fidPrefPhone_email">
															<div class="form-group">
																<label  class="col-sm-3 control-label">Phone or Email {{$index +1}}</label>
																<div class="col-sm-9 ">
																	<select id="phnEmailOpt2" class="form-control disabledField" onChange="alternatePhnEmailDiv();">
																		<option value="Phone" selected>Phone</option>
																		<option value="Email">Email</option>
																	</select>
																</div><br>
															</div><br>
															<div class="form-group phoneDiv" >
																<label  class="col-sm-3 control-label">Type</label>
																<div class="col-sm-9 ">
																	<select class="form-control disabledField" >
																		<option selected>Cell Phone</option>
																		<option>Land Line</option>
																	</select>
																</div><br>
															</div><br>
															<div class="form-group phoneDiv" >
																<label  class="col-sm-3 control-label">Phone Number</label>
																<div class="col-sm-9 ">
																	<input type="text" class="form-control disabledField"  placeholder="Phone Number">
																</div><br>
															</div><br>

															<div class="form-group emailDiv" style="display:none">
																<label  class="col-sm-3 control-label">Type</label>
																<div class="col-sm-9 ">
																	<select class="form-control disabledField" >
																		<option selected>Communication</option>
																		<option>Fax</option>
																	</select>
																</div><br>
															</div><br>
															<div class="form-group emailDiv" style="display:none">
																<label  class="col-sm-3 control-label">Email ID</label>
																<div class="col-sm-9 ">
																	<input type="text" class="form-control disabledField"  placeholder="Email Id">
																</div><br>
															</div><br>
														</div>


													</div><br>

												</div>

												<!-- /.box-body -->
											</div>

										</div><br>
										<div class="box-footer">
											<button type="submit" ng-click="addPhoneEmail()" class="btn btn-warning pull-right" >Add Phone_Email</button>
										</div>
										<!--<div class="box-footer" style="display:none">-->
										<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
										<!--</div>-->

									</div>
									<!-- /.box-body -->
								</div>
							</div>

							<div class="tab-pane" id="tab_8">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Currency</h3>
									</div>
									<div class="box-body">
										<div class="direct-chat-messages" style="height:260px">
											<div class="box" ng-repeat="x in selectedWorkFlow.fidCrncy">
												<div class="box-header with-border">
													<h3 class="box-title">Currency {{$index+1}}</h3>

													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
															<i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="box-body">
													<div class="form-group">
														<label  class="col-sm-3 control-label">Currency</label>
														<div class="col-sm-9 ">
															<select class="form-control disabledField" >
																<option selected>NGN</option>
																<option>USD</option>
																<option>GBP</option>
																<option>EUR</option>
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Witholding Tax Pcnt</label>
														<div class="col-sm-9 ">
															<input type="text" class="form-control disabledField" id="lName" placeholder="Witholding Tax Pcnt">
														</div><br>
													</div><br>
												</div>
												<!-- /.box-body -->
											</div>

										</div><br>
										<div class="box-footer">
											<button type="submit" ng-click="addCurrency();" class="btn btn-warning pull-right" >Add Currency</button>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
							</div>

							<div class="tab-pane" id="tab_9">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Demographic</h3>
										<div class="box-body">
											<div class="direct-chat-messages" style="height:260px">
												<div class="form-group">
													<label  class="col-sm-3 control-label">Nationality</label>
													<div class="col-sm-9 ">
														<select class="form-control disabledField" >
															<option selected>112</option>
															<option>111</option>
															<option>110</option>
														</select>
													</div><br>
												</div><br>
												<div class="form-group">
													<label  class="col-sm-3 control-label">Marital Status</label>
													<div class="col-sm-9 ">
														<select class="form-control disabledField" >
															<option selected>NGN</option>
															<option>Married</option>
															<option>Single</option>
															<option>Divorced</option>
														</select>
													</div><br>
												</div><br>
												<div class="box" ng-repeat="x in crcyList">
													<div class="box-header with-border">
														<h3 class="box-title">Employment Detail {{$index+1}}</h3>

														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																<i class="fa fa-minus"></i></button>
														</div>
													</div>
													<div class="box-body">
														<div class="form-group">
															<label  class="col-sm-3 control-label">Employment Type</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >
																	<option selected>Chemical</option>
																	<option>Tech</option>
																	<option>Agric</option>
																	<option>IT</option>
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Occupation</label>
															<div class="col-sm-9 ">
																<select class="form-control disabledField" >
																	<option selected>CIVIL</option>
																	<option>ENG</option>
																	<option>BLD</option>
																	<option>DCTR</option>
																</select>
															</div><br>
														</div><br>
													</div>
													<!-- /.box-body -->
												</div>
												<div class="form-group">
													<label  class="col-sm-3 control-label">Employment Type</label>
													<div class="col-sm-9 ">
														<select class="form-control disabledField" >
															<option selected>CIVIL</option>
															<option>BLDNG</option>
															<option>AGRIC</option>
														</select>
													</div><br>
												</div><br>
												<div class="form-group">
													<label  class="col-sm-3 control-label">Gross Income</label>
													<div class="col-sm-9 ">
														<input type="text" class="form-control disabledField"  placeholder="Gross Income">
													</div><br>
												</div><br>

											</div><br>
											<div class="box-footer">
												<button type="submit" ng-click="addCurrency();" class="btn btn-warning pull-right" >Add Employment Details</button>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
								</div>
							</div>

							<div class="tab-pane" id="tab_10">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Trade Finance</h3>
									</div>
									<div class="box-body">
										<div class="direct-chat-messages" style="height:330px">
											<div class="form-group">
												<label  class="col-sm-3 control-label">Inland Trade Allowed</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" >
														<option selected>Y</option>
														<option>N</option>
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Customer Native</label>
												<div class="col-sm-9 ">
													<select class="form-control disabledField" >
														<option selected>Y</option>
														<option>N</option>
													</select>
												</div><br>
											</div><br>

											<!--<div class="box-footer">-->
											<!--<button type="submit" ng-click="addCurrency();" class="btn btn-warning pull-right" >Add Currency</button>-->
											<!--</div>-->
										</div>
										<!-- /.box-body -->
									</div>
								</div>
							</div>
							<button type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'VERIFIED');" class="btn btn-primary">VERIFY</button>
							<button type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'ADVICED')" class="btn btn-danger" >REJECT</button>
							<a href="#" style="padding-left: 2%"><i class="fa fa-clipboard">Add Note</i></a>
							<button type="submit" ng-click="navToUpdateList()" class="btn btn-default pull-right" >Cancel</button>

							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
				</div>

				<div class="col-xs-12 col-md-6" id="Hacm" style="display:none;" >
					<div class="nav-tabs-custom" >
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_11" data-toggle="tab">General</a></li>
							<li><a href="#tab_12" data-toggle="tab">Directoer Related Exposure</a></li>
							<li><a href="#tab_13" data-toggle="tab">MIS Code</a></li>
							<li><a href="#tab_14" data-toggle="tab">Document Details</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_11" >
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Basic Data</h3>
									</div>
									<div class="box-body" >
										<div class="direct-chat-messages" style="height:315px">

											<div class="form-group">
												<label  class="col-sm-3 control-label">Account Number</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.accountid}}" ng-model="acctDetails.retDetails.accountid" readOnly>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">First Name</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.firstName}}" ng-model="acctDetails.retDetails.firstName" >
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Last Name</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.lastName}}" ng-model="acctDetails.retDetails.lastName" placeholder="Last Name">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Middle Name</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.middleName}}" ng-model="acctDetails.retDetails.middleName" placeholder="Middle Name">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">DOB</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.custDob|date}}" placeholder="Date of Birth">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Title</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.title}}" ng-model="acctDetails.retDetails.title" placeholder="Title">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Gender</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.retDetails.gender" ng-model="acctDetails.retDetails.gender" ng-options="option for option in appOptions.GenderOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">BVN</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.custBvn}}" ng-model="acctDetails.retDetails.custBvn" placeholder="BVN">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Segment</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.retDetails.acctSegment" ng-model="acctDetails.retDetails.acctSegment" ng-options="option for option in appOptions.SegmentOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Sub-segment</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.retDetails.acctSubsegment" ng-model="acctDetails.retDetails.acctSubsegment" ng-options="option for option in appOptions.SubSegmentOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Availed Trade</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.retDetails.availedTrade" ng-model="acctDetails.retDetails.availedTrade" ng-options="option for option in appOptions.availedTradeOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Region</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.retDetails.acctRegion" ng-model="acctDetails.retDetails.acctRegion" ng-options="option for option in appOptions.acctRegionOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Preferred Local</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.retDetails.prefLocal" ng-model="acctDetails.retDetails.prefLocal" ng-options="option for option in appOptions.prefLocalOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Enable CRM Alert</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.retDetails.enableCrm" ng-model="acctDetails.retDetails.enableCrm" ng-options="option for option in appOptions.dualOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Enable E-Banking</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.retDetails.enableEbanking" ng-model="acctDetails.retDetails.enableEbanking" ng-options="option for option in appOptions.dualOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Primary Relation ID</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.primaryRelatinId}}" ng-model="acctDetails.retDetails.primaryRelatinId" placeholder="Primary Relation ID">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Secondary Relation ID</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.retDetails.secondaryRelatinId}}" ng-model="acctDetails.retDetails.secondaryRelatinId" placeholder="Secondary Relation ID">
												</div><br>
											</div><br>
										</div><br>
										<!--<div class="box-footer" style="display:block">-->
										<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
										<!--</div>-->
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_12">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Identification Details</h3>
									</div>
									<div class="box-body">
										<div class="direct-chat-messages" style="height:315px">

											<div class="form-group">
												<label  class="col-sm-3 control-label">Document Type</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.fidDocument.docType" ng-model="fetchedCustomerDetails.fidDocument.docType" ng-options="option for option in appOptions.docTypeOptns">
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Document Code</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.fidDocument.docCode" ng-model="fetchedCustomerDetails.fidDocument.docCode" ng-options="option for option in appOptions.docCodeOptns" >
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Unique ID</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" ng-model="acctDetails.fidDocument.docId" value="{{selectedWorkFlow.fidDocument.docId}}" placeholder="Unique ID">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Place of Issue</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.fidDocument.placeOfIssue" ng-model="fetchedCustomerDetails.fidDocument.placeOfIssue" ng-options="option for option in appOptions.acctRegionOptns" >
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Country of Issue</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.fidDocument.cntryOfIssue" ng-model="fetchedCustomerDetails.fidDocument.cntryOfIssue" ng-options="option for option in appOptions.cntryOptns" >
													</select>
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Issue date</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.issueDtae|date}}" placeholder="Issue date">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Expiry date</label>
												<div class="col-sm-9 ">
													<input type="text" class="form-control" value="{{selectedWorkFlow.fidDocument.expiryDate|date}}" placeholder="Expiry date">
												</div><br>
											</div><br>
											<div class="form-group">
												<label  class="col-sm-3 control-label">Is Preferred</label>
												<div class="col-sm-9 ">
													<select class="form-control" ng-init="selectedWorkFlow.fidDocument.isPreferred" ng-model="fetchedCustomerDetails.fidDocument.isPreferred" ng-options="option for option in appOptions.dualOptns" >
													</select>
												</div><br>
											</div><br>
										</div><br>
										<!--<div class="box-footer" style="display:block">-->
										<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
										<!--</div>-->
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<!-- /.tab-pane -->
							<div class="tab-pane" id="tab_13">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Contact</h3>
									</div>
									<div class="box-body">

										<div class="direct-chat-messages" style="height:260px">
											<div class="box" >
												<div class="box-header with-border">
													<h3 class="box-title">Address</h3>

													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
															<i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="box-body">
													<div class="">

														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Format</label>
															<div class="col-sm-9 ">
																<select class="form-control" ng-init="selectedWorkFlow.contact.fidAddress.addressFormat" ng-model="acctDetails.contact.fidAddress.addressFormat" ng-options="option for option in appOptions.addressFormatOptns" >
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Type</label>
															<div class="col-sm-9 ">
																<select class="form-control" ng-init="selectedWorkFlow.contact.fidAddress.addressType" ng-model="acctDetails.contact.fidAddress.addressType" ng-options="option for option in appOptions.addressTypeOptns" >
																</select>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Label</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control" ng-model="acctDetails.contact.fidAddress.addressLabel" value="{{selectedWorkFlow.contact.fidAddress.addressLabel}}" placeholder="Address label">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Address Line 1</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control" ng-model="acctDetails.contact.fidAddress.addressLine1" value="{{selectedWorkFlow.contact.fidAddress.addressLine1}}" placeholder="Address Line">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Local Govt.</label>
															<div class="col-sm-7">
																<select class="form-control" ng-init="selectedWorkFlow.contact.fidAddress.localGovt" ng-model="acctDetails.contact.fidAddress.localGovt" ng-options="option for option in appOptions.acctRegionOptns" >
																</select>
															</div>
															<div class="col-sm-2 ">
                                                                <span class="input-group-btn">
                                                                    <button type="submit" ng-click="searchforAcct();" name="search" id="search-btn3" class="btn btn-flat bg-green">
                                                                    <i class="fa fa-search"></i>
                                                                    </button>
                                                                </span>
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">State</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.state}}" ng-model="acctDetails.contact.fidAddress.state" placeholder="State">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">Country</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.country}}" ng-model="acctDetails.contact.fidAddress.country" placeholder="Country">
															</div><br>
														</div><br>
														<div class="form-group">
															<label  class="col-sm-3 control-label">City</label>
															<div class="col-sm-9 ">
																<input type="text" class="form-control" value="{{selectedWorkFlow.contact.fidAddress.city}}" ng-model="acctDetails.contact.fidAddress.city" placeholder="City">
															</div><br>
														</div><br>
													</div>
													<br>
													<!--<div class="box-footer" style="display:block">-->
													<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
													<!--</div>-->
												</div>
												<!-- /.box-body -->
											</div>

											<div class="box" >
												<div class="box" >
													<div class="box-header with-border">
														<h3 class="box-title">Phone and Email</h3>

														<div class="box-tools pull-right">
															<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
																<i class="fa fa-minus"></i></button>
														</div>
													</div>
													<div class="box-body">
														<div class="">
															<label  class="control-label">Preferred Contact Type</label>
															<select class="form-control" ng-init="selectedWorkFlow.contact.fidPrefPhone_email.prefContactType" ng-model="acctDetails.contact.fidPrefPhone_email.prefContactType" ng-options="option for option in appOptions.prefContactType">
															</select><br>
															<label  class="control-label">Preferred Mail Type</label>
															<select class="form-control" ng-init="selectedWorkFlow.contact.fidPrefPhone_email.prefMailType" ng-model="acctDetails.contact.fidPrefPhone_email.prefMailType" ng-options="option for option in appOptions.prefMailType">
															</select><br>
															<label  class="control-label">Preferred Mobile Alert Type</label>
															<select class="form-control" ng-init="selectedWorkFlow.contact.fidPrefPhone_email.prefMobAlertType" ng-model="acctDetails.contact.fidPrefPhone_email.prefMobAlertType" ng-options="option for option in appOptions.prefMobAlertType">
															</select><br>
															<div ng-repeat="x in selectedWorkFlow.contact.fidPhoneEmails">
																<label  class="control-label">Phone or Email {{$index +1}}</label>
																<select class="form-control" ng-init="x.contactBy" ng-model="x.contactBy" ng-options="option for option in appOptions.phoneEmailOptns">
																</select><br>
																<div ng-if="x.contactBy == 'PHONE'">
																	<label  class="control-label">Type</label>
																	<select class="form-control" ng-init="x.contactType" ng-model="x.contactType" ng-options="option for option in appOptions.prefMobAlertType">
																	</select><br>
																	<label  class="control-label">Phone Number</label>
																	<div class="">
																		<input type="text" class="form-control"  value="{{x.contactId}}" ng-model="x.contactId" placeholder="Phone Number">
																	</div><br>
																</div>
																<div ng-if="x.contactBy == 'EMAIL'">
																	<label  class="control-label">Type</label>
																	<select class="form-control" ng-init="x.contactType" ng-model="x.contactType" ng-options="option for option in appOptions.prefMailType">
																	</select><br>
																	<label  class="control-label">Email ID</label>
																	<div class="">
																		<input type="text" class="form-control"  value="{{x.contactId}}" ng-model="x.contactId" placeholder="Email ID">
																	</div><br>
																</div>
															</div>
														</div><br>

													</div>

													<!-- /.box-body -->
												</div>

												<!-- /.box-body -->
											</div>

										</div><br>
										<div class="box-footer">
											<button type="submit" ng-click="addPhoneEmail()" class="btn btn-warning pull-right" >Add Phone_Email</button>
										</div>
										<!--<div class="box-footer" style="display:none">-->
										<!--<button type="submit" class="btn btn-warning pull-right">Validate</button>-->
										<!--</div>-->

									</div>
									<!-- /.box-body -->
								</div>
							</div>

							<div class="tab-pane" id="tab_14">
								<div class="box" >
									<div class="box-header with-border">
										<h3 class="box-title">Currency</h3>
									</div>
									<div class="box-body">
										<div class="direct-chat-messages" style="height:260px">
											<div class="box" ng-repeat="x in selectedWorkFlow.fidCrncy">
												<div class="box-header with-border">
													<h3 class="box-title">Currency {{$index+1}}</h3>

													<div class="box-tools pull-right">
														<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
															<i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="box-body">
													<div class="form-group">
														<label  class="col-sm-3 control-label">Currency</label>
														<div class="col-sm-9 ">
															<select class="form-control" ng-init="x.crncy" ng-model="x.crncy" ng-options="option for option in appOptions.crncyOptns">
															</select>
														</div><br>
													</div><br>
													<div class="form-group">
														<label  class="col-sm-3 control-label">Witholding Tax Pcnt</label>
														<div class="col-sm-9 ">
															<input type="text" ng-model="x.withTax" value="{{x.withTax}}" class="form-control"  placeholder="Witholding Tax Pcnt">
														</div><br>
													</div><br>
												</div>
												<!-- /.box-body -->
											</div>

										</div><br>
										<div class="box-footer">
											<button type="submit" ng-click="addCurrency();" class="btn btn-warning pull-right" >Add Currency</button>
										</div>
									</div>
									<!-- /.box-body -->
								</div>
							</div>
							<button ng-if="selectedWorkFlow.retDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'UPDATED');" class="btn btn-primary">Update</button>
							<button ng-if="selectedWorkFlow.retDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.retDetails,'REJECTED')" class="btn btn-danger" >Reject</button>
							<button ng-if="selectedWorkFlow.corpDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.corpDetails,'UPDATED');" class="btn btn-primary">Update</button>
							<button ng-if="selectedWorkFlow.corpDetails !=null" type="submit" ng-click="updateDocument(selectedWorkFlow.corpDetails,'REJECTED')" class="btn btn-danger" >Reject</button>
							<a href="#" style="padding-left: 2%"><i class="fa fa-clipboard">Add Note</i></a>
							<button type="submit" ng-click="navToUpdateList()" class="btn btn-default pull-right" >Cancel</button>

							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
				</div>
				<!-- /.col -->
				<div class="col-xs-12 col-md-6" id="docMntDiv">
					<div id="pdfBox" class="box " style="display:none" >
						<div class="box-header with-border">
							<h3 class="box-title">Uploaded Document: </h3>
							<small id="docTitle1"></small>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div style="height:370px">
								<embed width="100%" height="100%" name="plugin" id = "docName1" src="" type="application/pdf" internalinstanceid="4">
							</div>
						</div>
					</div>
					<div id="imgBox" class="box "  style="display:none">
						<div class="box-header with-border">
							<h3 class="box-title">Uploaded Document: </h3>
							<small id="docTitle2"></small>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
									<i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div style="height:370px">
								<!--<embed width="100%" height="100%" name="plugin" id = "docName2" src=" " type="application/pdf" internalinstanceid="4">-->
								<img  width="100%" height="100%" id = "docName2" src="">
							</div>
						</div>
						<!-- /.box-body -->
					</div>
					<div id="rejectDiv" class="form-group" style="display:none">
						<label>Reject To:</label>
						<select class="rejLabel" id="rejectOption" onChange="checkOption();">
							<option value="">--Select--</option>
							<option value="BRANCH">Branch</option>
							<option value="INPUTER">Inputer</option>
						</select>
						<label class="bg-red"  id="rejAlert">Please Select</label>
						<textarea id="rejectReason" class="form-control " rows="3" placeholder="Type Here" enabled=""></textarea>
					</div>
				</div>
				<!-- /.col -->

			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->

	</div>
	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 1
		</div>
		<strong>Copyright &copy; 2016 <a href="http://longbridgetechnology.com">Longbridge Technology</a>.</strong>
	</footer>

	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Create the tabs -->
		<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
			<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
			<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">

		</div>
	</aside>
	<!-- /.control-sidebar -->
	<!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
	<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<html xmlns:th="http://www.thymeleaf.org">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Fidelity CPC</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
<link rel="icon" href="/branchconsole/Fidelity-Icon.png">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
<script src="/angular/angular.min.js"></script>
<script src="/angular/controller.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-app="angularApp" ng-controller="reviewCntrl">
<script>
    function checkOption(){
        var optionSelected = document.getElementById("rejectOption").value;
        if(optionSelected!=""){
            $("#rejAlert").hide();;
        }else{
            $("#rejAlert").show();;
        }
    }
    function signOut(){
        document.getElementById("logout").click();
    }
</script>
<div th:if="${logtoken} != 'Y'">
    <script>
        window.location("/login");
    </script>
</div>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b><img src="/branchconsole/Fidelity-Icon.png" alt="App Logo" style="margin-top: 0px; width:42px; height: 42px"></b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="/branchconsole/Fidelity-Icon.png" alt="App Logo" style="margin-top: 0px; width:42px; height: 42px">&nbsp; <b>Fidelity  </b>CPC</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav"><!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">USER 1</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    User 1 - CSO
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="profile" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="#" onclick="signOut();" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <%--<        class="sidebar">--%>
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <section>
            <ul class="sidebar-menu"><br>
                <li class="header">ACCOUNT OPERATIONS</li>
                <b id="csoUser" style="display: none"><c:out  value="${pageContext.request.remoteUser}"></c:out></b>
                <%--<sec:authorize access="hasRole('REVIEWER') or hasRole('ADMIN')">--%>
                <%--<li>--%>
                    <%--<a href="review">--%>
                        <%--<i class="fa fa-pencil-square-o"></i> <span>Review</span>--%>
                    <%--</a>--%>
                <%--</li>--%>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('INPUTER') or hasRole('ADMIN')">--%>
                <li>
                    <a href="update">
                        <i class="fa fa-pencil-square-o"></i> <span>Update</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-red">3</small>
                        </span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('VERIFIER') or hasRole('ADMIN')">--%>
                <li>
                    <a href="verify">
                        <i class="fa fa-check-square"></i> <span>Verify</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-blue">17</small>
                        </span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--&lt;%&ndash;<sec:authorize access="hasRole('VISITOR')">&ndash;%&gt;--%>
                <li>
                    <a href="report">
                        <i class="fa fa-bar-chart"></i> <span>Reports</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('ADMIN')">--%>
                <li>
                    <a href="umr">
                        <i class="fa fa-user-plus"></i> <span>User Management</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('CSO')  or hasRole('ADMIN')">--%>
                <li>
                    <a href="docmgmnt">
                        <i class="fa fa-file-pdf-o"></i> <span>CSO Operations</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>

            </ul>
        </section>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Document Review
            </h1>
        </section>

        <!-- Main content -->
        <section class="content" id="csoDocList">
            <div class="row">
                <div class="col-xs-12">

                    <!-- /.box -->

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Account Number</th>
                                    <th>Account Open Date</th>
                                    <th>Document Name</th>
                                    <th>Date Uploaded</th>
                                    <th>Sol ID</th>
                                    <th>Sol Name</th>
                                    <th>Modifier</th>
                                    <th>Date Modified</th>
                                    <th>Verifier</th>
                                    <th>Date Submitted</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="x in workFlowforReviewer">
                                    <td>{{x.acctNumber}}</td>
                                    <th>23-may-2017</th>
                                    <td>{{x.docId}}</td>
                                    <th>23-may-2017</th>
                                    <th>001</th>
                                    <th>lekki</th>
                                    <td>{{x.docmntInputter}}</td>
                                    <th>23-may-2017</th>
                                    <td>{{x.acctVerifier}}</td>
                                    <th>23-may-2017</th>
                                    <td ng-if = "x.acctStatus == 'UPDATED'"><span class="label label-warning">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'VERIFIED'"><span class="label label-success">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'DEFAULT'"><span class="label label-default">{{x.acctStatus}}</span></td>
                                    <td ng-if = "x.acctStatus == 'DEFAULT'"><a href="#acctReview" ng-click="showAcctDetails(x.acctNumber,x.docId)" class="btn btn-primary">View</a></td>
                                    <td ng-if = "x.acctStatus == 'UPDATED'"><a href="#csoDocList" onClick="alert('You cannot Review Document, Already Updated')" class="btn btn-default" disabled>View</a></td>
                                    <td ng-if = "x.acctStatus == 'VERIFIED'"><a href="#csoDocList" onClick="alert('You cannot Review Document, Already Verified')" class="btn btn-default" disabled>View</a></td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
        <!-- /.content -->
        <section class="content" id="acctReview" style="display:none" >
            <div class="row">
                <div class="col-xs-12 col-md-6">

                    <!-- /.box -->

                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Basic Information</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="" style="height:370px">

                                <div id="acctDataBox" style="display:block">
                                    <div class="">
                                        <label  class="control-label col-md-4">Account Number</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{selectedWorkFlow.accountid}}" readonly >
                                        </div><br>
                                    </div><br>
                                    <div ng-if="selectedWorkFlow.acctType != 'RETAIL'" class="">
                                        <label  class="control-label col-md-4">Account Name</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{selectedWorkFlow.accountName}}" readonly >
                                        </div><br>
                                    </div>
                                    <div ng-if="selectedWorkFlow.acctType == 'RETAIL'" class="">
                                        <label  class="control-label col-md-4">Account Name</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{selectedWorkFlow.firstName+' '+selectedWorkFlow.middleName+' '+selectedWorkFlow.lastName}}" readonly >
                                        </div><br>
                                    </div><br>
                                    <div class="">
                                        <label  class="control-label col-md-4">Phone Number</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{selectedWorkFlow.phoneNum}}" readonly >
                                        </div><br>
                                    </div><br>
                                    <div class="">
                                        <label  class="control-label col-md-4">Email ID</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{selectedWorkFlow.emailId}}" readonly >
                                        </div><br>
                                    </div><br>
                                    <div ng-if="selectedWorkFlow.acctType == 'RETAIL' " class="">
                                        <label  class="control-label col-md-4">DOB</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{selectedWorkFlow.custDob|date}}" readonly >
                                        </div><br>
                                    </div>
                                    <div ng-if="selectedWorkFlow.acctType != 'RETAIL' " class="">
                                        <label  class="control-label col-md-4">Incorporation Date</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{selectedWorkFlow.incorpDate|date}}" readonly >
                                        </div><br>
                                    </div><br>
                                    <div class="">
                                        <label  class="control-label col-md-4">Addess</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" value="{{selectedWorkFlow.addressLine1}}" readonly >
                                        </div><br>
                                    </div><br>
                                </div><br><br>

                                <div id="rejectDiv" class="form-group" style="display:none">
                                    <label>Reject Option</label>
                                    <select id="rejectOption" onChange="checkOption();">
                                        <option value="">--Select--</option>
                                        <option value="D">Delete</option>
                                        <option value="M">Modify</option>
                                    </select>
                                    <label class="bg-red"  id="rejAlert">Please Select Type</label>
                                    <br>
                                    <textarea id="rejectReason" class="form-control" rows="3" placeholder="Enter Reason For Rejection" enabled=""></textarea>
                                </div>

                            </div><br>
                            <div>
                                <button type="submit" ng-click="reviewDocument(selectedWorkFlow.accountid,'REVIEWED')" class="btn btn-primary">Submit</button>
                                <button type="submit" ng-click="reviewDocument(selectedWorkFlow.accountid,'REJECTED')" class="btn btn-danger">Reject</button>
                                <button type="submit" ng-click="navToCsoDocList()" class="btn btn-default pull-right" >Cancel</button>
                            </div>






                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-xs-12 col-md-6">

                    <!-- /.box -->

                    <div id="pdfBox" class="box " style="display:none">
                        <div class="box-header with-border">
                            <h3 class="box-title">Uploaded Document: </h3>
                            <small id="docTitle1"></small>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div style="height:420px">
                                <embed width="100%" height="100%" name="plugin" id = "docName1" src="" type="application/pdf" internalinstanceid="4">
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div id="imgBox" class="box "  style="display:none">
                        <div class="box-header with-border">
                            <h3 class="box-title">Uploaded Document: </h3>
                            <small id="docTitle2"></small>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div style="height:420px">
                                <!--<embed width="100%" height="100%" name="plugin" id = "docName2" src=" " type="application/pdf" internalinstanceid="4">-->
                                <img  width="100%" height="100%" id = "docName2" src=" ">
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <!-- /.col -->

            </div>

        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1
        </div>
        <strong>Copyright &copy; 2016 <a href="http://longbridgetechnology.com">Longbridge Technology</a>.</strong>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">

        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Fidelity| CPC</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
    <link rel="icon" href="/branchconsole/Fidelity-Icon.png">
    <script src="/angular/angular.min.js"></script>
    <script src="/angular/controller.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-app="angularApp" ng-controller="umrCntrl">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b><img src="/branchconsole/icon.png"  style="margin-top: 0px; width:42px; height: 42px"></b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="/branchconsole/icon.png"  style="margin-top: 0px; width:42px; height: 42px">&nbsp; <b>Fidelity  </b>CPC</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav"><!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs">USER 1</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    User 1 - CSO
                                    <small>Member since Nov. 2012</small>
                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="profile" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="FidelityCPC" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <%--<        class="sidebar">--%>
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <section>
            <ul class="sidebar-menu"><br>
                <li class="header">ACCOUNT OPERATIONS</li>
                <b id="csoUser" style="display: none"><c:out  value="${pageContext.request.remoteUser}"></c:out></b>
                <%--<sec:authorize access="hasRole('REVIEWER') or hasRole('ADMIN')">--%>
                <%--<li>--%>
                    <%--<a href="review">--%>
                        <%--<i class="fa fa-pencil-square-o"></i> <span>Review</span>--%>
                    <%--</a>--%>
                <%--</li>--%>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('INPUTER') or hasRole('ADMIN')">--%>
                <li>
                    <a href="update">
                        <i class="fa fa-pencil-square-o"></i> <span>Update</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-red">3</small>
                        </span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('VERIFIER') or hasRole('ADMIN')">--%>
                <li>
                    <a href="verify">
                        <i class="fa fa-check-square"></i> <span>Verify</span>
                        <span class="pull-right-container">
                          <small class="label pull-right bg-blue">17</small>
                        </span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--&lt;%&ndash;<sec:authorize access="hasRole('VISITOR')">&ndash;%&gt;--%>
                <li>
                    <a href="report">
                        <i class="fa fa-bar-chart"></i> <span>Reports</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('ADMIN')">--%>
                <li>
                    <a href="umr">
                        <i class="fa fa-user-plus"></i> <span>User Management</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>
                <%--<sec:authorize access="hasRole('CSO')  or hasRole('ADMIN')">--%>
                <li>
                    <a href="docmgmnt">
                        <i class="fa fa-file-pdf-o"></i> <span>CSO Operations</span>
                    </a>
                </li>
                <%--</sec:authorize>--%>

            </ul>
        </section>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Management
            </h1>
        </section>

        <!-- Main content -->
        <section class="content" id="umrListView">
            <div class="row">
                <div class="col-xs-12">

                    <!-- /.box -->

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>User Id</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>User role</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="x in CPCUserList">
                                    <td>{{$index + 1}}</td>
                                    <td>{{x.userName}}</td>
                                    <td>{{x.userEmail}}</td>
                                    <td>{{x.userRole}}</td>
                                    <td>{{x.status}}</td>
                                    <td><a href="#viewUserData" ng-click="viewUserProfile(x)" class="btn btn-primary">View</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="#addUserView" ng-click="navToAddUserView()" class="btn btn-primary">Add User</a>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
        <section class="content" id="viewUserData" style="display:none">
            <div class="">
                <!-- Default box -->
                <div class="box box-primary" >
                    <div>
                        <div class="box-body">
                            <form action="#" method="get" class="primary " style="display:none">
                                <div class="input-group col-md-6 ">
                                    <input type="text" name="q" class="form-control " placeholder="Enter Username" style="border-radius:3px" required>
                                    <span class="input-group-btn">
								<button type="submit" name="search" id="search-btn" class="btn btn-flat bg-green"><i class="fa fa-search"></i>
                                </button>
							  </span>
                                </div>
                            </form><br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" value="{{CPCUserProfile.userName}}" readOnly>
                                <span class="input-group-addon">User Name</span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="email" class="form-control" value="{{CPCUserProfile.userEmail}}" readOnly >
                                <span class="input-group-addon">Email</span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input type="password" class="form-control" value="{{CPCUserProfile.pass}}" readOnly >
                                <span class="input-group-addon">Password</span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="pass" class="form-control" value="{{CPCUserProfile.dateCreated|date}}" readOnly>
                                <span class="input-group-addon">Date Created</span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-check-square"></i></span>
                                <input type="pass" class="form-control" value="{{CPCUserProfile.status}}" readOnly>
                                <span class="input-group-addon">Status</span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-tasks"></i></span>
                                <input type="text" class="form-control" value="{{CPCUserProfile.userRole}}" readOnly>
                                <span class="input-group-addon">User Role</span>
                            </div><br>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">

                            <button type="submit" ng-click="navToModifyUserData()" class="btn bg-green">Modify</button>
                            <button type="submit" ng-click="deleteUserData(CPCUserProfile)" class="btn btn-danger">Delete</button>
                            <button type="submit" ng-click="navToUmrListView()" class="btn  btn-default pull-right">Cancel</button>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <!-- /.box-footer-->
                </div>
            </div>
            <!-- /.box -->

        </section>

        <section class="content" id="addUserView" style="display:none">
            <div class="">
                <!-- Default box -->
                <div class="box box-primary" >
                    <div>
                        <div class="box-body">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" ng-model="CPCNewUser['accntId']" >
                                <span class="input-group-addon">Name</span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="email" class="form-control" ng-model="CPCNewUser['email']">
                                <span class="input-group-addon">Email</span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input type="password" class="form-control" ng-model="CPCNewUser['pass']">
                                <span class="input-group-addon">Password</span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input type="password" class="form-control" ng-model="CPCNewUser['passWord2']">
                                <span class="input-group-addon">Re-Type Password</span>
                            </div><br>
                            <div class="form-group ">
                                <label class="">CPC Role</label>
                                <select class="form-control  select2 select2-hidden-accessible" ng-model="CPCNewUser['userRole']" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    <option value="">--Select--</option>
                                    <option value="CSO">CSO</option>
                                    <%--<option value="REVIEWER">Reviewer</option>--%>
                                    <option value="MODIFIER">Modifier</option>
                                    <option value="VERIFIER">Verifier</option>
                                </select>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">

                            <button type="submit" ng-click="saveUserDetails(CPCNewUser)" class="btn bg-green">Save</button>
                            <button type="submit" ng-click="navToUmrListView()" class="btn  btn-default">Cancel</button>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <!-- /.box-footer-->
                </div>
            </div>
            <!-- /.box -->

        </section>

        <section class="content" id="modifyUserData" style="display:none">
            <div class="">
                <!-- Default box -->
                <div class="box box-primary" >
                    <div>

                        <div class="box-body">
                            <div class="input-group">
                                <span class="input-group-addon">Name</span>
                                <input type="text" class="form-control" ng-model="CPCUserProfile.userName"  value="{{CPCUserProfile.accntId}}" required>
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon">Email</span>
                                <input type="text" class="form-control" ng-model="CPCUserProfile.userEmail" value="{{CPCUserProfile.userEmail}}" required>
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div><br>
                            <div class="input-group">
                                <span class="input-group-addon">Status</span>
                                <input type="email" class="form-control" ng-model="CPCUserProfile.status" value="{{CPCUserProfile.status}}" required>
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            </div><br>
                            <div class="input-group initPassword">
                                <span class="input-group-addon">Password</span>
                                <input type="password" class="form-control" value="{{CPCUserProfile.pass}}" readonly>
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <span class="input-group-addon"><a href="#" ng-click="modifyPassword()">Change</a></span>
                            </div><br>
                            <div class="input-group modPassword" style="display:none">
                                <span class="input-group-addon">New Password</span>
                                <input type="password" class="form-control" ng-model="CPCUserProfile.pass" ng-init="" required>
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            </div><br>
                            <div class="input-group modPassword" style="display:none">
                                <span class="input-group-addon">Re-Type Password</span>
                                <input type="password" class="form-control" value="" required>
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            </div><br>
                            <div class="form-group ">
                                <label class="">CPC Role</label>
                                <select class="form-control  select2 select2-hidden-accessible" ng-init="CPCUserProfile.userRole" ng-model="CPCUserProfile.userRole" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    <option value="CSO">CSO</option>
                                    <%--<option value="REVIEWER">Reviewer</option>--%>
                                    <option value="MODIFIER">Modifier</option>
                                    <option value="VERIFIER">Verifier</option>
                                    <option value="ADMIN">Admin</option>
                                </select>
                            </div><br>
                        </div>
                        <div class="box-footer">
                            <button type="submit" ng-click="updateUserDetails(CPCUserProfile)" class="btn bg-green">Save</button>
                            <button type="submit" ng-click="navToViewUserData()" class="btn  btn-default">Cancel</button>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <!-- /.box-footer-->
                </div>
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.12
        </div>
        <strong>Copyright &copy; 2016 <a href="http://longbridgetechnology.com">Longbridge Technology</a>.</strong>
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">

        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
</body>
</html>

package com.longbridge.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by LB-PRJ-020 on 9/20/2017.
 */
@Component
@ConfigurationProperties(prefix="foo")
public class FiUrl {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}

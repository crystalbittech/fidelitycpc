
package com.longbridge.FiRetCustInq;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.longbridge.FiRetCustInq package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.longbridge.FiRetCustInq
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetCustInqResponse }
     * 
     */
    public RetCustInqResponse createRetCustInqResponse() {
        return new RetCustInqResponse();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs }
     * 
     */
    public RetCustInqResponse.RetCustInqRs createRetCustInqResponseRetCustInqRs() {
        return new RetCustInqResponse.RetCustInqRs();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs.RetCustDtls }
     * 
     */
    public RetCustInqResponse.RetCustInqRs.RetCustDtls createRetCustInqResponseRetCustInqRsRetCustDtls() {
        return new RetCustInqResponse.RetCustInqRs.RetCustDtls();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs.DemographicDtls }
     * 
     */
    public RetCustInqResponse.RetCustInqRs.DemographicDtls createRetCustInqResponseRetCustInqRsDemographicDtls() {
        return new RetCustInqResponse.RetCustInqRs.DemographicDtls();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs.EntityDocDtls }
     * 
     */
    public RetCustInqResponse.RetCustInqRs.EntityDocDtls createRetCustInqResponseRetCustInqRsEntityDocDtls() {
        return new RetCustInqResponse.RetCustInqRs.EntityDocDtls();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs.PsychographicDtls }
     * 
     */
    public RetCustInqResponse.RetCustInqRs.PsychographicDtls createRetCustInqResponseRetCustInqRsPsychographicDtls() {
        return new RetCustInqResponse.RetCustInqRs.PsychographicDtls();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs.RelBankDtls }
     * 
     */
    public RetCustInqResponse.RetCustInqRs.RelBankDtls createRetCustInqResponseRetCustInqRsRelBankDtls() {
        return new RetCustInqResponse.RetCustInqRs.RelBankDtls();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs.TradeFinDtls }
     * 
     */
    public RetCustInqResponse.RetCustInqRs.TradeFinDtls createRetCustInqResponseRetCustInqRsTradeFinDtls() {
        return new RetCustInqResponse.RetCustInqRs.TradeFinDtls();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs.RetailBaselDtls }
     * 
     */
    public RetCustInqResponse.RetCustInqRs.RetailBaselDtls createRetCustInqResponseRetCustInqRsRetailBaselDtls() {
        return new RetCustInqResponse.RetCustInqRs.RetailBaselDtls();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs.RetCustDtls.PhoneEmailInfo }
     * 
     */
    public RetCustInqResponse.RetCustInqRs.RetCustDtls.PhoneEmailInfo createRetCustInqResponseRetCustInqRsRetCustDtlsPhoneEmailInfo() {
        return new RetCustInqResponse.RetCustInqRs.RetCustDtls.PhoneEmailInfo();
    }

    /**
     * Create an instance of {@link RetCustInqResponse.RetCustInqRs.RetCustDtls.RetCustAddrInfo }
     * 
     */
    public RetCustInqResponse.RetCustInqRs.RetCustDtls.RetCustAddrInfo createRetCustInqResponseRetCustInqRsRetCustDtlsRetCustAddrInfo() {
        return new RetCustInqResponse.RetCustInqRs.RetCustDtls.RetCustAddrInfo();
    }

}

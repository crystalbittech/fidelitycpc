
package com.longbridge.FiRetCustInq;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetCustInqRs">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RetCustDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AnnualRevenue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Assistant" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AvailableCrLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BlacklistNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BlacklistReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsBlacklisted" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CardHolder" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ChargeLevelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CombinedStmtFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ConstitutionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ConstitutionRefCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CorpRepCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CountryOfBirth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CreatedFrom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CreatedBySystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CurrentCrExposure" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CommunityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Community" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BirthDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FirstNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FirstNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Health" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HealthCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LastNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LastNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MiddleNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MiddleNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsStaff" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SwiftCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsMinor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsNRE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustProfitability" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustRelNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustTrade" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusChangeDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DelinquencyFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DeliquencyPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Designation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsDocReceived" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DualFirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DualLastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DualMiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Education" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EntityCreationFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExtnNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FatherOrHusbandName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FaxHome" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GroupIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HouseholdName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IncrementalDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="InternalScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SalutationCodeOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NameOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SalutationOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsSwiftCodeOfBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsCorpRepresentative" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsDummy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsEBankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsSMSBankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsWAPBankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LeadSource" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LicenseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MaidenName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MaidenNameOfMother" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Manager" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ManagerOpinion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GuardianCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GuardianName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ModifiedBySysId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MotherName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NameSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NationalIdCardNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NativeLanguage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NativeLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NativeLanguageName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NativeLanguageTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NativeLanguageTitleCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsNegated" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NegationNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NegationReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NickName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Occupation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OfflineCumDrLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PAN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PassportNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PersonType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PhoneEmailInfo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="PhoneEmailInfo">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value=""/>
 *                                             &lt;enumeration value="noemail@fcmb.com"/>
 *                                             &lt;enumeration value="felixgrammar@yahoo.com"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="EmailPalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PhoneEmailType">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="COMM1"/>
 *                                             &lt;enumeration value="WORK1"/>
 *                                             &lt;enumeration value="COMML"/>
 *                                             &lt;enumeration value="WORKL"/>
 *                                             &lt;enumeration value="HOMEL"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="PhoneNum">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="+234(0)08052003607"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="PhoneNumCityCode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="0"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="PhoneNumCountryCode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="234"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="PhoneNumLocalCode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="08052003607"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="PhoneOrEmail">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="PHONE"/>
 *                                             &lt;enumeration value="EMAIL"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="PrefFlag">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="Y"/>
 *                                             &lt;enumeration value="N"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="WorkExtnNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PhoneEmailID">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="31685464"/>
 *                                             &lt;enumeration value="31685465"/>
 *                                             &lt;enumeration value="31685466"/>
 *                                             &lt;enumeration value="31685467"/>
 *                                             &lt;enumeration value="31685468"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="PlaceOfBirth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PotentialCrLine" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefCodeRefCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PreviousName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrimaryServiceCentre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrimarySolId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PriorityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProofOfAgeDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProofOfAgeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PassportDtls" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PassportExpDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Rating" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RatingCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RatingDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RelationshipLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RelationshipType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RelationshipOpeningDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RelationshipValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RetCustAddrInfo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="AddrLine1">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="NO ADDRESS IN FIN7"/>
 *                                             &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="AddrLine2">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value=""/>
 *                                             &lt;enumeration value="WARRI"/>
 *                                             &lt;enumeration value="."/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="AddrLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="AddrStartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="AddrCategory">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="Home"/>
 *                                             &lt;enumeration value="Mailing"/>
 *                                             &lt;enumeration value="Work"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="BuildingLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="BusinessCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CellNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="City">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="LAG"/>
 *                                             &lt;enumeration value="WAR"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="CityCode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="LAG"/>
 *                                             &lt;enumeration value="WAR"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="EndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="FaxNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="FaxNumCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="FaxNumCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="FaxNumLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="HoldMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="HoldMailInitiatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="HoldMailReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="HouseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="LocalityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="MailStop" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PagerNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PagerNumCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PagerNumCcountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PagerNumLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PhoneNum1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PrefAddr">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="N"/>
 *                                             &lt;enumeration value="Y"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="PrefFormat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PremiseName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ResidentialStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="State">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="LG"/>
 *                                             &lt;enumeration value="DL"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="StateCode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="LG"/>
 *                                             &lt;enumeration value="DL"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="StreetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="StreetNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="TelexCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="TelexCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="TelexLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Town" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="isAddressVerified" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="AddressID">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="4091277"/>
 *                                             &lt;enumeration value="4091275"/>
 *                                             &lt;enumeration value="4091276"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="RevenueUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Salutation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SegmentationClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ShortNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ShortNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SICCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SMSBankingMobNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SSN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StaffFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubSector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubSegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SuspendNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SuspendReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsSuspended" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TFPartyFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TickerSymbol" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TotalCrExposure" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PassportIssueDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ForeignAccTaxReportingReq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ForeignTaxReportingCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ForeignTaxReportingStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FatcaRemarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SenCitizenApplicableDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SeniorCitizen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PhysicalState" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AadhaarNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StaffEmployeeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="DemographicDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="DonotCallFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DonotMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DonotSendEMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NameOfEmployer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EmploymentStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MaritalStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MaritalStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NationalityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefContactTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefDayTimeContactNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefDayTimeContactNumArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefDayTimeContactNumCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefDayTimeContactNumLocal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ResidenceCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ResidenceCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Tax_Rate_Table_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="EntityDocDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DocCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsDocDeleted" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DocExpDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DocIssueDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DocRmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DocTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DocTypeDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IdentificationType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PlaceOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RefNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PsychographicDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="DespatchMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HouseHoldNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefAddrMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefRep" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrefName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RiskBehaviour" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SegmentationClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StmtFreq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StmtType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StmtDtWeekDay" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StmtDateWeekDay" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StmtWeekOfMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="External_System_Pricing" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Relationship_Pricing_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RelBankDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="EntityCreationFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NumOfCrCards" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TradeFinDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CautionStat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CentralBankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ContractLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CountryDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CorpKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CreatedFrom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CurrDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DCMmarginPercentage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DCNextNumCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DCNextNumCodeRCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DCSanctionIngAuth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsDeleted" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EntityCreationFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpImpInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FaxNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FCSanctionIngAuth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HundredPercentFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IndividualCorpFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="InlandTradeAllowed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LeasingLiabilities" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OrgKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PartyConst" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PartyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PhoneCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PhoneLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProductionCycle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SpecialCustFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SSIFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StateDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TradeAuthorityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RetailBaselDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="retBaselID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DebtHELOC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CurrFICOScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TotalDSR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BusinessTotalScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HasRelationship" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CombinedDSR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BusinessDSR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CashAssetRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DebtWorthRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="InterestTaxRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GeneralQuickRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ScoredSICCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OwnerYears" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NetWorth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DDABal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CombinedDebtRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProposedLeverage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GLBLCashCoverage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CurrentRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LiquidityRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MinTangibleWorth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FICOScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FinInqCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FinTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="InquiryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MinorDerogatoryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MajorDerogatoryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NeverPastDueCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OpenTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HighestCreditLmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TradeCntThirty" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TradeCntSixty" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TradeCntNinety" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RevolveDebtPrcnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SatisfactoryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="InstLoanBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="InstLoanCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MortgageBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MortgageTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OtherTradeBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OtherTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RevolveBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RevolveTradeLmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RevolveTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CCLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsWorst" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpenseIncomeRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SgmntPoolID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PoolPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PoolLGD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PoolEAD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LGD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EAD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ModelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ModelVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ModelResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsFailed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RetCustInq_CustomData" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retCustInqRs",
    "retCustInqCustomData"
})
@XmlRootElement(name = "RetCustInqResponse")
public class RetCustInqResponse implements Serializable {

    @XmlElement(name = "RetCustInqRs", required = true)
    protected RetCustInqResponse.RetCustInqRs retCustInqRs;
    @XmlElement(name = "RetCustInq_CustomData", required = true)
    protected String retCustInqCustomData;

    /**
     * Gets the value of the retCustInqRs property.
     * 
     * @return
     *     possible object is
     *     {@link RetCustInqResponse.RetCustInqRs }
     *     
     */
    public RetCustInqResponse.RetCustInqRs getRetCustInqRs() {
        return retCustInqRs;
    }

    /**
     * Sets the value of the retCustInqRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetCustInqResponse.RetCustInqRs }
     *     
     */
    public void setRetCustInqRs(RetCustInqResponse.RetCustInqRs value) {
        this.retCustInqRs = value;
    }

    /**
     * Gets the value of the retCustInqCustomData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetCustInqCustomData() {
        return retCustInqCustomData;
    }

    /**
     * Sets the value of the retCustInqCustomData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetCustInqCustomData(String value) {
        this.retCustInqCustomData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RetCustDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AnnualRevenue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Assistant" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AvailableCrLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BlacklistNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BlacklistReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsBlacklisted" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CardHolder" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ChargeLevelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CombinedStmtFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ConstitutionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ConstitutionRefCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CorpRepCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CountryOfBirth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CreatedFrom" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CreatedBySystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CurrentCrExposure" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CommunityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Community" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BirthDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FirstNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FirstNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Health" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HealthCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LastNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LastNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MiddleNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MiddleNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsStaff" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SwiftCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsMinor" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsNRE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustProfitability" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustRelNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustTrade" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusChangeDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DelinquencyFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DeliquencyPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Designation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsDocReceived" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DualFirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DualLastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DualMiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Education" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EntityCreationFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExtnNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FatherOrHusbandName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FaxHome" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GroupIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HouseholdName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IncrementalDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="InternalScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SalutationCodeOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NameOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SalutationOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsSwiftCodeOfBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsCorpRepresentative" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsDummy" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsEBankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsSMSBankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsWAPBankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LeadSource" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LicenseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MaidenName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MaidenNameOfMother" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Manager" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ManagerOpinion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GuardianCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GuardianName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ModifiedBySysId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MotherName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NameSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NationalIdCardNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NativeLanguage" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NativeLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NativeLanguageName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NativeLanguageTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NativeLanguageTitleCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsNegated" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NegationNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NegationReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NickName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Occupation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OfflineCumDrLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PAN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PassportNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PersonType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PhoneEmailInfo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="PhoneEmailInfo">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value=""/>
     *                                   &lt;enumeration value="noemail@fcmb.com"/>
     *                                   &lt;enumeration value="felixgrammar@yahoo.com"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="EmailPalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PhoneEmailType">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="COMM1"/>
     *                                   &lt;enumeration value="WORK1"/>
     *                                   &lt;enumeration value="COMML"/>
     *                                   &lt;enumeration value="WORKL"/>
     *                                   &lt;enumeration value="HOMEL"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="PhoneNum">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="+234(0)08052003607"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="PhoneNumCityCode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="0"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="PhoneNumCountryCode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="234"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="PhoneNumLocalCode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="08052003607"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="PhoneOrEmail">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="PHONE"/>
     *                                   &lt;enumeration value="EMAIL"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="PrefFlag">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="Y"/>
     *                                   &lt;enumeration value="N"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="WorkExtnNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PhoneEmailID">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="31685464"/>
     *                                   &lt;enumeration value="31685465"/>
     *                                   &lt;enumeration value="31685466"/>
     *                                   &lt;enumeration value="31685467"/>
     *                                   &lt;enumeration value="31685468"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="PlaceOfBirth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PotentialCrLine" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefCodeRefCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PreviousName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrimaryServiceCentre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrimarySolId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PriorityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProofOfAgeDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProofOfAgeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PassportDtls" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PassportExpDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Rating" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RatingCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RatingDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RelationshipLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RelationshipType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RelationshipOpeningDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RelationshipValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RetCustAddrInfo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="AddrLine1">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="NO ADDRESS IN FIN7"/>
     *                                   &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="AddrLine2">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value=""/>
     *                                   &lt;enumeration value="WARRI"/>
     *                                   &lt;enumeration value="."/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="AddrLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="AddrStartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="AddrCategory">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="Home"/>
     *                                   &lt;enumeration value="Mailing"/>
     *                                   &lt;enumeration value="Work"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="BuildingLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="BusinessCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CellNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="City">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="LAG"/>
     *                                   &lt;enumeration value="WAR"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="CityCode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="LAG"/>
     *                                   &lt;enumeration value="WAR"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="EndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="FaxNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="FaxNumCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="FaxNumCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="FaxNumLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="HoldMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="HoldMailInitiatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="HoldMailReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="HouseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="LocalityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="MailStop" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PagerNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PagerNumCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PagerNumCcountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PagerNumLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PhoneNum1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PrefAddr">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="N"/>
     *                                   &lt;enumeration value="Y"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="PrefFormat" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PremiseName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ResidentialStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="State">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="LG"/>
     *                                   &lt;enumeration value="DL"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="StateCode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="LG"/>
     *                                   &lt;enumeration value="DL"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="StreetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="StreetNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="TelexCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="TelexCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="TelexLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Town" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="isAddressVerified" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="AddressID">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="4091277"/>
     *                                   &lt;enumeration value="4091275"/>
     *                                   &lt;enumeration value="4091276"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="RevenueUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Salutation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SegmentationClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ShortNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ShortNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SICCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SMSBankingMobNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SSN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StaffFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubSector" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubSegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SuspendNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SuspendReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsSuspended" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TFPartyFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TickerSymbol" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TotalCrExposure" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PassportIssueDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ForeignAccTaxReportingReq" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ForeignTaxReportingCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ForeignTaxReportingStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FatcaRemarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SenCitizenApplicableDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SeniorCitizen" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PhysicalState" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AadhaarNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StaffEmployeeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="DemographicDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="DonotCallFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DonotMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DonotSendEMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NameOfEmployer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EmploymentStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MaritalStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MaritalStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NationalityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefContactTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefDayTimeContactNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefDayTimeContactNumArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefDayTimeContactNumCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefDayTimeContactNumLocal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ResidenceCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ResidenceCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Tax_Rate_Table_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="EntityDocDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DocCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsDocDeleted" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DocExpDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DocIssueDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DocRmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DocTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DocTypeDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IdentificationType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PlaceOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RefNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PsychographicDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="DespatchMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HouseHoldNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefAddrMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefRep" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrefName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RiskBehaviour" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SegmentationClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StmtFreq" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StmtType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StmtDtWeekDay" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StmtDateWeekDay" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StmtWeekOfMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="External_System_Pricing" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Relationship_Pricing_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RelBankDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="EntityCreationFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NumOfCrCards" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TradeFinDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CautionStat" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CentralBankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ContractLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CountryDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CorpKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CreatedFrom" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CurrDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DCMmarginPercentage" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DCNextNumCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DCNextNumCodeRCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DCSanctionIngAuth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsDeleted" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EntityCreationFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpImpInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FaxNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FCSanctionIngAuth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HundredPercentFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IndividualCorpFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="InlandTradeAllowed" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LeasingLiabilities" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OrgKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PartyConst" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PartyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PhoneCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PhoneLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProductionCycle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SpecialCustFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SSIFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StateDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TradeAuthorityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RetailBaselDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="retBaselID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DebtHELOC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CurrFICOScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TotalDSR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BusinessTotalScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HasRelationship" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CombinedDSR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BusinessDSR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CashAssetRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DebtWorthRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="InterestTaxRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GeneralQuickRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ScoredSICCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OwnerYears" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NetWorth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DDABal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CombinedDebtRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProposedLeverage" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GLBLCashCoverage" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CurrentRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LiquidityRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MinTangibleWorth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FICOScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FinInqCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FinTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="InquiryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MinorDerogatoryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MajorDerogatoryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NeverPastDueCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OpenTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HighestCreditLmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TradeCntThirty" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TradeCntSixty" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TradeCntNinety" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RevolveDebtPrcnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SatisfactoryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="InstLoanBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="InstLoanCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MortgageBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MortgageTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OtherTradeBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OtherTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RevolveBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RevolveTradeLmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RevolveTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CCLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsWorst" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpenseIncomeRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SgmntPoolID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PoolPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PoolLGD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PoolEAD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LGD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EAD" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ModelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ModelVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ModelResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsFailed" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "retCustDtls",
        "demographicDtls",
        "entityDocDtls",
        "psychographicDtls",
        "relBankDtls",
        "tradeFinDtls",
        "retailBaselDtls"
    })
    public static class RetCustInqRs implements Serializable{

        @XmlElement(name = "RetCustDtls", required = true)
        protected RetCustInqResponse.RetCustInqRs.RetCustDtls retCustDtls;
        @XmlElement(name = "DemographicDtls", required = true)
        protected RetCustInqResponse.RetCustInqRs.DemographicDtls demographicDtls;
        @XmlElement(name = "EntityDocDtls", required = true)
        protected RetCustInqResponse.RetCustInqRs.EntityDocDtls entityDocDtls;
        @XmlElement(name = "PsychographicDtls", required = true)
        protected RetCustInqResponse.RetCustInqRs.PsychographicDtls psychographicDtls;
        @XmlElement(name = "RelBankDtls", required = true)
        protected RetCustInqResponse.RetCustInqRs.RelBankDtls relBankDtls;
        @XmlElement(name = "TradeFinDtls", required = true)
        protected RetCustInqResponse.RetCustInqRs.TradeFinDtls tradeFinDtls;
        @XmlElement(name = "RetailBaselDtls", required = true)
        protected RetCustInqResponse.RetCustInqRs.RetailBaselDtls retailBaselDtls;

        /**
         * Gets the value of the retCustDtls property.
         * 
         * @return
         *     possible object is
         *     {@link RetCustInqResponse.RetCustInqRs.RetCustDtls }
         *     
         */
        public RetCustInqResponse.RetCustInqRs.RetCustDtls getRetCustDtls() {
            return retCustDtls;
        }

        /**
         * Sets the value of the retCustDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetCustInqResponse.RetCustInqRs.RetCustDtls }
         *     
         */
        public void setRetCustDtls(RetCustInqResponse.RetCustInqRs.RetCustDtls value) {
            this.retCustDtls = value;
        }

        /**
         * Gets the value of the demographicDtls property.
         * 
         * @return
         *     possible object is
         *     {@link RetCustInqResponse.RetCustInqRs.DemographicDtls }
         *     
         */
        public RetCustInqResponse.RetCustInqRs.DemographicDtls getDemographicDtls() {
            return demographicDtls;
        }

        /**
         * Sets the value of the demographicDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetCustInqResponse.RetCustInqRs.DemographicDtls }
         *     
         */
        public void setDemographicDtls(RetCustInqResponse.RetCustInqRs.DemographicDtls value) {
            this.demographicDtls = value;
        }

        /**
         * Gets the value of the entityDocDtls property.
         * 
         * @return
         *     possible object is
         *     {@link RetCustInqResponse.RetCustInqRs.EntityDocDtls }
         *     
         */
        public RetCustInqResponse.RetCustInqRs.EntityDocDtls getEntityDocDtls() {
            return entityDocDtls;
        }

        /**
         * Sets the value of the entityDocDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetCustInqResponse.RetCustInqRs.EntityDocDtls }
         *     
         */
        public void setEntityDocDtls(RetCustInqResponse.RetCustInqRs.EntityDocDtls value) {
            this.entityDocDtls = value;
        }

        /**
         * Gets the value of the psychographicDtls property.
         * 
         * @return
         *     possible object is
         *     {@link RetCustInqResponse.RetCustInqRs.PsychographicDtls }
         *     
         */
        public RetCustInqResponse.RetCustInqRs.PsychographicDtls getPsychographicDtls() {
            return psychographicDtls;
        }

        /**
         * Sets the value of the psychographicDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetCustInqResponse.RetCustInqRs.PsychographicDtls }
         *     
         */
        public void setPsychographicDtls(RetCustInqResponse.RetCustInqRs.PsychographicDtls value) {
            this.psychographicDtls = value;
        }

        /**
         * Gets the value of the relBankDtls property.
         * 
         * @return
         *     possible object is
         *     {@link RetCustInqResponse.RetCustInqRs.RelBankDtls }
         *     
         */
        public RetCustInqResponse.RetCustInqRs.RelBankDtls getRelBankDtls() {
            return relBankDtls;
        }

        /**
         * Sets the value of the relBankDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetCustInqResponse.RetCustInqRs.RelBankDtls }
         *     
         */
        public void setRelBankDtls(RetCustInqResponse.RetCustInqRs.RelBankDtls value) {
            this.relBankDtls = value;
        }

        /**
         * Gets the value of the tradeFinDtls property.
         * 
         * @return
         *     possible object is
         *     {@link RetCustInqResponse.RetCustInqRs.TradeFinDtls }
         *     
         */
        public RetCustInqResponse.RetCustInqRs.TradeFinDtls getTradeFinDtls() {
            return tradeFinDtls;
        }

        /**
         * Sets the value of the tradeFinDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetCustInqResponse.RetCustInqRs.TradeFinDtls }
         *     
         */
        public void setTradeFinDtls(RetCustInqResponse.RetCustInqRs.TradeFinDtls value) {
            this.tradeFinDtls = value;
        }

        /**
         * Gets the value of the retailBaselDtls property.
         * 
         * @return
         *     possible object is
         *     {@link RetCustInqResponse.RetCustInqRs.RetailBaselDtls }
         *     
         */
        public RetCustInqResponse.RetCustInqRs.RetailBaselDtls getRetailBaselDtls() {
            return retailBaselDtls;
        }

        /**
         * Sets the value of the retailBaselDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetCustInqResponse.RetCustInqRs.RetailBaselDtls }
         *     
         */
        public void setRetailBaselDtls(RetCustInqResponse.RetCustInqRs.RetailBaselDtls value) {
            this.retailBaselDtls = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="DonotCallFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DonotMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DonotSendEMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NameOfEmployer" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EmploymentStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MaritalStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MaritalStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NationalityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefContactTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefDayTimeContactNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefDayTimeContactNumArea" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefDayTimeContactNumCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefDayTimeContactNumLocal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ResidenceCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ResidenceCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Tax_Rate_Table_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "donotCallFlag",
            "donotMailFlag",
            "donotSendEMailFlag",
            "nameOfEmployer",
            "employmentStatus",
            "maritalStatus",
            "nationality",
            "maritalStatusCode",
            "nationalityCode",
            "prefContactTime",
            "prefDayTimeContactNum",
            "prefDayTimeContactNumArea",
            "prefDayTimeContactNumCountry",
            "prefDayTimeContactNumLocal",
            "residenceCountry",
            "residenceCountryCode",
            "taxRateTableCode"
        })
        public static class DemographicDtls implements Serializable{

            @XmlElement(name = "DonotCallFlag", required = true)
            protected String donotCallFlag;
            @XmlElement(name = "DonotMailFlag", required = true)
            protected String donotMailFlag;
            @XmlElement(name = "DonotSendEMailFlag", required = true)
            protected String donotSendEMailFlag;
            @XmlElement(name = "NameOfEmployer", required = true)
            protected String nameOfEmployer;
            @XmlElement(name = "EmploymentStatus", required = true)
            protected String employmentStatus;
            @XmlElement(name = "MaritalStatus", required = true)
            protected String maritalStatus;
            @XmlElement(name = "Nationality", required = true)
            protected String nationality;
            @XmlElement(name = "MaritalStatusCode", required = true)
            protected String maritalStatusCode;
            @XmlElement(name = "NationalityCode", required = true)
            protected String nationalityCode;
            @XmlElement(name = "PrefContactTime", required = true)
            protected String prefContactTime;
            @XmlElement(name = "PrefDayTimeContactNum", required = true)
            protected String prefDayTimeContactNum;
            @XmlElement(name = "PrefDayTimeContactNumArea", required = true)
            protected String prefDayTimeContactNumArea;
            @XmlElement(name = "PrefDayTimeContactNumCountry", required = true)
            protected String prefDayTimeContactNumCountry;
            @XmlElement(name = "PrefDayTimeContactNumLocal", required = true)
            protected String prefDayTimeContactNumLocal;
            @XmlElement(name = "ResidenceCountry", required = true)
            protected String residenceCountry;
            @XmlElement(name = "ResidenceCountryCode", required = true)
            protected String residenceCountryCode;
            @XmlElement(name = "Tax_Rate_Table_Code", required = true)
            protected String taxRateTableCode;

            /**
             * Gets the value of the donotCallFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDonotCallFlag() {
                return donotCallFlag;
            }

            /**
             * Sets the value of the donotCallFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDonotCallFlag(String value) {
                this.donotCallFlag = value;
            }

            /**
             * Gets the value of the donotMailFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDonotMailFlag() {
                return donotMailFlag;
            }

            /**
             * Sets the value of the donotMailFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDonotMailFlag(String value) {
                this.donotMailFlag = value;
            }

            /**
             * Gets the value of the donotSendEMailFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDonotSendEMailFlag() {
                return donotSendEMailFlag;
            }

            /**
             * Sets the value of the donotSendEMailFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDonotSendEMailFlag(String value) {
                this.donotSendEMailFlag = value;
            }

            /**
             * Gets the value of the nameOfEmployer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNameOfEmployer() {
                return nameOfEmployer;
            }

            /**
             * Sets the value of the nameOfEmployer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNameOfEmployer(String value) {
                this.nameOfEmployer = value;
            }

            /**
             * Gets the value of the employmentStatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEmploymentStatus() {
                return employmentStatus;
            }

            /**
             * Sets the value of the employmentStatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEmploymentStatus(String value) {
                this.employmentStatus = value;
            }

            /**
             * Gets the value of the maritalStatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMaritalStatus() {
                return maritalStatus;
            }

            /**
             * Sets the value of the maritalStatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMaritalStatus(String value) {
                this.maritalStatus = value;
            }

            /**
             * Gets the value of the nationality property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNationality() {
                return nationality;
            }

            /**
             * Sets the value of the nationality property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNationality(String value) {
                this.nationality = value;
            }

            /**
             * Gets the value of the maritalStatusCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMaritalStatusCode() {
                return maritalStatusCode;
            }

            /**
             * Sets the value of the maritalStatusCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMaritalStatusCode(String value) {
                this.maritalStatusCode = value;
            }

            /**
             * Gets the value of the nationalityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNationalityCode() {
                return nationalityCode;
            }

            /**
             * Sets the value of the nationalityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNationalityCode(String value) {
                this.nationalityCode = value;
            }

            /**
             * Gets the value of the prefContactTime property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefContactTime() {
                return prefContactTime;
            }

            /**
             * Sets the value of the prefContactTime property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefContactTime(String value) {
                this.prefContactTime = value;
            }

            /**
             * Gets the value of the prefDayTimeContactNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefDayTimeContactNum() {
                return prefDayTimeContactNum;
            }

            /**
             * Sets the value of the prefDayTimeContactNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefDayTimeContactNum(String value) {
                this.prefDayTimeContactNum = value;
            }

            /**
             * Gets the value of the prefDayTimeContactNumArea property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefDayTimeContactNumArea() {
                return prefDayTimeContactNumArea;
            }

            /**
             * Sets the value of the prefDayTimeContactNumArea property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefDayTimeContactNumArea(String value) {
                this.prefDayTimeContactNumArea = value;
            }

            /**
             * Gets the value of the prefDayTimeContactNumCountry property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefDayTimeContactNumCountry() {
                return prefDayTimeContactNumCountry;
            }

            /**
             * Sets the value of the prefDayTimeContactNumCountry property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefDayTimeContactNumCountry(String value) {
                this.prefDayTimeContactNumCountry = value;
            }

            /**
             * Gets the value of the prefDayTimeContactNumLocal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefDayTimeContactNumLocal() {
                return prefDayTimeContactNumLocal;
            }

            /**
             * Sets the value of the prefDayTimeContactNumLocal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefDayTimeContactNumLocal(String value) {
                this.prefDayTimeContactNumLocal = value;
            }

            /**
             * Gets the value of the residenceCountry property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResidenceCountry() {
                return residenceCountry;
            }

            /**
             * Sets the value of the residenceCountry property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResidenceCountry(String value) {
                this.residenceCountry = value;
            }

            /**
             * Gets the value of the residenceCountryCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResidenceCountryCode() {
                return residenceCountryCode;
            }

            /**
             * Sets the value of the residenceCountryCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResidenceCountryCode(String value) {
                this.residenceCountryCode = value;
            }

            /**
             * Gets the value of the taxRateTableCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxRateTableCode() {
                return taxRateTableCode;
            }

            /**
             * Sets the value of the taxRateTableCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxRateTableCode(String value) {
                this.taxRateTableCode = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DocCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsDocDeleted" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DocExpDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DocIssueDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DocRmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DocTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DocTypeDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IdentificationType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PlaceOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RefNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "countryOfIssue",
            "docCode",
            "isDocDeleted",
            "docExpDt",
            "docIssueDt",
            "docRmks",
            "docTypeCode",
            "docTypeDesc",
            "entityType",
            "identificationType",
            "placeOfIssue",
            "refNum",
            "status"
        })
        public static class EntityDocDtls implements Serializable{

            @XmlElement(name = "CountryOfIssue", required = true)
            protected String countryOfIssue;
            @XmlElement(name = "DocCode", required = true)
            protected String docCode;
            @XmlElement(name = "IsDocDeleted", required = true)
            protected String isDocDeleted;
            @XmlElement(name = "DocExpDt", required = true)
            protected String docExpDt;
            @XmlElement(name = "DocIssueDt", required = true)
            protected String docIssueDt;
            @XmlElement(name = "DocRmks", required = true)
            protected String docRmks;
            @XmlElement(name = "DocTypeCode", required = true)
            protected String docTypeCode;
            @XmlElement(name = "DocTypeDesc", required = true)
            protected String docTypeDesc;
            @XmlElement(name = "EntityType", required = true)
            protected String entityType;
            @XmlElement(name = "IdentificationType", required = true)
            protected String identificationType;
            @XmlElement(name = "PlaceOfIssue", required = true)
            protected String placeOfIssue;
            @XmlElement(name = "RefNum", required = true)
            protected String refNum;
            @XmlElement(name = "Status", required = true)
            protected String status;

            /**
             * Gets the value of the countryOfIssue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryOfIssue() {
                return countryOfIssue;
            }

            /**
             * Sets the value of the countryOfIssue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryOfIssue(String value) {
                this.countryOfIssue = value;
            }

            /**
             * Gets the value of the docCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocCode() {
                return docCode;
            }

            /**
             * Sets the value of the docCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocCode(String value) {
                this.docCode = value;
            }

            /**
             * Gets the value of the isDocDeleted property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsDocDeleted() {
                return isDocDeleted;
            }

            /**
             * Sets the value of the isDocDeleted property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsDocDeleted(String value) {
                this.isDocDeleted = value;
            }

            /**
             * Gets the value of the docExpDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocExpDt() {
                return docExpDt;
            }

            /**
             * Sets the value of the docExpDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocExpDt(String value) {
                this.docExpDt = value;
            }

            /**
             * Gets the value of the docIssueDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocIssueDt() {
                return docIssueDt;
            }

            /**
             * Sets the value of the docIssueDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocIssueDt(String value) {
                this.docIssueDt = value;
            }

            /**
             * Gets the value of the docRmks property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocRmks() {
                return docRmks;
            }

            /**
             * Sets the value of the docRmks property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocRmks(String value) {
                this.docRmks = value;
            }

            /**
             * Gets the value of the docTypeCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocTypeCode() {
                return docTypeCode;
            }

            /**
             * Sets the value of the docTypeCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocTypeCode(String value) {
                this.docTypeCode = value;
            }

            /**
             * Gets the value of the docTypeDesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocTypeDesc() {
                return docTypeDesc;
            }

            /**
             * Sets the value of the docTypeDesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocTypeDesc(String value) {
                this.docTypeDesc = value;
            }

            /**
             * Gets the value of the entityType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityType() {
                return entityType;
            }

            /**
             * Sets the value of the entityType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityType(String value) {
                this.entityType = value;
            }

            /**
             * Gets the value of the identificationType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdentificationType() {
                return identificationType;
            }

            /**
             * Sets the value of the identificationType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdentificationType(String value) {
                this.identificationType = value;
            }

            /**
             * Gets the value of the placeOfIssue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlaceOfIssue() {
                return placeOfIssue;
            }

            /**
             * Sets the value of the placeOfIssue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlaceOfIssue(String value) {
                this.placeOfIssue = value;
            }

            /**
             * Gets the value of the refNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRefNum() {
                return refNum;
            }

            /**
             * Sets the value of the refNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRefNum(String value) {
                this.refNum = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="DespatchMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HouseHoldNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefAddrMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefRep" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RiskBehaviour" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SegmentationClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StmtFreq" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StmtType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StmtDtWeekDay" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StmtDateWeekDay" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StmtWeekOfMonth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="External_System_Pricing" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Relationship_Pricing_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "despatchMode",
            "houseHoldNum",
            "prefAddrMode",
            "prefRep",
            "prefName",
            "riskBehaviour",
            "segmentationClass",
            "stmtFreq",
            "stmtType",
            "stmtDtWeekDay",
            "stmtDateWeekDay",
            "stmtWeekOfMonth",
            "externalSystemPricing",
            "relationshipPricingID"
        })
        public static class PsychographicDtls implements Serializable{

            @XmlElement(name = "DespatchMode", required = true)
            protected String despatchMode;
            @XmlElement(name = "HouseHoldNum", required = true)
            protected String houseHoldNum;
            @XmlElement(name = "PrefAddrMode", required = true)
            protected String prefAddrMode;
            @XmlElement(name = "PrefRep", required = true)
            protected String prefRep;
            @XmlElement(name = "PrefName", required = true)
            protected String prefName;
            @XmlElement(name = "RiskBehaviour", required = true)
            protected String riskBehaviour;
            @XmlElement(name = "SegmentationClass", required = true)
            protected String segmentationClass;
            @XmlElement(name = "StmtFreq", required = true)
            protected String stmtFreq;
            @XmlElement(name = "StmtType", required = true)
            protected String stmtType;
            @XmlElement(name = "StmtDtWeekDay", required = true)
            protected String stmtDtWeekDay;
            @XmlElement(name = "StmtDateWeekDay", required = true)
            protected String stmtDateWeekDay;
            @XmlElement(name = "StmtWeekOfMonth", required = true)
            protected String stmtWeekOfMonth;
            @XmlElement(name = "External_System_Pricing", required = true)
            protected String externalSystemPricing;
            @XmlElement(name = "Relationship_Pricing_ID", required = true)
            protected String relationshipPricingID;

            /**
             * Gets the value of the despatchMode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDespatchMode() {
                return despatchMode;
            }

            /**
             * Sets the value of the despatchMode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDespatchMode(String value) {
                this.despatchMode = value;
            }

            /**
             * Gets the value of the houseHoldNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseHoldNum() {
                return houseHoldNum;
            }

            /**
             * Sets the value of the houseHoldNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseHoldNum(String value) {
                this.houseHoldNum = value;
            }

            /**
             * Gets the value of the prefAddrMode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefAddrMode() {
                return prefAddrMode;
            }

            /**
             * Sets the value of the prefAddrMode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefAddrMode(String value) {
                this.prefAddrMode = value;
            }

            /**
             * Gets the value of the prefRep property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefRep() {
                return prefRep;
            }

            /**
             * Sets the value of the prefRep property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefRep(String value) {
                this.prefRep = value;
            }

            /**
             * Gets the value of the prefName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefName() {
                return prefName;
            }

            /**
             * Sets the value of the prefName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefName(String value) {
                this.prefName = value;
            }

            /**
             * Gets the value of the riskBehaviour property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRiskBehaviour() {
                return riskBehaviour;
            }

            /**
             * Sets the value of the riskBehaviour property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRiskBehaviour(String value) {
                this.riskBehaviour = value;
            }

            /**
             * Gets the value of the segmentationClass property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSegmentationClass() {
                return segmentationClass;
            }

            /**
             * Sets the value of the segmentationClass property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSegmentationClass(String value) {
                this.segmentationClass = value;
            }

            /**
             * Gets the value of the stmtFreq property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStmtFreq() {
                return stmtFreq;
            }

            /**
             * Sets the value of the stmtFreq property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStmtFreq(String value) {
                this.stmtFreq = value;
            }

            /**
             * Gets the value of the stmtType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStmtType() {
                return stmtType;
            }

            /**
             * Sets the value of the stmtType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStmtType(String value) {
                this.stmtType = value;
            }

            /**
             * Gets the value of the stmtDtWeekDay property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStmtDtWeekDay() {
                return stmtDtWeekDay;
            }

            /**
             * Sets the value of the stmtDtWeekDay property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStmtDtWeekDay(String value) {
                this.stmtDtWeekDay = value;
            }

            /**
             * Gets the value of the stmtDateWeekDay property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStmtDateWeekDay() {
                return stmtDateWeekDay;
            }

            /**
             * Sets the value of the stmtDateWeekDay property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStmtDateWeekDay(String value) {
                this.stmtDateWeekDay = value;
            }

            /**
             * Gets the value of the stmtWeekOfMonth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStmtWeekOfMonth() {
                return stmtWeekOfMonth;
            }

            /**
             * Sets the value of the stmtWeekOfMonth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStmtWeekOfMonth(String value) {
                this.stmtWeekOfMonth = value;
            }

            /**
             * Gets the value of the externalSystemPricing property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExternalSystemPricing() {
                return externalSystemPricing;
            }

            /**
             * Sets the value of the externalSystemPricing property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExternalSystemPricing(String value) {
                this.externalSystemPricing = value;
            }

            /**
             * Gets the value of the relationshipPricingID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipPricingID() {
                return relationshipPricingID;
            }

            /**
             * Sets the value of the relationshipPricingID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipPricingID(String value) {
                this.relationshipPricingID = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="EntityCreationFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NumOfCrCards" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "entityCreationFlag",
            "numOfCrCards"
        })
        public static class RelBankDtls implements Serializable{

            @XmlElement(name = "EntityCreationFlag", required = true)
            protected String entityCreationFlag;
            @XmlElement(name = "NumOfCrCards", required = true)
            protected String numOfCrCards;

            /**
             * Gets the value of the entityCreationFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityCreationFlag() {
                return entityCreationFlag;
            }

            /**
             * Sets the value of the entityCreationFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityCreationFlag(String value) {
                this.entityCreationFlag = value;
            }

            /**
             * Gets the value of the numOfCrCards property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumOfCrCards() {
                return numOfCrCards;
            }

            /**
             * Sets the value of the numOfCrCards property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumOfCrCards(String value) {
                this.numOfCrCards = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AnnualRevenue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Assistant" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AvailableCrLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BlacklistNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BlacklistReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsBlacklisted" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CardHolder" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ChargeLevelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CombinedStmtFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ConstitutionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ConstitutionRefCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CorpRepCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CountryOfBirth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CreatedFrom" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CreatedBySystemId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CurrentCrExposure" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CommunityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Community" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BirthDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FirstNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FirstNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Health" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HealthCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LastNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LastNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MiddleNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MiddleNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsStaff" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SwiftCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsMinor" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsNRE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustProfitability" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustRelNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustTrade" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusChangeDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DelinquencyFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DeliquencyPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Designation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsDocReceived" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DualFirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DualLastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DualMiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Education" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EntityCreationFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExtnNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FatherOrHusbandName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FaxHome" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GroupIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HouseholdName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IncrementalDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="InternalScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SalutationCodeOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NameOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SalutationOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsSwiftCodeOfBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsCorpRepresentative" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsDummy" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsEBankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsSMSBankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsWAPBankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LeadSource" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LicenseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MaidenName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MaidenNameOfMother" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Manager" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ManagerOpinion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GuardianCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GuardianName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ModifiedBySysId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MotherName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NameSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NationalIdCardNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NativeLanguage" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NativeLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NativeLanguageName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NativeLanguageTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NativeLanguageTitleCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsNegated" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NegationNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NegationReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NickName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Occupation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OfflineCumDrLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PAN" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PassportNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PersonType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PhoneEmailInfo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="PhoneEmailInfo">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value=""/>
         *                         &lt;enumeration value="noemail@fcmb.com"/>
         *                         &lt;enumeration value="felixgrammar@yahoo.com"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="EmailPalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PhoneEmailType">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="COMM1"/>
         *                         &lt;enumeration value="WORK1"/>
         *                         &lt;enumeration value="COMML"/>
         *                         &lt;enumeration value="WORKL"/>
         *                         &lt;enumeration value="HOMEL"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="PhoneNum">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="+234(0)08052003607"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="PhoneNumCityCode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="0"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="PhoneNumCountryCode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="234"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="PhoneNumLocalCode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="08052003607"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="PhoneOrEmail">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="PHONE"/>
         *                         &lt;enumeration value="EMAIL"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="PrefFlag">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="Y"/>
         *                         &lt;enumeration value="N"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="WorkExtnNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PhoneEmailID">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="31685464"/>
         *                         &lt;enumeration value="31685465"/>
         *                         &lt;enumeration value="31685466"/>
         *                         &lt;enumeration value="31685467"/>
         *                         &lt;enumeration value="31685468"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="PlaceOfBirth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PotentialCrLine" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefCodeRefCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrefName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PreviousName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrimaryServiceCentre" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrimarySolId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PriorityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProofOfAgeDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProofOfAgeFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PassportDtls" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PassportExpDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Rating" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RatingCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RatingDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RelationshipLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RelationshipType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RelationshipOpeningDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RelationshipValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RetCustAddrInfo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="AddrLine1">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="NO ADDRESS IN FIN7"/>
         *                         &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="AddrLine2">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value=""/>
         *                         &lt;enumeration value="WARRI"/>
         *                         &lt;enumeration value="."/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="AddrLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="AddrStartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="AddrCategory">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="Home"/>
         *                         &lt;enumeration value="Mailing"/>
         *                         &lt;enumeration value="Work"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="BuildingLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="BusinessCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CellNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="City">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="LAG"/>
         *                         &lt;enumeration value="WAR"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="CityCode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="LAG"/>
         *                         &lt;enumeration value="WAR"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="EndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="FaxNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="FaxNumCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="FaxNumCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="FaxNumLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="HoldMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="HoldMailInitiatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="HoldMailReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="HouseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="LocalityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="MailStop" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PagerNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PagerNumCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PagerNumCcountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PagerNumLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PhoneNum1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PrefAddr">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="N"/>
         *                         &lt;enumeration value="Y"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="PrefFormat" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PremiseName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ResidentialStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="State">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="LG"/>
         *                         &lt;enumeration value="DL"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="StateCode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="LG"/>
         *                         &lt;enumeration value="DL"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="StreetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="StreetNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="TelexCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="TelexCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="TelexLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Town" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="isAddressVerified" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="AddressID">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="4091277"/>
         *                         &lt;enumeration value="4091275"/>
         *                         &lt;enumeration value="4091276"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="RevenueUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Salutation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SegmentationClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ShortNameNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ShortNameNative1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SICCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SMSBankingMobNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SSN" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StaffFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubSector" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubSegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SuspendNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SuspendReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsSuspended" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TFPartyFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TickerSymbol" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TotalCrExposure" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PassportIssueDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ForeignAccTaxReportingReq" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ForeignTaxReportingCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ForeignTaxReportingStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FatcaRemarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SenCitizenApplicableDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SeniorCitizen" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PhysicalState" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AadhaarNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StaffEmployeeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "acctName",
            "annualRevenue",
            "assistant",
            "availableCrLimit",
            "blacklistNotes",
            "blacklistReason",
            "isBlacklisted",
            "cardHolder",
            "category",
            "chargeLevelCode",
            "custId",
            "cin",
            "city",
            "combinedStmtFlag",
            "constitutionCode",
            "constitutionRefCode",
            "corpRepCount",
            "countryOfBirth",
            "createdFrom",
            "createdBySystemId",
            "currCode",
            "currentCrExposure",
            "communityCode",
            "community",
            "birthDt",
            "firstName",
            "firstNameNative",
            "firstNameNative1",
            "health",
            "healthCode",
            "language",
            "lastName",
            "lastNameNative",
            "lastNameNative1",
            "middleName",
            "middleNameNative",
            "middleNameNative1",
            "isStaff",
            "swiftCode",
            "custType",
            "custTypeCode",
            "custClass",
            "isMinor",
            "isNRE",
            "custProfitability",
            "custRelNum",
            "custTrade",
            "statusChangeDt",
            "defaultAddrType",
            "delinquencyFlag",
            "deliquencyPeriod",
            "designation",
            "isDocReceived",
            "dualFirstName",
            "dualLastName",
            "dualMiddleName",
            "education",
            "entityCreationFlag",
            "extnNum",
            "fatherOrHusbandName",
            "fax",
            "faxHome",
            "gender",
            "groupIdCode",
            "householdName",
            "incrementalDt",
            "internalScore",
            "salutationCodeOfIntroducer",
            "statusOfIntroducer",
            "nameOfIntroducer",
            "salutationOfIntroducer",
            "isSwiftCodeOfBank",
            "isCorpRepresentative",
            "isDummy",
            "isEBankingEnabled",
            "isSMSBankingEnabled",
            "isWAPBankingEnabled",
            "leadSource",
            "licenseNum",
            "maidenName",
            "maidenNameOfMother",
            "manager",
            "managerOpinion",
            "guardianCode",
            "guardianName",
            "modifiedBySysId",
            "motherName",
            "name",
            "nameSuffix",
            "nationalIdCardNum",
            "nativeLanguage",
            "nativeLanguageCode",
            "nativeLanguageName",
            "nativeLanguageTitle",
            "nativeLanguageTitleCode",
            "isNegated",
            "negationNotes",
            "negationReason",
            "nickName",
            "notes",
            "occupation",
            "occupationCode",
            "offlineCumDrLimit",
            "pan",
            "passportNum",
            "personType",
            "phone",
            "phoneEmailInfo",
            "placeOfBirth",
            "potentialCrLine",
            "prefCode",
            "prefCodeRefCode",
            "prefName",
            "previousName",
            "primaryServiceCentre",
            "primarySolId",
            "priorityCode",
            "proofOfAgeDoc",
            "proofOfAgeFlag",
            "passportDtls",
            "passportExpDt",
            "rating",
            "ratingCode",
            "ratingDt",
            "region",
            "relationshipLevel",
            "relationshipType",
            "relationshipOpeningDt",
            "relationshipValue",
            "retCustAddrInfo",
            "revenueUnits",
            "salutation",
            "salutationCode",
            "sector",
            "sectorCode",
            "segmentationClass",
            "shortName",
            "shortNameNative",
            "shortNameNative1",
            "sicCode",
            "smsBankingMobNum",
            "ssn",
            "staffFlag",
            "startDt",
            "status",
            "subSector",
            "subSectorCode",
            "subSegment",
            "suspendNotes",
            "suspendReason",
            "isSuspended",
            "tfPartyFlag",
            "tickerSymbol",
            "totalCrExposure",
            "passportIssueDt",
            "foreignAccTaxReportingReq",
            "foreignTaxReportingCountry",
            "foreignTaxReportingStatus",
            "fatcaRemarks",
            "senCitizenApplicableDate",
            "seniorCitizen",
            "custStatus",
            "physicalState",
            "aadhaarNumber",
            "staffEmployeeID"
        })
        public static class RetCustDtls implements Serializable{

            @XmlElement(name = "AcctName", required = true)
            protected String acctName;
            @XmlElement(name = "AnnualRevenue", required = true)
            protected String annualRevenue;
            @XmlElement(name = "Assistant", required = true)
            protected String assistant;
            @XmlElement(name = "AvailableCrLimit", required = true)
            protected String availableCrLimit;
            @XmlElement(name = "BlacklistNotes", required = true)
            protected String blacklistNotes;
            @XmlElement(name = "BlacklistReason", required = true)
            protected String blacklistReason;
            @XmlElement(name = "IsBlacklisted", required = true)
            protected String isBlacklisted;
            @XmlElement(name = "CardHolder", required = true)
            protected String cardHolder;
            @XmlElement(name = "Category", required = true)
            protected String category;
            @XmlElement(name = "ChargeLevelCode", required = true)
            protected String chargeLevelCode;
            @XmlElement(name = "CustId", required = true)
            protected String custId;
            @XmlElement(name = "CIN", required = true)
            protected String cin;
            @XmlElement(name = "City", required = true)
            protected String city;
            @XmlElement(name = "CombinedStmtFlag", required = true)
            protected String combinedStmtFlag;
            @XmlElement(name = "ConstitutionCode", required = true)
            protected String constitutionCode;
            @XmlElement(name = "ConstitutionRefCode", required = true)
            protected String constitutionRefCode;
            @XmlElement(name = "CorpRepCount", required = true)
            protected String corpRepCount;
            @XmlElement(name = "CountryOfBirth", required = true)
            protected String countryOfBirth;
            @XmlElement(name = "CreatedFrom", required = true)
            protected String createdFrom;
            @XmlElement(name = "CreatedBySystemId", required = true)
            protected String createdBySystemId;
            @XmlElement(name = "CurrCode", required = true)
            protected String currCode;
            @XmlElement(name = "CurrentCrExposure", required = true)
            protected String currentCrExposure;
            @XmlElement(name = "CommunityCode", required = true)
            protected String communityCode;
            @XmlElement(name = "Community", required = true)
            protected String community;
            @XmlElement(name = "BirthDt", required = true)
            protected String birthDt;
            @XmlElement(name = "FirstName", required = true)
            protected String firstName;
            @XmlElement(name = "FirstNameNative", required = true)
            protected String firstNameNative;
            @XmlElement(name = "FirstNameNative1", required = true)
            protected String firstNameNative1;
            @XmlElement(name = "Health", required = true)
            protected String health;
            @XmlElement(name = "HealthCode", required = true)
            protected String healthCode;
            @XmlElement(name = "Language", required = true)
            protected String language;
            @XmlElement(name = "LastName", required = true)
            protected String lastName;
            @XmlElement(name = "LastNameNative", required = true)
            protected String lastNameNative;
            @XmlElement(name = "LastNameNative1", required = true)
            protected String lastNameNative1;
            @XmlElement(name = "MiddleName", required = true)
            protected String middleName;
            @XmlElement(name = "MiddleNameNative", required = true)
            protected String middleNameNative;
            @XmlElement(name = "MiddleNameNative1", required = true)
            protected String middleNameNative1;
            @XmlElement(name = "IsStaff", required = true)
            protected String isStaff;
            @XmlElement(name = "SwiftCode", required = true)
            protected String swiftCode;
            @XmlElement(name = "CustType", required = true)
            protected String custType;
            @XmlElement(name = "CustTypeCode", required = true)
            protected String custTypeCode;
            @XmlElement(name = "CustClass", required = true)
            protected String custClass;
            @XmlElement(name = "IsMinor", required = true)
            protected String isMinor;
            @XmlElement(name = "IsNRE", required = true)
            protected String isNRE;
            @XmlElement(name = "CustProfitability", required = true)
            protected String custProfitability;
            @XmlElement(name = "CustRelNum", required = true)
            protected String custRelNum;
            @XmlElement(name = "CustTrade", required = true)
            protected String custTrade;
            @XmlElement(name = "StatusChangeDt", required = true)
            protected String statusChangeDt;
            @XmlElement(name = "DefaultAddrType", required = true)
            protected String defaultAddrType;
            @XmlElement(name = "DelinquencyFlag", required = true)
            protected String delinquencyFlag;
            @XmlElement(name = "DeliquencyPeriod", required = true)
            protected String deliquencyPeriod;
            @XmlElement(name = "Designation", required = true)
            protected String designation;
            @XmlElement(name = "IsDocReceived", required = true)
            protected String isDocReceived;
            @XmlElement(name = "DualFirstName", required = true)
            protected String dualFirstName;
            @XmlElement(name = "DualLastName", required = true)
            protected String dualLastName;
            @XmlElement(name = "DualMiddleName", required = true)
            protected String dualMiddleName;
            @XmlElement(name = "Education", required = true)
            protected String education;
            @XmlElement(name = "EntityCreationFlag", required = true)
            protected String entityCreationFlag;
            @XmlElement(name = "ExtnNum", required = true)
            protected String extnNum;
            @XmlElement(name = "FatherOrHusbandName", required = true)
            protected String fatherOrHusbandName;
            @XmlElement(name = "Fax", required = true)
            protected String fax;
            @XmlElement(name = "FaxHome", required = true)
            protected String faxHome;
            @XmlElement(name = "Gender", required = true)
            protected String gender;
            @XmlElement(name = "GroupIdCode", required = true)
            protected String groupIdCode;
            @XmlElement(name = "HouseholdName", required = true)
            protected String householdName;
            @XmlElement(name = "IncrementalDt", required = true)
            protected String incrementalDt;
            @XmlElement(name = "InternalScore", required = true)
            protected String internalScore;
            @XmlElement(name = "SalutationCodeOfIntroducer", required = true)
            protected String salutationCodeOfIntroducer;
            @XmlElement(name = "StatusOfIntroducer", required = true)
            protected String statusOfIntroducer;
            @XmlElement(name = "NameOfIntroducer", required = true)
            protected String nameOfIntroducer;
            @XmlElement(name = "SalutationOfIntroducer", required = true)
            protected String salutationOfIntroducer;
            @XmlElement(name = "IsSwiftCodeOfBank", required = true)
            protected String isSwiftCodeOfBank;
            @XmlElement(name = "IsCorpRepresentative", required = true)
            protected String isCorpRepresentative;
            @XmlElement(name = "IsDummy", required = true)
            protected String isDummy;
            @XmlElement(name = "IsEBankingEnabled", required = true)
            protected String isEBankingEnabled;
            @XmlElement(name = "IsSMSBankingEnabled", required = true)
            protected String isSMSBankingEnabled;
            @XmlElement(name = "IsWAPBankingEnabled", required = true)
            protected String isWAPBankingEnabled;
            @XmlElement(name = "LeadSource", required = true)
            protected String leadSource;
            @XmlElement(name = "LicenseNum", required = true)
            protected String licenseNum;
            @XmlElement(name = "MaidenName", required = true)
            protected String maidenName;
            @XmlElement(name = "MaidenNameOfMother", required = true)
            protected String maidenNameOfMother;
            @XmlElement(name = "Manager", required = true)
            protected String manager;
            @XmlElement(name = "ManagerOpinion", required = true)
            protected String managerOpinion;
            @XmlElement(name = "GuardianCode", required = true)
            protected String guardianCode;
            @XmlElement(name = "GuardianName", required = true)
            protected String guardianName;
            @XmlElement(name = "ModifiedBySysId", required = true)
            protected String modifiedBySysId;
            @XmlElement(name = "MotherName", required = true)
            protected String motherName;
            @XmlElement(name = "Name", required = true)
            protected String name;
            @XmlElement(name = "NameSuffix", required = true)
            protected String nameSuffix;
            @XmlElement(name = "NationalIdCardNum", required = true)
            protected String nationalIdCardNum;
            @XmlElement(name = "NativeLanguage", required = true)
            protected String nativeLanguage;
            @XmlElement(name = "NativeLanguageCode", required = true)
            protected String nativeLanguageCode;
            @XmlElement(name = "NativeLanguageName", required = true)
            protected String nativeLanguageName;
            @XmlElement(name = "NativeLanguageTitle", required = true)
            protected String nativeLanguageTitle;
            @XmlElement(name = "NativeLanguageTitleCode", required = true)
            protected String nativeLanguageTitleCode;
            @XmlElement(name = "IsNegated", required = true)
            protected String isNegated;
            @XmlElement(name = "NegationNotes", required = true)
            protected String negationNotes;
            @XmlElement(name = "NegationReason", required = true)
            protected String negationReason;
            @XmlElement(name = "NickName", required = true)
            protected String nickName;
            @XmlElement(name = "Notes", required = true)
            protected String notes;
            @XmlElement(name = "Occupation", required = true)
            protected String occupation;
            @XmlElement(name = "OccupationCode", required = true)
            protected String occupationCode;
            @XmlElement(name = "OfflineCumDrLimit", required = true)
            protected String offlineCumDrLimit;
            @XmlElement(name = "PAN", required = true)
            protected String pan;
            @XmlElement(name = "PassportNum", required = true)
            protected String passportNum;
            @XmlElement(name = "PersonType", required = true)
            protected String personType;
            @XmlElement(name = "Phone", required = true)
            protected String phone;
            @XmlElement(name = "PhoneEmailInfo")
            protected List<RetCustInqResponse.RetCustInqRs.RetCustDtls.PhoneEmailInfo> phoneEmailInfo;
            @XmlElement(name = "PlaceOfBirth", required = true)
            protected String placeOfBirth;
            @XmlElement(name = "PotentialCrLine", required = true)
            protected String potentialCrLine;
            @XmlElement(name = "PrefCode", required = true)
            protected String prefCode;
            @XmlElement(name = "PrefCodeRefCode", required = true)
            protected String prefCodeRefCode;
            @XmlElement(name = "PrefName", required = true)
            protected String prefName;
            @XmlElement(name = "PreviousName", required = true)
            protected String previousName;
            @XmlElement(name = "PrimaryServiceCentre", required = true)
            protected String primaryServiceCentre;
            @XmlElement(name = "PrimarySolId", required = true)
            protected String primarySolId;
            @XmlElement(name = "PriorityCode", required = true)
            protected String priorityCode;
            @XmlElement(name = "ProofOfAgeDoc", required = true)
            protected String proofOfAgeDoc;
            @XmlElement(name = "ProofOfAgeFlag", required = true)
            protected String proofOfAgeFlag;
            @XmlElement(name = "PassportDtls", required = true)
            protected String passportDtls;
            @XmlElement(name = "PassportExpDt", required = true)
            protected String passportExpDt;
            @XmlElement(name = "Rating", required = true)
            protected String rating;
            @XmlElement(name = "RatingCode", required = true)
            protected String ratingCode;
            @XmlElement(name = "RatingDt", required = true)
            protected String ratingDt;
            @XmlElement(name = "Region", required = true)
            protected String region;
            @XmlElement(name = "RelationshipLevel", required = true)
            protected String relationshipLevel;
            @XmlElement(name = "RelationshipType", required = true)
            protected String relationshipType;
            @XmlElement(name = "RelationshipOpeningDt", required = true)
            protected String relationshipOpeningDt;
            @XmlElement(name = "RelationshipValue", required = true)
            protected String relationshipValue;
            @XmlElement(name = "RetCustAddrInfo")
            protected List<RetCustInqResponse.RetCustInqRs.RetCustDtls.RetCustAddrInfo> retCustAddrInfo;
            @XmlElement(name = "RevenueUnits", required = true)
            protected String revenueUnits;
            @XmlElement(name = "Salutation", required = true)
            protected String salutation;
            @XmlElement(name = "SalutationCode", required = true)
            protected String salutationCode;
            @XmlElement(name = "Sector", required = true)
            protected String sector;
            @XmlElement(name = "SectorCode", required = true)
            protected String sectorCode;
            @XmlElement(name = "SegmentationClass", required = true)
            protected String segmentationClass;
            @XmlElement(name = "ShortName", required = true)
            protected String shortName;
            @XmlElement(name = "ShortNameNative", required = true)
            protected String shortNameNative;
            @XmlElement(name = "ShortNameNative1", required = true)
            protected String shortNameNative1;
            @XmlElement(name = "SICCode", required = true)
            protected String sicCode;
            @XmlElement(name = "SMSBankingMobNum", required = true)
            protected String smsBankingMobNum;
            @XmlElement(name = "SSN", required = true)
            protected String ssn;
            @XmlElement(name = "StaffFlag", required = true)
            protected String staffFlag;
            @XmlElement(name = "StartDt", required = true)
            protected String startDt;
            @XmlElement(name = "Status", required = true)
            protected String status;
            @XmlElement(name = "SubSector", required = true)
            protected String subSector;
            @XmlElement(name = "SubSectorCode", required = true)
            protected String subSectorCode;
            @XmlElement(name = "SubSegment", required = true)
            protected String subSegment;
            @XmlElement(name = "SuspendNotes", required = true)
            protected String suspendNotes;
            @XmlElement(name = "SuspendReason", required = true)
            protected String suspendReason;
            @XmlElement(name = "IsSuspended", required = true)
            protected String isSuspended;
            @XmlElement(name = "TFPartyFlag", required = true)
            protected String tfPartyFlag;
            @XmlElement(name = "TickerSymbol", required = true)
            protected String tickerSymbol;
            @XmlElement(name = "TotalCrExposure", required = true)
            protected String totalCrExposure;
            @XmlElement(name = "PassportIssueDt", required = true)
            protected String passportIssueDt;
            @XmlElement(name = "ForeignAccTaxReportingReq", required = true)
            protected String foreignAccTaxReportingReq;
            @XmlElement(name = "ForeignTaxReportingCountry", required = true)
            protected String foreignTaxReportingCountry;
            @XmlElement(name = "ForeignTaxReportingStatus", required = true)
            protected String foreignTaxReportingStatus;
            @XmlElement(name = "FatcaRemarks", required = true)
            protected String fatcaRemarks;
            @XmlElement(name = "SenCitizenApplicableDate", required = true)
            protected String senCitizenApplicableDate;
            @XmlElement(name = "SeniorCitizen", required = true)
            protected String seniorCitizen;
            @XmlElement(name = "CustStatus", required = true)
            protected String custStatus;
            @XmlElement(name = "PhysicalState", required = true)
            protected String physicalState;
            @XmlElement(name = "AadhaarNumber", required = true)
            protected String aadhaarNumber;
            @XmlElement(name = "StaffEmployeeID", required = true)
            protected String staffEmployeeID;

            /**
             * Gets the value of the acctName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctName() {
                return acctName;
            }

            /**
             * Sets the value of the acctName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctName(String value) {
                this.acctName = value;
            }

            /**
             * Gets the value of the annualRevenue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAnnualRevenue() {
                return annualRevenue;
            }

            /**
             * Sets the value of the annualRevenue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAnnualRevenue(String value) {
                this.annualRevenue = value;
            }

            /**
             * Gets the value of the assistant property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAssistant() {
                return assistant;
            }

            /**
             * Sets the value of the assistant property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAssistant(String value) {
                this.assistant = value;
            }

            /**
             * Gets the value of the availableCrLimit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAvailableCrLimit() {
                return availableCrLimit;
            }

            /**
             * Sets the value of the availableCrLimit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAvailableCrLimit(String value) {
                this.availableCrLimit = value;
            }

            /**
             * Gets the value of the blacklistNotes property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBlacklistNotes() {
                return blacklistNotes;
            }

            /**
             * Sets the value of the blacklistNotes property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBlacklistNotes(String value) {
                this.blacklistNotes = value;
            }

            /**
             * Gets the value of the blacklistReason property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBlacklistReason() {
                return blacklistReason;
            }

            /**
             * Sets the value of the blacklistReason property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBlacklistReason(String value) {
                this.blacklistReason = value;
            }

            /**
             * Gets the value of the isBlacklisted property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsBlacklisted() {
                return isBlacklisted;
            }

            /**
             * Sets the value of the isBlacklisted property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsBlacklisted(String value) {
                this.isBlacklisted = value;
            }

            /**
             * Gets the value of the cardHolder property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCardHolder() {
                return cardHolder;
            }

            /**
             * Sets the value of the cardHolder property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCardHolder(String value) {
                this.cardHolder = value;
            }

            /**
             * Gets the value of the category property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCategory() {
                return category;
            }

            /**
             * Sets the value of the category property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCategory(String value) {
                this.category = value;
            }

            /**
             * Gets the value of the chargeLevelCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChargeLevelCode() {
                return chargeLevelCode;
            }

            /**
             * Sets the value of the chargeLevelCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChargeLevelCode(String value) {
                this.chargeLevelCode = value;
            }

            /**
             * Gets the value of the custId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustId() {
                return custId;
            }

            /**
             * Sets the value of the custId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustId(String value) {
                this.custId = value;
            }

            /**
             * Gets the value of the cin property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCIN() {
                return cin;
            }

            /**
             * Sets the value of the cin property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCIN(String value) {
                this.cin = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the combinedStmtFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCombinedStmtFlag() {
                return combinedStmtFlag;
            }

            /**
             * Sets the value of the combinedStmtFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCombinedStmtFlag(String value) {
                this.combinedStmtFlag = value;
            }

            /**
             * Gets the value of the constitutionCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getConstitutionCode() {
                return constitutionCode;
            }

            /**
             * Sets the value of the constitutionCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setConstitutionCode(String value) {
                this.constitutionCode = value;
            }

            /**
             * Gets the value of the constitutionRefCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getConstitutionRefCode() {
                return constitutionRefCode;
            }

            /**
             * Sets the value of the constitutionRefCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setConstitutionRefCode(String value) {
                this.constitutionRefCode = value;
            }

            /**
             * Gets the value of the corpRepCount property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorpRepCount() {
                return corpRepCount;
            }

            /**
             * Sets the value of the corpRepCount property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorpRepCount(String value) {
                this.corpRepCount = value;
            }

            /**
             * Gets the value of the countryOfBirth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryOfBirth() {
                return countryOfBirth;
            }

            /**
             * Sets the value of the countryOfBirth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryOfBirth(String value) {
                this.countryOfBirth = value;
            }

            /**
             * Gets the value of the createdFrom property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreatedFrom() {
                return createdFrom;
            }

            /**
             * Sets the value of the createdFrom property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreatedFrom(String value) {
                this.createdFrom = value;
            }

            /**
             * Gets the value of the createdBySystemId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreatedBySystemId() {
                return createdBySystemId;
            }

            /**
             * Sets the value of the createdBySystemId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreatedBySystemId(String value) {
                this.createdBySystemId = value;
            }

            /**
             * Gets the value of the currCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrCode() {
                return currCode;
            }

            /**
             * Sets the value of the currCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrCode(String value) {
                this.currCode = value;
            }

            /**
             * Gets the value of the currentCrExposure property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrentCrExposure() {
                return currentCrExposure;
            }

            /**
             * Sets the value of the currentCrExposure property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrentCrExposure(String value) {
                this.currentCrExposure = value;
            }

            /**
             * Gets the value of the communityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCommunityCode() {
                return communityCode;
            }

            /**
             * Sets the value of the communityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCommunityCode(String value) {
                this.communityCode = value;
            }

            /**
             * Gets the value of the community property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCommunity() {
                return community;
            }

            /**
             * Sets the value of the community property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCommunity(String value) {
                this.community = value;
            }

            /**
             * Gets the value of the birthDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBirthDt() {
                return birthDt;
            }

            /**
             * Sets the value of the birthDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBirthDt(String value) {
                this.birthDt = value;
            }

            /**
             * Gets the value of the firstName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstName() {
                return firstName;
            }

            /**
             * Sets the value of the firstName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstName(String value) {
                this.firstName = value;
            }

            /**
             * Gets the value of the firstNameNative property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstNameNative() {
                return firstNameNative;
            }

            /**
             * Sets the value of the firstNameNative property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstNameNative(String value) {
                this.firstNameNative = value;
            }

            /**
             * Gets the value of the firstNameNative1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstNameNative1() {
                return firstNameNative1;
            }

            /**
             * Sets the value of the firstNameNative1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstNameNative1(String value) {
                this.firstNameNative1 = value;
            }

            /**
             * Gets the value of the health property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHealth() {
                return health;
            }

            /**
             * Sets the value of the health property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHealth(String value) {
                this.health = value;
            }

            /**
             * Gets the value of the healthCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHealthCode() {
                return healthCode;
            }

            /**
             * Sets the value of the healthCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHealthCode(String value) {
                this.healthCode = value;
            }

            /**
             * Gets the value of the language property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLanguage() {
                return language;
            }

            /**
             * Sets the value of the language property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLanguage(String value) {
                this.language = value;
            }

            /**
             * Gets the value of the lastName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastName() {
                return lastName;
            }

            /**
             * Sets the value of the lastName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastName(String value) {
                this.lastName = value;
            }

            /**
             * Gets the value of the lastNameNative property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastNameNative() {
                return lastNameNative;
            }

            /**
             * Sets the value of the lastNameNative property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastNameNative(String value) {
                this.lastNameNative = value;
            }

            /**
             * Gets the value of the lastNameNative1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastNameNative1() {
                return lastNameNative1;
            }

            /**
             * Sets the value of the lastNameNative1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastNameNative1(String value) {
                this.lastNameNative1 = value;
            }

            /**
             * Gets the value of the middleName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMiddleName() {
                return middleName;
            }

            /**
             * Sets the value of the middleName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMiddleName(String value) {
                this.middleName = value;
            }

            /**
             * Gets the value of the middleNameNative property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMiddleNameNative() {
                return middleNameNative;
            }

            /**
             * Sets the value of the middleNameNative property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMiddleNameNative(String value) {
                this.middleNameNative = value;
            }

            /**
             * Gets the value of the middleNameNative1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMiddleNameNative1() {
                return middleNameNative1;
            }

            /**
             * Sets the value of the middleNameNative1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMiddleNameNative1(String value) {
                this.middleNameNative1 = value;
            }

            /**
             * Gets the value of the isStaff property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsStaff() {
                return isStaff;
            }

            /**
             * Sets the value of the isStaff property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsStaff(String value) {
                this.isStaff = value;
            }

            /**
             * Gets the value of the swiftCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSwiftCode() {
                return swiftCode;
            }

            /**
             * Sets the value of the swiftCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSwiftCode(String value) {
                this.swiftCode = value;
            }

            /**
             * Gets the value of the custType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustType() {
                return custType;
            }

            /**
             * Sets the value of the custType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustType(String value) {
                this.custType = value;
            }

            /**
             * Gets the value of the custTypeCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustTypeCode() {
                return custTypeCode;
            }

            /**
             * Sets the value of the custTypeCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustTypeCode(String value) {
                this.custTypeCode = value;
            }

            /**
             * Gets the value of the custClass property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustClass() {
                return custClass;
            }

            /**
             * Sets the value of the custClass property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustClass(String value) {
                this.custClass = value;
            }

            /**
             * Gets the value of the isMinor property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsMinor() {
                return isMinor;
            }

            /**
             * Sets the value of the isMinor property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsMinor(String value) {
                this.isMinor = value;
            }

            /**
             * Gets the value of the isNRE property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsNRE() {
                return isNRE;
            }

            /**
             * Sets the value of the isNRE property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsNRE(String value) {
                this.isNRE = value;
            }

            /**
             * Gets the value of the custProfitability property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustProfitability() {
                return custProfitability;
            }

            /**
             * Sets the value of the custProfitability property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustProfitability(String value) {
                this.custProfitability = value;
            }

            /**
             * Gets the value of the custRelNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustRelNum() {
                return custRelNum;
            }

            /**
             * Sets the value of the custRelNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustRelNum(String value) {
                this.custRelNum = value;
            }

            /**
             * Gets the value of the custTrade property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustTrade() {
                return custTrade;
            }

            /**
             * Sets the value of the custTrade property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustTrade(String value) {
                this.custTrade = value;
            }

            /**
             * Gets the value of the statusChangeDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusChangeDt() {
                return statusChangeDt;
            }

            /**
             * Sets the value of the statusChangeDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusChangeDt(String value) {
                this.statusChangeDt = value;
            }

            /**
             * Gets the value of the defaultAddrType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDefaultAddrType() {
                return defaultAddrType;
            }

            /**
             * Sets the value of the defaultAddrType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDefaultAddrType(String value) {
                this.defaultAddrType = value;
            }

            /**
             * Gets the value of the delinquencyFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDelinquencyFlag() {
                return delinquencyFlag;
            }

            /**
             * Sets the value of the delinquencyFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDelinquencyFlag(String value) {
                this.delinquencyFlag = value;
            }

            /**
             * Gets the value of the deliquencyPeriod property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDeliquencyPeriod() {
                return deliquencyPeriod;
            }

            /**
             * Sets the value of the deliquencyPeriod property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDeliquencyPeriod(String value) {
                this.deliquencyPeriod = value;
            }

            /**
             * Gets the value of the designation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDesignation() {
                return designation;
            }

            /**
             * Sets the value of the designation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDesignation(String value) {
                this.designation = value;
            }

            /**
             * Gets the value of the isDocReceived property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsDocReceived() {
                return isDocReceived;
            }

            /**
             * Sets the value of the isDocReceived property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsDocReceived(String value) {
                this.isDocReceived = value;
            }

            /**
             * Gets the value of the dualFirstName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDualFirstName() {
                return dualFirstName;
            }

            /**
             * Sets the value of the dualFirstName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDualFirstName(String value) {
                this.dualFirstName = value;
            }

            /**
             * Gets the value of the dualLastName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDualLastName() {
                return dualLastName;
            }

            /**
             * Sets the value of the dualLastName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDualLastName(String value) {
                this.dualLastName = value;
            }

            /**
             * Gets the value of the dualMiddleName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDualMiddleName() {
                return dualMiddleName;
            }

            /**
             * Sets the value of the dualMiddleName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDualMiddleName(String value) {
                this.dualMiddleName = value;
            }

            /**
             * Gets the value of the education property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEducation() {
                return education;
            }

            /**
             * Sets the value of the education property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEducation(String value) {
                this.education = value;
            }

            /**
             * Gets the value of the entityCreationFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityCreationFlag() {
                return entityCreationFlag;
            }

            /**
             * Sets the value of the entityCreationFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityCreationFlag(String value) {
                this.entityCreationFlag = value;
            }

            /**
             * Gets the value of the extnNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExtnNum() {
                return extnNum;
            }

            /**
             * Sets the value of the extnNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExtnNum(String value) {
                this.extnNum = value;
            }

            /**
             * Gets the value of the fatherOrHusbandName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFatherOrHusbandName() {
                return fatherOrHusbandName;
            }

            /**
             * Sets the value of the fatherOrHusbandName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFatherOrHusbandName(String value) {
                this.fatherOrHusbandName = value;
            }

            /**
             * Gets the value of the fax property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFax() {
                return fax;
            }

            /**
             * Sets the value of the fax property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFax(String value) {
                this.fax = value;
            }

            /**
             * Gets the value of the faxHome property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFaxHome() {
                return faxHome;
            }

            /**
             * Sets the value of the faxHome property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFaxHome(String value) {
                this.faxHome = value;
            }

            /**
             * Gets the value of the gender property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGender() {
                return gender;
            }

            /**
             * Sets the value of the gender property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGender(String value) {
                this.gender = value;
            }

            /**
             * Gets the value of the groupIdCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupIdCode() {
                return groupIdCode;
            }

            /**
             * Sets the value of the groupIdCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupIdCode(String value) {
                this.groupIdCode = value;
            }

            /**
             * Gets the value of the householdName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseholdName() {
                return householdName;
            }

            /**
             * Sets the value of the householdName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseholdName(String value) {
                this.householdName = value;
            }

            /**
             * Gets the value of the incrementalDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIncrementalDt() {
                return incrementalDt;
            }

            /**
             * Sets the value of the incrementalDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIncrementalDt(String value) {
                this.incrementalDt = value;
            }

            /**
             * Gets the value of the internalScore property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInternalScore() {
                return internalScore;
            }

            /**
             * Sets the value of the internalScore property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInternalScore(String value) {
                this.internalScore = value;
            }

            /**
             * Gets the value of the salutationCodeOfIntroducer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSalutationCodeOfIntroducer() {
                return salutationCodeOfIntroducer;
            }

            /**
             * Sets the value of the salutationCodeOfIntroducer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSalutationCodeOfIntroducer(String value) {
                this.salutationCodeOfIntroducer = value;
            }

            /**
             * Gets the value of the statusOfIntroducer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusOfIntroducer() {
                return statusOfIntroducer;
            }

            /**
             * Sets the value of the statusOfIntroducer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusOfIntroducer(String value) {
                this.statusOfIntroducer = value;
            }

            /**
             * Gets the value of the nameOfIntroducer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNameOfIntroducer() {
                return nameOfIntroducer;
            }

            /**
             * Sets the value of the nameOfIntroducer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNameOfIntroducer(String value) {
                this.nameOfIntroducer = value;
            }

            /**
             * Gets the value of the salutationOfIntroducer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSalutationOfIntroducer() {
                return salutationOfIntroducer;
            }

            /**
             * Sets the value of the salutationOfIntroducer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSalutationOfIntroducer(String value) {
                this.salutationOfIntroducer = value;
            }

            /**
             * Gets the value of the isSwiftCodeOfBank property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsSwiftCodeOfBank() {
                return isSwiftCodeOfBank;
            }

            /**
             * Sets the value of the isSwiftCodeOfBank property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsSwiftCodeOfBank(String value) {
                this.isSwiftCodeOfBank = value;
            }

            /**
             * Gets the value of the isCorpRepresentative property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsCorpRepresentative() {
                return isCorpRepresentative;
            }

            /**
             * Sets the value of the isCorpRepresentative property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsCorpRepresentative(String value) {
                this.isCorpRepresentative = value;
            }

            /**
             * Gets the value of the isDummy property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsDummy() {
                return isDummy;
            }

            /**
             * Sets the value of the isDummy property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsDummy(String value) {
                this.isDummy = value;
            }

            /**
             * Gets the value of the isEBankingEnabled property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsEBankingEnabled() {
                return isEBankingEnabled;
            }

            /**
             * Sets the value of the isEBankingEnabled property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsEBankingEnabled(String value) {
                this.isEBankingEnabled = value;
            }

            /**
             * Gets the value of the isSMSBankingEnabled property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsSMSBankingEnabled() {
                return isSMSBankingEnabled;
            }

            /**
             * Sets the value of the isSMSBankingEnabled property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsSMSBankingEnabled(String value) {
                this.isSMSBankingEnabled = value;
            }

            /**
             * Gets the value of the isWAPBankingEnabled property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsWAPBankingEnabled() {
                return isWAPBankingEnabled;
            }

            /**
             * Sets the value of the isWAPBankingEnabled property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsWAPBankingEnabled(String value) {
                this.isWAPBankingEnabled = value;
            }

            /**
             * Gets the value of the leadSource property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLeadSource() {
                return leadSource;
            }

            /**
             * Sets the value of the leadSource property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLeadSource(String value) {
                this.leadSource = value;
            }

            /**
             * Gets the value of the licenseNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLicenseNum() {
                return licenseNum;
            }

            /**
             * Sets the value of the licenseNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLicenseNum(String value) {
                this.licenseNum = value;
            }

            /**
             * Gets the value of the maidenName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMaidenName() {
                return maidenName;
            }

            /**
             * Sets the value of the maidenName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMaidenName(String value) {
                this.maidenName = value;
            }

            /**
             * Gets the value of the maidenNameOfMother property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMaidenNameOfMother() {
                return maidenNameOfMother;
            }

            /**
             * Sets the value of the maidenNameOfMother property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMaidenNameOfMother(String value) {
                this.maidenNameOfMother = value;
            }

            /**
             * Gets the value of the manager property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getManager() {
                return manager;
            }

            /**
             * Sets the value of the manager property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setManager(String value) {
                this.manager = value;
            }

            /**
             * Gets the value of the managerOpinion property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getManagerOpinion() {
                return managerOpinion;
            }

            /**
             * Sets the value of the managerOpinion property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setManagerOpinion(String value) {
                this.managerOpinion = value;
            }

            /**
             * Gets the value of the guardianCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGuardianCode() {
                return guardianCode;
            }

            /**
             * Sets the value of the guardianCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGuardianCode(String value) {
                this.guardianCode = value;
            }

            /**
             * Gets the value of the guardianName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGuardianName() {
                return guardianName;
            }

            /**
             * Sets the value of the guardianName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGuardianName(String value) {
                this.guardianName = value;
            }

            /**
             * Gets the value of the modifiedBySysId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getModifiedBySysId() {
                return modifiedBySysId;
            }

            /**
             * Sets the value of the modifiedBySysId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setModifiedBySysId(String value) {
                this.modifiedBySysId = value;
            }

            /**
             * Gets the value of the motherName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMotherName() {
                return motherName;
            }

            /**
             * Sets the value of the motherName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMotherName(String value) {
                this.motherName = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the nameSuffix property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNameSuffix() {
                return nameSuffix;
            }

            /**
             * Sets the value of the nameSuffix property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNameSuffix(String value) {
                this.nameSuffix = value;
            }

            /**
             * Gets the value of the nationalIdCardNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNationalIdCardNum() {
                return nationalIdCardNum;
            }

            /**
             * Sets the value of the nationalIdCardNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNationalIdCardNum(String value) {
                this.nationalIdCardNum = value;
            }

            /**
             * Gets the value of the nativeLanguage property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNativeLanguage() {
                return nativeLanguage;
            }

            /**
             * Sets the value of the nativeLanguage property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNativeLanguage(String value) {
                this.nativeLanguage = value;
            }

            /**
             * Gets the value of the nativeLanguageCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNativeLanguageCode() {
                return nativeLanguageCode;
            }

            /**
             * Sets the value of the nativeLanguageCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNativeLanguageCode(String value) {
                this.nativeLanguageCode = value;
            }

            /**
             * Gets the value of the nativeLanguageName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNativeLanguageName() {
                return nativeLanguageName;
            }

            /**
             * Sets the value of the nativeLanguageName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNativeLanguageName(String value) {
                this.nativeLanguageName = value;
            }

            /**
             * Gets the value of the nativeLanguageTitle property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNativeLanguageTitle() {
                return nativeLanguageTitle;
            }

            /**
             * Sets the value of the nativeLanguageTitle property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNativeLanguageTitle(String value) {
                this.nativeLanguageTitle = value;
            }

            /**
             * Gets the value of the nativeLanguageTitleCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNativeLanguageTitleCode() {
                return nativeLanguageTitleCode;
            }

            /**
             * Sets the value of the nativeLanguageTitleCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNativeLanguageTitleCode(String value) {
                this.nativeLanguageTitleCode = value;
            }

            /**
             * Gets the value of the isNegated property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsNegated() {
                return isNegated;
            }

            /**
             * Sets the value of the isNegated property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsNegated(String value) {
                this.isNegated = value;
            }

            /**
             * Gets the value of the negationNotes property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNegationNotes() {
                return negationNotes;
            }

            /**
             * Sets the value of the negationNotes property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNegationNotes(String value) {
                this.negationNotes = value;
            }

            /**
             * Gets the value of the negationReason property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNegationReason() {
                return negationReason;
            }

            /**
             * Sets the value of the negationReason property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNegationReason(String value) {
                this.negationReason = value;
            }

            /**
             * Gets the value of the nickName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNickName() {
                return nickName;
            }

            /**
             * Sets the value of the nickName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNickName(String value) {
                this.nickName = value;
            }

            /**
             * Gets the value of the notes property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNotes() {
                return notes;
            }

            /**
             * Sets the value of the notes property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNotes(String value) {
                this.notes = value;
            }

            /**
             * Gets the value of the occupation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOccupation() {
                return occupation;
            }

            /**
             * Sets the value of the occupation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOccupation(String value) {
                this.occupation = value;
            }

            /**
             * Gets the value of the occupationCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOccupationCode() {
                return occupationCode;
            }

            /**
             * Sets the value of the occupationCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOccupationCode(String value) {
                this.occupationCode = value;
            }

            /**
             * Gets the value of the offlineCumDrLimit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOfflineCumDrLimit() {
                return offlineCumDrLimit;
            }

            /**
             * Sets the value of the offlineCumDrLimit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOfflineCumDrLimit(String value) {
                this.offlineCumDrLimit = value;
            }

            /**
             * Gets the value of the pan property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPAN() {
                return pan;
            }

            /**
             * Sets the value of the pan property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPAN(String value) {
                this.pan = value;
            }

            /**
             * Gets the value of the passportNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassportNum() {
                return passportNum;
            }

            /**
             * Sets the value of the passportNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassportNum(String value) {
                this.passportNum = value;
            }

            /**
             * Gets the value of the personType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPersonType() {
                return personType;
            }

            /**
             * Sets the value of the personType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPersonType(String value) {
                this.personType = value;
            }

            /**
             * Gets the value of the phone property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhone() {
                return phone;
            }

            /**
             * Sets the value of the phone property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhone(String value) {
                this.phone = value;
            }

            /**
             * Gets the value of the phoneEmailInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the phoneEmailInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPhoneEmailInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RetCustInqResponse.RetCustInqRs.RetCustDtls.PhoneEmailInfo }
             * 
             * 
             */
            public List<RetCustInqResponse.RetCustInqRs.RetCustDtls.PhoneEmailInfo> getPhoneEmailInfo() {
                if (phoneEmailInfo == null) {
                    phoneEmailInfo = new ArrayList<RetCustInqResponse.RetCustInqRs.RetCustDtls.PhoneEmailInfo>();
                }
                return this.phoneEmailInfo;
            }

            /**
             * Gets the value of the placeOfBirth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlaceOfBirth() {
                return placeOfBirth;
            }

            /**
             * Sets the value of the placeOfBirth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlaceOfBirth(String value) {
                this.placeOfBirth = value;
            }

            /**
             * Gets the value of the potentialCrLine property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPotentialCrLine() {
                return potentialCrLine;
            }

            /**
             * Sets the value of the potentialCrLine property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPotentialCrLine(String value) {
                this.potentialCrLine = value;
            }

            /**
             * Gets the value of the prefCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefCode() {
                return prefCode;
            }

            /**
             * Sets the value of the prefCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefCode(String value) {
                this.prefCode = value;
            }

            /**
             * Gets the value of the prefCodeRefCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefCodeRefCode() {
                return prefCodeRefCode;
            }

            /**
             * Sets the value of the prefCodeRefCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefCodeRefCode(String value) {
                this.prefCodeRefCode = value;
            }

            /**
             * Gets the value of the prefName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefName() {
                return prefName;
            }

            /**
             * Sets the value of the prefName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefName(String value) {
                this.prefName = value;
            }

            /**
             * Gets the value of the previousName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPreviousName() {
                return previousName;
            }

            /**
             * Sets the value of the previousName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPreviousName(String value) {
                this.previousName = value;
            }

            /**
             * Gets the value of the primaryServiceCentre property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryServiceCentre() {
                return primaryServiceCentre;
            }

            /**
             * Sets the value of the primaryServiceCentre property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryServiceCentre(String value) {
                this.primaryServiceCentre = value;
            }

            /**
             * Gets the value of the primarySolId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimarySolId() {
                return primarySolId;
            }

            /**
             * Sets the value of the primarySolId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimarySolId(String value) {
                this.primarySolId = value;
            }

            /**
             * Gets the value of the priorityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPriorityCode() {
                return priorityCode;
            }

            /**
             * Sets the value of the priorityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPriorityCode(String value) {
                this.priorityCode = value;
            }

            /**
             * Gets the value of the proofOfAgeDoc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProofOfAgeDoc() {
                return proofOfAgeDoc;
            }

            /**
             * Sets the value of the proofOfAgeDoc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProofOfAgeDoc(String value) {
                this.proofOfAgeDoc = value;
            }

            /**
             * Gets the value of the proofOfAgeFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProofOfAgeFlag() {
                return proofOfAgeFlag;
            }

            /**
             * Sets the value of the proofOfAgeFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProofOfAgeFlag(String value) {
                this.proofOfAgeFlag = value;
            }

            /**
             * Gets the value of the passportDtls property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassportDtls() {
                return passportDtls;
            }

            /**
             * Sets the value of the passportDtls property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassportDtls(String value) {
                this.passportDtls = value;
            }

            /**
             * Gets the value of the passportExpDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassportExpDt() {
                return passportExpDt;
            }

            /**
             * Sets the value of the passportExpDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassportExpDt(String value) {
                this.passportExpDt = value;
            }

            /**
             * Gets the value of the rating property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRating() {
                return rating;
            }

            /**
             * Sets the value of the rating property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRating(String value) {
                this.rating = value;
            }

            /**
             * Gets the value of the ratingCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRatingCode() {
                return ratingCode;
            }

            /**
             * Sets the value of the ratingCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRatingCode(String value) {
                this.ratingCode = value;
            }

            /**
             * Gets the value of the ratingDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRatingDt() {
                return ratingDt;
            }

            /**
             * Sets the value of the ratingDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRatingDt(String value) {
                this.ratingDt = value;
            }

            /**
             * Gets the value of the region property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegion() {
                return region;
            }

            /**
             * Sets the value of the region property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegion(String value) {
                this.region = value;
            }

            /**
             * Gets the value of the relationshipLevel property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipLevel() {
                return relationshipLevel;
            }

            /**
             * Sets the value of the relationshipLevel property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipLevel(String value) {
                this.relationshipLevel = value;
            }

            /**
             * Gets the value of the relationshipType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipType() {
                return relationshipType;
            }

            /**
             * Sets the value of the relationshipType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipType(String value) {
                this.relationshipType = value;
            }

            /**
             * Gets the value of the relationshipOpeningDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipOpeningDt() {
                return relationshipOpeningDt;
            }

            /**
             * Sets the value of the relationshipOpeningDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipOpeningDt(String value) {
                this.relationshipOpeningDt = value;
            }

            /**
             * Gets the value of the relationshipValue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipValue() {
                return relationshipValue;
            }

            /**
             * Sets the value of the relationshipValue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipValue(String value) {
                this.relationshipValue = value;
            }

            /**
             * Gets the value of the retCustAddrInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the retCustAddrInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRetCustAddrInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RetCustInqResponse.RetCustInqRs.RetCustDtls.RetCustAddrInfo }
             * 
             * 
             */
            public List<RetCustInqResponse.RetCustInqRs.RetCustDtls.RetCustAddrInfo> getRetCustAddrInfo() {
                if (retCustAddrInfo == null) {
                    retCustAddrInfo = new ArrayList<RetCustInqResponse.RetCustInqRs.RetCustDtls.RetCustAddrInfo>();
                }
                return this.retCustAddrInfo;
            }

            /**
             * Gets the value of the revenueUnits property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRevenueUnits() {
                return revenueUnits;
            }

            /**
             * Sets the value of the revenueUnits property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRevenueUnits(String value) {
                this.revenueUnits = value;
            }

            /**
             * Gets the value of the salutation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSalutation() {
                return salutation;
            }

            /**
             * Sets the value of the salutation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSalutation(String value) {
                this.salutation = value;
            }

            /**
             * Gets the value of the salutationCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSalutationCode() {
                return salutationCode;
            }

            /**
             * Sets the value of the salutationCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSalutationCode(String value) {
                this.salutationCode = value;
            }

            /**
             * Gets the value of the sector property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSector() {
                return sector;
            }

            /**
             * Sets the value of the sector property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSector(String value) {
                this.sector = value;
            }

            /**
             * Gets the value of the sectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSectorCode() {
                return sectorCode;
            }

            /**
             * Sets the value of the sectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSectorCode(String value) {
                this.sectorCode = value;
            }

            /**
             * Gets the value of the segmentationClass property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSegmentationClass() {
                return segmentationClass;
            }

            /**
             * Sets the value of the segmentationClass property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSegmentationClass(String value) {
                this.segmentationClass = value;
            }

            /**
             * Gets the value of the shortName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortName() {
                return shortName;
            }

            /**
             * Sets the value of the shortName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortName(String value) {
                this.shortName = value;
            }

            /**
             * Gets the value of the shortNameNative property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortNameNative() {
                return shortNameNative;
            }

            /**
             * Sets the value of the shortNameNative property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortNameNative(String value) {
                this.shortNameNative = value;
            }

            /**
             * Gets the value of the shortNameNative1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortNameNative1() {
                return shortNameNative1;
            }

            /**
             * Sets the value of the shortNameNative1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortNameNative1(String value) {
                this.shortNameNative1 = value;
            }

            /**
             * Gets the value of the sicCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSICCode() {
                return sicCode;
            }

            /**
             * Sets the value of the sicCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSICCode(String value) {
                this.sicCode = value;
            }

            /**
             * Gets the value of the smsBankingMobNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSMSBankingMobNum() {
                return smsBankingMobNum;
            }

            /**
             * Sets the value of the smsBankingMobNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSMSBankingMobNum(String value) {
                this.smsBankingMobNum = value;
            }

            /**
             * Gets the value of the ssn property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSSN() {
                return ssn;
            }

            /**
             * Sets the value of the ssn property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSSN(String value) {
                this.ssn = value;
            }

            /**
             * Gets the value of the staffFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStaffFlag() {
                return staffFlag;
            }

            /**
             * Sets the value of the staffFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStaffFlag(String value) {
                this.staffFlag = value;
            }

            /**
             * Gets the value of the startDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStartDt() {
                return startDt;
            }

            /**
             * Sets the value of the startDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStartDt(String value) {
                this.startDt = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

            /**
             * Gets the value of the subSector property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubSector() {
                return subSector;
            }

            /**
             * Sets the value of the subSector property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubSector(String value) {
                this.subSector = value;
            }

            /**
             * Gets the value of the subSectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubSectorCode() {
                return subSectorCode;
            }

            /**
             * Sets the value of the subSectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubSectorCode(String value) {
                this.subSectorCode = value;
            }

            /**
             * Gets the value of the subSegment property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubSegment() {
                return subSegment;
            }

            /**
             * Sets the value of the subSegment property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubSegment(String value) {
                this.subSegment = value;
            }

            /**
             * Gets the value of the suspendNotes property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSuspendNotes() {
                return suspendNotes;
            }

            /**
             * Sets the value of the suspendNotes property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSuspendNotes(String value) {
                this.suspendNotes = value;
            }

            /**
             * Gets the value of the suspendReason property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSuspendReason() {
                return suspendReason;
            }

            /**
             * Sets the value of the suspendReason property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSuspendReason(String value) {
                this.suspendReason = value;
            }

            /**
             * Gets the value of the isSuspended property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsSuspended() {
                return isSuspended;
            }

            /**
             * Sets the value of the isSuspended property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsSuspended(String value) {
                this.isSuspended = value;
            }

            /**
             * Gets the value of the tfPartyFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTFPartyFlag() {
                return tfPartyFlag;
            }

            /**
             * Sets the value of the tfPartyFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTFPartyFlag(String value) {
                this.tfPartyFlag = value;
            }

            /**
             * Gets the value of the tickerSymbol property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTickerSymbol() {
                return tickerSymbol;
            }

            /**
             * Sets the value of the tickerSymbol property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTickerSymbol(String value) {
                this.tickerSymbol = value;
            }

            /**
             * Gets the value of the totalCrExposure property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTotalCrExposure() {
                return totalCrExposure;
            }

            /**
             * Sets the value of the totalCrExposure property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTotalCrExposure(String value) {
                this.totalCrExposure = value;
            }

            /**
             * Gets the value of the passportIssueDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPassportIssueDt() {
                return passportIssueDt;
            }

            /**
             * Sets the value of the passportIssueDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPassportIssueDt(String value) {
                this.passportIssueDt = value;
            }

            /**
             * Gets the value of the foreignAccTaxReportingReq property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignAccTaxReportingReq() {
                return foreignAccTaxReportingReq;
            }

            /**
             * Sets the value of the foreignAccTaxReportingReq property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignAccTaxReportingReq(String value) {
                this.foreignAccTaxReportingReq = value;
            }

            /**
             * Gets the value of the foreignTaxReportingCountry property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxReportingCountry() {
                return foreignTaxReportingCountry;
            }

            /**
             * Sets the value of the foreignTaxReportingCountry property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxReportingCountry(String value) {
                this.foreignTaxReportingCountry = value;
            }

            /**
             * Gets the value of the foreignTaxReportingStatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxReportingStatus() {
                return foreignTaxReportingStatus;
            }

            /**
             * Sets the value of the foreignTaxReportingStatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxReportingStatus(String value) {
                this.foreignTaxReportingStatus = value;
            }

            /**
             * Gets the value of the fatcaRemarks property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFatcaRemarks() {
                return fatcaRemarks;
            }

            /**
             * Sets the value of the fatcaRemarks property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFatcaRemarks(String value) {
                this.fatcaRemarks = value;
            }

            /**
             * Gets the value of the senCitizenApplicableDate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSenCitizenApplicableDate() {
                return senCitizenApplicableDate;
            }

            /**
             * Sets the value of the senCitizenApplicableDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSenCitizenApplicableDate(String value) {
                this.senCitizenApplicableDate = value;
            }

            /**
             * Gets the value of the seniorCitizen property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSeniorCitizen() {
                return seniorCitizen;
            }

            /**
             * Sets the value of the seniorCitizen property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSeniorCitizen(String value) {
                this.seniorCitizen = value;
            }

            /**
             * Gets the value of the custStatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustStatus() {
                return custStatus;
            }

            /**
             * Sets the value of the custStatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustStatus(String value) {
                this.custStatus = value;
            }

            /**
             * Gets the value of the physicalState property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhysicalState() {
                return physicalState;
            }

            /**
             * Sets the value of the physicalState property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhysicalState(String value) {
                this.physicalState = value;
            }

            /**
             * Gets the value of the aadhaarNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAadhaarNumber() {
                return aadhaarNumber;
            }

            /**
             * Sets the value of the aadhaarNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAadhaarNumber(String value) {
                this.aadhaarNumber = value;
            }

            /**
             * Gets the value of the staffEmployeeID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStaffEmployeeID() {
                return staffEmployeeID;
            }

            /**
             * Sets the value of the staffEmployeeID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStaffEmployeeID(String value) {
                this.staffEmployeeID = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="PhoneEmailInfo">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value=""/>
             *               &lt;enumeration value="noemail@fcmb.com"/>
             *               &lt;enumeration value="felixgrammar@yahoo.com"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="EmailPalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PhoneEmailType">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="COMM1"/>
             *               &lt;enumeration value="WORK1"/>
             *               &lt;enumeration value="COMML"/>
             *               &lt;enumeration value="WORKL"/>
             *               &lt;enumeration value="HOMEL"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="PhoneNum">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="+234(0)08052003607"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="PhoneNumCityCode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="0"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="PhoneNumCountryCode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="234"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="PhoneNumLocalCode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="08052003607"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="PhoneOrEmail">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="PHONE"/>
             *               &lt;enumeration value="EMAIL"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="PrefFlag">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="Y"/>
             *               &lt;enumeration value="N"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="WorkExtnNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PhoneEmailID">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="31685464"/>
             *               &lt;enumeration value="31685465"/>
             *               &lt;enumeration value="31685466"/>
             *               &lt;enumeration value="31685467"/>
             *               &lt;enumeration value="31685468"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "phoneEmailInfo",
                "emailPalm",
                "phoneEmailType",
                "phoneNum",
                "phoneNumCityCode",
                "phoneNumCountryCode",
                "phoneNumLocalCode",
                "phoneOrEmail",
                "prefFlag",
                "workExtnNum",
                "phoneEmailID"
            })
            public static class PhoneEmailInfo implements Serializable{

                @XmlElement(name = "PhoneEmailInfo", required = true)
                protected String phoneEmailInfo;
                @XmlElement(name = "EmailPalm", required = true)
                protected String emailPalm;
                @XmlElement(name = "PhoneEmailType", required = true)
                protected String phoneEmailType;
                @XmlElement(name = "PhoneNum", required = true)
                protected String phoneNum;
                @XmlElement(name = "PhoneNumCityCode", required = true)
                protected String phoneNumCityCode;
                @XmlElement(name = "PhoneNumCountryCode", required = true)
                protected String phoneNumCountryCode;
                @XmlElement(name = "PhoneNumLocalCode", required = true)
                protected String phoneNumLocalCode;
                @XmlElement(name = "PhoneOrEmail", required = true)
                protected String phoneOrEmail;
                @XmlElement(name = "PrefFlag", required = true)
                protected String prefFlag;
                @XmlElement(name = "WorkExtnNum", required = true)
                protected String workExtnNum;
                @XmlElement(name = "PhoneEmailID", required = true)
                protected String phoneEmailID;

                /**
                 * Gets the value of the phoneEmailInfo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneEmailInfo() {
                    return phoneEmailInfo;
                }

                /**
                 * Sets the value of the phoneEmailInfo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneEmailInfo(String value) {
                    this.phoneEmailInfo = value;
                }

                /**
                 * Gets the value of the emailPalm property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEmailPalm() {
                    return emailPalm;
                }

                /**
                 * Sets the value of the emailPalm property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEmailPalm(String value) {
                    this.emailPalm = value;
                }

                /**
                 * Gets the value of the phoneEmailType property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneEmailType() {
                    return phoneEmailType;
                }

                /**
                 * Sets the value of the phoneEmailType property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneEmailType(String value) {
                    this.phoneEmailType = value;
                }

                /**
                 * Gets the value of the phoneNum property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneNum() {
                    return phoneNum;
                }

                /**
                 * Sets the value of the phoneNum property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneNum(String value) {
                    this.phoneNum = value;
                }

                /**
                 * Gets the value of the phoneNumCityCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneNumCityCode() {
                    return phoneNumCityCode;
                }

                /**
                 * Sets the value of the phoneNumCityCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneNumCityCode(String value) {
                    this.phoneNumCityCode = value;
                }

                /**
                 * Gets the value of the phoneNumCountryCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneNumCountryCode() {
                    return phoneNumCountryCode;
                }

                /**
                 * Sets the value of the phoneNumCountryCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneNumCountryCode(String value) {
                    this.phoneNumCountryCode = value;
                }

                /**
                 * Gets the value of the phoneNumLocalCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneNumLocalCode() {
                    return phoneNumLocalCode;
                }

                /**
                 * Sets the value of the phoneNumLocalCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneNumLocalCode(String value) {
                    this.phoneNumLocalCode = value;
                }

                /**
                 * Gets the value of the phoneOrEmail property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneOrEmail() {
                    return phoneOrEmail;
                }

                /**
                 * Sets the value of the phoneOrEmail property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneOrEmail(String value) {
                    this.phoneOrEmail = value;
                }

                /**
                 * Gets the value of the prefFlag property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPrefFlag() {
                    return prefFlag;
                }

                /**
                 * Sets the value of the prefFlag property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPrefFlag(String value) {
                    this.prefFlag = value;
                }

                /**
                 * Gets the value of the workExtnNum property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getWorkExtnNum() {
                    return workExtnNum;
                }

                /**
                 * Sets the value of the workExtnNum property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setWorkExtnNum(String value) {
                    this.workExtnNum = value;
                }

                /**
                 * Gets the value of the phoneEmailID property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneEmailID() {
                    return phoneEmailID;
                }

                /**
                 * Sets the value of the phoneEmailID property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneEmailID(String value) {
                    this.phoneEmailID = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="AddrLine1">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="NO ADDRESS IN FIN7"/>
             *               &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="AddrLine2">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value=""/>
             *               &lt;enumeration value="WARRI"/>
             *               &lt;enumeration value="."/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="AddrLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="AddrStartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="AddrCategory">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="Home"/>
             *               &lt;enumeration value="Mailing"/>
             *               &lt;enumeration value="Work"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="BuildingLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="BusinessCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CellNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="City">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="LAG"/>
             *               &lt;enumeration value="WAR"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="CityCode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="LAG"/>
             *               &lt;enumeration value="WAR"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="EndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="FaxNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="FaxNumCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="FaxNumCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="FaxNumLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="HoldMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="HoldMailInitiatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="HoldMailReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="HouseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="LocalityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="MailStop" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PagerNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PagerNumCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PagerNumCcountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PagerNumLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PhoneNum1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PrefAddr">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="N"/>
             *               &lt;enumeration value="Y"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="PrefFormat" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PremiseName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ResidentialStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="State">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="LG"/>
             *               &lt;enumeration value="DL"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="StateCode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="LG"/>
             *               &lt;enumeration value="DL"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="StreetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="StreetNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="TelexCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="TelexCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="TelexLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Town" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="isAddressVerified" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="AddressID">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="4091277"/>
             *               &lt;enumeration value="4091275"/>
             *               &lt;enumeration value="4091276"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "addrLine1",
                "addrLine2",
                "addrLine3",
                "addrStartDt",
                "addrCategory",
                "buildingLevel",
                "businessCenter",
                "cellNum",
                "city",
                "cityCode",
                "country",
                "countryCode",
                "domicile",
                "email",
                "endDt",
                "faxNum",
                "faxNumCityCode",
                "faxNumCountryCode",
                "faxNumLocalCode",
                "holdMailFlag",
                "holdMailInitiatedBy",
                "holdMailReason",
                "houseNum",
                "localityName",
                "mailStop",
                "name",
                "pagerNum",
                "pagerNumCityCode",
                "pagerNumCcountryCode",
                "pagerNumLocalCode",
                "phoneNum1",
                "prefAddr",
                "prefFormat",
                "premiseName",
                "residentialStatus",
                "salutationCode",
                "state",
                "stateCode",
                "streetName",
                "streetNum",
                "suburb",
                "telex",
                "telexCityCode",
                "telexCountryCode",
                "telexLocalCode",
                "town",
                "postalCode",
                "isAddressVerified",
                "addressID"
            })
            public static class RetCustAddrInfo implements Serializable{

                @XmlElement(name = "AddrLine1", required = true)
                protected String addrLine1;
                @XmlElement(name = "AddrLine2", required = true)
                protected String addrLine2;
                @XmlElement(name = "AddrLine3", required = true)
                protected String addrLine3;
                @XmlElement(name = "AddrStartDt", required = true)
                protected String addrStartDt;
                @XmlElement(name = "AddrCategory", required = true)
                protected String addrCategory;
                @XmlElement(name = "BuildingLevel", required = true)
                protected String buildingLevel;
                @XmlElement(name = "BusinessCenter", required = true)
                protected String businessCenter;
                @XmlElement(name = "CellNum", required = true)
                protected String cellNum;
                @XmlElement(name = "City", required = true)
                protected String city;
                @XmlElement(name = "CityCode", required = true)
                protected String cityCode;
                @XmlElement(name = "Country", required = true)
                protected String country;
                @XmlElement(name = "CountryCode", required = true)
                protected String countryCode;
                @XmlElement(name = "Domicile", required = true)
                protected String domicile;
                @XmlElement(name = "Email", required = true)
                protected String email;
                @XmlElement(name = "EndDt", required = true)
                protected String endDt;
                @XmlElement(name = "FaxNum", required = true)
                protected String faxNum;
                @XmlElement(name = "FaxNumCityCode", required = true)
                protected String faxNumCityCode;
                @XmlElement(name = "FaxNumCountryCode", required = true)
                protected String faxNumCountryCode;
                @XmlElement(name = "FaxNumLocalCode", required = true)
                protected String faxNumLocalCode;
                @XmlElement(name = "HoldMailFlag", required = true)
                protected String holdMailFlag;
                @XmlElement(name = "HoldMailInitiatedBy", required = true)
                protected String holdMailInitiatedBy;
                @XmlElement(name = "HoldMailReason", required = true)
                protected String holdMailReason;
                @XmlElement(name = "HouseNum", required = true)
                protected String houseNum;
                @XmlElement(name = "LocalityName", required = true)
                protected String localityName;
                @XmlElement(name = "MailStop", required = true)
                protected String mailStop;
                @XmlElement(name = "Name", required = true)
                protected String name;
                @XmlElement(name = "PagerNum", required = true)
                protected String pagerNum;
                @XmlElement(name = "PagerNumCityCode", required = true)
                protected String pagerNumCityCode;
                @XmlElement(name = "PagerNumCcountryCode", required = true)
                protected String pagerNumCcountryCode;
                @XmlElement(name = "PagerNumLocalCode", required = true)
                protected String pagerNumLocalCode;
                @XmlElement(name = "PhoneNum1", required = true)
                protected String phoneNum1;
                @XmlElement(name = "PrefAddr", required = true)
                protected String prefAddr;
                @XmlElement(name = "PrefFormat", required = true)
                protected String prefFormat;
                @XmlElement(name = "PremiseName", required = true)
                protected String premiseName;
                @XmlElement(name = "ResidentialStatus", required = true)
                protected String residentialStatus;
                @XmlElement(name = "SalutationCode", required = true)
                protected String salutationCode;
                @XmlElement(name = "State", required = true)
                protected String state;
                @XmlElement(name = "StateCode", required = true)
                protected String stateCode;
                @XmlElement(name = "StreetName", required = true)
                protected String streetName;
                @XmlElement(name = "StreetNum", required = true)
                protected String streetNum;
                @XmlElement(name = "Suburb", required = true)
                protected String suburb;
                @XmlElement(name = "Telex", required = true)
                protected String telex;
                @XmlElement(name = "TelexCityCode", required = true)
                protected String telexCityCode;
                @XmlElement(name = "TelexCountryCode", required = true)
                protected String telexCountryCode;
                @XmlElement(name = "TelexLocalCode", required = true)
                protected String telexLocalCode;
                @XmlElement(name = "Town", required = true)
                protected String town;
                @XmlElement(name = "PostalCode", required = true)
                protected String postalCode;
                @XmlElement(required = true)
                protected String isAddressVerified;
                @XmlElement(name = "AddressID", required = true)
                protected String addressID;

                /**
                 * Gets the value of the addrLine1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddrLine1() {
                    return addrLine1;
                }

                /**
                 * Sets the value of the addrLine1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddrLine1(String value) {
                    this.addrLine1 = value;
                }

                /**
                 * Gets the value of the addrLine2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddrLine2() {
                    return addrLine2;
                }

                /**
                 * Sets the value of the addrLine2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddrLine2(String value) {
                    this.addrLine2 = value;
                }

                /**
                 * Gets the value of the addrLine3 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddrLine3() {
                    return addrLine3;
                }

                /**
                 * Sets the value of the addrLine3 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddrLine3(String value) {
                    this.addrLine3 = value;
                }

                /**
                 * Gets the value of the addrStartDt property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddrStartDt() {
                    return addrStartDt;
                }

                /**
                 * Sets the value of the addrStartDt property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddrStartDt(String value) {
                    this.addrStartDt = value;
                }

                /**
                 * Gets the value of the addrCategory property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddrCategory() {
                    return addrCategory;
                }

                /**
                 * Sets the value of the addrCategory property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddrCategory(String value) {
                    this.addrCategory = value;
                }

                /**
                 * Gets the value of the buildingLevel property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBuildingLevel() {
                    return buildingLevel;
                }

                /**
                 * Sets the value of the buildingLevel property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBuildingLevel(String value) {
                    this.buildingLevel = value;
                }

                /**
                 * Gets the value of the businessCenter property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBusinessCenter() {
                    return businessCenter;
                }

                /**
                 * Sets the value of the businessCenter property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBusinessCenter(String value) {
                    this.businessCenter = value;
                }

                /**
                 * Gets the value of the cellNum property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCellNum() {
                    return cellNum;
                }

                /**
                 * Sets the value of the cellNum property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCellNum(String value) {
                    this.cellNum = value;
                }

                /**
                 * Gets the value of the city property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCity() {
                    return city;
                }

                /**
                 * Sets the value of the city property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCity(String value) {
                    this.city = value;
                }

                /**
                 * Gets the value of the cityCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCityCode() {
                    return cityCode;
                }

                /**
                 * Sets the value of the cityCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCityCode(String value) {
                    this.cityCode = value;
                }

                /**
                 * Gets the value of the country property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountry() {
                    return country;
                }

                /**
                 * Sets the value of the country property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountry(String value) {
                    this.country = value;
                }

                /**
                 * Gets the value of the countryCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountryCode() {
                    return countryCode;
                }

                /**
                 * Sets the value of the countryCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountryCode(String value) {
                    this.countryCode = value;
                }

                /**
                 * Gets the value of the domicile property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDomicile() {
                    return domicile;
                }

                /**
                 * Sets the value of the domicile property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDomicile(String value) {
                    this.domicile = value;
                }

                /**
                 * Gets the value of the email property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEmail() {
                    return email;
                }

                /**
                 * Sets the value of the email property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEmail(String value) {
                    this.email = value;
                }

                /**
                 * Gets the value of the endDt property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEndDt() {
                    return endDt;
                }

                /**
                 * Sets the value of the endDt property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEndDt(String value) {
                    this.endDt = value;
                }

                /**
                 * Gets the value of the faxNum property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxNum() {
                    return faxNum;
                }

                /**
                 * Sets the value of the faxNum property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxNum(String value) {
                    this.faxNum = value;
                }

                /**
                 * Gets the value of the faxNumCityCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxNumCityCode() {
                    return faxNumCityCode;
                }

                /**
                 * Sets the value of the faxNumCityCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxNumCityCode(String value) {
                    this.faxNumCityCode = value;
                }

                /**
                 * Gets the value of the faxNumCountryCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxNumCountryCode() {
                    return faxNumCountryCode;
                }

                /**
                 * Sets the value of the faxNumCountryCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxNumCountryCode(String value) {
                    this.faxNumCountryCode = value;
                }

                /**
                 * Gets the value of the faxNumLocalCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxNumLocalCode() {
                    return faxNumLocalCode;
                }

                /**
                 * Sets the value of the faxNumLocalCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxNumLocalCode(String value) {
                    this.faxNumLocalCode = value;
                }

                /**
                 * Gets the value of the holdMailFlag property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHoldMailFlag() {
                    return holdMailFlag;
                }

                /**
                 * Sets the value of the holdMailFlag property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHoldMailFlag(String value) {
                    this.holdMailFlag = value;
                }

                /**
                 * Gets the value of the holdMailInitiatedBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHoldMailInitiatedBy() {
                    return holdMailInitiatedBy;
                }

                /**
                 * Sets the value of the holdMailInitiatedBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHoldMailInitiatedBy(String value) {
                    this.holdMailInitiatedBy = value;
                }

                /**
                 * Gets the value of the holdMailReason property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHoldMailReason() {
                    return holdMailReason;
                }

                /**
                 * Sets the value of the holdMailReason property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHoldMailReason(String value) {
                    this.holdMailReason = value;
                }

                /**
                 * Gets the value of the houseNum property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseNum() {
                    return houseNum;
                }

                /**
                 * Sets the value of the houseNum property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseNum(String value) {
                    this.houseNum = value;
                }

                /**
                 * Gets the value of the localityName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocalityName() {
                    return localityName;
                }

                /**
                 * Sets the value of the localityName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocalityName(String value) {
                    this.localityName = value;
                }

                /**
                 * Gets the value of the mailStop property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMailStop() {
                    return mailStop;
                }

                /**
                 * Sets the value of the mailStop property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMailStop(String value) {
                    this.mailStop = value;
                }

                /**
                 * Gets the value of the name property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Sets the value of the name property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setName(String value) {
                    this.name = value;
                }

                /**
                 * Gets the value of the pagerNum property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPagerNum() {
                    return pagerNum;
                }

                /**
                 * Sets the value of the pagerNum property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPagerNum(String value) {
                    this.pagerNum = value;
                }

                /**
                 * Gets the value of the pagerNumCityCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPagerNumCityCode() {
                    return pagerNumCityCode;
                }

                /**
                 * Sets the value of the pagerNumCityCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPagerNumCityCode(String value) {
                    this.pagerNumCityCode = value;
                }

                /**
                 * Gets the value of the pagerNumCcountryCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPagerNumCcountryCode() {
                    return pagerNumCcountryCode;
                }

                /**
                 * Sets the value of the pagerNumCcountryCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPagerNumCcountryCode(String value) {
                    this.pagerNumCcountryCode = value;
                }

                /**
                 * Gets the value of the pagerNumLocalCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPagerNumLocalCode() {
                    return pagerNumLocalCode;
                }

                /**
                 * Sets the value of the pagerNumLocalCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPagerNumLocalCode(String value) {
                    this.pagerNumLocalCode = value;
                }

                /**
                 * Gets the value of the phoneNum1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneNum1() {
                    return phoneNum1;
                }

                /**
                 * Sets the value of the phoneNum1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneNum1(String value) {
                    this.phoneNum1 = value;
                }

                /**
                 * Gets the value of the prefAddr property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPrefAddr() {
                    return prefAddr;
                }

                /**
                 * Sets the value of the prefAddr property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPrefAddr(String value) {
                    this.prefAddr = value;
                }

                /**
                 * Gets the value of the prefFormat property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPrefFormat() {
                    return prefFormat;
                }

                /**
                 * Sets the value of the prefFormat property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPrefFormat(String value) {
                    this.prefFormat = value;
                }

                /**
                 * Gets the value of the premiseName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPremiseName() {
                    return premiseName;
                }

                /**
                 * Sets the value of the premiseName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPremiseName(String value) {
                    this.premiseName = value;
                }

                /**
                 * Gets the value of the residentialStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResidentialStatus() {
                    return residentialStatus;
                }

                /**
                 * Sets the value of the residentialStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResidentialStatus(String value) {
                    this.residentialStatus = value;
                }

                /**
                 * Gets the value of the salutationCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSalutationCode() {
                    return salutationCode;
                }

                /**
                 * Sets the value of the salutationCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSalutationCode(String value) {
                    this.salutationCode = value;
                }

                /**
                 * Gets the value of the state property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getState() {
                    return state;
                }

                /**
                 * Sets the value of the state property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setState(String value) {
                    this.state = value;
                }

                /**
                 * Gets the value of the stateCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStateCode() {
                    return stateCode;
                }

                /**
                 * Sets the value of the stateCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStateCode(String value) {
                    this.stateCode = value;
                }

                /**
                 * Gets the value of the streetName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStreetName() {
                    return streetName;
                }

                /**
                 * Sets the value of the streetName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStreetName(String value) {
                    this.streetName = value;
                }

                /**
                 * Gets the value of the streetNum property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStreetNum() {
                    return streetNum;
                }

                /**
                 * Sets the value of the streetNum property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStreetNum(String value) {
                    this.streetNum = value;
                }

                /**
                 * Gets the value of the suburb property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSuburb() {
                    return suburb;
                }

                /**
                 * Sets the value of the suburb property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSuburb(String value) {
                    this.suburb = value;
                }

                /**
                 * Gets the value of the telex property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTelex() {
                    return telex;
                }

                /**
                 * Sets the value of the telex property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTelex(String value) {
                    this.telex = value;
                }

                /**
                 * Gets the value of the telexCityCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTelexCityCode() {
                    return telexCityCode;
                }

                /**
                 * Sets the value of the telexCityCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTelexCityCode(String value) {
                    this.telexCityCode = value;
                }

                /**
                 * Gets the value of the telexCountryCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTelexCountryCode() {
                    return telexCountryCode;
                }

                /**
                 * Sets the value of the telexCountryCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTelexCountryCode(String value) {
                    this.telexCountryCode = value;
                }

                /**
                 * Gets the value of the telexLocalCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTelexLocalCode() {
                    return telexLocalCode;
                }

                /**
                 * Sets the value of the telexLocalCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTelexLocalCode(String value) {
                    this.telexLocalCode = value;
                }

                /**
                 * Gets the value of the town property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTown() {
                    return town;
                }

                /**
                 * Sets the value of the town property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTown(String value) {
                    this.town = value;
                }

                /**
                 * Gets the value of the postalCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPostalCode() {
                    return postalCode;
                }

                /**
                 * Sets the value of the postalCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPostalCode(String value) {
                    this.postalCode = value;
                }

                /**
                 * Gets the value of the isAddressVerified property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIsAddressVerified() {
                    return isAddressVerified;
                }

                /**
                 * Sets the value of the isAddressVerified property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIsAddressVerified(String value) {
                    this.isAddressVerified = value;
                }

                /**
                 * Gets the value of the addressID property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddressID() {
                    return addressID;
                }

                /**
                 * Sets the value of the addressID property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddressID(String value) {
                    this.addressID = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="retBaselID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DebtHELOC" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CurrFICOScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TotalDSR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BusinessTotalScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HasRelationship" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CombinedDSR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BusinessDSR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CashAssetRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DebtWorthRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="InterestTaxRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GeneralQuickRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ScoredSICCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OwnerYears" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NetWorth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DDABal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CombinedDebtRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProposedLeverage" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GLBLCashCoverage" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CurrentRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LiquidityRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MinTangibleWorth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FICOScore" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FinInqCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FinTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="InquiryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MinorDerogatoryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MajorDerogatoryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NeverPastDueCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OpenTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HighestCreditLmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TradeCntThirty" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TradeCntSixty" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TradeCntNinety" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RevolveDebtPrcnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SatisfactoryCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="InstLoanBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="InstLoanCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MortgageBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MortgageTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OtherTradeBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OtherTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RevolveBal" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RevolveTradeLmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RevolveTradeCnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CCLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsWorst" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpenseIncomeRatio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SgmntPoolID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PoolPD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PoolLGD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PoolEAD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LGD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EAD" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ModelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ModelVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ModelResult" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsFailed" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "retBaselID",
            "debtHELOC",
            "currFICOScore",
            "totalDSR",
            "businessTotalScore",
            "hasRelationship",
            "combinedDSR",
            "businessDSR",
            "cashAssetRatio",
            "debtWorthRatio",
            "interestTaxRatio",
            "generalQuickRatio",
            "scoredSICCode",
            "ownerYears",
            "netWorth",
            "ddaBal",
            "combinedDebtRatio",
            "proposedLeverage",
            "glblCashCoverage",
            "currentRatio",
            "liquidityRatio",
            "minTangibleWorth",
            "ficoScore",
            "finInqCnt",
            "finTradeCnt",
            "inquiryCnt",
            "minorDerogatoryCnt",
            "majorDerogatoryCnt",
            "neverPastDueCnt",
            "openTradeCnt",
            "highestCreditLmt",
            "tradeCntThirty",
            "tradeCntSixty",
            "tradeCntNinety",
            "revolveDebtPrcnt",
            "satisfactoryCnt",
            "instLoanBal",
            "instLoanCnt",
            "mortgageBal",
            "mortgageTradeCnt",
            "otherTradeBal",
            "otherTradeCnt",
            "revolveBal",
            "revolveTradeLmt",
            "revolveTradeCnt",
            "ccLimit",
            "isWorst",
            "expenseIncomeRatio",
            "sgmntPoolID",
            "poolPD",
            "poolLGD",
            "poolEAD",
            "pd",
            "lgd",
            "ead",
            "modelName",
            "modelVersion",
            "modelResult",
            "isFailed"
        })
        public static class RetailBaselDtls implements Serializable{

            @XmlElement(required = true)
            protected String retBaselID;
            @XmlElement(name = "DebtHELOC", required = true)
            protected String debtHELOC;
            @XmlElement(name = "CurrFICOScore", required = true)
            protected String currFICOScore;
            @XmlElement(name = "TotalDSR", required = true)
            protected String totalDSR;
            @XmlElement(name = "BusinessTotalScore", required = true)
            protected String businessTotalScore;
            @XmlElement(name = "HasRelationship", required = true)
            protected String hasRelationship;
            @XmlElement(name = "CombinedDSR", required = true)
            protected String combinedDSR;
            @XmlElement(name = "BusinessDSR", required = true)
            protected String businessDSR;
            @XmlElement(name = "CashAssetRatio", required = true)
            protected String cashAssetRatio;
            @XmlElement(name = "DebtWorthRatio", required = true)
            protected String debtWorthRatio;
            @XmlElement(name = "InterestTaxRatio", required = true)
            protected String interestTaxRatio;
            @XmlElement(name = "GeneralQuickRatio", required = true)
            protected String generalQuickRatio;
            @XmlElement(name = "ScoredSICCode", required = true)
            protected String scoredSICCode;
            @XmlElement(name = "OwnerYears", required = true)
            protected String ownerYears;
            @XmlElement(name = "NetWorth", required = true)
            protected String netWorth;
            @XmlElement(name = "DDABal", required = true)
            protected String ddaBal;
            @XmlElement(name = "CombinedDebtRatio", required = true)
            protected String combinedDebtRatio;
            @XmlElement(name = "ProposedLeverage", required = true)
            protected String proposedLeverage;
            @XmlElement(name = "GLBLCashCoverage", required = true)
            protected String glblCashCoverage;
            @XmlElement(name = "CurrentRatio", required = true)
            protected String currentRatio;
            @XmlElement(name = "LiquidityRatio", required = true)
            protected String liquidityRatio;
            @XmlElement(name = "MinTangibleWorth", required = true)
            protected String minTangibleWorth;
            @XmlElement(name = "FICOScore", required = true)
            protected String ficoScore;
            @XmlElement(name = "FinInqCnt", required = true)
            protected String finInqCnt;
            @XmlElement(name = "FinTradeCnt", required = true)
            protected String finTradeCnt;
            @XmlElement(name = "InquiryCnt", required = true)
            protected String inquiryCnt;
            @XmlElement(name = "MinorDerogatoryCnt", required = true)
            protected String minorDerogatoryCnt;
            @XmlElement(name = "MajorDerogatoryCnt", required = true)
            protected String majorDerogatoryCnt;
            @XmlElement(name = "NeverPastDueCnt", required = true)
            protected String neverPastDueCnt;
            @XmlElement(name = "OpenTradeCnt", required = true)
            protected String openTradeCnt;
            @XmlElement(name = "HighestCreditLmt", required = true)
            protected String highestCreditLmt;
            @XmlElement(name = "TradeCntThirty", required = true)
            protected String tradeCntThirty;
            @XmlElement(name = "TradeCntSixty", required = true)
            protected String tradeCntSixty;
            @XmlElement(name = "TradeCntNinety", required = true)
            protected String tradeCntNinety;
            @XmlElement(name = "RevolveDebtPrcnt", required = true)
            protected String revolveDebtPrcnt;
            @XmlElement(name = "SatisfactoryCnt", required = true)
            protected String satisfactoryCnt;
            @XmlElement(name = "InstLoanBal", required = true)
            protected String instLoanBal;
            @XmlElement(name = "InstLoanCnt", required = true)
            protected String instLoanCnt;
            @XmlElement(name = "MortgageBal", required = true)
            protected String mortgageBal;
            @XmlElement(name = "MortgageTradeCnt", required = true)
            protected String mortgageTradeCnt;
            @XmlElement(name = "OtherTradeBal", required = true)
            protected String otherTradeBal;
            @XmlElement(name = "OtherTradeCnt", required = true)
            protected String otherTradeCnt;
            @XmlElement(name = "RevolveBal", required = true)
            protected String revolveBal;
            @XmlElement(name = "RevolveTradeLmt", required = true)
            protected String revolveTradeLmt;
            @XmlElement(name = "RevolveTradeCnt", required = true)
            protected String revolveTradeCnt;
            @XmlElement(name = "CCLimit", required = true)
            protected String ccLimit;
            @XmlElement(name = "IsWorst", required = true)
            protected String isWorst;
            @XmlElement(name = "ExpenseIncomeRatio", required = true)
            protected String expenseIncomeRatio;
            @XmlElement(name = "SgmntPoolID", required = true)
            protected String sgmntPoolID;
            @XmlElement(name = "PoolPD", required = true)
            protected String poolPD;
            @XmlElement(name = "PoolLGD", required = true)
            protected String poolLGD;
            @XmlElement(name = "PoolEAD", required = true)
            protected String poolEAD;
            @XmlElement(name = "PD", required = true)
            protected String pd;
            @XmlElement(name = "LGD", required = true)
            protected String lgd;
            @XmlElement(name = "EAD", required = true)
            protected String ead;
            @XmlElement(name = "ModelName", required = true)
            protected String modelName;
            @XmlElement(name = "ModelVersion", required = true)
            protected String modelVersion;
            @XmlElement(name = "ModelResult", required = true)
            protected String modelResult;
            @XmlElement(name = "IsFailed", required = true)
            protected String isFailed;

            /**
             * Gets the value of the retBaselID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRetBaselID() {
                return retBaselID;
            }

            /**
             * Sets the value of the retBaselID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRetBaselID(String value) {
                this.retBaselID = value;
            }

            /**
             * Gets the value of the debtHELOC property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDebtHELOC() {
                return debtHELOC;
            }

            /**
             * Sets the value of the debtHELOC property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDebtHELOC(String value) {
                this.debtHELOC = value;
            }

            /**
             * Gets the value of the currFICOScore property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrFICOScore() {
                return currFICOScore;
            }

            /**
             * Sets the value of the currFICOScore property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrFICOScore(String value) {
                this.currFICOScore = value;
            }

            /**
             * Gets the value of the totalDSR property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTotalDSR() {
                return totalDSR;
            }

            /**
             * Sets the value of the totalDSR property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTotalDSR(String value) {
                this.totalDSR = value;
            }

            /**
             * Gets the value of the businessTotalScore property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBusinessTotalScore() {
                return businessTotalScore;
            }

            /**
             * Sets the value of the businessTotalScore property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBusinessTotalScore(String value) {
                this.businessTotalScore = value;
            }

            /**
             * Gets the value of the hasRelationship property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHasRelationship() {
                return hasRelationship;
            }

            /**
             * Sets the value of the hasRelationship property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHasRelationship(String value) {
                this.hasRelationship = value;
            }

            /**
             * Gets the value of the combinedDSR property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCombinedDSR() {
                return combinedDSR;
            }

            /**
             * Sets the value of the combinedDSR property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCombinedDSR(String value) {
                this.combinedDSR = value;
            }

            /**
             * Gets the value of the businessDSR property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBusinessDSR() {
                return businessDSR;
            }

            /**
             * Sets the value of the businessDSR property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBusinessDSR(String value) {
                this.businessDSR = value;
            }

            /**
             * Gets the value of the cashAssetRatio property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCashAssetRatio() {
                return cashAssetRatio;
            }

            /**
             * Sets the value of the cashAssetRatio property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCashAssetRatio(String value) {
                this.cashAssetRatio = value;
            }

            /**
             * Gets the value of the debtWorthRatio property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDebtWorthRatio() {
                return debtWorthRatio;
            }

            /**
             * Sets the value of the debtWorthRatio property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDebtWorthRatio(String value) {
                this.debtWorthRatio = value;
            }

            /**
             * Gets the value of the interestTaxRatio property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInterestTaxRatio() {
                return interestTaxRatio;
            }

            /**
             * Sets the value of the interestTaxRatio property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInterestTaxRatio(String value) {
                this.interestTaxRatio = value;
            }

            /**
             * Gets the value of the generalQuickRatio property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGeneralQuickRatio() {
                return generalQuickRatio;
            }

            /**
             * Sets the value of the generalQuickRatio property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGeneralQuickRatio(String value) {
                this.generalQuickRatio = value;
            }

            /**
             * Gets the value of the scoredSICCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getScoredSICCode() {
                return scoredSICCode;
            }

            /**
             * Sets the value of the scoredSICCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setScoredSICCode(String value) {
                this.scoredSICCode = value;
            }

            /**
             * Gets the value of the ownerYears property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOwnerYears() {
                return ownerYears;
            }

            /**
             * Sets the value of the ownerYears property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOwnerYears(String value) {
                this.ownerYears = value;
            }

            /**
             * Gets the value of the netWorth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNetWorth() {
                return netWorth;
            }

            /**
             * Sets the value of the netWorth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNetWorth(String value) {
                this.netWorth = value;
            }

            /**
             * Gets the value of the ddaBal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDDABal() {
                return ddaBal;
            }

            /**
             * Sets the value of the ddaBal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDDABal(String value) {
                this.ddaBal = value;
            }

            /**
             * Gets the value of the combinedDebtRatio property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCombinedDebtRatio() {
                return combinedDebtRatio;
            }

            /**
             * Sets the value of the combinedDebtRatio property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCombinedDebtRatio(String value) {
                this.combinedDebtRatio = value;
            }

            /**
             * Gets the value of the proposedLeverage property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProposedLeverage() {
                return proposedLeverage;
            }

            /**
             * Sets the value of the proposedLeverage property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProposedLeverage(String value) {
                this.proposedLeverage = value;
            }

            /**
             * Gets the value of the glblCashCoverage property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGLBLCashCoverage() {
                return glblCashCoverage;
            }

            /**
             * Sets the value of the glblCashCoverage property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGLBLCashCoverage(String value) {
                this.glblCashCoverage = value;
            }

            /**
             * Gets the value of the currentRatio property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrentRatio() {
                return currentRatio;
            }

            /**
             * Sets the value of the currentRatio property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrentRatio(String value) {
                this.currentRatio = value;
            }

            /**
             * Gets the value of the liquidityRatio property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLiquidityRatio() {
                return liquidityRatio;
            }

            /**
             * Sets the value of the liquidityRatio property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLiquidityRatio(String value) {
                this.liquidityRatio = value;
            }

            /**
             * Gets the value of the minTangibleWorth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMinTangibleWorth() {
                return minTangibleWorth;
            }

            /**
             * Sets the value of the minTangibleWorth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMinTangibleWorth(String value) {
                this.minTangibleWorth = value;
            }

            /**
             * Gets the value of the ficoScore property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFICOScore() {
                return ficoScore;
            }

            /**
             * Sets the value of the ficoScore property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFICOScore(String value) {
                this.ficoScore = value;
            }

            /**
             * Gets the value of the finInqCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFinInqCnt() {
                return finInqCnt;
            }

            /**
             * Sets the value of the finInqCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFinInqCnt(String value) {
                this.finInqCnt = value;
            }

            /**
             * Gets the value of the finTradeCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFinTradeCnt() {
                return finTradeCnt;
            }

            /**
             * Sets the value of the finTradeCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFinTradeCnt(String value) {
                this.finTradeCnt = value;
            }

            /**
             * Gets the value of the inquiryCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInquiryCnt() {
                return inquiryCnt;
            }

            /**
             * Sets the value of the inquiryCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInquiryCnt(String value) {
                this.inquiryCnt = value;
            }

            /**
             * Gets the value of the minorDerogatoryCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMinorDerogatoryCnt() {
                return minorDerogatoryCnt;
            }

            /**
             * Sets the value of the minorDerogatoryCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMinorDerogatoryCnt(String value) {
                this.minorDerogatoryCnt = value;
            }

            /**
             * Gets the value of the majorDerogatoryCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMajorDerogatoryCnt() {
                return majorDerogatoryCnt;
            }

            /**
             * Sets the value of the majorDerogatoryCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMajorDerogatoryCnt(String value) {
                this.majorDerogatoryCnt = value;
            }

            /**
             * Gets the value of the neverPastDueCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNeverPastDueCnt() {
                return neverPastDueCnt;
            }

            /**
             * Sets the value of the neverPastDueCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNeverPastDueCnt(String value) {
                this.neverPastDueCnt = value;
            }

            /**
             * Gets the value of the openTradeCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOpenTradeCnt() {
                return openTradeCnt;
            }

            /**
             * Sets the value of the openTradeCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOpenTradeCnt(String value) {
                this.openTradeCnt = value;
            }

            /**
             * Gets the value of the highestCreditLmt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHighestCreditLmt() {
                return highestCreditLmt;
            }

            /**
             * Sets the value of the highestCreditLmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHighestCreditLmt(String value) {
                this.highestCreditLmt = value;
            }

            /**
             * Gets the value of the tradeCntThirty property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTradeCntThirty() {
                return tradeCntThirty;
            }

            /**
             * Sets the value of the tradeCntThirty property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTradeCntThirty(String value) {
                this.tradeCntThirty = value;
            }

            /**
             * Gets the value of the tradeCntSixty property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTradeCntSixty() {
                return tradeCntSixty;
            }

            /**
             * Sets the value of the tradeCntSixty property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTradeCntSixty(String value) {
                this.tradeCntSixty = value;
            }

            /**
             * Gets the value of the tradeCntNinety property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTradeCntNinety() {
                return tradeCntNinety;
            }

            /**
             * Sets the value of the tradeCntNinety property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTradeCntNinety(String value) {
                this.tradeCntNinety = value;
            }

            /**
             * Gets the value of the revolveDebtPrcnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRevolveDebtPrcnt() {
                return revolveDebtPrcnt;
            }

            /**
             * Sets the value of the revolveDebtPrcnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRevolveDebtPrcnt(String value) {
                this.revolveDebtPrcnt = value;
            }

            /**
             * Gets the value of the satisfactoryCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSatisfactoryCnt() {
                return satisfactoryCnt;
            }

            /**
             * Sets the value of the satisfactoryCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSatisfactoryCnt(String value) {
                this.satisfactoryCnt = value;
            }

            /**
             * Gets the value of the instLoanBal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInstLoanBal() {
                return instLoanBal;
            }

            /**
             * Sets the value of the instLoanBal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInstLoanBal(String value) {
                this.instLoanBal = value;
            }

            /**
             * Gets the value of the instLoanCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInstLoanCnt() {
                return instLoanCnt;
            }

            /**
             * Sets the value of the instLoanCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInstLoanCnt(String value) {
                this.instLoanCnt = value;
            }

            /**
             * Gets the value of the mortgageBal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMortgageBal() {
                return mortgageBal;
            }

            /**
             * Sets the value of the mortgageBal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMortgageBal(String value) {
                this.mortgageBal = value;
            }

            /**
             * Gets the value of the mortgageTradeCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMortgageTradeCnt() {
                return mortgageTradeCnt;
            }

            /**
             * Sets the value of the mortgageTradeCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMortgageTradeCnt(String value) {
                this.mortgageTradeCnt = value;
            }

            /**
             * Gets the value of the otherTradeBal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOtherTradeBal() {
                return otherTradeBal;
            }

            /**
             * Sets the value of the otherTradeBal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOtherTradeBal(String value) {
                this.otherTradeBal = value;
            }

            /**
             * Gets the value of the otherTradeCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOtherTradeCnt() {
                return otherTradeCnt;
            }

            /**
             * Sets the value of the otherTradeCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOtherTradeCnt(String value) {
                this.otherTradeCnt = value;
            }

            /**
             * Gets the value of the revolveBal property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRevolveBal() {
                return revolveBal;
            }

            /**
             * Sets the value of the revolveBal property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRevolveBal(String value) {
                this.revolveBal = value;
            }

            /**
             * Gets the value of the revolveTradeLmt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRevolveTradeLmt() {
                return revolveTradeLmt;
            }

            /**
             * Sets the value of the revolveTradeLmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRevolveTradeLmt(String value) {
                this.revolveTradeLmt = value;
            }

            /**
             * Gets the value of the revolveTradeCnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRevolveTradeCnt() {
                return revolveTradeCnt;
            }

            /**
             * Sets the value of the revolveTradeCnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRevolveTradeCnt(String value) {
                this.revolveTradeCnt = value;
            }

            /**
             * Gets the value of the ccLimit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCCLimit() {
                return ccLimit;
            }

            /**
             * Sets the value of the ccLimit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCCLimit(String value) {
                this.ccLimit = value;
            }

            /**
             * Gets the value of the isWorst property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsWorst() {
                return isWorst;
            }

            /**
             * Sets the value of the isWorst property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsWorst(String value) {
                this.isWorst = value;
            }

            /**
             * Gets the value of the expenseIncomeRatio property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpenseIncomeRatio() {
                return expenseIncomeRatio;
            }

            /**
             * Sets the value of the expenseIncomeRatio property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpenseIncomeRatio(String value) {
                this.expenseIncomeRatio = value;
            }

            /**
             * Gets the value of the sgmntPoolID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSgmntPoolID() {
                return sgmntPoolID;
            }

            /**
             * Sets the value of the sgmntPoolID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSgmntPoolID(String value) {
                this.sgmntPoolID = value;
            }

            /**
             * Gets the value of the poolPD property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPoolPD() {
                return poolPD;
            }

            /**
             * Sets the value of the poolPD property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPoolPD(String value) {
                this.poolPD = value;
            }

            /**
             * Gets the value of the poolLGD property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPoolLGD() {
                return poolLGD;
            }

            /**
             * Sets the value of the poolLGD property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPoolLGD(String value) {
                this.poolLGD = value;
            }

            /**
             * Gets the value of the poolEAD property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPoolEAD() {
                return poolEAD;
            }

            /**
             * Sets the value of the poolEAD property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPoolEAD(String value) {
                this.poolEAD = value;
            }

            /**
             * Gets the value of the pd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPD() {
                return pd;
            }

            /**
             * Sets the value of the pd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPD(String value) {
                this.pd = value;
            }

            /**
             * Gets the value of the lgd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLGD() {
                return lgd;
            }

            /**
             * Sets the value of the lgd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLGD(String value) {
                this.lgd = value;
            }

            /**
             * Gets the value of the ead property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEAD() {
                return ead;
            }

            /**
             * Sets the value of the ead property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEAD(String value) {
                this.ead = value;
            }

            /**
             * Gets the value of the modelName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getModelName() {
                return modelName;
            }

            /**
             * Sets the value of the modelName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setModelName(String value) {
                this.modelName = value;
            }

            /**
             * Gets the value of the modelVersion property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getModelVersion() {
                return modelVersion;
            }

            /**
             * Sets the value of the modelVersion property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setModelVersion(String value) {
                this.modelVersion = value;
            }

            /**
             * Gets the value of the modelResult property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getModelResult() {
                return modelResult;
            }

            /**
             * Sets the value of the modelResult property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setModelResult(String value) {
                this.modelResult = value;
            }

            /**
             * Gets the value of the isFailed property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsFailed() {
                return isFailed;
            }

            /**
             * Sets the value of the isFailed property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsFailed(String value) {
                this.isFailed = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CautionStat" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CentralBankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ContractLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CountryDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CorpKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CreatedFrom" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CurrDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustNative" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DCMmarginPercentage" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DCNextNumCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DCNextNumCodeRCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DCSanctionIngAuth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsDeleted" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EntityCreationFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpImpInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FaxNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FCSanctionIngAuth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HundredPercentFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IndividualCorpFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="InlandTradeAllowed" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LeasingLiabilities" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OrgKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PartyConst" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PartyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PhoneCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PhoneLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProductionCycle" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SpecialCustFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SSIFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StateDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TradeAuthorityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cautionStat",
            "centralBankId",
            "city",
            "contractLimit",
            "countryDesc",
            "corpKey",
            "corpName",
            "createdFrom",
            "currDesc",
            "custFlag",
            "custNative",
            "dcMmarginPercentage",
            "dcNextNumCode",
            "dcNextNumCodeRCode",
            "dcSanctionIngAuth",
            "isDeleted",
            "entityCreationFlag",
            "entityType",
            "expImpInd",
            "faxNum",
            "fcSanctionIngAuth",
            "hundredPercentFlag",
            "individualCorpFlag",
            "inlandTradeAllowed",
            "leasingLiabilities",
            "name",
            "orgKey",
            "partyConst",
            "partyType",
            "phone",
            "phoneCityCode",
            "phoneCountryCode",
            "phoneLocalCode",
            "productionCycle",
            "rmks",
            "specialCustFlag",
            "ssiFlag",
            "stateDesc",
            "telex",
            "tradeAuthorityCode",
            "postalCode"
        })
        public static class TradeFinDtls implements Serializable{

            @XmlElement(name = "CautionStat", required = true)
            protected String cautionStat;
            @XmlElement(name = "CentralBankId", required = true)
            protected String centralBankId;
            @XmlElement(name = "City", required = true)
            protected String city;
            @XmlElement(name = "ContractLimit", required = true)
            protected String contractLimit;
            @XmlElement(name = "CountryDesc", required = true)
            protected String countryDesc;
            @XmlElement(name = "CorpKey", required = true)
            protected String corpKey;
            @XmlElement(name = "CorpName", required = true)
            protected String corpName;
            @XmlElement(name = "CreatedFrom", required = true)
            protected String createdFrom;
            @XmlElement(name = "CurrDesc", required = true)
            protected String currDesc;
            @XmlElement(name = "CustFlag", required = true)
            protected String custFlag;
            @XmlElement(name = "CustNative", required = true)
            protected String custNative;
            @XmlElement(name = "DCMmarginPercentage", required = true)
            protected String dcMmarginPercentage;
            @XmlElement(name = "DCNextNumCode", required = true)
            protected String dcNextNumCode;
            @XmlElement(name = "DCNextNumCodeRCode", required = true)
            protected String dcNextNumCodeRCode;
            @XmlElement(name = "DCSanctionIngAuth", required = true)
            protected String dcSanctionIngAuth;
            @XmlElement(name = "IsDeleted", required = true)
            protected String isDeleted;
            @XmlElement(name = "EntityCreationFlag", required = true)
            protected String entityCreationFlag;
            @XmlElement(name = "EntityType", required = true)
            protected String entityType;
            @XmlElement(name = "ExpImpInd", required = true)
            protected String expImpInd;
            @XmlElement(name = "FaxNum", required = true)
            protected String faxNum;
            @XmlElement(name = "FCSanctionIngAuth", required = true)
            protected String fcSanctionIngAuth;
            @XmlElement(name = "HundredPercentFlag", required = true)
            protected String hundredPercentFlag;
            @XmlElement(name = "IndividualCorpFlag", required = true)
            protected String individualCorpFlag;
            @XmlElement(name = "InlandTradeAllowed", required = true)
            protected String inlandTradeAllowed;
            @XmlElement(name = "LeasingLiabilities", required = true)
            protected String leasingLiabilities;
            @XmlElement(name = "Name", required = true)
            protected String name;
            @XmlElement(name = "OrgKey", required = true)
            protected String orgKey;
            @XmlElement(name = "PartyConst", required = true)
            protected String partyConst;
            @XmlElement(name = "PartyType", required = true)
            protected String partyType;
            @XmlElement(name = "Phone", required = true)
            protected String phone;
            @XmlElement(name = "PhoneCityCode", required = true)
            protected String phoneCityCode;
            @XmlElement(name = "PhoneCountryCode", required = true)
            protected String phoneCountryCode;
            @XmlElement(name = "PhoneLocalCode", required = true)
            protected String phoneLocalCode;
            @XmlElement(name = "ProductionCycle", required = true)
            protected String productionCycle;
            @XmlElement(name = "Rmks", required = true)
            protected String rmks;
            @XmlElement(name = "SpecialCustFlag", required = true)
            protected String specialCustFlag;
            @XmlElement(name = "SSIFlag", required = true)
            protected String ssiFlag;
            @XmlElement(name = "StateDesc", required = true)
            protected String stateDesc;
            @XmlElement(name = "Telex", required = true)
            protected String telex;
            @XmlElement(name = "TradeAuthorityCode", required = true)
            protected String tradeAuthorityCode;
            @XmlElement(name = "PostalCode", required = true)
            protected String postalCode;

            /**
             * Gets the value of the cautionStat property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCautionStat() {
                return cautionStat;
            }

            /**
             * Sets the value of the cautionStat property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCautionStat(String value) {
                this.cautionStat = value;
            }

            /**
             * Gets the value of the centralBankId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCentralBankId() {
                return centralBankId;
            }

            /**
             * Sets the value of the centralBankId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCentralBankId(String value) {
                this.centralBankId = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the contractLimit property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getContractLimit() {
                return contractLimit;
            }

            /**
             * Sets the value of the contractLimit property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setContractLimit(String value) {
                this.contractLimit = value;
            }

            /**
             * Gets the value of the countryDesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryDesc() {
                return countryDesc;
            }

            /**
             * Sets the value of the countryDesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryDesc(String value) {
                this.countryDesc = value;
            }

            /**
             * Gets the value of the corpKey property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorpKey() {
                return corpKey;
            }

            /**
             * Sets the value of the corpKey property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorpKey(String value) {
                this.corpKey = value;
            }

            /**
             * Gets the value of the corpName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorpName() {
                return corpName;
            }

            /**
             * Sets the value of the corpName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorpName(String value) {
                this.corpName = value;
            }

            /**
             * Gets the value of the createdFrom property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreatedFrom() {
                return createdFrom;
            }

            /**
             * Sets the value of the createdFrom property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreatedFrom(String value) {
                this.createdFrom = value;
            }

            /**
             * Gets the value of the currDesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrDesc() {
                return currDesc;
            }

            /**
             * Sets the value of the currDesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrDesc(String value) {
                this.currDesc = value;
            }

            /**
             * Gets the value of the custFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustFlag() {
                return custFlag;
            }

            /**
             * Sets the value of the custFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustFlag(String value) {
                this.custFlag = value;
            }

            /**
             * Gets the value of the custNative property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustNative() {
                return custNative;
            }

            /**
             * Sets the value of the custNative property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustNative(String value) {
                this.custNative = value;
            }

            /**
             * Gets the value of the dcMmarginPercentage property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDCMmarginPercentage() {
                return dcMmarginPercentage;
            }

            /**
             * Sets the value of the dcMmarginPercentage property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDCMmarginPercentage(String value) {
                this.dcMmarginPercentage = value;
            }

            /**
             * Gets the value of the dcNextNumCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDCNextNumCode() {
                return dcNextNumCode;
            }

            /**
             * Sets the value of the dcNextNumCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDCNextNumCode(String value) {
                this.dcNextNumCode = value;
            }

            /**
             * Gets the value of the dcNextNumCodeRCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDCNextNumCodeRCode() {
                return dcNextNumCodeRCode;
            }

            /**
             * Sets the value of the dcNextNumCodeRCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDCNextNumCodeRCode(String value) {
                this.dcNextNumCodeRCode = value;
            }

            /**
             * Gets the value of the dcSanctionIngAuth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDCSanctionIngAuth() {
                return dcSanctionIngAuth;
            }

            /**
             * Sets the value of the dcSanctionIngAuth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDCSanctionIngAuth(String value) {
                this.dcSanctionIngAuth = value;
            }

            /**
             * Gets the value of the isDeleted property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsDeleted() {
                return isDeleted;
            }

            /**
             * Sets the value of the isDeleted property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsDeleted(String value) {
                this.isDeleted = value;
            }

            /**
             * Gets the value of the entityCreationFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityCreationFlag() {
                return entityCreationFlag;
            }

            /**
             * Sets the value of the entityCreationFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityCreationFlag(String value) {
                this.entityCreationFlag = value;
            }

            /**
             * Gets the value of the entityType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityType() {
                return entityType;
            }

            /**
             * Sets the value of the entityType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityType(String value) {
                this.entityType = value;
            }

            /**
             * Gets the value of the expImpInd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpImpInd() {
                return expImpInd;
            }

            /**
             * Sets the value of the expImpInd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpImpInd(String value) {
                this.expImpInd = value;
            }

            /**
             * Gets the value of the faxNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFaxNum() {
                return faxNum;
            }

            /**
             * Sets the value of the faxNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFaxNum(String value) {
                this.faxNum = value;
            }

            /**
             * Gets the value of the fcSanctionIngAuth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFCSanctionIngAuth() {
                return fcSanctionIngAuth;
            }

            /**
             * Sets the value of the fcSanctionIngAuth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFCSanctionIngAuth(String value) {
                this.fcSanctionIngAuth = value;
            }

            /**
             * Gets the value of the hundredPercentFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHundredPercentFlag() {
                return hundredPercentFlag;
            }

            /**
             * Sets the value of the hundredPercentFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHundredPercentFlag(String value) {
                this.hundredPercentFlag = value;
            }

            /**
             * Gets the value of the individualCorpFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIndividualCorpFlag() {
                return individualCorpFlag;
            }

            /**
             * Sets the value of the individualCorpFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIndividualCorpFlag(String value) {
                this.individualCorpFlag = value;
            }

            /**
             * Gets the value of the inlandTradeAllowed property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInlandTradeAllowed() {
                return inlandTradeAllowed;
            }

            /**
             * Sets the value of the inlandTradeAllowed property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInlandTradeAllowed(String value) {
                this.inlandTradeAllowed = value;
            }

            /**
             * Gets the value of the leasingLiabilities property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLeasingLiabilities() {
                return leasingLiabilities;
            }

            /**
             * Sets the value of the leasingLiabilities property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLeasingLiabilities(String value) {
                this.leasingLiabilities = value;
            }

            /**
             * Gets the value of the name property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Sets the value of the name property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Gets the value of the orgKey property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOrgKey() {
                return orgKey;
            }

            /**
             * Sets the value of the orgKey property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOrgKey(String value) {
                this.orgKey = value;
            }

            /**
             * Gets the value of the partyConst property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPartyConst() {
                return partyConst;
            }

            /**
             * Sets the value of the partyConst property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPartyConst(String value) {
                this.partyConst = value;
            }

            /**
             * Gets the value of the partyType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPartyType() {
                return partyType;
            }

            /**
             * Sets the value of the partyType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPartyType(String value) {
                this.partyType = value;
            }

            /**
             * Gets the value of the phone property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhone() {
                return phone;
            }

            /**
             * Sets the value of the phone property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhone(String value) {
                this.phone = value;
            }

            /**
             * Gets the value of the phoneCityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneCityCode() {
                return phoneCityCode;
            }

            /**
             * Sets the value of the phoneCityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneCityCode(String value) {
                this.phoneCityCode = value;
            }

            /**
             * Gets the value of the phoneCountryCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneCountryCode() {
                return phoneCountryCode;
            }

            /**
             * Sets the value of the phoneCountryCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneCountryCode(String value) {
                this.phoneCountryCode = value;
            }

            /**
             * Gets the value of the phoneLocalCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneLocalCode() {
                return phoneLocalCode;
            }

            /**
             * Sets the value of the phoneLocalCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneLocalCode(String value) {
                this.phoneLocalCode = value;
            }

            /**
             * Gets the value of the productionCycle property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProductionCycle() {
                return productionCycle;
            }

            /**
             * Sets the value of the productionCycle property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProductionCycle(String value) {
                this.productionCycle = value;
            }

            /**
             * Gets the value of the rmks property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRmks() {
                return rmks;
            }

            /**
             * Sets the value of the rmks property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRmks(String value) {
                this.rmks = value;
            }

            /**
             * Gets the value of the specialCustFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSpecialCustFlag() {
                return specialCustFlag;
            }

            /**
             * Sets the value of the specialCustFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSpecialCustFlag(String value) {
                this.specialCustFlag = value;
            }

            /**
             * Gets the value of the ssiFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSSIFlag() {
                return ssiFlag;
            }

            /**
             * Sets the value of the ssiFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSSIFlag(String value) {
                this.ssiFlag = value;
            }

            /**
             * Gets the value of the stateDesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStateDesc() {
                return stateDesc;
            }

            /**
             * Sets the value of the stateDesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStateDesc(String value) {
                this.stateDesc = value;
            }

            /**
             * Gets the value of the telex property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTelex() {
                return telex;
            }

            /**
             * Sets the value of the telex property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTelex(String value) {
                this.telex = value;
            }

            /**
             * Gets the value of the tradeAuthorityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTradeAuthorityCode() {
                return tradeAuthorityCode;
            }

            /**
             * Sets the value of the tradeAuthorityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTradeAuthorityCode(String value) {
                this.tradeAuthorityCode = value;
            }

            /**
             * Gets the value of the postalCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostalCode() {
                return postalCode;
            }

            /**
             * Sets the value of the postalCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostalCode(String value) {
                this.postalCode = value;
            }

        }

    }

}

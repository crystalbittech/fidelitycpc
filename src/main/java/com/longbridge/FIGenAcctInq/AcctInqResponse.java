
package com.longbridge.FIGenAcctInq;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctInqRs">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctId">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AcctType">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="SchmCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="SchmType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="AcctCurr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BankInfo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="BankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="BranchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="BranchName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PostAddr">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="Addr1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="Addr2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="Addr3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="StateProv" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="AddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CustId">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PersonName">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="TitlePrefix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AcctOpenDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BankAcctStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AcctBal" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="BalType">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="LEDGER"/>
 *                                   &lt;enumeration value="AVAIL"/>
 *                                   &lt;enumeration value="EFFAVL"/>
 *                                   &lt;enumeration value="FLOAT"/>
 *                                   &lt;enumeration value="LIEN"/>
 *                                   &lt;enumeration value="DRWPWR"/>
 *                                   &lt;enumeration value="ACCBAL"/>
 *                                   &lt;enumeration value="SHADOW"/>
 *                                   &lt;enumeration value="FUTBAL"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="BalAmt">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="amountValue">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="1075.75"/>
 *                                             &lt;enumeration value="1055.75"/>
 *                                             &lt;enumeration value="0.00"/>
 *                                             &lt;enumeration value="20.00"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="currencyCode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value=""/>
 *                                             &lt;enumeration value="NGN"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AcctInq_CustomData" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "acctInqRs",
    "acctInqCustomData"
})
@XmlRootElement(name = "AcctInqResponse")
public class AcctInqResponse implements Serializable{

    @XmlElement(name = "AcctInqRs", required = true)
    protected AcctInqResponse.AcctInqRs acctInqRs;
    @XmlElement(name = "AcctInq_CustomData", required = true)
    protected String acctInqCustomData;

    /**
     * Gets the value of the acctInqRs property.
     * 
     * @return
     *     possible object is
     *     {@link AcctInqResponse.AcctInqRs }
     *     
     */
    public AcctInqResponse.AcctInqRs getAcctInqRs() {
        return acctInqRs;
    }

    /**
     * Sets the value of the acctInqRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcctInqResponse.AcctInqRs }
     *     
     */
    public void setAcctInqRs(AcctInqResponse.AcctInqRs value) {
        this.acctInqRs = value;
    }

    /**
     * Gets the value of the acctInqCustomData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctInqCustomData() {
        return acctInqCustomData;
    }

    /**
     * Sets the value of the acctInqCustomData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctInqCustomData(String value) {
        this.acctInqCustomData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctId">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AcctType">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="SchmCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="SchmType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="AcctCurr" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BankInfo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="BankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="BranchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="BranchName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PostAddr">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="Addr1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="Addr2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="Addr3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="StateProv" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="AddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CustId">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PersonName">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="TitlePrefix" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AcctOpenDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BankAcctStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AcctBal" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="BalType">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="LEDGER"/>
     *                         &lt;enumeration value="AVAIL"/>
     *                         &lt;enumeration value="EFFAVL"/>
     *                         &lt;enumeration value="FLOAT"/>
     *                         &lt;enumeration value="LIEN"/>
     *                         &lt;enumeration value="DRWPWR"/>
     *                         &lt;enumeration value="ACCBAL"/>
     *                         &lt;enumeration value="SHADOW"/>
     *                         &lt;enumeration value="FUTBAL"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="BalAmt">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="amountValue">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="1075.75"/>
     *                                   &lt;enumeration value="1055.75"/>
     *                                   &lt;enumeration value="0.00"/>
     *                                   &lt;enumeration value="20.00"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="currencyCode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value=""/>
     *                                   &lt;enumeration value="NGN"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctId",
        "custId",
        "acctOpenDt",
        "bankAcctStatusCode",
        "acctBal"
    })
    public static class AcctInqRs implements Serializable{

        @XmlElement(name = "AcctId", required = true)
        protected AcctInqResponse.AcctInqRs.AcctId acctId;
        @XmlElement(name = "CustId", required = true)
        protected AcctInqResponse.AcctInqRs.CustId custId;
        @XmlElement(name = "AcctOpenDt", required = true)
        protected String acctOpenDt;
        @XmlElement(name = "BankAcctStatusCode", required = true)
        protected String bankAcctStatusCode;
        @XmlElement(name = "AcctBal")
        protected List<AcctInqResponse.AcctInqRs.AcctBal> acctBal;

        /**
         * Gets the value of the acctId property.
         * 
         * @return
         *     possible object is
         *     {@link AcctInqResponse.AcctInqRs.AcctId }
         *     
         */
        public AcctInqResponse.AcctInqRs.AcctId getAcctId() {
            return acctId;
        }

        /**
         * Sets the value of the acctId property.
         * 
         * @param value
         *     allowed object is
         *     {@link AcctInqResponse.AcctInqRs.AcctId }
         *     
         */
        public void setAcctId(AcctInqResponse.AcctInqRs.AcctId value) {
            this.acctId = value;
        }

        /**
         * Gets the value of the custId property.
         * 
         * @return
         *     possible object is
         *     {@link AcctInqResponse.AcctInqRs.CustId }
         *     
         */
        public AcctInqResponse.AcctInqRs.CustId getCustId() {
            return custId;
        }

        /**
         * Sets the value of the custId property.
         * 
         * @param value
         *     allowed object is
         *     {@link AcctInqResponse.AcctInqRs.CustId }
         *     
         */
        public void setCustId(AcctInqResponse.AcctInqRs.CustId value) {
            this.custId = value;
        }

        /**
         * Gets the value of the acctOpenDt property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctOpenDt() {
            return acctOpenDt;
        }

        /**
         * Sets the value of the acctOpenDt property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctOpenDt(String value) {
            this.acctOpenDt = value;
        }

        /**
         * Gets the value of the bankAcctStatusCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBankAcctStatusCode() {
            return bankAcctStatusCode;
        }

        /**
         * Sets the value of the bankAcctStatusCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBankAcctStatusCode(String value) {
            this.bankAcctStatusCode = value;
        }

        /**
         * Gets the value of the acctBal property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the acctBal property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAcctBal().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AcctInqResponse.AcctInqRs.AcctBal }
         * 
         * 
         */
        public List<AcctInqResponse.AcctInqRs.AcctBal> getAcctBal() {
            if (acctBal == null) {
                acctBal = new ArrayList<AcctInqResponse.AcctInqRs.AcctBal>();
            }
            return this.acctBal;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="BalType">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="LEDGER"/>
         *               &lt;enumeration value="AVAIL"/>
         *               &lt;enumeration value="EFFAVL"/>
         *               &lt;enumeration value="FLOAT"/>
         *               &lt;enumeration value="LIEN"/>
         *               &lt;enumeration value="DRWPWR"/>
         *               &lt;enumeration value="ACCBAL"/>
         *               &lt;enumeration value="SHADOW"/>
         *               &lt;enumeration value="FUTBAL"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="BalAmt">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="amountValue">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="1075.75"/>
         *                         &lt;enumeration value="1055.75"/>
         *                         &lt;enumeration value="0.00"/>
         *                         &lt;enumeration value="20.00"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="currencyCode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value=""/>
         *                         &lt;enumeration value="NGN"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "balType",
            "balAmt"
        })
        public static class AcctBal implements Serializable{

            @XmlElement(name = "BalType", required = true)
            protected String balType;
            @XmlElement(name = "BalAmt", required = true)
            protected AcctInqResponse.AcctInqRs.AcctBal.BalAmt balAmt;

            /**
             * Gets the value of the balType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBalType() {
                return balType;
            }

            /**
             * Sets the value of the balType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBalType(String value) {
                this.balType = value;
            }

            /**
             * Gets the value of the balAmt property.
             * 
             * @return
             *     possible object is
             *     {@link AcctInqResponse.AcctInqRs.AcctBal.BalAmt }
             *     
             */
            public AcctInqResponse.AcctInqRs.AcctBal.BalAmt getBalAmt() {
                return balAmt;
            }

            /**
             * Sets the value of the balAmt property.
             * 
             * @param value
             *     allowed object is
             *     {@link AcctInqResponse.AcctInqRs.AcctBal.BalAmt }
             *     
             */
            public void setBalAmt(AcctInqResponse.AcctInqRs.AcctBal.BalAmt value) {
                this.balAmt = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="amountValue">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="1075.75"/>
             *               &lt;enumeration value="1055.75"/>
             *               &lt;enumeration value="0.00"/>
             *               &lt;enumeration value="20.00"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="currencyCode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value=""/>
             *               &lt;enumeration value="NGN"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amountValue",
                "currencyCode"
            })
            public static class BalAmt implements Serializable{

                @XmlElement(required = true)
                protected String amountValue;
                @XmlElement(required = true)
                protected String currencyCode;

                /**
                 * Gets the value of the amountValue property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmountValue() {
                    return amountValue;
                }

                /**
                 * Sets the value of the amountValue property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmountValue(String value) {
                    this.amountValue = value;
                }

                /**
                 * Gets the value of the currencyCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Sets the value of the currencyCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AcctType">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="SchmCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="SchmType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="AcctCurr" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BankInfo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="BankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="BranchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="BranchName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PostAddr">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Addr1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="Addr2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="Addr3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="StateProv" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="AddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "acctId",
            "acctType",
            "acctCurr",
            "bankInfo"
        })
        public static class AcctId implements Serializable{

            @XmlElement(name = "AcctId", required = true)
            protected String acctId;
            @XmlElement(name = "AcctType", required = true)
            protected AcctInqResponse.AcctInqRs.AcctId.AcctType acctType;
            @XmlElement(name = "AcctCurr", required = true)
            protected String acctCurr;
            @XmlElement(name = "BankInfo", required = true)
            protected AcctInqResponse.AcctInqRs.AcctId.BankInfo bankInfo;

            /**
             * Gets the value of the acctId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctId() {
                return acctId;
            }

            /**
             * Sets the value of the acctId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctId(String value) {
                this.acctId = value;
            }

            /**
             * Gets the value of the acctType property.
             * 
             * @return
             *     possible object is
             *     {@link AcctInqResponse.AcctInqRs.AcctId.AcctType }
             *     
             */
            public AcctInqResponse.AcctInqRs.AcctId.AcctType getAcctType() {
                return acctType;
            }

            /**
             * Sets the value of the acctType property.
             * 
             * @param value
             *     allowed object is
             *     {@link AcctInqResponse.AcctInqRs.AcctId.AcctType }
             *     
             */
            public void setAcctType(AcctInqResponse.AcctInqRs.AcctId.AcctType value) {
                this.acctType = value;
            }

            /**
             * Gets the value of the acctCurr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctCurr() {
                return acctCurr;
            }

            /**
             * Sets the value of the acctCurr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctCurr(String value) {
                this.acctCurr = value;
            }

            /**
             * Gets the value of the bankInfo property.
             * 
             * @return
             *     possible object is
             *     {@link AcctInqResponse.AcctInqRs.AcctId.BankInfo }
             *     
             */
            public AcctInqResponse.AcctInqRs.AcctId.BankInfo getBankInfo() {
                return bankInfo;
            }

            /**
             * Sets the value of the bankInfo property.
             * 
             * @param value
             *     allowed object is
             *     {@link AcctInqResponse.AcctInqRs.AcctId.BankInfo }
             *     
             */
            public void setBankInfo(AcctInqResponse.AcctInqRs.AcctId.BankInfo value) {
                this.bankInfo = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="SchmCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="SchmType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "schmCode",
                "schmType"
            })
            public static class AcctType implements Serializable{

                @XmlElement(name = "SchmCode", required = true)
                protected String schmCode;
                @XmlElement(name = "SchmType", required = true)
                protected String schmType;

                /**
                 * Gets the value of the schmCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSchmCode() {
                    return schmCode;
                }

                /**
                 * Sets the value of the schmCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSchmCode(String value) {
                    this.schmCode = value;
                }

                /**
                 * Gets the value of the schmType property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSchmType() {
                    return schmType;
                }

                /**
                 * Sets the value of the schmType property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSchmType(String value) {
                    this.schmType = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="BankId" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="BranchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="BranchName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PostAddr">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Addr1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="Addr2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="Addr3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="StateProv" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="AddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "bankId",
                "name",
                "branchId",
                "branchName",
                "postAddr"
            })
            public static class BankInfo implements Serializable{

                @XmlElement(name = "BankId", required = true)
                protected String bankId;
                @XmlElement(name = "Name", required = true)
                protected String name;
                @XmlElement(name = "BranchId", required = true)
                protected String branchId;
                @XmlElement(name = "BranchName", required = true)
                protected String branchName;
                @XmlElement(name = "PostAddr", required = true)
                protected AcctInqResponse.AcctInqRs.AcctId.BankInfo.PostAddr postAddr;

                /**
                 * Gets the value of the bankId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBankId() {
                    return bankId;
                }

                /**
                 * Sets the value of the bankId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBankId(String value) {
                    this.bankId = value;
                }

                /**
                 * Gets the value of the name property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Sets the value of the name property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setName(String value) {
                    this.name = value;
                }

                /**
                 * Gets the value of the branchId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBranchId() {
                    return branchId;
                }

                /**
                 * Sets the value of the branchId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBranchId(String value) {
                    this.branchId = value;
                }

                /**
                 * Gets the value of the branchName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBranchName() {
                    return branchName;
                }

                /**
                 * Sets the value of the branchName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBranchName(String value) {
                    this.branchName = value;
                }

                /**
                 * Gets the value of the postAddr property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link AcctInqResponse.AcctInqRs.AcctId.BankInfo.PostAddr }
                 *     
                 */
                public AcctInqResponse.AcctInqRs.AcctId.BankInfo.PostAddr getPostAddr() {
                    return postAddr;
                }

                /**
                 * Sets the value of the postAddr property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link AcctInqResponse.AcctInqRs.AcctId.BankInfo.PostAddr }
                 *     
                 */
                public void setPostAddr(AcctInqResponse.AcctInqRs.AcctId.BankInfo.PostAddr value) {
                    this.postAddr = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Addr1" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="Addr2" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="Addr3" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="StateProv" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="AddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "addr1",
                    "addr2",
                    "addr3",
                    "city",
                    "stateProv",
                    "postalCode",
                    "country",
                    "addrType"
                })
                public static class PostAddr implements Serializable{

                    @XmlElement(name = "Addr1", required = true)
                    protected String addr1;
                    @XmlElement(name = "Addr2", required = true)
                    protected String addr2;
                    @XmlElement(name = "Addr3", required = true)
                    protected String addr3;
                    @XmlElement(name = "City", required = true)
                    protected String city;
                    @XmlElement(name = "StateProv", required = true)
                    protected String stateProv;
                    @XmlElement(name = "PostalCode", required = true)
                    protected String postalCode;
                    @XmlElement(name = "Country", required = true)
                    protected String country;
                    @XmlElement(name = "AddrType", required = true)
                    protected String addrType;

                    /**
                     * Gets the value of the addr1 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAddr1() {
                        return addr1;
                    }

                    /**
                     * Sets the value of the addr1 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAddr1(String value) {
                        this.addr1 = value;
                    }

                    /**
                     * Gets the value of the addr2 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAddr2() {
                        return addr2;
                    }

                    /**
                     * Sets the value of the addr2 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAddr2(String value) {
                        this.addr2 = value;
                    }

                    /**
                     * Gets the value of the addr3 property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAddr3() {
                        return addr3;
                    }

                    /**
                     * Sets the value of the addr3 property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAddr3(String value) {
                        this.addr3 = value;
                    }

                    /**
                     * Gets the value of the city property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCity() {
                        return city;
                    }

                    /**
                     * Sets the value of the city property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCity(String value) {
                        this.city = value;
                    }

                    /**
                     * Gets the value of the stateProv property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getStateProv() {
                        return stateProv;
                    }

                    /**
                     * Sets the value of the stateProv property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setStateProv(String value) {
                        this.stateProv = value;
                    }

                    /**
                     * Gets the value of the postalCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPostalCode() {
                        return postalCode;
                    }

                    /**
                     * Sets the value of the postalCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPostalCode(String value) {
                        this.postalCode = value;
                    }

                    /**
                     * Gets the value of the country property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCountry() {
                        return country;
                    }

                    /**
                     * Sets the value of the country property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCountry(String value) {
                        this.country = value;
                    }

                    /**
                     * Gets the value of the addrType property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAddrType() {
                        return addrType;
                    }

                    /**
                     * Sets the value of the addrType property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAddrType(String value) {
                        this.addrType = value;
                    }

                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PersonName">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="TitlePrefix" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "custId",
            "personName"
        })
        public static class CustId implements Serializable{

            @XmlElement(name = "CustId", required = true)
            protected String custId;
            @XmlElement(name = "PersonName", required = true)
            protected AcctInqResponse.AcctInqRs.CustId.PersonName personName;

            /**
             * Gets the value of the custId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustId() {
                return custId;
            }

            /**
             * Sets the value of the custId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustId(String value) {
                this.custId = value;
            }

            /**
             * Gets the value of the personName property.
             * 
             * @return
             *     possible object is
             *     {@link AcctInqResponse.AcctInqRs.CustId.PersonName }
             *     
             */
            public AcctInqResponse.AcctInqRs.CustId.PersonName getPersonName() {
                return personName;
            }

            /**
             * Sets the value of the personName property.
             * 
             * @param value
             *     allowed object is
             *     {@link AcctInqResponse.AcctInqRs.CustId.PersonName }
             *     
             */
            public void setPersonName(AcctInqResponse.AcctInqRs.CustId.PersonName value) {
                this.personName = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="TitlePrefix" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "lastName",
                "firstName",
                "middleName",
                "name",
                "titlePrefix"
            })
            public static class PersonName implements Serializable{

                @XmlElement(name = "LastName", required = true)
                protected String lastName;
                @XmlElement(name = "FirstName", required = true)
                protected String firstName;
                @XmlElement(name = "MiddleName", required = true)
                protected String middleName;
                @XmlElement(name = "Name", required = true)
                protected String name;
                @XmlElement(name = "TitlePrefix", required = true)
                protected String titlePrefix;

                /**
                 * Gets the value of the lastName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLastName() {
                    return lastName;
                }

                /**
                 * Sets the value of the lastName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLastName(String value) {
                    this.lastName = value;
                }

                /**
                 * Gets the value of the firstName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFirstName() {
                    return firstName;
                }

                /**
                 * Sets the value of the firstName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFirstName(String value) {
                    this.firstName = value;
                }

                /**
                 * Gets the value of the middleName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMiddleName() {
                    return middleName;
                }

                /**
                 * Sets the value of the middleName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMiddleName(String value) {
                    this.middleName = value;
                }

                /**
                 * Gets the value of the name property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Sets the value of the name property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setName(String value) {
                    this.name = value;
                }

                /**
                 * Gets the value of the titlePrefix property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTitlePrefix() {
                    return titlePrefix;
                }

                /**
                 * Sets the value of the titlePrefix property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTitlePrefix(String value) {
                    this.titlePrefix = value;
                }

            }

        }

    }

}

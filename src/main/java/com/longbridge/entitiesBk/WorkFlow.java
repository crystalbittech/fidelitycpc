package com.longbridge.entitiesBk;

/**
 * Created by mac on 3/29/17.
 */
public enum WorkFlow {
    NEWACCOUNT,
    AWAITING_VERIFICATION,
    UPLOADED,
    FINISHED
}

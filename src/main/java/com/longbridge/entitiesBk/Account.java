package com.longbridge.entitiesBk;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by mac on 3/27/17.
 */
@Getter
@Setter
@ToString
public class Account {

    String accountNumber;
    String accountStatus;
    String accountBalance;
    String accountOpenDate;
    String accountName;
    WorkFlow currentStatus;

    public Account(String accountNumber, String accountName, String accountStatus, String accountBalance, String accountOpenDate, WorkFlow currentStatus) {
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.accountStatus = accountStatus;
        this.accountBalance = accountBalance;
        this.accountOpenDate = accountOpenDate;
        this.currentStatus = currentStatus;
    }
}

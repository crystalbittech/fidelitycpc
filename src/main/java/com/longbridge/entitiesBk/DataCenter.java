package com.longbridge.entitiesBk;

import lombok.ToString;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Timothy on 3/29/17.
 */
@Service
@ToString
public class DataCenter {

  public Map<String, Account> accounts ;


  public DataCenter(){

      accounts = new HashMap<String, Account>();
      Account account = new Account("0123456789", "Babawale Olaniyonu A.","Active","10,0000","Tue,12,2017", WorkFlow.NEWACCOUNT);
      Account account1 = new Account("0023456789","Agbalaya Rasaq","Active","10,0000","Tue,12,2017",WorkFlow.NEWACCOUNT);
      Account account2 = new Account("0223456789","Yusuf Saheed T.","Active","10,0000","Tue,12,2017",WorkFlow.NEWACCOUNT);
      Account account3 = new Account("0423456789","Oduntan Olalekan","Active","10,0000","Tue,12,2017",WorkFlow.NEWACCOUNT);
      Account account4 = new Account("0723456789","Dada Oluwashina S.","Active","10,0000","Tue,12,2017",WorkFlow.NEWACCOUNT);
      Account account5 = new Account("0483456789","Phillips Samuel","Active","10,0000","Tue,12,2017",WorkFlow.NEWACCOUNT);

      accounts.put(account.accountNumber,account);
      accounts.put(account1.accountNumber,account1);
      accounts.put(account2.accountNumber,account2);
      accounts.put(account3.accountNumber,account3);
      accounts.put(account4.accountNumber,account4);
      accounts.put(account5.accountNumber,account5);

  }

}

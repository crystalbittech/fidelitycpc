package com.longbridge;

import com.longbridge.security.ExternalConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//
@Configuration
@EnableConfigurationProperties(ExternalConfig.class)
@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.longbridge.repository")
@EntityScan(basePackages = "com/longbridge/entities")
public class Application {

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(Application.class, args);
    }
}

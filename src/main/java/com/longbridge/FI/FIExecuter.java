package com.longbridge.FI;


import com.longbridge.security.ExternalConfig;
import lombok.Value;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.Properties;

public class FIExecuter {

    private final Logger logger = LoggerFactory.getLogger(FIExecuter.class);
    private String FI_SERVICE_URL;
    //@Value("${app.fi.url}")
    private String userBucketPath;

    public FIExecuter() {
    }


    public String postRequest (String payload) {
        this.logger.info("****************************************Initiating FI Call****************************************");
        this.logger.info("****************************************FI REQUEST**************************************** {}", payload);
        try {
            KeyStore var12 = KeyStore.getInstance(KeyStore.getDefaultType());
            var12.load((InputStream) null, (char[]) null);
            MySSLSocketFactory sf = new MySSLSocketFactory(var12);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            BasicHttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, "UTF-8");
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));
            ThreadSafeClientConnManager ccm = new ThreadSafeClientConnManager(params, registry);
            DefaultHttpClient httpClient = new DefaultHttpClient(ccm, params);
            ByteArrayEntity httpEntity = new ByteArrayEntity(payload.getBytes("UTF-8"));

            HttpPost httpPost = new HttpPost("https://172.27.10.179:6500/fiwebservice/FIWebService");
            ExternalConfig externalConfig = new ExternalConfig();
//            FI_SERVICE_URL = "https://finweb.fidelitybank.ng:8250/fiwebservice/FIWebService";//externalConfig.getIntegrifyURL();
//            logger.info("Using URL: "+FI_SERVICE_URL);
//            HttpPost httpPost = new HttpPost(FI_SERVICE_URL);
            httpPost.addHeader("SOAPAction", "");
            httpPost.setEntity(httpEntity);
            HttpResponse response = httpClient.execute(httpPost);
            String responseMessage = EntityUtils.toString(response.getEntity());
            if (responseMessage != null && responseMessage.length() > 0) {
                responseMessage = responseMessage.replace("&lt;", "<").replace("&gt;", ">");
            }

            this.logger.info("****************************************FI RESPONSE**************************************** {}", responseMessage);
            return responseMessage;
        } catch (Exception exception) {
            this.logger.error("Problem occurred in reaching FI {}", exception);
            return "Problem occurred in reaching FI";
        }
    }

    public String getPropertyFromFile(String property) {
        Properties prop = new Properties();
        InputStream input = null;
        String value = "";

        try {
            input = this.getClass().getResourceAsStream("integriservice.properties");
            prop.load(input);
            value = prop.getProperty(property);
            this.logger.info("Property : {} \t Value : {}", property, value);
        } catch (IOException var14) {
            this.logger.error("Error in reading props file {}", var14.getLocalizedMessage());
        } finally {
            if(input != null) {
                try {
                    input.close();
                } catch (IOException var13) {
                    this.logger.error("error in closing input stream {}", var13.toString());
                }
            }

        }

        return value;
    }

}
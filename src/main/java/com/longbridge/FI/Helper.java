package com.longbridge.FI;

import com.longbridge.FI.FIGenAcctInq.AcctInqResponse;
import com.longbridge.FI.FiCorpAcctInq.CorpCustAcctInqResponse;
import com.longbridge.FI.FiCorpCustInq.GetCorporateCustomerDetailsResponse;
import com.longbridge.FI.FiGenCustInq.CustInqResponse;
import com.longbridge.FI.FiRetAcctInq.RetCustAcctInqResponse;
import com.longbridge.FI.FiRetCustInq.RetCustInqResponse;
import com.longbridge.entities.CustomerDetailsForUpdate;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;


/**
 * Created by LB-PRJ-020 on 6/20/2017.
 */
public class Helper {
    private final Logger logger = LoggerFactory.getLogger(Helper.class);
    private VelocityEngine ve;
    private org.apache.velocity.Template fiTemplate;
    private VelocityContext context;
    JAXBContext jaxbContext;
    Unmarshaller jaxbUnMarshaller;
    private RetCustInqResponse retCustInqResponse;
    private RetCustAcctInqResponse retAcctInqResponse;
    private GetCorporateCustomerDetailsResponse corpCustInqresp;
    private CorpCustAcctInqResponse corpAcctInqResp;
    private AcctInqResponse acctInqResponse;
    private CustInqResponse custInqResponse;
    private InputStream inputStream;



    public Helper() {
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        ve = new VelocityEngine();
        ve.init(props);
        this.context = new VelocityContext();
    }

    //--------------    General Inquiry -----------------//

    public AcctInqResponse                      AcctInq(String acctId){
        this.logger.info(" Method pushed with details: {}",acctId);
        this.fiTemplate = this.ve.getTemplate("FI-XML/AcctInq.vm");
        StringWriter pyload = new StringWriter();
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID",requestId211);
        this.context.put("MessageDateTime",(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("AcctId",acctId);
        this.fiTemplate.merge(this.context,pyload);
        String serviceRequest = pyload.toString();
        String serviceResponse = new FIExecuter().postRequest(serviceRequest);
        serviceResponse = StringUtils.substringBetween(serviceResponse, "<Body>", "</Body>");
        inputStream = new ByteArrayInputStream((serviceResponse.getBytes()));
        acctInqResponse = new AcctInqResponse();
        try {
            jaxbContext = JAXBContext.newInstance(AcctInqResponse.class);
            jaxbUnMarshaller = jaxbContext.createUnmarshaller();
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            acctInqResponse = (AcctInqResponse) jaxbUnmarshaller.unmarshal(inputStream);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return acctInqResponse;

    }

    public CustInqResponse                      CustInq(String cifId){
        this.logger.info(" Method pushed with details: {}",cifId);
        this.fiTemplate = ve.getTemplate("FI-XML/CustInq.vm");
        StringWriter pyload = new StringWriter();
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID",requestId211);
        this.context.put("MessageDateTime",(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("CustId",cifId);
        this.fiTemplate.merge(this.context,pyload);
        String serviceRequest = pyload.toString();
        String serviceResponse = new FIExecuter().postRequest(serviceRequest);
        serviceResponse = StringUtils.substringBetween(serviceResponse, "<Body>", "</Body>");
        this.logger.info("Response is: "+serviceResponse);
        inputStream = new ByteArrayInputStream((serviceResponse.getBytes()));
        custInqResponse = new CustInqResponse();
        try {
            jaxbContext = JAXBContext.newInstance(CustInqResponse.class);
            jaxbUnMarshaller = jaxbContext.createUnmarshaller();
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            custInqResponse = (CustInqResponse) jaxbUnmarshaller.unmarshal(inputStream);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return custInqResponse;

    }
    //---------------   General Inquiry -----------------//

    public RetCustAcctInqResponse               RetCustAcctInq(String custId){
        this.logger.info(" Method pushed with details: {}",custId);
        this.fiTemplate = this.ve.getTemplate("FI-XML/RetCustAcctInq.vm");
        StringWriter pyload = new StringWriter();
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID",requestId211);
        this.context.put("MessageDateTime",(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("CustId",custId);
        this.fiTemplate.merge(this.context,pyload);
        String serviceRequest = pyload.toString();
        String serviceResponse = new FIExecuter().postRequest(serviceRequest);//todo
        retAcctInqResponse = new RetCustAcctInqResponse();

        boolean isError;
        if(serviceResponse!=""){
            serviceResponse = StringUtils.substringBetween(serviceResponse, "<Body>", "</Body>");
            isError = serviceResponse.contains("<Error>");
        }else{
            return retAcctInqResponse;
        }

        if(!isError){
            inputStream = new ByteArrayInputStream((serviceResponse.getBytes()));
            try {
                jaxbContext = JAXBContext.newInstance(RetCustAcctInqResponse.class);
                jaxbUnMarshaller = jaxbContext.createUnmarshaller();
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                retAcctInqResponse = (RetCustAcctInqResponse) jaxbUnmarshaller.unmarshal(inputStream);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            this.logger.info("Succesfully Recieved Response");
        }else{
            String errMessage = StringUtils.substringBetween(serviceResponse, "<Error>\n" +
                    "<FIBusinessException>\n" +
                    "<ErrorDetail>","</ErrorDetail>\n" +
                    "</FIBusinessException>\n" +
                    "</Error>");
            this.logger.info(errMessage);
        }


        return retAcctInqResponse;

    }

    public RetCustInqResponse                   RetCustInq(String custId){
        this.logger.info("updateRetCustomer Method pushed with details: {}",custId);
        this.fiTemplate = this.ve.getTemplate("FI-XML/RetCustInq.vm");
        StringWriter pyload = new StringWriter();
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID",requestId211);
        this.context.put("MessageDateTime",(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("CustId",custId);
        this.fiTemplate.merge(this.context,pyload);
        retCustInqResponse = new RetCustInqResponse();
        String serviceResponse = new FIExecuter().postRequest(pyload.toString());
        boolean isError=false;
        if(serviceResponse!=""){
            serviceResponse = StringUtils.substringBetween(serviceResponse, "<Body>", "</Body>");
            isError=serviceResponse.contains("<Error>");
        }else{
            return retCustInqResponse;
        }

        if(!isError){
            inputStream = new ByteArrayInputStream((serviceResponse.getBytes()));
            try {
                jaxbContext = JAXBContext.newInstance(RetCustInqResponse.class);
                jaxbUnMarshaller = jaxbContext.createUnmarshaller();
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                retCustInqResponse = (RetCustInqResponse) jaxbUnmarshaller.unmarshal(inputStream);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            this.logger.info("Succesfully Recieved Response");
        }else{
            String errMessage = StringUtils.substringBetween(serviceResponse, "<Error>\n" +
                    "<FIBusinessException>\n" +
                    "<ErrorDetail>","</ErrorDetail>\n" +
                    "</FIBusinessException>\n" +
                    "</Error>");
            this.logger.info(errMessage);
        }
        return retCustInqResponse;
    }

    public CorpCustAcctInqResponse              CorpCustAcctInq(String custId){
        this.logger.info(" Method pushed with details: {}",custId);
        this.fiTemplate = this.ve.getTemplate("FI-XML/CorpCustAcctInq.vm");
        StringWriter pyload = new StringWriter();
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID",requestId211);
        this.context.put("MessageDateTime",(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("CustId",custId);
        this.fiTemplate.merge(this.context,pyload);
        String serviceRequest = pyload.toString();
        String serviceResponse = new FIExecuter().postRequest(serviceRequest);
        boolean isError=false;
        if (serviceResponse!=""){
            isError = serviceResponse.contains("<Error>");
            serviceResponse = StringUtils.substringBetween(serviceResponse, "<Body>", "</Body>");
        }else if(serviceResponse.equals("")){
            this.logger.info("Issue Connecting to Service");
            return corpAcctInqResp;
        }

        if(!isError){
            inputStream = new ByteArrayInputStream((serviceResponse.getBytes()));
            try {
                jaxbContext = JAXBContext.newInstance(CorpCustAcctInqResponse.class);
                jaxbUnMarshaller = jaxbContext.createUnmarshaller();
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                corpAcctInqResp = (CorpCustAcctInqResponse) jaxbUnmarshaller.unmarshal(inputStream);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }else{
            String errMessage = StringUtils.substringBetween(serviceResponse, "<Error>\n" +
                    "<FIBusinessException>\n" +
                    "<ErrorDetail>","</ErrorDetail>\n" +
                    "</FIBusinessException>\n" +
                    "</Error>");
            this.logger.info(errMessage);
        }
                
        return corpAcctInqResp;

    }

    public GetCorporateCustomerDetailsResponse  CorpCustInq(String custId){
        this.logger.info("FetchCorpCuster details Method pushed with details: {}",custId);
        this.fiTemplate = this.ve.getTemplate("FI-XML/getCorporateCustomerDetails.vm");
        StringWriter pyload = new StringWriter();
        String requestId211 = "Req_" + RandomStringUtils.randomNumeric(13);
        this.context.put("RequestUUID",requestId211);
        this.context.put("MessageDateTime",(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("CustId",custId);
        this.fiTemplate.merge(this.context,pyload);
        String serviceRequest = pyload.toString();
        String serviceResponse = "";
        serviceResponse = new FIExecuter().postRequest(serviceRequest);
        boolean isError=false;
        if (serviceResponse!=""){
            serviceResponse = StringUtils.substringBetween(serviceResponse, "<Body>", "</Body>");
            isError=serviceResponse.contains("<Error>");
        }else if(serviceResponse.equals("")){
            this.logger.info("Issue Connecting to Service");
            return corpCustInqresp;
        }

        if(!isError){
            inputStream = new ByteArrayInputStream((serviceResponse.getBytes()));
            try {
                jaxbContext = JAXBContext.newInstance(GetCorporateCustomerDetailsResponse.class);
                jaxbUnMarshaller = jaxbContext.createUnmarshaller();
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                corpCustInqresp = (GetCorporateCustomerDetailsResponse) jaxbUnmarshaller.unmarshal(inputStream);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }else{
            String errMessage = StringUtils.substringBetween(serviceResponse, "<Error>\n" +
                    "<FIBusinessException>\n" +
                    "<ErrorDetail>","</ErrorDetail>\n" +
                    "</FIBusinessException>\n" +
                    "</Error>");
            this.logger.info(errMessage);
        }
        return corpCustInqresp;
    }

    //1365036018 Retail
    //0131297145 Corporate

    public String                               RetCustUpdate(CustomerDetailsForUpdate custDetails){
        String updResp = null;
        this.fiTemplate = this.ve.getTemplate("FI-XML/RetCustMod.vm");
        StringWriter pyload = new StringWriter();
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);
        String custId = custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getCustId();
        this.context.put("RequestUUID",requestId);
        this.context.put("MessageDateTime",(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("CustId",custId);

        this.context.put("AddrLineinfo",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getRetCustAddrInfo());
//        addrCategory
//        prefAddr
//        streetName
//        this.context.put("City",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getFirstName());
//        this.context.put("Country",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getLastName());
//        this.context.put("HouseNum","Mr");
//        this.context.put("State",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getFirstName());
//        this.context.put("StreetName",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getLastName());
//        this.context.put("StreetNum",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getRetCustAddrInfo());
//        this.context.put("addressID",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getFirstName());
        this.context.put("CustStatus",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getStatus());
        this.context.put("Sector",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getSector());
        this.context.put("SubSector", custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getSubSector());
        this.context.put("SegmentationClass", custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getSegmentationClass());
        this.context.put("SubSegment", custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getSubSegment());
        this.context.put("CustStatus", custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getCustStatus());
        this.context.put("DateOfBirth",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getBirthDt());
        this.context.put("Gender",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getGender());
        this.context.put("Occupation",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getOccupation());
        this.context.put("MaidenNameOfMother",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getMaidenNameOfMother());
        this.context.put("ShortName",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getShortName());
        this.context.put("FirstName",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getFirstName());
        this.context.put("LastName",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getLastName());
        this.context.put("Salutation",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getSalutation());

        this.context.put("StatusOfIntroducer", custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getStatusOfIntroducer());
        this.context.put("phoneEmailsData",custDetails.getRetCcustInqResponse().getRetCustInqRs().getRetCustDtls().getPhoneEmailInfo());

        this.fiTemplate.merge(this.context,pyload);
        String serviceResponse = new FIExecuter().postRequest(pyload.toString());
        boolean isError;
        if(serviceResponse!="Problem occurred in reaching FI"){
            serviceResponse = StringUtils.substringBetween(serviceResponse, "<Body>", "</Body>");
            isError = serviceResponse.contains("<Error>");
        }else{
            System.out.println("returning Problem occurred in reaching FI now");
            return "Problem occurred in reaching FI";
        }

        if(!isError||serviceResponse.contains("Status is not valid data category")){
            updResp = verifyCustDetails(custId,"RetailCustomer");
        }else{
            updResp = StringUtils.substringBetween(serviceResponse, "<Error>\n" +
                    "<FIBusinessException>\n" +
                    "<ErrorDetail>","</ErrorDetail>\n" +
                    "</FIBusinessException>\n" +
                    "</Error>");
        }

        return updResp;
    }

    public String                               CorpCustUpdate(CustomerDetailsForUpdate custDetails,String Address){
        String updResp = null;
        this.fiTemplate = this.ve.getTemplate("FI-XML/updateCorpCustomer.vm");
        StringWriter pyload = new StringWriter();
        String corpKey = custDetails.getCorpCustInqResp().getCorporateCustomerDetails().getCorpDet().getCorpKey();
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);;
        this.context.put("RequestUUID",requestId);
        this.context.put("MessageDateTime",(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("corpKey",corpKey);
        this.context.put("addressline3",Address);//
        this.context.put("addressid",custDetails.getCorpCustInqResp().getCorporateCustomerDetails().
                getCorpDet().getCorpaddresDet().get(0).getAddressID());
        this.context.put("dateOfIncorporation",
        custDetails.getCorpCustInqResp().getCorporateCustomerDetails().
                getCorpDet().getRelationshipStartdate());
        //2006-06-30T00:00:00.000
        this.fiTemplate.merge(this.context,pyload);
        String serviceResponse = new FIExecuter().postRequest(pyload.toString());
        boolean isError = false;
        if(serviceResponse!=""){
            serviceResponse = StringUtils.substringBetween(serviceResponse, "<Body>", "</Body>");
            isError = serviceResponse.contains("<Error>");
        }else{
            this.logger.info("Issue Connecting to FI");
            return "";
        }
        if(!isError||serviceResponse.contains("record_Status is not valid data category")){
            updResp = verifyCustDetails(corpKey,"CorporateCustomer");
        }else{
            updResp = StringUtils.substringBetween(serviceResponse, "<Error>\n" +
                    "<FIBusinessException>\n" +
                    "<ErrorDetail>","</ErrorDetail>\n" +
                    "</FIBusinessException>\n" +
                    "</Error>");
        }
        this.logger.info("Response is: "+updResp);
        return updResp;
    }

    public String                               verifyCustDetails(String custId,String entityName){
        this.fiTemplate = this.ve.getTemplate("FI-XML/verifyCustomerDetails.vm");
        StringWriter pyload = new StringWriter();
        String requestId = "Req_" + RandomStringUtils.randomNumeric(13);;
        this.context.put("RequestUUID",requestId);
        this.context.put("MessageDateTime",(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date())));
        this.context.put("cifId",custId);
        this.context.put("entityName",entityName);//
        this.fiTemplate.merge(this.context,pyload);

        String serviceResponse = new FIExecuter().postRequest(pyload.toString());

        boolean isError = false;
        if(serviceResponse!=""){
            serviceResponse = StringUtils.substringBetween(serviceResponse, "<Body>", "</Body>");
            isError= serviceResponse.contains("<Error>");
        }else{
            return "empty";
        }
        if(!isError){
            return "Verified Succesfully";
        }else{
            return StringUtils.substringBetween(serviceResponse, "<Error>\n" +
                    "<FIBusinessException>\n" +
                    "<ErrorDetail>","</ErrorDetail>\n" +
                    "</FIBusinessException>\n" +
                    "</Error>");
        }

    }



}


package com.longbridge.FI.FiRetAcctInq;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.longbridge.FI.FiRetAcctInq package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.longbridge.FI.FiRetAcctInq
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetCustAcctInqResponse }
     * 
     */
    public RetCustAcctInqResponse createRetCustAcctInqResponse() {
        return new RetCustAcctInqResponse();
    }

    /**
     * Create an instance of {@link RetCustAcctInqResponse.RetCustAcctInqRs }
     * 
     */
    public RetCustAcctInqResponse.RetCustAcctInqRs createRetCustAcctInqResponseRetCustAcctInqRs() {
        return new RetCustAcctInqResponse.RetCustAcctInqRs();
    }

    /**
     * Create an instance of {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls }
     * 
     */
    public RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls createRetCustAcctInqResponseRetCustAcctInqRsRetSaleDtls() {
        return new RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls();
    }

    /**
     * Create an instance of {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetCustDtlsInfo }
     * 
     */
    public RetCustAcctInqResponse.RetCustAcctInqRs.RetCustDtlsInfo createRetCustAcctInqResponseRetCustAcctInqRsRetCustDtlsInfo() {
        return new RetCustAcctInqResponse.RetCustAcctInqRs.RetCustDtlsInfo();
    }

    /**
     * Create an instance of {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.ProductDtlsInfo }
     * 
     */
    public RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.ProductDtlsInfo createRetCustAcctInqResponseRetCustAcctInqRsRetSaleDtlsProductDtlsInfo() {
        return new RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.ProductDtlsInfo();
    }

    /**
     * Create an instance of {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleChannelDtls }
     * 
     */
    public RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleChannelDtls createRetCustAcctInqResponseRetCustAcctInqRsRetSaleDtlsSaleChannelDtls() {
        return new RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleChannelDtls();
    }

    /**
     * Create an instance of {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleCust }
     * 
     */
    public RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleCust createRetCustAcctInqResponseRetCustAcctInqRsRetSaleDtlsSaleCust() {
        return new RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleCust();
    }

}

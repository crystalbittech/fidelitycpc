
package com.longbridge.FI.FiRetAcctInq;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RetCustAcctInqRs">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RetCustDtlsInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsSwiftCodeOfBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HouseholdFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HouseholdName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GroupId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsCorpRepresentative" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustSwiftCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NativeLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NativeLanguage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubSector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubSegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TDSCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TDSTable" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RetSaleDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AcctBranchCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ChannelCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ChannelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsMasterAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsMultiCurrAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProductCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProductCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProductDtlsInfo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ProductFeature" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="IsRevolvingODAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SaleChannelDtls">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="AcctCurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="AcctOpeningDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="AcctStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="AcctShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CrScoreStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="EligibilityResults" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="IntRepaymentAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ModeOfOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PosteIDSRStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PrefRepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="RepaymentAcctBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="RepaymentAcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="RepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="SaleCust">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="TopUpFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProdCatId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="isPPF" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RetCustAcctInq_CustomData" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "retCustAcctInqRs",
    "retCustAcctInqCustomData"
})
@XmlRootElement(name = "RetCustAcctInqResponse")
public class RetCustAcctInqResponse implements Serializable {

    @XmlElement(name = "RetCustAcctInqRs", required = true)
    protected RetCustAcctInqResponse.RetCustAcctInqRs retCustAcctInqRs;
    @XmlElement(name = "RetCustAcctInq_CustomData", required = true)
    protected String retCustAcctInqCustomData;

    /**
     * Gets the value of the retCustAcctInqRs property.
     * 
     * @return
     *     possible object is
     *     {@link RetCustAcctInqResponse.RetCustAcctInqRs }
     *     
     */
    public RetCustAcctInqResponse.RetCustAcctInqRs getRetCustAcctInqRs() {
        return retCustAcctInqRs;
    }

    /**
     * Sets the value of the retCustAcctInqRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetCustAcctInqResponse.RetCustAcctInqRs }
     *     
     */
    public void setRetCustAcctInqRs(RetCustAcctInqResponse.RetCustAcctInqRs value) {
        this.retCustAcctInqRs = value;
    }

    /**
     * Gets the value of the retCustAcctInqCustomData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetCustAcctInqCustomData() {
        return retCustAcctInqCustomData;
    }

    /**
     * Sets the value of the retCustAcctInqCustomData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetCustAcctInqCustomData(String value) {
        this.retCustAcctInqCustomData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RetCustDtlsInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsSwiftCodeOfBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HouseholdFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HouseholdName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GroupId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsCorpRepresentative" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustSwiftCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NativeLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NativeLanguage" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubSector" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubSegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TDSCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TDSTable" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RetSaleDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AcctBranchCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ChannelCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ChannelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsMasterAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsMultiCurrAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProductCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProductCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProductDtlsInfo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ProductFeature" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="IsRevolvingODAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SaleChannelDtls">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="AcctCurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="AcctOpeningDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="AcctStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="AcctShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CrScoreStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="EligibilityResults" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="IntRepaymentAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ModeOfOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PosteIDSRStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PrefRepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RepaymentAcctBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RepaymentAcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="SaleCust">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="TopUpFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProdCatId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="isPPF" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "retCustDtlsInfo",
        "retSaleDtls"
    })
    public static class RetCustAcctInqRs implements Serializable {

        @XmlElement(name = "RetCustDtlsInfo", required = true)
        protected RetCustAcctInqResponse.RetCustAcctInqRs.RetCustDtlsInfo retCustDtlsInfo;
        @XmlElement(name = "RetSaleDtls", required = true)
        protected RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls retSaleDtls;

        /**
         * Gets the value of the retCustDtlsInfo property.
         * 
         * @return
         *     possible object is
         *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetCustDtlsInfo }
         *     
         */
        public RetCustAcctInqResponse.RetCustAcctInqRs.RetCustDtlsInfo getRetCustDtlsInfo() {
            return retCustDtlsInfo;
        }

        /**
         * Sets the value of the retCustDtlsInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetCustDtlsInfo }
         *     
         */
        public void setRetCustDtlsInfo(RetCustAcctInqResponse.RetCustAcctInqRs.RetCustDtlsInfo value) {
            this.retCustDtlsInfo = value;
        }

        /**
         * Gets the value of the retSaleDtls property.
         * 
         * @return
         *     possible object is
         *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls }
         *     
         */
        public RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls getRetSaleDtls() {
            return retSaleDtls;
        }

        /**
         * Sets the value of the retSaleDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls }
         *     
         */
        public void setRetSaleDtls(RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls value) {
            this.retSaleDtls = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsSwiftCodeOfBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HouseholdFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HouseholdName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GroupId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsCorpRepresentative" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustSwiftCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NativeLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NativeLanguage" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubSector" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubSegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TDSCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TDSTable" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "firstName",
            "lastName",
            "isSwiftCodeOfBank",
            "householdFlag",
            "householdName",
            "groupId",
            "defaultAddrType",
            "custTypeCode",
            "isCorpRepresentative",
            "custSwiftCode",
            "nativeLanguageCode",
            "nativeLanguage",
            "occupationCode",
            "salutationCode",
            "sector",
            "sectorCode",
            "statusCode",
            "subSector",
            "subSectorCode",
            "subSegment",
            "tdsCustId",
            "tdsTable"
        })
        public static class RetCustDtlsInfo implements Serializable{

            @XmlElement(name = "FirstName", required = true)
            protected String firstName;
            @XmlElement(name = "LastName", required = true)
            protected String lastName;
            @XmlElement(name = "IsSwiftCodeOfBank", required = true)
            protected String isSwiftCodeOfBank;
            @XmlElement(name = "HouseholdFlag", required = true)
            protected String householdFlag;
            @XmlElement(name = "HouseholdName", required = true)
            protected String householdName;
            @XmlElement(name = "GroupId", required = true)
            protected String groupId;
            @XmlElement(name = "DefaultAddrType", required = true)
            protected String defaultAddrType;
            @XmlElement(name = "CustTypeCode", required = true)
            protected String custTypeCode;
            @XmlElement(name = "IsCorpRepresentative", required = true)
            protected String isCorpRepresentative;
            @XmlElement(name = "CustSwiftCode", required = true)
            protected String custSwiftCode;
            @XmlElement(name = "NativeLanguageCode", required = true)
            protected String nativeLanguageCode;
            @XmlElement(name = "NativeLanguage", required = true)
            protected String nativeLanguage;
            @XmlElement(name = "OccupationCode", required = true)
            protected String occupationCode;
            @XmlElement(name = "SalutationCode", required = true)
            protected String salutationCode;
            @XmlElement(name = "Sector", required = true)
            protected String sector;
            @XmlElement(name = "SectorCode", required = true)
            protected String sectorCode;
            @XmlElement(name = "StatusCode", required = true)
            protected String statusCode;
            @XmlElement(name = "SubSector", required = true)
            protected String subSector;
            @XmlElement(name = "SubSectorCode", required = true)
            protected String subSectorCode;
            @XmlElement(name = "SubSegment", required = true)
            protected String subSegment;
            @XmlElement(name = "TDSCustId", required = true)
            protected String tdsCustId;
            @XmlElement(name = "TDSTable", required = true)
            protected String tdsTable;

            /**
             * Gets the value of the firstName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstName() {
                return firstName;
            }

            /**
             * Sets the value of the firstName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstName(String value) {
                this.firstName = value;
            }

            /**
             * Gets the value of the lastName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastName() {
                return lastName;
            }

            /**
             * Sets the value of the lastName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastName(String value) {
                this.lastName = value;
            }

            /**
             * Gets the value of the isSwiftCodeOfBank property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsSwiftCodeOfBank() {
                return isSwiftCodeOfBank;
            }

            /**
             * Sets the value of the isSwiftCodeOfBank property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsSwiftCodeOfBank(String value) {
                this.isSwiftCodeOfBank = value;
            }

            /**
             * Gets the value of the householdFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseholdFlag() {
                return householdFlag;
            }

            /**
             * Sets the value of the householdFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseholdFlag(String value) {
                this.householdFlag = value;
            }

            /**
             * Gets the value of the householdName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseholdName() {
                return householdName;
            }

            /**
             * Sets the value of the householdName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseholdName(String value) {
                this.householdName = value;
            }

            /**
             * Gets the value of the groupId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupId() {
                return groupId;
            }

            /**
             * Sets the value of the groupId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupId(String value) {
                this.groupId = value;
            }

            /**
             * Gets the value of the defaultAddrType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDefaultAddrType() {
                return defaultAddrType;
            }

            /**
             * Sets the value of the defaultAddrType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDefaultAddrType(String value) {
                this.defaultAddrType = value;
            }

            /**
             * Gets the value of the custTypeCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustTypeCode() {
                return custTypeCode;
            }

            /**
             * Sets the value of the custTypeCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustTypeCode(String value) {
                this.custTypeCode = value;
            }

            /**
             * Gets the value of the isCorpRepresentative property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsCorpRepresentative() {
                return isCorpRepresentative;
            }

            /**
             * Sets the value of the isCorpRepresentative property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsCorpRepresentative(String value) {
                this.isCorpRepresentative = value;
            }

            /**
             * Gets the value of the custSwiftCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustSwiftCode() {
                return custSwiftCode;
            }

            /**
             * Sets the value of the custSwiftCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustSwiftCode(String value) {
                this.custSwiftCode = value;
            }

            /**
             * Gets the value of the nativeLanguageCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNativeLanguageCode() {
                return nativeLanguageCode;
            }

            /**
             * Sets the value of the nativeLanguageCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNativeLanguageCode(String value) {
                this.nativeLanguageCode = value;
            }

            /**
             * Gets the value of the nativeLanguage property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNativeLanguage() {
                return nativeLanguage;
            }

            /**
             * Sets the value of the nativeLanguage property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNativeLanguage(String value) {
                this.nativeLanguage = value;
            }

            /**
             * Gets the value of the occupationCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOccupationCode() {
                return occupationCode;
            }

            /**
             * Sets the value of the occupationCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOccupationCode(String value) {
                this.occupationCode = value;
            }

            /**
             * Gets the value of the salutationCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSalutationCode() {
                return salutationCode;
            }

            /**
             * Sets the value of the salutationCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSalutationCode(String value) {
                this.salutationCode = value;
            }

            /**
             * Gets the value of the sector property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSector() {
                return sector;
            }

            /**
             * Sets the value of the sector property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSector(String value) {
                this.sector = value;
            }

            /**
             * Gets the value of the sectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSectorCode() {
                return sectorCode;
            }

            /**
             * Sets the value of the sectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSectorCode(String value) {
                this.sectorCode = value;
            }

            /**
             * Gets the value of the statusCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusCode() {
                return statusCode;
            }

            /**
             * Sets the value of the statusCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusCode(String value) {
                this.statusCode = value;
            }

            /**
             * Gets the value of the subSector property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubSector() {
                return subSector;
            }

            /**
             * Sets the value of the subSector property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubSector(String value) {
                this.subSector = value;
            }

            /**
             * Gets the value of the subSectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubSectorCode() {
                return subSectorCode;
            }

            /**
             * Sets the value of the subSectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubSectorCode(String value) {
                this.subSectorCode = value;
            }

            /**
             * Gets the value of the subSegment property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubSegment() {
                return subSegment;
            }

            /**
             * Sets the value of the subSegment property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubSegment(String value) {
                this.subSegment = value;
            }

            /**
             * Gets the value of the tdsCustId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTDSCustId() {
                return tdsCustId;
            }

            /**
             * Sets the value of the tdsCustId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTDSCustId(String value) {
                this.tdsCustId = value;
            }

            /**
             * Gets the value of the tdsTable property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTDSTable() {
                return tdsTable;
            }

            /**
             * Sets the value of the tdsTable property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTDSTable(String value) {
                this.tdsTable = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AcctBranchCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ChannelCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ChannelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsMasterAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsMultiCurrAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProductCategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProductCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProductDtlsInfo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ProductFeature" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="IsRevolvingODAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SaleChannelDtls">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="AcctCurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="AcctOpeningDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="AcctStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="AcctShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CrScoreStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="EligibilityResults" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="IntRepaymentAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ModeOfOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PosteIDSRStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PrefRepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RepaymentAcctBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RepaymentAcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="SaleCust">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="TopUpFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProdCatId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="isPPF" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "acctBranchCode",
            "acctId",
            "channelCustId",
            "channelName",
            "isMasterAcct",
            "isMultiCurrAcct",
            "productCategory",
            "productCode",
            "productDtlsInfo",
            "isRevolvingODAcct",
            "saleChannelDtls",
            "saleCust",
            "topUpFlag",
            "prodCatId",
            "isPPF"
        })
        public static class RetSaleDtls implements Serializable{

            @XmlElement(name = "AcctBranchCode", required = true)
            protected String acctBranchCode;
            @XmlElement(name = "AcctId", required = true)
            protected String acctId;
            @XmlElement(name = "ChannelCustId", required = true)
            protected String channelCustId;
            @XmlElement(name = "ChannelName", required = true)
            protected String channelName;
            @XmlElement(name = "IsMasterAcct", required = true)
            protected String isMasterAcct;
            @XmlElement(name = "IsMultiCurrAcct", required = true)
            protected String isMultiCurrAcct;
            @XmlElement(name = "ProductCategory", required = true)
            protected String productCategory;
            @XmlElement(name = "ProductCode", required = true)
            protected String productCode;
            @XmlElement(name = "ProductDtlsInfo", required = true)
            protected RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.ProductDtlsInfo productDtlsInfo;
            @XmlElement(name = "IsRevolvingODAcct", required = true)
            protected String isRevolvingODAcct;
            @XmlElement(name = "SaleChannelDtls", required = true)
            protected RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleChannelDtls saleChannelDtls;
            @XmlElement(name = "SaleCust", required = true)
            protected RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleCust saleCust;
            @XmlElement(name = "TopUpFlag", required = true)
            protected String topUpFlag;
            @XmlElement(name = "ProdCatId", required = true)
            protected String prodCatId;
            @XmlElement(required = true)
            protected String isPPF;

            /**
             * Gets the value of the acctBranchCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctBranchCode() {
                return acctBranchCode;
            }

            /**
             * Sets the value of the acctBranchCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctBranchCode(String value) {
                this.acctBranchCode = value;
            }

            /**
             * Gets the value of the acctId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctId() {
                return acctId;
            }

            /**
             * Sets the value of the acctId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctId(String value) {
                this.acctId = value;
            }

            /**
             * Gets the value of the channelCustId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChannelCustId() {
                return channelCustId;
            }

            /**
             * Sets the value of the channelCustId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChannelCustId(String value) {
                this.channelCustId = value;
            }

            /**
             * Gets the value of the channelName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChannelName() {
                return channelName;
            }

            /**
             * Sets the value of the channelName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChannelName(String value) {
                this.channelName = value;
            }

            /**
             * Gets the value of the isMasterAcct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsMasterAcct() {
                return isMasterAcct;
            }

            /**
             * Sets the value of the isMasterAcct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsMasterAcct(String value) {
                this.isMasterAcct = value;
            }

            /**
             * Gets the value of the isMultiCurrAcct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsMultiCurrAcct() {
                return isMultiCurrAcct;
            }

            /**
             * Sets the value of the isMultiCurrAcct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsMultiCurrAcct(String value) {
                this.isMultiCurrAcct = value;
            }

            /**
             * Gets the value of the productCategory property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProductCategory() {
                return productCategory;
            }

            /**
             * Sets the value of the productCategory property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProductCategory(String value) {
                this.productCategory = value;
            }

            /**
             * Gets the value of the productCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProductCode() {
                return productCode;
            }

            /**
             * Sets the value of the productCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProductCode(String value) {
                this.productCode = value;
            }

            /**
             * Gets the value of the productDtlsInfo property.
             * 
             * @return
             *     possible object is
             *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.ProductDtlsInfo }
             *     
             */
            public RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.ProductDtlsInfo getProductDtlsInfo() {
                return productDtlsInfo;
            }

            /**
             * Sets the value of the productDtlsInfo property.
             * 
             * @param value
             *     allowed object is
             *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.ProductDtlsInfo }
             *     
             */
            public void setProductDtlsInfo(RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.ProductDtlsInfo value) {
                this.productDtlsInfo = value;
            }

            /**
             * Gets the value of the isRevolvingODAcct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsRevolvingODAcct() {
                return isRevolvingODAcct;
            }

            /**
             * Sets the value of the isRevolvingODAcct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsRevolvingODAcct(String value) {
                this.isRevolvingODAcct = value;
            }

            /**
             * Gets the value of the saleChannelDtls property.
             * 
             * @return
             *     possible object is
             *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleChannelDtls }
             *     
             */
            public RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleChannelDtls getSaleChannelDtls() {
                return saleChannelDtls;
            }

            /**
             * Sets the value of the saleChannelDtls property.
             * 
             * @param value
             *     allowed object is
             *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleChannelDtls }
             *     
             */
            public void setSaleChannelDtls(RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleChannelDtls value) {
                this.saleChannelDtls = value;
            }

            /**
             * Gets the value of the saleCust property.
             * 
             * @return
             *     possible object is
             *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleCust }
             *     
             */
            public RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleCust getSaleCust() {
                return saleCust;
            }

            /**
             * Sets the value of the saleCust property.
             * 
             * @param value
             *     allowed object is
             *     {@link RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleCust }
             *     
             */
            public void setSaleCust(RetCustAcctInqResponse.RetCustAcctInqRs.RetSaleDtls.SaleCust value) {
                this.saleCust = value;
            }

            /**
             * Gets the value of the topUpFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTopUpFlag() {
                return topUpFlag;
            }

            /**
             * Sets the value of the topUpFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTopUpFlag(String value) {
                this.topUpFlag = value;
            }

            /**
             * Gets the value of the prodCatId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProdCatId() {
                return prodCatId;
            }

            /**
             * Sets the value of the prodCatId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProdCatId(String value) {
                this.prodCatId = value;
            }

            /**
             * Gets the value of the isPPF property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsPPF() {
                return isPPF;
            }

            /**
             * Sets the value of the isPPF property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsPPF(String value) {
                this.isPPF = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ProductFeature" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "productFeature"
            })
            public static class ProductDtlsInfo implements Serializable{

                @XmlElement(name = "ProductFeature", required = true)
                protected String productFeature;

                /**
                 * Gets the value of the productFeature property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProductFeature() {
                    return productFeature;
                }

                /**
                 * Sets the value of the productFeature property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProductFeature(String value) {
                    this.productFeature = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="AcctCurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="AcctOpeningDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="AcctStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="AcctShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CrScoreStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="EligibilityResults" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="IntRepaymentAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ModeOfOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PosteIDSRStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PrefRepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RepaymentAcctBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RepaymentAcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "acctCurrCode",
                "acctOpeningDt",
                "acctStatus",
                "acctShortName",
                "crScoreStatus",
                "eligibilityResults",
                "intRepaymentAcct",
                "modeOfOperation",
                "posteIDSRStatus",
                "prefRepaymentMode",
                "repaymentAcctBank",
                "repaymentAcctId",
                "repaymentMode"
            })
            public static class SaleChannelDtls implements Serializable{

                @XmlElement(name = "AcctCurrCode", required = true)
                protected String acctCurrCode;
                @XmlElement(name = "AcctOpeningDt", required = true)
                protected String acctOpeningDt;
                @XmlElement(name = "AcctStatus", required = true)
                protected String acctStatus;
                @XmlElement(name = "AcctShortName", required = true)
                protected String acctShortName;
                @XmlElement(name = "CrScoreStatus", required = true)
                protected String crScoreStatus;
                @XmlElement(name = "EligibilityResults", required = true)
                protected String eligibilityResults;
                @XmlElement(name = "IntRepaymentAcct", required = true)
                protected String intRepaymentAcct;
                @XmlElement(name = "ModeOfOperation", required = true)
                protected String modeOfOperation;
                @XmlElement(name = "PosteIDSRStatus", required = true)
                protected String posteIDSRStatus;
                @XmlElement(name = "PrefRepaymentMode", required = true)
                protected String prefRepaymentMode;
                @XmlElement(name = "RepaymentAcctBank", required = true)
                protected String repaymentAcctBank;
                @XmlElement(name = "RepaymentAcctId", required = true)
                protected String repaymentAcctId;
                @XmlElement(name = "RepaymentMode", required = true)
                protected String repaymentMode;

                /**
                 * Gets the value of the acctCurrCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctCurrCode() {
                    return acctCurrCode;
                }

                /**
                 * Sets the value of the acctCurrCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctCurrCode(String value) {
                    this.acctCurrCode = value;
                }

                /**
                 * Gets the value of the acctOpeningDt property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctOpeningDt() {
                    return acctOpeningDt;
                }

                /**
                 * Sets the value of the acctOpeningDt property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctOpeningDt(String value) {
                    this.acctOpeningDt = value;
                }

                /**
                 * Gets the value of the acctStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctStatus() {
                    return acctStatus;
                }

                /**
                 * Sets the value of the acctStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctStatus(String value) {
                    this.acctStatus = value;
                }

                /**
                 * Gets the value of the acctShortName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctShortName() {
                    return acctShortName;
                }

                /**
                 * Sets the value of the acctShortName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctShortName(String value) {
                    this.acctShortName = value;
                }

                /**
                 * Gets the value of the crScoreStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCrScoreStatus() {
                    return crScoreStatus;
                }

                /**
                 * Sets the value of the crScoreStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCrScoreStatus(String value) {
                    this.crScoreStatus = value;
                }

                /**
                 * Gets the value of the eligibilityResults property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEligibilityResults() {
                    return eligibilityResults;
                }

                /**
                 * Sets the value of the eligibilityResults property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEligibilityResults(String value) {
                    this.eligibilityResults = value;
                }

                /**
                 * Gets the value of the intRepaymentAcct property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIntRepaymentAcct() {
                    return intRepaymentAcct;
                }

                /**
                 * Sets the value of the intRepaymentAcct property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIntRepaymentAcct(String value) {
                    this.intRepaymentAcct = value;
                }

                /**
                 * Gets the value of the modeOfOperation property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getModeOfOperation() {
                    return modeOfOperation;
                }

                /**
                 * Sets the value of the modeOfOperation property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setModeOfOperation(String value) {
                    this.modeOfOperation = value;
                }

                /**
                 * Gets the value of the posteIDSRStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPosteIDSRStatus() {
                    return posteIDSRStatus;
                }

                /**
                 * Sets the value of the posteIDSRStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPosteIDSRStatus(String value) {
                    this.posteIDSRStatus = value;
                }

                /**
                 * Gets the value of the prefRepaymentMode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPrefRepaymentMode() {
                    return prefRepaymentMode;
                }

                /**
                 * Sets the value of the prefRepaymentMode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPrefRepaymentMode(String value) {
                    this.prefRepaymentMode = value;
                }

                /**
                 * Gets the value of the repaymentAcctBank property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRepaymentAcctBank() {
                    return repaymentAcctBank;
                }

                /**
                 * Sets the value of the repaymentAcctBank property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRepaymentAcctBank(String value) {
                    this.repaymentAcctBank = value;
                }

                /**
                 * Gets the value of the repaymentAcctId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRepaymentAcctId() {
                    return repaymentAcctId;
                }

                /**
                 * Sets the value of the repaymentAcctId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRepaymentAcctId(String value) {
                    this.repaymentAcctId = value;
                }

                /**
                 * Gets the value of the repaymentMode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRepaymentMode() {
                    return repaymentMode;
                }

                /**
                 * Sets the value of the repaymentMode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRepaymentMode(String value) {
                    this.repaymentMode = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "role"
            })
            public static class SaleCust implements Serializable{

                @XmlElement(name = "Role", required = true)
                protected String role;

                /**
                 * Gets the value of the role property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRole() {
                    return role;
                }

                /**
                 * Sets the value of the role property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRole(String value) {
                    this.role = value;
                }

            }

        }

    }

}


package com.longbridge.FI.FIGenAcctInq;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.longbridge.FI.FIGenAcctInq package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.longbridge.FI.FIGenAcctInq
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AcctInqResponse }
     * 
     */
    public AcctInqResponse createAcctInqResponse() {
        return new AcctInqResponse();
    }

    /**
     * Create an instance of {@link AcctInqResponse.AcctInqRs }
     * 
     */
    public AcctInqResponse.AcctInqRs createAcctInqResponseAcctInqRs() {
        return new AcctInqResponse.AcctInqRs();
    }

    /**
     * Create an instance of {@link AcctInqResponse.AcctInqRs.AcctBal }
     * 
     */
    public AcctInqResponse.AcctInqRs.AcctBal createAcctInqResponseAcctInqRsAcctBal() {
        return new AcctInqResponse.AcctInqRs.AcctBal();
    }

    /**
     * Create an instance of {@link AcctInqResponse.AcctInqRs.CustId }
     * 
     */
    public AcctInqResponse.AcctInqRs.CustId createAcctInqResponseAcctInqRsCustId() {
        return new AcctInqResponse.AcctInqRs.CustId();
    }

    /**
     * Create an instance of {@link AcctInqResponse.AcctInqRs.AcctId }
     * 
     */
    public AcctInqResponse.AcctInqRs.AcctId createAcctInqResponseAcctInqRsAcctId() {
        return new AcctInqResponse.AcctInqRs.AcctId();
    }

    /**
     * Create an instance of {@link AcctInqResponse.AcctInqRs.AcctId.BankInfo }
     * 
     */
    public AcctInqResponse.AcctInqRs.AcctId.BankInfo createAcctInqResponseAcctInqRsAcctIdBankInfo() {
        return new AcctInqResponse.AcctInqRs.AcctId.BankInfo();
    }

    /**
     * Create an instance of {@link AcctInqResponse.AcctInqRs.AcctBal.BalAmt }
     * 
     */
    public AcctInqResponse.AcctInqRs.AcctBal.BalAmt createAcctInqResponseAcctInqRsAcctBalBalAmt() {
        return new AcctInqResponse.AcctInqRs.AcctBal.BalAmt();
    }

    /**
     * Create an instance of {@link AcctInqResponse.AcctInqRs.CustId.PersonName }
     * 
     */
    public AcctInqResponse.AcctInqRs.CustId.PersonName createAcctInqResponseAcctInqRsCustIdPersonName() {
        return new AcctInqResponse.AcctInqRs.CustId.PersonName();
    }

    /**
     * Create an instance of {@link AcctInqResponse.AcctInqRs.AcctId.AcctType }
     * 
     */
    public AcctInqResponse.AcctInqRs.AcctId.AcctType createAcctInqResponseAcctInqRsAcctIdAcctType() {
        return new AcctInqResponse.AcctInqRs.AcctId.AcctType();
    }

    /**
     * Create an instance of {@link AcctInqResponse.AcctInqRs.AcctId.BankInfo.PostAddr }
     * 
     */
    public AcctInqResponse.AcctInqRs.AcctId.BankInfo.PostAddr createAcctInqResponseAcctInqRsAcctIdBankInfoPostAddr() {
        return new AcctInqResponse.AcctInqRs.AcctId.BankInfo.PostAddr();
    }

}

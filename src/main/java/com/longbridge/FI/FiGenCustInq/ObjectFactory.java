
package com.longbridge.FI.FiGenCustInq;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.longbridge.FI.FiGenCustInq package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.longbridge.FI.FiGenCustInq
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CustInqResponse }
     * 
     */
    public CustInqResponse createCustInqResponse() {
        return new CustInqResponse();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs }
     * 
     */
    public CustInqResponse.CustInqRs createCustInqResponseCustInqRs() {
        return new CustInqResponse.CustInqRs();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.RetailCustInfo }
     * 
     */
    public CustInqResponse.CustInqRs.RetailCustInfo createCustInqResponseCustInqRsRetailCustInfo() {
        return new CustInqResponse.CustInqRs.RetailCustInfo();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.BlackListNegativeListDtls }
     * 
     */
    public CustInqResponse.CustInqRs.BlackListNegativeListDtls createCustInqResponseCustInqRsBlackListNegativeListDtls() {
        return new CustInqResponse.CustInqRs.BlackListNegativeListDtls();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.CustInqAddrInfo }
     * 
     */
    public CustInqResponse.CustInqRs.CustInqAddrInfo createCustInqResponseCustInqRsCustInqAddrInfo() {
        return new CustInqResponse.CustInqRs.CustInqAddrInfo();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.CorpCustInfo }
     * 
     */
    public CustInqResponse.CustInqRs.CorpCustInfo createCustInqResponseCustInqRsCorpCustInfo() {
        return new CustInqResponse.CustInqRs.CorpCustInfo();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.EntityDocInfo }
     * 
     */
    public CustInqResponse.CustInqRs.EntityDocInfo createCustInqResponseCustInqRsEntityDocInfo() {
        return new CustInqResponse.CustInqRs.EntityDocInfo();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.GenCustDtls }
     * 
     */
    public CustInqResponse.CustInqRs.GenCustDtls createCustInqResponseCustInqRsGenCustDtls() {
        return new CustInqResponse.CustInqRs.GenCustDtls();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.CustPhoneEmailInfo }
     * 
     */
    public CustInqResponse.CustInqRs.CustPhoneEmailInfo createCustInqResponseCustInqRsCustPhoneEmailInfo() {
        return new CustInqResponse.CustInqRs.CustPhoneEmailInfo();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.RetailCustInfo.EmploymentDtls }
     * 
     */
    public CustInqResponse.CustInqRs.RetailCustInfo.EmploymentDtls createCustInqResponseCustInqRsRetailCustInfoEmploymentDtls() {
        return new CustInqResponse.CustInqRs.RetailCustInfo.EmploymentDtls();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.BlackListNegativeListDtls.BlacklistReasonCode }
     * 
     */
    public CustInqResponse.CustInqRs.BlackListNegativeListDtls.BlacklistReasonCode createCustInqResponseCustInqRsBlackListNegativeListDtlsBlacklistReasonCode() {
        return new CustInqResponse.CustInqRs.BlackListNegativeListDtls.BlacklistReasonCode();
    }

    /**
     * Create an instance of {@link CustInqResponse.CustInqRs.BlackListNegativeListDtls.NegationReasonCode }
     * 
     */
    public CustInqResponse.CustInqRs.BlackListNegativeListDtls.NegationReasonCode createCustInqResponseCustInqRsBlackListNegativeListDtlsNegationReasonCode() {
        return new CustInqResponse.CustInqRs.BlackListNegativeListDtls.NegationReasonCode();
    }

}


package com.longbridge.FI.FiGenCustInq;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustInqRs">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustInqAddrInfo" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AddrEndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AddrStartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AddrType">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="Home"/>
 *                                   &lt;enumeration value="Mailing"/>
 *                                   &lt;enumeration value="Work"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="AddrLine1">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="NO ADDRESS IN FIN7"/>
 *                                   &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="AddrLine2">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value=""/>
 *                                   &lt;enumeration value="WARRI"/>
 *                                   &lt;enumeration value="."/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="AddrLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BuildingLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FreeTextAddr">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="."/>
 *                                   &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="HouseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LocalityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PremiseName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StateCode">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="LG"/>
 *                                   &lt;enumeration value="DL"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="StreetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StreetNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Town" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BlackListNegativeListDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="IsBlacklisted" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BlacklistNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BlacklistReasonCode">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="IsNegated" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NegationNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NegationReasonCode">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CorpCustInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CitryOfIncorporation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LegalEntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrincipalNatureOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrincipalPlaceOfOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrimaryRelMgrId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="EntityDocInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DocCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IssueDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsMandatory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PlaceOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsScanReqd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="UniqueId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="GenCustDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ChannelCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DSAId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EmployeeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GroupIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GSTFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NameInNativeLanguage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrimaryDocType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrimaryRMId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RatingCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SecondaryRMId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Segment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SegmentLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SegmentNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SegmentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SICCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsStaff" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsSuspended" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TertiaryRMId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BranchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CustPhoneEmailInfo" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Email">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value=""/>
 *                                   &lt;enumeration value="noemail@fcmb.com"/>
 *                                   &lt;enumeration value="felixgrammar@yahoo.com"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="EmailPalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PhoneEmailType">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="COMM1"/>
 *                                   &lt;enumeration value="WORK1"/>
 *                                   &lt;enumeration value="COMML"/>
 *                                   &lt;enumeration value="WORKL"/>
 *                                   &lt;enumeration value="HOMEL"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="PhoneNum">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="+234(0)08052003607"/>
 *                                   &lt;enumeration value=""/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="PhoneNumCityCode">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="0"/>
 *                                   &lt;enumeration value=""/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="PhoneNumCountryCode">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="234"/>
 *                                   &lt;enumeration value=""/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="PhoneNumLocalCode">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="08052003607"/>
 *                                   &lt;enumeration value=""/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="PhoneOrEmail">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="PHONE"/>
 *                                   &lt;enumeration value="EMAIL"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="PrefFlag">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="Y"/>
 *                                   &lt;enumeration value="N"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="WorkExtNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RetailCustInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AdditionalName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BirthDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EmploymentDtls">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="EmployementStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="NameOfEmployer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="JobTitleCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="JobTitleDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="OccupationDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PeriodOfEmployment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MaritalStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="MaritalStatusDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NationalityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NationalityDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OptOutInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Race" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ResidingCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ResidingCountryDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RaceDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PsychographicInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CustInq_CustomData" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "custInqRs",
    "custInqCustomData"
})
@XmlRootElement(name = "CustInqResponse")
public class CustInqResponse implements Serializable {

    @XmlElement(name = "CustInqRs", required = true)
    protected CustInqResponse.CustInqRs custInqRs;
    @XmlElement(name = "CustInq_CustomData", required = true)
    protected String custInqCustomData;

    /**
     * Gets the value of the custInqRs property.
     * 
     * @return
     *     possible object is
     *     {@link CustInqResponse.CustInqRs }
     *     
     */
    public CustInqResponse.CustInqRs getCustInqRs() {
        return custInqRs;
    }

    /**
     * Sets the value of the custInqRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustInqResponse.CustInqRs }
     *     
     */
    public void setCustInqRs(CustInqResponse.CustInqRs value) {
        this.custInqRs = value;
    }

    /**
     * Gets the value of the custInqCustomData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustInqCustomData() {
        return custInqCustomData;
    }

    /**
     * Sets the value of the custInqCustomData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustInqCustomData(String value) {
        this.custInqCustomData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustInqAddrInfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AddrEndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AddrStartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AddrType">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="Home"/>
     *                         &lt;enumeration value="Mailing"/>
     *                         &lt;enumeration value="Work"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="AddrLine1">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="NO ADDRESS IN FIN7"/>
     *                         &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="AddrLine2">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value=""/>
     *                         &lt;enumeration value="WARRI"/>
     *                         &lt;enumeration value="."/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="AddrLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BuildingLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FreeTextAddr">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="."/>
     *                         &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="HouseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LocalityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PremiseName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StateCode">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="LG"/>
     *                         &lt;enumeration value="DL"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="StreetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StreetNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Town" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BlackListNegativeListDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="IsBlacklisted" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BlacklistNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BlacklistReasonCode">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="IsNegated" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NegationNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NegationReasonCode">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CorpCustInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CitryOfIncorporation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LegalEntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrincipalNatureOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrincipalPlaceOfOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrimaryRelMgrId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="EntityDocInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DocCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IssueDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsMandatory" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PlaceOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsScanReqd" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="UniqueId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="GenCustDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ChannelCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DSAId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EmployeeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GroupIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GSTFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NameInNativeLanguage" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrimaryDocType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrimaryRMId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RatingCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SecondaryRMId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Segment" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SegmentLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SegmentNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SegmentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SICCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsStaff" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsSuspended" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TertiaryRMId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BranchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CustPhoneEmailInfo" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Email">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value=""/>
     *                         &lt;enumeration value="noemail@fcmb.com"/>
     *                         &lt;enumeration value="felixgrammar@yahoo.com"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="EmailPalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PhoneEmailType">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="COMM1"/>
     *                         &lt;enumeration value="WORK1"/>
     *                         &lt;enumeration value="COMML"/>
     *                         &lt;enumeration value="WORKL"/>
     *                         &lt;enumeration value="HOMEL"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="PhoneNum">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="+234(0)08052003607"/>
     *                         &lt;enumeration value=""/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="PhoneNumCityCode">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="0"/>
     *                         &lt;enumeration value=""/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="PhoneNumCountryCode">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="234"/>
     *                         &lt;enumeration value=""/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="PhoneNumLocalCode">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="08052003607"/>
     *                         &lt;enumeration value=""/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="PhoneOrEmail">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="PHONE"/>
     *                         &lt;enumeration value="EMAIL"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="PrefFlag">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="Y"/>
     *                         &lt;enumeration value="N"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="WorkExtNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RetailCustInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AdditionalName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BirthDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EmploymentDtls">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="EmployementStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="NameOfEmployer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="JobTitleCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="JobTitleDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="OccupationDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PeriodOfEmployment" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MaritalStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MaritalStatusDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NationalityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NationalityDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OptOutInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Race" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ResidingCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ResidingCountryDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RaceDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PsychographicInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custInqAddrInfo",
        "blackListNegativeListDtls",
        "corpCustInfo",
        "entityDocInfo",
        "genCustDtls",
        "custPhoneEmailInfo",
        "retailCustInfo",
        "psychographicInfo"
    })
    public static class CustInqRs implements Serializable{

        @XmlElement(name = "CustInqAddrInfo")
        protected List<CustInqResponse.CustInqRs.CustInqAddrInfo> custInqAddrInfo;
        @XmlElement(name = "BlackListNegativeListDtls", required = true)
        protected CustInqResponse.CustInqRs.BlackListNegativeListDtls blackListNegativeListDtls;
        @XmlElement(name = "CorpCustInfo", required = true)
        protected CustInqResponse.CustInqRs.CorpCustInfo corpCustInfo;
        @XmlElement(name = "EntityDocInfo", required = true)
        protected CustInqResponse.CustInqRs.EntityDocInfo entityDocInfo;
        @XmlElement(name = "GenCustDtls", required = true)
        protected CustInqResponse.CustInqRs.GenCustDtls genCustDtls;
        @XmlElement(name = "CustPhoneEmailInfo")
        protected List<CustInqResponse.CustInqRs.CustPhoneEmailInfo> custPhoneEmailInfo;
        @XmlElement(name = "RetailCustInfo", required = true)
        protected CustInqResponse.CustInqRs.RetailCustInfo retailCustInfo;
        @XmlElement(name = "PsychographicInfo", required = true)
        protected String psychographicInfo;

        /**
         * Gets the value of the custInqAddrInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the custInqAddrInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCustInqAddrInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CustInqResponse.CustInqRs.CustInqAddrInfo }
         * 
         * 
         */
        public List<CustInqResponse.CustInqRs.CustInqAddrInfo> getCustInqAddrInfo() {
            if (custInqAddrInfo == null) {
                custInqAddrInfo = new ArrayList<CustInqResponse.CustInqRs.CustInqAddrInfo>();
            }
            return this.custInqAddrInfo;
        }

        /**
         * Gets the value of the blackListNegativeListDtls property.
         * 
         * @return
         *     possible object is
         *     {@link CustInqResponse.CustInqRs.BlackListNegativeListDtls }
         *     
         */
        public CustInqResponse.CustInqRs.BlackListNegativeListDtls getBlackListNegativeListDtls() {
            return blackListNegativeListDtls;
        }

        /**
         * Sets the value of the blackListNegativeListDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustInqResponse.CustInqRs.BlackListNegativeListDtls }
         *     
         */
        public void setBlackListNegativeListDtls(CustInqResponse.CustInqRs.BlackListNegativeListDtls value) {
            this.blackListNegativeListDtls = value;
        }

        /**
         * Gets the value of the corpCustInfo property.
         * 
         * @return
         *     possible object is
         *     {@link CustInqResponse.CustInqRs.CorpCustInfo }
         *     
         */
        public CustInqResponse.CustInqRs.CorpCustInfo getCorpCustInfo() {
            return corpCustInfo;
        }

        /**
         * Sets the value of the corpCustInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustInqResponse.CustInqRs.CorpCustInfo }
         *     
         */
        public void setCorpCustInfo(CustInqResponse.CustInqRs.CorpCustInfo value) {
            this.corpCustInfo = value;
        }

        /**
         * Gets the value of the entityDocInfo property.
         * 
         * @return
         *     possible object is
         *     {@link CustInqResponse.CustInqRs.EntityDocInfo }
         *     
         */
        public CustInqResponse.CustInqRs.EntityDocInfo getEntityDocInfo() {
            return entityDocInfo;
        }

        /**
         * Sets the value of the entityDocInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustInqResponse.CustInqRs.EntityDocInfo }
         *     
         */
        public void setEntityDocInfo(CustInqResponse.CustInqRs.EntityDocInfo value) {
            this.entityDocInfo = value;
        }

        /**
         * Gets the value of the genCustDtls property.
         * 
         * @return
         *     possible object is
         *     {@link CustInqResponse.CustInqRs.GenCustDtls }
         *     
         */
        public CustInqResponse.CustInqRs.GenCustDtls getGenCustDtls() {
            return genCustDtls;
        }

        /**
         * Sets the value of the genCustDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustInqResponse.CustInqRs.GenCustDtls }
         *     
         */
        public void setGenCustDtls(CustInqResponse.CustInqRs.GenCustDtls value) {
            this.genCustDtls = value;
        }

        /**
         * Gets the value of the custPhoneEmailInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the custPhoneEmailInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCustPhoneEmailInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CustInqResponse.CustInqRs.CustPhoneEmailInfo }
         * 
         * 
         */
        public List<CustInqResponse.CustInqRs.CustPhoneEmailInfo> getCustPhoneEmailInfo() {
            if (custPhoneEmailInfo == null) {
                custPhoneEmailInfo = new ArrayList<CustInqResponse.CustInqRs.CustPhoneEmailInfo>();
            }
            return this.custPhoneEmailInfo;
        }

        /**
         * Gets the value of the retailCustInfo property.
         * 
         * @return
         *     possible object is
         *     {@link CustInqResponse.CustInqRs.RetailCustInfo }
         *     
         */
        public CustInqResponse.CustInqRs.RetailCustInfo getRetailCustInfo() {
            return retailCustInfo;
        }

        /**
         * Sets the value of the retailCustInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustInqResponse.CustInqRs.RetailCustInfo }
         *     
         */
        public void setRetailCustInfo(CustInqResponse.CustInqRs.RetailCustInfo value) {
            this.retailCustInfo = value;
        }

        /**
         * Gets the value of the psychographicInfo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPsychographicInfo() {
            return psychographicInfo;
        }

        /**
         * Sets the value of the psychographicInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPsychographicInfo(String value) {
            this.psychographicInfo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="IsBlacklisted" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BlacklistNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BlacklistReasonCode">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="IsNegated" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NegationNotes" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NegationReasonCode">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "isBlacklisted",
            "blacklistNotes",
            "blacklistReasonCode",
            "isNegated",
            "negationNotes",
            "negationReasonCode"
        })
        public static class BlackListNegativeListDtls implements Serializable {

            @XmlElement(name = "IsBlacklisted", required = true)
            protected String isBlacklisted;
            @XmlElement(name = "BlacklistNotes", required = true)
            protected String blacklistNotes;
            @XmlElement(name = "BlacklistReasonCode", required = true)
            protected CustInqResponse.CustInqRs.BlackListNegativeListDtls.BlacklistReasonCode blacklistReasonCode;
            @XmlElement(name = "IsNegated", required = true)
            protected String isNegated;
            @XmlElement(name = "NegationNotes", required = true)
            protected String negationNotes;
            @XmlElement(name = "NegationReasonCode", required = true)
            protected CustInqResponse.CustInqRs.BlackListNegativeListDtls.NegationReasonCode negationReasonCode;

            /**
             * Gets the value of the isBlacklisted property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsBlacklisted() {
                return isBlacklisted;
            }

            /**
             * Sets the value of the isBlacklisted property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsBlacklisted(String value) {
                this.isBlacklisted = value;
            }

            /**
             * Gets the value of the blacklistNotes property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBlacklistNotes() {
                return blacklistNotes;
            }

            /**
             * Sets the value of the blacklistNotes property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBlacklistNotes(String value) {
                this.blacklistNotes = value;
            }

            /**
             * Gets the value of the blacklistReasonCode property.
             * 
             * @return
             *     possible object is
             *     {@link CustInqResponse.CustInqRs.BlackListNegativeListDtls.BlacklistReasonCode }
             *     
             */
            public CustInqResponse.CustInqRs.BlackListNegativeListDtls.BlacklistReasonCode getBlacklistReasonCode() {
                return blacklistReasonCode;
            }

            /**
             * Sets the value of the blacklistReasonCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link CustInqResponse.CustInqRs.BlackListNegativeListDtls.BlacklistReasonCode }
             *     
             */
            public void setBlacklistReasonCode(CustInqResponse.CustInqRs.BlackListNegativeListDtls.BlacklistReasonCode value) {
                this.blacklistReasonCode = value;
            }

            /**
             * Gets the value of the isNegated property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsNegated() {
                return isNegated;
            }

            /**
             * Sets the value of the isNegated property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsNegated(String value) {
                this.isNegated = value;
            }

            /**
             * Gets the value of the negationNotes property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNegationNotes() {
                return negationNotes;
            }

            /**
             * Sets the value of the negationNotes property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNegationNotes(String value) {
                this.negationNotes = value;
            }

            /**
             * Gets the value of the negationReasonCode property.
             * 
             * @return
             *     possible object is
             *     {@link CustInqResponse.CustInqRs.BlackListNegativeListDtls.NegationReasonCode }
             *     
             */
            public CustInqResponse.CustInqRs.BlackListNegativeListDtls.NegationReasonCode getNegationReasonCode() {
                return negationReasonCode;
            }

            /**
             * Sets the value of the negationReasonCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link CustInqResponse.CustInqRs.BlackListNegativeListDtls.NegationReasonCode }
             *     
             */
            public void setNegationReasonCode(CustInqResponse.CustInqRs.BlackListNegativeListDtls.NegationReasonCode value) {
                this.negationReasonCode = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "reasonCode"
            })
            public static class BlacklistReasonCode implements Serializable {

                @XmlElement(name = "ReasonCode", required = true)
                protected String reasonCode;

                /**
                 * Gets the value of the reasonCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReasonCode() {
                    return reasonCode;
                }

                /**
                 * Sets the value of the reasonCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReasonCode(String value) {
                    this.reasonCode = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ReasonCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "reasonCode"
            })
            public static class NegationReasonCode implements Serializable{

                @XmlElement(name = "ReasonCode", required = true)
                protected String reasonCode;

                /**
                 * Gets the value of the reasonCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReasonCode() {
                    return reasonCode;
                }

                /**
                 * Sets the value of the reasonCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReasonCode(String value) {
                    this.reasonCode = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CitryOfIncorporation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LegalEntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrincipalNatureOfBusiness" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrincipalPlaceOfOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrimaryRelMgrId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "corpName",
            "citryOfIncorporation",
            "legalEntityType",
            "principalNatureOfBusiness",
            "principalPlaceOfOperation",
            "primaryRelMgrId"
        })
        public static class CorpCustInfo implements Serializable{

            @XmlElement(name = "CorpName", required = true)
            protected String corpName;
            @XmlElement(name = "CitryOfIncorporation", required = true)
            protected String citryOfIncorporation;
            @XmlElement(name = "LegalEntityType", required = true)
            protected String legalEntityType;
            @XmlElement(name = "PrincipalNatureOfBusiness", required = true)
            protected String principalNatureOfBusiness;
            @XmlElement(name = "PrincipalPlaceOfOperation", required = true)
            protected String principalPlaceOfOperation;
            @XmlElement(name = "PrimaryRelMgrId", required = true)
            protected String primaryRelMgrId;

            /**
             * Gets the value of the corpName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorpName() {
                return corpName;
            }

            /**
             * Sets the value of the corpName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorpName(String value) {
                this.corpName = value;
            }

            /**
             * Gets the value of the citryOfIncorporation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCitryOfIncorporation() {
                return citryOfIncorporation;
            }

            /**
             * Sets the value of the citryOfIncorporation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCitryOfIncorporation(String value) {
                this.citryOfIncorporation = value;
            }

            /**
             * Gets the value of the legalEntityType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLegalEntityType() {
                return legalEntityType;
            }

            /**
             * Sets the value of the legalEntityType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLegalEntityType(String value) {
                this.legalEntityType = value;
            }

            /**
             * Gets the value of the principalNatureOfBusiness property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrincipalNatureOfBusiness() {
                return principalNatureOfBusiness;
            }

            /**
             * Sets the value of the principalNatureOfBusiness property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrincipalNatureOfBusiness(String value) {
                this.principalNatureOfBusiness = value;
            }

            /**
             * Gets the value of the principalPlaceOfOperation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrincipalPlaceOfOperation() {
                return principalPlaceOfOperation;
            }

            /**
             * Sets the value of the principalPlaceOfOperation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrincipalPlaceOfOperation(String value) {
                this.principalPlaceOfOperation = value;
            }

            /**
             * Gets the value of the primaryRelMgrId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryRelMgrId() {
                return primaryRelMgrId;
            }

            /**
             * Sets the value of the primaryRelMgrId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryRelMgrId(String value) {
                this.primaryRelMgrId = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AddrEndDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AddrStartDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AddrType">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="Home"/>
         *               &lt;enumeration value="Mailing"/>
         *               &lt;enumeration value="Work"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="AddrLine1">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="NO ADDRESS IN FIN7"/>
         *               &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="AddrLine2">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value=""/>
         *               &lt;enumeration value="WARRI"/>
         *               &lt;enumeration value="."/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="AddrLine3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BuildingLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FreeTextAddr">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="."/>
         *               &lt;enumeration value="6 DIAMOND CLOSE OFF UZOMO ROAD OVWIAN"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="HouseNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LocalityName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PremiseName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StateCode">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="LG"/>
         *               &lt;enumeration value="DL"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="StreetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StreetNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Town" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "addrEndDt",
            "addrStartDt",
            "addrType",
            "addrLine1",
            "addrLine2",
            "addrLine3",
            "buildingLevel",
            "cityCode",
            "country",
            "countryCode",
            "domicile",
            "freeTextAddr",
            "houseNum",
            "localityName",
            "premiseName",
            "stateCode",
            "streetName",
            "streetNum",
            "suburb",
            "town",
            "postalCode"
        })
        public static class CustInqAddrInfo implements Serializable{

            @XmlElement(name = "AddrEndDt", required = true)
            protected String addrEndDt;
            @XmlElement(name = "AddrStartDt", required = true)
            protected String addrStartDt;
            @XmlElement(name = "AddrType", required = true)
            protected String addrType;
            @XmlElement(name = "AddrLine1", required = true)
            protected String addrLine1;
            @XmlElement(name = "AddrLine2", required = true)
            protected String addrLine2;
            @XmlElement(name = "AddrLine3", required = true)
            protected String addrLine3;
            @XmlElement(name = "BuildingLevel", required = true)
            protected String buildingLevel;
            @XmlElement(name = "CityCode", required = true)
            protected String cityCode;
            @XmlElement(name = "Country", required = true)
            protected String country;
            @XmlElement(name = "CountryCode", required = true)
            protected String countryCode;
            @XmlElement(name = "Domicile", required = true)
            protected String domicile;
            @XmlElement(name = "FreeTextAddr", required = true)
            protected String freeTextAddr;
            @XmlElement(name = "HouseNum", required = true)
            protected String houseNum;
            @XmlElement(name = "LocalityName", required = true)
            protected String localityName;
            @XmlElement(name = "PremiseName", required = true)
            protected String premiseName;
            @XmlElement(name = "StateCode", required = true)
            protected String stateCode;
            @XmlElement(name = "StreetName", required = true)
            protected String streetName;
            @XmlElement(name = "StreetNum", required = true)
            protected String streetNum;
            @XmlElement(name = "Suburb", required = true)
            protected String suburb;
            @XmlElement(name = "Town", required = true)
            protected String town;
            @XmlElement(name = "PostalCode", required = true)
            protected String postalCode;

            /**
             * Gets the value of the addrEndDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddrEndDt() {
                return addrEndDt;
            }

            /**
             * Sets the value of the addrEndDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddrEndDt(String value) {
                this.addrEndDt = value;
            }

            /**
             * Gets the value of the addrStartDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddrStartDt() {
                return addrStartDt;
            }

            /**
             * Sets the value of the addrStartDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddrStartDt(String value) {
                this.addrStartDt = value;
            }

            /**
             * Gets the value of the addrType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddrType() {
                return addrType;
            }

            /**
             * Sets the value of the addrType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddrType(String value) {
                this.addrType = value;
            }

            /**
             * Gets the value of the addrLine1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddrLine1() {
                return addrLine1;
            }

            /**
             * Sets the value of the addrLine1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddrLine1(String value) {
                this.addrLine1 = value;
            }

            /**
             * Gets the value of the addrLine2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddrLine2() {
                return addrLine2;
            }

            /**
             * Sets the value of the addrLine2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddrLine2(String value) {
                this.addrLine2 = value;
            }

            /**
             * Gets the value of the addrLine3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddrLine3() {
                return addrLine3;
            }

            /**
             * Sets the value of the addrLine3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddrLine3(String value) {
                this.addrLine3 = value;
            }

            /**
             * Gets the value of the buildingLevel property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBuildingLevel() {
                return buildingLevel;
            }

            /**
             * Sets the value of the buildingLevel property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBuildingLevel(String value) {
                this.buildingLevel = value;
            }

            /**
             * Gets the value of the cityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCityCode() {
                return cityCode;
            }

            /**
             * Sets the value of the cityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCityCode(String value) {
                this.cityCode = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountry() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountry(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the countryCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryCode() {
                return countryCode;
            }

            /**
             * Sets the value of the countryCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryCode(String value) {
                this.countryCode = value;
            }

            /**
             * Gets the value of the domicile property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDomicile() {
                return domicile;
            }

            /**
             * Sets the value of the domicile property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDomicile(String value) {
                this.domicile = value;
            }

            /**
             * Gets the value of the freeTextAddr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFreeTextAddr() {
                return freeTextAddr;
            }

            /**
             * Sets the value of the freeTextAddr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFreeTextAddr(String value) {
                this.freeTextAddr = value;
            }

            /**
             * Gets the value of the houseNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHouseNum() {
                return houseNum;
            }

            /**
             * Sets the value of the houseNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHouseNum(String value) {
                this.houseNum = value;
            }

            /**
             * Gets the value of the localityName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLocalityName() {
                return localityName;
            }

            /**
             * Sets the value of the localityName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLocalityName(String value) {
                this.localityName = value;
            }

            /**
             * Gets the value of the premiseName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPremiseName() {
                return premiseName;
            }

            /**
             * Sets the value of the premiseName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPremiseName(String value) {
                this.premiseName = value;
            }

            /**
             * Gets the value of the stateCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStateCode() {
                return stateCode;
            }

            /**
             * Sets the value of the stateCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStateCode(String value) {
                this.stateCode = value;
            }

            /**
             * Gets the value of the streetName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStreetName() {
                return streetName;
            }

            /**
             * Sets the value of the streetName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStreetName(String value) {
                this.streetName = value;
            }

            /**
             * Gets the value of the streetNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStreetNum() {
                return streetNum;
            }

            /**
             * Sets the value of the streetNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStreetNum(String value) {
                this.streetNum = value;
            }

            /**
             * Gets the value of the suburb property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSuburb() {
                return suburb;
            }

            /**
             * Sets the value of the suburb property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSuburb(String value) {
                this.suburb = value;
            }

            /**
             * Gets the value of the town property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTown() {
                return town;
            }

            /**
             * Sets the value of the town property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTown(String value) {
                this.town = value;
            }

            /**
             * Gets the value of the postalCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostalCode() {
                return postalCode;
            }

            /**
             * Sets the value of the postalCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostalCode(String value) {
                this.postalCode = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Email">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value=""/>
         *               &lt;enumeration value="noemail@fcmb.com"/>
         *               &lt;enumeration value="felixgrammar@yahoo.com"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="EmailPalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PhoneEmailType">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="COMM1"/>
         *               &lt;enumeration value="WORK1"/>
         *               &lt;enumeration value="COMML"/>
         *               &lt;enumeration value="WORKL"/>
         *               &lt;enumeration value="HOMEL"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="PhoneNum">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="+234(0)08052003607"/>
         *               &lt;enumeration value=""/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="PhoneNumCityCode">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="0"/>
         *               &lt;enumeration value=""/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="PhoneNumCountryCode">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="234"/>
         *               &lt;enumeration value=""/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="PhoneNumLocalCode">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="08052003607"/>
         *               &lt;enumeration value=""/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="PhoneOrEmail">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="PHONE"/>
         *               &lt;enumeration value="EMAIL"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="PrefFlag">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="Y"/>
         *               &lt;enumeration value="N"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="WorkExtNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "email",
            "emailPalm",
            "phoneEmailType",
            "phoneNum",
            "phoneNumCityCode",
            "phoneNumCountryCode",
            "phoneNumLocalCode",
            "phoneOrEmail",
            "prefFlag",
            "workExtNum"
        })
        public static class CustPhoneEmailInfo implements Serializable{

            @XmlElement(name = "Email", required = true)
            protected String email;
            @XmlElement(name = "EmailPalm", required = true)
            protected String emailPalm;
            @XmlElement(name = "PhoneEmailType", required = true)
            protected String phoneEmailType;
            @XmlElement(name = "PhoneNum", required = true)
            protected String phoneNum;
            @XmlElement(name = "PhoneNumCityCode", required = true)
            protected String phoneNumCityCode;
            @XmlElement(name = "PhoneNumCountryCode", required = true)
            protected String phoneNumCountryCode;
            @XmlElement(name = "PhoneNumLocalCode", required = true)
            protected String phoneNumLocalCode;
            @XmlElement(name = "PhoneOrEmail", required = true)
            protected String phoneOrEmail;
            @XmlElement(name = "PrefFlag", required = true)
            protected String prefFlag;
            @XmlElement(name = "WorkExtNum", required = true)
            protected String workExtNum;

            /**
             * Gets the value of the email property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEmail() {
                return email;
            }

            /**
             * Sets the value of the email property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEmail(String value) {
                this.email = value;
            }

            /**
             * Gets the value of the emailPalm property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEmailPalm() {
                return emailPalm;
            }

            /**
             * Sets the value of the emailPalm property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEmailPalm(String value) {
                this.emailPalm = value;
            }

            /**
             * Gets the value of the phoneEmailType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneEmailType() {
                return phoneEmailType;
            }

            /**
             * Sets the value of the phoneEmailType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneEmailType(String value) {
                this.phoneEmailType = value;
            }

            /**
             * Gets the value of the phoneNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneNum() {
                return phoneNum;
            }

            /**
             * Sets the value of the phoneNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneNum(String value) {
                this.phoneNum = value;
            }

            /**
             * Gets the value of the phoneNumCityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneNumCityCode() {
                return phoneNumCityCode;
            }

            /**
             * Sets the value of the phoneNumCityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneNumCityCode(String value) {
                this.phoneNumCityCode = value;
            }

            /**
             * Gets the value of the phoneNumCountryCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneNumCountryCode() {
                return phoneNumCountryCode;
            }

            /**
             * Sets the value of the phoneNumCountryCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneNumCountryCode(String value) {
                this.phoneNumCountryCode = value;
            }

            /**
             * Gets the value of the phoneNumLocalCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneNumLocalCode() {
                return phoneNumLocalCode;
            }

            /**
             * Sets the value of the phoneNumLocalCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneNumLocalCode(String value) {
                this.phoneNumLocalCode = value;
            }

            /**
             * Gets the value of the phoneOrEmail property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPhoneOrEmail() {
                return phoneOrEmail;
            }

            /**
             * Sets the value of the phoneOrEmail property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPhoneOrEmail(String value) {
                this.phoneOrEmail = value;
            }

            /**
             * Gets the value of the prefFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefFlag() {
                return prefFlag;
            }

            /**
             * Sets the value of the prefFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefFlag(String value) {
                this.prefFlag = value;
            }

            /**
             * Gets the value of the workExtNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getWorkExtNum() {
                return workExtNum;
            }

            /**
             * Sets the value of the workExtNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setWorkExtNum(String value) {
                this.workExtNum = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DocCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IssueDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsMandatory" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PlaceOfIssue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsScanReqd" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="UniqueId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "countryOfIssue",
            "docCode",
            "expDt",
            "issueDt",
            "rmks",
            "typeCode",
            "isMandatory",
            "placeOfIssue",
            "isScanReqd",
            "uniqueId"
        })
        public static class EntityDocInfo implements Serializable{

            @XmlElement(name = "CountryOfIssue", required = true)
            protected String countryOfIssue;
            @XmlElement(name = "DocCode", required = true)
            protected String docCode;
            @XmlElement(name = "ExpDt", required = true)
            protected String expDt;
            @XmlElement(name = "IssueDt", required = true)
            protected String issueDt;
            @XmlElement(name = "Rmks", required = true)
            protected String rmks;
            @XmlElement(name = "TypeCode", required = true)
            protected String typeCode;
            @XmlElement(name = "IsMandatory", required = true)
            protected String isMandatory;
            @XmlElement(name = "PlaceOfIssue", required = true)
            protected String placeOfIssue;
            @XmlElement(name = "IsScanReqd", required = true)
            protected String isScanReqd;
            @XmlElement(name = "UniqueId", required = true)
            protected String uniqueId;

            /**
             * Gets the value of the countryOfIssue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryOfIssue() {
                return countryOfIssue;
            }

            /**
             * Sets the value of the countryOfIssue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryOfIssue(String value) {
                this.countryOfIssue = value;
            }

            /**
             * Gets the value of the docCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocCode() {
                return docCode;
            }

            /**
             * Sets the value of the docCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocCode(String value) {
                this.docCode = value;
            }

            /**
             * Gets the value of the expDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpDt() {
                return expDt;
            }

            /**
             * Sets the value of the expDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpDt(String value) {
                this.expDt = value;
            }

            /**
             * Gets the value of the issueDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIssueDt() {
                return issueDt;
            }

            /**
             * Sets the value of the issueDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIssueDt(String value) {
                this.issueDt = value;
            }

            /**
             * Gets the value of the rmks property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRmks() {
                return rmks;
            }

            /**
             * Sets the value of the rmks property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRmks(String value) {
                this.rmks = value;
            }

            /**
             * Gets the value of the typeCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTypeCode() {
                return typeCode;
            }

            /**
             * Sets the value of the typeCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTypeCode(String value) {
                this.typeCode = value;
            }

            /**
             * Gets the value of the isMandatory property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsMandatory() {
                return isMandatory;
            }

            /**
             * Sets the value of the isMandatory property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsMandatory(String value) {
                this.isMandatory = value;
            }

            /**
             * Gets the value of the placeOfIssue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlaceOfIssue() {
                return placeOfIssue;
            }

            /**
             * Sets the value of the placeOfIssue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlaceOfIssue(String value) {
                this.placeOfIssue = value;
            }

            /**
             * Gets the value of the isScanReqd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsScanReqd() {
                return isScanReqd;
            }

            /**
             * Sets the value of the isScanReqd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsScanReqd(String value) {
                this.isScanReqd = value;
            }

            /**
             * Gets the value of the uniqueId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUniqueId() {
                return uniqueId;
            }

            /**
             * Sets the value of the uniqueId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUniqueId(String value) {
                this.uniqueId = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ChannelId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ChannelCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DSAId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EmployeeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GroupIdCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GSTFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NameInNativeLanguage" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrimaryDocType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrimaryRMId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RatingCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SalutationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SecondaryRMId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Segment" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SegmentLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SegmentNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SegmentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SICCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsStaff" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsSuspended" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TertiaryRMId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BranchId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "channelId",
            "channelCustId",
            "custId",
            "custTypeCode",
            "defaultAddrType",
            "dsaId",
            "employeeId",
            "groupIdCode",
            "gstFlag",
            "languageCode",
            "nameInNativeLanguage",
            "primaryDocType",
            "primaryRMId",
            "ratingCode",
            "salutationCode",
            "secondaryRMId",
            "sectorCode",
            "segment",
            "segmentLevel",
            "segmentNum",
            "segmentType",
            "shortName",
            "sicCode",
            "isStaff",
            "subSectorCode",
            "isSuspended",
            "tertiaryRMId",
            "branchId"
        })
        public static class GenCustDtls implements Serializable{

            @XmlElement(name = "ChannelId", required = true)
            protected String channelId;
            @XmlElement(name = "ChannelCustId", required = true)
            protected String channelCustId;
            @XmlElement(name = "CustId", required = true)
            protected String custId;
            @XmlElement(name = "CustTypeCode", required = true)
            protected String custTypeCode;
            @XmlElement(name = "DefaultAddrType", required = true)
            protected String defaultAddrType;
            @XmlElement(name = "DSAId", required = true)
            protected String dsaId;
            @XmlElement(name = "EmployeeId", required = true)
            protected String employeeId;
            @XmlElement(name = "GroupIdCode", required = true)
            protected String groupIdCode;
            @XmlElement(name = "GSTFlag", required = true)
            protected String gstFlag;
            @XmlElement(name = "LanguageCode", required = true)
            protected String languageCode;
            @XmlElement(name = "NameInNativeLanguage", required = true)
            protected String nameInNativeLanguage;
            @XmlElement(name = "PrimaryDocType", required = true)
            protected String primaryDocType;
            @XmlElement(name = "PrimaryRMId", required = true)
            protected String primaryRMId;
            @XmlElement(name = "RatingCode", required = true)
            protected String ratingCode;
            @XmlElement(name = "SalutationCode", required = true)
            protected String salutationCode;
            @XmlElement(name = "SecondaryRMId", required = true)
            protected String secondaryRMId;
            @XmlElement(name = "SectorCode", required = true)
            protected String sectorCode;
            @XmlElement(name = "Segment", required = true)
            protected String segment;
            @XmlElement(name = "SegmentLevel", required = true)
            protected String segmentLevel;
            @XmlElement(name = "SegmentNum", required = true)
            protected String segmentNum;
            @XmlElement(name = "SegmentType", required = true)
            protected String segmentType;
            @XmlElement(name = "ShortName", required = true)
            protected String shortName;
            @XmlElement(name = "SICCode", required = true)
            protected String sicCode;
            @XmlElement(name = "IsStaff", required = true)
            protected String isStaff;
            @XmlElement(name = "SubSectorCode", required = true)
            protected String subSectorCode;
            @XmlElement(name = "IsSuspended", required = true)
            protected String isSuspended;
            @XmlElement(name = "TertiaryRMId", required = true)
            protected String tertiaryRMId;
            @XmlElement(name = "BranchId", required = true)
            protected String branchId;

            /**
             * Gets the value of the channelId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChannelId() {
                return channelId;
            }

            /**
             * Sets the value of the channelId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChannelId(String value) {
                this.channelId = value;
            }

            /**
             * Gets the value of the channelCustId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChannelCustId() {
                return channelCustId;
            }

            /**
             * Sets the value of the channelCustId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChannelCustId(String value) {
                this.channelCustId = value;
            }

            /**
             * Gets the value of the custId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustId() {
                return custId;
            }

            /**
             * Sets the value of the custId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustId(String value) {
                this.custId = value;
            }

            /**
             * Gets the value of the custTypeCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustTypeCode() {
                return custTypeCode;
            }

            /**
             * Sets the value of the custTypeCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustTypeCode(String value) {
                this.custTypeCode = value;
            }

            /**
             * Gets the value of the defaultAddrType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDefaultAddrType() {
                return defaultAddrType;
            }

            /**
             * Sets the value of the defaultAddrType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDefaultAddrType(String value) {
                this.defaultAddrType = value;
            }

            /**
             * Gets the value of the dsaId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDSAId() {
                return dsaId;
            }

            /**
             * Sets the value of the dsaId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDSAId(String value) {
                this.dsaId = value;
            }

            /**
             * Gets the value of the employeeId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEmployeeId() {
                return employeeId;
            }

            /**
             * Sets the value of the employeeId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEmployeeId(String value) {
                this.employeeId = value;
            }

            /**
             * Gets the value of the groupIdCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupIdCode() {
                return groupIdCode;
            }

            /**
             * Sets the value of the groupIdCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupIdCode(String value) {
                this.groupIdCode = value;
            }

            /**
             * Gets the value of the gstFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGSTFlag() {
                return gstFlag;
            }

            /**
             * Sets the value of the gstFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGSTFlag(String value) {
                this.gstFlag = value;
            }

            /**
             * Gets the value of the languageCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLanguageCode() {
                return languageCode;
            }

            /**
             * Sets the value of the languageCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLanguageCode(String value) {
                this.languageCode = value;
            }

            /**
             * Gets the value of the nameInNativeLanguage property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNameInNativeLanguage() {
                return nameInNativeLanguage;
            }

            /**
             * Sets the value of the nameInNativeLanguage property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNameInNativeLanguage(String value) {
                this.nameInNativeLanguage = value;
            }

            /**
             * Gets the value of the primaryDocType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryDocType() {
                return primaryDocType;
            }

            /**
             * Sets the value of the primaryDocType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryDocType(String value) {
                this.primaryDocType = value;
            }

            /**
             * Gets the value of the primaryRMId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryRMId() {
                return primaryRMId;
            }

            /**
             * Sets the value of the primaryRMId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryRMId(String value) {
                this.primaryRMId = value;
            }

            /**
             * Gets the value of the ratingCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRatingCode() {
                return ratingCode;
            }

            /**
             * Sets the value of the ratingCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRatingCode(String value) {
                this.ratingCode = value;
            }

            /**
             * Gets the value of the salutationCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSalutationCode() {
                return salutationCode;
            }

            /**
             * Sets the value of the salutationCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSalutationCode(String value) {
                this.salutationCode = value;
            }

            /**
             * Gets the value of the secondaryRMId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSecondaryRMId() {
                return secondaryRMId;
            }

            /**
             * Sets the value of the secondaryRMId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSecondaryRMId(String value) {
                this.secondaryRMId = value;
            }

            /**
             * Gets the value of the sectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSectorCode() {
                return sectorCode;
            }

            /**
             * Sets the value of the sectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSectorCode(String value) {
                this.sectorCode = value;
            }

            /**
             * Gets the value of the segment property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSegment() {
                return segment;
            }

            /**
             * Sets the value of the segment property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSegment(String value) {
                this.segment = value;
            }

            /**
             * Gets the value of the segmentLevel property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSegmentLevel() {
                return segmentLevel;
            }

            /**
             * Sets the value of the segmentLevel property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSegmentLevel(String value) {
                this.segmentLevel = value;
            }

            /**
             * Gets the value of the segmentNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSegmentNum() {
                return segmentNum;
            }

            /**
             * Sets the value of the segmentNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSegmentNum(String value) {
                this.segmentNum = value;
            }

            /**
             * Gets the value of the segmentType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSegmentType() {
                return segmentType;
            }

            /**
             * Sets the value of the segmentType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSegmentType(String value) {
                this.segmentType = value;
            }

            /**
             * Gets the value of the shortName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortName() {
                return shortName;
            }

            /**
             * Sets the value of the shortName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortName(String value) {
                this.shortName = value;
            }

            /**
             * Gets the value of the sicCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSICCode() {
                return sicCode;
            }

            /**
             * Sets the value of the sicCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSICCode(String value) {
                this.sicCode = value;
            }

            /**
             * Gets the value of the isStaff property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsStaff() {
                return isStaff;
            }

            /**
             * Sets the value of the isStaff property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsStaff(String value) {
                this.isStaff = value;
            }

            /**
             * Gets the value of the subSectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubSectorCode() {
                return subSectorCode;
            }

            /**
             * Sets the value of the subSectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubSectorCode(String value) {
                this.subSectorCode = value;
            }

            /**
             * Gets the value of the isSuspended property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsSuspended() {
                return isSuspended;
            }

            /**
             * Sets the value of the isSuspended property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsSuspended(String value) {
                this.isSuspended = value;
            }

            /**
             * Gets the value of the tertiaryRMId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTertiaryRMId() {
                return tertiaryRMId;
            }

            /**
             * Sets the value of the tertiaryRMId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTertiaryRMId(String value) {
                this.tertiaryRMId = value;
            }

            /**
             * Gets the value of the branchId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBranchId() {
                return branchId;
            }

            /**
             * Sets the value of the branchId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBranchId(String value) {
                this.branchId = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AdditionalName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BirthDt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EmploymentDtls">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="EmployementStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="NameOfEmployer" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="JobTitleCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="JobTitleDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="OccupationDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PeriodOfEmployment" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MaritalStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MaritalStatusDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NationalityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NationalityDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OptOutInd" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Race" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ResidingCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ResidingCountryDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RaceDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "additionalName",
            "firstName",
            "lastName",
            "birthDt",
            "employmentDtls",
            "gender",
            "maritalStatusCode",
            "maritalStatusDesc",
            "nationalityCode",
            "nationalityDesc",
            "optOutInd",
            "race",
            "residingCountryCode",
            "residingCountryDesc",
            "raceDesc"
        })
        public static class RetailCustInfo implements Serializable{

            @XmlElement(name = "AdditionalName", required = true)
            protected String additionalName;
            @XmlElement(name = "FirstName", required = true)
            protected String firstName;
            @XmlElement(name = "LastName", required = true)
            protected String lastName;
            @XmlElement(name = "BirthDt", required = true)
            protected String birthDt;
            @XmlElement(name = "EmploymentDtls", required = true)
            protected CustInqResponse.CustInqRs.RetailCustInfo.EmploymentDtls employmentDtls;
            @XmlElement(name = "Gender", required = true)
            protected String gender;
            @XmlElement(name = "MaritalStatusCode", required = true)
            protected String maritalStatusCode;
            @XmlElement(name = "MaritalStatusDesc", required = true)
            protected String maritalStatusDesc;
            @XmlElement(name = "NationalityCode", required = true)
            protected String nationalityCode;
            @XmlElement(name = "NationalityDesc", required = true)
            protected String nationalityDesc;
            @XmlElement(name = "OptOutInd", required = true)
            protected String optOutInd;
            @XmlElement(name = "Race", required = true)
            protected String race;
            @XmlElement(name = "ResidingCountryCode", required = true)
            protected String residingCountryCode;
            @XmlElement(name = "ResidingCountryDesc", required = true)
            protected String residingCountryDesc;
            @XmlElement(name = "RaceDesc", required = true)
            protected String raceDesc;

            /**
             * Gets the value of the additionalName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAdditionalName() {
                return additionalName;
            }

            /**
             * Sets the value of the additionalName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAdditionalName(String value) {
                this.additionalName = value;
            }

            /**
             * Gets the value of the firstName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstName() {
                return firstName;
            }

            /**
             * Sets the value of the firstName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstName(String value) {
                this.firstName = value;
            }

            /**
             * Gets the value of the lastName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLastName() {
                return lastName;
            }

            /**
             * Sets the value of the lastName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLastName(String value) {
                this.lastName = value;
            }

            /**
             * Gets the value of the birthDt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBirthDt() {
                return birthDt;
            }

            /**
             * Sets the value of the birthDt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBirthDt(String value) {
                this.birthDt = value;
            }

            /**
             * Gets the value of the employmentDtls property.
             * 
             * @return
             *     possible object is
             *     {@link CustInqResponse.CustInqRs.RetailCustInfo.EmploymentDtls }
             *     
             */
            public CustInqResponse.CustInqRs.RetailCustInfo.EmploymentDtls getEmploymentDtls() {
                return employmentDtls;
            }

            /**
             * Sets the value of the employmentDtls property.
             * 
             * @param value
             *     allowed object is
             *     {@link CustInqResponse.CustInqRs.RetailCustInfo.EmploymentDtls }
             *     
             */
            public void setEmploymentDtls(CustInqResponse.CustInqRs.RetailCustInfo.EmploymentDtls value) {
                this.employmentDtls = value;
            }

            /**
             * Gets the value of the gender property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGender() {
                return gender;
            }

            /**
             * Sets the value of the gender property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGender(String value) {
                this.gender = value;
            }

            /**
             * Gets the value of the maritalStatusCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMaritalStatusCode() {
                return maritalStatusCode;
            }

            /**
             * Sets the value of the maritalStatusCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMaritalStatusCode(String value) {
                this.maritalStatusCode = value;
            }

            /**
             * Gets the value of the maritalStatusDesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMaritalStatusDesc() {
                return maritalStatusDesc;
            }

            /**
             * Sets the value of the maritalStatusDesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMaritalStatusDesc(String value) {
                this.maritalStatusDesc = value;
            }

            /**
             * Gets the value of the nationalityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNationalityCode() {
                return nationalityCode;
            }

            /**
             * Sets the value of the nationalityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNationalityCode(String value) {
                this.nationalityCode = value;
            }

            /**
             * Gets the value of the nationalityDesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNationalityDesc() {
                return nationalityDesc;
            }

            /**
             * Sets the value of the nationalityDesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNationalityDesc(String value) {
                this.nationalityDesc = value;
            }

            /**
             * Gets the value of the optOutInd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOptOutInd() {
                return optOutInd;
            }

            /**
             * Sets the value of the optOutInd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOptOutInd(String value) {
                this.optOutInd = value;
            }

            /**
             * Gets the value of the race property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRace() {
                return race;
            }

            /**
             * Sets the value of the race property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRace(String value) {
                this.race = value;
            }

            /**
             * Gets the value of the residingCountryCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResidingCountryCode() {
                return residingCountryCode;
            }

            /**
             * Sets the value of the residingCountryCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResidingCountryCode(String value) {
                this.residingCountryCode = value;
            }

            /**
             * Gets the value of the residingCountryDesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getResidingCountryDesc() {
                return residingCountryDesc;
            }

            /**
             * Sets the value of the residingCountryDesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setResidingCountryDesc(String value) {
                this.residingCountryDesc = value;
            }

            /**
             * Gets the value of the raceDesc property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRaceDesc() {
                return raceDesc;
            }

            /**
             * Sets the value of the raceDesc property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRaceDesc(String value) {
                this.raceDesc = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="EmployementStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="NameOfEmployer" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="JobTitleCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="JobTitleDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="OccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="OccupationDesc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PeriodOfEmployment" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "employementStatus",
                "nameOfEmployer",
                "jobTitleCode",
                "jobTitleDesc",
                "occupationCode",
                "occupationDesc",
                "periodOfEmployment"
            })
            public static class EmploymentDtls implements Serializable{

                @XmlElement(name = "EmployementStatus", required = true)
                protected String employementStatus;
                @XmlElement(name = "NameOfEmployer", required = true)
                protected String nameOfEmployer;
                @XmlElement(name = "JobTitleCode", required = true)
                protected String jobTitleCode;
                @XmlElement(name = "JobTitleDesc", required = true)
                protected String jobTitleDesc;
                @XmlElement(name = "OccupationCode", required = true)
                protected String occupationCode;
                @XmlElement(name = "OccupationDesc", required = true)
                protected String occupationDesc;
                @XmlElement(name = "PeriodOfEmployment", required = true)
                protected String periodOfEmployment;

                /**
                 * Gets the value of the employementStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEmployementStatus() {
                    return employementStatus;
                }

                /**
                 * Sets the value of the employementStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEmployementStatus(String value) {
                    this.employementStatus = value;
                }

                /**
                 * Gets the value of the nameOfEmployer property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNameOfEmployer() {
                    return nameOfEmployer;
                }

                /**
                 * Sets the value of the nameOfEmployer property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNameOfEmployer(String value) {
                    this.nameOfEmployer = value;
                }

                /**
                 * Gets the value of the jobTitleCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getJobTitleCode() {
                    return jobTitleCode;
                }

                /**
                 * Sets the value of the jobTitleCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setJobTitleCode(String value) {
                    this.jobTitleCode = value;
                }

                /**
                 * Gets the value of the jobTitleDesc property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getJobTitleDesc() {
                    return jobTitleDesc;
                }

                /**
                 * Sets the value of the jobTitleDesc property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setJobTitleDesc(String value) {
                    this.jobTitleDesc = value;
                }

                /**
                 * Gets the value of the occupationCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOccupationCode() {
                    return occupationCode;
                }

                /**
                 * Sets the value of the occupationCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOccupationCode(String value) {
                    this.occupationCode = value;
                }

                /**
                 * Gets the value of the occupationDesc property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOccupationDesc() {
                    return occupationDesc;
                }

                /**
                 * Sets the value of the occupationDesc property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOccupationDesc(String value) {
                    this.occupationDesc = value;
                }

                /**
                 * Gets the value of the periodOfEmployment property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPeriodOfEmployment() {
                    return periodOfEmployment;
                }

                /**
                 * Sets the value of the periodOfEmployment property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPeriodOfEmployment(String value) {
                    this.periodOfEmployment = value;
                }

            }

        }

    }

}

package com.longbridge.FI;




import com.longbridge.FI.FIGenAcctInq.AcctInqResponse;
import com.longbridge.FI.FiCorpAcctInq.CorpCustAcctInqResponse;
import com.longbridge.FI.FiCorpCustInq.GetCorporateCustomerDetailsResponse;
import com.longbridge.FI.FiGenCustInq.CustInqResponse;
import com.longbridge.FI.FiRetAcctInq.RetCustAcctInqResponse;
import com.longbridge.FI.FiRetCustInq.RetCustInqResponse;
import com.longbridge.entities.CustomerDetailsForInquiry;
import com.longbridge.entities.CustomerDetailsForUpdate;

import com.longbridge.repository.AccountsRepository;
import com.longbridge.repository.FidUpdateWorkFlowRepository;
import com.longbridge.security.FiUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by LB-PRJ-020 on 8/1/2017.
 */
@Service
public class FiTester {
    private CustomerDetailsForUpdate customerDetailsForUpdate;
    private CustomerDetailsForInquiry customerDetailsForInquiry;
    private static FidUpdateWorkFlowRepository fidUpdateWorkFlowRepository;
    private static AccountsRepository accountsRepository;

    public FiTester() {
    }




    @Autowired
    public void setFidUpdateWorkFlowRepository(FidUpdateWorkFlowRepository fidUpdateWorkFlowRepository) {
        this.fidUpdateWorkFlowRepository = fidUpdateWorkFlowRepository;
    }


    @Autowired
    public void setAccountsRepository(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    public   void main(String[] args) {
        //String acctType = accountsRepository.fetchCorpId("124124124");
        //CustomerDetailsForUpdate customerDetailsForUpdate = new FiTester().fetchforUpdate("3233443344","RETAIL");
        //new FiTester().updateCustomer(customerDetailsForUpdate,true,"3233443344");

    }

    public CustomerDetailsForUpdate fetchforUpdate(String foracid,String acctType,String custId){

        if(acctType.equals("RETAIL")){
            RetCustInqResponse cifInfo2 = new Helper().RetCustInq(custId);
            if(cifInfo2!=null){
                RetCustAcctInqResponse acctInfo2 =  new Helper().RetCustAcctInq(cifInfo2.getRetCustInqRs().getRetCustDtls().getCustId());
                if(acctInfo2!=null && acctInfo2.getRetCustAcctInqRs().getRetSaleDtls().getAcctId()!=null){
                    customerDetailsForUpdate = new CustomerDetailsForUpdate(cifInfo2,acctInfo2,"RETAIL");

                }
            }
        }else if(acctType.equals("CORPORATE")){
            GetCorporateCustomerDetailsResponse cifInfo = new Helper().CorpCustInq(custId);
            if(cifInfo!=null){
                CorpCustAcctInqResponse acctInfo =  new Helper().CorpCustAcctInq(cifInfo.getCorporateCustomerDetails().getCorpDet().getCorpKey());
                if(acctInfo!=null && acctInfo.getCorpCustAcctInqRs().getCorpCustDtls().getCorpName()!=null){
                    customerDetailsForUpdate = new CustomerDetailsForUpdate(cifInfo,acctInfo,"CORPORATE");

                }
            }
        }
        return customerDetailsForUpdate;
    }

    public CustomerDetailsForInquiry fetchforInquiry(String foracid,String custId ){
        AcctInqResponse acctInqResponse = new Helper().AcctInq(foracid);
        CustInqResponse custInqResponse = new Helper().CustInq(custId);
        customerDetailsForInquiry = new CustomerDetailsForInquiry(acctInqResponse,custInqResponse);

        return  customerDetailsForInquiry;
    }

    public String updateCustomer (CustomerDetailsForUpdate custDetails,boolean isRetail){
        String isVerified="F";
        String resp;
        if(!isRetail){
            System.out.println("Got into Update for Corporate");
            resp = new Helper().CorpCustUpdate(custDetails,"testing address");
            if(resp.contains("Succesfully")){
                isVerified = "S";
                resp = isVerified+"_"+resp;
            }
        }else{
            System.out.println("Got into Update for Reatil");
            resp = new Helper().RetCustUpdate(custDetails);
            if(resp.contains("Succesfully")){
                isVerified = "S";
                resp = isVerified+"_"+resp;
                System.out.println("result is "+resp);
            }
        }
        return resp;
    }
}

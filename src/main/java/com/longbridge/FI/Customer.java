package com.longbridge.FI;

/**
 * Created by LB-PRJ-020 on 8/1/2017.
 */
public class Customer {
    private String name;

    public String getCustId() {
        return CustId;
    }

    public void setCustId(String custId) {
        CustId = custId;
    }

    private String CustId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private int age;
}

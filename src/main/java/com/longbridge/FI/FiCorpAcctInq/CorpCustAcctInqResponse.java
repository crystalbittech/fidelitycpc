
package com.longbridge.FI.FiCorpAcctInq;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorpCustAcctInqRs">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CorpSaleDtls" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ProdCatId">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="3"/>
 *                                   &lt;enumeration value="1"/>
 *                                   &lt;enumeration value="6"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="ProductDtlsInfo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ProductFeature" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="AcctBranchCode">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="008"/>
 *                                   &lt;enumeration value="999"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="AcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ChannelCustId">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="OD206"/>
 *                                   &lt;enumeration value="OD246"/>
 *                                   &lt;enumeration value="OD247"/>
 *                                   &lt;enumeration value="OD249"/>
 *                                   &lt;enumeration value="OD208"/>
 *                                   &lt;enumeration value="OD273"/>
 *                                   &lt;enumeration value="SB108"/>
 *                                   &lt;enumeration value="OD274"/>
 *                                   &lt;enumeration value="LA489"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="ChannelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsMasterAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="IsMultiCurrAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ProductCategory">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="ODA"/>
 *                                   &lt;enumeration value="SBA"/>
 *                                   &lt;enumeration value="LAA"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="ProductCode">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="OD206"/>
 *                                   &lt;enumeration value="OD246"/>
 *                                   &lt;enumeration value="OD247"/>
 *                                   &lt;enumeration value="OD249"/>
 *                                   &lt;enumeration value="OD208"/>
 *                                   &lt;enumeration value="OD273"/>
 *                                   &lt;enumeration value="SB108"/>
 *                                   &lt;enumeration value="OD274"/>
 *                                   &lt;enumeration value="LA489"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="IsRevolvingODAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SaleChannelDtls">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="AcctClosureDt" minOccurs="0">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="2017-05-15T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-07-19T00:00:00.000"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="AcctCurrCode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="NGN"/>
 *                                             &lt;enumeration value="USD"/>
 *                                             &lt;enumeration value="EUR"/>
 *                                             &lt;enumeration value="GBP"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="AcctOpeningDt">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="2003-06-02T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-02-11T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-04-07T00:00:00.000"/>
 *                                             &lt;enumeration value="2017-01-11T00:00:00.000"/>
 *                                             &lt;enumeration value="2017-04-28T00:00:00.000"/>
 *                                             &lt;enumeration value="2017-05-15T00:00:00.000"/>
 *                                             &lt;enumeration value="2017-06-06T00:00:00.000"/>
 *                                             &lt;enumeration value="2016-02-19T00:00:00.000"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="AcctStatus">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="Open"/>
 *                                             &lt;enumeration value="Closed"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="AcctShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CrScoreStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="EligibilityResults" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="IntRepaymentAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ModeOfOperation">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="."/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="PosteIDSRStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="PrefRepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="RepaymentAcctBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="RepaymentAcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="RepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="SaleCust">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="TopUpFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CorpCustDtls">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="BusinessGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="BusinessType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ChargeLevelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CorpKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CountryOfIncorporation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CountryOfOrigin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CountryOfPrincipalOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustAssetClassification" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DSAId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="GroupId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="HealthCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NameOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="KeyContactPersonName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LegalEntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ParentOfEntity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ParentCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrimaryServiceCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrimaryRMUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PrincipalPlaceOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PriorityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RegionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RelationshipType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SecondaryRMUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TaxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TDSCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="TertiaryRMUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CorpCustAcctInq_CustomData" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "corpCustAcctInqRs",
    "corpCustAcctInqCustomData"
})
@XmlRootElement(name = "CorpCustAcctInqResponse")
public class CorpCustAcctInqResponse implements Serializable {

    @XmlElement(name = "CorpCustAcctInqRs", required = true)
    protected CorpCustAcctInqResponse.CorpCustAcctInqRs corpCustAcctInqRs;
    @XmlElement(name = "CorpCustAcctInq_CustomData", required = true)
    protected String corpCustAcctInqCustomData;

    /**
     * Gets the value of the corpCustAcctInqRs property.
     * 
     * @return
     *     possible object is
     *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs }
     *     
     */
    public CorpCustAcctInqResponse.CorpCustAcctInqRs getCorpCustAcctInqRs() {
        return corpCustAcctInqRs;
    }

    /**
     * Sets the value of the corpCustAcctInqRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs }
     *     
     */
    public void setCorpCustAcctInqRs(CorpCustAcctInqResponse.CorpCustAcctInqRs value) {
        this.corpCustAcctInqRs = value;
    }

    /**
     * Gets the value of the corpCustAcctInqCustomData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpCustAcctInqCustomData() {
        return corpCustAcctInqCustomData;
    }

    /**
     * Sets the value of the corpCustAcctInqCustomData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpCustAcctInqCustomData(String value) {
        this.corpCustAcctInqCustomData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CorpSaleDtls" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ProdCatId">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="3"/>
     *                         &lt;enumeration value="1"/>
     *                         &lt;enumeration value="6"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="ProductDtlsInfo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ProductFeature" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="AcctBranchCode">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="008"/>
     *                         &lt;enumeration value="999"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="AcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ChannelCustId">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="OD206"/>
     *                         &lt;enumeration value="OD246"/>
     *                         &lt;enumeration value="OD247"/>
     *                         &lt;enumeration value="OD249"/>
     *                         &lt;enumeration value="OD208"/>
     *                         &lt;enumeration value="OD273"/>
     *                         &lt;enumeration value="SB108"/>
     *                         &lt;enumeration value="OD274"/>
     *                         &lt;enumeration value="LA489"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="ChannelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsMasterAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="IsMultiCurrAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ProductCategory">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="ODA"/>
     *                         &lt;enumeration value="SBA"/>
     *                         &lt;enumeration value="LAA"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="ProductCode">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="OD206"/>
     *                         &lt;enumeration value="OD246"/>
     *                         &lt;enumeration value="OD247"/>
     *                         &lt;enumeration value="OD249"/>
     *                         &lt;enumeration value="OD208"/>
     *                         &lt;enumeration value="OD273"/>
     *                         &lt;enumeration value="SB108"/>
     *                         &lt;enumeration value="OD274"/>
     *                         &lt;enumeration value="LA489"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="IsRevolvingODAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SaleChannelDtls">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="AcctClosureDt" minOccurs="0">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="2017-05-15T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-07-19T00:00:00.000"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="AcctCurrCode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="NGN"/>
     *                                   &lt;enumeration value="USD"/>
     *                                   &lt;enumeration value="EUR"/>
     *                                   &lt;enumeration value="GBP"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="AcctOpeningDt">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="2003-06-02T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-02-11T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-04-07T00:00:00.000"/>
     *                                   &lt;enumeration value="2017-01-11T00:00:00.000"/>
     *                                   &lt;enumeration value="2017-04-28T00:00:00.000"/>
     *                                   &lt;enumeration value="2017-05-15T00:00:00.000"/>
     *                                   &lt;enumeration value="2017-06-06T00:00:00.000"/>
     *                                   &lt;enumeration value="2016-02-19T00:00:00.000"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="AcctStatus">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="Open"/>
     *                                   &lt;enumeration value="Closed"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="AcctShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CrScoreStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="EligibilityResults" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="IntRepaymentAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ModeOfOperation">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="."/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="PosteIDSRStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="PrefRepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RepaymentAcctBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RepaymentAcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="SaleCust">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="TopUpFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Service" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CorpCustDtls">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="BusinessGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BusinessType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ChargeLevelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CorpKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CountryOfIncorporation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CountryOfOrigin" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CountryOfPrincipalOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustAssetClassification" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DSAId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GroupId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HealthCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NameOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="KeyContactPersonName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LegalEntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ParentOfEntity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ParentCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrimaryServiceCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrimaryRMUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PrincipalPlaceOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PriorityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RegionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RelationshipType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SecondaryRMUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TaxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TDSCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="TertiaryRMUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "corpSaleDtls",
        "service",
        "status",
        "corpCustDtls"
    })
    public static class CorpCustAcctInqRs implements Serializable{

        @XmlElement(name = "CorpSaleDtls")
        protected List<CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls> corpSaleDtls;
        @XmlElement(name = "Service", required = true)
        protected String service;
        @XmlElement(name = "Status", required = true)
        protected String status;
        @XmlElement(name = "CorpCustDtls", required = true)
        protected CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpCustDtls corpCustDtls;

        /**
         * Gets the value of the corpSaleDtls property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the corpSaleDtls property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCorpSaleDtls().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls }
         * 
         * 
         */
        public List<CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls> getCorpSaleDtls() {
            if (corpSaleDtls == null) {
                corpSaleDtls = new ArrayList<CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls>();
            }
            return this.corpSaleDtls;
        }

        /**
         * Gets the value of the service property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getService() {
            return service;
        }

        /**
         * Sets the value of the service property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setService(String value) {
            this.service = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Gets the value of the corpCustDtls property.
         * 
         * @return
         *     possible object is
         *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpCustDtls }
         *     
         */
        public CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpCustDtls getCorpCustDtls() {
            return corpCustDtls;
        }

        /**
         * Sets the value of the corpCustDtls property.
         * 
         * @param value
         *     allowed object is
         *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpCustDtls }
         *     
         */
        public void setCorpCustDtls(CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpCustDtls value) {
            this.corpCustDtls = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="BusinessGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BusinessType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ChargeLevelCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CorpKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CorpName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CountryOfIncorporation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CountryOfOrigin" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CountryOfPrincipalOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CurrCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustAssetClassification" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DefaultAddrType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DSAId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GroupId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HealthCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NameOfIntroducer" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="KeyContactPersonName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LegalEntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ParentOfEntity" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ParentCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrimaryServiceCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrimaryRMUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PrincipalPlaceOperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PriorityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RegionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RelationshipType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SecondaryRMUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubSectorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TaxId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TDSCustId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="TertiaryRMUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "businessGroup",
            "businessType",
            "chargeLevelCode",
            "corpKey",
            "corpName",
            "countryOfIncorporation",
            "countryOfOrigin",
            "countryOfPrincipalOperation",
            "currCode",
            "custGroup",
            "custType",
            "custAssetClassification",
            "defaultAddrType",
            "dsaId",
            "entityType",
            "groupId",
            "healthCode",
            "nameOfIntroducer",
            "keyContactPersonName",
            "languageCode",
            "legalEntityType",
            "parentOfEntity",
            "parentCustId",
            "primaryServiceCenter",
            "primaryRMUserId",
            "principalPlaceOperation",
            "priority",
            "priorityCode",
            "region",
            "regionCode",
            "relationshipType",
            "rmks",
            "secondaryRMUserId",
            "sectorCode",
            "shortName",
            "subSectorCode",
            "taxId",
            "tdsCustId",
            "tertiaryRMUserId"
        })
        public static class CorpCustDtls implements Serializable{

            @XmlElement(name = "BusinessGroup", required = true)
            protected String businessGroup;
            @XmlElement(name = "BusinessType", required = true)
            protected String businessType;
            @XmlElement(name = "ChargeLevelCode", required = true)
            protected String chargeLevelCode;
            @XmlElement(name = "CorpKey", required = true)
            protected String corpKey;
            @XmlElement(name = "CorpName", required = true)
            protected String corpName;
            @XmlElement(name = "CountryOfIncorporation", required = true)
            protected String countryOfIncorporation;
            @XmlElement(name = "CountryOfOrigin", required = true)
            protected String countryOfOrigin;
            @XmlElement(name = "CountryOfPrincipalOperation", required = true)
            protected String countryOfPrincipalOperation;
            @XmlElement(name = "CurrCode", required = true)
            protected String currCode;
            @XmlElement(name = "CustGroup", required = true)
            protected String custGroup;
            @XmlElement(name = "CustType", required = true)
            protected String custType;
            @XmlElement(name = "CustAssetClassification", required = true)
            protected String custAssetClassification;
            @XmlElement(name = "DefaultAddrType", required = true)
            protected String defaultAddrType;
            @XmlElement(name = "DSAId", required = true)
            protected String dsaId;
            @XmlElement(name = "EntityType", required = true)
            protected String entityType;
            @XmlElement(name = "GroupId", required = true)
            protected String groupId;
            @XmlElement(name = "HealthCode", required = true)
            protected String healthCode;
            @XmlElement(name = "NameOfIntroducer", required = true)
            protected String nameOfIntroducer;
            @XmlElement(name = "KeyContactPersonName", required = true)
            protected String keyContactPersonName;
            @XmlElement(name = "LanguageCode", required = true)
            protected String languageCode;
            @XmlElement(name = "LegalEntityType", required = true)
            protected String legalEntityType;
            @XmlElement(name = "ParentOfEntity", required = true)
            protected String parentOfEntity;
            @XmlElement(name = "ParentCustId", required = true)
            protected String parentCustId;
            @XmlElement(name = "PrimaryServiceCenter", required = true)
            protected String primaryServiceCenter;
            @XmlElement(name = "PrimaryRMUserId", required = true)
            protected String primaryRMUserId;
            @XmlElement(name = "PrincipalPlaceOperation", required = true)
            protected String principalPlaceOperation;
            @XmlElement(name = "Priority", required = true)
            protected String priority;
            @XmlElement(name = "PriorityCode", required = true)
            protected String priorityCode;
            @XmlElement(name = "Region", required = true)
            protected String region;
            @XmlElement(name = "RegionCode", required = true)
            protected String regionCode;
            @XmlElement(name = "RelationshipType", required = true)
            protected String relationshipType;
            @XmlElement(name = "Rmks", required = true)
            protected String rmks;
            @XmlElement(name = "SecondaryRMUserId", required = true)
            protected String secondaryRMUserId;
            @XmlElement(name = "SectorCode", required = true)
            protected String sectorCode;
            @XmlElement(name = "ShortName", required = true)
            protected String shortName;
            @XmlElement(name = "SubSectorCode", required = true)
            protected String subSectorCode;
            @XmlElement(name = "TaxId", required = true)
            protected String taxId;
            @XmlElement(name = "TDSCustId", required = true)
            protected String tdsCustId;
            @XmlElement(name = "TertiaryRMUserId", required = true)
            protected String tertiaryRMUserId;

            /**
             * Gets the value of the businessGroup property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBusinessGroup() {
                return businessGroup;
            }

            /**
             * Sets the value of the businessGroup property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBusinessGroup(String value) {
                this.businessGroup = value;
            }

            /**
             * Gets the value of the businessType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBusinessType() {
                return businessType;
            }

            /**
             * Sets the value of the businessType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBusinessType(String value) {
                this.businessType = value;
            }

            /**
             * Gets the value of the chargeLevelCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChargeLevelCode() {
                return chargeLevelCode;
            }

            /**
             * Sets the value of the chargeLevelCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChargeLevelCode(String value) {
                this.chargeLevelCode = value;
            }

            /**
             * Gets the value of the corpKey property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorpKey() {
                return corpKey;
            }

            /**
             * Sets the value of the corpKey property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorpKey(String value) {
                this.corpKey = value;
            }

            /**
             * Gets the value of the corpName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorpName() {
                return corpName;
            }

            /**
             * Sets the value of the corpName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorpName(String value) {
                this.corpName = value;
            }

            /**
             * Gets the value of the countryOfIncorporation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryOfIncorporation() {
                return countryOfIncorporation;
            }

            /**
             * Sets the value of the countryOfIncorporation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryOfIncorporation(String value) {
                this.countryOfIncorporation = value;
            }

            /**
             * Gets the value of the countryOfOrigin property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryOfOrigin() {
                return countryOfOrigin;
            }

            /**
             * Sets the value of the countryOfOrigin property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryOfOrigin(String value) {
                this.countryOfOrigin = value;
            }

            /**
             * Gets the value of the countryOfPrincipalOperation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryOfPrincipalOperation() {
                return countryOfPrincipalOperation;
            }

            /**
             * Sets the value of the countryOfPrincipalOperation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryOfPrincipalOperation(String value) {
                this.countryOfPrincipalOperation = value;
            }

            /**
             * Gets the value of the currCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCurrCode() {
                return currCode;
            }

            /**
             * Sets the value of the currCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCurrCode(String value) {
                this.currCode = value;
            }

            /**
             * Gets the value of the custGroup property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustGroup() {
                return custGroup;
            }

            /**
             * Sets the value of the custGroup property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustGroup(String value) {
                this.custGroup = value;
            }

            /**
             * Gets the value of the custType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustType() {
                return custType;
            }

            /**
             * Sets the value of the custType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustType(String value) {
                this.custType = value;
            }

            /**
             * Gets the value of the custAssetClassification property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustAssetClassification() {
                return custAssetClassification;
            }

            /**
             * Sets the value of the custAssetClassification property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustAssetClassification(String value) {
                this.custAssetClassification = value;
            }

            /**
             * Gets the value of the defaultAddrType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDefaultAddrType() {
                return defaultAddrType;
            }

            /**
             * Sets the value of the defaultAddrType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDefaultAddrType(String value) {
                this.defaultAddrType = value;
            }

            /**
             * Gets the value of the dsaId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDSAId() {
                return dsaId;
            }

            /**
             * Sets the value of the dsaId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDSAId(String value) {
                this.dsaId = value;
            }

            /**
             * Gets the value of the entityType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityType() {
                return entityType;
            }

            /**
             * Sets the value of the entityType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityType(String value) {
                this.entityType = value;
            }

            /**
             * Gets the value of the groupId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupId() {
                return groupId;
            }

            /**
             * Sets the value of the groupId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupId(String value) {
                this.groupId = value;
            }

            /**
             * Gets the value of the healthCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHealthCode() {
                return healthCode;
            }

            /**
             * Sets the value of the healthCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHealthCode(String value) {
                this.healthCode = value;
            }

            /**
             * Gets the value of the nameOfIntroducer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNameOfIntroducer() {
                return nameOfIntroducer;
            }

            /**
             * Sets the value of the nameOfIntroducer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNameOfIntroducer(String value) {
                this.nameOfIntroducer = value;
            }

            /**
             * Gets the value of the keyContactPersonName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getKeyContactPersonName() {
                return keyContactPersonName;
            }

            /**
             * Sets the value of the keyContactPersonName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setKeyContactPersonName(String value) {
                this.keyContactPersonName = value;
            }

            /**
             * Gets the value of the languageCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLanguageCode() {
                return languageCode;
            }

            /**
             * Sets the value of the languageCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLanguageCode(String value) {
                this.languageCode = value;
            }

            /**
             * Gets the value of the legalEntityType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLegalEntityType() {
                return legalEntityType;
            }

            /**
             * Sets the value of the legalEntityType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLegalEntityType(String value) {
                this.legalEntityType = value;
            }

            /**
             * Gets the value of the parentOfEntity property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParentOfEntity() {
                return parentOfEntity;
            }

            /**
             * Sets the value of the parentOfEntity property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParentOfEntity(String value) {
                this.parentOfEntity = value;
            }

            /**
             * Gets the value of the parentCustId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParentCustId() {
                return parentCustId;
            }

            /**
             * Sets the value of the parentCustId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParentCustId(String value) {
                this.parentCustId = value;
            }

            /**
             * Gets the value of the primaryServiceCenter property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryServiceCenter() {
                return primaryServiceCenter;
            }

            /**
             * Sets the value of the primaryServiceCenter property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryServiceCenter(String value) {
                this.primaryServiceCenter = value;
            }

            /**
             * Gets the value of the primaryRMUserId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryRMUserId() {
                return primaryRMUserId;
            }

            /**
             * Sets the value of the primaryRMUserId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryRMUserId(String value) {
                this.primaryRMUserId = value;
            }

            /**
             * Gets the value of the principalPlaceOperation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrincipalPlaceOperation() {
                return principalPlaceOperation;
            }

            /**
             * Sets the value of the principalPlaceOperation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrincipalPlaceOperation(String value) {
                this.principalPlaceOperation = value;
            }

            /**
             * Gets the value of the priority property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPriority() {
                return priority;
            }

            /**
             * Sets the value of the priority property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPriority(String value) {
                this.priority = value;
            }

            /**
             * Gets the value of the priorityCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPriorityCode() {
                return priorityCode;
            }

            /**
             * Sets the value of the priorityCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPriorityCode(String value) {
                this.priorityCode = value;
            }

            /**
             * Gets the value of the region property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegion() {
                return region;
            }

            /**
             * Sets the value of the region property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegion(String value) {
                this.region = value;
            }

            /**
             * Gets the value of the regionCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegionCode() {
                return regionCode;
            }

            /**
             * Sets the value of the regionCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegionCode(String value) {
                this.regionCode = value;
            }

            /**
             * Gets the value of the relationshipType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipType() {
                return relationshipType;
            }

            /**
             * Sets the value of the relationshipType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipType(String value) {
                this.relationshipType = value;
            }

            /**
             * Gets the value of the rmks property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRmks() {
                return rmks;
            }

            /**
             * Sets the value of the rmks property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRmks(String value) {
                this.rmks = value;
            }

            /**
             * Gets the value of the secondaryRMUserId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSecondaryRMUserId() {
                return secondaryRMUserId;
            }

            /**
             * Sets the value of the secondaryRMUserId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSecondaryRMUserId(String value) {
                this.secondaryRMUserId = value;
            }

            /**
             * Gets the value of the sectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSectorCode() {
                return sectorCode;
            }

            /**
             * Sets the value of the sectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSectorCode(String value) {
                this.sectorCode = value;
            }

            /**
             * Gets the value of the shortName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortName() {
                return shortName;
            }

            /**
             * Sets the value of the shortName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortName(String value) {
                this.shortName = value;
            }

            /**
             * Gets the value of the subSectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubSectorCode() {
                return subSectorCode;
            }

            /**
             * Sets the value of the subSectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubSectorCode(String value) {
                this.subSectorCode = value;
            }

            /**
             * Gets the value of the taxId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxId() {
                return taxId;
            }

            /**
             * Sets the value of the taxId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxId(String value) {
                this.taxId = value;
            }

            /**
             * Gets the value of the tdsCustId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTDSCustId() {
                return tdsCustId;
            }

            /**
             * Sets the value of the tdsCustId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTDSCustId(String value) {
                this.tdsCustId = value;
            }

            /**
             * Gets the value of the tertiaryRMUserId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTertiaryRMUserId() {
                return tertiaryRMUserId;
            }

            /**
             * Sets the value of the tertiaryRMUserId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTertiaryRMUserId(String value) {
                this.tertiaryRMUserId = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ProdCatId">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="3"/>
         *               &lt;enumeration value="1"/>
         *               &lt;enumeration value="6"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="ProductDtlsInfo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ProductFeature" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="AcctBranchCode">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="008"/>
         *               &lt;enumeration value="999"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="AcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ChannelCustId">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="OD206"/>
         *               &lt;enumeration value="OD246"/>
         *               &lt;enumeration value="OD247"/>
         *               &lt;enumeration value="OD249"/>
         *               &lt;enumeration value="OD208"/>
         *               &lt;enumeration value="OD273"/>
         *               &lt;enumeration value="SB108"/>
         *               &lt;enumeration value="OD274"/>
         *               &lt;enumeration value="LA489"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="ChannelName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsMasterAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="IsMultiCurrAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ProductCategory">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="ODA"/>
         *               &lt;enumeration value="SBA"/>
         *               &lt;enumeration value="LAA"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="ProductCode">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="OD206"/>
         *               &lt;enumeration value="OD246"/>
         *               &lt;enumeration value="OD247"/>
         *               &lt;enumeration value="OD249"/>
         *               &lt;enumeration value="OD208"/>
         *               &lt;enumeration value="OD273"/>
         *               &lt;enumeration value="SB108"/>
         *               &lt;enumeration value="OD274"/>
         *               &lt;enumeration value="LA489"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="IsRevolvingODAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SaleChannelDtls">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="AcctClosureDt" minOccurs="0">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="2017-05-15T00:00:00.000"/>
         *                         &lt;enumeration value="2016-07-19T00:00:00.000"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="AcctCurrCode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="NGN"/>
         *                         &lt;enumeration value="USD"/>
         *                         &lt;enumeration value="EUR"/>
         *                         &lt;enumeration value="GBP"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="AcctOpeningDt">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="2003-06-02T00:00:00.000"/>
         *                         &lt;enumeration value="2016-02-11T00:00:00.000"/>
         *                         &lt;enumeration value="2016-04-07T00:00:00.000"/>
         *                         &lt;enumeration value="2017-01-11T00:00:00.000"/>
         *                         &lt;enumeration value="2017-04-28T00:00:00.000"/>
         *                         &lt;enumeration value="2017-05-15T00:00:00.000"/>
         *                         &lt;enumeration value="2017-06-06T00:00:00.000"/>
         *                         &lt;enumeration value="2016-02-19T00:00:00.000"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="AcctStatus">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="Open"/>
         *                         &lt;enumeration value="Closed"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="AcctShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CrScoreStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="EligibilityResults" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="IntRepaymentAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ModeOfOperation">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="."/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="PosteIDSRStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="PrefRepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RepaymentAcctBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RepaymentAcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="SaleCust">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="TopUpFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "prodCatId",
            "productDtlsInfo",
            "acctBranchCode",
            "acctId",
            "channelCustId",
            "channelName",
            "isMasterAcct",
            "isMultiCurrAcct",
            "productCategory",
            "productCode",
            "isRevolvingODAcct",
            "saleChannelDtls",
            "saleCust",
            "topUpFlag"
        })
        public static class CorpSaleDtls implements Serializable{

            @XmlElement(name = "ProdCatId", required = true)
            protected String prodCatId;
            @XmlElement(name = "ProductDtlsInfo", required = true)
            protected CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.ProductDtlsInfo productDtlsInfo;
            @XmlElement(name = "AcctBranchCode", required = true)
            protected String acctBranchCode;
            @XmlElement(name = "AcctId", required = true)
            protected String acctId;
            @XmlElement(name = "ChannelCustId", required = true)
            protected String channelCustId;
            @XmlElement(name = "ChannelName", required = true)
            protected String channelName;
            @XmlElement(name = "IsMasterAcct", required = true)
            protected String isMasterAcct;
            @XmlElement(name = "IsMultiCurrAcct", required = true)
            protected String isMultiCurrAcct;
            @XmlElement(name = "ProductCategory", required = true)
            protected String productCategory;
            @XmlElement(name = "ProductCode", required = true)
            protected String productCode;
            @XmlElement(name = "IsRevolvingODAcct", required = true)
            protected String isRevolvingODAcct;
            @XmlElement(name = "SaleChannelDtls", required = true)
            protected CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleChannelDtls saleChannelDtls;
            @XmlElement(name = "SaleCust", required = true)
            protected CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleCust saleCust;
            @XmlElement(name = "TopUpFlag", required = true)
            protected String topUpFlag;

            /**
             * Gets the value of the prodCatId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProdCatId() {
                return prodCatId;
            }

            /**
             * Sets the value of the prodCatId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProdCatId(String value) {
                this.prodCatId = value;
            }

            /**
             * Gets the value of the productDtlsInfo property.
             * 
             * @return
             *     possible object is
             *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.ProductDtlsInfo }
             *     
             */
            public CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.ProductDtlsInfo getProductDtlsInfo() {
                return productDtlsInfo;
            }

            /**
             * Sets the value of the productDtlsInfo property.
             * 
             * @param value
             *     allowed object is
             *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.ProductDtlsInfo }
             *     
             */
            public void setProductDtlsInfo(CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.ProductDtlsInfo value) {
                this.productDtlsInfo = value;
            }

            /**
             * Gets the value of the acctBranchCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctBranchCode() {
                return acctBranchCode;
            }

            /**
             * Sets the value of the acctBranchCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctBranchCode(String value) {
                this.acctBranchCode = value;
            }

            /**
             * Gets the value of the acctId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctId() {
                return acctId;
            }

            /**
             * Sets the value of the acctId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctId(String value) {
                this.acctId = value;
            }

            /**
             * Gets the value of the channelCustId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChannelCustId() {
                return channelCustId;
            }

            /**
             * Sets the value of the channelCustId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChannelCustId(String value) {
                this.channelCustId = value;
            }

            /**
             * Gets the value of the channelName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChannelName() {
                return channelName;
            }

            /**
             * Sets the value of the channelName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChannelName(String value) {
                this.channelName = value;
            }

            /**
             * Gets the value of the isMasterAcct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsMasterAcct() {
                return isMasterAcct;
            }

            /**
             * Sets the value of the isMasterAcct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsMasterAcct(String value) {
                this.isMasterAcct = value;
            }

            /**
             * Gets the value of the isMultiCurrAcct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsMultiCurrAcct() {
                return isMultiCurrAcct;
            }

            /**
             * Sets the value of the isMultiCurrAcct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsMultiCurrAcct(String value) {
                this.isMultiCurrAcct = value;
            }

            /**
             * Gets the value of the productCategory property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProductCategory() {
                return productCategory;
            }

            /**
             * Sets the value of the productCategory property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProductCategory(String value) {
                this.productCategory = value;
            }

            /**
             * Gets the value of the productCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProductCode() {
                return productCode;
            }

            /**
             * Sets the value of the productCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProductCode(String value) {
                this.productCode = value;
            }

            /**
             * Gets the value of the isRevolvingODAcct property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsRevolvingODAcct() {
                return isRevolvingODAcct;
            }

            /**
             * Sets the value of the isRevolvingODAcct property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsRevolvingODAcct(String value) {
                this.isRevolvingODAcct = value;
            }

            /**
             * Gets the value of the saleChannelDtls property.
             * 
             * @return
             *     possible object is
             *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleChannelDtls }
             *     
             */
            public CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleChannelDtls getSaleChannelDtls() {
                return saleChannelDtls;
            }

            /**
             * Sets the value of the saleChannelDtls property.
             * 
             * @param value
             *     allowed object is
             *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleChannelDtls }
             *     
             */
            public void setSaleChannelDtls(CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleChannelDtls value) {
                this.saleChannelDtls = value;
            }

            /**
             * Gets the value of the saleCust property.
             * 
             * @return
             *     possible object is
             *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleCust }
             *     
             */
            public CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleCust getSaleCust() {
                return saleCust;
            }

            /**
             * Sets the value of the saleCust property.
             * 
             * @param value
             *     allowed object is
             *     {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleCust }
             *     
             */
            public void setSaleCust(CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleCust value) {
                this.saleCust = value;
            }

            /**
             * Gets the value of the topUpFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTopUpFlag() {
                return topUpFlag;
            }

            /**
             * Sets the value of the topUpFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTopUpFlag(String value) {
                this.topUpFlag = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ProductFeature" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "productFeature"
            })
            public static class ProductDtlsInfo implements Serializable{

                @XmlElement(name = "ProductFeature", required = true)
                protected String productFeature;

                /**
                 * Gets the value of the productFeature property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProductFeature() {
                    return productFeature;
                }

                /**
                 * Sets the value of the productFeature property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProductFeature(String value) {
                    this.productFeature = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="AcctClosureDt" minOccurs="0">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="2017-05-15T00:00:00.000"/>
             *               &lt;enumeration value="2016-07-19T00:00:00.000"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="AcctCurrCode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="NGN"/>
             *               &lt;enumeration value="USD"/>
             *               &lt;enumeration value="EUR"/>
             *               &lt;enumeration value="GBP"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="AcctOpeningDt">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="2003-06-02T00:00:00.000"/>
             *               &lt;enumeration value="2016-02-11T00:00:00.000"/>
             *               &lt;enumeration value="2016-04-07T00:00:00.000"/>
             *               &lt;enumeration value="2017-01-11T00:00:00.000"/>
             *               &lt;enumeration value="2017-04-28T00:00:00.000"/>
             *               &lt;enumeration value="2017-05-15T00:00:00.000"/>
             *               &lt;enumeration value="2017-06-06T00:00:00.000"/>
             *               &lt;enumeration value="2016-02-19T00:00:00.000"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="AcctStatus">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="Open"/>
             *               &lt;enumeration value="Closed"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="AcctShortName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CrScoreStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="EligibilityResults" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="IntRepaymentAcct" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ModeOfOperation">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="."/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="PosteIDSRStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="PrefRepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RepaymentAcctBank" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RepaymentAcctId" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RepaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "acctClosureDt",
                "acctCurrCode",
                "acctOpeningDt",
                "acctStatus",
                "acctShortName",
                "crScoreStatus",
                "eligibilityResults",
                "intRepaymentAcct",
                "modeOfOperation",
                "posteIDSRStatus",
                "prefRepaymentMode",
                "repaymentAcctBank",
                "repaymentAcctId",
                "repaymentMode"
            })
            public static class SaleChannelDtls implements Serializable{

                @XmlElement(name = "AcctClosureDt")
                protected String acctClosureDt;
                @XmlElement(name = "AcctCurrCode", required = true)
                protected String acctCurrCode;
                @XmlElement(name = "AcctOpeningDt", required = true)
                protected String acctOpeningDt;
                @XmlElement(name = "AcctStatus", required = true)
                protected String acctStatus;
                @XmlElement(name = "AcctShortName", required = true)
                protected String acctShortName;
                @XmlElement(name = "CrScoreStatus", required = true)
                protected String crScoreStatus;
                @XmlElement(name = "EligibilityResults", required = true)
                protected String eligibilityResults;
                @XmlElement(name = "IntRepaymentAcct", required = true)
                protected String intRepaymentAcct;
                @XmlElement(name = "ModeOfOperation", required = true)
                protected String modeOfOperation;
                @XmlElement(name = "PosteIDSRStatus", required = true)
                protected String posteIDSRStatus;
                @XmlElement(name = "PrefRepaymentMode", required = true)
                protected String prefRepaymentMode;
                @XmlElement(name = "RepaymentAcctBank", required = true)
                protected String repaymentAcctBank;
                @XmlElement(name = "RepaymentAcctId", required = true)
                protected String repaymentAcctId;
                @XmlElement(name = "RepaymentMode", required = true)
                protected String repaymentMode;

                /**
                 * Gets the value of the acctClosureDt property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctClosureDt() {
                    return acctClosureDt;
                }

                /**
                 * Sets the value of the acctClosureDt property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctClosureDt(String value) {
                    this.acctClosureDt = value;
                }

                /**
                 * Gets the value of the acctCurrCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctCurrCode() {
                    return acctCurrCode;
                }

                /**
                 * Sets the value of the acctCurrCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctCurrCode(String value) {
                    this.acctCurrCode = value;
                }

                /**
                 * Gets the value of the acctOpeningDt property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctOpeningDt() {
                    return acctOpeningDt;
                }

                /**
                 * Sets the value of the acctOpeningDt property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctOpeningDt(String value) {
                    this.acctOpeningDt = value;
                }

                /**
                 * Gets the value of the acctStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctStatus() {
                    return acctStatus;
                }

                /**
                 * Sets the value of the acctStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctStatus(String value) {
                    this.acctStatus = value;
                }

                /**
                 * Gets the value of the acctShortName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctShortName() {
                    return acctShortName;
                }

                /**
                 * Sets the value of the acctShortName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctShortName(String value) {
                    this.acctShortName = value;
                }

                /**
                 * Gets the value of the crScoreStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCrScoreStatus() {
                    return crScoreStatus;
                }

                /**
                 * Sets the value of the crScoreStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCrScoreStatus(String value) {
                    this.crScoreStatus = value;
                }

                /**
                 * Gets the value of the eligibilityResults property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEligibilityResults() {
                    return eligibilityResults;
                }

                /**
                 * Sets the value of the eligibilityResults property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEligibilityResults(String value) {
                    this.eligibilityResults = value;
                }

                /**
                 * Gets the value of the intRepaymentAcct property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIntRepaymentAcct() {
                    return intRepaymentAcct;
                }

                /**
                 * Sets the value of the intRepaymentAcct property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIntRepaymentAcct(String value) {
                    this.intRepaymentAcct = value;
                }

                /**
                 * Gets the value of the modeOfOperation property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getModeOfOperation() {
                    return modeOfOperation;
                }

                /**
                 * Sets the value of the modeOfOperation property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setModeOfOperation(String value) {
                    this.modeOfOperation = value;
                }

                /**
                 * Gets the value of the posteIDSRStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPosteIDSRStatus() {
                    return posteIDSRStatus;
                }

                /**
                 * Sets the value of the posteIDSRStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPosteIDSRStatus(String value) {
                    this.posteIDSRStatus = value;
                }

                /**
                 * Gets the value of the prefRepaymentMode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPrefRepaymentMode() {
                    return prefRepaymentMode;
                }

                /**
                 * Sets the value of the prefRepaymentMode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPrefRepaymentMode(String value) {
                    this.prefRepaymentMode = value;
                }

                /**
                 * Gets the value of the repaymentAcctBank property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRepaymentAcctBank() {
                    return repaymentAcctBank;
                }

                /**
                 * Sets the value of the repaymentAcctBank property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRepaymentAcctBank(String value) {
                    this.repaymentAcctBank = value;
                }

                /**
                 * Gets the value of the repaymentAcctId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRepaymentAcctId() {
                    return repaymentAcctId;
                }

                /**
                 * Sets the value of the repaymentAcctId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRepaymentAcctId(String value) {
                    this.repaymentAcctId = value;
                }

                /**
                 * Gets the value of the repaymentMode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRepaymentMode() {
                    return repaymentMode;
                }

                /**
                 * Sets the value of the repaymentMode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRepaymentMode(String value) {
                    this.repaymentMode = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Role" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "role"
            })
            public static class SaleCust implements Serializable{

                @XmlElement(name = "Role", required = true)
                protected String role;

                /**
                 * Gets the value of the role property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRole() {
                    return role;
                }

                /**
                 * Sets the value of the role property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRole(String value) {
                    this.role = value;
                }

            }

        }

    }

}

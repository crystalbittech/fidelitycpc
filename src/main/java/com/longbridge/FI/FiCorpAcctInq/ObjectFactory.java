
package com.longbridge.FI.FiCorpAcctInq;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.longbridge.FI.FiCorpAcctInq package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.longbridge.FI.FiCorpAcctInq
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CorpCustAcctInqResponse }
     * 
     */
    public CorpCustAcctInqResponse createCorpCustAcctInqResponse() {
        return new CorpCustAcctInqResponse();
    }

    /**
     * Create an instance of {@link CorpCustAcctInqResponse.CorpCustAcctInqRs }
     * 
     */
    public CorpCustAcctInqResponse.CorpCustAcctInqRs createCorpCustAcctInqResponseCorpCustAcctInqRs() {
        return new CorpCustAcctInqResponse.CorpCustAcctInqRs();
    }

    /**
     * Create an instance of {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls }
     * 
     */
    public CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls createCorpCustAcctInqResponseCorpCustAcctInqRsCorpSaleDtls() {
        return new CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls();
    }

    /**
     * Create an instance of {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpCustDtls }
     * 
     */
    public CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpCustDtls createCorpCustAcctInqResponseCorpCustAcctInqRsCorpCustDtls() {
        return new CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpCustDtls();
    }

    /**
     * Create an instance of {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.ProductDtlsInfo }
     * 
     */
    public CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.ProductDtlsInfo createCorpCustAcctInqResponseCorpCustAcctInqRsCorpSaleDtlsProductDtlsInfo() {
        return new CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.ProductDtlsInfo();
    }

    /**
     * Create an instance of {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleChannelDtls }
     * 
     */
    public CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleChannelDtls createCorpCustAcctInqResponseCorpCustAcctInqRsCorpSaleDtlsSaleChannelDtls() {
        return new CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleChannelDtls();
    }

    /**
     * Create an instance of {@link CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleCust }
     * 
     */
    public CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleCust createCorpCustAcctInqResponseCorpCustAcctInqRsCorpSaleDtlsSaleCust() {
        return new CorpCustAcctInqResponse.CorpCustAcctInqRs.CorpSaleDtls.SaleCust();
    }

}

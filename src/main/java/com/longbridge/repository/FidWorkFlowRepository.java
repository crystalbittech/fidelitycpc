package com.longbridge.repository;

import com.longbridge.entities.FidWorkflow;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
public interface FidWorkFlowRepository extends JpaRepository<FidWorkflow, String> {

    List<FidWorkflow> findAll();

    FidWorkflow save(FidWorkflow fidWorkflow);

    FidWorkflow findByDocmntUploaderAndAcctNumber(String docmntUploader,String acctNumber);

    FidWorkflow findByDocmntInputterAndAcctNumber(String docInputer,String acctNumber);

    List<FidWorkflow> findByDocmntUploader(String docmntUploader);

    FidWorkflow findByAcctNumber(String acctNumber);

}

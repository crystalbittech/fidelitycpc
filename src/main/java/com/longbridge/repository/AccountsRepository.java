package com.longbridge.repository;

import com.longbridge.entities.FinacleEntities.Accounts;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountsRepository extends CrudRepository<Accounts, String> {
    @Query(value = "select corp_id from crmuser.accounts where orgkey = ?1", nativeQuery = true)
    String fetchCorpId(String s);
    
}
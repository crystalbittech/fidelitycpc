package com.longbridge.repository;

import com.longbridge.entities.UpldDoc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UpldDocRepository extends JpaRepository<UpldDoc, String> {

    List<UpldDoc> findAll();

    UpldDoc save(UpldDoc upldDoc);

    UpldDoc findOne(String doc_Id);
}

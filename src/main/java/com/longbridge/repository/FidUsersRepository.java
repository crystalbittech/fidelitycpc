package com.longbridge.repository;

import com.longbridge.entities.FidUsers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FidUsersRepository extends JpaRepository<FidUsers, String> {

    List<FidUsers> findAll();

    List<FidUsers> findByStatus(String status);

    FidUsers findByUserEmailAndPass(String userNmae, String passWord);



    //List<FidUsers> save(FidUsers usrDetails);

    //List<FidUsers> save(FidUsers fidUsers);


}

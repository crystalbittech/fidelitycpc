package com.longbridge.repository;

import com.longbridge.entities.FidWorkflowHist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FidWorkFlowHistRepository extends JpaRepository<FidWorkflowHist, String> {

    List<FidWorkflowHist> findAll();

    List<FidWorkflowHist> findByAcctNumber(String acctNumber);

    long count();

    FidWorkflowHist save (FidWorkflowHist fidWorkflowHist);

}

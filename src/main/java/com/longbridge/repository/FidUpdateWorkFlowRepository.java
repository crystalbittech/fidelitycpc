package com.longbridge.repository;

import com.longbridge.entities.FidUpdateWorkFlow;
import com.longbridge.entities.FidUsers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FidUpdateWorkFlowRepository extends JpaRepository<FidUpdateWorkFlow, String> {
    //@Query(value = "select update_value from fid_update_work_flow where accnt_id = ?1", nativeQuery = true)
    FidUpdateWorkFlow findByAccntId(String s);

    FidUpdateWorkFlow save(FidUpdateWorkFlow fidUpdateWorkFlow);


}

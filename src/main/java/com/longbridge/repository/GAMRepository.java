package com.longbridge.repository;

import com.longbridge.entities.FinacleEntities.GAM;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GAMRepository extends CrudRepository<GAM, String> {
    @Query(value = "select cust_id from tbaadm.GENERAL_ACCT_MAST_TABLE where FORACID = ?1", nativeQuery = true)
    String fetchCustId(String foracid);
    
}
package com.longbridge.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter{

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        registry.addViewController("/").setViewName("redirect");
        registry.addViewController("/review").setViewName("review");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/docmgmnt").setViewName("docmgmnt");
        registry.addViewController("/update").setViewName("update");
        registry.addViewController("/verify").setViewName("verify");
        registry.addViewController("/umr").setViewName("umr");
        registry.addViewController("/report").setViewName("report");
        registry.addViewController("/403").setViewName("403");
        registry.addViewController("/500").setViewName("500");
    }


    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/jsp/");
        resolver.setSuffix(".jsp");
        return resolver;
    }
}

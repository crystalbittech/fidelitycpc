package com.longbridge.controller;



import com.fasterxml.jackson.databind.ObjectMapper;
import com.longbridge.FI.FiTester;
import com.longbridge.entities.*;
import com.longbridge.entitiesBk.Account;
import com.longbridge.entitiesBk.DataCenter;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.longbridge.repository.*;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by mac on 3/29/17.
 */
@org.springframework.web.bind.annotation.RestController
public class RestController {
    private FidUsersRepository fidUsersRepository;
    private FidWorkFlowRepository fidWorkFlowRepository;
    private FidWorkFlowHistRepository fidWorkFlowHistRepository;
    private UpldDocRepository upldDocRepository;
    private SimpleDateFormat  formatter1 =new SimpleDateFormat("yyyy-MM-dd");
    private static FidUpdateWorkFlowRepository fidUpdateWorkFlowRepository;
    private static AccountsRepository accountsRepository;
    private static GAMRepository gamRepository;



    @Autowired
    public void setAccountsRepository(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Autowired
    public void setGAMRepository(GAMRepository gamRepository) {
        this.gamRepository = gamRepository;
    }

//    @Autowired
    static org.slf4j.Logger logger = LoggerFactory.getLogger(RestController.class);;
    @Autowired
    DataCenter dataCenter;

    @Autowired
    public void setFidUsersRepository(FidUsersRepository fidUsersRepository) {
        this.fidUsersRepository = fidUsersRepository;
    }

    @Autowired
    public void setFidWorkFlowRepository(FidWorkFlowRepository fidWorkFlowRepository) {
        this.fidWorkFlowRepository = fidWorkFlowRepository;
    }
    @Autowired
    public void setFidWorkFlowHistRepository(FidWorkFlowHistRepository fidWorkFlowHistRepository) {
        this.fidWorkFlowHistRepository = fidWorkFlowHistRepository;
    }


    @Autowired
    public void setUpldDocRepository(UpldDocRepository upldDocRepository) {
        this.upldDocRepository = upldDocRepository;
    }

    @Autowired
    public void setFidUpdateWorkFlowRepository(FidUpdateWorkFlowRepository fidUpdateWorkFlowRepository) {
        this.fidUpdateWorkFlowRepository = fidUpdateWorkFlowRepository;
    }




    @RequestMapping("/search")
    public Account search(@RequestBody Map<String, Object> payload){
               logger.info("payload is : "+payload);
                String acctNum = payload.get("acctid").toString();
               if(dataCenter == null){
                       logger.info("it sems datacenter is null");
                }
                Account account = dataCenter.accounts.get(acctNum);
               logger.info(String.valueOf(account));

                       return account;
    }

    @RequestMapping("/fetchUsers")
    public List<FidUsers> fetchUsers(@RequestBody Map<String, Object> payload){
        logger.info("payload is : " + payload);
        List<FidUsers> userDetails = fidUsersRepository.findByStatus("ACTIVE");
        if(userDetails!=null){
            for(FidUsers usr:userDetails){
                logger.info(usr.getUserName());
            }
        }
        return userDetails;
    }

    @RequestMapping("/fetchWorkFlow")
    public List<FidWorkflow> fetchWorkFlow(@RequestBody Map<String, Object> payload){
        logger.info("payload is : " + payload);
        List<FidWorkflow> workFlowList = fidWorkFlowRepository.findAll();
        return workFlowList;
    }

    @RequestMapping("/fetchDocument")
    public String fetchDocument(@RequestBody Map<String, Object> payload){
        logger.info("payload is : " + payload);
        UpldDoc upldDoc=null;
        String base64String="";
        String docId = (String) payload.get("docId");
        upldDoc = upldDocRepository.findOne(docId);
        if(upldDoc!=null) {
            BASE64Encoder encoder = new BASE64Encoder();
            base64String = encoder.encode(upldDoc.getDocVal());
        }
        return base64String;
    }

    @RequestMapping("/fetchAcctWorkFlowHist")
    public List<FidWorkflowHist> fetchAcctWorkFlowHist(@RequestBody Map<String, Object> payload){
        logger.info("payload is : " + payload);
        String acctId = (String) payload.get("acctId");
        List<FidWorkflowHist> fidWorkflowHist=null;
        fidWorkflowHist = fidWorkFlowHistRepository.findByAcctNumber(acctId);
        return fidWorkflowHist;
    }

    @RequestMapping("/fetchCustomerDetails")
    public CustomerDetailsForUpdate fetchCustomerDetails(@RequestBody Map<String, Object> payload){
        CustomerDetailsForUpdate customerDetailsForUpdate = new CustomerDetailsForUpdate();
        logger.info("payload is : " + payload);
        String acctId = payload.get("acctId").toString();
        String acctLevel = payload.get("acctLevel").toString();
        logger.info("Fetching custId using accountId: "+acctId);
        String custId = gamRepository.fetchCustId(acctId);
        logger.info("Fetching corpId using custId: "+custId);
        String corpId = accountsRepository.fetchCorpId(custId);
        String acctType = accountsRepository.fetchCorpId(custId)==null?"RETAIL":"CORPORATE";
        System.out.println("Customer Type is "+acctType+" "+acctId);
        if(acctLevel.equalsIgnoreCase("updated")||acctLevel.equalsIgnoreCase("adviced")){
            FidUpdateWorkFlow fidUpdateWorkFlow1 = fidUpdateWorkFlowRepository.findByAccntId(acctId.trim());
            byte[] fidUpdateWorkFlow = fidUpdateWorkFlow1.getUpdateValue();
            logger.info("cached data is "+fidUpdateWorkFlow1.getAccntId());
            System.out.println(fidUpdateWorkFlow.toString());
            ByteArrayInputStream in = new ByteArrayInputStream(fidUpdateWorkFlow);
            ObjectInputStream is = null;
            try {
                is = is = new ObjectInputStream(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                customerDetailsForUpdate = (CustomerDetailsForUpdate) is.readObject();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }else{
            customerDetailsForUpdate = new FiTester().fetchforUpdate(acctId,acctType,custId);

        }
        logger.info("Successfuly Fetched for verification"+customerDetailsForUpdate.toString());
        return customerDetailsForUpdate;
    }

    @RequestMapping("/fetchAcctDetails")
    public CustomerDetailsForInquiry fetchAcctDetails(@RequestBody Map<String, Object> payload){
        CustomerDetailsForInquiry customerDetailsForInquiry;
        logger.info("payload is : " + payload);
        String acctId = payload.get("acctId").toString();
        String acctLevel = payload.get("acctLevel").toString();
        String custId = gamRepository.fetchCustId(acctId.trim());
        customerDetailsForInquiry = new FiTester().fetchforInquiry(acctId,custId);
        return customerDetailsForInquiry;
    }


    //Saves and Update
    @RequestMapping("/saveWorkFlow")
    public String saveWorkFlow(@RequestBody Map<String, Object> payload) throws IOException, SQLException {
        String status="";
        logger.info("payload is : " + payload);
        String acctid = (String) payload.get("acctId");
        String updateStatus = (String) payload.get("docStatus");
        String docInputter = (String)payload.get("docInputter");
        String docId = (String)payload.get("docId");
        String docUploader = (String)payload.get("docUploader");
        String docVerifier = (String)payload.get("docVerifier");
        String rejectReason = (String)payload.get("rejectReason");
        String note = (String)payload.get("note");
        String docsSubmitted = (String)payload.get("docsSubmitted");//;
        String acctOpndtStr = String.valueOf(payload.get("acctOpnDate"));
        Timestamp acctOpnDate = null;
        if(acctOpndtStr.contains("-")){
            try{
                acctOpnDate = new Timestamp(formatter1.parse(acctOpndtStr.substring(0,10)).getTime());
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            acctOpnDate = new Timestamp(Long.valueOf(acctOpndtStr));
        }

        Timestamp oprtndate = new Timestamp(new Date().getTime());
    //todo start from here to comment lock
        System.out.println("Initia Stat of Lock is :"+payload.get("originalStat"));
        if(updateStatus.equals("UNLOCK")) {
            System.out.println("Enetered Here for Unlock");
            FidWorkflow workFlow = fidWorkFlowRepository.findByDocmntUploaderAndAcctNumber(docUploader,acctid);
            String presntStatus = workFlow.getAcctStatus();
            String originalStat = (String)payload.get("originalStat");
            if(presntStatus.equals("LOCKED")){
                workFlow.setAcctStatus(originalStat);
                FidWorkflow workFlow2 = new FidWorkflow(workFlow);
                fidWorkFlowRepository.save(workFlow2);
                return "";
            }
        }else if(updateStatus.equals("INPUTERUNLOCK")) {
            FidWorkflow workFlow = fidWorkFlowRepository.findByDocmntInputterAndAcctNumber(docInputter,acctid);
            String presntStatus = workFlow.getAcctStatus();
            String originalStat = (String)payload.get("originalStat");
            if(presntStatus.equals("LOCKED")){
                workFlow.setAcctStatus(originalStat);
                FidWorkflow workFlow2 = new FidWorkflow(workFlow);
                fidWorkFlowRepository.save(workFlow2);
                return "";
            }
        }else if(updateStatus.equals("UNLOCKALL")) {
            List<FidWorkflow> workFlow = fidWorkFlowRepository.findByDocmntUploader(docUploader);
            for(FidWorkflow fidWorkflow:workFlow){
                String originalStat = (String)payload.get("originalStat");
                String presntStatus = fidWorkflow.getAcctStatus();
                if(presntStatus.equals("LOCKED")){
                    fidWorkflow.setAcctStatus(originalStat);
                    FidWorkflow workFlow2 = new FidWorkflow(fidWorkflow);
                    fidWorkFlowRepository.save(workFlow2);
                }
            }
            return "";
        }else if(updateStatus.equals("LOCK")) {
            //dateInputted = new Timestamp(new Date().getTime());
            FidWorkflow workFlow = fidWorkFlowRepository.findByDocmntUploaderAndAcctNumber(docUploader,acctid);
            workFlow.setAcctStatus("LOCKED");
            FidWorkflow workFlow2 = new FidWorkflow(workFlow);
            fidWorkFlowRepository.save(workFlow2);
            return "";
        }else if(updateStatus.equals("INPUTERLOCK")) {
            FidWorkflow workFlow = fidWorkFlowRepository.findByDocmntInputterAndAcctNumber(docInputter,acctid);
            workFlow.setAcctStatus("LOCKED");
            FidWorkflow workFlow2 = new FidWorkflow(workFlow);
            fidWorkFlowRepository.save(workFlow2);
            return "";
        }else
          //todo stop here to comment lock
        if (updateStatus.equals("UPDATED")) {
            //dateInputted = new Timestamp(new Date().getTime());
            FidWorkflow workFlow = fidWorkFlowRepository.findByDocmntUploaderAndAcctNumber(docUploader,acctid);
            Timestamp dateInputted = new Timestamp(new Date().getTime());
            workFlow.setAcctStatus("UPDATED");
            workFlow.setDocInputDate(dateInputted);
            workFlow.setDocmntInputter(docInputter);
            workFlow.setNote(note);
            workFlow.setAcctVerifier("NA");
            workFlow.setAcctVerifyDate(null);
            FidWorkflow workFlow2 = new FidWorkflow(workFlow);
            fidWorkFlowRepository.save(workFlow2);
            logger.info(String.valueOf(payload.get("updateData")));
            String appdata = String.valueOf(payload.get("updateData"));

            CustomerDetailsForUpdate customerDetailsForUpdate = new CustomerDetailsForUpdate();
            ObjectMapper mapper = new ObjectMapper();
            try {
                customerDetailsForUpdate = mapper.readValue(appdata,CustomerDetailsForUpdate.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            logger.info("Now saving update with data "+customerDetailsForUpdate.toString());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(customerDetailsForUpdate);
            byte[] blob = out.toByteArray();
            FidUpdateWorkFlow fidUpdateWorkFlow = new FidUpdateWorkFlow();
            fidUpdateWorkFlow.setAccntId(acctid);
            fidUpdateWorkFlow.setUpdateValue(blob);
            fidUpdateWorkFlowRepository.save(fidUpdateWorkFlow);
        }else if(updateStatus.equals("VERIFIED")) {
            //dateInputted = new Timestamp(new Date().getTime());
            FidWorkflow workFlow = fidWorkFlowRepository.findByDocmntInputterAndAcctNumber(docInputter,acctid);
            Timestamp dateVerified = new Timestamp(new Date().getTime());
            workFlow.setAcctStatus("VERIFIED");
            workFlow.setAcctVerifyDate(dateVerified);
            workFlow.setAcctVerifier(docVerifier);
            FidWorkflow workFlow2 = new FidWorkflow(workFlow);
//            FidUpdateWorkFlow fidUpdateWorkFlow2 = fidUpdateWorkFlowRepository.findByAccntId(acctid);
//            byte[] fidUpdateWorkFlow = fidUpdateWorkFlow2.getUpdateValue();
//            CustomerDetailsForUpdate customerDetailsForUpdate = null;
//            ByteArrayInputStream in = new ByteArrayInputStream(fidUpdateWorkFlow);
//            ObjectInputStream is = null;
//            try {
//                is = new ObjectInputStream(in);
//                try {
//                    customerDetailsForUpdate = (CustomerDetailsForUpdate) is.readObject();
//                } catch (ClassNotFoundException e) {
//                    e.printStackTrace();
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            String custId = gamRepository.fetchCustId(acctid);
//            boolean isRetail = accountsRepository.fetchCorpId(custId)==null?true:false;
//            String resp = new FiTester().updateCustomer(customerDetailsForUpdate,isRetail);
//
//            if(resp.contains("Succesfully")){
                fidWorkFlowRepository.save(workFlow2);
//            }else{
//                System.out.println("response is "+resp);
//                return resp;
//            }
            return "SUCCESSFULLY VERIFIED";
            //return "";
        }else if(updateStatus.equals("DEFAULT")) {
            //dateInputted = new Timestamp(new Date().getTime());

            String base64String =  String.valueOf(payload.get("fileVal"));
            String fileVal="";
            try{
                fileVal = base64String.split(",")[1];
            }catch(Exception e){
                e.printStackTrace();
            }
            //String UPLOADED_FOLDER = "C:/Users/LB-PRJ-020/Documents/Longbridge/"+docId;
            BASE64Decoder decoder = new BASE64Decoder();
            byte[]decodeBytes = decoder.decodeBuffer(fileVal);
//            File fileToSave = new File(UPLOADED_FOLDER);
//            FileOutputStream fop = new FileOutputStream(fileToSave);
//            fop.write(decodeBytes);
//            fop.flush();
//            fop.close();
            Timestamp dateUploaded = new Timestamp(new Date().getTime());
            FidWorkflow workFlow = new FidWorkflow(docId,acctid,acctOpnDate,updateStatus,docUploader,dateUploaded,docInputter,
                    null,docVerifier,null,docsSubmitted,rejectReason,note);
            fidWorkFlowRepository.save(workFlow);
//            base64String = base64String.replace("[","");
//            base64String = base64String.replace("]","");
            UpldDoc upldDoc = new UpldDoc(docId,decodeBytes);//todo change to byte[]
            upldDocRepository.save(upldDoc);

            //return "";
        }else if(updateStatus.equals("REJECTED")) {
            FidWorkflow workFlow = fidWorkFlowRepository.findByDocmntUploaderAndAcctNumber(docUploader,acctid);
            Timestamp dateInputted = new Timestamp(new Date().getTime());
            workFlow.setAcctStatus("REJECTED");
            workFlow.setDocInputDate(dateInputted);
            workFlow.setDocmntInputter(docInputter);
            workFlow.setStatusReason(rejectReason);
            workFlow.setAcctVerifier("NA");
            workFlow.setAcctVerifyDate(null);
            FidWorkflow workFlow2 = new FidWorkflow(workFlow);
            fidWorkFlowRepository.save(workFlow2);
            //return "";
        }
//        else if(updateStatus.equals("RETURNED")) {
//            FidWorkflow workFlow = fidWorkFlowRepository.findByDocmntInputterAndAcctNumber(docInputter,acctid);
//            Timestamp dateVerified = new Timestamp(new Date().getTime());
//            workFlow.setAcctStatus("REJECTED");
//            workFlow.setAcctVerifyDate(dateVerified);
//            workFlow.setAcctVerifier(docVerifier);
//            workFlow.setStatusReason(rejectReason);
//            FidWorkflow workFlow2 = new FidWorkflow(workFlow);
//            fidWorkFlowRepository.save(workFlow2);
//        }
        else if(updateStatus.equals("ADVICED")) {
            FidWorkflow workFlow = fidWorkFlowRepository.findByDocmntInputterAndAcctNumber(docInputter,acctid);
            Timestamp dateVerified = new Timestamp(new Date().getTime());
            workFlow.setAcctStatus("ADVICED");
            workFlow.setAcctVerifyDate(dateVerified);
            workFlow.setAcctVerifier(docVerifier);
            workFlow.setStatusReason(rejectReason);
            FidWorkflow workFlow2 = new FidWorkflow(workFlow);
            fidWorkFlowRepository.save(workFlow2);
            //return "";
        }

        if(updateStatus.equalsIgnoreCase("ADVICED")||updateStatus.equalsIgnoreCase("REJECTED")
                ||updateStatus.equalsIgnoreCase("VERIFIED")||updateStatus.equalsIgnoreCase("UPDATED")
                ||updateStatus.equalsIgnoreCase("DEFAULT")){
            long nxtIndex = fidWorkFlowHistRepository.count()+1;
            FidWorkflowHist workFlowHist = new FidWorkflowHist(nxtIndex,docId,acctid,acctOpnDate,updateStatus,oprtndate,rejectReason,note,docsSubmitted);
            fidWorkFlowHistRepository.save(workFlowHist);
        }


        return status;
    }
    //Good
    @RequestMapping("/chkWorkFlow")
    public String chkWorkFlow(@RequestBody Map<String, Object> payload){
        logger.info("Payload is: "+payload);
        String acctId = String.valueOf(payload.get("acctId"));
        String acctStatus="";
        FidWorkflow fidWorkflow = fidWorkFlowRepository.findByAcctNumber(acctId);
        acctStatus = fidWorkflow.getAcctStatus();
        return acctStatus;
    }
    //Saves and Update
//    @RequestMapping("/saveDocument")
//    public String saveDocument(@RequestBody Map<String, Object> payload){
//        String status="";
//        logger.info("payload is : " + payload);
////        String docId = (String) payload.get("acctId");
////        String updateStatus = (String) payload.get("docStatus");
////        String docVal = (String)payload.get("docVal");
////        String docUploader = (String)payload.get("acctReviewer");
////        String docCommenter = (String)payload.get("acctModifier");
////        Timestamp dateUploaded = (Timestamp) payload.get("acctVerifier");
////        String updateReason = (String)payload.get("updateReason");
////        if(updateStatus=="D"){
////            updateStatus = "DELETE";
////        }
//        //FidWorkflow workFlow = new FidWorkflow(acctid,docId,updateStatus,acctReviewer,acctModifier,acctVerifier,rejectReason);
////        try{fidWorkFlowRepository.save(workFlow);}
////        catch(Exception e){
////            status = "Failed";
////            logger.info("Error is ; "+e);
////        }
//        return "Goodd";
//    }

    @RequestMapping("/saveUserDetails")
    public void saveUserDetails(@RequestBody Map<String, Object> payload){
        logger.info("Payload is :"+payload);
        String userName = (String) payload.get("userName");

        String userEmail = (String) payload.get("userEmail");
        String passWord = (String) payload.get("passWord");
        String status = (String) payload.get("status");
        String userRole = (String) payload.get("userRole");
        Timestamp initDateCreated = (Timestamp) payload.get("dateCreated");
        Timestamp dateCreated = null;
        if(initDateCreated==null){
            long dateTime = new Date().getTime();
            dateCreated = new Timestamp(dateTime);
        }
        Long usrCount;
        if(payload.get("userId")!=null){
            usrCount = Long.valueOf(String.valueOf(payload.get("userId")));
            logger.info("Doing Update");
        }else{
            usrCount = fidUsersRepository.count() + 1;
            logger.info("Doing UpdateInserting New Record ");
        }
        FidUsers usrDetails = new FidUsers(usrCount,userName,passWord,userRole,userEmail,dateCreated,status);
        fidUsersRepository.save(usrDetails);
        //return fidUsers;
    }

//    @RequestMapping("/updateCustomerDetails")
//    public String updateCustomerDetails(@RequestBody Map<String, Object> payload){
//        String status="";
//        logger.info("payload is : " + payload);
//        String accountid = (String) payload.get("accountid");
//        String acctTye = (String) payload.get("acctType");
//        String acctBal = (String) payload.get("acctBal");
//        String title = (String) payload.get("title");
//        String gender = (String) payload.get("gender");
//        String firstName = (String) payload.get("firstName");
//        String middleName = (String) payload.get("middleName");
//        String lastName = (String) payload.get("lastName");
//        Long timeStamp = (Long) payload.get("custDob");
//        Timestamp custDob = new Timestamp(timeStamp);
//        String custBvn = (String) payload.get("custBvn");
//        String acctSegment = (String) payload.get("acctSegment");
//        String acctSubsegment = (String) payload.get("acctSubsegment");
//        String availedTrade = (String) payload.get("availedTrade");
//        String acctRegion = (String) payload.get("acctRegion");
//        String prefLocal = (String) payload.get("prefLocal");
//
//        String enableCrm = (String) payload.get("enableCrm");
//        String enableEbanking = (String) payload.get("enableEbanking");
//        String primaryRelatinId = (String) payload.get("primaryRelatinId");
//        String secondaryRelatinId = (String) payload.get("secondaryRelatinId");
//        String addressFormat = (String) payload.get("addressFormat");
//        String addressType = (String) payload.get("addressType");
//        String addressLabel = (String) payload.get("addressLabel");
//
//        String addressLine1 = (String) payload.get("addressLine1");
//        String localGovt = (String) payload.get("localGovt");
//        String state = (String) payload.get("state");
//        String country = (String) payload.get("country");
//        String city = (String) payload.get("city");
//        String prefContactType = (String) payload.get("prefContactType");
//        String prefMailType = (String) payload.get("prefMailType");
//
//        String prefMobileAlertType = (String) payload.get("prefMobileAlertType");
//        String phoneNum = (String) payload.get("phoneNum");
//        logger.info("trying me abi "+addressLine1+phoneNum);
//        String phoneType = (String) payload.get("phoneType");
//        String emailId = (String) payload.get("emailId");
//        String crncyId = (String) payload.get("crncyId");
//        String docId = (String) payload.get("docId");
//        String trade = (String) payload.get("trade");
//        String demographicId = (String) payload.get("demographicId");
//        Long acctOpnDateLng = (Long) payload.get("acctOpnDate");
//        Timestamp acctOpnDate = new Timestamp(acctOpnDateLng);
//        String acctStatus = (String) payload.get("acctStatus");
//
//
//        //RetDetails retDetails = new RetDetails(accountid,acctTye,acctBal,acctOpnDate,title,gender,firstName,middleName,
////                lastName,custDob,custBvn,acctSegment,acctSubsegment,availedTrade,acctRegion,prefLocal,enableCrm
////                ,enableEbanking,primaryRelatinId,secondaryRelatinId,addressFormat,addressType,addressLabel,addressLine1,
////                localGovt,state,country,city,prefContactType,prefMailType,prefMobileAlertType,phoneNum,
////                phoneType,emailId,crncyId,docId,trade,demographicId,acctStatus);
//
//
//        //retDetalsRepository.save(retDetails);
//        return status;
//    }

    @RequestMapping("/verifyCustomerDetails")
    public String verifyCustomerDetails(@RequestBody Map<String, Object> payload){
        List<Account> accounts = new ArrayList<Account>();
        Object object = payload.get("input");
        //Pushes to FI
        return null;
    }


//
//
//
//
//
//
//
//    @RequestMapping("/fetchSngCorpCustomer")
//    public CorpDetails fetchSngCorpCustomer(@RequestBody Map<String, Object> payload){
//        logger.info("payload is : " + payload);
//        String acctId = payload.get("acctId").toString();
//        CorpDetails corpDetails = corpDetalsRepository.findOne(acctId);
//
//        return corpDetails;
//    }
//
//    @RequestMapping("/fetchRetailCustomerForUpdate")
//    public RetDetailsUpdate fetchRetailCustomerForUpdate(@RequestBody Map<String, Object> payload){
//        logger.info("payload is : " + payload);
//        String acctId = payload.get("acctId").toString();
//        String docId = payload.get("docId").toString();
//
//        FidAddress fidAddress = fidAddressRepository.findOne(acctId);
//        FidPrefPhoneEmail fidPrefPhone_email = fidPrefPhnEmailRepository.findOne(acctId);
//        List<FidPhoneEmail> fidPhoneEmails = fidPhnEmailRepository.findByAcctId(acctId);
//        FidDemographic fidDemographic = fidDemographicRepository.findOne(acctId);
//        List<FidEmploymentHist> fidEmploymentHist = fidEmplHistRepository.findByAcctId(acctId);
//        RetDetails retDetails = retDetalsRepository.findOne(acctId);
//        FidDocument fidDocument = fidDocRepository.findOne(docId);
//        Contact contact = new Contact(fidAddress, fidPrefPhone_email,fidPhoneEmails);
//        List<FidCrncy> fidCrncy = fidCrncyRepository.findByAccountid(acctId);
//        Demographic demographic = new Demographic(fidDemographic,fidEmploymentHist);
//        FidTradeFinance tradeFinance = fidTradeFinRepository.findOne(acctId);
//        RetDetailsUpdate retAcctDetails = new RetDetailsUpdate(retDetails,fidDocument,contact,fidCrncy,demographic,tradeFinance);
//        return retAcctDetails;
//    }
//
//    @RequestMapping("/fetchCorpCustomerForUpdate")
//    public CorpDetailsUpdate fetchCorpCustomerForUpdate(@RequestBody Map<String, Object> payload){
//        logger.info("payload is : " + payload);
//        String acctId = payload.get("acctId").toString();
//        String docId = payload.get("docId").toString();
//
//        FidAddress fidAddress = fidAddressRepository.findOne(acctId);
//        FidPrefPhoneEmail fidPrefPhone_email = fidPrefPhnEmailRepository.findOne(acctId);
//        List<FidPhoneEmail> fidPhoneEmails = fidPhnEmailRepository.findByAcctId(acctId);
//        CorpDetails corpDetails = corpDetalsRepository.findOne(acctId);
//        FidDocument fidDocument = fidDocRepository.findOne(docId);
//        Contact contact = new Contact(fidAddress, fidPrefPhone_email,fidPhoneEmails);
//        List<FidCrncy> fidCrncy =  fidCrncyRepository.findByAccountid(acctId);
//        CorpDetailsUpdate corpDetailsUpdate = new CorpDetailsUpdate(corpDetails,fidDocument,contact,fidCrncy);
//        return corpDetailsUpdate;
//    }
//
//    @RequestMapping("/fetchAllCorpCustomers")//todo to be removed
//    public List<CorpDetails> fetchAllCorpCustomers(@RequestBody Map<String, Object> payload){
//        logger.info("payload is : " + payload);
//        List<CorpDetails> corpDetails = corpDetalsRepository.findAll();
//        return corpDetails;
//    }
//
//    @RequestMapping("/fetchAllRetailCustomers")//todo to be removed
//    public List<RetDetails> fetchAllRetailCustomers(@RequestBody Map<String, Object> payload){
//        logger.info("payload is : " + payload);
//        List<RetDetails> retDetails = retDetalsRepository.findAll();
//        return retDetails;
//    }

//    @RequestMapping("/checkAcctType")
//    public String checkAcctType(@RequestBody Map<String, Object> payload){
//        logger.info("payload is : " + payload);
//        String rslt="";
//        String id = payload.get("acctId").toString();
//        logger.info("payload is : " + id);
//        RetDetails retCustDetails = retDetalsRepository.findOne(id);
//        if(retCustDetails!=null){
//            rslt = retCustDetails.getAcctType();
//            logger.info("fetched "+rslt);
//        }else{
//            CorpDetails corpCustDetails = corpDetalsRepository.findOne(id);
//            rslt = corpCustDetails.getAcctType();
//            logger.info("fetched "+rslt);
//        }
//
//        return rslt;
//    }


}

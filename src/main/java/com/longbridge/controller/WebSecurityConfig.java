package com.longbridge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.longbridge.security.CustomUserDetailsService;

@Configuration
@EnableWebMvcSecurity
@ComponentScan(basePackageClasses = CustomUserDetailsService.class)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

 @Autowired 
 private UserDetailsService userDetailsService;
 
 @Autowired
 public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {    
	 auth.userDetailsService(userDetailsService).passwordEncoder(passwordencoder());
 } 
 

 
 @Override
 protected void configure(HttpSecurity http) throws Exception {
   http.authorizeRequests()
    .antMatchers("/").access("hasRole('VISITOR') or hasRole('ADMIN') or hasRole('CSO') " +
           "or hasRole('REVIEWER') or hasRole('INPUTER') or hasRole('VERIFIER')")
           .antMatchers(HttpMethod.POST,"/*").permitAll()
//    .antMatchers("/docmgmnt").access("hasRole('CSO') or hasRole('ADMIN')")
//    .antMatchers("/review").access("hasRole('REVIEWER') or hasRole('ADMIN')")
//    .antMatchers("/update").access("hasRole('INPUTER') or hasRole('ADMIN')")
//    .antMatchers("/verify").access("hasRole('VERIFIER') or hasRole('ADMIN')")
//    .antMatchers("/umr").access("hasRole('ADMIN')")
//    .antMatchers("/report").access("hasRole('VISITOR') or hasRole('ADMIN')")
    .antMatchers("/fetchWorkFlow").permitAll()
    .anyRequest().permitAll()
    .and()
    .formLogin().loginPage("/login")
    .usernameParameter("username").passwordParameter("password")
    .and()
    .logout().logoutSuccessUrl("/login?logout")
    .and()
    .exceptionHandling().accessDeniedPage("/403")
    .and()
    .csrf().disable();
 }
 
 @Bean(name="passwordEncoder")
    public PasswordEncoder passwordencoder(){
     return new BCryptPasswordEncoder();
    }
}
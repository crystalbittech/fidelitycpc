
package com.longbridge.FiCorpCustInq;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.longbridge.FiCorpCustInq package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.longbridge.FiCorpCustInq
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse }
     * 
     */
    public GetCorporateCustomerDetailsResponse createGetCorporateCustomerDetailsResponse() {
        return new GetCorporateCustomerDetailsResponse();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails createGetCorporateCustomerDetailsResponseCorporateCustomerDetails() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet createGetCorporateCustomerDetailsResponseCorporateCustomerDetailsPreferencesDet() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet createGetCorporateCustomerDetailsResponseCorporateCustomerDetailsCorpDet() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.EntityDocumentDetails }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.EntityDocumentDetails createGetCorporateCustomerDetailsResponseCorporateCustomerDetailsEntityDocumentDetails() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.EntityDocumentDetails();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.FinancialDet }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.FinancialDet createGetCorporateCustomerDetailsResponseCorporateCustomerDetailsFinancialDet() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.FinancialDet();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpBaselDetails }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpBaselDetails createGetCorporateCustomerDetailsResponseCorporateCustomerDetailsCorpBaselDetails() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpBaselDetails();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet.PrefCorpmiscInfo }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet.PrefCorpmiscInfo createGetCorporateCustomerDetailsResponseCorporateCustomerDetailsPreferencesDetPrefCorpmiscInfo() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet.PrefCorpmiscInfo();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpaddresDet }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpaddresDet createGetCorporateCustomerDetailsResponseCorporateCustomerDetailsCorpDetCorpaddresDet() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpaddresDet();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpMiscellaneousInfo }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpMiscellaneousInfo createGetCorporateCustomerDetailsResponseCorporateCustomerDetailsCorpDetCorpMiscellaneousInfo() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpMiscellaneousInfo();
    }

    /**
     * Create an instance of {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.PhoneEmailData }
     * 
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.PhoneEmailData createGetCorporateCustomerDetailsResponseCorporateCustomerDetailsCorpDetPhoneEmailData() {
        return new GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.PhoneEmailData();
    }

}

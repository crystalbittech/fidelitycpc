
package com.longbridge.FiCorpCustInq;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorporateCustomerDetails">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="corpDet">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="average_annualincome" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="business_group" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="chargelevelcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="corp_key" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="corpaddresDet" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="address_line1">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="NO.10A ADETOKUNBO ADEMOLA STREET,"/>
 *                                             &lt;enumeration value="354, NEW OGORODE ROAD, OGORODE INDUSTRIAL EST"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="address_line2">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="VICTORIA ISLAND,"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="address_line3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="addresscategory">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="Mailing"/>
 *                                             &lt;enumeration value="Registered"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="building_level" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="businessCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="cellno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="cellnocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="cellnocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="cellnolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="city">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="VIL"/>
 *                                             &lt;enumeration value="."/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="cityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="countryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="faxno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="faxno2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="faxnocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="faxnocitycode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="faxnocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="faxnocountrycode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="faxnolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="faxnolocalcode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="freeTextAddress">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="NO.10A ADETOKUNBO ADEMOLA STREET,"/>
 *                                             &lt;enumeration value="354, NEW OGORODE ROAD, OGORODE INDUSTRIAL EST"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="freeTextLabel">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="Mailing"/>
 *                                             &lt;enumeration value="Registered"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="holdMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="holdMailInitiatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="holdMailReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="house_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="locality_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="mailstop" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="pagerno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="pagernocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="pagernocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="pagernolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="preferredAddress">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="N"/>
 *                                             &lt;enumeration value="Y"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="preferredFormat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="premise_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="residentialstatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="salutation_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="state">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="LG"/>
 *                                             &lt;enumeration value="."/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="street_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="street_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="telexCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="telexCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="telexLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="phoneNo1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="state_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="isAddressVerified" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="addressID">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="9664386"/>
 *                                             &lt;enumeration value="9664387"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="corpMiscellaneousInfo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="amount1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="amount2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="amount3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="amount4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="bankRelation">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="WALE"/>
 *                                             &lt;enumeration value="ONOSEREBA"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="cifId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="cifType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="corporateName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="firstName">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="SAMUSIDEEN"/>
 *                                             &lt;enumeration value="EDETAN"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="int1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="preferredcontactno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str11" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str7" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str8" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="corporate_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="corporatename_native" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="createdbysystemid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="crncy_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="customer_rating" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="customer_rating_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="date_of_commencement" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="date_of_incorporation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="defaultaddresstype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="dsaid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="entity_create_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="entity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="entityclass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="keycontact_personname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="legalentity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="modifiedbysystemid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="phoneEmailData" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="email">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value=""/>
 *                                             &lt;enumeration value="noemail@fcmb.com"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="emailpalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="phoneemailtype">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="COMM1"/>
 *                                             &lt;enumeration value="COMM2"/>
 *                                             &lt;enumeration value="COMML"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="phoneno">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="+234(0)0803000000"/>
 *                                             &lt;enumeration value="+234(0)012611862"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="phonenocitycode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="0"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="phonenocountrycode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="234"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="phonenolocalcode">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="0803000000"/>
 *                                             &lt;enumeration value="012611862"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="phoneoremail">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="PHONE"/>
 *                                             &lt;enumeration value="EMAIL"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="preferredflag">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="Y"/>
 *                                             &lt;enumeration value="N"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="workextension" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="phoneEmailID">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="40158193"/>
 *                                             &lt;enumeration value="40158194"/>
 *                                             &lt;enumeration value="40158195"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="primary_service_center" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="primaryrm_id_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="primaryRMLogin_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="principle_placeoperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="registration_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="relationship_createdby" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="relationship_startdate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="secondaryrm_id_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="secondRMLogin_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sector_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="segment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="short_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="short_name_native" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="source_of_funds" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="subsector" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="subsector_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="subsegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="taxid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tertiaryrmlogin_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="isEbankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ForeignAccTaxReportingReq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ForeignTaxReportingCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ForeignTaxReportingStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="FatcaRemarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AccessOwnerGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="entityDocumentDetails" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="countryofissue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="doccode">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="PASST"/>
 *                                   &lt;enumeration value="ID"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="docdelflg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="docexpirydate">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="2021-10-20T00:00:00.000"/>
 *                                   &lt;enumeration value="2099-12-31T00:00:00.000"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="docissuedate">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="2011-10-21T00:00:00.000"/>
 *                                   &lt;enumeration value="2010-01-01T00:00:00.000"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="docremarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="doctypecode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="doctypedescr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="entitytype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="identificationtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ismandatory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="placeofissue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="referencenumber">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="M0051652"/>
 *                                   &lt;enumeration value="RC8331"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="scanrequired" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="financialDet">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="business_assets" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="created_from" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="crncy_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cust_fin_year_end_mnth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cust_net_worth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="deposits_other_bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="entity_create_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="entity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="financial_year" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="financial_year_end" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="invest_shares_units" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="legal_details" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="legal_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="numberof_employees" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="property" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="sharehld_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tot_share_investment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="total_amount_held" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="type_of_statement" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="preferencesDet">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="calendar_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="combined_stmt_reqd" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="combined_stmtflag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cust_floor_limit_tds" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="do_not_email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="do_not_mail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="do_not_phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="entity_create_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="entity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="frequency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="frequency_holidaystatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="frequency_weekday" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="holdmail_flag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="loansstatementtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="mode_of_dispatch" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="prefCorpmiscInfo" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="amount1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="amount2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="amount3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="amount4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="date1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="int1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str1">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="NGN"/>
 *                                             &lt;enumeration value="USD"/>
 *                                             &lt;enumeration value="EUR"/>
 *                                             &lt;enumeration value="GBP"/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="str10" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str11" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str2">
 *                                         &lt;simpleType>
 *                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                             &lt;enumeration value="0131297"/>
 *                                             &lt;enumeration value=""/>
 *                                           &lt;/restriction>
 *                                         &lt;/simpleType>
 *                                       &lt;/element>
 *                                       &lt;element name="str3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str7" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str8" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="str9" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="preferred_channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="preffered_channel_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="statement_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tds_exmpt_ref_num" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tds_exmpt_rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tdsstatementtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="External_System_Pricing" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Relationship_Pricing_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Tax_Rate_Table_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="corpBaselDetails">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="obligor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="pdriskrating" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="lossdefprcnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="probdefprcnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="regulatoryrating" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="seniority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="exposurecategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="wholesaleexposubcat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="revenuesize" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="totalassets" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="custmessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="iscustnamedisclosure" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="securityindicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="isconglomerate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="investment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="industrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="getCorporateCustomerDetails_CustomData" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "corporateCustomerDetails",
    "getCorporateCustomerDetailsCustomData"
})
@XmlRootElement(name = "getCorporateCustomerDetailsResponse")
public class GetCorporateCustomerDetailsResponse implements Serializable {

    @XmlElement(name = "CorporateCustomerDetails", required = true)
    protected GetCorporateCustomerDetailsResponse.CorporateCustomerDetails corporateCustomerDetails;
    @XmlElement(name = "getCorporateCustomerDetails_CustomData", required = true)
    protected String getCorporateCustomerDetailsCustomData;

    /**
     * Gets the value of the corporateCustomerDetails property.
     * 
     * @return
     *     possible object is
     *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails }
     *     
     */
    public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails getCorporateCustomerDetails() {
        return corporateCustomerDetails;
    }

    /**
     * Sets the value of the corporateCustomerDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails }
     *     
     */
    public void setCorporateCustomerDetails(GetCorporateCustomerDetailsResponse.CorporateCustomerDetails value) {
        this.corporateCustomerDetails = value;
    }

    /**
     * Gets the value of the getCorporateCustomerDetailsCustomData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetCorporateCustomerDetailsCustomData() {
        return getCorporateCustomerDetailsCustomData;
    }

    /**
     * Sets the value of the getCorporateCustomerDetailsCustomData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetCorporateCustomerDetailsCustomData(String value) {
        this.getCorporateCustomerDetailsCustomData = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="corpDet">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="average_annualincome" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="business_group" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="chargelevelcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="corp_key" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="corpaddresDet" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="address_line1">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="NO.10A ADETOKUNBO ADEMOLA STREET,"/>
     *                                   &lt;enumeration value="354, NEW OGORODE ROAD, OGORODE INDUSTRIAL EST"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="address_line2">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="VICTORIA ISLAND,"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="address_line3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="addresscategory">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="Mailing"/>
     *                                   &lt;enumeration value="Registered"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="building_level" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="businessCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="cellno" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="cellnocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="cellnocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="cellnolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="city">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="VIL"/>
     *                                   &lt;enumeration value="."/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="cityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="countryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="faxno" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="faxno2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="faxnocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="faxnocitycode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="faxnocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="faxnocountrycode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="faxnolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="faxnolocalcode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="freeTextAddress">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="NO.10A ADETOKUNBO ADEMOLA STREET,"/>
     *                                   &lt;enumeration value="354, NEW OGORODE ROAD, OGORODE INDUSTRIAL EST"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="freeTextLabel">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="Mailing"/>
     *                                   &lt;enumeration value="Registered"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="holdMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="holdMailInitiatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="holdMailReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="house_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="locality_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="mailstop" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="pagerno" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="pagernocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="pagernocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="pagernolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="preferredAddress">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="N"/>
     *                                   &lt;enumeration value="Y"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="preferredFormat" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="premise_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="residentialstatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="salutation_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="state">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="LG"/>
     *                                   &lt;enumeration value="."/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="street_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="street_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="telexCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="telexCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="telexLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="phoneNo1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="state_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="isAddressVerified" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="addressID">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="9664386"/>
     *                                   &lt;enumeration value="9664387"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="corpMiscellaneousInfo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="amount1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="amount2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="amount3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="amount4" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="bankRelation">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="WALE"/>
     *                                   &lt;enumeration value="ONOSEREBA"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="cifId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="cifType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="corporateName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="firstName">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="SAMUSIDEEN"/>
     *                                   &lt;enumeration value="EDETAN"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="int1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="preferredcontactno" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str11" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str6" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str7" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str8" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="corporate_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="corporatename_native" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="createdbysystemid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="crncy_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="customer_rating" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="customer_rating_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="date_of_commencement" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="date_of_incorporation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="defaultaddresstype" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="dsaid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="entity_create_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="entity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="entityclass" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="keycontact_personname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="legalentity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="modifiedbysystemid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="phoneEmailData" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="email">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value=""/>
     *                                   &lt;enumeration value="noemail@fcmb.com"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="emailpalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="phoneemailtype">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="COMM1"/>
     *                                   &lt;enumeration value="COMM2"/>
     *                                   &lt;enumeration value="COMML"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="phoneno">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="+234(0)0803000000"/>
     *                                   &lt;enumeration value="+234(0)012611862"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="phonenocitycode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="0"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="phonenocountrycode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="234"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="phonenolocalcode">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="0803000000"/>
     *                                   &lt;enumeration value="012611862"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="phoneoremail">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="PHONE"/>
     *                                   &lt;enumeration value="EMAIL"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="preferredflag">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="Y"/>
     *                                   &lt;enumeration value="N"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="workextension" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="phoneEmailID">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="40158193"/>
     *                                   &lt;enumeration value="40158194"/>
     *                                   &lt;enumeration value="40158195"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="primary_service_center" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="primaryrm_id_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="primaryRMLogin_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="principle_placeoperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="registration_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="relationship_createdby" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="relationship_startdate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="secondaryrm_id_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="secondRMLogin_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sector_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="segment" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="short_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="short_name_native" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="source_of_funds" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="subsector" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="subsector_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="subsegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="taxid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tertiaryrmlogin_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="isEbankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ForeignAccTaxReportingReq" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ForeignTaxReportingCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ForeignTaxReportingStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FatcaRemarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AccessOwnerGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="entityDocumentDetails" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="countryofissue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="doccode">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="PASST"/>
     *                         &lt;enumeration value="ID"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="docdelflg" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="docexpirydate">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="2021-10-20T00:00:00.000"/>
     *                         &lt;enumeration value="2099-12-31T00:00:00.000"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="docissuedate">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="2011-10-21T00:00:00.000"/>
     *                         &lt;enumeration value="2010-01-01T00:00:00.000"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="docremarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="doctypecode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="doctypedescr" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="entitytype" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="identificationtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ismandatory" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="placeofissue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="referencenumber">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="M0051652"/>
     *                         &lt;enumeration value="RC8331"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="scanrequired" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="financialDet">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="business_assets" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="created_from" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="crncy_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cust_fin_year_end_mnth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cust_net_worth" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="deposits_other_bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="entity_create_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="entity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="financial_year" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="financial_year_end" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="invest_shares_units" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="legal_details" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="legal_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="numberof_employees" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="property" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="sharehld_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tot_share_investment" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="total_amount_held" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="type_of_statement" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="preferencesDet">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="calendar_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="combined_stmt_reqd" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="combined_stmtflag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cust_floor_limit_tds" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="do_not_email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="do_not_mail" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="do_not_phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="entity_create_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="entity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="frequency" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="frequency_holidaystatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="frequency_weekday" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="holdmail_flag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="loansstatementtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="mode_of_dispatch" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="prefCorpmiscInfo" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="amount1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="amount2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="amount3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="amount4" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="date1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="int1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str1">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="NGN"/>
     *                                   &lt;enumeration value="USD"/>
     *                                   &lt;enumeration value="EUR"/>
     *                                   &lt;enumeration value="GBP"/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="str10" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str11" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str2">
     *                               &lt;simpleType>
     *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                                   &lt;enumeration value="0131297"/>
     *                                   &lt;enumeration value=""/>
     *                                 &lt;/restriction>
     *                               &lt;/simpleType>
     *                             &lt;/element>
     *                             &lt;element name="str3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str4" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str5" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str6" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str7" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str8" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="str9" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="preferred_channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="preffered_channel_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="statement_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tds_exmpt_ref_num" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tds_exmpt_rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tdsstatementtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="External_System_Pricing" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Relationship_Pricing_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Tax_Rate_Table_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="corpBaselDetails">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="obligor" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="pdriskrating" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="lossdefprcnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="probdefprcnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="regulatoryrating" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="seniority" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="exposurecategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="wholesaleexposubcat" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="revenuesize" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="totalassets" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="custmessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="iscustnamedisclosure" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="securityindicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="isconglomerate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="investment" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="industrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "corpDet",
        "entityDocumentDetails",
        "financialDet",
        "preferencesDet",
        "corpBaselDetails"
    })
    public static class CorporateCustomerDetails implements Serializable{

        @XmlElement(required = true)
        protected GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet corpDet;
        protected List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.EntityDocumentDetails> entityDocumentDetails;
        @XmlElement(required = true)
        protected GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.FinancialDet financialDet;
        @XmlElement(required = true)
        protected GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet preferencesDet;
        @XmlElement(required = true)
        protected GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpBaselDetails corpBaselDetails;

        /**
         * Gets the value of the corpDet property.
         * 
         * @return
         *     possible object is
         *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet }
         *     
         */
        public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet getCorpDet() {
            return corpDet;
        }

        /**
         * Sets the value of the corpDet property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet }
         *     
         */
        public void setCorpDet(GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet value) {
            this.corpDet = value;
        }

        /**
         * Gets the value of the entityDocumentDetails property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the entityDocumentDetails property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEntityDocumentDetails().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.EntityDocumentDetails }
         * 
         * 
         */
        public List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.EntityDocumentDetails> getEntityDocumentDetails() {
            if (entityDocumentDetails == null) {
                entityDocumentDetails = new ArrayList<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.EntityDocumentDetails>();
            }
            return this.entityDocumentDetails;
        }

        /**
         * Gets the value of the financialDet property.
         * 
         * @return
         *     possible object is
         *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.FinancialDet }
         *     
         */
        public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.FinancialDet getFinancialDet() {
            return financialDet;
        }

        /**
         * Sets the value of the financialDet property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.FinancialDet }
         *     
         */
        public void setFinancialDet(GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.FinancialDet value) {
            this.financialDet = value;
        }

        /**
         * Gets the value of the preferencesDet property.
         * 
         * @return
         *     possible object is
         *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet }
         *     
         */
        public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet getPreferencesDet() {
            return preferencesDet;
        }

        /**
         * Sets the value of the preferencesDet property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet }
         *     
         */
        public void setPreferencesDet(GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet value) {
            this.preferencesDet = value;
        }

        /**
         * Gets the value of the corpBaselDetails property.
         * 
         * @return
         *     possible object is
         *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpBaselDetails }
         *     
         */
        public GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpBaselDetails getCorpBaselDetails() {
            return corpBaselDetails;
        }

        /**
         * Sets the value of the corpBaselDetails property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpBaselDetails }
         *     
         */
        public void setCorpBaselDetails(GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpBaselDetails value) {
            this.corpBaselDetails = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="obligor" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="pdriskrating" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="lossdefprcnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="probdefprcnt" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="regulatoryrating" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="seniority" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="exposurecategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="wholesaleexposubcat" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="revenuesize" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="totalassets" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="custmessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="iscustnamedisclosure" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="securityindicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="isconglomerate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="investment" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="industrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "obligor",
            "pdriskrating",
            "lossdefprcnt",
            "probdefprcnt",
            "regulatoryrating",
            "seniority",
            "exposurecategory",
            "wholesaleexposubcat",
            "revenuesize",
            "totalassets",
            "custmessage",
            "iscustnamedisclosure",
            "securityindicator",
            "isconglomerate",
            "investment",
            "industrycode"
        })
        public static class CorpBaselDetails implements Serializable{

            @XmlElement(required = true)
            protected String obligor;
            @XmlElement(required = true)
            protected String pdriskrating;
            @XmlElement(required = true)
            protected String lossdefprcnt;
            @XmlElement(required = true)
            protected String probdefprcnt;
            @XmlElement(required = true)
            protected String regulatoryrating;
            @XmlElement(required = true)
            protected String seniority;
            @XmlElement(required = true)
            protected String exposurecategory;
            @XmlElement(required = true)
            protected String wholesaleexposubcat;
            @XmlElement(required = true)
            protected String revenuesize;
            @XmlElement(required = true)
            protected String totalassets;
            @XmlElement(required = true)
            protected String custmessage;
            @XmlElement(required = true)
            protected String iscustnamedisclosure;
            @XmlElement(required = true)
            protected String securityindicator;
            @XmlElement(required = true)
            protected String isconglomerate;
            @XmlElement(required = true)
            protected String investment;
            @XmlElement(required = true)
            protected String industrycode;

            /**
             * Gets the value of the obligor property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getObligor() {
                return obligor;
            }

            /**
             * Sets the value of the obligor property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setObligor(String value) {
                this.obligor = value;
            }

            /**
             * Gets the value of the pdriskrating property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPdriskrating() {
                return pdriskrating;
            }

            /**
             * Sets the value of the pdriskrating property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPdriskrating(String value) {
                this.pdriskrating = value;
            }

            /**
             * Gets the value of the lossdefprcnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLossdefprcnt() {
                return lossdefprcnt;
            }

            /**
             * Sets the value of the lossdefprcnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLossdefprcnt(String value) {
                this.lossdefprcnt = value;
            }

            /**
             * Gets the value of the probdefprcnt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProbdefprcnt() {
                return probdefprcnt;
            }

            /**
             * Sets the value of the probdefprcnt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProbdefprcnt(String value) {
                this.probdefprcnt = value;
            }

            /**
             * Gets the value of the regulatoryrating property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegulatoryrating() {
                return regulatoryrating;
            }

            /**
             * Sets the value of the regulatoryrating property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegulatoryrating(String value) {
                this.regulatoryrating = value;
            }

            /**
             * Gets the value of the seniority property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSeniority() {
                return seniority;
            }

            /**
             * Sets the value of the seniority property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSeniority(String value) {
                this.seniority = value;
            }

            /**
             * Gets the value of the exposurecategory property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExposurecategory() {
                return exposurecategory;
            }

            /**
             * Sets the value of the exposurecategory property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExposurecategory(String value) {
                this.exposurecategory = value;
            }

            /**
             * Gets the value of the wholesaleexposubcat property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getWholesaleexposubcat() {
                return wholesaleexposubcat;
            }

            /**
             * Sets the value of the wholesaleexposubcat property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setWholesaleexposubcat(String value) {
                this.wholesaleexposubcat = value;
            }

            /**
             * Gets the value of the revenuesize property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRevenuesize() {
                return revenuesize;
            }

            /**
             * Sets the value of the revenuesize property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRevenuesize(String value) {
                this.revenuesize = value;
            }

            /**
             * Gets the value of the totalassets property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTotalassets() {
                return totalassets;
            }

            /**
             * Sets the value of the totalassets property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTotalassets(String value) {
                this.totalassets = value;
            }

            /**
             * Gets the value of the custmessage property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustmessage() {
                return custmessage;
            }

            /**
             * Sets the value of the custmessage property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustmessage(String value) {
                this.custmessage = value;
            }

            /**
             * Gets the value of the iscustnamedisclosure property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIscustnamedisclosure() {
                return iscustnamedisclosure;
            }

            /**
             * Sets the value of the iscustnamedisclosure property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIscustnamedisclosure(String value) {
                this.iscustnamedisclosure = value;
            }

            /**
             * Gets the value of the securityindicator property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSecurityindicator() {
                return securityindicator;
            }

            /**
             * Sets the value of the securityindicator property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSecurityindicator(String value) {
                this.securityindicator = value;
            }

            /**
             * Gets the value of the isconglomerate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsconglomerate() {
                return isconglomerate;
            }

            /**
             * Sets the value of the isconglomerate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsconglomerate(String value) {
                this.isconglomerate = value;
            }

            /**
             * Gets the value of the investment property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInvestment() {
                return investment;
            }

            /**
             * Sets the value of the investment property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInvestment(String value) {
                this.investment = value;
            }

            /**
             * Gets the value of the industrycode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIndustrycode() {
                return industrycode;
            }

            /**
             * Sets the value of the industrycode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIndustrycode(String value) {
                this.industrycode = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="average_annualincome" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="business_group" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="chargelevelcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="corp_key" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="corpaddresDet" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="address_line1">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="NO.10A ADETOKUNBO ADEMOLA STREET,"/>
         *                         &lt;enumeration value="354, NEW OGORODE ROAD, OGORODE INDUSTRIAL EST"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="address_line2">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="VICTORIA ISLAND,"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="address_line3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="addresscategory">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="Mailing"/>
         *                         &lt;enumeration value="Registered"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="building_level" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="businessCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="cellno" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="cellnocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="cellnocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="cellnolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="city">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="VIL"/>
         *                         &lt;enumeration value="."/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="cityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="countryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="faxno" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="faxno2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="faxnocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="faxnocitycode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="faxnocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="faxnocountrycode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="faxnolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="faxnolocalcode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="freeTextAddress">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="NO.10A ADETOKUNBO ADEMOLA STREET,"/>
         *                         &lt;enumeration value="354, NEW OGORODE ROAD, OGORODE INDUSTRIAL EST"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="freeTextLabel">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="Mailing"/>
         *                         &lt;enumeration value="Registered"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="holdMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="holdMailInitiatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="holdMailReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="house_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="locality_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="mailstop" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="pagerno" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="pagernocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="pagernocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="pagernolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="preferredAddress">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="N"/>
         *                         &lt;enumeration value="Y"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="preferredFormat" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="premise_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="residentialstatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="salutation_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="state">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="LG"/>
         *                         &lt;enumeration value="."/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="street_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="street_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="telexCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="telexCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="telexLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="phoneNo1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="state_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="isAddressVerified" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="addressID">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="9664386"/>
         *                         &lt;enumeration value="9664387"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="corpMiscellaneousInfo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="amount1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="amount2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="amount3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="amount4" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="bankRelation">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="WALE"/>
         *                         &lt;enumeration value="ONOSEREBA"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="cifId" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="cifType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="corporateName" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="firstName">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="SAMUSIDEEN"/>
         *                         &lt;enumeration value="EDETAN"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="int1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="preferredcontactno" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str11" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str6" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str7" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str8" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="corporate_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="corporatename_native" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="createdbysystemid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="crncy_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="customer_rating" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="customer_rating_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="date_of_commencement" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="date_of_incorporation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="defaultaddresstype" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="dsaid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="entity_create_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="entity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="entityclass" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="keycontact_personname" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="legalentity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="modifiedbysystemid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="phoneEmailData" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="email">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value=""/>
         *                         &lt;enumeration value="noemail@fcmb.com"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="emailpalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="phoneemailtype">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="COMM1"/>
         *                         &lt;enumeration value="COMM2"/>
         *                         &lt;enumeration value="COMML"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="phoneno">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="+234(0)0803000000"/>
         *                         &lt;enumeration value="+234(0)012611862"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="phonenocitycode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="0"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="phonenocountrycode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="234"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="phonenolocalcode">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="0803000000"/>
         *                         &lt;enumeration value="012611862"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="phoneoremail">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="PHONE"/>
         *                         &lt;enumeration value="EMAIL"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="preferredflag">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="Y"/>
         *                         &lt;enumeration value="N"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="workextension" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="phoneEmailID">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="40158193"/>
         *                         &lt;enumeration value="40158194"/>
         *                         &lt;enumeration value="40158195"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="primary_service_center" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="primaryrm_id_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="primaryRMLogin_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="principle_placeoperation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="region" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="registration_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="relationship_createdby" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="relationship_startdate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="secondaryrm_id_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="secondRMLogin_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sector" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sector_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="segment" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="short_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="short_name_native" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="source_of_funds" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="subsector" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="subsector_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="subsegment" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="taxid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tertiaryrmlogin_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="isEbankingEnabled" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ForeignAccTaxReportingReq" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ForeignTaxReportingCountry" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ForeignTaxReportingStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FatcaRemarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AccessOwnerGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "averageAnnualincome",
            "businessGroup",
            "chargelevelcode",
            "corpKey",
            "corpaddresDet",
            "corpMiscellaneousInfo",
            "corporateName",
            "corporatenameNative",
            "createdbysystemid",
            "crncyCode",
            "customerRating",
            "customerRatingCode",
            "dateOfCommencement",
            "dateOfIncorporation",
            "defaultaddresstype",
            "dsaid",
            "entityCreateFlg",
            "entityType",
            "entityclass",
            "keycontactPersonname",
            "legalentityType",
            "modifiedbysystemid",
            "phoneEmailData",
            "primaryServiceCenter",
            "primaryrmIdCode",
            "primaryRMLoginID",
            "principlePlaceoperation",
            "priority",
            "region",
            "registrationNumber",
            "relationshipCreatedby",
            "relationshipStartdate",
            "secondaryrmIdCode",
            "secondRMLoginID",
            "sector",
            "sectorCode",
            "segment",
            "shortName",
            "shortNameNative",
            "sourceOfFunds",
            "subsector",
            "subsectorCode",
            "subsegment",
            "taxid",
            "tertiaryrmloginId",
            "isEbankingEnabled",
            "foreignAccTaxReportingReq",
            "foreignTaxReportingCountry",
            "foreignTaxReportingStatus",
            "fatcaRemarks",
            "accessOwnerGroup"
        })
        public static class CorpDet implements Serializable{

            @XmlElement(name = "average_annualincome", required = true)
            protected String averageAnnualincome;
            @XmlElement(name = "business_group", required = true)
            protected String businessGroup;
            @XmlElement(required = true)
            protected String chargelevelcode;
            @XmlElement(name = "corp_key", required = true)
            protected String corpKey;
            protected List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpaddresDet> corpaddresDet;
            protected List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpMiscellaneousInfo> corpMiscellaneousInfo;
            @XmlElement(name = "corporate_name", required = true)
            protected String corporateName;
            @XmlElement(name = "corporatename_native", required = true)
            protected String corporatenameNative;
            @XmlElement(required = true)
            protected String createdbysystemid;
            @XmlElement(name = "crncy_code", required = true)
            protected String crncyCode;
            @XmlElement(name = "customer_rating", required = true)
            protected String customerRating;
            @XmlElement(name = "customer_rating_code", required = true)
            protected String customerRatingCode;
            @XmlElement(name = "date_of_commencement", required = true)
            protected String dateOfCommencement;
            @XmlElement(name = "date_of_incorporation", required = true)
            protected String dateOfIncorporation;
            @XmlElement(required = true)
            protected String defaultaddresstype;
            @XmlElement(required = true)
            protected String dsaid;
            @XmlElement(name = "entity_create_flg", required = true)
            protected String entityCreateFlg;
            @XmlElement(name = "entity_type", required = true)
            protected String entityType;
            @XmlElement(required = true)
            protected String entityclass;
            @XmlElement(name = "keycontact_personname", required = true)
            protected String keycontactPersonname;
            @XmlElement(name = "legalentity_type", required = true)
            protected String legalentityType;
            @XmlElement(required = true)
            protected String modifiedbysystemid;
            protected List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.PhoneEmailData> phoneEmailData;
            @XmlElement(name = "primary_service_center", required = true)
            protected String primaryServiceCenter;
            @XmlElement(name = "primaryrm_id_code", required = true)
            protected String primaryrmIdCode;
            @XmlElement(name = "primaryRMLogin_ID", required = true)
            protected String primaryRMLoginID;
            @XmlElement(name = "principle_placeoperation", required = true)
            protected String principlePlaceoperation;
            @XmlElement(required = true)
            protected String priority;
            @XmlElement(required = true)
            protected String region;
            @XmlElement(name = "registration_number", required = true)
            protected String registrationNumber;
            @XmlElement(name = "relationship_createdby", required = true)
            protected String relationshipCreatedby;
            @XmlElement(name = "relationship_startdate", required = true)
            protected String relationshipStartdate;
            @XmlElement(name = "secondaryrm_id_code", required = true)
            protected String secondaryrmIdCode;
            @XmlElement(name = "secondRMLogin_ID", required = true)
            protected String secondRMLoginID;
            @XmlElement(required = true)
            protected String sector;
            @XmlElement(name = "sector_code", required = true)
            protected String sectorCode;
            @XmlElement(required = true)
            protected String segment;
            @XmlElement(name = "short_name", required = true)
            protected String shortName;
            @XmlElement(name = "short_name_native", required = true)
            protected String shortNameNative;
            @XmlElement(name = "source_of_funds", required = true)
            protected String sourceOfFunds;
            @XmlElement(required = true)
            protected String subsector;
            @XmlElement(name = "subsector_code", required = true)
            protected String subsectorCode;
            @XmlElement(required = true)
            protected String subsegment;
            @XmlElement(required = true)
            protected String taxid;
            @XmlElement(name = "tertiaryrmlogin_id", required = true)
            protected String tertiaryrmloginId;
            @XmlElement(required = true)
            protected String isEbankingEnabled;
            @XmlElement(name = "ForeignAccTaxReportingReq", required = true)
            protected String foreignAccTaxReportingReq;
            @XmlElement(name = "ForeignTaxReportingCountry", required = true)
            protected String foreignTaxReportingCountry;
            @XmlElement(name = "ForeignTaxReportingStatus", required = true)
            protected String foreignTaxReportingStatus;
            @XmlElement(name = "FatcaRemarks", required = true)
            protected String fatcaRemarks;
            @XmlElement(name = "AccessOwnerGroup", required = true)
            protected String accessOwnerGroup;

            /**
             * Gets the value of the averageAnnualincome property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAverageAnnualincome() {
                return averageAnnualincome;
            }

            /**
             * Sets the value of the averageAnnualincome property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAverageAnnualincome(String value) {
                this.averageAnnualincome = value;
            }

            /**
             * Gets the value of the businessGroup property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBusinessGroup() {
                return businessGroup;
            }

            /**
             * Sets the value of the businessGroup property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBusinessGroup(String value) {
                this.businessGroup = value;
            }

            /**
             * Gets the value of the chargelevelcode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChargelevelcode() {
                return chargelevelcode;
            }

            /**
             * Sets the value of the chargelevelcode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChargelevelcode(String value) {
                this.chargelevelcode = value;
            }

            /**
             * Gets the value of the corpKey property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorpKey() {
                return corpKey;
            }

            /**
             * Sets the value of the corpKey property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorpKey(String value) {
                this.corpKey = value;
            }

            /**
             * Gets the value of the corpaddresDet property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the corpaddresDet property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCorpaddresDet().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpaddresDet }
             * 
             * 
             */
            public List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpaddresDet> getCorpaddresDet() {
                if (corpaddresDet == null) {
                    corpaddresDet = new ArrayList<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpaddresDet>();
                }
                return this.corpaddresDet;
            }

            /**
             * Gets the value of the corpMiscellaneousInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the corpMiscellaneousInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCorpMiscellaneousInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpMiscellaneousInfo }
             * 
             * 
             */
            public List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpMiscellaneousInfo> getCorpMiscellaneousInfo() {
                if (corpMiscellaneousInfo == null) {
                    corpMiscellaneousInfo = new ArrayList<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.CorpMiscellaneousInfo>();
                }
                return this.corpMiscellaneousInfo;
            }

            /**
             * Gets the value of the corporateName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorporateName() {
                return corporateName;
            }

            /**
             * Sets the value of the corporateName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorporateName(String value) {
                this.corporateName = value;
            }

            /**
             * Gets the value of the corporatenameNative property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCorporatenameNative() {
                return corporatenameNative;
            }

            /**
             * Sets the value of the corporatenameNative property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCorporatenameNative(String value) {
                this.corporatenameNative = value;
            }

            /**
             * Gets the value of the createdbysystemid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreatedbysystemid() {
                return createdbysystemid;
            }

            /**
             * Sets the value of the createdbysystemid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreatedbysystemid(String value) {
                this.createdbysystemid = value;
            }

            /**
             * Gets the value of the crncyCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCrncyCode() {
                return crncyCode;
            }

            /**
             * Sets the value of the crncyCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCrncyCode(String value) {
                this.crncyCode = value;
            }

            /**
             * Gets the value of the customerRating property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerRating() {
                return customerRating;
            }

            /**
             * Sets the value of the customerRating property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerRating(String value) {
                this.customerRating = value;
            }

            /**
             * Gets the value of the customerRatingCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerRatingCode() {
                return customerRatingCode;
            }

            /**
             * Sets the value of the customerRatingCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerRatingCode(String value) {
                this.customerRatingCode = value;
            }

            /**
             * Gets the value of the dateOfCommencement property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateOfCommencement() {
                return dateOfCommencement;
            }

            /**
             * Sets the value of the dateOfCommencement property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateOfCommencement(String value) {
                this.dateOfCommencement = value;
            }

            /**
             * Gets the value of the dateOfIncorporation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateOfIncorporation() {
                return dateOfIncorporation;
            }

            /**
             * Sets the value of the dateOfIncorporation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateOfIncorporation(String value) {
                this.dateOfIncorporation = value;
            }

            /**
             * Gets the value of the defaultaddresstype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDefaultaddresstype() {
                return defaultaddresstype;
            }

            /**
             * Sets the value of the defaultaddresstype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDefaultaddresstype(String value) {
                this.defaultaddresstype = value;
            }

            /**
             * Gets the value of the dsaid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDsaid() {
                return dsaid;
            }

            /**
             * Sets the value of the dsaid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDsaid(String value) {
                this.dsaid = value;
            }

            /**
             * Gets the value of the entityCreateFlg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityCreateFlg() {
                return entityCreateFlg;
            }

            /**
             * Sets the value of the entityCreateFlg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityCreateFlg(String value) {
                this.entityCreateFlg = value;
            }

            /**
             * Gets the value of the entityType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityType() {
                return entityType;
            }

            /**
             * Sets the value of the entityType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityType(String value) {
                this.entityType = value;
            }

            /**
             * Gets the value of the entityclass property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityclass() {
                return entityclass;
            }

            /**
             * Sets the value of the entityclass property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityclass(String value) {
                this.entityclass = value;
            }

            /**
             * Gets the value of the keycontactPersonname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getKeycontactPersonname() {
                return keycontactPersonname;
            }

            /**
             * Sets the value of the keycontactPersonname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setKeycontactPersonname(String value) {
                this.keycontactPersonname = value;
            }

            /**
             * Gets the value of the legalentityType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLegalentityType() {
                return legalentityType;
            }

            /**
             * Sets the value of the legalentityType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLegalentityType(String value) {
                this.legalentityType = value;
            }

            /**
             * Gets the value of the modifiedbysystemid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getModifiedbysystemid() {
                return modifiedbysystemid;
            }

            /**
             * Sets the value of the modifiedbysystemid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setModifiedbysystemid(String value) {
                this.modifiedbysystemid = value;
            }

            /**
             * Gets the value of the phoneEmailData property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the phoneEmailData property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPhoneEmailData().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.PhoneEmailData }
             * 
             * 
             */
            public List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.PhoneEmailData> getPhoneEmailData() {
                if (phoneEmailData == null) {
                    phoneEmailData = new ArrayList<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.CorpDet.PhoneEmailData>();
                }
                return this.phoneEmailData;
            }

            /**
             * Gets the value of the primaryServiceCenter property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryServiceCenter() {
                return primaryServiceCenter;
            }

            /**
             * Sets the value of the primaryServiceCenter property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryServiceCenter(String value) {
                this.primaryServiceCenter = value;
            }

            /**
             * Gets the value of the primaryrmIdCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryrmIdCode() {
                return primaryrmIdCode;
            }

            /**
             * Sets the value of the primaryrmIdCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryrmIdCode(String value) {
                this.primaryrmIdCode = value;
            }

            /**
             * Gets the value of the primaryRMLoginID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryRMLoginID() {
                return primaryRMLoginID;
            }

            /**
             * Sets the value of the primaryRMLoginID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryRMLoginID(String value) {
                this.primaryRMLoginID = value;
            }

            /**
             * Gets the value of the principlePlaceoperation property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrinciplePlaceoperation() {
                return principlePlaceoperation;
            }

            /**
             * Sets the value of the principlePlaceoperation property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrinciplePlaceoperation(String value) {
                this.principlePlaceoperation = value;
            }

            /**
             * Gets the value of the priority property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPriority() {
                return priority;
            }

            /**
             * Sets the value of the priority property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPriority(String value) {
                this.priority = value;
            }

            /**
             * Gets the value of the region property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegion() {
                return region;
            }

            /**
             * Sets the value of the region property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegion(String value) {
                this.region = value;
            }

            /**
             * Gets the value of the registrationNumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegistrationNumber() {
                return registrationNumber;
            }

            /**
             * Sets the value of the registrationNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegistrationNumber(String value) {
                this.registrationNumber = value;
            }

            /**
             * Gets the value of the relationshipCreatedby property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipCreatedby() {
                return relationshipCreatedby;
            }

            /**
             * Sets the value of the relationshipCreatedby property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipCreatedby(String value) {
                this.relationshipCreatedby = value;
            }

            /**
             * Gets the value of the relationshipStartdate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipStartdate() {
                return relationshipStartdate;
            }

            /**
             * Sets the value of the relationshipStartdate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipStartdate(String value) {
                this.relationshipStartdate = value;
            }

            /**
             * Gets the value of the secondaryrmIdCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSecondaryrmIdCode() {
                return secondaryrmIdCode;
            }

            /**
             * Sets the value of the secondaryrmIdCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSecondaryrmIdCode(String value) {
                this.secondaryrmIdCode = value;
            }

            /**
             * Gets the value of the secondRMLoginID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSecondRMLoginID() {
                return secondRMLoginID;
            }

            /**
             * Sets the value of the secondRMLoginID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSecondRMLoginID(String value) {
                this.secondRMLoginID = value;
            }

            /**
             * Gets the value of the sector property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSector() {
                return sector;
            }

            /**
             * Sets the value of the sector property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSector(String value) {
                this.sector = value;
            }

            /**
             * Gets the value of the sectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSectorCode() {
                return sectorCode;
            }

            /**
             * Sets the value of the sectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSectorCode(String value) {
                this.sectorCode = value;
            }

            /**
             * Gets the value of the segment property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSegment() {
                return segment;
            }

            /**
             * Sets the value of the segment property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSegment(String value) {
                this.segment = value;
            }

            /**
             * Gets the value of the shortName property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortName() {
                return shortName;
            }

            /**
             * Sets the value of the shortName property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortName(String value) {
                this.shortName = value;
            }

            /**
             * Gets the value of the shortNameNative property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShortNameNative() {
                return shortNameNative;
            }

            /**
             * Sets the value of the shortNameNative property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShortNameNative(String value) {
                this.shortNameNative = value;
            }

            /**
             * Gets the value of the sourceOfFunds property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSourceOfFunds() {
                return sourceOfFunds;
            }

            /**
             * Sets the value of the sourceOfFunds property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSourceOfFunds(String value) {
                this.sourceOfFunds = value;
            }

            /**
             * Gets the value of the subsector property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubsector() {
                return subsector;
            }

            /**
             * Sets the value of the subsector property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubsector(String value) {
                this.subsector = value;
            }

            /**
             * Gets the value of the subsectorCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubsectorCode() {
                return subsectorCode;
            }

            /**
             * Sets the value of the subsectorCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubsectorCode(String value) {
                this.subsectorCode = value;
            }

            /**
             * Gets the value of the subsegment property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubsegment() {
                return subsegment;
            }

            /**
             * Sets the value of the subsegment property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubsegment(String value) {
                this.subsegment = value;
            }

            /**
             * Gets the value of the taxid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxid() {
                return taxid;
            }

            /**
             * Sets the value of the taxid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxid(String value) {
                this.taxid = value;
            }

            /**
             * Gets the value of the tertiaryrmloginId property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTertiaryrmloginId() {
                return tertiaryrmloginId;
            }

            /**
             * Sets the value of the tertiaryrmloginId property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTertiaryrmloginId(String value) {
                this.tertiaryrmloginId = value;
            }

            /**
             * Gets the value of the isEbankingEnabled property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsEbankingEnabled() {
                return isEbankingEnabled;
            }

            /**
             * Sets the value of the isEbankingEnabled property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsEbankingEnabled(String value) {
                this.isEbankingEnabled = value;
            }

            /**
             * Gets the value of the foreignAccTaxReportingReq property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignAccTaxReportingReq() {
                return foreignAccTaxReportingReq;
            }

            /**
             * Sets the value of the foreignAccTaxReportingReq property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignAccTaxReportingReq(String value) {
                this.foreignAccTaxReportingReq = value;
            }

            /**
             * Gets the value of the foreignTaxReportingCountry property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxReportingCountry() {
                return foreignTaxReportingCountry;
            }

            /**
             * Sets the value of the foreignTaxReportingCountry property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxReportingCountry(String value) {
                this.foreignTaxReportingCountry = value;
            }

            /**
             * Gets the value of the foreignTaxReportingStatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxReportingStatus() {
                return foreignTaxReportingStatus;
            }

            /**
             * Sets the value of the foreignTaxReportingStatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxReportingStatus(String value) {
                this.foreignTaxReportingStatus = value;
            }

            /**
             * Gets the value of the fatcaRemarks property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFatcaRemarks() {
                return fatcaRemarks;
            }

            /**
             * Sets the value of the fatcaRemarks property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFatcaRemarks(String value) {
                this.fatcaRemarks = value;
            }

            /**
             * Gets the value of the accessOwnerGroup property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAccessOwnerGroup() {
                return accessOwnerGroup;
            }

            /**
             * Sets the value of the accessOwnerGroup property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAccessOwnerGroup(String value) {
                this.accessOwnerGroup = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="amount1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="amount2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="amount3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="amount4" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="bankRelation">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="WALE"/>
             *               &lt;enumeration value="ONOSEREBA"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="cifId" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="cifType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="corporateName" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="firstName">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="SAMUSIDEEN"/>
             *               &lt;enumeration value="EDETAN"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="int1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="preferredcontactno" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str10" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str11" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str6" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str7" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str8" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="EntityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amount1",
                "amount2",
                "amount3",
                "amount4",
                "bankRelation",
                "cifId",
                "cifType",
                "corporateName",
                "firstName",
                "int1",
                "preferredcontactno",
                "str10",
                "str11",
                "str3",
                "str6",
                "str7",
                "str8",
                "type",
                "entityType"
            })
            public static class CorpMiscellaneousInfo implements Serializable{

                @XmlElement(required = true)
                protected String amount1;
                @XmlElement(required = true)
                protected String amount2;
                @XmlElement(required = true)
                protected String amount3;
                @XmlElement(required = true)
                protected String amount4;
                @XmlElement(required = true)
                protected String bankRelation;
                @XmlElement(required = true)
                protected String cifId;
                @XmlElement(required = true)
                protected String cifType;
                @XmlElement(required = true)
                protected String corporateName;
                @XmlElement(required = true)
                protected String firstName;
                @XmlElement(required = true)
                protected String int1;
                @XmlElement(required = true)
                protected String preferredcontactno;
                @XmlElement(required = true)
                protected String str10;
                @XmlElement(required = true)
                protected String str11;
                @XmlElement(required = true)
                protected String str3;
                @XmlElement(required = true)
                protected String str6;
                @XmlElement(required = true)
                protected String str7;
                @XmlElement(required = true)
                protected String str8;
                @XmlElement(required = true)
                protected String type;
                @XmlElement(name = "EntityType", required = true)
                protected String entityType;

                /**
                 * Gets the value of the amount1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount1() {
                    return amount1;
                }

                /**
                 * Sets the value of the amount1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount1(String value) {
                    this.amount1 = value;
                }

                /**
                 * Gets the value of the amount2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount2() {
                    return amount2;
                }

                /**
                 * Sets the value of the amount2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount2(String value) {
                    this.amount2 = value;
                }

                /**
                 * Gets the value of the amount3 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount3() {
                    return amount3;
                }

                /**
                 * Sets the value of the amount3 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount3(String value) {
                    this.amount3 = value;
                }

                /**
                 * Gets the value of the amount4 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount4() {
                    return amount4;
                }

                /**
                 * Sets the value of the amount4 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount4(String value) {
                    this.amount4 = value;
                }

                /**
                 * Gets the value of the bankRelation property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBankRelation() {
                    return bankRelation;
                }

                /**
                 * Sets the value of the bankRelation property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBankRelation(String value) {
                    this.bankRelation = value;
                }

                /**
                 * Gets the value of the cifId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCifId() {
                    return cifId;
                }

                /**
                 * Sets the value of the cifId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCifId(String value) {
                    this.cifId = value;
                }

                /**
                 * Gets the value of the cifType property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCifType() {
                    return cifType;
                }

                /**
                 * Sets the value of the cifType property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCifType(String value) {
                    this.cifType = value;
                }

                /**
                 * Gets the value of the corporateName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCorporateName() {
                    return corporateName;
                }

                /**
                 * Sets the value of the corporateName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCorporateName(String value) {
                    this.corporateName = value;
                }

                /**
                 * Gets the value of the firstName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFirstName() {
                    return firstName;
                }

                /**
                 * Sets the value of the firstName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFirstName(String value) {
                    this.firstName = value;
                }

                /**
                 * Gets the value of the int1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getInt1() {
                    return int1;
                }

                /**
                 * Sets the value of the int1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setInt1(String value) {
                    this.int1 = value;
                }

                /**
                 * Gets the value of the preferredcontactno property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPreferredcontactno() {
                    return preferredcontactno;
                }

                /**
                 * Sets the value of the preferredcontactno property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPreferredcontactno(String value) {
                    this.preferredcontactno = value;
                }

                /**
                 * Gets the value of the str10 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr10() {
                    return str10;
                }

                /**
                 * Sets the value of the str10 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr10(String value) {
                    this.str10 = value;
                }

                /**
                 * Gets the value of the str11 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr11() {
                    return str11;
                }

                /**
                 * Sets the value of the str11 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr11(String value) {
                    this.str11 = value;
                }

                /**
                 * Gets the value of the str3 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr3() {
                    return str3;
                }

                /**
                 * Sets the value of the str3 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr3(String value) {
                    this.str3 = value;
                }

                /**
                 * Gets the value of the str6 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr6() {
                    return str6;
                }

                /**
                 * Sets the value of the str6 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr6(String value) {
                    this.str6 = value;
                }

                /**
                 * Gets the value of the str7 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr7() {
                    return str7;
                }

                /**
                 * Sets the value of the str7 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr7(String value) {
                    this.str7 = value;
                }

                /**
                 * Gets the value of the str8 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr8() {
                    return str8;
                }

                /**
                 * Sets the value of the str8 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr8(String value) {
                    this.str8 = value;
                }

                /**
                 * Gets the value of the type property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Sets the value of the type property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Gets the value of the entityType property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEntityType() {
                    return entityType;
                }

                /**
                 * Sets the value of the entityType property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEntityType(String value) {
                    this.entityType = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="address_line1">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="NO.10A ADETOKUNBO ADEMOLA STREET,"/>
             *               &lt;enumeration value="354, NEW OGORODE ROAD, OGORODE INDUSTRIAL EST"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="address_line2">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="VICTORIA ISLAND,"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="address_line3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="addresscategory">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="Mailing"/>
             *               &lt;enumeration value="Registered"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="building_level" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="businessCenter" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="cellno" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="cellnocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="cellnocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="cellnolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="city">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="VIL"/>
             *               &lt;enumeration value="."/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="cityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="countryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="domicile" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="faxno" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="faxno2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="faxnocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="faxnocitycode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="faxnocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="faxnocountrycode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="faxnolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="faxnolocalcode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="freeTextAddress">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="NO.10A ADETOKUNBO ADEMOLA STREET,"/>
             *               &lt;enumeration value="354, NEW OGORODE ROAD, OGORODE INDUSTRIAL EST"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="freeTextLabel">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="Mailing"/>
             *               &lt;enumeration value="Registered"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="holdMailFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="holdMailInitiatedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="holdMailReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="house_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="locality_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="mailstop" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="pagerno" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="pagernocitycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="pagernocountrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="pagernolocalcode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="preferredAddress">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="N"/>
             *               &lt;enumeration value="Y"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="preferredFormat" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="premise_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="residentialstatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="salutation_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="state">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="LG"/>
             *               &lt;enumeration value="."/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="street_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="street_no" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="suburb" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="telex" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="telexCityCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="telexCountryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="telexLocalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="town" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="zip" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="phoneNo1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="state_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="isAddressVerified" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="addressID">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="9664386"/>
             *               &lt;enumeration value="9664387"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "addressLine1",
                "addressLine2",
                "addressLine3",
                "addresscategory",
                "buildingLevel",
                "businessCenter",
                "cellno",
                "cellnocitycode",
                "cellnocountrycode",
                "cellnolocalcode",
                "city",
                "cityCode",
                "country",
                "countryCode",
                "domicile",
                "endDate",
                "faxno",
                "faxno2",
                "faxnocitycode",
                "faxnocitycode2",
                "faxnocountrycode",
                "faxnocountrycode2",
                "faxnolocalcode",
                "faxnolocalcode2",
                "freeTextAddress",
                "freeTextLabel",
                "holdMailFlag",
                "holdMailInitiatedBy",
                "holdMailReason",
                "houseNo",
                "localityName",
                "mailstop",
                "name",
                "pagerno",
                "pagernocitycode",
                "pagernocountrycode",
                "pagernolocalcode",
                "preferredAddress",
                "preferredFormat",
                "premiseName",
                "residentialstatus",
                "salutationCode",
                "startDate",
                "state",
                "streetName",
                "streetNo",
                "suburb",
                "telex",
                "telexCityCode",
                "telexCountryCode",
                "telexLocalCode",
                "town",
                "zip",
                "email",
                "phoneNo1",
                "stateCode",
                "isAddressVerified",
                "addressID"
            })
            public static class CorpaddresDet implements Serializable{

                @XmlElement(name = "address_line1", required = true)
                protected String addressLine1;
                @XmlElement(name = "address_line2", required = true)
                protected String addressLine2;
                @XmlElement(name = "address_line3", required = true)
                protected String addressLine3;
                @XmlElement(required = true)
                protected String addresscategory;
                @XmlElement(name = "building_level", required = true)
                protected String buildingLevel;
                @XmlElement(required = true)
                protected String businessCenter;
                @XmlElement(required = true)
                protected String cellno;
                @XmlElement(required = true)
                protected String cellnocitycode;
                @XmlElement(required = true)
                protected String cellnocountrycode;
                @XmlElement(required = true)
                protected String cellnolocalcode;
                @XmlElement(required = true)
                protected String city;
                @XmlElement(required = true)
                protected String cityCode;
                @XmlElement(required = true)
                protected String country;
                @XmlElement(required = true)
                protected String countryCode;
                @XmlElement(required = true)
                protected String domicile;
                @XmlElement(name = "end_date", required = true)
                protected String endDate;
                @XmlElement(required = true)
                protected String faxno;
                @XmlElement(required = true)
                protected String faxno2;
                @XmlElement(required = true)
                protected String faxnocitycode;
                @XmlElement(required = true)
                protected String faxnocitycode2;
                @XmlElement(required = true)
                protected String faxnocountrycode;
                @XmlElement(required = true)
                protected String faxnocountrycode2;
                @XmlElement(required = true)
                protected String faxnolocalcode;
                @XmlElement(required = true)
                protected String faxnolocalcode2;
                @XmlElement(required = true)
                protected String freeTextAddress;
                @XmlElement(required = true)
                protected String freeTextLabel;
                @XmlElement(required = true)
                protected String holdMailFlag;
                @XmlElement(required = true)
                protected String holdMailInitiatedBy;
                @XmlElement(required = true)
                protected String holdMailReason;
                @XmlElement(name = "house_no", required = true)
                protected String houseNo;
                @XmlElement(name = "locality_name", required = true)
                protected String localityName;
                @XmlElement(required = true)
                protected String mailstop;
                @XmlElement(required = true)
                protected String name;
                @XmlElement(required = true)
                protected String pagerno;
                @XmlElement(required = true)
                protected String pagernocitycode;
                @XmlElement(required = true)
                protected String pagernocountrycode;
                @XmlElement(required = true)
                protected String pagernolocalcode;
                @XmlElement(required = true)
                protected String preferredAddress;
                @XmlElement(required = true)
                protected String preferredFormat;
                @XmlElement(name = "premise_name", required = true)
                protected String premiseName;
                @XmlElement(required = true)
                protected String residentialstatus;
                @XmlElement(name = "salutation_code", required = true)
                protected String salutationCode;
                @XmlElement(name = "start_date", required = true)
                protected String startDate;
                @XmlElement(required = true)
                protected String state;
                @XmlElement(name = "street_name", required = true)
                protected String streetName;
                @XmlElement(name = "street_no", required = true)
                protected String streetNo;
                @XmlElement(required = true)
                protected String suburb;
                @XmlElement(required = true)
                protected String telex;
                @XmlElement(required = true)
                protected String telexCityCode;
                @XmlElement(required = true)
                protected String telexCountryCode;
                @XmlElement(required = true)
                protected String telexLocalCode;
                @XmlElement(required = true)
                protected String town;
                @XmlElement(required = true)
                protected String zip;
                @XmlElement(required = true)
                protected String email;
                @XmlElement(required = true)
                protected String phoneNo1;
                @XmlElement(name = "state_Code", required = true)
                protected String stateCode;
                @XmlElement(required = true)
                protected String isAddressVerified;
                @XmlElement(required = true)
                protected String addressID;

                /**
                 * Gets the value of the addressLine1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddressLine1() {
                    return addressLine1;
                }

                /**
                 * Sets the value of the addressLine1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddressLine1(String value) {
                    this.addressLine1 = value;
                }

                /**
                 * Gets the value of the addressLine2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddressLine2() {
                    return addressLine2;
                }

                /**
                 * Sets the value of the addressLine2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddressLine2(String value) {
                    this.addressLine2 = value;
                }

                /**
                 * Gets the value of the addressLine3 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddressLine3() {
                    return addressLine3;
                }

                /**
                 * Sets the value of the addressLine3 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddressLine3(String value) {
                    this.addressLine3 = value;
                }

                /**
                 * Gets the value of the addresscategory property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddresscategory() {
                    return addresscategory;
                }

                /**
                 * Sets the value of the addresscategory property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddresscategory(String value) {
                    this.addresscategory = value;
                }

                /**
                 * Gets the value of the buildingLevel property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBuildingLevel() {
                    return buildingLevel;
                }

                /**
                 * Sets the value of the buildingLevel property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBuildingLevel(String value) {
                    this.buildingLevel = value;
                }

                /**
                 * Gets the value of the businessCenter property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBusinessCenter() {
                    return businessCenter;
                }

                /**
                 * Sets the value of the businessCenter property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBusinessCenter(String value) {
                    this.businessCenter = value;
                }

                /**
                 * Gets the value of the cellno property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCellno() {
                    return cellno;
                }

                /**
                 * Sets the value of the cellno property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCellno(String value) {
                    this.cellno = value;
                }

                /**
                 * Gets the value of the cellnocitycode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCellnocitycode() {
                    return cellnocitycode;
                }

                /**
                 * Sets the value of the cellnocitycode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCellnocitycode(String value) {
                    this.cellnocitycode = value;
                }

                /**
                 * Gets the value of the cellnocountrycode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCellnocountrycode() {
                    return cellnocountrycode;
                }

                /**
                 * Sets the value of the cellnocountrycode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCellnocountrycode(String value) {
                    this.cellnocountrycode = value;
                }

                /**
                 * Gets the value of the cellnolocalcode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCellnolocalcode() {
                    return cellnolocalcode;
                }

                /**
                 * Sets the value of the cellnolocalcode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCellnolocalcode(String value) {
                    this.cellnolocalcode = value;
                }

                /**
                 * Gets the value of the city property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCity() {
                    return city;
                }

                /**
                 * Sets the value of the city property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCity(String value) {
                    this.city = value;
                }

                /**
                 * Gets the value of the cityCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCityCode() {
                    return cityCode;
                }

                /**
                 * Sets the value of the cityCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCityCode(String value) {
                    this.cityCode = value;
                }

                /**
                 * Gets the value of the country property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountry() {
                    return country;
                }

                /**
                 * Sets the value of the country property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountry(String value) {
                    this.country = value;
                }

                /**
                 * Gets the value of the countryCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCountryCode() {
                    return countryCode;
                }

                /**
                 * Sets the value of the countryCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCountryCode(String value) {
                    this.countryCode = value;
                }

                /**
                 * Gets the value of the domicile property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDomicile() {
                    return domicile;
                }

                /**
                 * Sets the value of the domicile property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDomicile(String value) {
                    this.domicile = value;
                }

                /**
                 * Gets the value of the endDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEndDate() {
                    return endDate;
                }

                /**
                 * Sets the value of the endDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEndDate(String value) {
                    this.endDate = value;
                }

                /**
                 * Gets the value of the faxno property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxno() {
                    return faxno;
                }

                /**
                 * Sets the value of the faxno property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxno(String value) {
                    this.faxno = value;
                }

                /**
                 * Gets the value of the faxno2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxno2() {
                    return faxno2;
                }

                /**
                 * Sets the value of the faxno2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxno2(String value) {
                    this.faxno2 = value;
                }

                /**
                 * Gets the value of the faxnocitycode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxnocitycode() {
                    return faxnocitycode;
                }

                /**
                 * Sets the value of the faxnocitycode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxnocitycode(String value) {
                    this.faxnocitycode = value;
                }

                /**
                 * Gets the value of the faxnocitycode2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxnocitycode2() {
                    return faxnocitycode2;
                }

                /**
                 * Sets the value of the faxnocitycode2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxnocitycode2(String value) {
                    this.faxnocitycode2 = value;
                }

                /**
                 * Gets the value of the faxnocountrycode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxnocountrycode() {
                    return faxnocountrycode;
                }

                /**
                 * Sets the value of the faxnocountrycode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxnocountrycode(String value) {
                    this.faxnocountrycode = value;
                }

                /**
                 * Gets the value of the faxnocountrycode2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxnocountrycode2() {
                    return faxnocountrycode2;
                }

                /**
                 * Sets the value of the faxnocountrycode2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxnocountrycode2(String value) {
                    this.faxnocountrycode2 = value;
                }

                /**
                 * Gets the value of the faxnolocalcode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxnolocalcode() {
                    return faxnolocalcode;
                }

                /**
                 * Sets the value of the faxnolocalcode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxnolocalcode(String value) {
                    this.faxnolocalcode = value;
                }

                /**
                 * Gets the value of the faxnolocalcode2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFaxnolocalcode2() {
                    return faxnolocalcode2;
                }

                /**
                 * Sets the value of the faxnolocalcode2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFaxnolocalcode2(String value) {
                    this.faxnolocalcode2 = value;
                }

                /**
                 * Gets the value of the freeTextAddress property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFreeTextAddress() {
                    return freeTextAddress;
                }

                /**
                 * Sets the value of the freeTextAddress property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFreeTextAddress(String value) {
                    this.freeTextAddress = value;
                }

                /**
                 * Gets the value of the freeTextLabel property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFreeTextLabel() {
                    return freeTextLabel;
                }

                /**
                 * Sets the value of the freeTextLabel property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFreeTextLabel(String value) {
                    this.freeTextLabel = value;
                }

                /**
                 * Gets the value of the holdMailFlag property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHoldMailFlag() {
                    return holdMailFlag;
                }

                /**
                 * Sets the value of the holdMailFlag property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHoldMailFlag(String value) {
                    this.holdMailFlag = value;
                }

                /**
                 * Gets the value of the holdMailInitiatedBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHoldMailInitiatedBy() {
                    return holdMailInitiatedBy;
                }

                /**
                 * Sets the value of the holdMailInitiatedBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHoldMailInitiatedBy(String value) {
                    this.holdMailInitiatedBy = value;
                }

                /**
                 * Gets the value of the holdMailReason property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHoldMailReason() {
                    return holdMailReason;
                }

                /**
                 * Sets the value of the holdMailReason property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHoldMailReason(String value) {
                    this.holdMailReason = value;
                }

                /**
                 * Gets the value of the houseNo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHouseNo() {
                    return houseNo;
                }

                /**
                 * Sets the value of the houseNo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHouseNo(String value) {
                    this.houseNo = value;
                }

                /**
                 * Gets the value of the localityName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocalityName() {
                    return localityName;
                }

                /**
                 * Sets the value of the localityName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocalityName(String value) {
                    this.localityName = value;
                }

                /**
                 * Gets the value of the mailstop property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMailstop() {
                    return mailstop;
                }

                /**
                 * Sets the value of the mailstop property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMailstop(String value) {
                    this.mailstop = value;
                }

                /**
                 * Gets the value of the name property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getName() {
                    return name;
                }

                /**
                 * Sets the value of the name property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setName(String value) {
                    this.name = value;
                }

                /**
                 * Gets the value of the pagerno property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPagerno() {
                    return pagerno;
                }

                /**
                 * Sets the value of the pagerno property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPagerno(String value) {
                    this.pagerno = value;
                }

                /**
                 * Gets the value of the pagernocitycode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPagernocitycode() {
                    return pagernocitycode;
                }

                /**
                 * Sets the value of the pagernocitycode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPagernocitycode(String value) {
                    this.pagernocitycode = value;
                }

                /**
                 * Gets the value of the pagernocountrycode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPagernocountrycode() {
                    return pagernocountrycode;
                }

                /**
                 * Sets the value of the pagernocountrycode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPagernocountrycode(String value) {
                    this.pagernocountrycode = value;
                }

                /**
                 * Gets the value of the pagernolocalcode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPagernolocalcode() {
                    return pagernolocalcode;
                }

                /**
                 * Sets the value of the pagernolocalcode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPagernolocalcode(String value) {
                    this.pagernolocalcode = value;
                }

                /**
                 * Gets the value of the preferredAddress property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPreferredAddress() {
                    return preferredAddress;
                }

                /**
                 * Sets the value of the preferredAddress property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPreferredAddress(String value) {
                    this.preferredAddress = value;
                }

                /**
                 * Gets the value of the preferredFormat property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPreferredFormat() {
                    return preferredFormat;
                }

                /**
                 * Sets the value of the preferredFormat property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPreferredFormat(String value) {
                    this.preferredFormat = value;
                }

                /**
                 * Gets the value of the premiseName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPremiseName() {
                    return premiseName;
                }

                /**
                 * Sets the value of the premiseName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPremiseName(String value) {
                    this.premiseName = value;
                }

                /**
                 * Gets the value of the residentialstatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResidentialstatus() {
                    return residentialstatus;
                }

                /**
                 * Sets the value of the residentialstatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResidentialstatus(String value) {
                    this.residentialstatus = value;
                }

                /**
                 * Gets the value of the salutationCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSalutationCode() {
                    return salutationCode;
                }

                /**
                 * Sets the value of the salutationCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSalutationCode(String value) {
                    this.salutationCode = value;
                }

                /**
                 * Gets the value of the startDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStartDate() {
                    return startDate;
                }

                /**
                 * Sets the value of the startDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStartDate(String value) {
                    this.startDate = value;
                }

                /**
                 * Gets the value of the state property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getState() {
                    return state;
                }

                /**
                 * Sets the value of the state property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setState(String value) {
                    this.state = value;
                }

                /**
                 * Gets the value of the streetName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStreetName() {
                    return streetName;
                }

                /**
                 * Sets the value of the streetName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStreetName(String value) {
                    this.streetName = value;
                }

                /**
                 * Gets the value of the streetNo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStreetNo() {
                    return streetNo;
                }

                /**
                 * Sets the value of the streetNo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStreetNo(String value) {
                    this.streetNo = value;
                }

                /**
                 * Gets the value of the suburb property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSuburb() {
                    return suburb;
                }

                /**
                 * Sets the value of the suburb property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSuburb(String value) {
                    this.suburb = value;
                }

                /**
                 * Gets the value of the telex property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTelex() {
                    return telex;
                }

                /**
                 * Sets the value of the telex property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTelex(String value) {
                    this.telex = value;
                }

                /**
                 * Gets the value of the telexCityCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTelexCityCode() {
                    return telexCityCode;
                }

                /**
                 * Sets the value of the telexCityCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTelexCityCode(String value) {
                    this.telexCityCode = value;
                }

                /**
                 * Gets the value of the telexCountryCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTelexCountryCode() {
                    return telexCountryCode;
                }

                /**
                 * Sets the value of the telexCountryCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTelexCountryCode(String value) {
                    this.telexCountryCode = value;
                }

                /**
                 * Gets the value of the telexLocalCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTelexLocalCode() {
                    return telexLocalCode;
                }

                /**
                 * Sets the value of the telexLocalCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTelexLocalCode(String value) {
                    this.telexLocalCode = value;
                }

                /**
                 * Gets the value of the town property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTown() {
                    return town;
                }

                /**
                 * Sets the value of the town property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTown(String value) {
                    this.town = value;
                }

                /**
                 * Gets the value of the zip property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getZip() {
                    return zip;
                }

                /**
                 * Sets the value of the zip property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setZip(String value) {
                    this.zip = value;
                }

                /**
                 * Gets the value of the email property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEmail() {
                    return email;
                }

                /**
                 * Sets the value of the email property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEmail(String value) {
                    this.email = value;
                }

                /**
                 * Gets the value of the phoneNo1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneNo1() {
                    return phoneNo1;
                }

                /**
                 * Sets the value of the phoneNo1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneNo1(String value) {
                    this.phoneNo1 = value;
                }

                /**
                 * Gets the value of the stateCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStateCode() {
                    return stateCode;
                }

                /**
                 * Sets the value of the stateCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStateCode(String value) {
                    this.stateCode = value;
                }

                /**
                 * Gets the value of the isAddressVerified property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIsAddressVerified() {
                    return isAddressVerified;
                }

                /**
                 * Sets the value of the isAddressVerified property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIsAddressVerified(String value) {
                    this.isAddressVerified = value;
                }

                /**
                 * Gets the value of the addressID property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAddressID() {
                    return addressID;
                }

                /**
                 * Sets the value of the addressID property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAddressID(String value) {
                    this.addressID = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="email">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value=""/>
             *               &lt;enumeration value="noemail@fcmb.com"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="emailpalm" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="phoneemailtype">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="COMM1"/>
             *               &lt;enumeration value="COMM2"/>
             *               &lt;enumeration value="COMML"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="phoneno">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="+234(0)0803000000"/>
             *               &lt;enumeration value="+234(0)012611862"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="phonenocitycode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="0"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="phonenocountrycode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="234"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="phonenolocalcode">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="0803000000"/>
             *               &lt;enumeration value="012611862"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="phoneoremail">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="PHONE"/>
             *               &lt;enumeration value="EMAIL"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="preferredflag">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="Y"/>
             *               &lt;enumeration value="N"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="workextension" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="phoneEmailID">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="40158193"/>
             *               &lt;enumeration value="40158194"/>
             *               &lt;enumeration value="40158195"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "email",
                "emailpalm",
                "phoneemailtype",
                "phoneno",
                "phonenocitycode",
                "phonenocountrycode",
                "phonenolocalcode",
                "phoneoremail",
                "preferredflag",
                "workextension",
                "phoneEmailID"
            })
            public static class PhoneEmailData implements Serializable{

                @XmlElement(required = true)
                protected String email;
                @XmlElement(required = true)
                protected String emailpalm;
                @XmlElement(required = true)
                protected String phoneemailtype;
                @XmlElement(required = true)
                protected String phoneno;
                @XmlElement(required = true)
                protected String phonenocitycode;
                @XmlElement(required = true)
                protected String phonenocountrycode;
                @XmlElement(required = true)
                protected String phonenolocalcode;
                @XmlElement(required = true)
                protected String phoneoremail;
                @XmlElement(required = true)
                protected String preferredflag;
                @XmlElement(required = true)
                protected String workextension;
                @XmlElement(required = true)
                protected String phoneEmailID;

                /**
                 * Gets the value of the email property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEmail() {
                    return email;
                }

                /**
                 * Sets the value of the email property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEmail(String value) {
                    this.email = value;
                }

                /**
                 * Gets the value of the emailpalm property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEmailpalm() {
                    return emailpalm;
                }

                /**
                 * Sets the value of the emailpalm property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEmailpalm(String value) {
                    this.emailpalm = value;
                }

                /**
                 * Gets the value of the phoneemailtype property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneemailtype() {
                    return phoneemailtype;
                }

                /**
                 * Sets the value of the phoneemailtype property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneemailtype(String value) {
                    this.phoneemailtype = value;
                }

                /**
                 * Gets the value of the phoneno property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneno() {
                    return phoneno;
                }

                /**
                 * Sets the value of the phoneno property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneno(String value) {
                    this.phoneno = value;
                }

                /**
                 * Gets the value of the phonenocitycode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhonenocitycode() {
                    return phonenocitycode;
                }

                /**
                 * Sets the value of the phonenocitycode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhonenocitycode(String value) {
                    this.phonenocitycode = value;
                }

                /**
                 * Gets the value of the phonenocountrycode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhonenocountrycode() {
                    return phonenocountrycode;
                }

                /**
                 * Sets the value of the phonenocountrycode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhonenocountrycode(String value) {
                    this.phonenocountrycode = value;
                }

                /**
                 * Gets the value of the phonenolocalcode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhonenolocalcode() {
                    return phonenolocalcode;
                }

                /**
                 * Sets the value of the phonenolocalcode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhonenolocalcode(String value) {
                    this.phonenolocalcode = value;
                }

                /**
                 * Gets the value of the phoneoremail property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneoremail() {
                    return phoneoremail;
                }

                /**
                 * Sets the value of the phoneoremail property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneoremail(String value) {
                    this.phoneoremail = value;
                }

                /**
                 * Gets the value of the preferredflag property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPreferredflag() {
                    return preferredflag;
                }

                /**
                 * Sets the value of the preferredflag property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPreferredflag(String value) {
                    this.preferredflag = value;
                }

                /**
                 * Gets the value of the workextension property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getWorkextension() {
                    return workextension;
                }

                /**
                 * Sets the value of the workextension property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setWorkextension(String value) {
                    this.workextension = value;
                }

                /**
                 * Gets the value of the phoneEmailID property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPhoneEmailID() {
                    return phoneEmailID;
                }

                /**
                 * Sets the value of the phoneEmailID property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPhoneEmailID(String value) {
                    this.phoneEmailID = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="countryofissue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="doccode">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="PASST"/>
         *               &lt;enumeration value="ID"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="docdelflg" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="docexpirydate">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="2021-10-20T00:00:00.000"/>
         *               &lt;enumeration value="2099-12-31T00:00:00.000"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="docissuedate">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="2011-10-21T00:00:00.000"/>
         *               &lt;enumeration value="2010-01-01T00:00:00.000"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="docremarks" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="doctypecode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="doctypedescr" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="entitytype" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="identificationtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ismandatory" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="placeofissue" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="referencenumber">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="M0051652"/>
         *               &lt;enumeration value="RC8331"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="scanrequired" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "countryofissue",
            "doccode",
            "docdelflg",
            "docexpirydate",
            "docissuedate",
            "docremarks",
            "doctypecode",
            "doctypedescr",
            "entitytype",
            "identificationtype",
            "ismandatory",
            "placeofissue",
            "referencenumber",
            "scanrequired",
            "status"
        })
        public static class EntityDocumentDetails implements Serializable{

            @XmlElement(required = true)
            protected String countryofissue;
            @XmlElement(required = true)
            protected String doccode;
            @XmlElement(required = true)
            protected String docdelflg;
            @XmlElement(required = true)
            protected String docexpirydate;
            @XmlElement(required = true)
            protected String docissuedate;
            @XmlElement(required = true)
            protected String docremarks;
            @XmlElement(required = true)
            protected String doctypecode;
            @XmlElement(required = true)
            protected String doctypedescr;
            @XmlElement(required = true)
            protected String entitytype;
            @XmlElement(required = true)
            protected String identificationtype;
            @XmlElement(required = true)
            protected String ismandatory;
            @XmlElement(required = true)
            protected String placeofissue;
            @XmlElement(required = true)
            protected String referencenumber;
            @XmlElement(required = true)
            protected String scanrequired;
            @XmlElement(required = true)
            protected String status;

            /**
             * Gets the value of the countryofissue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountryofissue() {
                return countryofissue;
            }

            /**
             * Sets the value of the countryofissue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountryofissue(String value) {
                this.countryofissue = value;
            }

            /**
             * Gets the value of the doccode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDoccode() {
                return doccode;
            }

            /**
             * Sets the value of the doccode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDoccode(String value) {
                this.doccode = value;
            }

            /**
             * Gets the value of the docdelflg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocdelflg() {
                return docdelflg;
            }

            /**
             * Sets the value of the docdelflg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocdelflg(String value) {
                this.docdelflg = value;
            }

            /**
             * Gets the value of the docexpirydate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocexpirydate() {
                return docexpirydate;
            }

            /**
             * Sets the value of the docexpirydate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocexpirydate(String value) {
                this.docexpirydate = value;
            }

            /**
             * Gets the value of the docissuedate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocissuedate() {
                return docissuedate;
            }

            /**
             * Sets the value of the docissuedate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocissuedate(String value) {
                this.docissuedate = value;
            }

            /**
             * Gets the value of the docremarks property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDocremarks() {
                return docremarks;
            }

            /**
             * Sets the value of the docremarks property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDocremarks(String value) {
                this.docremarks = value;
            }

            /**
             * Gets the value of the doctypecode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDoctypecode() {
                return doctypecode;
            }

            /**
             * Sets the value of the doctypecode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDoctypecode(String value) {
                this.doctypecode = value;
            }

            /**
             * Gets the value of the doctypedescr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDoctypedescr() {
                return doctypedescr;
            }

            /**
             * Sets the value of the doctypedescr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDoctypedescr(String value) {
                this.doctypedescr = value;
            }

            /**
             * Gets the value of the entitytype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntitytype() {
                return entitytype;
            }

            /**
             * Sets the value of the entitytype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntitytype(String value) {
                this.entitytype = value;
            }

            /**
             * Gets the value of the identificationtype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdentificationtype() {
                return identificationtype;
            }

            /**
             * Sets the value of the identificationtype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdentificationtype(String value) {
                this.identificationtype = value;
            }

            /**
             * Gets the value of the ismandatory property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIsmandatory() {
                return ismandatory;
            }

            /**
             * Sets the value of the ismandatory property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIsmandatory(String value) {
                this.ismandatory = value;
            }

            /**
             * Gets the value of the placeofissue property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPlaceofissue() {
                return placeofissue;
            }

            /**
             * Sets the value of the placeofissue property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPlaceofissue(String value) {
                this.placeofissue = value;
            }

            /**
             * Gets the value of the referencenumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getReferencenumber() {
                return referencenumber;
            }

            /**
             * Sets the value of the referencenumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setReferencenumber(String value) {
                this.referencenumber = value;
            }

            /**
             * Gets the value of the scanrequired property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getScanrequired() {
                return scanrequired;
            }

            /**
             * Sets the value of the scanrequired property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setScanrequired(String value) {
                this.scanrequired = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="business_assets" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="created_from" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="crncy_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cust_fin_year_end_mnth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cust_net_worth" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="deposits_other_bank" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="entity_create_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="entity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="financial_year" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="financial_year_end" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="invest_shares_units" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="legal_details" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="legal_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="numberof_employees" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="property" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="sharehld_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tot_share_investment" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="total_amount_held" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="type_of_statement" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "businessAssets",
            "country",
            "createdFrom",
            "crncyCode",
            "custFinYearEndMnth",
            "custNetWorth",
            "depositsOtherBank",
            "entityCreateFlg",
            "entityType",
            "financialYear",
            "financialYearEnd",
            "investSharesUnits",
            "legalDetails",
            "legalStatus",
            "numberofEmployees",
            "period",
            "property",
            "sharehldFlg",
            "totShareInvestment",
            "totalAmountHeld",
            "typeOfStatement"
        })
        public static class FinancialDet implements Serializable{

            @XmlElement(name = "business_assets", required = true)
            protected String businessAssets;
            @XmlElement(required = true)
            protected String country;
            @XmlElement(name = "created_from", required = true)
            protected String createdFrom;
            @XmlElement(name = "crncy_code", required = true)
            protected String crncyCode;
            @XmlElement(name = "cust_fin_year_end_mnth", required = true)
            protected String custFinYearEndMnth;
            @XmlElement(name = "cust_net_worth", required = true)
            protected String custNetWorth;
            @XmlElement(name = "deposits_other_bank", required = true)
            protected String depositsOtherBank;
            @XmlElement(name = "entity_create_flg", required = true)
            protected String entityCreateFlg;
            @XmlElement(name = "entity_type", required = true)
            protected String entityType;
            @XmlElement(name = "financial_year", required = true)
            protected String financialYear;
            @XmlElement(name = "financial_year_end", required = true)
            protected String financialYearEnd;
            @XmlElement(name = "invest_shares_units", required = true)
            protected String investSharesUnits;
            @XmlElement(name = "legal_details", required = true)
            protected String legalDetails;
            @XmlElement(name = "legal_status", required = true)
            protected String legalStatus;
            @XmlElement(name = "numberof_employees", required = true)
            protected String numberofEmployees;
            @XmlElement(required = true)
            protected String period;
            @XmlElement(required = true)
            protected String property;
            @XmlElement(name = "sharehld_flg", required = true)
            protected String sharehldFlg;
            @XmlElement(name = "tot_share_investment", required = true)
            protected String totShareInvestment;
            @XmlElement(name = "total_amount_held", required = true)
            protected String totalAmountHeld;
            @XmlElement(name = "type_of_statement", required = true)
            protected String typeOfStatement;

            /**
             * Gets the value of the businessAssets property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBusinessAssets() {
                return businessAssets;
            }

            /**
             * Sets the value of the businessAssets property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBusinessAssets(String value) {
                this.businessAssets = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountry() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountry(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the createdFrom property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreatedFrom() {
                return createdFrom;
            }

            /**
             * Sets the value of the createdFrom property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreatedFrom(String value) {
                this.createdFrom = value;
            }

            /**
             * Gets the value of the crncyCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCrncyCode() {
                return crncyCode;
            }

            /**
             * Sets the value of the crncyCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCrncyCode(String value) {
                this.crncyCode = value;
            }

            /**
             * Gets the value of the custFinYearEndMnth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustFinYearEndMnth() {
                return custFinYearEndMnth;
            }

            /**
             * Sets the value of the custFinYearEndMnth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustFinYearEndMnth(String value) {
                this.custFinYearEndMnth = value;
            }

            /**
             * Gets the value of the custNetWorth property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustNetWorth() {
                return custNetWorth;
            }

            /**
             * Sets the value of the custNetWorth property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustNetWorth(String value) {
                this.custNetWorth = value;
            }

            /**
             * Gets the value of the depositsOtherBank property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDepositsOtherBank() {
                return depositsOtherBank;
            }

            /**
             * Sets the value of the depositsOtherBank property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDepositsOtherBank(String value) {
                this.depositsOtherBank = value;
            }

            /**
             * Gets the value of the entityCreateFlg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityCreateFlg() {
                return entityCreateFlg;
            }

            /**
             * Sets the value of the entityCreateFlg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityCreateFlg(String value) {
                this.entityCreateFlg = value;
            }

            /**
             * Gets the value of the entityType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityType() {
                return entityType;
            }

            /**
             * Sets the value of the entityType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityType(String value) {
                this.entityType = value;
            }

            /**
             * Gets the value of the financialYear property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFinancialYear() {
                return financialYear;
            }

            /**
             * Sets the value of the financialYear property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFinancialYear(String value) {
                this.financialYear = value;
            }

            /**
             * Gets the value of the financialYearEnd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFinancialYearEnd() {
                return financialYearEnd;
            }

            /**
             * Sets the value of the financialYearEnd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFinancialYearEnd(String value) {
                this.financialYearEnd = value;
            }

            /**
             * Gets the value of the investSharesUnits property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getInvestSharesUnits() {
                return investSharesUnits;
            }

            /**
             * Sets the value of the investSharesUnits property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setInvestSharesUnits(String value) {
                this.investSharesUnits = value;
            }

            /**
             * Gets the value of the legalDetails property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLegalDetails() {
                return legalDetails;
            }

            /**
             * Sets the value of the legalDetails property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLegalDetails(String value) {
                this.legalDetails = value;
            }

            /**
             * Gets the value of the legalStatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLegalStatus() {
                return legalStatus;
            }

            /**
             * Sets the value of the legalStatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLegalStatus(String value) {
                this.legalStatus = value;
            }

            /**
             * Gets the value of the numberofEmployees property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumberofEmployees() {
                return numberofEmployees;
            }

            /**
             * Sets the value of the numberofEmployees property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumberofEmployees(String value) {
                this.numberofEmployees = value;
            }

            /**
             * Gets the value of the period property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPeriod() {
                return period;
            }

            /**
             * Sets the value of the period property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPeriod(String value) {
                this.period = value;
            }

            /**
             * Gets the value of the property property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProperty() {
                return property;
            }

            /**
             * Sets the value of the property property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProperty(String value) {
                this.property = value;
            }

            /**
             * Gets the value of the sharehldFlg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSharehldFlg() {
                return sharehldFlg;
            }

            /**
             * Sets the value of the sharehldFlg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSharehldFlg(String value) {
                this.sharehldFlg = value;
            }

            /**
             * Gets the value of the totShareInvestment property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTotShareInvestment() {
                return totShareInvestment;
            }

            /**
             * Sets the value of the totShareInvestment property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTotShareInvestment(String value) {
                this.totShareInvestment = value;
            }

            /**
             * Gets the value of the totalAmountHeld property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTotalAmountHeld() {
                return totalAmountHeld;
            }

            /**
             * Sets the value of the totalAmountHeld property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTotalAmountHeld(String value) {
                this.totalAmountHeld = value;
            }

            /**
             * Gets the value of the typeOfStatement property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTypeOfStatement() {
                return typeOfStatement;
            }

            /**
             * Sets the value of the typeOfStatement property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTypeOfStatement(String value) {
                this.typeOfStatement = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="calendar_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="combined_stmt_reqd" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="combined_stmtflag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cust_floor_limit_tds" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="do_not_email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="do_not_mail" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="do_not_phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="entity_create_flg" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="entity_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="frequency" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="frequency_holidaystatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="frequency_weekday" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="holdmail_flag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="loansstatementtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="mode_of_dispatch" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="prefCorpmiscInfo" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="amount1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="amount2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="amount3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="amount4" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="date1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="int1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str1">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="NGN"/>
         *                         &lt;enumeration value="USD"/>
         *                         &lt;enumeration value="EUR"/>
         *                         &lt;enumeration value="GBP"/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="str10" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str11" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str2">
         *                     &lt;simpleType>
         *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *                         &lt;enumeration value="0131297"/>
         *                         &lt;enumeration value=""/>
         *                       &lt;/restriction>
         *                     &lt;/simpleType>
         *                   &lt;/element>
         *                   &lt;element name="str3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str4" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str5" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str6" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str7" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str8" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="str9" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="preferred_channel" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="preffered_channel_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="statement_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tds_exmpt_ref_num" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tds_exmpt_rmks" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tdsstatementtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="External_System_Pricing" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Relationship_Pricing_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Tax_Rate_Table_Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "calendarType",
            "combinedStmtReqd",
            "combinedStmtflag",
            "custFloorLimitTds",
            "description",
            "doNotEmail",
            "doNotMail",
            "doNotPhone",
            "entityCreateFlg",
            "entityType",
            "frequency",
            "frequencyHolidaystatus",
            "frequencyWeekday",
            "holdmailFlag",
            "loansstatementtype",
            "modeOfDispatch",
            "prefCorpmiscInfo",
            "preferredChannel",
            "prefferedChannelCode",
            "statementType",
            "tdsExmptRefNum",
            "tdsExmptRmks",
            "tdsstatementtype",
            "externalSystemPricing",
            "relationshipPricingID",
            "taxRateTableCode"
        })
        public static class PreferencesDet implements Serializable{

            @XmlElement(name = "calendar_type", required = true)
            protected String calendarType;
            @XmlElement(name = "combined_stmt_reqd", required = true)
            protected String combinedStmtReqd;
            @XmlElement(name = "combined_stmtflag", required = true)
            protected String combinedStmtflag;
            @XmlElement(name = "cust_floor_limit_tds", required = true)
            protected String custFloorLimitTds;
            @XmlElement(required = true)
            protected String description;
            @XmlElement(name = "do_not_email", required = true)
            protected String doNotEmail;
            @XmlElement(name = "do_not_mail", required = true)
            protected String doNotMail;
            @XmlElement(name = "do_not_phone", required = true)
            protected String doNotPhone;
            @XmlElement(name = "entity_create_flg", required = true)
            protected String entityCreateFlg;
            @XmlElement(name = "entity_type", required = true)
            protected String entityType;
            @XmlElement(required = true)
            protected String frequency;
            @XmlElement(name = "frequency_holidaystatus", required = true)
            protected String frequencyHolidaystatus;
            @XmlElement(name = "frequency_weekday", required = true)
            protected String frequencyWeekday;
            @XmlElement(name = "holdmail_flag", required = true)
            protected String holdmailFlag;
            @XmlElement(required = true)
            protected String loansstatementtype;
            @XmlElement(name = "mode_of_dispatch", required = true)
            protected String modeOfDispatch;
            protected List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet.PrefCorpmiscInfo> prefCorpmiscInfo;
            @XmlElement(name = "preferred_channel", required = true)
            protected String preferredChannel;
            @XmlElement(name = "preffered_channel_code", required = true)
            protected String prefferedChannelCode;
            @XmlElement(name = "statement_type", required = true)
            protected String statementType;
            @XmlElement(name = "tds_exmpt_ref_num", required = true)
            protected String tdsExmptRefNum;
            @XmlElement(name = "tds_exmpt_rmks", required = true)
            protected String tdsExmptRmks;
            @XmlElement(required = true)
            protected String tdsstatementtype;
            @XmlElement(name = "External_System_Pricing", required = true)
            protected String externalSystemPricing;
            @XmlElement(name = "Relationship_Pricing_ID", required = true)
            protected String relationshipPricingID;
            @XmlElement(name = "Tax_Rate_Table_Code", required = true)
            protected String taxRateTableCode;

            /**
             * Gets the value of the calendarType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCalendarType() {
                return calendarType;
            }

            /**
             * Sets the value of the calendarType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCalendarType(String value) {
                this.calendarType = value;
            }

            /**
             * Gets the value of the combinedStmtReqd property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCombinedStmtReqd() {
                return combinedStmtReqd;
            }

            /**
             * Sets the value of the combinedStmtReqd property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCombinedStmtReqd(String value) {
                this.combinedStmtReqd = value;
            }

            /**
             * Gets the value of the combinedStmtflag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCombinedStmtflag() {
                return combinedStmtflag;
            }

            /**
             * Sets the value of the combinedStmtflag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCombinedStmtflag(String value) {
                this.combinedStmtflag = value;
            }

            /**
             * Gets the value of the custFloorLimitTds property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustFloorLimitTds() {
                return custFloorLimitTds;
            }

            /**
             * Sets the value of the custFloorLimitTds property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustFloorLimitTds(String value) {
                this.custFloorLimitTds = value;
            }

            /**
             * Gets the value of the description property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Sets the value of the description property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Gets the value of the doNotEmail property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDoNotEmail() {
                return doNotEmail;
            }

            /**
             * Sets the value of the doNotEmail property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDoNotEmail(String value) {
                this.doNotEmail = value;
            }

            /**
             * Gets the value of the doNotMail property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDoNotMail() {
                return doNotMail;
            }

            /**
             * Sets the value of the doNotMail property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDoNotMail(String value) {
                this.doNotMail = value;
            }

            /**
             * Gets the value of the doNotPhone property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDoNotPhone() {
                return doNotPhone;
            }

            /**
             * Sets the value of the doNotPhone property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDoNotPhone(String value) {
                this.doNotPhone = value;
            }

            /**
             * Gets the value of the entityCreateFlg property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityCreateFlg() {
                return entityCreateFlg;
            }

            /**
             * Sets the value of the entityCreateFlg property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityCreateFlg(String value) {
                this.entityCreateFlg = value;
            }

            /**
             * Gets the value of the entityType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEntityType() {
                return entityType;
            }

            /**
             * Sets the value of the entityType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEntityType(String value) {
                this.entityType = value;
            }

            /**
             * Gets the value of the frequency property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFrequency() {
                return frequency;
            }

            /**
             * Sets the value of the frequency property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFrequency(String value) {
                this.frequency = value;
            }

            /**
             * Gets the value of the frequencyHolidaystatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFrequencyHolidaystatus() {
                return frequencyHolidaystatus;
            }

            /**
             * Sets the value of the frequencyHolidaystatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFrequencyHolidaystatus(String value) {
                this.frequencyHolidaystatus = value;
            }

            /**
             * Gets the value of the frequencyWeekday property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFrequencyWeekday() {
                return frequencyWeekday;
            }

            /**
             * Sets the value of the frequencyWeekday property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFrequencyWeekday(String value) {
                this.frequencyWeekday = value;
            }

            /**
             * Gets the value of the holdmailFlag property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHoldmailFlag() {
                return holdmailFlag;
            }

            /**
             * Sets the value of the holdmailFlag property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHoldmailFlag(String value) {
                this.holdmailFlag = value;
            }

            /**
             * Gets the value of the loansstatementtype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLoansstatementtype() {
                return loansstatementtype;
            }

            /**
             * Sets the value of the loansstatementtype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLoansstatementtype(String value) {
                this.loansstatementtype = value;
            }

            /**
             * Gets the value of the modeOfDispatch property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getModeOfDispatch() {
                return modeOfDispatch;
            }

            /**
             * Sets the value of the modeOfDispatch property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setModeOfDispatch(String value) {
                this.modeOfDispatch = value;
            }

            /**
             * Gets the value of the prefCorpmiscInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the prefCorpmiscInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPrefCorpmiscInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet.PrefCorpmiscInfo }
             * 
             * 
             */
            public List<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet.PrefCorpmiscInfo> getPrefCorpmiscInfo() {
                if (prefCorpmiscInfo == null) {
                    prefCorpmiscInfo = new ArrayList<GetCorporateCustomerDetailsResponse.CorporateCustomerDetails.PreferencesDet.PrefCorpmiscInfo>();
                }
                return this.prefCorpmiscInfo;
            }

            /**
             * Gets the value of the preferredChannel property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPreferredChannel() {
                return preferredChannel;
            }

            /**
             * Sets the value of the preferredChannel property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPreferredChannel(String value) {
                this.preferredChannel = value;
            }

            /**
             * Gets the value of the prefferedChannelCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrefferedChannelCode() {
                return prefferedChannelCode;
            }

            /**
             * Sets the value of the prefferedChannelCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrefferedChannelCode(String value) {
                this.prefferedChannelCode = value;
            }

            /**
             * Gets the value of the statementType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatementType() {
                return statementType;
            }

            /**
             * Sets the value of the statementType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatementType(String value) {
                this.statementType = value;
            }

            /**
             * Gets the value of the tdsExmptRefNum property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTdsExmptRefNum() {
                return tdsExmptRefNum;
            }

            /**
             * Sets the value of the tdsExmptRefNum property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTdsExmptRefNum(String value) {
                this.tdsExmptRefNum = value;
            }

            /**
             * Gets the value of the tdsExmptRmks property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTdsExmptRmks() {
                return tdsExmptRmks;
            }

            /**
             * Sets the value of the tdsExmptRmks property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTdsExmptRmks(String value) {
                this.tdsExmptRmks = value;
            }

            /**
             * Gets the value of the tdsstatementtype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTdsstatementtype() {
                return tdsstatementtype;
            }

            /**
             * Sets the value of the tdsstatementtype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTdsstatementtype(String value) {
                this.tdsstatementtype = value;
            }

            /**
             * Gets the value of the externalSystemPricing property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExternalSystemPricing() {
                return externalSystemPricing;
            }

            /**
             * Sets the value of the externalSystemPricing property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExternalSystemPricing(String value) {
                this.externalSystemPricing = value;
            }

            /**
             * Gets the value of the relationshipPricingID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationshipPricingID() {
                return relationshipPricingID;
            }

            /**
             * Sets the value of the relationshipPricingID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationshipPricingID(String value) {
                this.relationshipPricingID = value;
            }

            /**
             * Gets the value of the taxRateTableCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxRateTableCode() {
                return taxRateTableCode;
            }

            /**
             * Sets the value of the taxRateTableCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxRateTableCode(String value) {
                this.taxRateTableCode = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="amount1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="amount2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="amount3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="amount4" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="date1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="int1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str1">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="NGN"/>
             *               &lt;enumeration value="USD"/>
             *               &lt;enumeration value="EUR"/>
             *               &lt;enumeration value="GBP"/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="str10" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str11" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str2">
             *           &lt;simpleType>
             *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
             *               &lt;enumeration value="0131297"/>
             *               &lt;enumeration value=""/>
             *             &lt;/restriction>
             *           &lt;/simpleType>
             *         &lt;/element>
             *         &lt;element name="str3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str4" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str5" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str6" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str7" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str8" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="str9" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "amount1",
                "amount2",
                "amount3",
                "amount4",
                "date1",
                "int1",
                "str1",
                "str10",
                "str11",
                "str2",
                "str3",
                "str4",
                "str5",
                "str6",
                "str7",
                "str8",
                "str9",
                "type"
            })
            public static class PrefCorpmiscInfo implements Serializable{

                @XmlElement(required = true)
                protected String amount1;
                @XmlElement(required = true)
                protected String amount2;
                @XmlElement(required = true)
                protected String amount3;
                @XmlElement(required = true)
                protected String amount4;
                @XmlElement(required = true)
                protected String date1;
                @XmlElement(required = true)
                protected String int1;
                @XmlElement(required = true)
                protected String str1;
                @XmlElement(required = true)
                protected String str10;
                @XmlElement(required = true)
                protected String str11;
                @XmlElement(required = true)
                protected String str2;
                @XmlElement(required = true)
                protected String str3;
                @XmlElement(required = true)
                protected String str4;
                @XmlElement(required = true)
                protected String str5;
                @XmlElement(required = true)
                protected String str6;
                @XmlElement(required = true)
                protected String str7;
                @XmlElement(required = true)
                protected String str8;
                @XmlElement(required = true)
                protected String str9;
                @XmlElement(required = true)
                protected String type;

                /**
                 * Gets the value of the amount1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount1() {
                    return amount1;
                }

                /**
                 * Sets the value of the amount1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount1(String value) {
                    this.amount1 = value;
                }

                /**
                 * Gets the value of the amount2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount2() {
                    return amount2;
                }

                /**
                 * Sets the value of the amount2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount2(String value) {
                    this.amount2 = value;
                }

                /**
                 * Gets the value of the amount3 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount3() {
                    return amount3;
                }

                /**
                 * Sets the value of the amount3 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount3(String value) {
                    this.amount3 = value;
                }

                /**
                 * Gets the value of the amount4 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAmount4() {
                    return amount4;
                }

                /**
                 * Sets the value of the amount4 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAmount4(String value) {
                    this.amount4 = value;
                }

                /**
                 * Gets the value of the date1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDate1() {
                    return date1;
                }

                /**
                 * Sets the value of the date1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDate1(String value) {
                    this.date1 = value;
                }

                /**
                 * Gets the value of the int1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getInt1() {
                    return int1;
                }

                /**
                 * Sets the value of the int1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setInt1(String value) {
                    this.int1 = value;
                }

                /**
                 * Gets the value of the str1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr1() {
                    return str1;
                }

                /**
                 * Sets the value of the str1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr1(String value) {
                    this.str1 = value;
                }

                /**
                 * Gets the value of the str10 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr10() {
                    return str10;
                }

                /**
                 * Sets the value of the str10 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr10(String value) {
                    this.str10 = value;
                }

                /**
                 * Gets the value of the str11 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr11() {
                    return str11;
                }

                /**
                 * Sets the value of the str11 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr11(String value) {
                    this.str11 = value;
                }

                /**
                 * Gets the value of the str2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr2() {
                    return str2;
                }

                /**
                 * Sets the value of the str2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr2(String value) {
                    this.str2 = value;
                }

                /**
                 * Gets the value of the str3 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr3() {
                    return str3;
                }

                /**
                 * Sets the value of the str3 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr3(String value) {
                    this.str3 = value;
                }

                /**
                 * Gets the value of the str4 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr4() {
                    return str4;
                }

                /**
                 * Sets the value of the str4 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr4(String value) {
                    this.str4 = value;
                }

                /**
                 * Gets the value of the str5 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr5() {
                    return str5;
                }

                /**
                 * Sets the value of the str5 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr5(String value) {
                    this.str5 = value;
                }

                /**
                 * Gets the value of the str6 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr6() {
                    return str6;
                }

                /**
                 * Sets the value of the str6 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr6(String value) {
                    this.str6 = value;
                }

                /**
                 * Gets the value of the str7 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr7() {
                    return str7;
                }

                /**
                 * Sets the value of the str7 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr7(String value) {
                    this.str7 = value;
                }

                /**
                 * Gets the value of the str8 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr8() {
                    return str8;
                }

                /**
                 * Sets the value of the str8 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr8(String value) {
                    this.str8 = value;
                }

                /**
                 * Gets the value of the str9 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStr9() {
                    return str9;
                }

                /**
                 * Sets the value of the str9 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStr9(String value) {
                    this.str9 = value;
                }

                /**
                 * Gets the value of the type property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Sets the value of the type property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

            }

        }

    }

}

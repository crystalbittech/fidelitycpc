package com.longbridge.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Created by Timmy on 4/10/2017.
 */
@Entity
@Table(name = "Fid_Update_WorkFlow",schema = "CUSTOM")
public class FidUpdateWorkFlow {

    private String accntId;
    private byte[] updateValue;
    @Id
    @Column(name = "accnt_id", nullable = false, insertable = true, updatable = true)
    public String getAccntId() {
        return accntId;
    }

    public void setAccntId(String accntId) {
        this.accntId = accntId;
    }
    @Column(name = "update_value", nullable = false, insertable = true, updatable = true)
    public byte[] getUpdateValue() {
        return updateValue;
    }

    public void setUpdateValue(byte[] updateValue) {
        this.updateValue = updateValue;
    }

    @Override
    public String toString() {
        return "FidUpdateWorkFlow{" +
                "accntId='" + accntId + '\'' +
                ", updateValue=" + Arrays.toString(updateValue) +
                '}';
    }
}

package com.longbridge.entities;

import com.longbridge.FI.FIGenAcctInq.AcctInqResponse;
import com.longbridge.FI.FiGenCustInq.CustInqResponse;

import java.io.Serializable;

/**
 * Created by LB-PRJ-020 on 8/2/2017.
 */
public class CustomerDetailsForInquiry implements Serializable{

    private AcctInqResponse acctInqResponse;
    private CustInqResponse custInqResponse;
//    protected static CustomerDetailsForInquiry.HACM hacm;
//    protected static CustomerDetailsForInquiry.CRM crm;

//    public HACM getHacm() {
//        return hacm;
//    }
//
//    public void setHacm(HACM hacm) {
//        this.hacm = hacm;
//    }
//
//    public CRM getCrm() {
//        return crm;
//    }
//
//    public void setCrm(CRM crm) {
//        this.crm = crm;
//    }

//    public static class HACM{
//        protected static General general;
////        public General getGeneral() {
////            return general;
////        }
////
////        public void setGeneral(General general) {
////            this.general = general;
////        }
//        public static class General{
//            protected String accShortName;
//            protected String accOpnDate;
//            protected String chrgLvlcode;
//            protected String modeOfOpr;
//            protected String locCode;
//            protected String acRepCode;
//            protected String channLvlCode;
//            protected String custReltnCode;
//            protected String accMgerID;
//            protected String ledgerNum;
//            protected String contactPhnNum;
//            protected String cashDebitLimitExp;
//            protected String cashCreditLimitExp;
//            protected String closurExpLimit;
//            protected String clrExpLimit;
//            protected String tranExpLimit;
//            protected String preflangCode;
//            protected String nameInPrefLang;
//
//            public String getAccShortName() {
//                return accShortName;
//            }
//
//            public void setAccShortName(String accShortName) {
//                this.accShortName = accShortName;
//            }
//
//            public String getAccOpnDate() {
//                return accOpnDate;
//            }
//
//            public void setAccOpnDate(String accOpnDate) {
//                this.accOpnDate = accOpnDate;
//            }
//
//            public String getChrgLvlcode() {
//                return chrgLvlcode;
//            }
//
//            public void setChrgLvlcode(String chrgLvlcode) {
//                this.chrgLvlcode = chrgLvlcode;
//            }
//
//            public String getModeOfOpr() {
//                return modeOfOpr;
//            }
//
//            public void setModeOfOpr(String modeOfOpr) {
//                this.modeOfOpr = modeOfOpr;
//            }
//
//            public String getLocCode() {
//                return locCode;
//            }
//
//            public void setLocCode(String locCode) {
//                this.locCode = locCode;
//            }
//
//            public String getAcRepCode() {
//                return acRepCode;
//            }
//
//            public void setAcRepCode(String acRepCode) {
//                this.acRepCode = acRepCode;
//            }
//
//            public String getChannLvlCode() {
//                return channLvlCode;
//            }
//
//            public void setChannLvlCode(String channLvlCode) {
//                this.channLvlCode = channLvlCode;
//            }
//
//            public String getCustReltnCode() {
//                return custReltnCode;
//            }
//
//            public void setCustReltnCode(String custReltnCode) {
//                this.custReltnCode = custReltnCode;
//            }
//
//            public String getAccMgerID() {
//                return accMgerID;
//            }
//
//            public void setAccMgerID(String accMgerID) {
//                this.accMgerID = accMgerID;
//            }
//
//            public String getLedgerNum() {
//                return ledgerNum;
//            }
//
//            public void setLedgerNum(String ledgerNum) {
//                this.ledgerNum = ledgerNum;
//            }
//
//            public String getContactPhnNum() {
//                return contactPhnNum;
//            }
//
//            public void setContactPhnNum(String contactPhnNum) {
//                this.contactPhnNum = contactPhnNum;
//            }
//
//            public String getCashDebitLimitExp() {
//                return cashDebitLimitExp;
//            }
//
//            public void setCashDebitLimitExp(String cashDebitLimitExp) {
//                this.cashDebitLimitExp = cashDebitLimitExp;
//            }
//
//            public String getCashCreditLimitExp() {
//                return cashCreditLimitExp;
//            }
//
//            public void setCashCreditLimitExp(String cashCreditLimitExp) {
//                this.cashCreditLimitExp = cashCreditLimitExp;
//            }
//
//            public String getClosurExpLimit() {
//                return closurExpLimit;
//            }
//
//            public void setClosurExpLimit(String closurExpLimit) {
//                this.closurExpLimit = closurExpLimit;
//            }
//
//            public String getClrExpLimit() {
//                return clrExpLimit;
//            }
//
//            public void setClrExpLimit(String clrExpLimit) {
//                this.clrExpLimit = clrExpLimit;
//            }
//
//            public String getTranExpLimit() {
//                return tranExpLimit;
//            }
//
//            public void setTranExpLimit(String tranExpLimit) {
//                this.tranExpLimit = tranExpLimit;
//            }
//
//            public String getPreflangCode() {
//                return preflangCode;
//            }
//
//            public void setPreflangCode(String preflangCode) {
//                this.preflangCode = preflangCode;
//            }
//
//            public String getNameInPrefLang() {
//                return nameInPrefLang;
//            }
//
//            public void setNameInPrefLang(String nameInPrefLang) {
//                this.nameInPrefLang = nameInPrefLang;
//            }
//
//
//
//        }
//    }
//
//    public static  class CRM{
//
//    }

    public CustomerDetailsForInquiry(AcctInqResponse acctInqResponse,CustInqResponse custInqResponse) {
        this.acctInqResponse = acctInqResponse;
        this.custInqResponse = custInqResponse;
//        hacm = new HACM();
//        hacm.general = new HACM.General();
//        hacm.general.setAccShortName(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setAccMgerID(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setAccOpnDate(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setAcRepCode(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setCashCreditLimitExp(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setCashDebitLimitExp(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setChannLvlCode(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setClosurExpLimit(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setClrExpLimit(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setCustReltnCode(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setLedgerNum(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setLocCode(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setModeOfOpr(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setNameInPrefLang(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setPreflangCode(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
//        hacm.general.setTranExpLimit(acctInqResponse.getAcctInqRs().getAcctId().getAcctId());
    }

    public CustomerDetailsForInquiry() {

    }

//    public AcctInqResponse getAcctInqResponse() {
//        return acctInqResponse;
//    }

//    public void setAcctInqResponse(AcctInqResponse acctInqResponse) {
//        this.acctInqResponse = acctInqResponse;
//    }

//    public CustInqResponse getCustInqResponse() {
//        return custInqResponse;
//    }

//    public void setCustInqResponse(CustInqResponse custInqResponse) {
//        this.custInqResponse = custInqResponse;
//    }


    public AcctInqResponse getAcctInqResponse() {
        return acctInqResponse;
    }

    public void setAcctInqResponse(AcctInqResponse acctInqResponse) {
        this.acctInqResponse = acctInqResponse;
    }

    public CustInqResponse getCustInqResponse() {
        return custInqResponse;
    }

    public void setCustInqResponse(CustInqResponse custInqResponse) {
        this.custInqResponse = custInqResponse;
    }

    @Override
    public String toString() {
        return "CustomerDetailsForInquiry{" +
                "acctInqResponse=" + acctInqResponse +
                ", custInqResponse=" + custInqResponse +
                '}';
    }

}

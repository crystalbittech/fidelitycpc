package com.longbridge.entities.FinacleEntities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GENERAL_ACCT_MAST_TABLE",schema = "TBAADM")
public class GAM {
  @Id
  private String foracid;
  private String acid;
  private String entity_cre_flg;
  private String del_flg;
  private String sol_id;
  private String acct_prefix;
  private String acct_num;
  private String bacid;
  private String acct_name;
  private String acct_short_name;
  private String cust_id;
  private String emp_id;
  private String gl_sub_head_code;
  private String acct_ownership;
  private String schm_code;
  private String dr_bal_lim;
  private String acct_rpt_code;
  private String frez_code;
  private String frez_reason_code;
  private java.sql.Date acct_opn_date;
  private String acct_cls_flg;
  private java.sql.Date acct_cls_date;
  private String clr_bal_amt;
  private String tot_mod_times;
  private String ledg_num;
  private String un_clr_bal_amt;
  private String drwng_power;
  private String sanct_lim;
  private String adhoc_lim;
  private String emer_advn;
  private String dacc_lim;
  private String system_reserved_amt;
  private String single_tran_lim;
  private String clean_adhoc_lim;
  private String clean_emer_advn;
  private String clean_single_tran_lim;
  private String system_gen_lim;
  private String chq_alwd_flg;
  private String cash_excp_amt_lim;
  private String clg_excp_amt_lim;
  private String xfer_excp_amt_lim;
  private String cash_cr_excp_amt_lim;
  private String clg_cr_excp_amt_lim;
  private String xfer_cr_excp_amt_lim;
  private String cash_abnrml_amt_lim;
  private String clg_abnrml_amt_lim;
  private String xfer_abnrml_amt_lim;
  private String cum_dr_amt;
  private String cum_cr_amt;
  private String acrd_cr_amt;
  private java.sql.Date last_tran_date;
  private String mode_of_oper_code;
  private String pb_ps_code;
  private String serv_chrg_coll_flg;
  private String free_text;
  private String acct_turnover_det_flg;
  private String nom_available_flg;
  private String acct_locn_code;
  private java.sql.Date last_purge_date;
  private String bal_on_purge_date;
  private String int_paid_flg;
  private String int_coll_flg;
  private java.sql.Date last_any_tran_date;
  private String hashed_no;
  private String lchg_user_id;
  private java.sql.Date lchg_time;
  private String rcre_user_id;
  private java.sql.Date rcre_time;
  private String limit_b2kid;
  private String drwng_power_ind;
  private String drwng_power_pcnt;
  private String micr_chq_chrg_coll_flg;
  private java.sql.Date last_turnover_date;
  private String notional_rate;
  private String notional_rate_code;
  private String fx_clr_bal_amt;
  private String fx_bal_on_purge_date;
  private String fd_ref_num;
  private String fx_cum_cr_amt;
  private String fx_cum_dr_amt;
  private String crncy_code;
  private String source_of_fund;
  private String anw_non_cust_alwd_flg;
  private String acct_crncy_code;
  private String lien_amt;
  private String acct_classification_flg;
  private String system_only_acct_flg;
  private String single_tran_flg;
  private String utilised_amt;
  private String inter_sol_access_flg;
  private String purge_allowed_flg;
  private String purge_text;
  private java.sql.Date min_value_date;
  private String acct_mgr_user_id;
  private String schm_type;
  private java.sql.Date last_frez_date;
  private java.sql.Date last_unfrez_date;
  private String bal_on_frez_date;
  private String swift_allowed_flg;
  private String dacc_lim_pcnt;
  private String dacc_lim_abs;
  private String chrg_level_code;
  private String acct_cls_chrg_pend_verf;
  private String partitioned_flg;
  private String partitioned_type;
  private String pbf_download_flg;
  private java.sql.Date pbf_delink_date;
  private String wtax_flg;
  private String wtax_amount_scope_flg;
  private String int_adj_for_deduction_flg;
  private String operative_acid;
  private String phone_num;
  private String native_lang_name;
  private String nat_lang_title_code;
  private String lang_code;
  private String ts_cnt;
  private String pool_id;
  private String allow_sweeps;
  private String order_of_utilisation;
  private String wtax_pcnt;
  private String wtax_floor_limit;
  private String dsa_penal_flg;
  private String product_group;
  private String source_deal_code;
  private String disburse_deal_code;
  private String sweep_in_min_bal;
  private String used_single_tran_lim;
  private String used_clean_single_tran_lim;
  private String used_un_clr_over_dacc_amt;
  private String ffd_contrib_to_acct;
  private String acct_creation_mode;
  private String wtax_level_flg;
  private java.sql.Date last_modified_date;
  private String cif_id;
  private String future_bal_amt;
  private String util_future_bal_amt;
  private String dafa_lim;
  private String dafa_lim_abs;
  private String dafa_lim_pcnt;
  private String future_oc_tod_amt;
  private String used_oc_cln_single_tran_lim;
  private String iban_number;
  private String channel_level_code;
  private String channel_id;
  private String future_clr_bal_amt;
  private String future_un_clr_bal_amt;
  private String master_b2k_id;
  private String bank_id;
  private String frez_reason_code_2;
  private String frez_reason_code_3;
  private String frez_reason_code_4;
  private String frez_reason_code_5;
  private String is_loan_agnst_cltrl_flg;
  private String preferred_cal_base;
  private String additional_cal_base;
  private String mud_pool_acct_flg;
  private String alt1_acct_short_name;
  private String alt1_acct_name;
  private String external_pricing_flg;
  private String acct_mod_flg;
  private String wtax_floor_limit_freq;
  private String chq_prov_by_bank_flg;
  private String chq_val_prd_mnth;
  private String chq_val_prd_day;
  private String pymnt_ref_num;
  private String ra_ref_num;
  private String asynch_bal_upd_flg;
  private java.sql.Date last_tran_date_cr;
  private java.sql.Date last_tran_date_dr;
  private String last_tran_id_cr;
  private String last_tran_id_dr;
  private String dr_int_method;
  private String cons_bal_flg;
  private String schm_sub_type;
  private String ssa_plan_id;
  private java.sql.Date wtax_waiver_start_date;
  private java.sql.Date wtax_waiver_end_date;
  private String tax_event_reason_code;
  private String wtax_floor_limit_scope_flg;

  public String getAcid() {
    return acid;
  }

  public void setAcid(String acid) {
    this.acid = acid;
  }

  public String getEntity_cre_flg() {
    return entity_cre_flg;
  }

  public void setEntity_cre_flg(String entity_cre_flg) {
    this.entity_cre_flg = entity_cre_flg;
  }

  public String getDel_flg() {
    return del_flg;
  }

  public void setDel_flg(String del_flg) {
    this.del_flg = del_flg;
  }

  public String getSol_id() {
    return sol_id;
  }

  public void setSol_id(String sol_id) {
    this.sol_id = sol_id;
  }

  public String getAcct_prefix() {
    return acct_prefix;
  }

  public void setAcct_prefix(String acct_prefix) {
    this.acct_prefix = acct_prefix;
  }

  public String getAcct_num() {
    return acct_num;
  }

  public void setAcct_num(String acct_num) {
    this.acct_num = acct_num;
  }

  public String getBacid() {
    return bacid;
  }

  public void setBacid(String bacid) {
    this.bacid = bacid;
  }

  public String getForacid() {
    return foracid;
  }

  public void setForacid(String foracid) {
    this.foracid = foracid;
  }

  public String getAcct_name() {
    return acct_name;
  }

  public void setAcct_name(String acct_name) {
    this.acct_name = acct_name;
  }

  public String getAcct_short_name() {
    return acct_short_name;
  }

  public void setAcct_short_name(String acct_short_name) {
    this.acct_short_name = acct_short_name;
  }

  public String getCust_id() {
    return cust_id;
  }

  public void setCust_id(String cust_id) {
    this.cust_id = cust_id;
  }

  public String getEmp_id() {
    return emp_id;
  }

  public void setEmp_id(String emp_id) {
    this.emp_id = emp_id;
  }

  public String getGl_sub_head_code() {
    return gl_sub_head_code;
  }

  public void setGl_sub_head_code(String gl_sub_head_code) {
    this.gl_sub_head_code = gl_sub_head_code;
  }

  public String getAcct_ownership() {
    return acct_ownership;
  }

  public void setAcct_ownership(String acct_ownership) {
    this.acct_ownership = acct_ownership;
  }

  public String getSchm_code() {
    return schm_code;
  }

  public void setSchm_code(String schm_code) {
    this.schm_code = schm_code;
  }

  public String getDr_bal_lim() {
    return dr_bal_lim;
  }

  public void setDr_bal_lim(String dr_bal_lim) {
    this.dr_bal_lim = dr_bal_lim;
  }

  public String getAcct_rpt_code() {
    return acct_rpt_code;
  }

  public void setAcct_rpt_code(String acct_rpt_code) {
    this.acct_rpt_code = acct_rpt_code;
  }

  public String getFrez_code() {
    return frez_code;
  }

  public void setFrez_code(String frez_code) {
    this.frez_code = frez_code;
  }

  public String getFrez_reason_code() {
    return frez_reason_code;
  }

  public void setFrez_reason_code(String frez_reason_code) {
    this.frez_reason_code = frez_reason_code;
  }

  public java.sql.Date getAcct_opn_date() {
    return acct_opn_date;
  }

  public void setAcct_opn_date(java.sql.Date acct_opn_date) {
    this.acct_opn_date = acct_opn_date;
  }

  public String getAcct_cls_flg() {
    return acct_cls_flg;
  }

  public void setAcct_cls_flg(String acct_cls_flg) {
    this.acct_cls_flg = acct_cls_flg;
  }

  public java.sql.Date getAcct_cls_date() {
    return acct_cls_date;
  }

  public void setAcct_cls_date(java.sql.Date acct_cls_date) {
    this.acct_cls_date = acct_cls_date;
  }

  public String getClr_bal_amt() {
    return clr_bal_amt;
  }

  public void setClr_bal_amt(String clr_bal_amt) {
    this.clr_bal_amt = clr_bal_amt;
  }

  public String getTot_mod_times() {
    return tot_mod_times;
  }

  public void setTot_mod_times(String tot_mod_times) {
    this.tot_mod_times = tot_mod_times;
  }

  public String getLedg_num() {
    return ledg_num;
  }

  public void setLedg_num(String ledg_num) {
    this.ledg_num = ledg_num;
  }

  public String getUn_clr_bal_amt() {
    return un_clr_bal_amt;
  }

  public void setUn_clr_bal_amt(String un_clr_bal_amt) {
    this.un_clr_bal_amt = un_clr_bal_amt;
  }

  public String getDrwng_power() {
    return drwng_power;
  }

  public void setDrwng_power(String drwng_power) {
    this.drwng_power = drwng_power;
  }

  public String getSanct_lim() {
    return sanct_lim;
  }

  public void setSanct_lim(String sanct_lim) {
    this.sanct_lim = sanct_lim;
  }

  public String getAdhoc_lim() {
    return adhoc_lim;
  }

  public void setAdhoc_lim(String adhoc_lim) {
    this.adhoc_lim = adhoc_lim;
  }

  public String getEmer_advn() {
    return emer_advn;
  }

  public void setEmer_advn(String emer_advn) {
    this.emer_advn = emer_advn;
  }

  public String getDacc_lim() {
    return dacc_lim;
  }

  public void setDacc_lim(String dacc_lim) {
    this.dacc_lim = dacc_lim;
  }

  public String getSystem_reserved_amt() {
    return system_reserved_amt;
  }

  public void setSystem_reserved_amt(String system_reserved_amt) {
    this.system_reserved_amt = system_reserved_amt;
  }

  public String getSingle_tran_lim() {
    return single_tran_lim;
  }

  public void setSingle_tran_lim(String single_tran_lim) {
    this.single_tran_lim = single_tran_lim;
  }

  public String getClean_adhoc_lim() {
    return clean_adhoc_lim;
  }

  public void setClean_adhoc_lim(String clean_adhoc_lim) {
    this.clean_adhoc_lim = clean_adhoc_lim;
  }

  public String getClean_emer_advn() {
    return clean_emer_advn;
  }

  public void setClean_emer_advn(String clean_emer_advn) {
    this.clean_emer_advn = clean_emer_advn;
  }

  public String getClean_single_tran_lim() {
    return clean_single_tran_lim;
  }

  public void setClean_single_tran_lim(String clean_single_tran_lim) {
    this.clean_single_tran_lim = clean_single_tran_lim;
  }

  public String getSystem_gen_lim() {
    return system_gen_lim;
  }

  public void setSystem_gen_lim(String system_gen_lim) {
    this.system_gen_lim = system_gen_lim;
  }

  public String getChq_alwd_flg() {
    return chq_alwd_flg;
  }

  public void setChq_alwd_flg(String chq_alwd_flg) {
    this.chq_alwd_flg = chq_alwd_flg;
  }

  public String getCash_excp_amt_lim() {
    return cash_excp_amt_lim;
  }

  public void setCash_excp_amt_lim(String cash_excp_amt_lim) {
    this.cash_excp_amt_lim = cash_excp_amt_lim;
  }

  public String getClg_excp_amt_lim() {
    return clg_excp_amt_lim;
  }

  public void setClg_excp_amt_lim(String clg_excp_amt_lim) {
    this.clg_excp_amt_lim = clg_excp_amt_lim;
  }

  public String getXfer_excp_amt_lim() {
    return xfer_excp_amt_lim;
  }

  public void setXfer_excp_amt_lim(String xfer_excp_amt_lim) {
    this.xfer_excp_amt_lim = xfer_excp_amt_lim;
  }

  public String getCash_cr_excp_amt_lim() {
    return cash_cr_excp_amt_lim;
  }

  public void setCash_cr_excp_amt_lim(String cash_cr_excp_amt_lim) {
    this.cash_cr_excp_amt_lim = cash_cr_excp_amt_lim;
  }

  public String getClg_cr_excp_amt_lim() {
    return clg_cr_excp_amt_lim;
  }

  public void setClg_cr_excp_amt_lim(String clg_cr_excp_amt_lim) {
    this.clg_cr_excp_amt_lim = clg_cr_excp_amt_lim;
  }

  public String getXfer_cr_excp_amt_lim() {
    return xfer_cr_excp_amt_lim;
  }

  public void setXfer_cr_excp_amt_lim(String xfer_cr_excp_amt_lim) {
    this.xfer_cr_excp_amt_lim = xfer_cr_excp_amt_lim;
  }

  public String getCash_abnrml_amt_lim() {
    return cash_abnrml_amt_lim;
  }

  public void setCash_abnrml_amt_lim(String cash_abnrml_amt_lim) {
    this.cash_abnrml_amt_lim = cash_abnrml_amt_lim;
  }

  public String getClg_abnrml_amt_lim() {
    return clg_abnrml_amt_lim;
  }

  public void setClg_abnrml_amt_lim(String clg_abnrml_amt_lim) {
    this.clg_abnrml_amt_lim = clg_abnrml_amt_lim;
  }

  public String getXfer_abnrml_amt_lim() {
    return xfer_abnrml_amt_lim;
  }

  public void setXfer_abnrml_amt_lim(String xfer_abnrml_amt_lim) {
    this.xfer_abnrml_amt_lim = xfer_abnrml_amt_lim;
  }

  public String getCum_dr_amt() {
    return cum_dr_amt;
  }

  public void setCum_dr_amt(String cum_dr_amt) {
    this.cum_dr_amt = cum_dr_amt;
  }

  public String getCum_cr_amt() {
    return cum_cr_amt;
  }

  public void setCum_cr_amt(String cum_cr_amt) {
    this.cum_cr_amt = cum_cr_amt;
  }

  public String getAcrd_cr_amt() {
    return acrd_cr_amt;
  }

  public void setAcrd_cr_amt(String acrd_cr_amt) {
    this.acrd_cr_amt = acrd_cr_amt;
  }

  public java.sql.Date getLast_tran_date() {
    return last_tran_date;
  }

  public void setLast_tran_date(java.sql.Date last_tran_date) {
    this.last_tran_date = last_tran_date;
  }

  public String getMode_of_oper_code() {
    return mode_of_oper_code;
  }

  public void setMode_of_oper_code(String mode_of_oper_code) {
    this.mode_of_oper_code = mode_of_oper_code;
  }

  public String getPb_ps_code() {
    return pb_ps_code;
  }

  public void setPb_ps_code(String pb_ps_code) {
    this.pb_ps_code = pb_ps_code;
  }

  public String getServ_chrg_coll_flg() {
    return serv_chrg_coll_flg;
  }

  public void setServ_chrg_coll_flg(String serv_chrg_coll_flg) {
    this.serv_chrg_coll_flg = serv_chrg_coll_flg;
  }

  public String getFree_text() {
    return free_text;
  }

  public void setFree_text(String free_text) {
    this.free_text = free_text;
  }

  public String getAcct_turnover_det_flg() {
    return acct_turnover_det_flg;
  }

  public void setAcct_turnover_det_flg(String acct_turnover_det_flg) {
    this.acct_turnover_det_flg = acct_turnover_det_flg;
  }

  public String getNom_available_flg() {
    return nom_available_flg;
  }

  public void setNom_available_flg(String nom_available_flg) {
    this.nom_available_flg = nom_available_flg;
  }

  public String getAcct_locn_code() {
    return acct_locn_code;
  }

  public void setAcct_locn_code(String acct_locn_code) {
    this.acct_locn_code = acct_locn_code;
  }

  public java.sql.Date getLast_purge_date() {
    return last_purge_date;
  }

  public void setLast_purge_date(java.sql.Date last_purge_date) {
    this.last_purge_date = last_purge_date;
  }

  public String getBal_on_purge_date() {
    return bal_on_purge_date;
  }

  public void setBal_on_purge_date(String bal_on_purge_date) {
    this.bal_on_purge_date = bal_on_purge_date;
  }

  public String getInt_paid_flg() {
    return int_paid_flg;
  }

  public void setInt_paid_flg(String int_paid_flg) {
    this.int_paid_flg = int_paid_flg;
  }

  public String getInt_coll_flg() {
    return int_coll_flg;
  }

  public void setInt_coll_flg(String int_coll_flg) {
    this.int_coll_flg = int_coll_flg;
  }

  public java.sql.Date getLast_any_tran_date() {
    return last_any_tran_date;
  }

  public void setLast_any_tran_date(java.sql.Date last_any_tran_date) {
    this.last_any_tran_date = last_any_tran_date;
  }

  public String getHashed_no() {
    return hashed_no;
  }

  public void setHashed_no(String hashed_no) {
    this.hashed_no = hashed_no;
  }

  public String getLchg_user_id() {
    return lchg_user_id;
  }

  public void setLchg_user_id(String lchg_user_id) {
    this.lchg_user_id = lchg_user_id;
  }

  public java.sql.Date getLchg_time() {
    return lchg_time;
  }

  public void setLchg_time(java.sql.Date lchg_time) {
    this.lchg_time = lchg_time;
  }

  public String getRcre_user_id() {
    return rcre_user_id;
  }

  public void setRcre_user_id(String rcre_user_id) {
    this.rcre_user_id = rcre_user_id;
  }

  public java.sql.Date getRcre_time() {
    return rcre_time;
  }

  public void setRcre_time(java.sql.Date rcre_time) {
    this.rcre_time = rcre_time;
  }

  public String getLimit_b2kid() {
    return limit_b2kid;
  }

  public void setLimit_b2kid(String limit_b2kid) {
    this.limit_b2kid = limit_b2kid;
  }

  public String getDrwng_power_ind() {
    return drwng_power_ind;
  }

  public void setDrwng_power_ind(String drwng_power_ind) {
    this.drwng_power_ind = drwng_power_ind;
  }

  public String getDrwng_power_pcnt() {
    return drwng_power_pcnt;
  }

  public void setDrwng_power_pcnt(String drwng_power_pcnt) {
    this.drwng_power_pcnt = drwng_power_pcnt;
  }

  public String getMicr_chq_chrg_coll_flg() {
    return micr_chq_chrg_coll_flg;
  }

  public void setMicr_chq_chrg_coll_flg(String micr_chq_chrg_coll_flg) {
    this.micr_chq_chrg_coll_flg = micr_chq_chrg_coll_flg;
  }

  public java.sql.Date getLast_turnover_date() {
    return last_turnover_date;
  }

  public void setLast_turnover_date(java.sql.Date last_turnover_date) {
    this.last_turnover_date = last_turnover_date;
  }

  public String getNotional_rate() {
    return notional_rate;
  }

  public void setNotional_rate(String notional_rate) {
    this.notional_rate = notional_rate;
  }

  public String getNotional_rate_code() {
    return notional_rate_code;
  }

  public void setNotional_rate_code(String notional_rate_code) {
    this.notional_rate_code = notional_rate_code;
  }

  public String getFx_clr_bal_amt() {
    return fx_clr_bal_amt;
  }

  public void setFx_clr_bal_amt(String fx_clr_bal_amt) {
    this.fx_clr_bal_amt = fx_clr_bal_amt;
  }

  public String getFx_bal_on_purge_date() {
    return fx_bal_on_purge_date;
  }

  public void setFx_bal_on_purge_date(String fx_bal_on_purge_date) {
    this.fx_bal_on_purge_date = fx_bal_on_purge_date;
  }

  public String getFd_ref_num() {
    return fd_ref_num;
  }

  public void setFd_ref_num(String fd_ref_num) {
    this.fd_ref_num = fd_ref_num;
  }

  public String getFx_cum_cr_amt() {
    return fx_cum_cr_amt;
  }

  public void setFx_cum_cr_amt(String fx_cum_cr_amt) {
    this.fx_cum_cr_amt = fx_cum_cr_amt;
  }

  public String getFx_cum_dr_amt() {
    return fx_cum_dr_amt;
  }

  public void setFx_cum_dr_amt(String fx_cum_dr_amt) {
    this.fx_cum_dr_amt = fx_cum_dr_amt;
  }

  public String getCrncy_code() {
    return crncy_code;
  }

  public void setCrncy_code(String crncy_code) {
    this.crncy_code = crncy_code;
  }

  public String getSource_of_fund() {
    return source_of_fund;
  }

  public void setSource_of_fund(String source_of_fund) {
    this.source_of_fund = source_of_fund;
  }

  public String getAnw_non_cust_alwd_flg() {
    return anw_non_cust_alwd_flg;
  }

  public void setAnw_non_cust_alwd_flg(String anw_non_cust_alwd_flg) {
    this.anw_non_cust_alwd_flg = anw_non_cust_alwd_flg;
  }

  public String getAcct_crncy_code() {
    return acct_crncy_code;
  }

  public void setAcct_crncy_code(String acct_crncy_code) {
    this.acct_crncy_code = acct_crncy_code;
  }

  public String getLien_amt() {
    return lien_amt;
  }

  public void setLien_amt(String lien_amt) {
    this.lien_amt = lien_amt;
  }

  public String getAcct_classification_flg() {
    return acct_classification_flg;
  }

  public void setAcct_classification_flg(String acct_classification_flg) {
    this.acct_classification_flg = acct_classification_flg;
  }

  public String getSystem_only_acct_flg() {
    return system_only_acct_flg;
  }

  public void setSystem_only_acct_flg(String system_only_acct_flg) {
    this.system_only_acct_flg = system_only_acct_flg;
  }

  public String getSingle_tran_flg() {
    return single_tran_flg;
  }

  public void setSingle_tran_flg(String single_tran_flg) {
    this.single_tran_flg = single_tran_flg;
  }

  public String getUtilised_amt() {
    return utilised_amt;
  }

  public void setUtilised_amt(String utilised_amt) {
    this.utilised_amt = utilised_amt;
  }

  public String getInter_sol_access_flg() {
    return inter_sol_access_flg;
  }

  public void setInter_sol_access_flg(String inter_sol_access_flg) {
    this.inter_sol_access_flg = inter_sol_access_flg;
  }

  public String getPurge_allowed_flg() {
    return purge_allowed_flg;
  }

  public void setPurge_allowed_flg(String purge_allowed_flg) {
    this.purge_allowed_flg = purge_allowed_flg;
  }

  public String getPurge_text() {
    return purge_text;
  }

  public void setPurge_text(String purge_text) {
    this.purge_text = purge_text;
  }

  public java.sql.Date getMin_value_date() {
    return min_value_date;
  }

  public void setMin_value_date(java.sql.Date min_value_date) {
    this.min_value_date = min_value_date;
  }

  public String getAcct_mgr_user_id() {
    return acct_mgr_user_id;
  }

  public void setAcct_mgr_user_id(String acct_mgr_user_id) {
    this.acct_mgr_user_id = acct_mgr_user_id;
  }

  public String getSchm_type() {
    return schm_type;
  }

  public void setSchm_type(String schm_type) {
    this.schm_type = schm_type;
  }

  public java.sql.Date getLast_frez_date() {
    return last_frez_date;
  }

  public void setLast_frez_date(java.sql.Date last_frez_date) {
    this.last_frez_date = last_frez_date;
  }

  public java.sql.Date getLast_unfrez_date() {
    return last_unfrez_date;
  }

  public void setLast_unfrez_date(java.sql.Date last_unfrez_date) {
    this.last_unfrez_date = last_unfrez_date;
  }

  public String getBal_on_frez_date() {
    return bal_on_frez_date;
  }

  public void setBal_on_frez_date(String bal_on_frez_date) {
    this.bal_on_frez_date = bal_on_frez_date;
  }

  public String getSwift_allowed_flg() {
    return swift_allowed_flg;
  }

  public void setSwift_allowed_flg(String swift_allowed_flg) {
    this.swift_allowed_flg = swift_allowed_flg;
  }

  public String getDacc_lim_pcnt() {
    return dacc_lim_pcnt;
  }

  public void setDacc_lim_pcnt(String dacc_lim_pcnt) {
    this.dacc_lim_pcnt = dacc_lim_pcnt;
  }

  public String getDacc_lim_abs() {
    return dacc_lim_abs;
  }

  public void setDacc_lim_abs(String dacc_lim_abs) {
    this.dacc_lim_abs = dacc_lim_abs;
  }

  public String getChrg_level_code() {
    return chrg_level_code;
  }

  public void setChrg_level_code(String chrg_level_code) {
    this.chrg_level_code = chrg_level_code;
  }

  public String getAcct_cls_chrg_pend_verf() {
    return acct_cls_chrg_pend_verf;
  }

  public void setAcct_cls_chrg_pend_verf(String acct_cls_chrg_pend_verf) {
    this.acct_cls_chrg_pend_verf = acct_cls_chrg_pend_verf;
  }

  public String getPartitioned_flg() {
    return partitioned_flg;
  }

  public void setPartitioned_flg(String partitioned_flg) {
    this.partitioned_flg = partitioned_flg;
  }

  public String getPartitioned_type() {
    return partitioned_type;
  }

  public void setPartitioned_type(String partitioned_type) {
    this.partitioned_type = partitioned_type;
  }

  public String getPbf_download_flg() {
    return pbf_download_flg;
  }

  public void setPbf_download_flg(String pbf_download_flg) {
    this.pbf_download_flg = pbf_download_flg;
  }

  public java.sql.Date getPbf_delink_date() {
    return pbf_delink_date;
  }

  public void setPbf_delink_date(java.sql.Date pbf_delink_date) {
    this.pbf_delink_date = pbf_delink_date;
  }

  public String getWtax_flg() {
    return wtax_flg;
  }

  public void setWtax_flg(String wtax_flg) {
    this.wtax_flg = wtax_flg;
  }

  public String getWtax_amount_scope_flg() {
    return wtax_amount_scope_flg;
  }

  public void setWtax_amount_scope_flg(String wtax_amount_scope_flg) {
    this.wtax_amount_scope_flg = wtax_amount_scope_flg;
  }

  public String getInt_adj_for_deduction_flg() {
    return int_adj_for_deduction_flg;
  }

  public void setInt_adj_for_deduction_flg(String int_adj_for_deduction_flg) {
    this.int_adj_for_deduction_flg = int_adj_for_deduction_flg;
  }

  public String getOperative_acid() {
    return operative_acid;
  }

  public void setOperative_acid(String operative_acid) {
    this.operative_acid = operative_acid;
  }

  public String getPhone_num() {
    return phone_num;
  }

  public void setPhone_num(String phone_num) {
    this.phone_num = phone_num;
  }

  public String getNative_lang_name() {
    return native_lang_name;
  }

  public void setNative_lang_name(String native_lang_name) {
    this.native_lang_name = native_lang_name;
  }

  public String getNat_lang_title_code() {
    return nat_lang_title_code;
  }

  public void setNat_lang_title_code(String nat_lang_title_code) {
    this.nat_lang_title_code = nat_lang_title_code;
  }

  public String getLang_code() {
    return lang_code;
  }

  public void setLang_code(String lang_code) {
    this.lang_code = lang_code;
  }

  public String getTs_cnt() {
    return ts_cnt;
  }

  public void setTs_cnt(String ts_cnt) {
    this.ts_cnt = ts_cnt;
  }

  public String getPool_id() {
    return pool_id;
  }

  public void setPool_id(String pool_id) {
    this.pool_id = pool_id;
  }

  public String getAllow_sweeps() {
    return allow_sweeps;
  }

  public void setAllow_sweeps(String allow_sweeps) {
    this.allow_sweeps = allow_sweeps;
  }

  public String getOrder_of_utilisation() {
    return order_of_utilisation;
  }

  public void setOrder_of_utilisation(String order_of_utilisation) {
    this.order_of_utilisation = order_of_utilisation;
  }

  public String getWtax_pcnt() {
    return wtax_pcnt;
  }

  public void setWtax_pcnt(String wtax_pcnt) {
    this.wtax_pcnt = wtax_pcnt;
  }

  public String getWtax_floor_limit() {
    return wtax_floor_limit;
  }

  public void setWtax_floor_limit(String wtax_floor_limit) {
    this.wtax_floor_limit = wtax_floor_limit;
  }

  public String getDsa_penal_flg() {
    return dsa_penal_flg;
  }

  public void setDsa_penal_flg(String dsa_penal_flg) {
    this.dsa_penal_flg = dsa_penal_flg;
  }

  public String getProduct_group() {
    return product_group;
  }

  public void setProduct_group(String product_group) {
    this.product_group = product_group;
  }

  public String getSource_deal_code() {
    return source_deal_code;
  }

  public void setSource_deal_code(String source_deal_code) {
    this.source_deal_code = source_deal_code;
  }

  public String getDisburse_deal_code() {
    return disburse_deal_code;
  }

  public void setDisburse_deal_code(String disburse_deal_code) {
    this.disburse_deal_code = disburse_deal_code;
  }

  public String getSweep_in_min_bal() {
    return sweep_in_min_bal;
  }

  public void setSweep_in_min_bal(String sweep_in_min_bal) {
    this.sweep_in_min_bal = sweep_in_min_bal;
  }

  public String getUsed_single_tran_lim() {
    return used_single_tran_lim;
  }

  public void setUsed_single_tran_lim(String used_single_tran_lim) {
    this.used_single_tran_lim = used_single_tran_lim;
  }

  public String getUsed_clean_single_tran_lim() {
    return used_clean_single_tran_lim;
  }

  public void setUsed_clean_single_tran_lim(String used_clean_single_tran_lim) {
    this.used_clean_single_tran_lim = used_clean_single_tran_lim;
  }

  public String getUsed_un_clr_over_dacc_amt() {
    return used_un_clr_over_dacc_amt;
  }

  public void setUsed_un_clr_over_dacc_amt(String used_un_clr_over_dacc_amt) {
    this.used_un_clr_over_dacc_amt = used_un_clr_over_dacc_amt;
  }

  public String getFfd_contrib_to_acct() {
    return ffd_contrib_to_acct;
  }

  public void setFfd_contrib_to_acct(String ffd_contrib_to_acct) {
    this.ffd_contrib_to_acct = ffd_contrib_to_acct;
  }

  public String getAcct_creation_mode() {
    return acct_creation_mode;
  }

  public void setAcct_creation_mode(String acct_creation_mode) {
    this.acct_creation_mode = acct_creation_mode;
  }

  public String getWtax_level_flg() {
    return wtax_level_flg;
  }

  public void setWtax_level_flg(String wtax_level_flg) {
    this.wtax_level_flg = wtax_level_flg;
  }

  public java.sql.Date getLast_modified_date() {
    return last_modified_date;
  }

  public void setLast_modified_date(java.sql.Date last_modified_date) {
    this.last_modified_date = last_modified_date;
  }

  public String getCif_id() {
    return cif_id;
  }

  public void setCif_id(String cif_id) {
    this.cif_id = cif_id;
  }

  public String getFuture_bal_amt() {
    return future_bal_amt;
  }

  public void setFuture_bal_amt(String future_bal_amt) {
    this.future_bal_amt = future_bal_amt;
  }

  public String getUtil_future_bal_amt() {
    return util_future_bal_amt;
  }

  public void setUtil_future_bal_amt(String util_future_bal_amt) {
    this.util_future_bal_amt = util_future_bal_amt;
  }

  public String getDafa_lim() {
    return dafa_lim;
  }

  public void setDafa_lim(String dafa_lim) {
    this.dafa_lim = dafa_lim;
  }

  public String getDafa_lim_abs() {
    return dafa_lim_abs;
  }

  public void setDafa_lim_abs(String dafa_lim_abs) {
    this.dafa_lim_abs = dafa_lim_abs;
  }

  public String getDafa_lim_pcnt() {
    return dafa_lim_pcnt;
  }

  public void setDafa_lim_pcnt(String dafa_lim_pcnt) {
    this.dafa_lim_pcnt = dafa_lim_pcnt;
  }

  public String getFuture_oc_tod_amt() {
    return future_oc_tod_amt;
  }

  public void setFuture_oc_tod_amt(String future_oc_tod_amt) {
    this.future_oc_tod_amt = future_oc_tod_amt;
  }

  public String getUsed_oc_cln_single_tran_lim() {
    return used_oc_cln_single_tran_lim;
  }

  public void setUsed_oc_cln_single_tran_lim(String used_oc_cln_single_tran_lim) {
    this.used_oc_cln_single_tran_lim = used_oc_cln_single_tran_lim;
  }

  public String getIban_number() {
    return iban_number;
  }

  public void setIban_number(String iban_number) {
    this.iban_number = iban_number;
  }

  public String getChannel_level_code() {
    return channel_level_code;
  }

  public void setChannel_level_code(String channel_level_code) {
    this.channel_level_code = channel_level_code;
  }

  public String getChannel_id() {
    return channel_id;
  }

  public void setChannel_id(String channel_id) {
    this.channel_id = channel_id;
  }

  public String getFuture_clr_bal_amt() {
    return future_clr_bal_amt;
  }

  public void setFuture_clr_bal_amt(String future_clr_bal_amt) {
    this.future_clr_bal_amt = future_clr_bal_amt;
  }

  public String getFuture_un_clr_bal_amt() {
    return future_un_clr_bal_amt;
  }

  public void setFuture_un_clr_bal_amt(String future_un_clr_bal_amt) {
    this.future_un_clr_bal_amt = future_un_clr_bal_amt;
  }

  public String getMaster_b2k_id() {
    return master_b2k_id;
  }

  public void setMaster_b2k_id(String master_b2k_id) {
    this.master_b2k_id = master_b2k_id;
  }

  public String getBank_id() {
    return bank_id;
  }

  public void setBank_id(String bank_id) {
    this.bank_id = bank_id;
  }

  public String getFrez_reason_code_2() {
    return frez_reason_code_2;
  }

  public void setFrez_reason_code_2(String frez_reason_code_2) {
    this.frez_reason_code_2 = frez_reason_code_2;
  }

  public String getFrez_reason_code_3() {
    return frez_reason_code_3;
  }

  public void setFrez_reason_code_3(String frez_reason_code_3) {
    this.frez_reason_code_3 = frez_reason_code_3;
  }

  public String getFrez_reason_code_4() {
    return frez_reason_code_4;
  }

  public void setFrez_reason_code_4(String frez_reason_code_4) {
    this.frez_reason_code_4 = frez_reason_code_4;
  }

  public String getFrez_reason_code_5() {
    return frez_reason_code_5;
  }

  public void setFrez_reason_code_5(String frez_reason_code_5) {
    this.frez_reason_code_5 = frez_reason_code_5;
  }

  public String getIs_loan_agnst_cltrl_flg() {
    return is_loan_agnst_cltrl_flg;
  }

  public void setIs_loan_agnst_cltrl_flg(String is_loan_agnst_cltrl_flg) {
    this.is_loan_agnst_cltrl_flg = is_loan_agnst_cltrl_flg;
  }

  public String getPreferred_cal_base() {
    return preferred_cal_base;
  }

  public void setPreferred_cal_base(String preferred_cal_base) {
    this.preferred_cal_base = preferred_cal_base;
  }

  public String getAdditional_cal_base() {
    return additional_cal_base;
  }

  public void setAdditional_cal_base(String additional_cal_base) {
    this.additional_cal_base = additional_cal_base;
  }

  public String getMud_pool_acct_flg() {
    return mud_pool_acct_flg;
  }

  public void setMud_pool_acct_flg(String mud_pool_acct_flg) {
    this.mud_pool_acct_flg = mud_pool_acct_flg;
  }

  public String getAlt1_acct_short_name() {
    return alt1_acct_short_name;
  }

  public void setAlt1_acct_short_name(String alt1_acct_short_name) {
    this.alt1_acct_short_name = alt1_acct_short_name;
  }

  public String getAlt1_acct_name() {
    return alt1_acct_name;
  }

  public void setAlt1_acct_name(String alt1_acct_name) {
    this.alt1_acct_name = alt1_acct_name;
  }

  public String getExternal_pricing_flg() {
    return external_pricing_flg;
  }

  public void setExternal_pricing_flg(String external_pricing_flg) {
    this.external_pricing_flg = external_pricing_flg;
  }

  public String getAcct_mod_flg() {
    return acct_mod_flg;
  }

  public void setAcct_mod_flg(String acct_mod_flg) {
    this.acct_mod_flg = acct_mod_flg;
  }

  public String getWtax_floor_limit_freq() {
    return wtax_floor_limit_freq;
  }

  public void setWtax_floor_limit_freq(String wtax_floor_limit_freq) {
    this.wtax_floor_limit_freq = wtax_floor_limit_freq;
  }

  public String getChq_prov_by_bank_flg() {
    return chq_prov_by_bank_flg;
  }

  public void setChq_prov_by_bank_flg(String chq_prov_by_bank_flg) {
    this.chq_prov_by_bank_flg = chq_prov_by_bank_flg;
  }

  public String getChq_val_prd_mnth() {
    return chq_val_prd_mnth;
  }

  public void setChq_val_prd_mnth(String chq_val_prd_mnth) {
    this.chq_val_prd_mnth = chq_val_prd_mnth;
  }

  public String getChq_val_prd_day() {
    return chq_val_prd_day;
  }

  public void setChq_val_prd_day(String chq_val_prd_day) {
    this.chq_val_prd_day = chq_val_prd_day;
  }

  public String getPymnt_ref_num() {
    return pymnt_ref_num;
  }

  public void setPymnt_ref_num(String pymnt_ref_num) {
    this.pymnt_ref_num = pymnt_ref_num;
  }

  public String getRa_ref_num() {
    return ra_ref_num;
  }

  public void setRa_ref_num(String ra_ref_num) {
    this.ra_ref_num = ra_ref_num;
  }

  public String getAsynch_bal_upd_flg() {
    return asynch_bal_upd_flg;
  }

  public void setAsynch_bal_upd_flg(String asynch_bal_upd_flg) {
    this.asynch_bal_upd_flg = asynch_bal_upd_flg;
  }

  public java.sql.Date getLast_tran_date_cr() {
    return last_tran_date_cr;
  }

  public void setLast_tran_date_cr(java.sql.Date last_tran_date_cr) {
    this.last_tran_date_cr = last_tran_date_cr;
  }

  public java.sql.Date getLast_tran_date_dr() {
    return last_tran_date_dr;
  }

  public void setLast_tran_date_dr(java.sql.Date last_tran_date_dr) {
    this.last_tran_date_dr = last_tran_date_dr;
  }

  public String getLast_tran_id_cr() {
    return last_tran_id_cr;
  }

  public void setLast_tran_id_cr(String last_tran_id_cr) {
    this.last_tran_id_cr = last_tran_id_cr;
  }

  public String getLast_tran_id_dr() {
    return last_tran_id_dr;
  }

  public void setLast_tran_id_dr(String last_tran_id_dr) {
    this.last_tran_id_dr = last_tran_id_dr;
  }

  public String getDr_int_method() {
    return dr_int_method;
  }

  public void setDr_int_method(String dr_int_method) {
    this.dr_int_method = dr_int_method;
  }

  public String getCons_bal_flg() {
    return cons_bal_flg;
  }

  public void setCons_bal_flg(String cons_bal_flg) {
    this.cons_bal_flg = cons_bal_flg;
  }

  public String getSchm_sub_type() {
    return schm_sub_type;
  }

  public void setSchm_sub_type(String schm_sub_type) {
    this.schm_sub_type = schm_sub_type;
  }

  public String getSsa_plan_id() {
    return ssa_plan_id;
  }

  public void setSsa_plan_id(String ssa_plan_id) {
    this.ssa_plan_id = ssa_plan_id;
  }

  public java.sql.Date getWtax_waiver_start_date() {
    return wtax_waiver_start_date;
  }

  public void setWtax_waiver_start_date(java.sql.Date wtax_waiver_start_date) {
    this.wtax_waiver_start_date = wtax_waiver_start_date;
  }

  public java.sql.Date getWtax_waiver_end_date() {
    return wtax_waiver_end_date;
  }

  public void setWtax_waiver_end_date(java.sql.Date wtax_waiver_end_date) {
    this.wtax_waiver_end_date = wtax_waiver_end_date;
  }

  public String getTax_event_reason_code() {
    return tax_event_reason_code;
  }

  public void setTax_event_reason_code(String tax_event_reason_code) {
    this.tax_event_reason_code = tax_event_reason_code;
  }

  public String getWtax_floor_limit_scope_flg() {
    return wtax_floor_limit_scope_flg;
  }

  public void setWtax_floor_limit_scope_flg(String wtax_floor_limit_scope_flg) {
    this.wtax_floor_limit_scope_flg = wtax_floor_limit_scope_flg;
  }
}

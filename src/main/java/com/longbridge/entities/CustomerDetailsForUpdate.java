package com.longbridge.entities;

import com.longbridge.FI.FiCorpAcctInq.CorpCustAcctInqResponse;
import com.longbridge.FI.FiCorpCustInq.GetCorporateCustomerDetailsResponse;
import com.longbridge.FI.FiRetAcctInq.RetCustAcctInqResponse;
import com.longbridge.FI.FiRetCustInq.RetCustInqResponse;

import java.io.Serializable;

/**
 * Created by LB-PRJ-020 on 8/2/2017.
 */
public class CustomerDetailsForUpdate implements Serializable {
    private RetCustInqResponse retCcustInqResponse;
    private RetCustAcctInqResponse retCustAcctResponse;
    private CorpCustAcctInqResponse corpAcctInqResp;
    private GetCorporateCustomerDetailsResponse corpCustInqResp;
//    private

    public CustomerDetailsForUpdate(Object retCcustInqResponse, Object acctInqResponse, String custResptype) {
        if(custResptype.equals("RETAIL")){
            this.retCcustInqResponse = (RetCustInqResponse) retCcustInqResponse;
            this.retCustAcctResponse = (RetCustAcctInqResponse) acctInqResponse;
        }else{
            this.corpCustInqResp = (GetCorporateCustomerDetailsResponse) retCcustInqResponse;
            this.corpAcctInqResp = (CorpCustAcctInqResponse) acctInqResponse;
        }

    }

    public CustomerDetailsForUpdate() {
    }

    public RetCustInqResponse getRetCcustInqResponse() {
        return retCcustInqResponse;
    }

    public void setRetCcustInqResponse(RetCustInqResponse retCcustInqResponse) {
        this.retCcustInqResponse = retCcustInqResponse;
    }

    public RetCustAcctInqResponse getRetCustAcctResponse() {
        return retCustAcctResponse;
    }

    public void setRetCustAcctResponse(RetCustAcctInqResponse retCustAcctResponse) {
        this.retCustAcctResponse = retCustAcctResponse;
    }

    public CorpCustAcctInqResponse getCorpAcctInqResp() {
        return corpAcctInqResp;
    }

    public void setCorpAcctInqResp(CorpCustAcctInqResponse corpAcctInqResp) {
        this.corpAcctInqResp = corpAcctInqResp;
    }

    public GetCorporateCustomerDetailsResponse getCorpCustInqResp() {
        return corpCustInqResp;
    }

    public void setCorpCustInqResp(GetCorporateCustomerDetailsResponse corpCustInqResp) {
        this.corpCustInqResp = corpCustInqResp;
    }

    @Override
    public String toString() {
        return "CustomerDetailsForUpdate{" +
                "retCcustInqResponse=" + retCcustInqResponse +
                ", retCustAcctResponse=" + retCustAcctResponse +
                ", corpAcctInqResp=" + corpAcctInqResp +
                ", corpCustInqResp=" + corpCustInqResp +
                '}';
    }




}

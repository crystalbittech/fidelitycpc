package com.longbridge.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Timmy on 1/2/2012.
 */
@Entity
@Table(name = "UPLD_DOC",schema = "CUSTOM")
public class UpldDoc {




    private String docId;
    private byte[] docVal;



    public UpldDoc() {
    }


    public UpldDoc(String docId, byte[] docVal){
        this.docId = docId;
        this.docVal = docVal;

    }

    @Id
    @Column(name = "DOC_ID")
    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    @Column(name = "DOC_VAL")
    public byte[] getDocVal() {
        return docVal;
    }
    public void setDocVal(byte[] docVal) {
        this.docVal = docVal;
    }
}

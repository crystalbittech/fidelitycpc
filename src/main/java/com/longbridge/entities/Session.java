package com.longbridge.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Created by Timmy on 1/2/2012.
 */
@Entity
@Table(name = "SESSION",schema = "CUSTOM")
public class Session implements Serializable{

    private String docId;
    private String acctNumber;
    private String acctStatus;
    private String docmntUploader;
    private Timestamp docmntUpldDate;
    private String docmntInputter;
    private Timestamp docInputDate;
    private String acctVerifier;
    private Timestamp acctVerifyDate;
    private String statusReason;
    private String docsSubmitted;
    private Timestamp acctOpnDate;
    private String note;
    private byte[]acctDetails;


    public Session() {
    }



    @Id
    @Column(name = "ACCT_NUMBER")
    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }
    @Column(name = "DOC_ID")
    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    @Column(name = "STATUS_REASON")
    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

   
    @Column(name = "ACCT_STATUS")
    public String getAcctStatus() {
        return acctStatus;
    }

    public void setAcctStatus(String acctStatus) {
        this.acctStatus = acctStatus;
    }

    
    @Column(name = "DOC_UPLOADER")
    public String getDocmntUploader() {
        return docmntUploader;
    }

    public void setDocmntUploader(String docmntUploader) {
        this.docmntUploader = docmntUploader;
    }

    
    @Column(name = "DOC_INPUTTER")
    public String getDocmntInputter() {
        return docmntInputter;
    }

    public void setDocmntInputter(String docmntInputter) {
        this.docmntInputter = docmntInputter;
    }

    
    @Column(name = "DOC_VERIFIER")
    public String getAcctVerifier() {
        return acctVerifier;
    }

    public void setAcctVerifier(String acctVerifier) {
        this.acctVerifier = acctVerifier;
    }

    @Column(name = "DATE_UPLOADED")
    public Timestamp getDocmntUpldDate() {
        return docmntUpldDate;
    }

    public void setDocmntUpldDate(Timestamp docmntUpldDate) {
        this.docmntUpldDate = docmntUpldDate;
    }
    @Column(name = "DATE_INPUTTED")
    public Timestamp getDocInputDate() {
        return docInputDate;
    }

    public void setDocInputDate(Timestamp docInputDate) {
        this.docInputDate = docInputDate;
    }
    @Column(name = "DATE_VERIFIED")
    public Timestamp getAcctVerifyDate() {
        return acctVerifyDate;
    }

    public void setAcctVerifyDate(Timestamp acctVerifyDate) {
        this.acctVerifyDate = acctVerifyDate;
    }

    @Column(name = "DOCS_SUBMITTED")
    public String getDocsSubmitted() {
        return docsSubmitted;
    }

    public void setDocsSubmitted(String docsSubmitted) {
        this.docsSubmitted = docsSubmitted;
    }
    @Column(name = "ACCT_OPN_DATE")
    public Timestamp getAcctOpnDate() {
        return acctOpnDate;
    }

    public void setAcctOpnDate(Timestamp acctOpnDate) {
        this.acctOpnDate = acctOpnDate;
    }

    @Column(name = "NOTE")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Column(name = "Acct_DET")
    public byte[] getAcctDetails() {
        return acctDetails;
    }

    public void setAcctDetails(byte[] acctDetails) {
        this.acctDetails = acctDetails;
    }

    @Override
    public String toString() {
        return "FidWorkflow{" +
                "docId='" + docId + '\'' +
                ", acctNumber='" + acctNumber + '\'' +
                ", acctStatus='" + acctStatus + '\'' +
                ", docmntUploader='" + docmntUploader + '\'' +
                ", docmntUpldDate=" + docmntUpldDate +
                ", docmntInputter='" + docmntInputter + '\'' +
                ", docInputDate=" + docInputDate +
                ", acctVerifier='" + acctVerifier + '\'' +
                ", acctVerifyDate=" + acctVerifyDate +
                ", statusReason='" + statusReason + '\'' +
                ", docsSubmitted='" + docsSubmitted + '\'' +
                ", acctOpnDate=" + acctOpnDate +
                ", note='" + note + '\'' +
                ", acctDetails=" + Arrays.toString(acctDetails) +
                '}';
    }
}

package com.longbridge.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Timmy on 4/10/2017.
 */
@Entity
@Table(name = "FID_USERS",schema = "CUSTOM")
public class FidUsers implements Serializable{


    private long userId;
    private String userName;
    private String pass;
    private String userRole;
    private String status;
    private String userEmail;
    private Timestamp dateCreated;

    public FidUsers() {
    }


    public FidUsers(long userId, String userName, String pass, String userRole, String userEmail, Timestamp dateCreated, String status) {
        this.userId = userId;
        this.userName = userName;
        this.pass = pass;
        this.userRole = userRole;
        this.status = status;
        this.userEmail = userEmail;
        this.dateCreated = dateCreated;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "USER_ID", nullable = false, insertable = true, updatable = true, precision = 0)
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }


    @Column(name = "USER_NAME", nullable = false, insertable = true, updatable = true)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    
    @Column(name = "PASS", nullable = true, insertable = true, updatable = true)
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    
    @Column(name = "USER_ROLE", nullable = true, insertable = true, updatable = true)
    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    
    @Column(name = "STATUS", nullable = true, insertable = true, updatable = true)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "USER_EMAIL", nullable = true, insertable = true, updatable = true)
    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
    @Column(name = "DATE_CREATED", nullable = true, insertable = true, updatable = true)
    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

}

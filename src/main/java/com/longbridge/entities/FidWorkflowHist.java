package com.longbridge.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Timmy on 1/2/2012.
 */
@Entity
@Table(name = "FID_WORKFLOW_HIST",schema = "CUSTOM")
public class FidWorkflowHist {


    private long id;
    private String docId;
    private String acctNumber;
    private Timestamp acctOpnDate;
    private String operation;
    private Timestamp oprtnDate;
    private String statusReason;
    private String note;
    private String docsSubmitted;


    public FidWorkflowHist() {
    }


    public FidWorkflowHist(long id, String docId, String acctNumber, Timestamp acctOpnDate, String operation,
                           Timestamp oprtnDate, String statusReason,String note, String docsSubmitted) {
        this.id = id;
        this.docId = docId;
        this.acctNumber = acctNumber;
        this.acctOpnDate = acctOpnDate;
        this.operation = operation;
        this.oprtnDate = oprtnDate;
        this.statusReason = statusReason;
        this.note = note;
        this.docsSubmitted = docsSubmitted;
    }

    @Id
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "ACCT_NUMBER")
    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }
    @Column(name = "DOC_ID")
    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    @Column(name = "STATUS_REASON")
    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    @Column(name = "DOCS_SUBMITTED")
    public String getDocsSubmitted() {
        return docsSubmitted;
    }

    public void setDocsSubmitted(String docsSubmitted) {
        this.docsSubmitted = docsSubmitted;
    }
    @Column(name = "ACCT_OPN_DATE")
    public Timestamp getAcctOpnDate() {
        return acctOpnDate;
    }

    public void setAcctOpnDate(Timestamp acctOpnDate) {
        this.acctOpnDate = acctOpnDate;
    }
    @Column(name = "OPERTN")
    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
    @Column(name = "OPRTN_DATE")
    public Timestamp getOprtnDate() {
        return oprtnDate;
    }

    public void setOprtnDate(Timestamp oprtnDate) {
        this.oprtnDate = oprtnDate;
    }
    @Column(name = "NOTE")
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}

package com.longbridge.FinacleEntities;

public class CRMACCOUNTS {
  private String accountid;
  private String bank_id;
  private String enable_alerts;
  private String defaultchannel_alert;
  private String preferred_mobile_alert_no;
  private String preferred_mobile_alert_type;
  private String uniqueidnumber;
  private String sencitizenconversionflag;
  private java.sql.Date sencitizenapplicabledate;
  private String seniorcitizen;
  private java.sql.Date dateofnotification;
  private java.sql.Date dateofdeath;
  private String fatcaremarks;
  private java.sql.Date nextforeigntaxreviewdate;
  private String foreigntaxreportingstatus;
  private String foreigntaxreportingcountry;
  private String foreignacctaxreportingreq;
  private java.sql.Date lastforeigntaxreviewdate;
  private String submitforkyc;
  private java.sql.Date kyc_reviewdate;
  private java.sql.Date kyc_date;
  private String riskrating;
  private String idtyper1;
  private String idtyper2;
  private String idtyper3;
  private String idtyper4;
  private String idtyper5;
  private String introducername_alt1;
  private String cust_last_name_alt1;
  private String cust_first_name_alt1;
  private String cust_middle_name_alt1;
  private String strfield6_alt1;
  private String short_name_alt1;
  private String name_alt1;
  private String createdbysystemid;
  private String modifiedbysystemid;
  private String corp_id;
  private String primarypersonid;
  private String orgkey;
  private String otuoldcifid;
  private String orgname;
  private String orgtype;
  private String rating;
  private String accounttype;
  private String cust_first_name;
  private String cust_middle_name;
  private String cust_last_name;
  private String salutation;
  private String gender;
  private String birth_day;
  private String birth_month;
  private String birth_year;
  private java.sql.Date cust_dob;
  private String cust_type;
  private String education;
  private String occupation;
  private String persontype;
  private String cust_language;
  private String cust_staff_status;
  private String phone;
  private String extension;
  private String fax;
  private String fax_home;
  private String phone_home;
  private String phone_home2;
  private String phone_cell;
  private String email_home;
  private String email_palm;
  private String email;
  private String city;
  private String preferredchannelid;
  private String pan;
  private String passportno;
  private String licenseno;
  private String customerrelationshipno;
  private java.sql.Date relationshipopeningdate;
  private String relationshipvalue;
  private String category;
  private String numberofproducts;
  private String relationshipmgrid;
  private String relationshipcreatedbyid;
  private String futureprocess;
  private String intwfid;
  private String url;
  private String status;
  private String industry;
  private String parentorg;
  private String competitor;
  private String siccode;
  private String region;
  private String cin;
  private String manager;
  private String designation;
  private String assistant;
  private String internalscore;
  private java.sql.Date creditbureauscorevalidity;
  private String creditbureauscore;
  private java.sql.Date creditbureaurequestdate;
  private String creditbureaudescription;
  private String maidennameofmother;
  private String annualrevenue;
  private String revenueunits;
  private String tickersymbol;
  private String autoapproval;
  private String freezeproductsale;
  private String relationshipfield1;
  private String relationshipfield2;
  private String relationshipfield3;
  private String delinquencyflg;
  private String customernreflg;
  private String customerminor;
  private String combinedstatementflg;
  private String customertrade;
  private String uniqueid;
  private String uniqueidtype;
  private String staffemployeeid;
  private String placeofbirth;
  private String countryofbirth;
  private String proofofageflag;
  private String proofofagedocument;
  private String staffflag;
  private String namesuffix;
  private String maidenname;
  private String customerprofitability;
  private String householdid;
  private String householdname;
  private String hshlduflag;
  private String currentcrexposure;
  private String totalcrexposure;
  private String potentialcrline;
  private String availablecrlimit;
  private String creditscorerequestedflag;
  private String credithistoryrequestedflag;
  private String spouseid;
  private String ssn;
  private String groupid;
  private String flg1;
  private String flg2;
  private String flg3;
  private String alert1;
  private String alert2;
  private String alert3;
  private String relationshipoffer1;
  private String relationshipoffer2;
  private java.sql.Date dtdate1;
  private java.sql.Date dtdate2;
  private java.sql.Date dtdate3;
  private java.sql.Date dtdate4;
  private java.sql.Date dtdate5;
  private java.sql.Date dtdate6;
  private java.sql.Date dtdate7;
  private java.sql.Date dtdate8;
  private java.sql.Date dtdate9;
  private String amount1;
  private String amount2;
  private String amount3;
  private String amount4;
  private String amount5;
  private String strfield1;
  private String strfield2;
  private String strfield3;
  private String strfield4;
  private String strfield5;
  private String strfield6;
  private String strfield7;
  private String strfield8;
  private String strfield9;
  private String strfield10;
  private String strfield11;
  private String strfield12;
  private String strfield13;
  private String strfield14;
  private String strfield15;
  private String userflag1;
  private String userflag2;
  private String userflag3;
  private String userflag4;
  private String mluserfield1;
  private String mluserfield2;
  private String mluserfield3;
  private String mluserfield4;
  private String mluserfield5;
  private String mluserfield6;
  private String mluserfield7;
  private String mluserfield8;
  private String mluserfield9;
  private String mluserfield10;
  private String mluserfield11;
  private java.sql.Date dateofbecomingnre;
  private java.sql.Date bodatecreated;
  private String bocreatedby;
  private java.sql.Date bodatemodified;
  private String bomodifiedby;
  private String createduserid;
  private String owneduserid;
  private String notes;
  private String extensionid;
  private String xmldata;
  private String concurdetect_x;
  private String boaclid;
  private String securityilhint;
  private String securityiuhint;
  private String securityglhint;
  private String securityguhint;
  private String prioritycode;
  private String created_from;
  private String constitution_code;
  private String strfield18;
  private String strfield16;
  private String strfield17;
  private String strfield19;
  private String strfield20;
  private String strfield21;
  private String strfield22;
  private String amount6;
  private String amount7;
  private String amount8;
  private String amount9;
  private String amount10;
  private String amount11;
  private String amount12;
  private String intfield1;
  private String intfield2;
  private String intfield3;
  private String intfield4;
  private String intfield5;
  private String custclass;
  private String short_name;
  private String nick_name;
  private String mother_name;
  private String father_husband_name;
  private String previous_name;
  private String primary_service_centre;
  private String lead_source;
  private String relationship_type;
  private String relationship_level;
  private String rm_group_id;
  private String dsa_id;
  private String card_holder;
  private String photograph_id;
  private String secure_id;
  private String blacklisted;
  private String negated;
  private String suspended;
  private String deliquencyperiod;
  private String addname1;
  private String addname2;
  private String addname3;
  private String addname4;
  private String addname5;
  private String preferredname;
  private java.sql.Date oldentitycreatedon;
  private String oldentitytype;
  private String oldentityid;
  private String recordstatus;
  private String assignedto;
  private String processid;
  private String currentstep;
  private String processstatus;
  private String document_received;
  private String suspend_notes;
  private String suspend_reason;
  private String blacklist_notes;
  private String blacklist_reason;
  private String negated_notes;
  private String negated_reason;
  private String segmentation_class;
  private String name;
  private java.sql.Date ratingdate;
  private String manageropinion;
  private String introducerid;
  private String introducersalutation;
  private String introducername;
  private String introducerstatuscode;
  private String introd_status;
  private java.sql.Date custstatuschgdate;
  private String purgeflag;
  private String purgeremarks;
  private String tfpartyflag;
  private String nativelangname;
  private String nativelangtitle;
  private String nativelangcode;
  private java.sql.Date minorattainmajordate;
  private java.sql.Date nrebecomingorddate;
  private String defaultaddresstype;
  private String preferredemailtype;
  private String preferredphone;
  private java.sql.Date currstepduedate;
  private java.sql.Date startdate;
  private String stagename;
  private String assignedtogroup;
  private String add1_first_name;
  private String add1_middle_name;
  private String add1_last_name;
  private String add2_first_name;
  private String add2_middle_name;
  private String add2_last_name;
  private String add3_first_name;
  private String add3_middle_name;
  private String add3_last_name;
  private String add4_first_name;
  private String add4_middle_name;
  private String add4_last_name;
  private String add5_first_name;
  private String add5_middle_name;
  private String add5_last_name;
  private String dual_first_name;
  private String dual_middle_name;
  private String dual_last_name;
  private String cust_commu_code;
  private String cust_community;
  private String core_introd_cust_id;
  private String introd_salutation_code;
  private String cust_hlth_code;
  private String cust_hlth;
  private String tds_tbl_code;
  private String tds_tbl;
  private String tds_cust_id;
  private String nat_id_card_num;
  private java.sql.Date psprt_issue_date;
  private String psprt_det;
  private java.sql.Date psprt_exp_date;
  private java.sql.Date cust_pref_till_date;
  private String crncy_code;
  private String primary_sol_id;
  private String offline_cum_debit_limit;
  private String pref_code;
  private String introd_status_code;
  private String nativelangtitle_code;
  private String core_cust_id;
  private String entity_cre_flag;
  private String salutation_code;
  private String groupid_code;
  private String occupation_code;
  private String sector_code;
  private String sector;
  private String subsector_code;
  private String subsector;
  private String rating_code;
  private String cust_type_code;
  private String status_code;
  private String constitution_ref_code;
  private String custcreationmode;
  private String minor_guard_code;
  private String minor_guard_name;
  private String minor_guard_name_alt1;
  private String onetime_oldcustid;
  private java.sql.Date incrementaldate;
  private String processgroupid;
  private String assignedbyuserid;
  private String transferredbyuserid;
  private java.sql.Date prevstependdate;
  private String first_product_processor;
  private String interface_reference_id;
  private String cust_health_ref_code;
  private String tds_cifid;
  private String corprepcount;
  private String iscorprep;
  private String cust_first_name_native;
  private String cust_middle_name_native;
  private String cust_last_name_native;
  private String short_name_native;
  private String cust_first_name_native1;
  private String cust_middle_name_native1;
  private String cust_last_name_native1;
  private String short_name_native1;
  private String secondaryrm_id;
  private String isebankingenabled;
  private String issmsbankingenabled;
  private String tertiaryrm_id;
  private String subsegment;
  private String iswapbankingenabled;
  private String alreadycreatedinebanking;
  private String pref_code_rcode;
  private String cust_swift_code_desc;
  private String smsbankingmobilenumber;
  private String is_swift_code_of_bank;
  private String nativelangcode_code;
  private String accessownergroup;
  private String accessownersegment;
  private String accessownerbc;
  private String accessowneragent;
  private String accessassigneeagent;
  private String tabvalidator;
  private String lasteditedpage;
  private String chargelevelcode;
  private java.sql.Date segupdatedate;
  private String intuserfield1;
  private String intuserfield2;
  private String intuserfield3;
  private String intuserfield4;
  private String intuserfield5;
  private String struserfield1;
  private String struserfield2;
  private String struserfield3;
  private String struserfield4;
  private String struserfield5;
  private String struserfield6;
  private String struserfield7;
  private String struserfield8;
  private String struserfield9;
  private String struserfield10;
  private String struserfield11;
  private String struserfield12;
  private String struserfield13;
  private String struserfield14;
  private String struserfield15;
  private String struserfield16;
  private String struserfield17;
  private String struserfield18;
  private String struserfield19;
  private String struserfield20;
  private String struserfield21;
  private String struserfield22;
  private String struserfield23;
  private String struserfield24;
  private String struserfield25;
  private String struserfield26;
  private String struserfield27;
  private String struserfield28;
  private String struserfield29;
  private String struserfield30;
  private java.sql.Date dateuserfield1;
  private java.sql.Date dateuserfield2;
  private java.sql.Date dateuserfield3;
  private java.sql.Date dateuserfield4;
  private java.sql.Date dateuserfield5;
  private java.sql.Date converted_date;
  private String lastoperperformed;
  private String isdummy;
  private String istampered;
  private String checksum;
  private String assignedlocationid;
  private String ownedlocationid;
  private String isrouted;
  private java.sql.Date esc_due_time;
  private String esc_level;
  private String createdlocationid;
  private String editedlocationid;
  private String nocurstepassign;
  private java.sql.Date routedtime;
  private java.sql.Date procescduetime;
  private String stopescalation;
  private String firstgrassignownid;
  private String firstbcassignownid;
  private String firstuserassignownid;
  private String currnumposesc;
  private String currnumnegesc;
  private String numofposesc;
  private String numofnegesc;
  private String procesclevel;
  private String manualrouting;
  private String slalevel;
  private String routerulenum;
  private String escrulenum;
  private String escalationattr1;
  private String escalationattr2;
  private String escalationattr3;
  private String ownergroup;
  private String routeruleforgroup;
  private String routeruleforbc;
  private String routeruleforuser;
  private String tatduration;
  private String stopprocescalation;
  private java.sql.Date duedate;
  private String backendid;
  private java.sql.Date lastsubmitteddate;
  private String risk_profile_score;
  private java.sql.Date risk_profile_expiry_date;
  private String ismcedited;
  private String preferredphonetype;
  private String preferredemail;
  private String zakat_deduction;
  private String islamic_banking_customer;
  private String asset_classification;
  private String customer_level_provisioning;
  private String preferredcalendar;
  private String gcifid;
  private String enrollmentstatus;
  private String address_line1;
  private String address_line2;
  private String address_line3;
  private String state;
  private String country;
  private String zip;
  private String physical_state;
  private java.sql.Date tmdate;

  public String getAccountid() {
    return accountid;
  }

  public void setAccountid(String accountid) {
    this.accountid = accountid;
  }

  public String getBank_id() {
    return bank_id;
  }

  public void setBank_id(String bank_id) {
    this.bank_id = bank_id;
  }

  public String getEnable_alerts() {
    return enable_alerts;
  }

  public void setEnable_alerts(String enable_alerts) {
    this.enable_alerts = enable_alerts;
  }

  public String getDefaultchannel_alert() {
    return defaultchannel_alert;
  }

  public void setDefaultchannel_alert(String defaultchannel_alert) {
    this.defaultchannel_alert = defaultchannel_alert;
  }

  public String getPreferred_mobile_alert_no() {
    return preferred_mobile_alert_no;
  }

  public void setPreferred_mobile_alert_no(String preferred_mobile_alert_no) {
    this.preferred_mobile_alert_no = preferred_mobile_alert_no;
  }

  public String getPreferred_mobile_alert_type() {
    return preferred_mobile_alert_type;
  }

  public void setPreferred_mobile_alert_type(String preferred_mobile_alert_type) {
    this.preferred_mobile_alert_type = preferred_mobile_alert_type;
  }

  public String getUniqueidnumber() {
    return uniqueidnumber;
  }

  public void setUniqueidnumber(String uniqueidnumber) {
    this.uniqueidnumber = uniqueidnumber;
  }

  public String getSencitizenconversionflag() {
    return sencitizenconversionflag;
  }

  public void setSencitizenconversionflag(String sencitizenconversionflag) {
    this.sencitizenconversionflag = sencitizenconversionflag;
  }

  public java.sql.Date getSencitizenapplicabledate() {
    return sencitizenapplicabledate;
  }

  public void setSencitizenapplicabledate(java.sql.Date sencitizenapplicabledate) {
    this.sencitizenapplicabledate = sencitizenapplicabledate;
  }

  public String getSeniorcitizen() {
    return seniorcitizen;
  }

  public void setSeniorcitizen(String seniorcitizen) {
    this.seniorcitizen = seniorcitizen;
  }

  public java.sql.Date getDateofnotification() {
    return dateofnotification;
  }

  public void setDateofnotification(java.sql.Date dateofnotification) {
    this.dateofnotification = dateofnotification;
  }

  public java.sql.Date getDateofdeath() {
    return dateofdeath;
  }

  public void setDateofdeath(java.sql.Date dateofdeath) {
    this.dateofdeath = dateofdeath;
  }

  public String getFatcaremarks() {
    return fatcaremarks;
  }

  public void setFatcaremarks(String fatcaremarks) {
    this.fatcaremarks = fatcaremarks;
  }

  public java.sql.Date getNextforeigntaxreviewdate() {
    return nextforeigntaxreviewdate;
  }

  public void setNextforeigntaxreviewdate(java.sql.Date nextforeigntaxreviewdate) {
    this.nextforeigntaxreviewdate = nextforeigntaxreviewdate;
  }

  public String getForeigntaxreportingstatus() {
    return foreigntaxreportingstatus;
  }

  public void setForeigntaxreportingstatus(String foreigntaxreportingstatus) {
    this.foreigntaxreportingstatus = foreigntaxreportingstatus;
  }

  public String getForeigntaxreportingcountry() {
    return foreigntaxreportingcountry;
  }

  public void setForeigntaxreportingcountry(String foreigntaxreportingcountry) {
    this.foreigntaxreportingcountry = foreigntaxreportingcountry;
  }

  public String getForeignacctaxreportingreq() {
    return foreignacctaxreportingreq;
  }

  public void setForeignacctaxreportingreq(String foreignacctaxreportingreq) {
    this.foreignacctaxreportingreq = foreignacctaxreportingreq;
  }

  public java.sql.Date getLastforeigntaxreviewdate() {
    return lastforeigntaxreviewdate;
  }

  public void setLastforeigntaxreviewdate(java.sql.Date lastforeigntaxreviewdate) {
    this.lastforeigntaxreviewdate = lastforeigntaxreviewdate;
  }

  public String getSubmitforkyc() {
    return submitforkyc;
  }

  public void setSubmitforkyc(String submitforkyc) {
    this.submitforkyc = submitforkyc;
  }

  public java.sql.Date getKyc_reviewdate() {
    return kyc_reviewdate;
  }

  public void setKyc_reviewdate(java.sql.Date kyc_reviewdate) {
    this.kyc_reviewdate = kyc_reviewdate;
  }

  public java.sql.Date getKyc_date() {
    return kyc_date;
  }

  public void setKyc_date(java.sql.Date kyc_date) {
    this.kyc_date = kyc_date;
  }

  public String getRiskrating() {
    return riskrating;
  }

  public void setRiskrating(String riskrating) {
    this.riskrating = riskrating;
  }

  public String getIdtyper1() {
    return idtyper1;
  }

  public void setIdtyper1(String idtyper1) {
    this.idtyper1 = idtyper1;
  }

  public String getIdtyper2() {
    return idtyper2;
  }

  public void setIdtyper2(String idtyper2) {
    this.idtyper2 = idtyper2;
  }

  public String getIdtyper3() {
    return idtyper3;
  }

  public void setIdtyper3(String idtyper3) {
    this.idtyper3 = idtyper3;
  }

  public String getIdtyper4() {
    return idtyper4;
  }

  public void setIdtyper4(String idtyper4) {
    this.idtyper4 = idtyper4;
  }

  public String getIdtyper5() {
    return idtyper5;
  }

  public void setIdtyper5(String idtyper5) {
    this.idtyper5 = idtyper5;
  }

  public String getIntroducername_alt1() {
    return introducername_alt1;
  }

  public void setIntroducername_alt1(String introducername_alt1) {
    this.introducername_alt1 = introducername_alt1;
  }

  public String getCust_last_name_alt1() {
    return cust_last_name_alt1;
  }

  public void setCust_last_name_alt1(String cust_last_name_alt1) {
    this.cust_last_name_alt1 = cust_last_name_alt1;
  }

  public String getCust_first_name_alt1() {
    return cust_first_name_alt1;
  }

  public void setCust_first_name_alt1(String cust_first_name_alt1) {
    this.cust_first_name_alt1 = cust_first_name_alt1;
  }

  public String getCust_middle_name_alt1() {
    return cust_middle_name_alt1;
  }

  public void setCust_middle_name_alt1(String cust_middle_name_alt1) {
    this.cust_middle_name_alt1 = cust_middle_name_alt1;
  }

  public String getStrfield6_alt1() {
    return strfield6_alt1;
  }

  public void setStrfield6_alt1(String strfield6_alt1) {
    this.strfield6_alt1 = strfield6_alt1;
  }

  public String getShort_name_alt1() {
    return short_name_alt1;
  }

  public void setShort_name_alt1(String short_name_alt1) {
    this.short_name_alt1 = short_name_alt1;
  }

  public String getName_alt1() {
    return name_alt1;
  }

  public void setName_alt1(String name_alt1) {
    this.name_alt1 = name_alt1;
  }

  public String getCreatedbysystemid() {
    return createdbysystemid;
  }

  public void setCreatedbysystemid(String createdbysystemid) {
    this.createdbysystemid = createdbysystemid;
  }

  public String getModifiedbysystemid() {
    return modifiedbysystemid;
  }

  public void setModifiedbysystemid(String modifiedbysystemid) {
    this.modifiedbysystemid = modifiedbysystemid;
  }

  public String getCorp_id() {
    return corp_id;
  }

  public void setCorp_id(String corp_id) {
    this.corp_id = corp_id;
  }

  public String getPrimarypersonid() {
    return primarypersonid;
  }

  public void setPrimarypersonid(String primarypersonid) {
    this.primarypersonid = primarypersonid;
  }

  public String getOrgkey() {
    return orgkey;
  }

  public void setOrgkey(String orgkey) {
    this.orgkey = orgkey;
  }

  public String getOtuoldcifid() {
    return otuoldcifid;
  }

  public void setOtuoldcifid(String otuoldcifid) {
    this.otuoldcifid = otuoldcifid;
  }

  public String getOrgname() {
    return orgname;
  }

  public void setOrgname(String orgname) {
    this.orgname = orgname;
  }

  public String getOrgtype() {
    return orgtype;
  }

  public void setOrgtype(String orgtype) {
    this.orgtype = orgtype;
  }

  public String getRating() {
    return rating;
  }

  public void setRating(String rating) {
    this.rating = rating;
  }

  public String getAccounttype() {
    return accounttype;
  }

  public void setAccounttype(String accounttype) {
    this.accounttype = accounttype;
  }

  public String getCust_first_name() {
    return cust_first_name;
  }

  public void setCust_first_name(String cust_first_name) {
    this.cust_first_name = cust_first_name;
  }

  public String getCust_middle_name() {
    return cust_middle_name;
  }

  public void setCust_middle_name(String cust_middle_name) {
    this.cust_middle_name = cust_middle_name;
  }

  public String getCust_last_name() {
    return cust_last_name;
  }

  public void setCust_last_name(String cust_last_name) {
    this.cust_last_name = cust_last_name;
  }

  public String getSalutation() {
    return salutation;
  }

  public void setSalutation(String salutation) {
    this.salutation = salutation;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getBirth_day() {
    return birth_day;
  }

  public void setBirth_day(String birth_day) {
    this.birth_day = birth_day;
  }

  public String getBirth_month() {
    return birth_month;
  }

  public void setBirth_month(String birth_month) {
    this.birth_month = birth_month;
  }

  public String getBirth_year() {
    return birth_year;
  }

  public void setBirth_year(String birth_year) {
    this.birth_year = birth_year;
  }

  public java.sql.Date getCust_dob() {
    return cust_dob;
  }

  public void setCust_dob(java.sql.Date cust_dob) {
    this.cust_dob = cust_dob;
  }

  public String getCust_type() {
    return cust_type;
  }

  public void setCust_type(String cust_type) {
    this.cust_type = cust_type;
  }

  public String getEducation() {
    return education;
  }

  public void setEducation(String education) {
    this.education = education;
  }

  public String getOccupation() {
    return occupation;
  }

  public void setOccupation(String occupation) {
    this.occupation = occupation;
  }

  public String getPersontype() {
    return persontype;
  }

  public void setPersontype(String persontype) {
    this.persontype = persontype;
  }

  public String getCust_language() {
    return cust_language;
  }

  public void setCust_language(String cust_language) {
    this.cust_language = cust_language;
  }

  public String getCust_staff_status() {
    return cust_staff_status;
  }

  public void setCust_staff_status(String cust_staff_status) {
    this.cust_staff_status = cust_staff_status;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getFax_home() {
    return fax_home;
  }

  public void setFax_home(String fax_home) {
    this.fax_home = fax_home;
  }

  public String getPhone_home() {
    return phone_home;
  }

  public void setPhone_home(String phone_home) {
    this.phone_home = phone_home;
  }

  public String getPhone_home2() {
    return phone_home2;
  }

  public void setPhone_home2(String phone_home2) {
    this.phone_home2 = phone_home2;
  }

  public String getPhone_cell() {
    return phone_cell;
  }

  public void setPhone_cell(String phone_cell) {
    this.phone_cell = phone_cell;
  }

  public String getEmail_home() {
    return email_home;
  }

  public void setEmail_home(String email_home) {
    this.email_home = email_home;
  }

  public String getEmail_palm() {
    return email_palm;
  }

  public void setEmail_palm(String email_palm) {
    this.email_palm = email_palm;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getPreferredchannelid() {
    return preferredchannelid;
  }

  public void setPreferredchannelid(String preferredchannelid) {
    this.preferredchannelid = preferredchannelid;
  }

  public String getPan() {
    return pan;
  }

  public void setPan(String pan) {
    this.pan = pan;
  }

  public String getPassportno() {
    return passportno;
  }

  public void setPassportno(String passportno) {
    this.passportno = passportno;
  }

  public String getLicenseno() {
    return licenseno;
  }

  public void setLicenseno(String licenseno) {
    this.licenseno = licenseno;
  }

  public String getCustomerrelationshipno() {
    return customerrelationshipno;
  }

  public void setCustomerrelationshipno(String customerrelationshipno) {
    this.customerrelationshipno = customerrelationshipno;
  }

  public java.sql.Date getRelationshipopeningdate() {
    return relationshipopeningdate;
  }

  public void setRelationshipopeningdate(java.sql.Date relationshipopeningdate) {
    this.relationshipopeningdate = relationshipopeningdate;
  }

  public String getRelationshipvalue() {
    return relationshipvalue;
  }

  public void setRelationshipvalue(String relationshipvalue) {
    this.relationshipvalue = relationshipvalue;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getNumberofproducts() {
    return numberofproducts;
  }

  public void setNumberofproducts(String numberofproducts) {
    this.numberofproducts = numberofproducts;
  }

  public String getRelationshipmgrid() {
    return relationshipmgrid;
  }

  public void setRelationshipmgrid(String relationshipmgrid) {
    this.relationshipmgrid = relationshipmgrid;
  }

  public String getRelationshipcreatedbyid() {
    return relationshipcreatedbyid;
  }

  public void setRelationshipcreatedbyid(String relationshipcreatedbyid) {
    this.relationshipcreatedbyid = relationshipcreatedbyid;
  }

  public String getFutureprocess() {
    return futureprocess;
  }

  public void setFutureprocess(String futureprocess) {
    this.futureprocess = futureprocess;
  }

  public String getIntwfid() {
    return intwfid;
  }

  public void setIntwfid(String intwfid) {
    this.intwfid = intwfid;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getIndustry() {
    return industry;
  }

  public void setIndustry(String industry) {
    this.industry = industry;
  }

  public String getParentorg() {
    return parentorg;
  }

  public void setParentorg(String parentorg) {
    this.parentorg = parentorg;
  }

  public String getCompetitor() {
    return competitor;
  }

  public void setCompetitor(String competitor) {
    this.competitor = competitor;
  }

  public String getSiccode() {
    return siccode;
  }

  public void setSiccode(String siccode) {
    this.siccode = siccode;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getCin() {
    return cin;
  }

  public void setCin(String cin) {
    this.cin = cin;
  }

  public String getManager() {
    return manager;
  }

  public void setManager(String manager) {
    this.manager = manager;
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public String getAssistant() {
    return assistant;
  }

  public void setAssistant(String assistant) {
    this.assistant = assistant;
  }

  public String getInternalscore() {
    return internalscore;
  }

  public void setInternalscore(String internalscore) {
    this.internalscore = internalscore;
  }

  public java.sql.Date getCreditbureauscorevalidity() {
    return creditbureauscorevalidity;
  }

  public void setCreditbureauscorevalidity(java.sql.Date creditbureauscorevalidity) {
    this.creditbureauscorevalidity = creditbureauscorevalidity;
  }

  public String getCreditbureauscore() {
    return creditbureauscore;
  }

  public void setCreditbureauscore(String creditbureauscore) {
    this.creditbureauscore = creditbureauscore;
  }

  public java.sql.Date getCreditbureaurequestdate() {
    return creditbureaurequestdate;
  }

  public void setCreditbureaurequestdate(java.sql.Date creditbureaurequestdate) {
    this.creditbureaurequestdate = creditbureaurequestdate;
  }

  public String getCreditbureaudescription() {
    return creditbureaudescription;
  }

  public void setCreditbureaudescription(String creditbureaudescription) {
    this.creditbureaudescription = creditbureaudescription;
  }

  public String getMaidennameofmother() {
    return maidennameofmother;
  }

  public void setMaidennameofmother(String maidennameofmother) {
    this.maidennameofmother = maidennameofmother;
  }

  public String getAnnualrevenue() {
    return annualrevenue;
  }

  public void setAnnualrevenue(String annualrevenue) {
    this.annualrevenue = annualrevenue;
  }

  public String getRevenueunits() {
    return revenueunits;
  }

  public void setRevenueunits(String revenueunits) {
    this.revenueunits = revenueunits;
  }

  public String getTickersymbol() {
    return tickersymbol;
  }

  public void setTickersymbol(String tickersymbol) {
    this.tickersymbol = tickersymbol;
  }

  public String getAutoapproval() {
    return autoapproval;
  }

  public void setAutoapproval(String autoapproval) {
    this.autoapproval = autoapproval;
  }

  public String getFreezeproductsale() {
    return freezeproductsale;
  }

  public void setFreezeproductsale(String freezeproductsale) {
    this.freezeproductsale = freezeproductsale;
  }

  public String getRelationshipfield1() {
    return relationshipfield1;
  }

  public void setRelationshipfield1(String relationshipfield1) {
    this.relationshipfield1 = relationshipfield1;
  }

  public String getRelationshipfield2() {
    return relationshipfield2;
  }

  public void setRelationshipfield2(String relationshipfield2) {
    this.relationshipfield2 = relationshipfield2;
  }

  public String getRelationshipfield3() {
    return relationshipfield3;
  }

  public void setRelationshipfield3(String relationshipfield3) {
    this.relationshipfield3 = relationshipfield3;
  }

  public String getDelinquencyflg() {
    return delinquencyflg;
  }

  public void setDelinquencyflg(String delinquencyflg) {
    this.delinquencyflg = delinquencyflg;
  }

  public String getCustomernreflg() {
    return customernreflg;
  }

  public void setCustomernreflg(String customernreflg) {
    this.customernreflg = customernreflg;
  }

  public String getCustomerminor() {
    return customerminor;
  }

  public void setCustomerminor(String customerminor) {
    this.customerminor = customerminor;
  }

  public String getCombinedstatementflg() {
    return combinedstatementflg;
  }

  public void setCombinedstatementflg(String combinedstatementflg) {
    this.combinedstatementflg = combinedstatementflg;
  }

  public String getCustomertrade() {
    return customertrade;
  }

  public void setCustomertrade(String customertrade) {
    this.customertrade = customertrade;
  }

  public String getUniqueid() {
    return uniqueid;
  }

  public void setUniqueid(String uniqueid) {
    this.uniqueid = uniqueid;
  }

  public String getUniqueidtype() {
    return uniqueidtype;
  }

  public void setUniqueidtype(String uniqueidtype) {
    this.uniqueidtype = uniqueidtype;
  }

  public String getStaffemployeeid() {
    return staffemployeeid;
  }

  public void setStaffemployeeid(String staffemployeeid) {
    this.staffemployeeid = staffemployeeid;
  }

  public String getPlaceofbirth() {
    return placeofbirth;
  }

  public void setPlaceofbirth(String placeofbirth) {
    this.placeofbirth = placeofbirth;
  }

  public String getCountryofbirth() {
    return countryofbirth;
  }

  public void setCountryofbirth(String countryofbirth) {
    this.countryofbirth = countryofbirth;
  }

  public String getProofofageflag() {
    return proofofageflag;
  }

  public void setProofofageflag(String proofofageflag) {
    this.proofofageflag = proofofageflag;
  }

  public String getProofofagedocument() {
    return proofofagedocument;
  }

  public void setProofofagedocument(String proofofagedocument) {
    this.proofofagedocument = proofofagedocument;
  }

  public String getStaffflag() {
    return staffflag;
  }

  public void setStaffflag(String staffflag) {
    this.staffflag = staffflag;
  }

  public String getNamesuffix() {
    return namesuffix;
  }

  public void setNamesuffix(String namesuffix) {
    this.namesuffix = namesuffix;
  }

  public String getMaidenname() {
    return maidenname;
  }

  public void setMaidenname(String maidenname) {
    this.maidenname = maidenname;
  }

  public String getCustomerprofitability() {
    return customerprofitability;
  }

  public void setCustomerprofitability(String customerprofitability) {
    this.customerprofitability = customerprofitability;
  }

  public String getHouseholdid() {
    return householdid;
  }

  public void setHouseholdid(String householdid) {
    this.householdid = householdid;
  }

  public String getHouseholdname() {
    return householdname;
  }

  public void setHouseholdname(String householdname) {
    this.householdname = householdname;
  }

  public String getHshlduflag() {
    return hshlduflag;
  }

  public void setHshlduflag(String hshlduflag) {
    this.hshlduflag = hshlduflag;
  }

  public String getCurrentcrexposure() {
    return currentcrexposure;
  }

  public void setCurrentcrexposure(String currentcrexposure) {
    this.currentcrexposure = currentcrexposure;
  }

  public String getTotalcrexposure() {
    return totalcrexposure;
  }

  public void setTotalcrexposure(String totalcrexposure) {
    this.totalcrexposure = totalcrexposure;
  }

  public String getPotentialcrline() {
    return potentialcrline;
  }

  public void setPotentialcrline(String potentialcrline) {
    this.potentialcrline = potentialcrline;
  }

  public String getAvailablecrlimit() {
    return availablecrlimit;
  }

  public void setAvailablecrlimit(String availablecrlimit) {
    this.availablecrlimit = availablecrlimit;
  }

  public String getCreditscorerequestedflag() {
    return creditscorerequestedflag;
  }

  public void setCreditscorerequestedflag(String creditscorerequestedflag) {
    this.creditscorerequestedflag = creditscorerequestedflag;
  }

  public String getCredithistoryrequestedflag() {
    return credithistoryrequestedflag;
  }

  public void setCredithistoryrequestedflag(String credithistoryrequestedflag) {
    this.credithistoryrequestedflag = credithistoryrequestedflag;
  }

  public String getSpouseid() {
    return spouseid;
  }

  public void setSpouseid(String spouseid) {
    this.spouseid = spouseid;
  }

  public String getSsn() {
    return ssn;
  }

  public void setSsn(String ssn) {
    this.ssn = ssn;
  }

  public String getGroupid() {
    return groupid;
  }

  public void setGroupid(String groupid) {
    this.groupid = groupid;
  }

  public String getFlg1() {
    return flg1;
  }

  public void setFlg1(String flg1) {
    this.flg1 = flg1;
  }

  public String getFlg2() {
    return flg2;
  }

  public void setFlg2(String flg2) {
    this.flg2 = flg2;
  }

  public String getFlg3() {
    return flg3;
  }

  public void setFlg3(String flg3) {
    this.flg3 = flg3;
  }

  public String getAlert1() {
    return alert1;
  }

  public void setAlert1(String alert1) {
    this.alert1 = alert1;
  }

  public String getAlert2() {
    return alert2;
  }

  public void setAlert2(String alert2) {
    this.alert2 = alert2;
  }

  public String getAlert3() {
    return alert3;
  }

  public void setAlert3(String alert3) {
    this.alert3 = alert3;
  }

  public String getRelationshipoffer1() {
    return relationshipoffer1;
  }

  public void setRelationshipoffer1(String relationshipoffer1) {
    this.relationshipoffer1 = relationshipoffer1;
  }

  public String getRelationshipoffer2() {
    return relationshipoffer2;
  }

  public void setRelationshipoffer2(String relationshipoffer2) {
    this.relationshipoffer2 = relationshipoffer2;
  }

  public java.sql.Date getDtdate1() {
    return dtdate1;
  }

  public void setDtdate1(java.sql.Date dtdate1) {
    this.dtdate1 = dtdate1;
  }

  public java.sql.Date getDtdate2() {
    return dtdate2;
  }

  public void setDtdate2(java.sql.Date dtdate2) {
    this.dtdate2 = dtdate2;
  }

  public java.sql.Date getDtdate3() {
    return dtdate3;
  }

  public void setDtdate3(java.sql.Date dtdate3) {
    this.dtdate3 = dtdate3;
  }

  public java.sql.Date getDtdate4() {
    return dtdate4;
  }

  public void setDtdate4(java.sql.Date dtdate4) {
    this.dtdate4 = dtdate4;
  }

  public java.sql.Date getDtdate5() {
    return dtdate5;
  }

  public void setDtdate5(java.sql.Date dtdate5) {
    this.dtdate5 = dtdate5;
  }

  public java.sql.Date getDtdate6() {
    return dtdate6;
  }

  public void setDtdate6(java.sql.Date dtdate6) {
    this.dtdate6 = dtdate6;
  }

  public java.sql.Date getDtdate7() {
    return dtdate7;
  }

  public void setDtdate7(java.sql.Date dtdate7) {
    this.dtdate7 = dtdate7;
  }

  public java.sql.Date getDtdate8() {
    return dtdate8;
  }

  public void setDtdate8(java.sql.Date dtdate8) {
    this.dtdate8 = dtdate8;
  }

  public java.sql.Date getDtdate9() {
    return dtdate9;
  }

  public void setDtdate9(java.sql.Date dtdate9) {
    this.dtdate9 = dtdate9;
  }

  public String getAmount1() {
    return amount1;
  }

  public void setAmount1(String amount1) {
    this.amount1 = amount1;
  }

  public String getAmount2() {
    return amount2;
  }

  public void setAmount2(String amount2) {
    this.amount2 = amount2;
  }

  public String getAmount3() {
    return amount3;
  }

  public void setAmount3(String amount3) {
    this.amount3 = amount3;
  }

  public String getAmount4() {
    return amount4;
  }

  public void setAmount4(String amount4) {
    this.amount4 = amount4;
  }

  public String getAmount5() {
    return amount5;
  }

  public void setAmount5(String amount5) {
    this.amount5 = amount5;
  }

  public String getStrfield1() {
    return strfield1;
  }

  public void setStrfield1(String strfield1) {
    this.strfield1 = strfield1;
  }

  public String getStrfield2() {
    return strfield2;
  }

  public void setStrfield2(String strfield2) {
    this.strfield2 = strfield2;
  }

  public String getStrfield3() {
    return strfield3;
  }

  public void setStrfield3(String strfield3) {
    this.strfield3 = strfield3;
  }

  public String getStrfield4() {
    return strfield4;
  }

  public void setStrfield4(String strfield4) {
    this.strfield4 = strfield4;
  }

  public String getStrfield5() {
    return strfield5;
  }

  public void setStrfield5(String strfield5) {
    this.strfield5 = strfield5;
  }

  public String getStrfield6() {
    return strfield6;
  }

  public void setStrfield6(String strfield6) {
    this.strfield6 = strfield6;
  }

  public String getStrfield7() {
    return strfield7;
  }

  public void setStrfield7(String strfield7) {
    this.strfield7 = strfield7;
  }

  public String getStrfield8() {
    return strfield8;
  }

  public void setStrfield8(String strfield8) {
    this.strfield8 = strfield8;
  }

  public String getStrfield9() {
    return strfield9;
  }

  public void setStrfield9(String strfield9) {
    this.strfield9 = strfield9;
  }

  public String getStrfield10() {
    return strfield10;
  }

  public void setStrfield10(String strfield10) {
    this.strfield10 = strfield10;
  }

  public String getStrfield11() {
    return strfield11;
  }

  public void setStrfield11(String strfield11) {
    this.strfield11 = strfield11;
  }

  public String getStrfield12() {
    return strfield12;
  }

  public void setStrfield12(String strfield12) {
    this.strfield12 = strfield12;
  }

  public String getStrfield13() {
    return strfield13;
  }

  public void setStrfield13(String strfield13) {
    this.strfield13 = strfield13;
  }

  public String getStrfield14() {
    return strfield14;
  }

  public void setStrfield14(String strfield14) {
    this.strfield14 = strfield14;
  }

  public String getStrfield15() {
    return strfield15;
  }

  public void setStrfield15(String strfield15) {
    this.strfield15 = strfield15;
  }

  public String getUserflag1() {
    return userflag1;
  }

  public void setUserflag1(String userflag1) {
    this.userflag1 = userflag1;
  }

  public String getUserflag2() {
    return userflag2;
  }

  public void setUserflag2(String userflag2) {
    this.userflag2 = userflag2;
  }

  public String getUserflag3() {
    return userflag3;
  }

  public void setUserflag3(String userflag3) {
    this.userflag3 = userflag3;
  }

  public String getUserflag4() {
    return userflag4;
  }

  public void setUserflag4(String userflag4) {
    this.userflag4 = userflag4;
  }

  public String getMluserfield1() {
    return mluserfield1;
  }

  public void setMluserfield1(String mluserfield1) {
    this.mluserfield1 = mluserfield1;
  }

  public String getMluserfield2() {
    return mluserfield2;
  }

  public void setMluserfield2(String mluserfield2) {
    this.mluserfield2 = mluserfield2;
  }

  public String getMluserfield3() {
    return mluserfield3;
  }

  public void setMluserfield3(String mluserfield3) {
    this.mluserfield3 = mluserfield3;
  }

  public String getMluserfield4() {
    return mluserfield4;
  }

  public void setMluserfield4(String mluserfield4) {
    this.mluserfield4 = mluserfield4;
  }

  public String getMluserfield5() {
    return mluserfield5;
  }

  public void setMluserfield5(String mluserfield5) {
    this.mluserfield5 = mluserfield5;
  }

  public String getMluserfield6() {
    return mluserfield6;
  }

  public void setMluserfield6(String mluserfield6) {
    this.mluserfield6 = mluserfield6;
  }

  public String getMluserfield7() {
    return mluserfield7;
  }

  public void setMluserfield7(String mluserfield7) {
    this.mluserfield7 = mluserfield7;
  }

  public String getMluserfield8() {
    return mluserfield8;
  }

  public void setMluserfield8(String mluserfield8) {
    this.mluserfield8 = mluserfield8;
  }

  public String getMluserfield9() {
    return mluserfield9;
  }

  public void setMluserfield9(String mluserfield9) {
    this.mluserfield9 = mluserfield9;
  }

  public String getMluserfield10() {
    return mluserfield10;
  }

  public void setMluserfield10(String mluserfield10) {
    this.mluserfield10 = mluserfield10;
  }

  public String getMluserfield11() {
    return mluserfield11;
  }

  public void setMluserfield11(String mluserfield11) {
    this.mluserfield11 = mluserfield11;
  }

  public java.sql.Date getDateofbecomingnre() {
    return dateofbecomingnre;
  }

  public void setDateofbecomingnre(java.sql.Date dateofbecomingnre) {
    this.dateofbecomingnre = dateofbecomingnre;
  }

  public java.sql.Date getBodatecreated() {
    return bodatecreated;
  }

  public void setBodatecreated(java.sql.Date bodatecreated) {
    this.bodatecreated = bodatecreated;
  }

  public String getBocreatedby() {
    return bocreatedby;
  }

  public void setBocreatedby(String bocreatedby) {
    this.bocreatedby = bocreatedby;
  }

  public java.sql.Date getBodatemodified() {
    return bodatemodified;
  }

  public void setBodatemodified(java.sql.Date bodatemodified) {
    this.bodatemodified = bodatemodified;
  }

  public String getBomodifiedby() {
    return bomodifiedby;
  }

  public void setBomodifiedby(String bomodifiedby) {
    this.bomodifiedby = bomodifiedby;
  }

  public String getCreateduserid() {
    return createduserid;
  }

  public void setCreateduserid(String createduserid) {
    this.createduserid = createduserid;
  }

  public String getOwneduserid() {
    return owneduserid;
  }

  public void setOwneduserid(String owneduserid) {
    this.owneduserid = owneduserid;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public String getExtensionid() {
    return extensionid;
  }

  public void setExtensionid(String extensionid) {
    this.extensionid = extensionid;
  }

  public String getXmldata() {
    return xmldata;
  }

  public void setXmldata(String xmldata) {
    this.xmldata = xmldata;
  }

  public String getConcurdetect_x() {
    return concurdetect_x;
  }

  public void setConcurdetect_x(String concurdetect_x) {
    this.concurdetect_x = concurdetect_x;
  }

  public String getBoaclid() {
    return boaclid;
  }

  public void setBoaclid(String boaclid) {
    this.boaclid = boaclid;
  }

  public String getSecurityilhint() {
    return securityilhint;
  }

  public void setSecurityilhint(String securityilhint) {
    this.securityilhint = securityilhint;
  }

  public String getSecurityiuhint() {
    return securityiuhint;
  }

  public void setSecurityiuhint(String securityiuhint) {
    this.securityiuhint = securityiuhint;
  }

  public String getSecurityglhint() {
    return securityglhint;
  }

  public void setSecurityglhint(String securityglhint) {
    this.securityglhint = securityglhint;
  }

  public String getSecurityguhint() {
    return securityguhint;
  }

  public void setSecurityguhint(String securityguhint) {
    this.securityguhint = securityguhint;
  }

  public String getPrioritycode() {
    return prioritycode;
  }

  public void setPrioritycode(String prioritycode) {
    this.prioritycode = prioritycode;
  }

  public String getCreated_from() {
    return created_from;
  }

  public void setCreated_from(String created_from) {
    this.created_from = created_from;
  }

  public String getConstitution_code() {
    return constitution_code;
  }

  public void setConstitution_code(String constitution_code) {
    this.constitution_code = constitution_code;
  }

  public String getStrfield18() {
    return strfield18;
  }

  public void setStrfield18(String strfield18) {
    this.strfield18 = strfield18;
  }

  public String getStrfield16() {
    return strfield16;
  }

  public void setStrfield16(String strfield16) {
    this.strfield16 = strfield16;
  }

  public String getStrfield17() {
    return strfield17;
  }

  public void setStrfield17(String strfield17) {
    this.strfield17 = strfield17;
  }

  public String getStrfield19() {
    return strfield19;
  }

  public void setStrfield19(String strfield19) {
    this.strfield19 = strfield19;
  }

  public String getStrfield20() {
    return strfield20;
  }

  public void setStrfield20(String strfield20) {
    this.strfield20 = strfield20;
  }

  public String getStrfield21() {
    return strfield21;
  }

  public void setStrfield21(String strfield21) {
    this.strfield21 = strfield21;
  }

  public String getStrfield22() {
    return strfield22;
  }

  public void setStrfield22(String strfield22) {
    this.strfield22 = strfield22;
  }

  public String getAmount6() {
    return amount6;
  }

  public void setAmount6(String amount6) {
    this.amount6 = amount6;
  }

  public String getAmount7() {
    return amount7;
  }

  public void setAmount7(String amount7) {
    this.amount7 = amount7;
  }

  public String getAmount8() {
    return amount8;
  }

  public void setAmount8(String amount8) {
    this.amount8 = amount8;
  }

  public String getAmount9() {
    return amount9;
  }

  public void setAmount9(String amount9) {
    this.amount9 = amount9;
  }

  public String getAmount10() {
    return amount10;
  }

  public void setAmount10(String amount10) {
    this.amount10 = amount10;
  }

  public String getAmount11() {
    return amount11;
  }

  public void setAmount11(String amount11) {
    this.amount11 = amount11;
  }

  public String getAmount12() {
    return amount12;
  }

  public void setAmount12(String amount12) {
    this.amount12 = amount12;
  }

  public String getIntfield1() {
    return intfield1;
  }

  public void setIntfield1(String intfield1) {
    this.intfield1 = intfield1;
  }

  public String getIntfield2() {
    return intfield2;
  }

  public void setIntfield2(String intfield2) {
    this.intfield2 = intfield2;
  }

  public String getIntfield3() {
    return intfield3;
  }

  public void setIntfield3(String intfield3) {
    this.intfield3 = intfield3;
  }

  public String getIntfield4() {
    return intfield4;
  }

  public void setIntfield4(String intfield4) {
    this.intfield4 = intfield4;
  }

  public String getIntfield5() {
    return intfield5;
  }

  public void setIntfield5(String intfield5) {
    this.intfield5 = intfield5;
  }

  public String getCustclass() {
    return custclass;
  }

  public void setCustclass(String custclass) {
    this.custclass = custclass;
  }

  public String getShort_name() {
    return short_name;
  }

  public void setShort_name(String short_name) {
    this.short_name = short_name;
  }

  public String getNick_name() {
    return nick_name;
  }

  public void setNick_name(String nick_name) {
    this.nick_name = nick_name;
  }

  public String getMother_name() {
    return mother_name;
  }

  public void setMother_name(String mother_name) {
    this.mother_name = mother_name;
  }

  public String getFather_husband_name() {
    return father_husband_name;
  }

  public void setFather_husband_name(String father_husband_name) {
    this.father_husband_name = father_husband_name;
  }

  public String getPrevious_name() {
    return previous_name;
  }

  public void setPrevious_name(String previous_name) {
    this.previous_name = previous_name;
  }

  public String getPrimary_service_centre() {
    return primary_service_centre;
  }

  public void setPrimary_service_centre(String primary_service_centre) {
    this.primary_service_centre = primary_service_centre;
  }

  public String getLead_source() {
    return lead_source;
  }

  public void setLead_source(String lead_source) {
    this.lead_source = lead_source;
  }

  public String getRelationship_type() {
    return relationship_type;
  }

  public void setRelationship_type(String relationship_type) {
    this.relationship_type = relationship_type;
  }

  public String getRelationship_level() {
    return relationship_level;
  }

  public void setRelationship_level(String relationship_level) {
    this.relationship_level = relationship_level;
  }

  public String getRm_group_id() {
    return rm_group_id;
  }

  public void setRm_group_id(String rm_group_id) {
    this.rm_group_id = rm_group_id;
  }

  public String getDsa_id() {
    return dsa_id;
  }

  public void setDsa_id(String dsa_id) {
    this.dsa_id = dsa_id;
  }

  public String getCard_holder() {
    return card_holder;
  }

  public void setCard_holder(String card_holder) {
    this.card_holder = card_holder;
  }

  public String getPhotograph_id() {
    return photograph_id;
  }

  public void setPhotograph_id(String photograph_id) {
    this.photograph_id = photograph_id;
  }

  public String getSecure_id() {
    return secure_id;
  }

  public void setSecure_id(String secure_id) {
    this.secure_id = secure_id;
  }

  public String getBlacklisted() {
    return blacklisted;
  }

  public void setBlacklisted(String blacklisted) {
    this.blacklisted = blacklisted;
  }

  public String getNegated() {
    return negated;
  }

  public void setNegated(String negated) {
    this.negated = negated;
  }

  public String getSuspended() {
    return suspended;
  }

  public void setSuspended(String suspended) {
    this.suspended = suspended;
  }

  public String getDeliquencyperiod() {
    return deliquencyperiod;
  }

  public void setDeliquencyperiod(String deliquencyperiod) {
    this.deliquencyperiod = deliquencyperiod;
  }

  public String getAddname1() {
    return addname1;
  }

  public void setAddname1(String addname1) {
    this.addname1 = addname1;
  }

  public String getAddname2() {
    return addname2;
  }

  public void setAddname2(String addname2) {
    this.addname2 = addname2;
  }

  public String getAddname3() {
    return addname3;
  }

  public void setAddname3(String addname3) {
    this.addname3 = addname3;
  }

  public String getAddname4() {
    return addname4;
  }

  public void setAddname4(String addname4) {
    this.addname4 = addname4;
  }

  public String getAddname5() {
    return addname5;
  }

  public void setAddname5(String addname5) {
    this.addname5 = addname5;
  }

  public String getPreferredname() {
    return preferredname;
  }

  public void setPreferredname(String preferredname) {
    this.preferredname = preferredname;
  }

  public java.sql.Date getOldentitycreatedon() {
    return oldentitycreatedon;
  }

  public void setOldentitycreatedon(java.sql.Date oldentitycreatedon) {
    this.oldentitycreatedon = oldentitycreatedon;
  }

  public String getOldentitytype() {
    return oldentitytype;
  }

  public void setOldentitytype(String oldentitytype) {
    this.oldentitytype = oldentitytype;
  }

  public String getOldentityid() {
    return oldentityid;
  }

  public void setOldentityid(String oldentityid) {
    this.oldentityid = oldentityid;
  }

  public String getRecordstatus() {
    return recordstatus;
  }

  public void setRecordstatus(String recordstatus) {
    this.recordstatus = recordstatus;
  }

  public String getAssignedto() {
    return assignedto;
  }

  public void setAssignedto(String assignedto) {
    this.assignedto = assignedto;
  }

  public String getProcessid() {
    return processid;
  }

  public void setProcessid(String processid) {
    this.processid = processid;
  }

  public String getCurrentstep() {
    return currentstep;
  }

  public void setCurrentstep(String currentstep) {
    this.currentstep = currentstep;
  }

  public String getProcessstatus() {
    return processstatus;
  }

  public void setProcessstatus(String processstatus) {
    this.processstatus = processstatus;
  }

  public String getDocument_received() {
    return document_received;
  }

  public void setDocument_received(String document_received) {
    this.document_received = document_received;
  }

  public String getSuspend_notes() {
    return suspend_notes;
  }

  public void setSuspend_notes(String suspend_notes) {
    this.suspend_notes = suspend_notes;
  }

  public String getSuspend_reason() {
    return suspend_reason;
  }

  public void setSuspend_reason(String suspend_reason) {
    this.suspend_reason = suspend_reason;
  }

  public String getBlacklist_notes() {
    return blacklist_notes;
  }

  public void setBlacklist_notes(String blacklist_notes) {
    this.blacklist_notes = blacklist_notes;
  }

  public String getBlacklist_reason() {
    return blacklist_reason;
  }

  public void setBlacklist_reason(String blacklist_reason) {
    this.blacklist_reason = blacklist_reason;
  }

  public String getNegated_notes() {
    return negated_notes;
  }

  public void setNegated_notes(String negated_notes) {
    this.negated_notes = negated_notes;
  }

  public String getNegated_reason() {
    return negated_reason;
  }

  public void setNegated_reason(String negated_reason) {
    this.negated_reason = negated_reason;
  }

  public String getSegmentation_class() {
    return segmentation_class;
  }

  public void setSegmentation_class(String segmentation_class) {
    this.segmentation_class = segmentation_class;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public java.sql.Date getRatingdate() {
    return ratingdate;
  }

  public void setRatingdate(java.sql.Date ratingdate) {
    this.ratingdate = ratingdate;
  }

  public String getManageropinion() {
    return manageropinion;
  }

  public void setManageropinion(String manageropinion) {
    this.manageropinion = manageropinion;
  }

  public String getIntroducerid() {
    return introducerid;
  }

  public void setIntroducerid(String introducerid) {
    this.introducerid = introducerid;
  }

  public String getIntroducersalutation() {
    return introducersalutation;
  }

  public void setIntroducersalutation(String introducersalutation) {
    this.introducersalutation = introducersalutation;
  }

  public String getIntroducername() {
    return introducername;
  }

  public void setIntroducername(String introducername) {
    this.introducername = introducername;
  }

  public String getIntroducerstatuscode() {
    return introducerstatuscode;
  }

  public void setIntroducerstatuscode(String introducerstatuscode) {
    this.introducerstatuscode = introducerstatuscode;
  }

  public String getIntrod_status() {
    return introd_status;
  }

  public void setIntrod_status(String introd_status) {
    this.introd_status = introd_status;
  }

  public java.sql.Date getCuststatuschgdate() {
    return custstatuschgdate;
  }

  public void setCuststatuschgdate(java.sql.Date custstatuschgdate) {
    this.custstatuschgdate = custstatuschgdate;
  }

  public String getPurgeflag() {
    return purgeflag;
  }

  public void setPurgeflag(String purgeflag) {
    this.purgeflag = purgeflag;
  }

  public String getPurgeremarks() {
    return purgeremarks;
  }

  public void setPurgeremarks(String purgeremarks) {
    this.purgeremarks = purgeremarks;
  }

  public String getTfpartyflag() {
    return tfpartyflag;
  }

  public void setTfpartyflag(String tfpartyflag) {
    this.tfpartyflag = tfpartyflag;
  }

  public String getNativelangname() {
    return nativelangname;
  }

  public void setNativelangname(String nativelangname) {
    this.nativelangname = nativelangname;
  }

  public String getNativelangtitle() {
    return nativelangtitle;
  }

  public void setNativelangtitle(String nativelangtitle) {
    this.nativelangtitle = nativelangtitle;
  }

  public String getNativelangcode() {
    return nativelangcode;
  }

  public void setNativelangcode(String nativelangcode) {
    this.nativelangcode = nativelangcode;
  }

  public java.sql.Date getMinorattainmajordate() {
    return minorattainmajordate;
  }

  public void setMinorattainmajordate(java.sql.Date minorattainmajordate) {
    this.minorattainmajordate = minorattainmajordate;
  }

  public java.sql.Date getNrebecomingorddate() {
    return nrebecomingorddate;
  }

  public void setNrebecomingorddate(java.sql.Date nrebecomingorddate) {
    this.nrebecomingorddate = nrebecomingorddate;
  }

  public String getDefaultaddresstype() {
    return defaultaddresstype;
  }

  public void setDefaultaddresstype(String defaultaddresstype) {
    this.defaultaddresstype = defaultaddresstype;
  }

  public String getPreferredemailtype() {
    return preferredemailtype;
  }

  public void setPreferredemailtype(String preferredemailtype) {
    this.preferredemailtype = preferredemailtype;
  }

  public String getPreferredphone() {
    return preferredphone;
  }

  public void setPreferredphone(String preferredphone) {
    this.preferredphone = preferredphone;
  }

  public java.sql.Date getCurrstepduedate() {
    return currstepduedate;
  }

  public void setCurrstepduedate(java.sql.Date currstepduedate) {
    this.currstepduedate = currstepduedate;
  }

  public java.sql.Date getStartdate() {
    return startdate;
  }

  public void setStartdate(java.sql.Date startdate) {
    this.startdate = startdate;
  }

  public String getStagename() {
    return stagename;
  }

  public void setStagename(String stagename) {
    this.stagename = stagename;
  }

  public String getAssignedtogroup() {
    return assignedtogroup;
  }

  public void setAssignedtogroup(String assignedtogroup) {
    this.assignedtogroup = assignedtogroup;
  }

  public String getAdd1_first_name() {
    return add1_first_name;
  }

  public void setAdd1_first_name(String add1_first_name) {
    this.add1_first_name = add1_first_name;
  }

  public String getAdd1_middle_name() {
    return add1_middle_name;
  }

  public void setAdd1_middle_name(String add1_middle_name) {
    this.add1_middle_name = add1_middle_name;
  }

  public String getAdd1_last_name() {
    return add1_last_name;
  }

  public void setAdd1_last_name(String add1_last_name) {
    this.add1_last_name = add1_last_name;
  }

  public String getAdd2_first_name() {
    return add2_first_name;
  }

  public void setAdd2_first_name(String add2_first_name) {
    this.add2_first_name = add2_first_name;
  }

  public String getAdd2_middle_name() {
    return add2_middle_name;
  }

  public void setAdd2_middle_name(String add2_middle_name) {
    this.add2_middle_name = add2_middle_name;
  }

  public String getAdd2_last_name() {
    return add2_last_name;
  }

  public void setAdd2_last_name(String add2_last_name) {
    this.add2_last_name = add2_last_name;
  }

  public String getAdd3_first_name() {
    return add3_first_name;
  }

  public void setAdd3_first_name(String add3_first_name) {
    this.add3_first_name = add3_first_name;
  }

  public String getAdd3_middle_name() {
    return add3_middle_name;
  }

  public void setAdd3_middle_name(String add3_middle_name) {
    this.add3_middle_name = add3_middle_name;
  }

  public String getAdd3_last_name() {
    return add3_last_name;
  }

  public void setAdd3_last_name(String add3_last_name) {
    this.add3_last_name = add3_last_name;
  }

  public String getAdd4_first_name() {
    return add4_first_name;
  }

  public void setAdd4_first_name(String add4_first_name) {
    this.add4_first_name = add4_first_name;
  }

  public String getAdd4_middle_name() {
    return add4_middle_name;
  }

  public void setAdd4_middle_name(String add4_middle_name) {
    this.add4_middle_name = add4_middle_name;
  }

  public String getAdd4_last_name() {
    return add4_last_name;
  }

  public void setAdd4_last_name(String add4_last_name) {
    this.add4_last_name = add4_last_name;
  }

  public String getAdd5_first_name() {
    return add5_first_name;
  }

  public void setAdd5_first_name(String add5_first_name) {
    this.add5_first_name = add5_first_name;
  }

  public String getAdd5_middle_name() {
    return add5_middle_name;
  }

  public void setAdd5_middle_name(String add5_middle_name) {
    this.add5_middle_name = add5_middle_name;
  }

  public String getAdd5_last_name() {
    return add5_last_name;
  }

  public void setAdd5_last_name(String add5_last_name) {
    this.add5_last_name = add5_last_name;
  }

  public String getDual_first_name() {
    return dual_first_name;
  }

  public void setDual_first_name(String dual_first_name) {
    this.dual_first_name = dual_first_name;
  }

  public String getDual_middle_name() {
    return dual_middle_name;
  }

  public void setDual_middle_name(String dual_middle_name) {
    this.dual_middle_name = dual_middle_name;
  }

  public String getDual_last_name() {
    return dual_last_name;
  }

  public void setDual_last_name(String dual_last_name) {
    this.dual_last_name = dual_last_name;
  }

  public String getCust_commu_code() {
    return cust_commu_code;
  }

  public void setCust_commu_code(String cust_commu_code) {
    this.cust_commu_code = cust_commu_code;
  }

  public String getCust_community() {
    return cust_community;
  }

  public void setCust_community(String cust_community) {
    this.cust_community = cust_community;
  }

  public String getCore_introd_cust_id() {
    return core_introd_cust_id;
  }

  public void setCore_introd_cust_id(String core_introd_cust_id) {
    this.core_introd_cust_id = core_introd_cust_id;
  }

  public String getIntrod_salutation_code() {
    return introd_salutation_code;
  }

  public void setIntrod_salutation_code(String introd_salutation_code) {
    this.introd_salutation_code = introd_salutation_code;
  }

  public String getCust_hlth_code() {
    return cust_hlth_code;
  }

  public void setCust_hlth_code(String cust_hlth_code) {
    this.cust_hlth_code = cust_hlth_code;
  }

  public String getCust_hlth() {
    return cust_hlth;
  }

  public void setCust_hlth(String cust_hlth) {
    this.cust_hlth = cust_hlth;
  }

  public String getTds_tbl_code() {
    return tds_tbl_code;
  }

  public void setTds_tbl_code(String tds_tbl_code) {
    this.tds_tbl_code = tds_tbl_code;
  }

  public String getTds_tbl() {
    return tds_tbl;
  }

  public void setTds_tbl(String tds_tbl) {
    this.tds_tbl = tds_tbl;
  }

  public String getTds_cust_id() {
    return tds_cust_id;
  }

  public void setTds_cust_id(String tds_cust_id) {
    this.tds_cust_id = tds_cust_id;
  }

  public String getNat_id_card_num() {
    return nat_id_card_num;
  }

  public void setNat_id_card_num(String nat_id_card_num) {
    this.nat_id_card_num = nat_id_card_num;
  }

  public java.sql.Date getPsprt_issue_date() {
    return psprt_issue_date;
  }

  public void setPsprt_issue_date(java.sql.Date psprt_issue_date) {
    this.psprt_issue_date = psprt_issue_date;
  }

  public String getPsprt_det() {
    return psprt_det;
  }

  public void setPsprt_det(String psprt_det) {
    this.psprt_det = psprt_det;
  }

  public java.sql.Date getPsprt_exp_date() {
    return psprt_exp_date;
  }

  public void setPsprt_exp_date(java.sql.Date psprt_exp_date) {
    this.psprt_exp_date = psprt_exp_date;
  }

  public java.sql.Date getCust_pref_till_date() {
    return cust_pref_till_date;
  }

  public void setCust_pref_till_date(java.sql.Date cust_pref_till_date) {
    this.cust_pref_till_date = cust_pref_till_date;
  }

  public String getCrncy_code() {
    return crncy_code;
  }

  public void setCrncy_code(String crncy_code) {
    this.crncy_code = crncy_code;
  }

  public String getPrimary_sol_id() {
    return primary_sol_id;
  }

  public void setPrimary_sol_id(String primary_sol_id) {
    this.primary_sol_id = primary_sol_id;
  }

  public String getOffline_cum_debit_limit() {
    return offline_cum_debit_limit;
  }

  public void setOffline_cum_debit_limit(String offline_cum_debit_limit) {
    this.offline_cum_debit_limit = offline_cum_debit_limit;
  }

  public String getPref_code() {
    return pref_code;
  }

  public void setPref_code(String pref_code) {
    this.pref_code = pref_code;
  }

  public String getIntrod_status_code() {
    return introd_status_code;
  }

  public void setIntrod_status_code(String introd_status_code) {
    this.introd_status_code = introd_status_code;
  }

  public String getNativelangtitle_code() {
    return nativelangtitle_code;
  }

  public void setNativelangtitle_code(String nativelangtitle_code) {
    this.nativelangtitle_code = nativelangtitle_code;
  }

  public String getCore_cust_id() {
    return core_cust_id;
  }

  public void setCore_cust_id(String core_cust_id) {
    this.core_cust_id = core_cust_id;
  }

  public String getEntity_cre_flag() {
    return entity_cre_flag;
  }

  public void setEntity_cre_flag(String entity_cre_flag) {
    this.entity_cre_flag = entity_cre_flag;
  }

  public String getSalutation_code() {
    return salutation_code;
  }

  public void setSalutation_code(String salutation_code) {
    this.salutation_code = salutation_code;
  }

  public String getGroupid_code() {
    return groupid_code;
  }

  public void setGroupid_code(String groupid_code) {
    this.groupid_code = groupid_code;
  }

  public String getOccupation_code() {
    return occupation_code;
  }

  public void setOccupation_code(String occupation_code) {
    this.occupation_code = occupation_code;
  }

  public String getSector_code() {
    return sector_code;
  }

  public void setSector_code(String sector_code) {
    this.sector_code = sector_code;
  }

  public String getSector() {
    return sector;
  }

  public void setSector(String sector) {
    this.sector = sector;
  }

  public String getSubsector_code() {
    return subsector_code;
  }

  public void setSubsector_code(String subsector_code) {
    this.subsector_code = subsector_code;
  }

  public String getSubsector() {
    return subsector;
  }

  public void setSubsector(String subsector) {
    this.subsector = subsector;
  }

  public String getRating_code() {
    return rating_code;
  }

  public void setRating_code(String rating_code) {
    this.rating_code = rating_code;
  }

  public String getCust_type_code() {
    return cust_type_code;
  }

  public void setCust_type_code(String cust_type_code) {
    this.cust_type_code = cust_type_code;
  }

  public String getStatus_code() {
    return status_code;
  }

  public void setStatus_code(String status_code) {
    this.status_code = status_code;
  }

  public String getConstitution_ref_code() {
    return constitution_ref_code;
  }

  public void setConstitution_ref_code(String constitution_ref_code) {
    this.constitution_ref_code = constitution_ref_code;
  }

  public String getCustcreationmode() {
    return custcreationmode;
  }

  public void setCustcreationmode(String custcreationmode) {
    this.custcreationmode = custcreationmode;
  }

  public String getMinor_guard_code() {
    return minor_guard_code;
  }

  public void setMinor_guard_code(String minor_guard_code) {
    this.minor_guard_code = minor_guard_code;
  }

  public String getMinor_guard_name() {
    return minor_guard_name;
  }

  public void setMinor_guard_name(String minor_guard_name) {
    this.minor_guard_name = minor_guard_name;
  }

  public String getMinor_guard_name_alt1() {
    return minor_guard_name_alt1;
  }

  public void setMinor_guard_name_alt1(String minor_guard_name_alt1) {
    this.minor_guard_name_alt1 = minor_guard_name_alt1;
  }

  public String getOnetime_oldcustid() {
    return onetime_oldcustid;
  }

  public void setOnetime_oldcustid(String onetime_oldcustid) {
    this.onetime_oldcustid = onetime_oldcustid;
  }

  public java.sql.Date getIncrementaldate() {
    return incrementaldate;
  }

  public void setIncrementaldate(java.sql.Date incrementaldate) {
    this.incrementaldate = incrementaldate;
  }

  public String getProcessgroupid() {
    return processgroupid;
  }

  public void setProcessgroupid(String processgroupid) {
    this.processgroupid = processgroupid;
  }

  public String getAssignedbyuserid() {
    return assignedbyuserid;
  }

  public void setAssignedbyuserid(String assignedbyuserid) {
    this.assignedbyuserid = assignedbyuserid;
  }

  public String getTransferredbyuserid() {
    return transferredbyuserid;
  }

  public void setTransferredbyuserid(String transferredbyuserid) {
    this.transferredbyuserid = transferredbyuserid;
  }

  public java.sql.Date getPrevstependdate() {
    return prevstependdate;
  }

  public void setPrevstependdate(java.sql.Date prevstependdate) {
    this.prevstependdate = prevstependdate;
  }

  public String getFirst_product_processor() {
    return first_product_processor;
  }

  public void setFirst_product_processor(String first_product_processor) {
    this.first_product_processor = first_product_processor;
  }

  public String getInterface_reference_id() {
    return interface_reference_id;
  }

  public void setInterface_reference_id(String interface_reference_id) {
    this.interface_reference_id = interface_reference_id;
  }

  public String getCust_health_ref_code() {
    return cust_health_ref_code;
  }

  public void setCust_health_ref_code(String cust_health_ref_code) {
    this.cust_health_ref_code = cust_health_ref_code;
  }

  public String getTds_cifid() {
    return tds_cifid;
  }

  public void setTds_cifid(String tds_cifid) {
    this.tds_cifid = tds_cifid;
  }

  public String getCorprepcount() {
    return corprepcount;
  }

  public void setCorprepcount(String corprepcount) {
    this.corprepcount = corprepcount;
  }

  public String getIscorprep() {
    return iscorprep;
  }

  public void setIscorprep(String iscorprep) {
    this.iscorprep = iscorprep;
  }

  public String getCust_first_name_native() {
    return cust_first_name_native;
  }

  public void setCust_first_name_native(String cust_first_name_native) {
    this.cust_first_name_native = cust_first_name_native;
  }

  public String getCust_middle_name_native() {
    return cust_middle_name_native;
  }

  public void setCust_middle_name_native(String cust_middle_name_native) {
    this.cust_middle_name_native = cust_middle_name_native;
  }

  public String getCust_last_name_native() {
    return cust_last_name_native;
  }

  public void setCust_last_name_native(String cust_last_name_native) {
    this.cust_last_name_native = cust_last_name_native;
  }

  public String getShort_name_native() {
    return short_name_native;
  }

  public void setShort_name_native(String short_name_native) {
    this.short_name_native = short_name_native;
  }

  public String getCust_first_name_native1() {
    return cust_first_name_native1;
  }

  public void setCust_first_name_native1(String cust_first_name_native1) {
    this.cust_first_name_native1 = cust_first_name_native1;
  }

  public String getCust_middle_name_native1() {
    return cust_middle_name_native1;
  }

  public void setCust_middle_name_native1(String cust_middle_name_native1) {
    this.cust_middle_name_native1 = cust_middle_name_native1;
  }

  public String getCust_last_name_native1() {
    return cust_last_name_native1;
  }

  public void setCust_last_name_native1(String cust_last_name_native1) {
    this.cust_last_name_native1 = cust_last_name_native1;
  }

  public String getShort_name_native1() {
    return short_name_native1;
  }

  public void setShort_name_native1(String short_name_native1) {
    this.short_name_native1 = short_name_native1;
  }

  public String getSecondaryrm_id() {
    return secondaryrm_id;
  }

  public void setSecondaryrm_id(String secondaryrm_id) {
    this.secondaryrm_id = secondaryrm_id;
  }

  public String getIsebankingenabled() {
    return isebankingenabled;
  }

  public void setIsebankingenabled(String isebankingenabled) {
    this.isebankingenabled = isebankingenabled;
  }

  public String getIssmsbankingenabled() {
    return issmsbankingenabled;
  }

  public void setIssmsbankingenabled(String issmsbankingenabled) {
    this.issmsbankingenabled = issmsbankingenabled;
  }

  public String getTertiaryrm_id() {
    return tertiaryrm_id;
  }

  public void setTertiaryrm_id(String tertiaryrm_id) {
    this.tertiaryrm_id = tertiaryrm_id;
  }

  public String getSubsegment() {
    return subsegment;
  }

  public void setSubsegment(String subsegment) {
    this.subsegment = subsegment;
  }

  public String getIswapbankingenabled() {
    return iswapbankingenabled;
  }

  public void setIswapbankingenabled(String iswapbankingenabled) {
    this.iswapbankingenabled = iswapbankingenabled;
  }

  public String getAlreadycreatedinebanking() {
    return alreadycreatedinebanking;
  }

  public void setAlreadycreatedinebanking(String alreadycreatedinebanking) {
    this.alreadycreatedinebanking = alreadycreatedinebanking;
  }

  public String getPref_code_rcode() {
    return pref_code_rcode;
  }

  public void setPref_code_rcode(String pref_code_rcode) {
    this.pref_code_rcode = pref_code_rcode;
  }

  public String getCust_swift_code_desc() {
    return cust_swift_code_desc;
  }

  public void setCust_swift_code_desc(String cust_swift_code_desc) {
    this.cust_swift_code_desc = cust_swift_code_desc;
  }

  public String getSmsbankingmobilenumber() {
    return smsbankingmobilenumber;
  }

  public void setSmsbankingmobilenumber(String smsbankingmobilenumber) {
    this.smsbankingmobilenumber = smsbankingmobilenumber;
  }

  public String getIs_swift_code_of_bank() {
    return is_swift_code_of_bank;
  }

  public void setIs_swift_code_of_bank(String is_swift_code_of_bank) {
    this.is_swift_code_of_bank = is_swift_code_of_bank;
  }

  public String getNativelangcode_code() {
    return nativelangcode_code;
  }

  public void setNativelangcode_code(String nativelangcode_code) {
    this.nativelangcode_code = nativelangcode_code;
  }

  public String getAccessownergroup() {
    return accessownergroup;
  }

  public void setAccessownergroup(String accessownergroup) {
    this.accessownergroup = accessownergroup;
  }

  public String getAccessownersegment() {
    return accessownersegment;
  }

  public void setAccessownersegment(String accessownersegment) {
    this.accessownersegment = accessownersegment;
  }

  public String getAccessownerbc() {
    return accessownerbc;
  }

  public void setAccessownerbc(String accessownerbc) {
    this.accessownerbc = accessownerbc;
  }

  public String getAccessowneragent() {
    return accessowneragent;
  }

  public void setAccessowneragent(String accessowneragent) {
    this.accessowneragent = accessowneragent;
  }

  public String getAccessassigneeagent() {
    return accessassigneeagent;
  }

  public void setAccessassigneeagent(String accessassigneeagent) {
    this.accessassigneeagent = accessassigneeagent;
  }

  public String getTabvalidator() {
    return tabvalidator;
  }

  public void setTabvalidator(String tabvalidator) {
    this.tabvalidator = tabvalidator;
  }

  public String getLasteditedpage() {
    return lasteditedpage;
  }

  public void setLasteditedpage(String lasteditedpage) {
    this.lasteditedpage = lasteditedpage;
  }

  public String getChargelevelcode() {
    return chargelevelcode;
  }

  public void setChargelevelcode(String chargelevelcode) {
    this.chargelevelcode = chargelevelcode;
  }

  public java.sql.Date getSegupdatedate() {
    return segupdatedate;
  }

  public void setSegupdatedate(java.sql.Date segupdatedate) {
    this.segupdatedate = segupdatedate;
  }

  public String getIntuserfield1() {
    return intuserfield1;
  }

  public void setIntuserfield1(String intuserfield1) {
    this.intuserfield1 = intuserfield1;
  }

  public String getIntuserfield2() {
    return intuserfield2;
  }

  public void setIntuserfield2(String intuserfield2) {
    this.intuserfield2 = intuserfield2;
  }

  public String getIntuserfield3() {
    return intuserfield3;
  }

  public void setIntuserfield3(String intuserfield3) {
    this.intuserfield3 = intuserfield3;
  }

  public String getIntuserfield4() {
    return intuserfield4;
  }

  public void setIntuserfield4(String intuserfield4) {
    this.intuserfield4 = intuserfield4;
  }

  public String getIntuserfield5() {
    return intuserfield5;
  }

  public void setIntuserfield5(String intuserfield5) {
    this.intuserfield5 = intuserfield5;
  }

  public String getStruserfield1() {
    return struserfield1;
  }

  public void setStruserfield1(String struserfield1) {
    this.struserfield1 = struserfield1;
  }

  public String getStruserfield2() {
    return struserfield2;
  }

  public void setStruserfield2(String struserfield2) {
    this.struserfield2 = struserfield2;
  }

  public String getStruserfield3() {
    return struserfield3;
  }

  public void setStruserfield3(String struserfield3) {
    this.struserfield3 = struserfield3;
  }

  public String getStruserfield4() {
    return struserfield4;
  }

  public void setStruserfield4(String struserfield4) {
    this.struserfield4 = struserfield4;
  }

  public String getStruserfield5() {
    return struserfield5;
  }

  public void setStruserfield5(String struserfield5) {
    this.struserfield5 = struserfield5;
  }

  public String getStruserfield6() {
    return struserfield6;
  }

  public void setStruserfield6(String struserfield6) {
    this.struserfield6 = struserfield6;
  }

  public String getStruserfield7() {
    return struserfield7;
  }

  public void setStruserfield7(String struserfield7) {
    this.struserfield7 = struserfield7;
  }

  public String getStruserfield8() {
    return struserfield8;
  }

  public void setStruserfield8(String struserfield8) {
    this.struserfield8 = struserfield8;
  }

  public String getStruserfield9() {
    return struserfield9;
  }

  public void setStruserfield9(String struserfield9) {
    this.struserfield9 = struserfield9;
  }

  public String getStruserfield10() {
    return struserfield10;
  }

  public void setStruserfield10(String struserfield10) {
    this.struserfield10 = struserfield10;
  }

  public String getStruserfield11() {
    return struserfield11;
  }

  public void setStruserfield11(String struserfield11) {
    this.struserfield11 = struserfield11;
  }

  public String getStruserfield12() {
    return struserfield12;
  }

  public void setStruserfield12(String struserfield12) {
    this.struserfield12 = struserfield12;
  }

  public String getStruserfield13() {
    return struserfield13;
  }

  public void setStruserfield13(String struserfield13) {
    this.struserfield13 = struserfield13;
  }

  public String getStruserfield14() {
    return struserfield14;
  }

  public void setStruserfield14(String struserfield14) {
    this.struserfield14 = struserfield14;
  }

  public String getStruserfield15() {
    return struserfield15;
  }

  public void setStruserfield15(String struserfield15) {
    this.struserfield15 = struserfield15;
  }

  public String getStruserfield16() {
    return struserfield16;
  }

  public void setStruserfield16(String struserfield16) {
    this.struserfield16 = struserfield16;
  }

  public String getStruserfield17() {
    return struserfield17;
  }

  public void setStruserfield17(String struserfield17) {
    this.struserfield17 = struserfield17;
  }

  public String getStruserfield18() {
    return struserfield18;
  }

  public void setStruserfield18(String struserfield18) {
    this.struserfield18 = struserfield18;
  }

  public String getStruserfield19() {
    return struserfield19;
  }

  public void setStruserfield19(String struserfield19) {
    this.struserfield19 = struserfield19;
  }

  public String getStruserfield20() {
    return struserfield20;
  }

  public void setStruserfield20(String struserfield20) {
    this.struserfield20 = struserfield20;
  }

  public String getStruserfield21() {
    return struserfield21;
  }

  public void setStruserfield21(String struserfield21) {
    this.struserfield21 = struserfield21;
  }

  public String getStruserfield22() {
    return struserfield22;
  }

  public void setStruserfield22(String struserfield22) {
    this.struserfield22 = struserfield22;
  }

  public String getStruserfield23() {
    return struserfield23;
  }

  public void setStruserfield23(String struserfield23) {
    this.struserfield23 = struserfield23;
  }

  public String getStruserfield24() {
    return struserfield24;
  }

  public void setStruserfield24(String struserfield24) {
    this.struserfield24 = struserfield24;
  }

  public String getStruserfield25() {
    return struserfield25;
  }

  public void setStruserfield25(String struserfield25) {
    this.struserfield25 = struserfield25;
  }

  public String getStruserfield26() {
    return struserfield26;
  }

  public void setStruserfield26(String struserfield26) {
    this.struserfield26 = struserfield26;
  }

  public String getStruserfield27() {
    return struserfield27;
  }

  public void setStruserfield27(String struserfield27) {
    this.struserfield27 = struserfield27;
  }

  public String getStruserfield28() {
    return struserfield28;
  }

  public void setStruserfield28(String struserfield28) {
    this.struserfield28 = struserfield28;
  }

  public String getStruserfield29() {
    return struserfield29;
  }

  public void setStruserfield29(String struserfield29) {
    this.struserfield29 = struserfield29;
  }

  public String getStruserfield30() {
    return struserfield30;
  }

  public void setStruserfield30(String struserfield30) {
    this.struserfield30 = struserfield30;
  }

  public java.sql.Date getDateuserfield1() {
    return dateuserfield1;
  }

  public void setDateuserfield1(java.sql.Date dateuserfield1) {
    this.dateuserfield1 = dateuserfield1;
  }

  public java.sql.Date getDateuserfield2() {
    return dateuserfield2;
  }

  public void setDateuserfield2(java.sql.Date dateuserfield2) {
    this.dateuserfield2 = dateuserfield2;
  }

  public java.sql.Date getDateuserfield3() {
    return dateuserfield3;
  }

  public void setDateuserfield3(java.sql.Date dateuserfield3) {
    this.dateuserfield3 = dateuserfield3;
  }

  public java.sql.Date getDateuserfield4() {
    return dateuserfield4;
  }

  public void setDateuserfield4(java.sql.Date dateuserfield4) {
    this.dateuserfield4 = dateuserfield4;
  }

  public java.sql.Date getDateuserfield5() {
    return dateuserfield5;
  }

  public void setDateuserfield5(java.sql.Date dateuserfield5) {
    this.dateuserfield5 = dateuserfield5;
  }

  public java.sql.Date getConverted_date() {
    return converted_date;
  }

  public void setConverted_date(java.sql.Date converted_date) {
    this.converted_date = converted_date;
  }

  public String getLastoperperformed() {
    return lastoperperformed;
  }

  public void setLastoperperformed(String lastoperperformed) {
    this.lastoperperformed = lastoperperformed;
  }

  public String getIsdummy() {
    return isdummy;
  }

  public void setIsdummy(String isdummy) {
    this.isdummy = isdummy;
  }

  public String getIstampered() {
    return istampered;
  }

  public void setIstampered(String istampered) {
    this.istampered = istampered;
  }

  public String getChecksum() {
    return checksum;
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  public String getAssignedlocationid() {
    return assignedlocationid;
  }

  public void setAssignedlocationid(String assignedlocationid) {
    this.assignedlocationid = assignedlocationid;
  }

  public String getOwnedlocationid() {
    return ownedlocationid;
  }

  public void setOwnedlocationid(String ownedlocationid) {
    this.ownedlocationid = ownedlocationid;
  }

  public String getIsrouted() {
    return isrouted;
  }

  public void setIsrouted(String isrouted) {
    this.isrouted = isrouted;
  }

  public java.sql.Date getEsc_due_time() {
    return esc_due_time;
  }

  public void setEsc_due_time(java.sql.Date esc_due_time) {
    this.esc_due_time = esc_due_time;
  }

  public String getEsc_level() {
    return esc_level;
  }

  public void setEsc_level(String esc_level) {
    this.esc_level = esc_level;
  }

  public String getCreatedlocationid() {
    return createdlocationid;
  }

  public void setCreatedlocationid(String createdlocationid) {
    this.createdlocationid = createdlocationid;
  }

  public String getEditedlocationid() {
    return editedlocationid;
  }

  public void setEditedlocationid(String editedlocationid) {
    this.editedlocationid = editedlocationid;
  }

  public String getNocurstepassign() {
    return nocurstepassign;
  }

  public void setNocurstepassign(String nocurstepassign) {
    this.nocurstepassign = nocurstepassign;
  }

  public java.sql.Date getRoutedtime() {
    return routedtime;
  }

  public void setRoutedtime(java.sql.Date routedtime) {
    this.routedtime = routedtime;
  }

  public java.sql.Date getProcescduetime() {
    return procescduetime;
  }

  public void setProcescduetime(java.sql.Date procescduetime) {
    this.procescduetime = procescduetime;
  }

  public String getStopescalation() {
    return stopescalation;
  }

  public void setStopescalation(String stopescalation) {
    this.stopescalation = stopescalation;
  }

  public String getFirstgrassignownid() {
    return firstgrassignownid;
  }

  public void setFirstgrassignownid(String firstgrassignownid) {
    this.firstgrassignownid = firstgrassignownid;
  }

  public String getFirstbcassignownid() {
    return firstbcassignownid;
  }

  public void setFirstbcassignownid(String firstbcassignownid) {
    this.firstbcassignownid = firstbcassignownid;
  }

  public String getFirstuserassignownid() {
    return firstuserassignownid;
  }

  public void setFirstuserassignownid(String firstuserassignownid) {
    this.firstuserassignownid = firstuserassignownid;
  }

  public String getCurrnumposesc() {
    return currnumposesc;
  }

  public void setCurrnumposesc(String currnumposesc) {
    this.currnumposesc = currnumposesc;
  }

  public String getCurrnumnegesc() {
    return currnumnegesc;
  }

  public void setCurrnumnegesc(String currnumnegesc) {
    this.currnumnegesc = currnumnegesc;
  }

  public String getNumofposesc() {
    return numofposesc;
  }

  public void setNumofposesc(String numofposesc) {
    this.numofposesc = numofposesc;
  }

  public String getNumofnegesc() {
    return numofnegesc;
  }

  public void setNumofnegesc(String numofnegesc) {
    this.numofnegesc = numofnegesc;
  }

  public String getProcesclevel() {
    return procesclevel;
  }

  public void setProcesclevel(String procesclevel) {
    this.procesclevel = procesclevel;
  }

  public String getManualrouting() {
    return manualrouting;
  }

  public void setManualrouting(String manualrouting) {
    this.manualrouting = manualrouting;
  }

  public String getSlalevel() {
    return slalevel;
  }

  public void setSlalevel(String slalevel) {
    this.slalevel = slalevel;
  }

  public String getRouterulenum() {
    return routerulenum;
  }

  public void setRouterulenum(String routerulenum) {
    this.routerulenum = routerulenum;
  }

  public String getEscrulenum() {
    return escrulenum;
  }

  public void setEscrulenum(String escrulenum) {
    this.escrulenum = escrulenum;
  }

  public String getEscalationattr1() {
    return escalationattr1;
  }

  public void setEscalationattr1(String escalationattr1) {
    this.escalationattr1 = escalationattr1;
  }

  public String getEscalationattr2() {
    return escalationattr2;
  }

  public void setEscalationattr2(String escalationattr2) {
    this.escalationattr2 = escalationattr2;
  }

  public String getEscalationattr3() {
    return escalationattr3;
  }

  public void setEscalationattr3(String escalationattr3) {
    this.escalationattr3 = escalationattr3;
  }

  public String getOwnergroup() {
    return ownergroup;
  }

  public void setOwnergroup(String ownergroup) {
    this.ownergroup = ownergroup;
  }

  public String getRouteruleforgroup() {
    return routeruleforgroup;
  }

  public void setRouteruleforgroup(String routeruleforgroup) {
    this.routeruleforgroup = routeruleforgroup;
  }

  public String getRouteruleforbc() {
    return routeruleforbc;
  }

  public void setRouteruleforbc(String routeruleforbc) {
    this.routeruleforbc = routeruleforbc;
  }

  public String getRouteruleforuser() {
    return routeruleforuser;
  }

  public void setRouteruleforuser(String routeruleforuser) {
    this.routeruleforuser = routeruleforuser;
  }

  public String getTatduration() {
    return tatduration;
  }

  public void setTatduration(String tatduration) {
    this.tatduration = tatduration;
  }

  public String getStopprocescalation() {
    return stopprocescalation;
  }

  public void setStopprocescalation(String stopprocescalation) {
    this.stopprocescalation = stopprocescalation;
  }

  public java.sql.Date getDuedate() {
    return duedate;
  }

  public void setDuedate(java.sql.Date duedate) {
    this.duedate = duedate;
  }

  public String getBackendid() {
    return backendid;
  }

  public void setBackendid(String backendid) {
    this.backendid = backendid;
  }

  public java.sql.Date getLastsubmitteddate() {
    return lastsubmitteddate;
  }

  public void setLastsubmitteddate(java.sql.Date lastsubmitteddate) {
    this.lastsubmitteddate = lastsubmitteddate;
  }

  public String getRisk_profile_score() {
    return risk_profile_score;
  }

  public void setRisk_profile_score(String risk_profile_score) {
    this.risk_profile_score = risk_profile_score;
  }

  public java.sql.Date getRisk_profile_expiry_date() {
    return risk_profile_expiry_date;
  }

  public void setRisk_profile_expiry_date(java.sql.Date risk_profile_expiry_date) {
    this.risk_profile_expiry_date = risk_profile_expiry_date;
  }

  public String getIsmcedited() {
    return ismcedited;
  }

  public void setIsmcedited(String ismcedited) {
    this.ismcedited = ismcedited;
  }

  public String getPreferredphonetype() {
    return preferredphonetype;
  }

  public void setPreferredphonetype(String preferredphonetype) {
    this.preferredphonetype = preferredphonetype;
  }

  public String getPreferredemail() {
    return preferredemail;
  }

  public void setPreferredemail(String preferredemail) {
    this.preferredemail = preferredemail;
  }

  public String getZakat_deduction() {
    return zakat_deduction;
  }

  public void setZakat_deduction(String zakat_deduction) {
    this.zakat_deduction = zakat_deduction;
  }

  public String getIslamic_banking_customer() {
    return islamic_banking_customer;
  }

  public void setIslamic_banking_customer(String islamic_banking_customer) {
    this.islamic_banking_customer = islamic_banking_customer;
  }

  public String getAsset_classification() {
    return asset_classification;
  }

  public void setAsset_classification(String asset_classification) {
    this.asset_classification = asset_classification;
  }

  public String getCustomer_level_provisioning() {
    return customer_level_provisioning;
  }

  public void setCustomer_level_provisioning(String customer_level_provisioning) {
    this.customer_level_provisioning = customer_level_provisioning;
  }

  public String getPreferredcalendar() {
    return preferredcalendar;
  }

  public void setPreferredcalendar(String preferredcalendar) {
    this.preferredcalendar = preferredcalendar;
  }

  public String getGcifid() {
    return gcifid;
  }

  public void setGcifid(String gcifid) {
    this.gcifid = gcifid;
  }

  public String getEnrollmentstatus() {
    return enrollmentstatus;
  }

  public void setEnrollmentstatus(String enrollmentstatus) {
    this.enrollmentstatus = enrollmentstatus;
  }

  public String getAddress_line1() {
    return address_line1;
  }

  public void setAddress_line1(String address_line1) {
    this.address_line1 = address_line1;
  }

  public String getAddress_line2() {
    return address_line2;
  }

  public void setAddress_line2(String address_line2) {
    this.address_line2 = address_line2;
  }

  public String getAddress_line3() {
    return address_line3;
  }

  public void setAddress_line3(String address_line3) {
    this.address_line3 = address_line3;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getPhysical_state() {
    return physical_state;
  }

  public void setPhysical_state(String physical_state) {
    this.physical_state = physical_state;
  }

  public java.sql.Date getTmdate() {
    return tmdate;
  }

  public void setTmdate(java.sql.Date tmdate) {
    this.tmdate = tmdate;
  }
}

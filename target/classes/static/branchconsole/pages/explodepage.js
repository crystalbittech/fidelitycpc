/*
 * This does the frontend validation for declination
 */
function validatecustomerdecline(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("demreason").innerHTML = "<font color=blue>You must provide a reason</font>";
        return false;
    }else{document.getElementById("demreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("demremark").innerHTML = "<font color=blue>You must provide a comment</font>";
        return false;
    }else{document.getElementById("demremark").innerHTML = "<font color=green>Valid</font>";}
    return true;
}


function getAccountRole() {
    var role;

    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        dataType: 'json',
        url: '/user/role/account',
        success: function (data) {
            role = data;
            effectAccountRoles(role)

        },
        error : function (data) {
            console.log(data);

        }
    });

}

function  changeStatus(status){
    switch (status) {
        case 'A':
            document.getElementById("dstatus").innerHTML="Active"
            break;
        case 'I':
            document.getElementById("dstatus").innerHTML="Inactive"
            break
        case 'D':
            document.getElementById("dstatus").innerHTML="Dormant"
            break
    }
}

function effectAccountRoles(role){


    if(!role.modifyAccount){
        $("#modifyAccount").css("display", "none");

        $("#modifyAccount").prop("disabled", true);

    }if(!role.verifyAccount){
        console.log("Sure!!")
        $("#verifyAccount").css("display", "none");

        $("#verifyAccount").prop("disabled", true);

    }if(!role.declineAccount){
        $("#declineAccount").css("display", "none");

        $("#declineAccount").prop("disabled", true);

    }if(!role.cancelAccount){
        $("#cancelAccount").css("display", "none");

        $("#cancelAccount").prop("disabled", true);

    }if(!role.reactivateAccount){
        $("#reactivateAccount").css("display", "none");

        $("#reactivateAccount").prop("disabled", true);

    }



}



function accountdisplaypage(account){
    //noinspection GjsLint
        html = "<form action='#' method='POST' style='font-size:10px!important;'>" +
            "<div class='col-md-12'>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
            "<fieldset class='form-group'>" +
            "<div style='' class='col-md-6'>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitycategory' name='entitycategory'/>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Account Number: </h5><h5 class='col-md-6'  >" + account.accountnumber + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Account Name: </h5><h5 class='col-md-6'  >" + account.accountnamevalue + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Scheme Code: </h5><h5 class='col-md-6' >" + account.accountscheme + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitydata' name='entitydata'/>" +
            "<h5 class='col-md-5' >Account Branch: </h5><h5 class='col-md-6' >" + "" + " " + account.branchsol + "</h5>" +
            "</div>" +
            "</div>" +
            "<div style='' class='col-md-6'>" +

            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Account Status:</h5><h5 class='col-md-6' >" + changeStatus(account.accountstatusvalue) + "</h5>" +
            "</div>" + "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Account Balance:</h5><h5 class='col-md-6' >" + account.accountbalance + "</h5>" +
            "</div>" +
            "</div>" +
            "</fieldset>" +
            "</div>" +
            "<div class='col-md-12'>";

        if (account.accountnamestatus.indexOf("AWAITING") >= 0 || account.accountstatusstatus.indexOf("AWAITING") >= 0) {

            if (data.updatedBy.branch.sol == document.getElementById("currentUserSol").value) {
                html = html + "<input type='button' id='verifyAccount' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifyaccount(" + JSON.stringify(account) + ")'/>" +
                    "<input type='button' id='declineAccount' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='declineaccount(" + account.id + ")'/>";
            }
            if(document.getElementById("currentUserId").value == data.updatedBy.id) {
                html = html + "<input type='button' id='cancelAccount' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelaccount(this.form)'/>";
            }
            html = html + "<input type='button' id='modifyAccount' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updateaccount(" + JSON.stringify(account) + ")'/>";
            if (account.updateform !== "0") {
                html = html + "<a class='btn btn-sm btn-info' id='viewLink' target='_blank' href='/file/load/" + account.updateformUrl + "'>" + " update form " + "</a></h5>";
            }
        } else if (account.accountnamestatus.indexOf("DECLINE") >= 0 || account.accountstatusstatus.indexOf("Decline") >= 0) {
            if(document.getElementById("currentUserId").value == data.updatedBy.id) {
                html = html + "<input type='button' id='cancelAccount' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelaccount(this.form)'/>";
            }
            html = html + "<input type='button' id='modifyAccount' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updateaccount(" + JSON.stringify(account) + ")'/>";
            if (account.updateform !== "0") {
                html = html + "<a class='btn btn-sm btn-info' id='viewLink' target='_blank' href='/file/load/" + account.updateformUrl + "'>" + " update form " + "</a></h5>";
            }
        } else if (account.accountstatusvalue == 'D' || account.accountstatusvalue == 'I') {
            html = html + "<input type='button' id='reactivateAccount' class='btn btn-danger btnpad pull-right' Value='Reactivate' onClick='reactivateaccount(" + JSON.stringify(account) + ")'/>";
        } else {
            html = html + "<input type='button' id='modifyAccount' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updateaccount(" + JSON.stringify(account) + ")'/>";
            // html = html + "<input type='button' id='cancelAccount' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelaccount(this.form)'/>";
        }

        // html = html +  "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='updatecustomer(" + JSON.stringify(account) +")'/>";

        html = html + "</div>" + "</form>";
        getAccountRole();
        hideSpinner("account");

    return html;
}


function accountreactivatepage(data){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};

    html = "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left'>Account details</h6>" +
        "</div>" +
        "<div class='col-md-12' style='height: 100%;'>" +
        "<div style='float: left; clear: none;'>" +
        "<br>" +
        "<br>" +
        "<p>" +
        "<h4>Do you want to update account?</h4>" +
        "</p>" +
        "</div>" +
        "</div>" +
        "<div class='col-md-12 '>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Yes' onClick='updateaccount(" + JSON.stringify(data) +")'/>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='No' onClick='channelsaccount(" + JSON.stringify(data) +")'/>" +
        "</div>";
    hideSpinner("account");
    return html;
}


function accountupdatepage(data){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};

    html = "<form action='#' method='POST' id='accountupdatepage'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Account details</h6>" +
        "</div>" +
        "<div class='col-md-12' style='height: 100%;'>" +
        "<div style='float: left; clear: none;'>" +
        "<fieldset class='form-group'>" +
        "<label for='acctname'>Account name </label>" +
        "<input type='text' name='accountname' id='accountname' value='"+ data.accountnamevalue +"' required  class='form-control '>" +
        "<input type='hidden' name='accountnameid' id='accountnameid' value='"+ data.accountnameid +"' />" +
        "<input type='hidden' name='accountstatus' id='accountstatus' value='"+ data.accountstatusvalue +"' required class='form-control ' >" +
        "<input type='hidden' name='accountstatusid' id='accountstatusid' value='"+ data.accountstatusid +"' />" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ data.id +"' />" +
        "<input type='hidden' name='entitykey' id='entitykey' value='' class='form-control bcon-field' type='text' />" +
        "</fieldset>" +
        "</div>" +
        "</div>" +
        "<div class='col-md-12 '>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Next' onClick='channelsaccount(" + JSON.stringify(data) +")'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='getaccount(" +data.id +")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("account");
    return html;
}

function accountchannelspage(data){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};

    html = "<div class='modal-content' >" +
        "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Select channels requested</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<div>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ data.id +"' />" +
        "</div >" +
        "<br >" +
        "<div >" +
        "<div class='col-md-4'>FCMB Mobile</div><div class='col-md-6'> <input type='checkbox' name='fcmbmobile' id='fcmbmobile' ></div>" +
        "<div class='col-md-4'>FCMB Online</div><div class='col-md-6'> <input type='checkbox' name='fcmbonline' id='fcmbonline' ></div>" +
        "<div class='col-md-4'>MaterCard" +
        "</div><div class='col-md-6'><input type='checkbox' name='matercard' id='matercard' ></div>" +
        "<div class='col-md-4'>R.I.B " +
        "</div><div class='col-md-6'><input type='checkbox' name='rib' id='rib' ></div>" +
        "<div class='col-md-4'>Verve" +
        "</div><div class='col-md-6'><input type='checkbox' name='verve' id='verve' ></div>" +
        "<div class='col-md-4'>VISA" +
        "</div><div class='col-md-6'><input type='checkbox' name='visa' id='visa' ></div>" +
    "</div>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Next' onClick='updateformaccount(" + JSON.stringify(data) +")'/>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Back' onClick='reactivateaccount(" + JSON.stringify(data) +")'/>" +
        "</div>" +
        "</form>" +
        "</div>";
    hideSpinner("account");
    return html;
}

function accountupdateformpage(data){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};

    html = "<form action='#' method='POST' id='accountupdateformpage' enctype='multipart/form-data'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Account details</h6>" +
        "</div>" +
        "<div class='col-md-12' style='height: 100%;'>" +
        "<div style='float: left; clear: none;'>" +
        "<fieldset class='form-group'>" +
        "<label for='acctname'>Upload document </label>" +
        "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field' data-preview-file-type='text' >" +
        "<small class='text-muted'>Please select Document</small>" +
        "</fieldset>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ data.id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory' value='ACCOUNT-UPDATE' />" +
        "<input type='hidden' name='entitykey' id='entitykey' value='ACCOUNT-UPDATE' />" +
        "</div>" +
        "</div>" +
        "<div class='col-md-12 '>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='uploadaccountform(this.form, " + JSON.stringify(data) +")'/>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Back' onClick='channelsaccount(" + JSON.stringify(data) +")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("account");
    return html;
}


function accountdeclinepage(id){


    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Account Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +

        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory'  />" +
        "<div >" +
        "<label for='reason'>Reason </label>" +
        "<input class='form-control bcon-field' type='' id='entitykey' name='entitykey' onkeyup='validateaccountdecline(this.form)' required />" +
        "<small class='text-muted' id='adecreason'>Please enter a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' class='form-control dpicker bcon-field' type='text' name='entitydata'  onkeyup='validateaccountdecline(this.form)' />" +
        "<small class='text-muted' id='adecremark'>please enter a comment</small>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='declineaccountprocess(this.form)'/>" +
        "</div>" +
        "</form>";
    hideSpinner("account");
    return html;
}


function accountresultpage(data){
    if(data==null || data=="undefined" || data==""){html = "<h3>Account reactivated successfully</h3>";}
    else {
        if (data.phone == null) {
            data.phone = ''
        }
        ;
        if (data.firstname == null) {
            data.firstname = ''
        }
        ;
        if (data.lastname == null) {
            data.lastname = ''
        }
        ;
        if (data.address == null) {
            data.address = ''
        }
        ;
        if (data.email == null) {
            data.email = ''
        }
        ;
        if (data.middlename == null) {
            data.middlename = ''
        }
        ;

        html = "<div class='col-md-12'>" +
            "<div class='col-md-12 response-box'><div class='inner-response'>" +
            "<label><h3>" + data.searchmessage + "</h3></label><p></p>" +
            "<label><h7>Request Id: " + data.entityid + "</h7></label>" +
            "</div><div class='col-md-12'>" +
            "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='getaccount(" + data.entityid + ")'/>" +
            "</div></div></div>";

    }   hideSpinner("account");
    return html;
}
function reloadaccountbottom(data){
    console.log(data);
    htmlbottom = "<table class='table table-hover table-condensed' style='visibility:none'> " +
        "<thead> <tr> <th>Category</th> <th>Type</th> <th>Status</th> " +
        "<th><a type='button'  class='pull-right btn btn-sm' href='javascript:newfreeze()'> " +
        "<i class='fa fa-plus-circle'></i>&nbsp;New charge</a></th> </tr> </thead>" +
        "<tbody>";
    for (var i=0;i<data.length;i++){
        htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:chargeitem(" +data[i].id+","+data.searchacctid+ ")&apos;'  role='button'>"+
            "<td>"+sentenceCase(data[i].category).replace("_"," ")+"</td>"+
            "<td>"+data[i].value+"</td>" +
            "<td>"+sentenceCase(data[i].status).replace("_"," ")+"</td><td></td>" +
            "</tr>";
    }
    htmlbottom = htmlbottom + "</tbody> </table>";
    document.getElementById("accountbottom").innerHTML = htmlbottom;
}


function customeritempage(data){
    if(data.phone==null){data.phone=''};
    if(data.phone.status==null){data.phone.status='';};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.address.status==null){data.address.status='';};
    if(data.email==null){data.email=''};
    if(data.email.status==null){data.email.status='';};
    if(data.middlename==null){data.middlename=''};
    if(data.bvn==null){data.bvn=''};

    html = "<form action='#' method='POST' style='font-size:10px!important;'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Customer Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group small-scrollable'>" +
        "<div style='' class='col-md-5'>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitycategory' name='entitycategory'/>" +
        "<h5 class='col-md-5'>Request Id: </h5><h5 class='col-md-6'  >"+ data.id +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitycategory' name='entitycategory'/>" +
        "<h5 class='col-md-5'>Title: </h5><h5 class='col-md-6'  >"+ data.title.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.id+"'  id='entityid' name='entityid'/>" +
        "<h5 class='col-md-5'>First name: </h5><h5 class='col-md-6'  >"+ data.firstname.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Last name: </h5><h5 class='col-md-6' >"+ data.lastname.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitydata' name='entitydata'/>" +
        "<h5 class='col-md-5' >Middle name: </h5><h5 class='col-md-6' >"+ data.middlename.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>BVN: </h5><h5 class='col-md-6' >"+ data.bvn +"</h5>" +
        "<h5 class='col-md-5'><a target='_blank' href='javascript: showcustomersuploadedform(" + JSON.stringify(data) +")'>Update form</a></h5>" +
        "</div>" +
        "</div>" +
        "<div style='' class='col-md-5'>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Phone no:</h5><h5 class='col-md-6'>"+ data.phone.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Date of birth:</h5><h5 class='col-md-6' >"+ data.dateOfBirth.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Mother\'s maiden name:</h5><h5 class='col-md-6' >"+ data.motherMaidenName.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Email address:</h5><h5 class='col-md-6' >"+ data.email.value +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-5'>Address:</h5><h5 class='col-md-6' >"+ data.address.value +"</h5>" +
        // "<h6 class='col-md-6'><a target='_blank' href='javascript: showcustomersuploadedform(" + JSON.stringify(data) +")'>View</a></h6>" +
        "</div>" +
        "</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>";
    if (data.firstname.status.indexOf("AWAITING")>=0 || data.lastname.status.indexOf("AWAITING")>=0 || data.middlename.status.indexOf("AWAITING")>=0
        ||data.phone.status.indexOf("AWAITING")>=0 || data.email.status.indexOf("AWAITING")>=0 || data.address.status.indexOf("AWAITING")>=0
        ||data.dateOfBirth.status.indexOf("AWAITING")>=0 || data.motherMaidenName.status.indexOf("AWAITING")>=0 || data.title.status.indexOf("AWAITING")>=0){

            if(data.verifyCustomerDetails){
                html = html + "<input type='button' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifycustomer(this.form)'/>" ;
            }

        if(data.declineCustomerDetails){
            html = html +  "<input type='button' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='declinecustomerpage("+ data.id +")'/>";
        }

        if(confirm_if_user_is_same_as_updatedBy(data)) {

            html = html + "<input type='button' class='btn btn-success btnpad pull-right' id='verifyCustomer' Value='Awaiting verification' onClick='loaddetailspage(" + JSON.stringify(data) + ",this.form)'/>";
        }

    }else{
        if (data.firstname.status.indexOf("DECLINE")>=0 || data.lastname.status.indexOf("DECLINE")>=0 || data.middlename.status.indexOf("DECLINE")>=0
            ||data.phone.status.indexOf("DECLINE")>=0 || data.email.status.indexOf("DECLINE")>=0 || data.address.status.indexOf("DECLINE")>=0
            ||data.dateOfBirth.status.indexOf("DECLINE")>=0 || data.motherMaidenName.status.indexOf("DECLINE")>=0 || data.title.status.indexOf("DECLINE")>=0){
            if(confirm_if_user_is_same_as_updatedBy(data)) {
                    html = html + "<input type='button' id='cancelCustomer' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelcustomer(this.form)'/>";
            }
            }


            html = html + "<input type='button' class='btn btn-danger btnpad pull-right' id='modifyCustomer' Value='Modify' onClick='updatecustomer(" + JSON.stringify(data) +")'/>";


    }


    html = html + "</div>" + "</form>";
    getCustomerRole()
    return html;
}

function confirm_if_user_is_same_as_updatedBy(data){
    var flag = false;
    if(data.firstname.updatedBy.branch.sol === document.getElementById("currentUserSol")){
        flag = true;
    }
    if(data.lastname.updatedBy.branch.sol === document.getElementById("currentUserSol") && flag === true){
        flag = true;
    }else{flag = false;}

    if(data.middlename.updatedBy.branch.sol === document.getElementById("currentUserSol") && flag === true){
        flag = true;
    }else{flag = true;}

    if(data.phone.updatedBy.branch.sol === document.getElementById("currentUserSol") && flag === true){
        flag = true;
    }else{flag = false;}

    if(data.email.updatedBy.branch.sol === document.getElementById("currentUserSol") && flag === true){
        flag = true;
    }else{flag = false;}

    if(data.address.updatedBy.branch.sol === document.getElementById("currentUserSol") && flag === true){
        flag = true;
    }else{flag = false;}

    if(data.dateOfBirth.updatedBy.branch.sol === document.getElementById("currentUserSol") && flag === true){
        flag = true;
    }else{flag = false;}

    if(data.motherMaidenName.updatedBy.branch.sol === document.getElementById("currentUserSol") && flag === true){
        flag = true;
    }else{flag = false;}

    if(data.title.updatedBy.branch.sol === document.getElementById("currentUserSol") && flag === true){
        flag = true;
    }else{flag = false;}

    return flag;
}

function customerupdatepage(data){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};
    if(data.title==null){data.title=''};
    if(data.dateOfBirth==null){data.dateOfBirth=''};
    if(data.motherMaidenName==null){data.motherMaidenName=''};

    html = "<form id='updatecustomerform' action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Customer details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +
        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ data.id +"' />" +
        "<div class=col-md-4><div >" +
        "<label for='acctnum'>First name </label>" +
        "<input type='hidden' name='firstnameid' id='firstnameid' value='"+ data.firstname.id +"' />" +
        "<input type='text' name='firstname' id='firstname' value='"+ data.firstname.value +"' required class='form-control ' >" +
        "<small id='firstname_label' class='text-muted'>Enter first name</small>" +
        "</div>" +
        "<div >" +
        "<label for='acctnum'>Middle name </label>" +
        "<input type='hidden' name='middlenameid' id='middlenameid' value='"+ data.middlename.id +"' />" +
        "<input type='text' name='middlename' id='middlename' value='"+ data.middlename.value +"' required class='form-control ' placeholder='Enter middle name' >" +
        // "<small class='text-muted'>Enter middle name</small><br/>" +
        "<small class='text-muted'>Source doc in place</small> <input type='checkbox' name='middlenamesourcedoc' id='middlenamesourcedoc' class='text-muted' />" +
        "</div>" +
        "<div >" +
        "<label for='acctnum'>Last name </label>" +
        "<input type='hidden' name='lastnameid' id='lastnameid' value='"+ data.lastname.id +"' />" +
        "<input type='text' name='lastname' id='lastname' value='"+ data.lastname.value +"' required class='form-control ' >" +
        "<small id='lastname_label' class='text-muted'>Enter last name</small>" +
        "</div></div>" +
        "<div class=col-md-4><div >" +
        "<label for='acctnum'>Date of birth </label>" +
        "<input type='hidden' name='dobid' id='dobid' value='"+ data.dateOfBirth.id +"' />" +
        "<input type='text' name='dob' id='dob' onclick='pickDobDate()' value='"+ data.dateOfBirth.value +"' required class='form-control ' placeholder='Enter date of birth' >" +
        "<small id='dob_label' class='text-muted'></small> &nbsp; &nbsp;" +
        "<small class='text-muted'>Source doc in place</small> <input type='checkbox' name='dobsourcedoc' id='dobsourcedoc' class='text-muted' />" +
        "</div>" +
        "<div >" +
        "<label for='acctnum'>Title </label>" +
        "<input type='hidden' name='titleid' id='titleid' value='"+ data.title.id +"' />" +
        "<input type='text' name='title' id='title' value='"+ data.title.value +"' required class='form-control ' >" +
        "<small class='text-muted'>Enter title</small>" +
        "</div>" +
        "<div >" +
        "<label for='acctnum'>Mother\'s maiden name</label>" +
        "<input type='hidden' name='motherMaidenNameid' id='motherMaidenNameid' value='"+ data.motherMaidenName.id +"' />" +
        "<input type='text' name='motherMaidenName' id='motherMaidenName' value='"+ data.motherMaidenName.value +"' required class='form-control ' >" +
        "<small id='maidenname_label' class='text-muted'>Enter mother's maiden name</small>" +
        "</div></div>" +
        "<div class=col-md-4 style='float:right;clear:none;'><div >" +
        "<label for='acctnum'>Phone no </label>" +
        "<input type='hidden' name='phoneid' id='phoneid' value='"+ data.phone.id +"' />" +
        "<input type='text' name='phone' id='phone' value='"+ data.phone.value +"' required class='form-control ' >" +
        "<small id='phone_label'  class='text-muted'>Enter phone number</small>" +
        "</div>" +
        "<div>" +
        "<label for='acctnum'>Email </label>" +
        "<input type='hidden' name='emailid' id='emailid' value='"+ data.email.id +"' />" +
        "<input type='text' name='email' id='email' value='"+ data.email.value +"'  required class='form-control ' >" +
        "<small id='email_label' class='text-muted'>Enter email address</small>" +
        "</div>" +
        "<div>" +
        "<label for='acctnum'>Address </label>" +
        "<input type='hidden' name='addressid' id='addressid' value='"+ data.address.id +"' />" +
        "<input type='text' name='address' id='address' value='"+ data.address.value +"' required class='form-control ' >" +
        "<small id='address_label' class='text-muted'></small> &nbsp; &nbsp;" +
        "<small class='text-muted'>Source doc in place</small> <input type='checkbox' name='addresssourcedoc' id='addresssourcedoc' class='text-muted' />" +
        "</div>" +
        "</div></div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Proceed' onClick='uploadcustomerupdateform(" + JSON.stringify(data) + ")'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='customeritem("+data.id+")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("customer");
    return html;
}
function pickDobDate(){
    // alert("date");
    $('#dob').datepicker({
        format: 'yyyy-mm-dd',
        todayBtn: "linked",
        autoclose: true
    });
    pickDobDate();
}
function customerupdateformuploadpage(data){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};
    var $form = $("#updatecustomerform");
    var updatecustomerformdata = getFormData($form);

    html = "<form action='#' id='uploadcustomerupdateform' enctype='multipart/form-data' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>New Document</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +

        "<input type='hidden' name='entityid' id='entityid' value='"+ data.id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory' value='CUSTOMER-UPDATE' />" +
        "<input type='hidden' name='entitykey' id='entitykey' value='CUSTOMER-UPDATE' />" +
        "<div class='col-md-12'>" +
        "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field' data-preview-file-type='text' >" +
        "<small id='custlabel' class='text-muted'>Please select Document</small>" +
        "</div>" +

        "</fieldset>" +
        "</div><br/><br/>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='addnewcustupdateform(this.form, " + JSON.stringify(updatecustomerformdata) + "," + JSON.stringify(data) +")'/>"+
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='updatecustomer(" + JSON.stringify(data) +")'/>"+
        "</div>" +
        "</form>";
    hideSpinner("customer");
    return html;
}
function customeraddpage(){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>New Freeze</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='1' />" +
        "<div >" +
        "<label for='acctnum'>Freeze reason </label>" +
        "<select name='entitydata' id='entitydata' required class='form-control ' >" +
        "<option value='Bvn'>Bvn</option>" +
        "<option value='Disease'>Disease</option>" +
        "<option value='Fraud'>Fraud</option>" +
        "</select>" +
        "<small class='text-muted'>Please select a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='expiry-date'>Freeze category</label>" +
        "<select name='entitycategory' id='entitycategory' required class='form-control ' >" +
        "<option value='Total customer'>Total customer</option>" +
        "<option value='Credit customer'>Credit customer</option>" +
        "<option value='Debit customer'>Debit customer</option>" +
        "</select>" +
        "<small class='text-muted'>Please select a category</small>" +
        "</div>" +
        "<div >" +
        "<label for='remarks'>Remarks </label>" +
        "<input id='entitykey' class='form-control bcon-field' type='text'  name='entitykey' />" +
        "<small class='text-muted'>Please enter a remark</small>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='addnewcustomer(this.form)'/>" +
        "</div>" +
        "</form>";
    hideSpinner("customer");
    return html;
}
function customerdeclinepage(id){

    // if(data.phone==null){data.phone=''};
    // if(data.firstname==null){data.firstname=''};
    // if(data.lastname==null){data.lastname=''};
    // if(data.address==null){data.address=''};
    // if(data.email==null){data.email=''};
    // if(data.middlename==null){data.middlename=''};
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Freeze Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory'  />" +

        "<div >" +
        "<label for='reason'>Reason </label>" +
        "<input class='form-control bcon-field' type='text' id='entitykey' name='entitykey' onkeyup='validatecustomerdecline(this.form)' required />" +
        "<small class='text-muted' id='demreason'>Please enter a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' class='form-control dpicker bcon-field' type='text' name='entitydata' onkeyup='validatecustomerdecline(this.form)'/>" +
        "<small class='text-muted' id='demremark'>please enter a comment</small>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='declinecustomer(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='customeritem("+id+")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("customer");
    return html;
}
function customeruploadedformpage(data){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};

    html = "<div>" +
        "<object data='/file/load/"+ data.accessURL +"' type='application/pdf'  >alt : <a href='/file/load/"+ data.accessURL +"'>test.pdf</a>" +
        "</object>" +
        "</div>";
    hideSpinner("customer");
    return html;
}
function customerslistpage(data){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};
    htmlbottom = "<table class='table table-hover table-condensed'> " +
        "<thead> <tr> <th>Cust id</th> <th>Type</th> <th>Name</th> " +
        "<th><a type='button'  class='pull-right btn btn-sm' href='javascript:newcustomer()'> " +
        "<i class='fa fa-plus-circle'></i>&nbsp;New customer</a></th> </tr> </thead>" +
        "<tbody>";
    for (var i=0;i<data.length;i++){
        htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:customeritem(" +data[i].id+ ")&apos;'  role='button'>"+
            "<td>"+data[i].cifId+"</td>"+
            "<td>"+data[i].category+"</td>" +
            "<td>"+data[i].lastname.value+"</td><td></td>" +
            "</tr>";
    }
    htmlbottom = htmlbottom + "</tbody> </table>";
    return htmlbottom;
}
function customerresult(data){
    if(data.phone==null){data.phone=''};
    if(data.firstname==null){data.firstname=''};
    if(data.lastname==null){data.lastname=''};
    if(data.address==null){data.address=''};
    if(data.email==null){data.email=''};
    if(data.middlename==null){data.middlename=''};
    html = "<div class='col-md-12'>" +
        "<div class='col-md-12 response-box'><div class='inner-response'>" +
        "<label><h3>"+ data.searchmessage +"</h3></label><p></p>" +
        "<label><h7>Request Id: "+ data.entityid +"</h7></label>" +
        "</div><div class='col-md-12'>" +
        "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='customeritem("+data.entityid+")'/>" +
        "</div></div></div>";
    hideSpinner("customer");
    return html;
}

/**
 * POST  UpdateFormController addnew
 * This creates a new updateform on account
 */
function addnewacctupdateform(form_id){
    $("form#upload_form").submit(function(){
        var formData = new FormData($(this)[0]);
        var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value, this.file_upload.value);
        postRequest.appendinputsformdata(formData);

        $.ajax({
            url: '/updateform/addforacct',
            type: 'POST',
            data: formData,
            async: false,
            success: resultupdateform,
            error : resultupdateform,
            cache: false,
            contentType: false,
            processData: false
        });

        return false;
    });
    $("form#upload_form").submit();
}








function updateformitem(id){

    url = '/updateform/getdata'
    var postdata = {
        'id': id
    }
    process(url, postdata, explodeupdateform);
}


function explodeupdateform(data){
    console.log(data);

    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>UpdateForm Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.id+"' id='entityid' name='entitid'/>" +
        "<fieldset class='form-group'>" +

        "<div style='float: left; clear: none; ' class='col-md-8'>" +
            "<div>" +
                "<input type='hidden' class='form-control bcon-field' type='text' name='entitydata' id='entitydata' name='entitydata'/>" +
                "<input type='hidden' class='form-control bcon-field' type='text' name='entitycategory' id='entitycategory' name='entitycategory'/>" +
                "<h5 class='col-md-3' >Ref. Number: </h5><h5 class='col-md-9' >" + data.value + "</h5>" +
            "</div>" +
            "<div>" +
                "<h5 class='col-md-3'>Status: </h5><h5 class='col-md-9'>" + data.status + "</h5>" +
            "</div>" +
            "<div>" +
                "<h5 class='col-md-3'>View UpdateForm</h5><h5 class='col-md-9'><a class='btn btn-sm btn-info' target='_blank' href='/file/load/"+ data.filez.imageURL +"/"+ data.id +"'>"+ data.category + "</a></h5>" +
            "</div>" +
        "</div>" +

        "<div style='float: left; ' class='col-md-4 '>" +
            "<div>" +
                "<h6 class='col-md-6'>Update by:</h6><h6 class='col-md-6'  >"+ data.updatedBy + "</h6>" +
            "</div>" +
            "<div>" +
                "<h6 class='col-md-6'>Update at:</h6><h6 class='col-md-6' >" +data.updatedBy + "</h6>" +
            "</div>" +
            "<div>" +
                "<h6 class='col-md-6'>Update date:</h6><h6 class='col-md-6' >" + "" + "</h6>" +
            "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>";

    if (data.status.indexOf("AWAITING")>=0){
        html = html + "<input type='button' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifyupdateform(this.form)'/>";
        html = html + "<input type='button' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='updateformdecline("+ data.id +")'/>";
    }else{
        if (data.status.indexOf("DECLINE")>=0){
            html = html + "<input type='button' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelupdateform(this.form)'/>";
        }else if(data.status == "FREEZE_PRESENT"){
            html = html + "<input type='button' class='btn btn-danger btnpad pull-right' Value='Remove' onClick='removeupdateform(this.form)'/>";
        }
        if (data.status!="REMOVE_FREEZE_VERIFIED"){
            html = html + "<input type='button' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updateupdateform(" + JSON.stringify(data) +")'/>";
        }
    }
    html = html + "</div>" + "</form>";
    updateform.getElementById("updateformtop").innerHTML = html;
}

function newupdateform(acctid){
    html = "<form action='#' id='upload_form' enctype='multipart/form-data' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>New UpdateForm</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<input type='hidden' name='searchacctid2' value='"+ acctid +"' >"+

            "<input type='hidden' name='entityid' id='entityid' value='1' />" +
            "<input type='hidden' name='entitycategory' id='entitycategory' value='DRIVERS-LICENSE' />" +
            "<div class='col-md-12'>" +
                "<label for='category'>Reference No</label>" +
                "<input id='updateformname' class='form-control bcon-field' type='text' name='entitykey' required />" +
                "<small class='text-muted'>Please enter a updateform name/description</small>" +
            "</div>" +
            "<div class='col-md-12'>" +
                "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field' data-preview-file-type='text' >" +
                "<small class='text-muted'>Please select UpdateForm</small>" +
            "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='buttom' class='btn btn-success btnpad pull-right' Value='Submit' onClick='addnewupdateform()'/>" +
        "</div>" +
        "</form>"

    updateform.getElementById("updateformtop").innerHTML = html;
}



//This displays the view for the decline form
function updateformdecline(id){

    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>UpdateForm Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +

        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory'  />" +

        "<div >" +
        "<label for='reason'>Reason </label>" +
        "<input class='form-control bcon-field' type='number' id='entitykey' name='entitykey' required />" +
        "<small class='text-muted'>Please enter a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' class='form-control dpicker bcon-field' type='text' name='entitydata' />" +
        "<small class='text-muted'>please enter a comment</small>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='declineupdateform(this.form)'/>" +
        "</div>" +
        "</form>"

    updateform.getElementById("updateformtop").innerHTML = html;
}



function updateupdateform(data){

    html = "<form action='#' id='upload_form' enctype='multipart/form-data' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>New UpdateForm</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<input type='hidden' name='searchacctid2' value='"+ acctid +"' >"+

        "<input type='hidden' name='entityid' id='entityid' value='"+data.id+"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory' value='DRIVERS-LICENSE' />" +
        "<div class='col-md-12'>" +
        "<label for='category'>Reference No</label>" +
        "<input id='updateformname' class='form-control bcon-field' type='text' name='entitykey' required />" +
        "<small class='text-muted'>Please enter a updateform name/description</small>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field' data-preview-file-type='text' >" +
        "<small class='text-muted'>Please select UpdateForm</small>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='buttom' class='btn btn-success btnpad pull-right' Value='Submit' onClick='modifyupdateform()'/>" +
        "</div>" +
        "</form>"

    updateform.getElementById("updateformtop").innerHTML = html;
}

function resultupdateform(data){
    html = "<div class='col-md-12'>" +
        "<div class='col-md-12 response-box'><div class='inner-response'>" +
        "<label><h3>"+ data.searchmessage +"</h3></label><p></p>" +
        "<label><h7>Request Id: "+ data.entityid +"</h7></label>" +
        "</div><div class='col-md-12'>" +
        "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='updateformitem("+data.entityid+")'/>" +
        "</div></div></div>";
    updateform.getElementById("updateformtop").innerHTML = html;
}










function validatemandate(form){
    //console.log(form.indemnity.checked)
    if(form.indemnity.checked){

        if (!(form.entitydata.value==null || form.entitydata.value=='')){
            if(validateEmail(form.entitydata.value)){
                document.getElementById("instno1").innerHTML = "<font color=green>Valid</font>";
                return true;
            }else {
                document.getElementById("instno1").innerHTML = "<font color=red>You must enter a valid email or uncheck indemnity</font>";
                return false;
            }

        }else{document.getElementById("instno1").innerHTML = "<font color=green>Valid</font>";}
    }
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("instno").innerHTML = "<font color=blue>You must enter instruction/decription</font>";
        return false;
    }else{document.getElementById("instno").innerHTML = "<font color=green>Valid</font>";}
    if (form.file_upload.value==null || form.file_upload.value==''){
        document.getElementById("fileupload").innerHTML = "<font color=blue>You must select an image</font>";
        return false;
    }else{document.getElementById("fileupload").innerHTML = "<font color=green>Valid</font>";}



    return true;
}
function validatemandateupdate(form){
    //console.log(form.indemnity.checked)

    if (form.file_upload.value==null || form.file_upload.value==''){
        document.getElementById("fileupload").innerHTML = "<font color=blue>You must select an image</font>";
        return false;
    }
     else{document.getElementById("fileupload").innerHTML = "<font color=green>Valid</font>";}



    return true;
}
function validatemandateInsupdate(form){
    //console.log(form.indemnity.checked)

    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("instno").innerHTML = "<font color=blue>You must enter instruction/decription</font>";
        return false;
    }

    return true;
}
/*
 * This does the frontend validation for declination
 */
function validatemandatedecline(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("demreason").innerHTML = "<font color=blue>You must provide a reason</font>";
        return false;
    }
    // else{document.getElementById("demreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("demremark").innerHTML = "<font color=blue>You must provide a comment</font>";
        return false;
    }
    // else{document.getElementById("demremark").innerHTML = "<font color=green>Valid</font>";}
    return true;
}
/**
 * POST  MandateController addnew
 * This creates a new mandate on account
 */
function addnewmandate(form){
    if (validatemandate(form)==true) {
        showSpinner("mandate");
        $("form#upload_form").submit(function () {
            var formData = new FormData($(this)[0]);
            var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value, this.file_upload.value,this.entitydata.value);
            postRequest.appendinputsformdata(formData);
            $.ajax({
                url: '/mandate/addnew',
                type: 'POST',
                data: formData,
                async: false,
                success: resultmandate,
                error: resultmandate,
                cache: false,
                contentType: false,
                processData: false
            });
            return false;
        });
        $("form#upload_form").submit();
    }
}


/**
 * POST  MandateController remove
 * This removes a mandate on account
 */
function removemandate(form) {
    showSpinner("mandate");
    var url = '/mandate/remove';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultmandate);
}


/**
 * POST  MandateController modify
 * This removes a mandate on account
 */
function modifymandate(form) {

    if (validatemandateupdate(form)==true) {
        showSpinner("mandate");
        $("form#upload_form").submit(function () {
            var formData = new FormData($(this)[0]);
            var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value, this.file_upload.value);
            postRequest.appendinputsformdata(formData);

            $.ajax({
                url: '/mandate/modify',
                type: 'POST',
                data: formData,
                async: false,
                success: resultmandate,
                error: resultmandate,
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });
        $("form#upload_form").submit();
        hideSpinner("mandate");
    }
}
function modifymandateFull(form) {

    if (validatemandateupdate(form)==true) {
        showSpinner("mandate");
        $("form#upload_form").submit(function () {
            var formData = new FormData($(this)[0]);
            var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value, this.file_upload.value);
            postRequest.appendinputsformdata(formData);

            $.ajax({
                url: '/mandate/modifyfull',
                type: 'POST',
                data: formData,
                async: false,
                success: resultmandate,
                error: resultmandate,
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });
        $("form#upload_form").submit();
        hideSpinner("mandate");
    }
}
function modifymandateInstruction(form) {

    if (validatemandateInsupdate(form)==true) {
        showSpinner("mandate");

        $("form#updateMan_form").submit(function () {
            var formData = new FormData($(this)[0]);
            var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value);
            postRequest.appendinputsformdata(formData);

            $.ajax({
                url: '/mandate/modifyinstruction',
                type: 'POST',
                data: formData,
                async: false,
                success: resultmandate,
                error: resultmandate,
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });
        $("form#updateMan_form").submit();
        hideSpinner("mandate");
    }
}


/**
 * POST  MandateController verify
 * This verifies a mandate process
 */
function verifymandate(form) {
    showSpinner("mandate");
    var url = '/mandate/verify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultmandate);
}

/**
 * POST  MandateController cancel
 * This cancels a mandate action
 */
function cancelmandate(form) {
    showSpinner("mandate");
    var url = '/mandate/cancel';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultmandate);
}
/**
 * POST  MandateController decline
 * This declines a mandate process
 */
//This method does the declination from the controller with ajax call
function declinemandate(form) {
    if (validatemandatedecline(form)==true) {
        showSpinner("mandate");
        var url = '/mandate/decline';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
        postRequest.postdata(url, resultmandate);
    }
}

function mandateitem(id,accid){
    // alert($('#acctid').val());
showSpinner("mandate");
    //START HIGHLIGHTING
    $(".mandateTable tr").click(function() {
        $("tr").removeClass("highlightcolor");
        $(this).addClass("highlightcolor");
    });
    //END HIGHLIGHTING
    url = '/mandate/getdata'
    var postdata = {
        'id': id,
        'accid': $('#acctid').val()
    }
    process(url, postdata, explodemandate);
hideSpinner("mandate");
}

function getmandateimg(data){

    $('#mandate-image').attr("src", "/file/load/"+ data.accessURL +"");
}

function getMandateRole() {
    var role;

    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        dataType: 'json',
        url: '/user/role/mandate',
        success: function (data) {
            role = data;

            effectMandateRoles(role)
        },
        error : function (data) {


        }
    });

}

function effectMandateRoles(role){
    if(!role.viewMandate){


        $('#viewMandate').click(function(e) {
            e.preventDefault();
            //do other stuff when a click happens
            bootbox.alert("Sorry You Are Not priviledged To Perfom This task")
        });

    }if(!role.newMandate){


        $('#newMandate').click(function(e) {
            e.preventDefault();
            //do other stuff when a click happens
            bootbox.alert("Sorry You Are Not priviledged To Perfom This task")
        });

    }if(!role.modifyMandate){
        $(".modifyM").css("display", "none");

        $(".modifyM").prop("disabled", true);

    }if(!role.verifyMandate){
        $("#verifyMandate").css("display", "none");

        $("#verifyMandate").prop("disabled", true);

    }if(!role.declineMandate){
        $("#declineMandate").css("display", "none");

        $("#declineMandate").prop("disabled", true);

    }if(!role.declineMandate){
        $("#removeMandate").css("display", "none");

        $("#removeMandate").prop("disabled", true);

    }if(!role.cancelMandate){
        $("#cancelMandate").css("display", "none");

        $("#cancelMandate").prop("disabled", true);

    }


}
function importmandate(form){
    showSpinner("mandate");
    var url = '/mandate/importmandate';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultmandate);
}
function mandateimport(){
    hideSpinner("mandate");
    $( "#mandatetop" ).load( "/mandate/loadmandatepage");
}



function explodemandate(data) {
console.log(data);
    hideSpinner("mandate");
    console.log('data ==> ' + JSON.stringify(data));
    if (data == null || data.id == "none") {
        rhtml = "<div><h3>No mandate for this account </h3></div><br>"+
            "<input type='button' class='btn btn-danger btnpad pull-right' Value='Import mandate from another account' onClick='mandateimport()'/>";
        document.getElementById("mandatetop").innerHTML = rhtml;

    } else {
        //This gets the updated sol since branch has been ignored from the json
        var sol = "";
        console.log("Ent id "+data.id);
        var postdata = {"entid":data.id};
        $.ajax({
            url:'/mandate/getUpdatedSol',
            type:'POST',
            data:postdata,
            dataType: 'json',
            async:false,
            success:function(data1){sol=data1.sol},
            error:function(){console.log("Error occured");}
            
        });
        $(document).on("click",".mandateview",function(){
                        // alert(hh);
            $('#mandateimageview').attr("src",data.freeText2 +"");
            $('#mandateflash').modal('show');
            // document.getElementById("chqdetails").innerHTML = hh;
        });
        html = "<form action='#' method='POST'>" +
            "<div class='col-md-12'>" +
            "<h6 class='modal-title pull-left' id='myModalLabel'>Mandate Details</h6>" +
            "</div>" +

        //     "<div class='col-md-12'>" +
        //     "<h6 class='modal-title pull-right' id='myModalLabel' style="padding: .5em .1em;color: rebeccapurple !important; ">Account Name:@.account.accountName.value</h6>"+
        // "</div>"+



        "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitycategory' name='entitycategory'/>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.id+"' id='entityid' name='entityid'/>" +
            "<fieldset class='form-group'>" +

            "<div style='float: left; clear: none; ' class='col-md-8'>" +
            "<div>" +
            "<input type='hidden' class='form-control bcon-field' type='text' name='entitydata' id='entitydata' name='entitydata'/>" +
            "<h5 class='col-md-5' >Reference id: </h5><h5 class='col-md-6' >" + data.id + "</h5>" +
            "</div>" +
            "<div>" +
            "<input type='hidden' class='form-control bcon-field' type='text' name='entitydata' id='entitydata' name='entitydata'/>" +
            "<h5 class='col-md-5' >Instructions: </h5><h5 class='col-md-6' >" + data.value + "" ;
        if (data.status == "DOCUMENT_PRESENT") {
            html = html + "<input type='button' id='modifyMandate' style='padding: 2px 2px !important;' class='btn modifyM my-btn btn-danger btnpad '  Value='Modify' onClick='updatemandateInstruction(" + JSON.stringify(data) + ")'/>";
        }
        html = html + "</h5></div>" +
            "<div>" ;
        if(data.status == "MOD_DOC_INST_AWAIT_VER") {
            html = html +   "<h5 class='col-md-5'>Status: </h5><div class='col-md-6'><h5>MODIFIED DOC INSTR AWAITING VERFICATION</h5></div>" +
            "</div>";
        }else if(data.status == "MOD_DOC_MAND_AWAIT_VER") {
            html = html +  "<h5 class='col-md-5'>Status: </h5><div class='col-md-6'><h5>MODIFIED DOC MANDATES AWAITING VERFICATION</h5></div>" +
            "</div>";
        }else {
            html = html +   "<h5 class='col-md-5'>Status: </h5><div class='col-md-6'><h5>" + sentenceCase(data.status).replace("_", " ").replace("_", " ").replace("_", " ") + "</h5></div>" +
            "</div>";
        }
        html = html + "<div>" +
            "<h5 class='col-md-5'>Update date:</h5><h5 class='col-md-6' >" + convertJsonDate(data.updatedOn) + "</h5>" +
            "</div>" ;

            if(!(data.emailIndemnity == '' || data.emailIndemnity == null)){
                html = html +"<div style='color: blue !important;'style='color: blue!important;'>" +
                    "<h5 class='col-md-5'>Email indemnity</h5><h5 class='col-md-6'  >" + data.emailIndemnity + "</h5>" +
                    "</div>";
            }

        if (data.status.indexOf("DECLINE")>=0){
            html = html +

                "<h5 class='col-md-5'>Decline reason: </h5><h5 class='col-md-5' ><font color=red>"+ data.reason +"</font></h5>" +
                "<h5 class='col-md-5'>Decline Comment: </h5><h5 class='col-md-5' ><font color=red>"+ data.comment +"</font></h5>" +
                "";
        };
        html = html + "</div>" ;
           html = html + "<div style='float: left; ' class='col-md-4 '>" +
            "<div >" +
            "<img id='mandate-image' src="+ data.freeText2 +">" +
            "</div>" +
            "<div>" ;

           html = html + "<h6 class='col-md-12'><a data-toggle='modal' data-id='mandateview' title='' class='mandateview'>View mandate</a>" ;
        if (data.status == "DOCUMENT_PRESENT") {

             html = html + "<input type='button' id='modifyMandate' style='padding: 2px 2px !important;' class='btn modifyM my-btn btn-danger btnpad pull-right' Value='Modify' onClick='updatemandate(" + JSON.stringify(data) + ")'/>";
        }

            // "<h6 class='col-md-12'><a id='viewMandate' target='_blank' href='javascript: getmandateimg(" + JSON.stringify(data) + ")'>View</a></h6>" +
          html = html+  "</h6></div>" +
            "</div>" ;
        if (data.status == "DOCUMENT_PRESENT") {
            html = html + "<input type='button' id='modifyMandate' class='btn modifyM btn-danger btnpad ' Value='Modify' onClick='updatemandatefull(" + JSON.stringify(data) + ")'/>    " +
            "<input type='button' id='modifyMandate' class='btn modifyM btn-danger' Value='Delete' onClick='expireMandateMain(" + JSON.stringify(data) + ")'/>" ;
        }
        html = html + "</fieldset>" +
            "</div>" +
            // "<a data-toggle='modal' data-id='mandateview' title='Mandate view page inquiry' class='mandateview'>View mandate</a>"+
            "<div class='col-md-12'>";

        if (data.status.indexOf("AWAITING") >= 0 || data.status=="MOD_DOC_INST_AWAIT_VER" || data.status=="MOD_DOC_MAND_AWAIT_VER" || data.status=="MAND_EXP_AWAIT_VER")  {
            if(document.getElementById("currentUserId").value == data.updatedBy.id) {
                html = html + "<input type='button' id='modifyMandate' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatemandatefull(" + JSON.stringify(data) + ")'/>";
                html = html + "<input type='button'  class='btn btn-danger btnpad pull-right' id='cancelMandate' Value='Cancel' onClick='cancelmandate(this.form)'/>";
            }
            if (sol === document.getElementById("currentUserSol").value) {
                html = html + "<input type='button' id='verifyMandate' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifymandate(this.form)'/>";
                html = html + "<input type='button' id='declineMandate' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='mandatedecline(" + data.id + ")'/>";
                      }

            } else {
            if (data.status.indexOf("DECLINE") >= 0) {
                if(document.getElementById("currentUserId").value == data.updatedBy.id) {
                      html = html + "<input type='button' id='modifyMandate' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatemandatefull(" + JSON.stringify(data) + ")'/>";
                    html = html + "<input type='button'  class='btn btn-danger btnpad pull-right' id='cancelMandate' Value='Cancel' onClick='cancelmandate(this.form)'/>";
                }
            }
            // else if (data.status == "NEW_DOCUMENT_VERIFIED" ||  data.status == "DOCUMENT_PRESENT" ||  data.status == "MODIFY_DOCUMENT_VERIFIED" ) {
            //     html = html + "<input type='button' id='removeMandate' class='btn btn-danger btnpad pull-right' Value='Remove' onClick='removemandate(this.form)'/>";
            // }
           //  if (data.status == "DOCUMENT_PRESENT") {
           //      html = html + "<input type='button' id='modifyMandate' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatemandate(" + JSON.stringify(data) + ")'/>";
           // html = html + "<input type='button' id='modifyMandate' class='btn btn-danger btnpad pull-right' Value='Modify Instruct' onClick='updatemandateInstruction(" + JSON.stringify(data) + ")'/>";
           //  }
        }

        hideSpinner("mandate");
        document.getElementById("mandatetop").innerHTML = html;
        $('#mandate-image').attr("src", data.freeText2 +"");

        getMandateRole();
    }
}

function newmandate(acctid){
    html = "<form action='#' id='upload_form' enctype='multipart/form-data' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>New Mandate</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<input type='hidden' name='searchacctid' value='"+ acctid +"' >"+

            "<input type='hidden' name='entityid' id='entityid' value='1' />" +
            "<input type='hidden' name='entitycategory' id='entitycategory' value='MANDATE' />" +
            "<div class='col-md-12'>" +
                "<label for='category'>Mandate Instruction</label>" +
                "<input id='mandatename' class='form-control bcon-field' type='text' name='entitykey' onkeyup='validatemandate(this.form)' required />" +
                "<small class='text-muted' id='instno'>Please enter instruction/description</small>" +
            "</div>" +
        "<div class='col-md-12'>" +
        "<label><input type='checkbox' id='indemnity'  name='indemnity'>Email Indemnity </label>"+
                "<div hidden id='emaildiv'>" +
                "<input id='entitydata' class='form-control bcon-field' type='text' name='entitydata' onkeyup='validatemandate(this.form)' required />" +
                "<small class='text-muted' id='instno1'>Please enter email address</small></div>" +
            "</div>" +
            "<div class='col-md-12'>" +
                "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field' onchange='validatemandate(this.form)' data-preview-file-type='text' >" +
                "<small class='text-muted' id='fileupload'>Please select image</small>" +
            "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='addnewmandate(this.form)'/>" +
        "</div>" +
        "</form>";
    hideSpinner("mandate");

    document.getElementById("mandatetop").innerHTML = html;

    var $conditionalInput = $('#emaildiv');
    var $subscribeInput = $('input[name="indemnity"]');

    $conditionalInput.hide();
    $subscribeInput.on('click', function(){
        if ( $(this).is(':checked') )
            $conditionalInput.show();
        else
            $conditionalInput.hide();
    });
}



//This displays the view for the decline form
function mandatedecline(id){
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Mandate Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory' value />" +
        "<div >" +
        "<label for='reason'>Reason </label>" +
        "<input class='form-control bcon-field' type='text' id='entitykey' name='entitykey' onkeyup='validatemandatedecline(this.form)' required />" +
        "<small class='text-muted' id='demreason'>Please enter a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' name='entitydata' class='form-control dpicker bcon-field' type='text' onkeyup='validatemandatedecline(this.form)'  />" +
        "<small class='text-muted' id='demremark'>please enter a comment</small>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='declinemandate(this.form)'/>" +
       "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='mandateitem(" + id+")'/>"+
    "</div>" +
        "</form>";
    hideSpinner("mandate");
    document.getElementById("mandatetop").innerHTML = html;
}



function updatemandate(data){

    html = "<form action='#' id='upload_form' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Mandate Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<input type='hidden' name='searchacctid2' value='"+ acctid +"' >"+
        "<input type='hidden' name='entitydata' value='' >"+

        "<input type='hidden' name='entityid' id='entityid' value='"+data.id+"' />" +
        "<input type='hidden' name='entitykey' id='entitykey' value='"+data.id+"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory' value='MANDATE' />" +
        "<div class='col-md-12'>" +
        "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field' onchange='validatemandateupdate(this.form)' data-preview-file-type='text' >" +
        "<small class='text-muted' id='fileupload'>Please select image</small>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Update' onClick='modifymandate(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='mandateitem(" + data.id+")'/>"+
        "</div>" +
        "</form>";
    hideSpinner("mandate");
    document.getElementById("mandatetop").innerHTML = html;
}

function expireMandateMain(data) {
    var postRequest = {'entityid':data.id};
            $.ajax({
                url: '/mandate/expire',
                type: 'POST',
                data: postRequest,
                async: false,
                success: resultmandate,
                error: resultmandate,
            });
}
function updatemandatefull(data){

    html = "<form action='#' id='upload_form' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Mandate Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<input type='hidden' name='searchacctid2' value='"+ acctid +"' >"+
        "<input type='hidden' name='entitydata' value='' >"+

        "<input type='hidden' name='entityid' id='entityid' value='"+data.id+"' />" +
        // "<input type='hidden' name='entitykey' id='entitykey' value='"+data.id+"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory' value='MANDATE' />" +
        "<div class='col-md-12'>" +
        "<label for='category'>Mandate Instruction</label>" +
        "<input id='entitykey' class='form-control bcon-field' type='text' name='entitykey' onkeyup='validatemandate(this.form)' required />" +
        "<small class='text-muted' id='instno'>Please enter instruction/description</small>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input id='file-upload' name='file_upload' type='file' class='file form-control bcon-field' onchange='validatemandateupdate(this.form)' data-preview-file-type='text' >" +
        "<small class='text-muted' id='fileupload'>Please select image</small>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Update' onClick='modifymandateFull(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='mandateitem(" + data.id+")'/>"+
        "</div>" +
        "</form>";
    hideSpinner("mandate");
    document.getElementById("mandatetop").innerHTML = html;
}

function updatemandateInstruction(data){

    html = "<form action='#' id='updateMan_form' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Mandate Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<input type='hidden' name='searchacctid2' value='"+ acctid +"' >"+
        "<input type='hidden' name='entitydata' value='' >"+
        "<input type='hidden' name='entityid' id='entityid' value='"+data.id+"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory' value='MANDATE' />" +
        "<div class='col-md-12'>" +
        "<label for='category'>Mandate Instruction</label>" +
        "<input id='entitykey' class='form-control bcon-field' value='"+data.value+"' type='text' name='entitykey' onkeyup='validatemandateInsupdate(this.form)' required />" +
        "<small class='text-muted' id='instno'>Please enter instruction/description</small>" +
        "</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Update' onClick='modifymandateInstruction(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='mandateitem(" + data.id+")'/>"+
        "</div>" +
        "</form>";
    hideSpinner("mandate");
    document.getElementById("mandatetop").innerHTML = html;
}

function resultmandate(data){
    html = "<div class='col-md-12'>" +
        "<div class='col-md-12 response-box'><div class='inner-response'>" +
        "<label><h3>"+ data.searchmessage +"</h3></label><p></p>" +
        "<label><h7>Request Id: "+ data.entityid +"</h7></label>" +
        "</div><div class='col-md-12'>" +
        "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='mandateitem("+data.entityid+","+data.searchacctid+")'/>" +
        "</div></div></div>";
    hideSpinner("mandate");
    document.getElementById("mandatetop").innerHTML = html;
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        url: '/mandate/getmandatebottom',
        success: reloadmandatebottom,
        error : errorin
    });
}
function errorin(data){

}
function reloadmandatebottom(data){


    htmlbottom = "<table class='table table-hover table-condensed mandateTable'> " +
        "<thead> <tr> <th>Category</th> <th>Mandate instruction</th> <th>Status</th> " +
        "<th><a type='button' id='newMandate'  class='pull-right btn btn-sm' href='javascript:newmandate()'> " +
        "<i class='fa fa-plus-circle'></i>&nbsp;New mandate</a></th> </tr> </thead>" +
        "<tbody>";
    for (var i=0;i<data.length;i++){
        if(i==0){
            htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:mandateitem(" +data[i].id+","+data.searchacctid+ ")&apos;'  role='button' class='highlightcolor'>";
        }else{
            htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:mandateitem(" +data[i].id+","+data.searchacctid+ ")&apos;'  role='button'>";
        }
           htmlbottom = htmlbottom + "<td>"+data[i].category+"</td>"+
            "<td>"+data[i].value+"</td>" +
            "<td>"+sentenceCase(data[i].status).replace("New_document"," New mandate ").replace("document_"," mandate ").replace("_"," ").replace("_"," ")+"</td><td></td>" +
            "</tr>";
    }

    htmlbottom = htmlbottom + "</tbody> </table>";
    document.getElementById("mandatebottom").innerHTML = htmlbottom;

}








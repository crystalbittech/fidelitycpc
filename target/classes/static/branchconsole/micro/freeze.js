/*
* This does the frontend validation
 */
function validatefreeze(form){


    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("freason").innerHTML = "<font color=red>You must select a freeze reason</font>";
        return false;
    }else{document.getElementById("freason").innerHTML = "<font color=green>Valid</font>";}
    if(form.entitydata.value == "OTH"){
        if (form.file_upload.value==null || form.file_upload.value==''|| form.file_upload.value==undefined){
            document.getElementById("fname").innerHTML = "<font color=red>You must select a document for this reason</font>";
            return false;
        }else{
            if(ValidatePDFInFOrm(form)){
                console.log("Es!!")
            document.getElementById("fname").innerHTML = "<font color=green>Valid</font>";
            }
        }
    }else{document.getElementById("fname").innerHTML = "<font color=green>You can select a document(optional)</font>"}
    if (form.entitycategory.value==null || form.entitycategory.value==''){
        document.getElementById("fcategory").innerHTML = "<font color=blue>You must select a freeze category</font>";
        return false;
    }else{document.getElementById("fcategory").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("fremark").innerHTML = "<font color=blue>You must provide a freeze remark</font>";
        return false;
    }else{document.getElementById("fremark").innerHTML = "<font color=green>Valid</font>";}

    return true;
}/*
* This does the frontend validation for declination
 */
function validatedecline(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("decreason").innerHTML = "<font color=blue>You must provide a reason</font>";
        return false;
    }else{document.getElementById("decreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("decremark").innerHTML = "<font color=blue>You must provide a comment</font>";
        return false;
    }else{document.getElementById("decremark").innerHTML = "<font color=green>Valid</font>";}
    return true;
}

/**
 * POST  FreezeController addnew
 * This creates a new freeze on account
 */
function addnewfreeze(form) {
    // if (validatefreeze(form) == true) {
    //     showSpinner("freeze");
    //     var url = '/freeze/addnew';
    //     var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    //     postRequest.postdata(url, resultfreeze);
    // }

    if (validatefreeze(form)==true) {
        $("form#upload_form").submit(function () {
            showSpinner("freeze");
            var formData = new FormData($(this)[0]);
            var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value,this.entitydata.value, this.file_upload.value);
            postRequest.appendinputsformdata(formData);
            $.ajax({
                url: '/freeze/addnew',
                type: 'POST',
                data: formData,
                async: false,
                success: resultfreeze,
                error: function(){ document.getElementById('freezetop').innerHTML = "<h3>There was an error modifying the freeze</h3>"},
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });
        $("form#upload_form").submit();
    }else{return false;}
}

/**
 * POST  FreezeController remove
 * This removes a freeze on account
 */
function removefreeze(form) {
    showSpinner("freeze");
    var url = '/freeze/remove';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultfreeze);
}


/**
 * POST  FreezeController modify
 * This removes a freeze on account
 */
function modifyfreeze(form) {
    // if (validatefreeze(form) == true) {
    //     showSpinner("freeze");
    //     var url = '/freeze/modify';
    //     var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    //     postRequest.postdata(url, resultfreeze);
    // }
 console.log("IN!!")
    if (validatefreeze(form)==true) {
        $("form#modifyForm").submit(function () {
            showSpinner("freeze");
            var formData = new FormData($(this)[0]);
            var postRequest = new PostRequest(this.entityid.value, this.entitycategory.value, this.entitykey.value,this.entitydata.value, this.file_upload.value);
            postRequest.appendinputsformdata(formData);
            $.ajax({
                url: '/freeze/modify',
                type: 'POST',
                data: formData,
                async: false,
                success: resultfreeze,
                error: function(){ document.getElementById('freezetop').innerHTML = "<h3>There was an error modifying the freeze</h3>";},
                cache: false,
                contentType: false,
                processData: false
            });

            return false;
        });
        $("form#modifyForm").submit();
    }else{
     console.log("wahala oo")
     return false;}
}

/**
 * POST  FreezeController verify
 * This verifies a freeze process
 */
function verifyfreeze(form) {
    // alert(form.entityid);
    showSpinner("freeze");
    var url = '/freeze/verify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultfreeze);
}

/**
 * POST  FreezeController cancel
 * This cancels a freeze action
 */
function cancelfreeze(form) {
    showSpinner("freeze");
    var url = '/freeze/cancel';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultfreeze);
}

/**
 * POST  FreezeController decline
 * This declines a freeze process
 */
function declinefreeze(form) {
    if (validatedecline(form) == true) {
        showSpinner("freeze");
        var url = '/freeze/decline';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
        postRequest.postdata(url, resultfreeze);
    }
}

function freezeitem(id,accid){
     showSpinner("freeze");
    //START HIGHLIGHTING
    $(".freezeTable tr").click(function() {
        $("tr").removeClass("highlightcolor");
        $(this).addClass("highlightcolor");
    });
    //END HIGHLIGHTING
         url = '/freeze/getdata'
         var postdata = {
             'id': id,
             'accid': accid
         }
    var acctId = $("#acctid").val();
    // $( "#freezetop" ).load( "/freeze/get/"+id);
    /*small
    changes
     */
    $.ajax({
        type:'POST',
        data:{"id":id,"acctId":acctId},
        url:'/freeze/getWithPost',
        async:false,
        success:function(data){document.getElementById("freezetop").innerHTML=data;console.log("successfull")},
        error:function(){console.log("Error occured");}
    })
    
       //  process(url, postdata, explodefreeze);
         hideSpinner("freeze");
}

function getFreezeRole() {
    var role;
    showSpinner("freeze") ;
    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        dataType: 'json',
        url: '/user/role/freeze',
        success: function (data) {
            role = data;
            hideSpinner("freeze") ;
            effectFreezeRoles(role);
        },
        error : function (data) {

            hideSpinner("freeze") ;
        }
    });

}


function effectFreezeRoles(role){

    if(!role.declineFreeze){
        $("#declineFreeze").css("display", "none");
        $("#declineFreeze").prop("disabled", true);

    }if(!role.modifyFreeze){
        $("#modifyFreeze").css("display", "none");
        $("#modifyFreeze").prop("disabled", true);

    }if(!role.verifyFreeze){
        $("#verifyFreeze").css("display", "none");
        $("#verifyFreeze").prop("disabled", true);

    }if(!role.cancelFreeze){
        $("#cancelFreeze").css("display", "none");
        $("#cancelFreeze").prop("disabled", true);

    }if(!role.removeFreeze){
        $("#removeFreeze").css("display", "none");
        $("#removeFreeze").prop("disabled", true);

    }if(!role.addFreeze){



        // $("#addLien").prop("disabled", true);
        $('#addFreeze').click(function(e) {
            e.preventDefault();
            //do other stuff when a click happens
            bootbox.alert("Sorry You Are Not priviledged To Perfom This task");
        });

    }

}

/**
 * POST  FreezeController get freeze
 * This get a freeze with the freeze id
 */
function getfreeze(id){
showSpinner("freeze");
    url = '/freeze/getdata'
    var postdata = {
        'id': id
    }
    $( "#freezetop" ).load( "/freeze/get/"+id );
    
    //process(url, postdata, explodefreeze);
}

function explodefreeze(data){
    if(data==null || data.id=="none"){document.getElementById("freezetop").innerHTML = "NO FREEZE ON ACCOUNT";}else {
        html = "<form action='#' method='POST'>" +
            "<div class='col-md-12'>" +
            "<h6 class='modal-title pull-left' id='myModalLabel'>Freeze Details</h6>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
            "<fieldset class='form-group'>" +
            "<div style='' class='col-md-6'>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitycategory' name='entitycategory'/>" +
            "<h5 class='col-md-5'>Category: </h5><h5 class='col-md-6'  >" + sentenceCase(data.category).replace("_", " ") + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='" + data.id + "'  id='entityid' name='entityid'/>" +
            "<h5 class='col-md-5'>Reason: </h5><h5 class='col-md-6'  >" + sentenceCase(data.reasonCode).replace("_", " ") + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Status: </h5><h5 class='col-md-6' >" + sentenceCase(data.status).replace("_", " ").replace("_", " ") + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitydata' name='entitydata'/>" +
            "<h5 class='col-md-5' >Request Id: </h5><h5 class='col-md-6' >" + data.id + "</h5>" +
            "</div>" +
            "</div>" +
            "<div style='' class='col-md-6 '>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Updated by:</h5><h5 class='col-md-6'  >" + sentenceCase(data.updatedBy.username) + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Updated at:</h5><h5 class='col-md-6' >" + sentenceCase(data.updatedBy.branch.solDescription) + "</h5>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<h5 class='col-md-5'>Updated date:</h5><h5 class='col-md-6' >" + convertJsonDate(data.updatedOn) + "</h5>" +
            "</div>";
        if (!(data.accessURL.indexOf("NOTFOUND") >= 0)) {

        html = html + "<div class='col-md-12'>" +
            "<h5 class='col-md-6' ><a class='btn btn-sm btn-info' id='viewLink' target='_blank' href='/file/load/" + data.accessURL + "'>" + "DOC" + "</a></h5>" +
            "</div>";
         }
        if (data.status.indexOf("DECLINE")>=0){
            html = html +
                "<div class='col-md-12'>" +
                "<h5 class='col-md-5'>Decline reason: </h5><h5 class='col-md-6' ><font color=red>"+ data.reason +"</font></h5>" +
                "</div>"+
                "<div class='col-md-12'>" +
                "<h5 class='col-md-5'>Decline Comment: </h5><h5 class='col-md-6' ><font color=red>"+ data.comment +"</font></h5>" +
                "</div>";
        }
        html = html + "</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>";

    if (data.status.indexOf("AWAITING")>=0){
        if (data.updatedBy.branch.sol == document.getElementById("currentUserSol").value) {
            html = html + "<input type='button' id='verifyFreeze' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifyfreeze(this.form)'/>";
            html = html + "<input type='button' id='declineFreeze' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='freezedecline(" + data.id + ")'/>";
        }

        if(document.getElementById("currentUserId").value == data.updatedBy.id){
            html = html + "<input type='button' id='cancelFreeze' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelfreeze(this.form)'/>";
        }
        html = html + "<input type='button'  id='modifyFreeze' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatefreeze(" + JSON.stringify(data) +")'/>";

    }else{
        if (data.status.indexOf("DECLINE")>=0){
            if(document.getElementById("currentUserId").value == data.updatedBy.id) {
                html = html + "<input type='button' id='cancelFreeze' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelfreeze(this.form)'/>";
            }

            } else if(data.status == "FREEZE_PRESENT") {
            if (data.updatedBy.branch.sol == "001" || data.updatedBy.branch.sol == document.getElementById("currentUserSol").value) {
            html = html + "<input type='button'  id='modifyFreeze' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatefreeze(" + JSON.stringify(data) + ")'/>";
            html = html + "<input type='button' id='removeFreeze' class='btn btn-danger btnpad pull-right' Value='Remove' onClick='removefreeze(this.form)'/>";
                    }
        }
        if (data.status!="REMOVE_FREEZE_VERIFIED" && data.status != "FREEZE_PRESENT"){
            html = html + "<input type='button'  id='modifyFreeze' class='btn btn-danger btnpad pull-right' Value='Modify' onClick='updatefreeze(" + JSON.stringify(data) +")'/>";
        }
    }
    html = html + "</div>" + "</form>";
    hideSpinner("freeze") ;
    document.getElementById("freezetop").innerHTML = html;
        getFreezeRole();
}
}
function newfreeze() {

    $.ajax({
        type:'POST',
        url:'/freeze/form',
        success:function (data) {
            $("#freezetop").html(data);
        },
    });
    // alert($('#acctid').val());
        hideSpinner("freeze");



}
//This displays the view for the decline form
function freezedecline(id){

    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Freeze Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +
        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory'  />" +

        "<div >" +
        "<label for='reason'>Reason</label>" +
        "<input class='form-control bcon-field' type='text' id='entitykey' name='entitykey' required onkeyup='validatedecline(this.form)' />" +
        "<small class='text-muted' id='decreason'>Please enter a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' class='form-control dpicker bcon-field' type='text' name='entitydata' onkeyup='validatedecline(this.form)'/>" +
        "<small class='text-muted' id='decremark'>please enter a comment</small>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='declinefreeze(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-right' Value='Back' onClick='freezeitem("+id+")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("freeze") ;
    document.getElementById("freezetop").innerHTML = html;
}



function updatefreeze(data){

    $( "#freezetop" ).load( "/freeze/modify/"+data.id );

    hideSpinner("freeze") ;
}
function resultfreeze(data) {

    var summarylist = [];
    var postdata = {
        'id': data.entityid,
        'accid': data.searchacctid
    };
    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: postdata,
        async: false,
        url: '/freeze/fetchsummarydata',
        success: function (data) {
            summarylist.push(data.category);
            summarylist.push(data.status);
            summarylist.push(data.reason);
        },
        error: function () {
        }
    });


    if (data.entityid == null || data.entityid == "" || data.entityid == "none" || data.entityid == "undefined" || data.entityid == undefined) {
        html = "Freeze removed";
        hideSpinner("freeze");
    } else {
        html = "<div class='col-md-12'>" +
            "<div class='col-md-12 response-box'><div class='inner-response'>" +
            "<label><h3>" + data.searchmessage + "</h3></label><p></p>" +
            "<label><h7>Request Id: " + data.entityid + "</h7></label>" +
            "</div><div class='col-md-12'>" +
            "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='freezeitem(" + data.entityid + "," + data.searchacctid + ")'/>" +
            "</div>" +
            "<div>" +
            "<label for='remarks'>" + "Freeze category: " + sentenceCase(summarylist[0]) + "</label>" +
            "</div>" +
            "<div>" +
            "<label for='remarks'>" + "Freeze status: " + sentenceCase(summarylist[1]).replace("_", " ").replace("_", " ") + "</label>" +
            "</div>" +
            "<div>" +
            "<label for='remarks'>" + "Freeze reason: " + sentenceCase(summarylist[2]) + "</label>" +
            "</div>" +
            "</div></div>"
    }
    ;
    hideSpinner("freeze");
    document.getElementById("freezetop").innerHTML = html;

    var acctId =$('#acctid').val();

    $("#freezebottom").load("/freeze/reload/below/"+acctId);
    function reloadfreezebottom(data) {


        hideSpinner("freeze");
        document.getElementById("freeze_tab").innerHTML = data;
    }
}
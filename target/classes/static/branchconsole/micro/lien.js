/*
 * This does the frontend validation
 */
function validatelien(form){


    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("lreason").innerHTML = "<font color=blue>Please enter a correct amount</font>";
        return false;
    }else{document.getElementById("lreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitycategory.value==null || form.entitycategory.value==''){
        document.getElementById("lcategory").innerHTML = "<font color=blue>You must select a lien category</font>";
        return false;
    }else{document.getElementById("lcategory").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("expdate").innerHTML = "<font color=blue>You must provide an expiry date</font>";
        return false;
    }else{document.getElementById("expdate").innerHTML = "<font color=green>Valid</font>";}
    if (form.lienremark.value==null || form.lienremark.value==''){
        document.getElementById("remark").innerHTML = "<font color=blue>You must provide a lien remark</font>";
        return false;
    }else{document.getElementById("remark").innerHTML = "<font color=green>Valid</font>";}
    return true;
}
function validatelienmodify(form){
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("lreason1").innerHTML = "<font color=blue>Please enter a correct amount</font>";
        return false;
    }else{document.getElementById("lreason1").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitycategory.value==null || form.entitycategory.value==''){
        document.getElementById("lcategory1").innerHTML = "<font color=blue>You must select a lien category</font>";
        return false;
    }else{document.getElementById("lcategory1").innerHTML = "<font color=green>Valid</font>";}
    if (form.lienremark.value==null || form.lienremark.value==''){
        document.getElementById("remark1").innerHTML = "<font color=blue>You must provide a lien remark</font>";
        return false;
    }else{document.getElementById("remark1").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("expdate1").innerHTML = "<font color=blue>You must provide an expiry date</font>";
        return false;
    }else{document.getElementById("expdate1").innerHTML = "<font color=green>Valid</font>";}
    return true;
}/*
 * This does the frontend validation for declination
 */
function validateliendecline(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("ldecreason").innerHTML = "<font color=blue>You must provide a reason</font>";
        return false;
    }else{document.getElementById("ldecreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("ldecremark").innerHTML = "<font color=blue>You must provide a comment</font>";
        return false;
    }else{document.getElementById("ldecremark").innerHTML = "<font color=green>Valid</font>";}
    return true;
}

/**
 * POST  LienController addnew
 * This creates a new lien on account
 */
function addnewlien(form) {

    if (validatelien(form) == true) {
        showSpinner("lien");
        var url = '/lien/addnew';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value,form.lienremark.value);
        postRequest.postdata(url, resultlien);
    }
}

function lienitem(id,accid){
    showSpinner("lien");
    $(".lienTable tr").click(function() {
        $("tr").removeClass("highlightcolor");
        $(this).addClass("highlightcolor");
    });
    //END HIGHLIGHTING
    url = '/lien/getdata';
    var postdata = {
        'id': id,
        'accid': accid
    }

    
    $( "#lientop" ).load( "/lien/get/"+id , function(responseTxt, statusTxt, xhr) {
        $('#entitykey').datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: "linked",
            startDate: '+0d',
            autoclose: true
        });
    });


   // process(url, postdata, explodelien);
    hideSpinner("lien");
}

function getLienRole() {
    var role;
    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        dataType: 'json',
        url: '/user/role/lien',
        success: function (data) {
            role = data;
            effectLienRoles(role);
        },
        error : function (data) {

        }
    });

}

function effectLienRoles(role){

    if(!role.declineLien){
        $("#declineLien").css("display", "none");
        $("#declineLien").prop("disabled", true);

    }if(!role.modifyLien){
        $("#modifyLien").css("display","none");
        $("#modifyLien").prop("disabled", true);

    }if(!role.verifyLien){
        $("#verifyLien").css("display","none");
        $("#verifyLien").prop("disabled", true);

    }if(!role.cancelLien){
        $("#cancelLien").css("display","none");
        $("#cancelLien").prop("disabled", true);

    }if(!role.addLien){

        console.log("YEs!")
        // $("#addLien").prop("disabled", true);
        $('#addLien').click(function(e) {
            e.preventDefault();
            //do other stuff when a click happens
            bootbox.alert("Sorry You Are Not priviledged To Perfom This task")
        });

    }

}

function explodelien(data){

    console.log(data)
// alert("in lien");
    if(data==null || data.id=="none"){document.getElementById("lientop").innerHTML = "";}else {
        if (data.amount == null) {
            data.amount = '';
        }
        if (data.id == null) {
            data.id = '';
        }
        if (data.status == null) {
            data.status = '';
        }
        if (data.category == null) {
            data.category = '';
        }
        if (data.reason == null) {
            data.reason = '';
        }
        if (data.updatedBy.branch.solDescription == null) {
            data.updatedBy.branch.solDescription = '';
        }


        var fdate=""+data.expiryDate;
        var concat = "";
        var date = new Date(parseInt(fdate));
        concat = concat + (date.getDay()+1) + "/"+ (date.getMonth()+1) + "/" + (date.getFullYear());
        var update=""+data.updatedOn;
        var concat1 = "";
        var date1 = new Date(parseInt(update));
        concat1 = concat1 + (date1.getDay()+1) + "/"+ (date1.getMonth()+1) + "/" + (date1.getFullYear());

        html = "<form action='#' method='POST'>" +
            "<div class='col-md-12'>" +
            "<h6 class='modal-title pull-left' id='myModalLabel'>Lien details</h6>" +
            "</div>" +
            "<div class='col-md-12'>" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='01-09-2016' id='entitykey' name='entitykey'/>" +
            "<fieldset class='form-group'>" +
            "<div style='' class='col-md-6'>" +
            "<div >" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='" + data.reason + "' id='entitycategory' name='entitycategory'/>" +
            "<h5 class='col-md-5'>Amount: </h5><h5 class='col-md-6'  >" + data.amount + "</h5>" +
            "</div>" +
            "<div >" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='" + data.id + "'  id='entityid' name='entityid'/>" +
            "<h5 class='col-md-5'>Expiry Date: </h5><h5 class='col-md-6'  >" + convertJsonDate(data.expiryDate) +"</h5>" +
            "</div>" +
            "<div >" +
            "<h5 class='col-md-5'>Category: </h5><h5 class='col-md-6' >" + sentenceCase(data.category) + "</h5>" +
            "</div>" +
            "<div >" +
            "<h5 class='col-md-5'>Status: </h5><h5 class='col-md-6' >" + sentenceCase(data.status).replace("_"," ").replace("_"," ") + "</h5>" +
            "</div>" +
            "<div >" +
            "<input type='hidden' class='form-control bcon-field' type='text' value='" + data.amount + "' id='entitydata' name='entitydata'/>" +
            "<h5 class='col-md-5' >Remark: </h5><h5 class='col-md-6' >" + sentenceCase(data.value)+ "</h5>" +

            "</div>" +
            "<div >" +
            "<h5 class='col-md-5' >Request Id: </h5><h5 class='col-md-6' >" + data.id + "</h5>" +
            "</div>" +
            "</div>" +
            "<div style='' class='col-md-6 '>" +
            "<div >" +
            "<h6 class='col-md-5'>Update by:</h6><h6 class='col-md-6'  >" + sentenceCase(data.updatedBy.username) + "</h6>" +
            "</div>" +
            "<div >" +
            "<h6 class='col-md-5'>Lien placed by:</h6><h6 class='col-md-6'  >" + sentenceCase(data.createdBy.username) + "</h6>" +
            "</div>" +
            "<div >" +
            "<h6 class='col-md-5'>Update at:</h6><h6 class='col-md-6' >" + sentenceCase(data.updatedBy.branch.solDescription) + "</h6>" +
            "</div>" +
            "<div >" +
            "<h5 class='col-md-5'>Update date:</h5><h5 class='col-md-6' >" + convertJsonDate(data.updatedOn) + "</h5>" +
            "</div>" +
            "<div >" +
            "<h5 class='col-md-5'>Created date:</h5><h5 class='col-md-6' >" + convertJsonDate(data.createdOn) + "</h5>" +
            "</div>";
        if (data.status.indexOf("DECLINE")>=0){
            html = html +
                "<div class='col-md-12'>" +
                "<h5 class='col-md-5'>Decline reason: </h5><h5 class='col-md-6' ><font color=blue>"+ data.reason +"</font></h5>" +
                "</div>"+
                "<div class='col-md-12'>" +
                "<h5 class='col-md-5'>Decline Comment: </h5><h5 class='col-md-6' ><font color=blue>"+ data.comment +"</font></h5>" +
                "</div>";
        };
        html = html + "</div>" +
            "</fieldset>" +
            "</div>" +
            "<div class='col-md-12'>";
        if (data.status.indexOf("AWAITING") >= 0) {
            if (data.updatedBy.branch.sol === document.getElementById("currentUserSol").value) {
                html = html + "<input type='button' id='verifyLien' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifylien(this.form)'/>" +
                    "<input id='declineLien' type='button' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='liendecline(" + data.id + ")'/>";
            }
            if(document.getElementById("currentUserId").value == data.updatedBy.id.toString()) {
                html = html + "<input  id='cancelLien' type='button' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancellien(this.form)'/>";
                html = html + "<input type='button' id='modifyLien' class='btn btn-success btnpad pull-right' Value='Modify' onClick='updatelien(" + JSON.stringify(data) + ")'/>";
            }
        }
        if (data.status.indexOf("DECLINE") >= 0) {
            html = html + "<input type='button' id='modifyLien' class='btn btn-success btnpad pull-right' Value='Modify' onClick='updatelien(" + JSON.stringify(data) + ")'/>" ;
            if(document.getElementById("currentUserId").value == data.updatedBy.id.toString()) {
                console.log("Trye!")
                html = html + "<input  id='cancelLien' type='button' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancellien(this.form)'/>";
            }
        }

        if ((data.status == "LIEN_PRESENT")) {
            if (data.updatedBy.branch.sol == "001" || data.updatedBy.branch.sol == document.getElementById("currentUserSol")) {
                html = html + "<input type='button' id='modifyLien' class='btn btn-success btnpad pull-right' Value='Modify' onClick='updatelien(" + JSON.stringify(data) + ")'/>";
            }
        }
        if ((data.status.indexOf("DECLINE") >= 0) && (data.status.indexOf("AWAITING") >= 0)) {
            html = html + "<input type='button' id='modifyLien' class='btn btn-success btnpad pull-right' Value='Modify' onClick='updatelien(" + JSON.stringify(data) + ")'/>" ;
            // "<input type='button' id='removeLien' class='btn btn-danger btnpad pull-right' Value='Remove' onClick='removelien(this.form)'/>";
        }
        html = html +
            "</div>" +
            "</form>";

        document.getElementById("lientop").innerHTML = html;

        showSpinner("lien")
        getLienRole();
        hideSpinner("lien");
        pickDate();
    }
}
/**
 * POST  LienController remove
 * This removes a lien on account
 */
function newlien() {
    // alert("new lien");
    showSpinner("lien");
    $( "#lientop" ).load( "/lien/newlienpage" , function(responseTxt, statusTxt, xhr) {
        $('.expiryDate').datepicker({
            format: 'dd-mm-yyyy',
            todayBtn: "linked",
            startDate: '+0d',
            autoclose: true
        });
    });
    hideSpinner("lien");
    $('.pick-date').datepicker({
        format: 'dd-mm-yyyy',
        todayBtn: "linked",
        startDate: '+0d',
        autoclose: true
    });

    pickDate();

    //getlienCategory();
}
function pickDate(){
    $('#expiryDate').datepicker({
        format: 'dd-mm-yyyy',
        todayBtn: "linked",
        startDate: '+0d',
        autoclose: true
    });
}
function pickDate2(){
    $('#expiryDate2').datepicker({
        format: 'dd-mm-yyyy',
        todayBtn: "linked",
        startDate: '+0d',
        autoclose: true
    });
}
function updateLienform(lien){
    $.ajax({
        type:'POST',
        url:'/lien/loadLienmodifypage',
        data:{"id":lien.id},
        async:false,
        success:function(data){console.log("Good");document.getElementById("lientop").innerHTML = data;pickDate2();
        },
        error:function () {
            console.log("Error loading lien settings");
        }
    });
}


function updatelien(data) {
    // alert(data);
    if(data.amount== null){
        data.amount = '';
    }
    if(data.id== null){
        data.id = '';
    }
    if(data.status== null){
        data.status = '';
    }
    if(data.reason == null){
        data.reason = '';
    }
    // var settArr = [] ;
    // $.ajax({
    //     type:'POST',
    //     url:'/lien/loadLienSettings',
    //     success:function(data){},
    //     error:function () {
    //         console.log("Error loading lien settings");
    //     }
    // });
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Modify lien</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'> " +
        "<div style='float: left; clear: none; '> " +
        "<input type='hidden' name='entityid' id='entityid'value='"+data.id+"' /> " +
        "</div > " +
        "<div class='col-md-5' > " +
        "<label for='acctnum'>Amount </label> " +
        "<input class='form-control bcon-field' type='number' id='entitydata' value='"+data.amount+"' name='entitydata' onkeypress='return isNumber(event)' required /> " +
        "<small class='text-muted' id='lreason'>Please enter a correct amount</small> " +
        "</div> " +
        "<div class='col-md-5' > " +
        "<label for='remarks'>Category </label> " +
        "<select id='entitycategory' class='form-control' type='text'  name='entitycategory'> " +
        "<option value='' class='form-control'>--Select--</option>"+
        "<option value='BVN' class='form-control'>Bvn</option>"+
        "<option value='59' class='form-control'>59</option>"+
        "<option value='SMS' class='form-control'>Sms</option>"+"</select>"+
        "<small class='text-muted' id='lcategory'>Please pick a category</small> " +
        "</div>" +
        "<div class='col-md-5' > " +
        "<label for='expiry-date'>Expiry date</label> " +
        "<input id='expiryDate' class='form-control bcon-search-input' onClick='pickDate()' name='entitykey' placeholder='Expiry date'/> " +
        "<small class='text-muted' id='expdate'>Please pick an expiry date</small> " +
        "</div> " +
        "<div class='col-md-5' >"+
        "<label for='expiry-date'>Remark</label>"+
        "<input id='lienremark' class='form-control bcon-field' name='lienremark' placeholder='Lien remark'/>"+
        "<small class='text-muted' id='remark'>Provide a remark</small>"+
        "</div>"+
        "</div> " +
        "</fieldset> " +
        "<div class='col-md-12' style='padding-bottom: 5px'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='modifylien(this.form)' /> " +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='lienitem("+data.id+")' /> " +
        "</div>" +
        "</form>";
    hideSpinner("lien");
    document.getElementById("lientop").innerHTML = html;
    $('#expiryDate').datepicker({
        format: 'dd-mm-yyyy',
        todayBtn: "linked",
        startDate: '+0d',
        autoclose: true
    });
}

function removelien(form) {
    showSpinner("lien")
    var url = '/lien/remove';;
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url,resultlien);
}


/**
 * POST  LienController remove
 * This modifies a lien on account
 */
function modifylien(form) {
    if (validatelienmodify(form) == true) {
        showSpinner("lien");
        var url = '/lien/modify';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value,form.lienremark.value);
        postRequest.postdata(url, resultlien);
    }
}


/**
 * POST  LienController remove
 * This verifies a lien action
 */
function verifylien(form) {
    showSpinner("lien");
    var url = '/lien/verify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url,resultlien);
}


/**
 * POST  LienController cancel
 * This cancels a lien action
 */
function cancellien(form) {
    showSpinner("lien");
    var url = '/lien/cancel';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url,resultlien);
}


/**
 * POST  LienController remove
 * This declines a lien action
 */
function liendecline(id){
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Lien Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitycategory' name='entitycategory'/>" +
        "<fieldset class='form-group'>" +
        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory'  />" +
        "<div >" +
        "<label for='reason'>Reason </label>" +
        "<input class='form-control bcon-field' type='text' id='entitykey' name='entitykey' onchange='validateliendecline(this.form)' required />" +
        "<small class='text-muted' id='ldecreason'>Please enter a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' class='form-control dpicker bcon-field' type='text' name='entitydata' onchange='validateliendecline(this.form)' />" +
        "<small class='text-muted' id='ldecremark'>please enter a comment</small>" +
        "</div>" +
        "</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='declinelien(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' Value='Back' onClick='lienitem("+id+")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("lien");
    document.getElementById("lientop").innerHTML = html;
}
function declinelien(form) {
    if (validateliendecline(form) == true) {
        showSpinner("lien");
        var url = '/lien/decline';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
        postRequest.postdata(url, resultlien);
    }
}

function resultlien(data){
    showSpinner("lien");
    var summarylist = [];
    var postdata = {
        'id': data.entityid,
        'accid': data.searchacctid
    };
    $.ajax({
        type:'POST',
        dataType:'json',
        data:postdata,
        async:false,
        url:'/lien/fetchsummarydata',
        success:function(data){summarylist.push(data.amount);summarylist.push(data.category);summarylist.push(data.expiryDate);summarylist.push(data.status);summarylist.push(data.reason);},
        error:function () { }
    });

    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h4 class='modal-title pull-left' id='myModalLabel'>Lien response page</h4>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +

        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ data.searchmessage +"' />" +
        "<div>" +
        "<label for='remarks'>"+ data.searchmessage +"</label>" +
        "</div>" +
        "<div>" +
        "<label for='remarks'>"+ "Lien amount: "+summarylist[0] +"</label>" +
        "</div>" +
        "<div>" +
        "<label for='remarks'>"+ "Lien category: "+sentenceCase(summarylist[1]) +"</label>" +
        "</div>" +
        "<div>" +
        "<label for='remarks'>"+ "Lien expiry date: "+(summarylist[2]) +"</label>" +
        "</div>" +
        "<div>" +
        "<label for='remarks'>"+ "Lien status: "+sentenceCase(summarylist[3])+"</label>" +
        "</div>" +
        "<div>" +
        "<label for='remarks'>"+ "Lien remark: "+sentenceCase(summarylist[4])+"</label>" +
        "</div>";

      html = html +"</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='lienitem("+data.entityid+","+data.searchacctid+")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("lien");
    document.getElementById("lientop").innerHTML = html;
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        url: '/lien/getlienbottom',
        success: reloadlienbottom,
        error : reloadlienbottom
    });
}
function reloadlienbottom(data){

    htmlbottom = "<table class='table table-hover table-condensed lienTable' > " +
        "<thead> <tr> <th>Amount</th> <th>Expiry date</th> <th>Status</th> " +
        "<th><a type='button' id='addLien'  class='pull-right btn btn-sm' href='javascript:newlien()'> " +
        "<i class='fa fa-plus-circle'></i>&nbsp;New Lien</a></th> </tr> </thead>" +
        "<tbody>";
    for (var i=0;i<data.length;i++){
        if(i==0){
            htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:lienitem(" +data[i].id+","+data.searchacctid+ ")&apos;'  role='button' class='highlightcolor'>";
        }else{
            htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:lienitem(" +data[i].id+","+data.searchacctid+ ")&apos;'  role='button'>";
        }
        htmlbottom = htmlbottom + "<td>"+data[i].amount+"</td>"+
            "<td>"+convertJsonDate(data[i].expiryDate)+"</td>" +
            "<td>"+sentenceCase(data[i].status)+"</td><td></td>" +
            "</tr>";
    }
    htmlbottom = htmlbottom + "</tbody> </table>";
    document.getElementById("lienbottom").innerHTML = htmlbottom;
}



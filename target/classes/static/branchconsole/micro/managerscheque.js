/**
 * Created by ZEED on 1/20/2017.
 */
var addClicked = false;
function showAdddPage(){
    //Added by ONI Israel
    var addClicked = false;
    var elems = document.getElementById('fromlocclass').value;
    var elems1 = document.getElementById('tolocclass').value;
    var elems11 = document.getElementById('toloccode').value;

    if (elems==null || elems==''){
        document.getElementById("option1").innerHTML = "<font color=blue>You must select an option</font>";
        return false;
    }else{document.getElementById("option1").innerHTML = "<font color=green>Valid</font>";}
    if (elems1==null || elems1==''){
        document.getElementById("option2").innerHTML = "<font color=blue>You must select an option</font>";
        return false;
    }else{document.getElementById("option2").innerHTML = "<font color=green>Valid</font>";}
    if (elems11==null || elems11==''){
        document.getElementById("option3").innerHTML = "<font color=red>You must select an option</font>";
        return false;
    }else{document.getElementById("option3").innerHTML = "<font color=green>Valid</font>";}

    if((elems == 'ZZ') && (elems1 == 'EM')){
        alert("Cannot move inventory from External to Employee directly");
        return false;
    }
    if((elems == 'DL') && (elems1 == 'DL')){
        alert("Cannot move inventory within the same Classes");
        return false;
    }
    if ((elems1 == 'EM') && (elems11 == 'DL')){
        alert("Select appropriate employee id");
        document.getElementById("option3").innerHTML = "<font color=red>Select appropriate employee id</font>";
        return false;
    }
    //Ends here

    var postdata = {'fromcode':$("#fromloccode").val(),'fromclass':$("#fromlocclass").val(),'tocode':$("#toloccode").val(),'toclass':$("#tolocclass").val()};
    $.ajax({
       type:'POST',
        data:postdata,
        url:'/managerscheque/addpage',
        success: function(data){$("#managerscheque").html(data);},
        error:function(){console.log("An error occured");
        }
    });
}

function validateForm(){
    var elems22 = document.getElementById('alpha').value;
    var elems2 = document.getElementById('startno').value;
    var elems23 = document.getElementById('type').value;

    if (elems23==null || elems23==''){
        document.getElementById("optionS").innerHTML = "<font color=red>You must select an value</font>";
        return false;
    }else{document.getElementById("optionS").innerHTML = "<font color=green>Valid</font>";}

    if (elems22==null || elems22==''){
        document.getElementById("optionA").innerHTML = "<font color=red>You must select an value</font>";
        return false;
    }else{document.getElementById("optionA").innerHTML = "<font color=green>Valid</font>";}

    if (elems2==null || elems2==''){
        document.getElementById("optionB").innerHTML = "<font color=red>You must select an Value</font>";
        return false;
    }else{document.getElementById("optionB").innerHTML = "<font color=green>Valid</font>";}

    var elems3 = Number(elems2) + Number(elems24) - 1;
    document.getElementById('endno').value=elems3;
}

function removefromtable(tthis){
    tthis.parent().parent().remove();
}
function rejectfromtable(tthis,id){
    tthis.parent().parent().remove();
    var postInputed = {'invid':id};
    $.ajax({
       type:'POST',
       async:false,
       data:postInputed,
       url:'/managerscheque/rejectinventory',
       success:function (data) {
           // alert("Oya");
           $("#managerscheque").html(data);
           // alert("Oya2");
       },
       error:function () {
         console.log("Error occured");
       }
    });
}

function modifyManagerscheque(id){
    console.log("Id is "+id);
    var postInputed = {'invid':id};
    $.ajax({
        type:'POST',
        async:false,
        data:postInputed,
        url:'/managerscheque/openmodifymodal',
        success:function (data) {
            // alert("Oya");
            $("#Modifydiv").html(data);
            // alert("Oya2");
        },
        error:function () {
            console.log("Error occured");
        }
    });
}

function addtotable(){
    //Added by ONI Israel
    addClicked = true;
    var elems22 = document.getElementById('alpha').value;
    var elems2 = document.getElementById('startno').value;
    var elems23 = document.getElementById('type').value;
    var elems24 = document.getElementById('qty').value;

    if (elems23==null || elems23==''){
        document.getElementById("optionS").innerHTML = "<font color=red>You must select an value</font>";
        return false;
    }else{document.getElementById("optionS").innerHTML = "<font color=green>Valid</font>";}

    if (elems22==null || elems22==''){
        document.getElementById("optionA").innerHTML = "<font color=red>You must select an value</font>";
        return false;
    }else{document.getElementById("optionA").innerHTML = "<font color=green>Valid</font>";}

    if (elems2==null || elems2==''){
        document.getElementById("optionB").innerHTML = "<font color=red>You must select an Value</font>";
        return false;
    }else{document.getElementById("optionB").innerHTML = "<font color=green>Valid</font>";}

    if (document.getElementById("startno").value.toString().length > 16) {
        document.getElementById("optionB").innerHTML = "<font color=red>Maximum number required is 16</font>";
        return false;
    } else {
        document.getElementById("optionB").innerHTML = "<font color=green>Valid</font>";
    }

    if (elems24==null || elems24==''){
        document.getElementById("optionQ").innerHTML = "<font color=red>You must select an value</font>";
        return false;
    }else{document.getElementById("optionQ").innerHTML = "<font color=green>Valid</font>";}

    if (elems23 == 'WSL25'){
        if (elems24 !== '25'){
            document.getElementById("optionQ").innerHTML = "<font color=red>Invalid number of leaves</font>";
            return false;
        }else{document.getElementById("optionQ").innerHTML = "<font color=green>Valid</font>";
        }
    }
    if ((elems23 == 'DDMC') || (elems23 == 'DDCHQ')){
        if (elems24 == '25'){
            document.getElementById("optionQ").innerHTML = "<font color=red>Invalid number of leaves</font>";
            return false;
        }else{document.getElementById("optionQ").innerHTML = "<font color=green>Valid</font>";
        }
    }


    var elems3 = Number(elems2) + Number(elems24) - 1;
    document.getElementById('endno').value=elems3;
    // document.getElementById('qty').value='25';
    //Ends here

    $("#addtable").append("<tr><td>"+$("#type").val()+"</td><td>"+$("#qty").val()+
    "</td><td>"+$("#alpha").val()+"</td><td>"+$("#startno").val()+"</td><td>"+$("#endno").val()+"</td><td>"+$("#tranp").val()+
    "</td><td><input type='button' onclick='removefromtable($(this))' class='btn btn-primary btn-md' name='submit' value='Remove' /></td></tr>");

}

function submitRecord(){
    //Added by ONI Israel
    if(addClicked == false){
        alert("Kindly ADD records");
        return false;
    }
    var elems22 = document.getElementById('alpha').value;
    var elems2 = document.getElementById('startno').value;
    var elems23 = document.getElementById('type').value;
    var elems24 = document.getElementById('qty').value;

    if (elems24==null || elems24==''){
        document.getElementById("optionQ").innerHTML = "<font color=red>You must select an value</font>";
        return false;
    }else{document.getElementById("optionQ").innerHTML = "<font color=green>Valid</font>";}

    if (elems23==null || elems23==''){
        document.getElementById("optionS").innerHTML = "<font color=red>You must select an value</font>";
        return false;
    }else{document.getElementById("optionS").innerHTML = "<font color=green>Valid</font>";}

    if (elems22==null || elems22==''){
        document.getElementById("optionA").innerHTML = "<font color=red>You must select an value</font>";
        return false;
    }else{document.getElementById("optionA").innerHTML = "<font color=green>Valid</font>";}

    if (elems2==null || elems2==''){
        document.getElementById("optionB").innerHTML = "<font color=red>You must select an Value</font>";
        return false;
    }else{document.getElementById("optionB").innerHTML = "<font color=green>Valid</font>";}

    if (document.getElementById("startno").value.toString().length > 16) {
        document.getElementById("optionB").innerHTML = "<font color=red>Maximum number required is 16</font>";
        return false;
    } else {
        document.getElementById("optionB").innerHTML = "<font color=green>Valid</font>";
    }
    //Ends here
    var iclassdata = {"iclass":[],"iqty":[],"ialpha":[],"istartno":[],"iendno":[],"itransf":[]};
    var MyRows = $('table#addtable').find('tbody').find('tr');
    /*The index starts from 1 so as to escape the table header
     */
    for (var i = 1; i < MyRows.length; i++) {
        iclassdata.iclass.push($(MyRows[i]).find('td:eq(0)').html());
        iclassdata.iqty.push($(MyRows[i]).find('td:eq(1)').html());
        iclassdata.ialpha.push($(MyRows[i]).find('td:eq(2)').html());
        iclassdata.istartno.push($(MyRows[i]).find('td:eq(3)').html());
        iclassdata.iendno.push($(MyRows[i]).find('td:eq(4)').html());
        iclassdata.itransf.push($(MyRows[i]).find('td:eq(5)').html());
    }
    // console.log("Iclassdata "+JSON.stringify(iclassdata));
    var url = "/managerscheque/submitinventory";
    var postInput = {"iclassdata":JSON.stringify(iclassdata),"fromclass":$("#gfromclass").val(),"fromcode":$("#gfromcode").val(),"tocode":$("#gtocode").val(),"toclass":$("#gtoclass").val()};
    post(url, postInput);
}

//Added by ONI Israel
function changeCode(newCode) {
    var elem = document.getElementById('fromlocclass');
    if(elem.value == 'DL'){
        document.getElementById('fromloccode').value='DL';
        document.getElementById('tolocclass').value = 'EM';
        document.getElementById('toloccode').disabled = false;
        document.getElementById('tolocclass').disabled = true;
    }
    if(elem.value == 'ZZ'){
        // document.getElementById('fromloccode').text ='EXT (External to the system)';
        document.getElementById('fromloccode').value = 'EXT';
        document.getElementById('toloccode').value ='DL';
        document.getElementById('toloccode').disabled = true;
        document.getElementById('tolocclass').value = 'DL';
        document.getElementById('tolocclass').disabled = true;
    }
}
/*
function changeCode1(newCode1) {
    var elem1 = document.getElementById('tolocclass');
    if(elem1.value == 'DL'){
        document.getElementById('toloccode').value='DL (Double lock)';
        document.getElementById('toloccode').disabled = true;
    }
    if(elem1.value == 'EM'){
        document.getElementById('toloccode').disabled = false;
    }
}
//Ends here
*/
function loaddiv(){
    if ($("#type").val()=="Add"){
        $("#add").css("display","block");
        $("#verifyc").css("display","none");
        $("#Modifydiv").css("display","none");
    }
    if ($("#type").val()=="Modify"){
        $("#add").css("display","none");
        $("#Modifydiv").css("display","block");
        $("#verifyc").css("display","none");
        loadModifyDiv();
    }if ($("#type").val()=="Verify"){
        $("#add").css("display","none");
        $("#Modifydiv").css("display","none");
        $("#verifyc").css("display","block");
        loadUploadeddetails();
    }
    // if ($("#type").val()!="verify" && $("#type").val()!="Add" && $("#type").val()!="Modify"){
    //     $("#add").css("display","none");
    //     $("#verifyc").css("display","none");
    //     $("#Modifydiv").css("display","none");
    // }
    // $( "#type" ).prop( "disabled", true);
}

function beginNumCheck(form) {
    if (document.getElementById("startno").value.toString().length > 16) {
        document.getElementById("optionB").innerHTML = "<font color=red>Maximum number required is 16</font>";
        return false;
    } else {
        document.getElementById("optionB").innerHTML = "<font color=green>Valid</font>";
    }
}

function beginNumCheck2(form) {
    if (document.getElementById("startNo").value.toString().length > 16) {
        document.getElementById("ModInvtStartNo").innerHTML = "<font color=red>Maximum number required is 16</font>";
        return false;
    } else {
        document.getElementById("ModInvtStartNo").innerHTML = "<font color=green>Valid</font>";
    }
}

function loadUploadeddetails(){
    var id = $("#invid").val();
    var postdata = {"id":id};
    $.ajax({
        type:'POST',
        data:postdata,
        async:false,
        url:'/managerscheque/loadUploadeddetails',
        success:function(data){$("#managerscheque").html(data);},
        error:function () {console.log("An error occured")}
    })
}
function verifyInventory(invId){
    var postdata = {"invId":invId};
    var url = "/managerscheque/verifyInventory";
    post(url,postdata);
}

function loadModifyDiv(){
    $.ajax({
        type:'POST',
        async:false,
        url:'/managerscheque/loadModifyDiv',
        success:function(data){$("#Modifydiv").html(data);},
        error:function (){console.log("An error occured")}
    });
}

function updateInventoryDetails(id){
    if (document.getElementById("startNo").value.toString().length > 16) {
        document.getElementById("ModInvtStartNo").innerHTML = "<font color=red>Maximum number required is 16</font>";
        return false;
    } else {
        document.getElementById("ModInvtStartNo").innerHTML = "<font color=green>Valid</font>";
    }
    
    var postdata = {"id":id,"ctype":$("#ctype").val(),"calpha":$("#calpha").val(),"qty":$("#qty").val(),"startNo":$("#startNo").val(),"transferP":$("#transferP").val()};
    $.ajax({
        type:'POST',
        data:postdata,
        async:false,
        url:'/managerscheque/updateInventoryDetails',
        success:function(data){$("#errMsg").html("<h4>Update successful</h4>");},
        error:function () {console.log("An error occured");$("#errMsg").html("<h4><font color=red> Update failed</font></h4>");}
    });
}
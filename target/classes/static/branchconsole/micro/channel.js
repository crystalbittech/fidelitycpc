/**
 * Created by ZEED on 12/3/2016.
 */
/**
 * POST  FreezeController modify
 * This removes a onboard on account
 */
function modifyonboard(form) {
    if(validatechannel(form)==true) {
        showSpinner("channel");
        var url = '/channel/modify';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
        postRequest.postdata(url, resultchannel);
    }
}
/*
 * This does the frontend validation for declination
 */
function validatechannel(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("demreason").innerHTML = "<font color=red>You must select a reponse</font>";
        return false;
    }else{document.getElementById("demreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("demremark").innerHTML = "<font color=red>You must provide a comment</font>";
        return false;
    }else{document.getElementById("demremark").innerHTML = "<font color=green>Valid</font>";}
    return true;
}
function channelpost(data, url){
    showSpinner("channel");
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        url: url,
        success: function(data) {

            explodechannel(data);
               hideSpinner("channel");
        },
        error : function(data) {

            return data;
        }
    });
}

function channelitem(id,accid){
    // alert(id);
    showSpinner("channel");
    url = '/channel/getdata';

    var postdata = {
        'id': id,
        'accid': accid 
    }
    $( "#channeltop" ).load( "/channel/get/"+id );
    //process(url, postdata, explodechannel);
    // var responsedata =  channelpost(postdata, url)
//    explodefreeze(responsedata);
    hideSpinner("channel");

}

function explodechannel(data){
    console.log('data ME ME ==> ' + JSON.stringify(data));
    if(data==null || data.id=="none"){document.getElementById("channeltop").innerHTML = "";}else {
    if(data.date== null){
        data.date = '';
    }else{data.date = convertJsonDate(data.date)}
    if(data.id== null){
        data.id = '';
    }
    if(data.channelStatus== null){
        data.channelStatus = '';
    }
    if(data.comment == null){
        data.comment = '';
    }
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Channel details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.date+"' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +
        "<div style='' class='col-md-6'>" +
        "<div >" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.category+"' id='entitycategory' name='entitycategory'/>" +
        "<h5 class='col-md-5'>Category: </h5><h5 class='col-md-6'  >"+ sentenceCase(data.category).replace("_"," ") +"</h5>" +
        "</div>" +
        "<div >" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.id+"'  id='entityid' name='entityid'/>" +
        "<h5 class='col-md-5'>Status: </h5><h5 class='col-md-6'  >"+ sentenceCase(data.channelStatus).replace("_"," ") +"</h5>" +
        "</div>" +
        "<div >" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.id+"' id='entitydata' name='entitydata'/>" +
        "<h5 class='col-md-5' >Request Id:  </h5><h5 class='col-md-6' >"+ data.id +"</h5>" +
        "</div>" ;
        if(!(data.value == '' || data.value == null)){
            html = html +
                "<div >" +
                "<h5 class='col-md-5' >Response: </h5><h5 class='col-md-6' >" + sentenceCase(data.value) + "</h5>" +
                "</div>";
        }
        if(!(data.comment == '' || data.comment == null)){
            html = html +
                "<div >" +
                "<h5 class='col-md-5' >Comment: </h5><h5 class='col-md-6' >" + sentenceCase(data.comment) + "</h5>" +
                "</div>";
        }
    // if (!(data.channelStatus.indexOf("NOT_ONB")>=0)) {
    //     html = html +
    //         "<div >" +
    //         "<h5 class='col-md-5' >Comment: </h5><h5 class='col-md-6' >" + sentenceCase(data.comment) + "</h5>" +
    //         "</div>";
    // }
        html = html +"</div>" +
        "<div style='' class='col-md-6 '>" +
        "<div >" +
        "<h5 class='col-md-5'>Update by:</h5><h5 class='col-md-6'  >"+sentenceCase(data.updatedBy.username)  +"</h5>" +
        "</div>" +
        "<div >" +
        "<h5 class='col-md-5'>Update at:</h5><h5 class='col-md-6' >"+sentenceCase(data.updatedBy.branch.solDescription) +"</h5>" +
        "</div>" +
        "<div >" +
        "<h5 class='col-md-5'>Update date:</h5><h5 class='col-md-6' >"+ convertJsonDate(data.updatedOn) +"</h5>" +
        "</div>" +
        "</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" ;
    if (data.channelStatus.indexOf("NOT")>=0){

            html = html + "<input type='button' class='btn btn-success btnpad pull-right' id='bet' Value='Onboard' onClick='updatechannel(" + JSON.stringify(data) + ")'/>";

        
    }
    html = html +"</div>" +
        "</form>";
    hideSpinner("channel");
    document.getElementById("channeltop").innerHTML = html;
}
}
function updatechannel(data){
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Channel Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<fieldset class='form-group'>" +

        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ data.id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory'  />" +
        "<div >" +
        "<label for='entitykey'>Response </label>" +
        "<select name='entitykey' id='entitykey' required class='form-control' onkeyup='validatechannel(this.form)'>" +
        "<option value=''>--Select--</option>" +
        "<option value='Yes' >Yes</option>" +
        "<option value='Via-call' >No(Call)</option>" +
        "<option value='Via-email' >No(Email)</option>" +
        "<option value='via-sms' >No(Sms)</option>" +
        "<option value='physical-contact' >No(Physical contact)</option>" +
        "</select>" +
        "<small class='text-muted'id='demreason'>Please select a reponse</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' class='form-control bcon-field' type='text' name='entitydata' onkeyup='validatechannel(this.form)'/>" +
        "<small class='text-muted' id='demremark'>please enter a comment</small>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' id='bet' Value='Submit' onClick='modifyonboard(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-left' id='bet' Value='Back' onClick='channelitem("+data.id+")'/>" +
        "</div>" +
        "</form>";
        hideSpinner("channel");
    document.getElementById("channeltop").innerHTML = html;
}




/**
 * POST  FreezeController verify
 * This verifies a onboard process
 */
function verifyonboard(form) {
    showSpinner("channel");
    var url = '/onboard/verify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url);
}

/**
 * POST  FreezeController cancel
 * This cancels a onboard action
 */
function cancelonboard(form) {
    showSpinner("channel");
    var url = '/onboard/cancel';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url);
}
/**
 * POST  FreezeController decline
 * This declines a onboard process
 */
function declineonboard(form) {
    showSpinner("channel");
    var url = '/onboard/decline';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url);
}

function resultchannel(data){
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        // "<h4 class='modal-title pull-left' id='myModalLabel'>Channels response page</h4>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +

        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ data.searchmessage +"' />" +
        "<div>" +
        "<label for='remarks'>"+ data.searchmessage +"</label>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btnpad pull-right' id='bet' Value='OK' onClick='channelitem("+data.entityid+")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("channel");
    document.getElementById("channeltop").innerHTML = html;
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        url: '/channel/getchannelbottom',
        success: reloadchannelbottom,
        error : reloadchannelbottom
    });
}
function reloadchannelbottom(data){

    htmlbottom = "<table class='table table-hover table-condensed'> " +
        "<thead> <tr> <th>Type</th> <th width='40%'>Status</th> <th>Date</th> " +
        "<th><a type='button'  class='pull-right btn btn-sm' href='javascript:newchannel()'> " +
        "<i class='fa fa-plus-circle'></i>&nbsp;New channel</a></th> </tr> </thead>" +
        "<tbody>";
    for (var i=0;i<data.length;i++){
        htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:channelitem(" +data[i].id+","+data.searchacctid+ ")&apos;'  role='button'>"+
            "<td>"+sentenceCase(data[i].category).replace("_"," ")+"</td>"+
            "<td width='40%'>"+sentenceCase(data[i].channelStatus).replace("_"," ")+"</td>" +
            "<td>"+convertJsonDate(data[i].channelDate)+"</td><td></td>" +
            "</tr>";
    }
    htmlbottom = htmlbottom + "</tbody> </table>";
    document.getElementById("channelbottom").innerHTML = htmlbottom;
}
function myFunction() {
    myVar = setTimeout(showPage, 3000);
}
function showSpinner(divName) {
    // alert("cha");
    $("#"+divName+"spinner").show();
    $('#'+divName+'container').css("zIndex", 2);
    $('#'+divName+'spinner').css("zIndex", 3);
}
function hideSpinner(divName) {
    // alert("hide");
    $("#"+divName+"spinner").hide();
    $('#'+divName+'container').css("zIndex", -1);
    $('#'+divName+'spinner').css("zIndex", -2);
}



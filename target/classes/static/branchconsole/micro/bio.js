/**
 * POST  BioController addnew
 * This creates a new bio on account
 */
function addnewbio(form) {

    var url = '/bio/addnew';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url);
}

/**
 * POST  BioController remove
 * This removes a bio on account
 */
function removebio(form) {
    var url = '/bio/remove';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url);
}


/**
 * POST  BioController modify
 * This removes a bio on account
 */
function modifybio(form) {
    var url = '/bio/modify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url);
}


/**
 * POST  BioController verify
 * This verifies a bio process
 */
function verifybio(form) {
    var url = '/bio/verify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url);
}

/**
 * POST  BioController cancel
 * This cancels a bio action
 */
function cancelbio(form) {

    var url = '/bio/cancel';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url);
}
/**
 * POST  BioController decline
 * This declines a bio process
 */
function declinebio(form) {

    var url = '/bio/decline';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url);
}

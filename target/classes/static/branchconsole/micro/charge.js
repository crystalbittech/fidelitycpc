/*
 * This does the frontend validation for declination
 */
function validatechargedecline(form){
    if (form.entitykey.value==null || form.entitykey.value==''){
        document.getElementById("cdecreason").innerHTML = "<font color=red>You must provide a reason</font>";
        return false;
    }else{document.getElementById("cdecreason").innerHTML = "<font color=green>Valid</font>";}
    if (form.entitydata.value==null || form.entitydata.value==''){
        document.getElementById("cdecremark").innerHTML = "<font color=red>You must provide a comment</font>";
        return false;
    }else{document.getElementById("cdecremark").innerHTML = "<font color=green>Valid</font>";}
    return true;
}

/**
 * POST  ChargeController addnew
 * This creates a new charge on account
 */
    function addnewcharge(form) {
    showSpinner("charge");
    var url = '/charge/addnew';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultcharge);
}

/**
 * POST  ChargeController remove
 * This removes a charge on account
 */
function removecharge(form) {
    showSpinner("chargetop");
    var url = '/charge/remove';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultcharge);
}


/**
 * POST  ChargeController modify
 * This removes a charge on account
 */
function modifycharge(form) {
    showSpinner("chargetop");
    var url = '/charge/modify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultcharge);
}


/**
 * POST  ChargeController verify
 * This verifies a charge process
 */
function verifycharge(form) {
    showSpinner("charge");
    var url = '/charge/verify';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultcharge);
}

/**
 * POST  ChargeController cancel
 * This cancels a charge action
 */
function cancelcharge(form) {
    showSpinner("charge");
    var url = '/charge/cancel';
    var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
    postRequest.postdata(url, resultcharge);
}
/**
 * POST  ChargeController decline
 * This declines a charge process
 */
//This method does the declination from the controller with ajax call
function declinecharge(form) {
    if (validatechargedecline(form)==true) {
        showSpinner("charge");
        var url = '/charge/decline';
        var postRequest = new PostRequest(form.entityid.value, form.entitycategory.value, form.entitykey.value, form.entitydata.value);
        postRequest.postdata(url, resultcharge);
    }
}



function chargeitem(id){
    showSpinner("charge");
    getChargeRole()
    url = '/charge/getdata';
    var postdata = {
        'id': id
    }

    $( "#chargetop" ).load( "/charge/get/"+id );
    //process(url, postdata, explodecharge);
    hideSpinner("charge");
}


function getChargeRole() {
    var role;
    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        dataType: 'json',
        url: '/user/role/charge',
        success: function (data) {
            role = data;
            effectChargeRoles(role);
        },
        error : function (data) {

        }
    });

}

function effectChargeRoles(role){

    if(!role.declineCharge){

        $("#declineCharge").prop("disabled", true);

    }if(!role.verifyCharge){

        $("#verifyCharge").prop("disabled", true);

    }if(!role.waiveCharge){

        $("#waiveCharge").prop("disabled", true);

    }

}


function explodecharge(data){
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Charge Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.entityId+"' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +
        "<div style='float: left; clear: none; ' class='col-md-8'>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.category+"' id='entitycategory' name='entitycategory'/>" +
        "<h5 class='col-md-3'>Category: </h5><h5 class='col-md-9'  >"+sentenceCase(data.category).replace("_"," ").replace("_"," ")+"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.id+"'  id='entityid' name='entityid'/>" +
        "<h5 class='col-md-3'>Description: </h5><h5 class='col-md-9'  >"+ sentenceCase(data.event).replace("_"," ").replace("_"," ") +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h5 class='col-md-3'>Status: </h5><h5 class='col-md-9' >"+ sentenceCase(data.status).replace("_"," ").replace("_"," ")  +"</h5>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='hidden' class='form-control bcon-field' type='text' value='"+data.value+"' id='entitydata' name='entitydata'/>" +
        "<h5 class='col-md-3' >Request Id: </h5><h5 class='col-md-9' >"+ data.id +"</h5>" +
        "</div>" +
        "</div>" +
        "<div style='float: left; ' class='col-md-4 '>" +
        "<div class='col-md-12'>" +
        "<h6 class='col-md-6'>Update by:</h6><h6 class='col-md-6'  >"+ sentenceCase(data.updatedBy.username)  +"</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h6 class='col-md-6'>Update at:</h6><h6 class='col-md-6' >"+ sentenceCase(data.updatedBy.branch.solDescription)+"</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<h6 class='col-md-6'>Update date:</h6><h6 class='col-md-6' >"+ convertJsonDate(data.updatedOn) +"</h6>" +
        "</div>" +
        "</div>" +
        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>";

    if (data.status.indexOf("AWAITING")>=0){
        if (data.updatedBy.branch.sol == document.getElementById("currentUserSol").value) {
            html = html + "<input type='button' id='verifyCharge' class='btn btn-success btnpad pull-right' Value='Verify' onClick='verifycharge(this.form)'/>";
            html = html + "<input type='button' id='declineCharge' class='btn btn-danger btnpad pull-right' Value='Decline' onClick='chargedecline(" + data.id + ")'/>";
        }
    }else{
        // if (data.status.indexOf("DECLINE")>=0){
        //     html = html + "<input type='button' class='btn btn-danger btnpad pull-right' Value='Cancel' onClick='cancelcharge(this.form)'/>";
        // }
    }
    html = html + "</div>" + "</form>";
    getChargeRole()
    hideSpinner("charge") ;
    document.getElementById("chargetop").innerHTML = html;
}
//This displays the view for the decline form
function chargedecline(id){
    html = "<form action='#' method='POST'>" +
        "<div class='col-md-12'>" +
        "<h6 class='modal-title pull-left' id='myModalLabel'>Charge Details</h6>" +
        "</div>" +
        "<div class='col-md-12'>" +
        // "<input type='hidden' class='form-control bcon-field' type='text' value='' id='entitykey' name='entitykey'/>" +
        "<fieldset class='form-group'>" +

        "<div style='float: left; clear: none;'>" +
        "<input type='hidden' name='entityid' id='entityid' value='"+ id +"' />" +
        "<input type='hidden' name='entitycategory' id='entitycategory'  />" +

        "<div >" +
        "<label for='reason'>Reason </label>" +
        "<input class='form-control bcon-field' type='' id='entitykey' name='entitykey' onkeyup='validatechargedecline(this.form)' />" +
        "<small class='text-muted' id='cdecreason'>Please enter a reason</small>" +
        "</div>" +
        "<div >" +
        "<label for='comment'>Comment</label>" +
        "<input id='entitydata' class='form-control dpicker bcon-field' type='text' name='entitydata' onkeyup='validatechargedecline(this.form)' />" +
        "<small class='text-muted' id='cdecremark'>please enter a comment</small>" +
        "</div>" +
        "</div>" +

        "</fieldset>" +
        "</div>" +
        "<div class='col-md-12'>" +
        "<input type='button' class='btn btn-success btnpad pull-right' Value='Submit' onClick='declinecharge(this.form)'/>" +
        "<input type='button' class='btn btn-primary btnpad pull-right' Value='Back' onClick='chargeitem("+id+")'/>" +
        "</div>" +
        "</form>";
    hideSpinner("charge") ;
    document.getElementById("chargetop").innerHTML = html;
}
function resultcharge(data){
    html = "<div class='col-md-12'>" +
        "<div class='col-md-12 response-box'><div class='inner-response'>" +
        "<label><h3>"+ data.searchmessage +"</h3></label><p></p>" +
        "<label><h7>Request Id: "+ data.entityid +"</h7></label>" +
        "</div><div class='col-md-12'>" +
        "<input type='button' class='btn btnpad pull-right' Value='OK' onClick='chargeitem("+data.entityid+")'/>" +
        "</div></div></div>";
    hideSpinner("charge") ;
    document.getElementById("chargetop").innerHTML = html;
    $.ajax({
        type:  'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),

        url: '/charge/getchargebottom',
        success: reloadchargebottom,
        error : reloadchargebottom
    });
}

function reloadchargebottom(data){

    htmlbottom = "<table class='table table-hover table-condensed'> " +
        "<thead> <tr> <th>Category</th> <th>Type</th> <th>Status</th> " +
        "<th><a type='button'  class='pull-right btn btn-sm' href='javascript:newfreeze()'> " +
        "<i class='fa fa-plus-circle'></i>&nbsp;New charge</a></th> </tr> </thead>" +
        "<tbody>";

    for (var i=0;i<data.length;i++){
        htmlbottom = htmlbottom + "<tr onclick= 'window.location.href = &apos;javascript:chargeitem(" +data[i].id+","+data.searchacctid+ ")&apos;'  role='button'>"+
            "<td>"+sentenceCase(data[i].category).replace("_"," ")+"</td>"+
            "<td>"+sentenceCase(data[i].event.category).replace("_"," ").replace("_"," ")+"</td>" +
            "<td>"+sentenceCase(data[i].status).replace("_"," ")+"</td><td></td>" +
            "</tr>";
    }
    htmlbottom = htmlbottom + "</tbody> </table>";
    hideSpinner("charge") ;
    document.getElementById("chargebottom").innerHTML = htmlbottom;
}


function previewLetter(id){
     $( "#letterdoc" ).load( "/preview/get/"+id );
    $('#chargeModal').modal('show');
}







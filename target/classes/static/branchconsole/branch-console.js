/**
 * Created by rasaq on 6/27/16.
 */
/**Download Javacript  */


/** Calls the Account Controller with the inputted data to fetch a focused Account
 * and list of accounts
 * @param urlType this will determine what url to post to.example "INACTIVE" will post to /accounts/inactive
 * @param using
 * @param value
 * @param from lower limit of date range
 * @param to upper limit of date range
 * @param filter the column to filter by
 * @param filtervalue the value of the filter
 * @param sortby the column to sort by
 * @param currentPage the page number of the page we're currently in
 * @param accountId the account number of the account we want to hold focus in the search result page
 */

function doalert(){
    bootbox.alert("COnFIrm");
}

function search_account(account,alertId) {

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10){
        dd='0'+dd;
    }
    if(mm<10){
        mm='0'+mm;
    }
    var todayFormatted = dd+'-'+mm+'-'+yyyy;
    $('#searchvalue').val(account);
    $('#from').val('01-01-1990');
    $('#to').val(todayFormatted);
    $('#searchfiltervalue').val("");

    // $.post("/alert/update_status",
    //     {
    //         alert_id: alertId
    //     });
    $.ajax({
        type:'POST',
        url:'/alert/update_status',
        data:{'alert_id':alertId},
        success:function(){search();
        },
        error:function(){console.log("Error occured here after clicking");}
    })

    // search();
}

function search() {
    //call pace
    Pace.restart();

    switch($('#searchmodule').val()){

        default:
            url = '/account/search';
            break;
    }

    var postInput = {
        module: "SEARCH",
        using: $('#searchusing').val(),
        linkId: $('#searchlink').val(),
        value: $('#searchvalue').val(),
        from: $('#from').val(),
        to: $('#to').val(),
        filter: $('#searchfilter').val(),
        filtervalue: $('#searchfiltervalue').val(),
        sortby: $('#searchsortby').val(),
        message: $('#searchmessage').val(),
        page: $('#searchpage').val(),
        acctid: $('#searchacctid').val()
    };
    post(url, postInput);
}

function pageclick(page) {
    //call pace
    Pace.restart();

    url = '/account/search';
    var postInput = {
        module: $('#module').val(),
        using: $('#using').val(),
        value: $('#value').val(),
        from: $('#from').val(),
        to: $('#to').val(),
        filter: $('#filter').val(),
        filtervalue: $('#filtervalue').val(),
        sortby: $('#sortby').val(),
        message: $('#message').val(),
        page: page,
        acctid: $('#acctid').val(),
    };
    post(url, postInput);
}

function accountclick(acctid) {
    Pace.restart();
    url = '/account/search';

    $('#acctid').val(acctid);
    console.log($('#acctid').val());
    $('#modulepage').load("/account/module/"+acctid);
    // var postInput = {
    //     module: $('#module').val(),
    //     using: $('#using').val(),
    //     value: $('#value').val(),
    //     from: $('#from').val(),
    //     to: $('#to').val(),
    //     filter: $('#filter').val(),
    //     filtervalue: $('#filtervalue').val(),
    //     sortby: $('#sortby').val(),
    //     message: $('#message').val(),
    //     page: $('#page').val(),
    //     acctid: acctid
    // };
    // post(url, postInput);
}


function trackaccounts(module, link) {
    //call pace
    Pace.restart();

    url = '/account/search';
    var postInput = {
        module: module,
        linkId: link,
        using: "",
        value: "",
        from: $('#from').val(),
        to: $('#to').val(),
        filter: "DEFAULT",
        filtervalue: "",
        sortby: "",
        message: 'SEARCH',
        page: 1,
        acctid: 0,
    };
    post(url, postInput);
}



function setDropdown(selection){
    $('#buttonfilter').html("");
    $('#buttonfilter').append(selection);
    $('#buttonfilter').append('<span class="fa fa-caret-down"></span>');
    $('#searchfilter').val(selection);

    //set the value of your form field
    //$('input[name="filter...."]').val(selection);
}


function searchParams() {
    var searchmodule = $('#module').val();
    var searchusing = $('#using').val();
    var searchvalue = $('#value').val();
    var searchfrom = $('#from').val();
    var searchto = $('#to').val();
    var searchfilter = $('#filter').val();
    var searchfiltervalue = $('#filtervalue').val();
    var searchsortby = $('#sortby').val();
    var searchmessage = $('#message').val();
    var searchpage = ($("#page").val());
    var searchacctid = $('#acctid').val();
}



function PostRequest(id, category, key, data,lienremark,defauthority){

    // Add object properties like this
    this.searchmodule = $('#module').val();
    this.searchusing = $('#using').val();
    this.searchvalue = $('#value').val();
    this.searchfrom = $('#from').val();
    this.searchto = $('#to').val();
    this.searchfilter = $('#filter').val();
    this.searchfiltervalue = $('#filtervalue').val();
    this.searchsortby = $('#sortby').val();
    this.searchmessage = $('#message').val();
    this.searchpage = ($("#page").val());
    this.searchacctid = $('#acctid').val();

    console.log(this.searchacctid);

    this.entityid = id;
    this.entitycategory = category;
    this.entitykey = key;
    this.entitydata = data;

    this.lienremark = lienremark;
    this.defauthority = defauthority;
}

PostRequest.prototype.appendinputs = function (url, callback, formId) {

    var postInput = {

        searchmodule: this.searchmodule,
        searchusing: this.searchusing,
        searchvalue: this.searchvalue,
        searchfrom: this.searchfrom,
        searchto: this.searchto,
        searchfilter: this.searchfilter,
        searchfiltervalue: this.searchfiltervalue,
        searchsortby: this.searchsortby,
        searchmessage: this.searchmessage,
        searchpage: this.searchpage,
        searchacctid: this.searchacctid,
        entityid: this.entityid,
        entitycategory: this.entitycategory,
        entitykey: this.entitykey,
        entitydata: this.entitydata
    };

    appendParameters(postInput, formId);
    process(url, postInput, callback);
};

PostRequest.prototype.appendinputsformdata = function (formData) {

    var postInput = {

        searchmodule: this.searchmodule,
        searchusing: this.searchusing,
        searchvalue: this.searchvalue,
        searchfrom: this.searchfrom,
        searchto: this.searchto,
        searchfilter: this.searchfilter,
        searchfiltervalue: this.searchfiltervalue,
        searchsortby: this.searchsortby,
        searchmessage: this.searchmessage,
        searchpage: this.searchpage,
        searchacctid: this.searchacctid,
        entityid: this.entityid,
        entitycategory: this.entitycategory,
        entitykey: this.entitykey,
        entitydata: this.entitydata,
        lienremark:this.lienremark,
        defauthority:this.defauthority
    };

    appendParametersFormData(postInput, formData);
//    process(url, postInput, callback);

};


PostRequest.prototype.postdata = function (url, callback) {
    // showChannelSpinner();
    var postInput = {
        searchmodule: this.searchmodule,
        searchusing: this.searchusing,
        searchvalue: this.searchvalue,
        searchfrom: this.searchfrom,
        searchto: this.searchto,
        searchfilter: this.searchfilter,
        searchfiltervalue: this.searchfiltervalue,
        searchsortby: this.searchsortby,
        searchmessage: this.searchmessage,
        searchpage: this.searchpage,
        searchacctid: this.searchacctid,
        entityid: this.entityid,
        entitycategory: this.entitycategory,
        entitykey: this.entitykey,
        entitydata: this.entitydata,

        lienremark : this.lienremark,
        defauthority:this.defauthority


    };
    process(url, postInput, callback);
//    post(url, postInput);
};


/** This adds the parameters in params to the form specified by the id formid
 *
 * @param params
 * @param formId
 */
function appendParameters(params, formId) {
    var form = document.getElementById(formId);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);
            form.appendChild(hiddenField);
        }
    }
}

/** This adds the parameters in params to the form data object specified
 *
 * @param params
 * @param formId
 */
function appendParametersFormData(params, formData) {
//    var form = document.getElementById(formId);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            formData.append(key, params[key]);
//            var hiddenField = document.createElement("input");
//            hiddenField.setAttribute("type", "hidden");
//            hiddenField.setAttribute("name", key);
//            hiddenField.setAttribute("value", params[key]);
//            form.appendChild(hiddenField);
        }
    }
}

function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.
    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }
    document.body.appendChild(form);
    form.submit();
}


//Function to validate number is entered
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


function process(url, data, callback) {
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data),
        url: url,
        success: callback,
        error: callback
    });
    // hideSpinner();
    // hideChannelSpinner();
}
function errorhe(data) {
}


function handlereponse(data) {
    console.log(data);
    return data;
}


function populateLetterFields() {
    var e = document.getElementById("category");
    var category = e.options[e.selectedIndex].value;
    var acct = "";
    if ((document.getElementById("acctnum").value != "")) {
        var acct = document.getElementById("acctnum").value;
        var charge = $("input[name=charge]:checked").val();
    }
    var selOption = e.options[e.selectedIndex].innerHTML;
    var url = '/letter/validate/details';
    var postInput = {category: category, acct: acct, optionSelected: selOption};
    post(url, postInput);
}
function previewReport(items) {

    var url = '/report/preview';
    var key = $('#report-opt').val();
    var from = $('#reportfrom').val();
    var to = $('#reportto').val();
    var sol = $('#sol').val();
    // var doctype = $('#doctype').val();
    var doctype = '';

    $('#checkboxes').find("input[type='checkbox']:checked").each(function(index, element){
        if(index == 0){
            doctype = this.value;
        }else {
            doctype = doctype + ","+ this.value;
        }
    });


    var acctbal = $('#acctbal').val();
    var texting=$('#texting').val();
    var item = items;
    var header = "";
    switch (key) {
        case '1':
            header = "Account with Incomplete documentation";
            break;
        case '2':
            header = "Account on PND due to AVR and Legal Search";
            break;
        case '3':
            header = "Reactivated Account on Portal";
            break;
        case '4':
            header = "Accounts with incomplete documentation and on deferral";
            break;
        case '5':
            header = "Accounts on PND with expired Deferral";
            break;
        case '6':
            header = "Account whose customer information update was done on portal";
            break;
       // case '7':
          //  header = "Account whose reactivation was done via SMS short code";
           // break;
        case '8':
            header = "Account with incomplete Quality assurance (Onboarding)";
            break;
        case '9':
            header = "CSM/Branch managed account balance (Individual 0-200,000)";
            break;
        case '10':
            header = "Account with active channel";
            break;
        case '11':
            header = "Newly opened account";
            break;
        case '12':
            header = "Unscanned mandate ";
            break;
        case '13':
            header = "Account with zero balance";
            break;
        case '14':
            header = "Dormant accounts";
            break;
        case '15':
            header = "Inactive accounts";
            break;
        case '16':
            header = "Account on PND above 90 Days";
            break;
        case '17':
            header = "Account on PND(Frozen Account)";
            break;
        case '18':
            header = "User Activity";
            break;
        case '19':
            header = "Pending Verification";
            break;
        case '20':
             header = "Account Reactivated";
             break;
        case '21':
             header = "Stop Order";
             break;
        case '22':
             header = "Closed Account";
             break;
        case '23':
             header = "Cheque Book Destruction";
             break;
        case '24':
            header = "Static Data Update";
            break;
        case '25':
            header = "Account Unfreeze";
            break;
        case '26':
            header = "Account on PND For AVR Due to Exceeding Limit Threshold";
            break;
}

    var postInput = {key: key, from: from, to: to, sol: sol, header : header,item:items,doctype:doctype,acctbal:acctbal,schcode:texting};
    post(url, postInput);

}

function postLetter(id) {

    url = "/letter/send";
    var id = id;
    var postInput = {
        id: id
    };
    post(url, postInput);
}


function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function postLetter(tempId, letterId) {
    // alert(id);
    url = "/letter/send";
    var tempId = tempId;
    var letterId = letterId;
    var postInput = {
        tempId: tempId,
        letterId: letterId
    };
    post(url, postInput);
}
function cancelLetter(id) {
    url = "/letter/cancel";
    var id = id;
    var postInput = {
        id: id
    };
    post(url, postInput);
}

function isEmpty(myObject) {
    for (var key in myObject) {
        if (myObject.hasOwnProperty(key)) {
            return false;
        }
    }

    return true;
}
function sentenceCase(str) {
    if ((str === null) || (str === '')) {
        return "";
    }
    str = str.toString();
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }).replace("_", " ");
}

function convertJsonDate(fdate){
    if ((fdate === null) || (fdate === '')) {
        return "Not available";
    }
    var concat = "";
    var date = new Date(parseInt(fdate));
    concat = concat + (date.getUTCDate()) + "/" + (date.getMonth() + 1) + "/" + (date.getFullYear());
    return concat;
}
//This returns the account satatus and takes no input parameter
function getacctstatus() {
    var postdata = {'id': $('#acctid').val(), 'accid': $('#acctid').val()};
    var acctStatus = "";
    $.ajax({
        type:'POST',
        dataType:'json',
        url:'/freeze/getaccount',
        data:postdata,
        async:false,
        success:function(data){acctStatus=data.accountStatus},
        error:function(){}
    });
    return acctStatus;
}
//This returns the pnd status of the acccount
function getacctpndstatus() {
    var postdata = {'id': $('#acctid').val(), 'accid': $('#acctid').val()};
    var pndStatus = "";
    $.ajax({
        type:'POST',
        dataType:'json',
        url:'/account/getpndstatus',
        data:postdata,
        async:false,
        success:function(data){pndStatus=data.pndStatus},
        error:function(){}
    });
    return pndStatus;
}

function toggleBranchList(){
    var role = $("#role1").val();
    var role3 = $("#role3").val();


    if(role === 'ZONAL_SERVICE_MANAGER'){
        //hide branch for normal users, show branch for zsms
        document.getElementById("branchdata1").style.display = 'none';
        document.getElementById("branchdata11").style.display = 'block';

    }else{
        //hide branch for zsms and show branch normal users
        document.getElementById("branchdata11").style.display = 'none';
        document.getElementById("branchdata1").style.display = 'block';
       
    }

    if(role3 === 'ZONAL_SERVICE_MANAGER'){
        //hide branch for normal users, show branch for zsms
        document.getElementById("branchdata3").style.display = 'none';
        document.getElementById("branchdata33").style.display = 'block';

    }else{
        //hide branch for zsms and show branch normal users
        document.getElementById("branchdata3").style.display = 'block';
        document.getElementById("branchdata33").style.display = 'none';

    }
}

function toggleBranchList1(){
    var role = $("#role2").val();

    if(role === 'ZONAL_SERVICE_MANAGER'){
        //hide branch for normal users, show branch for zsms
        document.getElementById("branchdata2").style.display = 'none';
        document.getElementById("branchdata22").style.display = 'block';
    }else{
        //hide branch for zsms and show branch normal users
        document.getElementById("branchdata22").style.display = 'none';
        document.getElementById("branchdata2").style.display = 'block';
    }

}

function toggleBranchList3(){
    var role4 = $("#role4").val();


    if(role4 === 'ZONAL_SERVICE_MANAGER'){
        //hide branch for normal users, show branch for zsms
        document.getElementById("branchdata4").style.display = 'none';
        document.getElementById("branchdata44").style.display = 'block';
    }else{
        //hide branch for zsms and show branch normal users

        document.getElementById("branchdata4").style.display = 'block';
        document.getElementById("branchdata44").style.display = 'none';
    }
}

function validateUserCreation(form){
    //validate user details
    if (form.username.value ==null || form.username.value==''){
        document.getElementById("usermsg").innerHTML = "<font color=red>Username is required</font>";
        return false;
    }else{document.getElementById("usermsg").innerHTML = "<font color=green>Valid</font>";}
    if(!/^[A-Za-z]{3,}(\.[A-Za-z]{3,})$/.test(form.username.value)){
        document.getElementById("usermsg").innerHTML = "<font color=red>Username is invalid</font>";
        return false;
    }else{document.getElementById("usermsg").innerHTML = "<font color=green>Valid</font>";}
    if (form.firstname.value==null || form.firstname.value==''){
        document.getElementById("firstmsg").innerHTML = "<font color=red>Firstname is required</font>";
        return false;
    }else{document.getElementById("firstmsg").innerHTML = "<font color=green>Valid</font>";}
    if (form.lastname.value==null || form.lastname.value==''){
        document.getElementById("lastmsg").innerHTML = "<font color=red>Last name is required</font>";
        return false;
    }else{document.getElementById("lastmsg").innerHTML = "<font color=green>Valid</font>";}
    if (form.phonenumber.value==null || form.phonenumber.value==''){
        document.getElementById("phonemsg").innerHTML = "<font color=red>Phone number is required</font>";
        return false;
    }else{document.getElementById("phonemsg").innerHTML = "<font color=green>Valid</font>";}
    if (! /^[0-9]{11}$/.test(form.phonenumber.value)) {
        document.getElementById("phonemsg").innerHTML = "<font color=red>Phone number is invalid</font>";
        return false;
    }else{document.getElementById("phonemsg").innerHTML = "<font color=green>Valid</font>";}
    if (form.email.value==null || form.email.value==''){
        document.getElementById("emailmsg").innerHTML = "<font color=red>Email is required</font>";
        return false;
    }else{document.getElementById("emailmsg").innerHTML = "<font color=green>Valid</font>";}
    if(!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(form.email.value)){
        document.getElementById("emailmsg").innerHTML = "<font color=red>Email is invalid</font>";
        return false;
    }else{document.getElementById("emailmsg").innerHTML = "<font color=green>Valid</font>";}
    if (form.employeeid.value==null || form.employeeid.value==''){
        document.getElementById("employeeidmsg").innerHTML = "<font color=red>Employeeid is required</font>";
        return false;
    }else{document.getElementById("employeeidmsg").innerHTML = "<font color=green>Valid</font>";}

    if(!/^[0-9]{1,8}$/.test(form.employeeid.value)){
        document.getElementById("employeeidmsg").innerHTML = "<font color=red>Invalid employee id</font>";
        return false;
    }else{document.getElementById("employeeidmsg").innerHTML = "<font color=green>Valid</font>";}

    if(form.employeeid.value.length > 8){
        document.getElementById("employeeidmsg").innerHTML = "<font color=red>Employee id can not be greater than 8 digits</font>";
        return false;
    }else{document.getElementById("employeeidmsg").innerHTML = "<font color=green>Valid</font>";}

    if($("#role1 option:selected").val() === 'ZONAL_SERVICE_MESSAGE'){ //if role is zsm validate unless u ll get error on ==> #branchzsmlist").val().length > 3
        if($("#branchzsmlist").val().length > 3){
            document.getElementById("branchmsg").innerHTML = "<font color=red>ZSM branches is more than 3</font>";
            return false;
        }else{ document.getElementById("branchmsg").innerHTML = "<font color=green>Valid</font>"; }
    }

    return true;
}

function create_user(form){
    if(validateUserCreation(form) === true) {

        var username = $('#username').val();
        var firstname = $('#firstname').val();
        var middlename = $('#middlename').val();
        var lastname = $('#lastname').val();
        var phonenumber = $('#phonenumber').val();
        var email = $('#email').val();
        var address = $("#address").val();
        var branch;
        var employeeid = $('#employeeid').val();
        var role = $("#role1 option:selected").val();
        var branches ;


        if(role === 'ZONAL_SERVICE_MANAGER'){
            branches = $("#branchzsmlist").val();
        }else{
            branch = $('#branch option:selected').val();
        }

        var postdata = {
            'username': username, 'firstname': firstname, 'middlename': middlename,
            'lastname': lastname, 'phonenumber': phonenumber, 'email': email, 'address': address,
            'branch': branch, 'employeeid': employeeid, "role": role, "branches":branches
        };
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: '/settings/createUser',
            data: JSON.stringify(postdata),
            async: false,
            success: function (data) {

                console.log('user creation result ==>' + JSON.stringify(data));
                $("#user_tab").html(data);

            },
            error: function () {
                console.log("Error occured");
            }

        });
    }
}



function loadUserSetttings(){
    var postdata = {};
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: '/setting/loadusersettingstab',
        data: JSON.stringify(postdata),
        async: false,
        success: function (data) {
            console.log('user creation result ==>' + JSON.stringify(data));
            $("#user_tab").html(data);

        },
        error: function () {
            console.log("Error occured");
        }

    });
}

function loadUploadUserForm(userType){
    // window.location.href = '/setting/loaduploaduserform';
    var postdata = {"UserType": userType};
    $.ajax({
        type: 'POST',
        url: '/setting/loaduploaduserform',
        data: postdata,
        async: false,
        success: function (data) {
            console.log('user creation result ==>' + JSON.stringify(data));
             // alert('UploadUser Already Uploaded');
            $("#user_tab").html(data);
        },
        error: function () {
            console.log("Error occured");
        }
    });
}

function uploadUser(form){

    var uploadedUsersFile = form.file_upload.files;
    var postdata = {"userfile" : uploadedUsersFile};
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: '/setting/uploaduserdata',
        data: JSON.stringify(postdata),
        async: false,
        success: function (data) {
            console.log('user creation result ==>' + JSON.stringify(data));
            $("#user_tab").html(data);
        },
        error: function (data) {
            console.log("Error occured : " + data);
            console.log("Error occured : " + JSON.stringify(data));
        }
    });

}


function fetchUsers() {
    // var answer = $( "#branchdata option:selected" ).val();
    // var postdata = {'id': answer};
    // // alert(JSON.stringify(postdata));
    // $.ajax({
    //     type: 'POST',
    //     contentType: 'application/json',
    //     dataType: 'json',
    //     url: '/setting/getusers',
    //     data: JSON.stringify(postdata),
    //     async: false,
    //     success: function (data) {
    //         html = "<br/>" +
    //                "<label for='acctnum'>Userid</label>";
    //
    //              html = html +
    //                 "<select  id='userids' class='form-control'>";
    //              for (i = 0; i < data.length; i++) {
    //                html = html + "<option value=" + data[i].username + ">" + data[i].username + "</option>";
    //              }
    //              html = html +  "</select>";
    //
    //         html = html + "<small class='text-muted'>Please select a userid</small>" +
    //                "<br/><input class='btn btn-primary'  onclick='javascript: loadUserSettings()' type='button' value='Submit'/>";
    //         document.getElementById('userdata').innerHTML = html;
    //         console.log(JSON.stringify(data));
    //
    //     },
    //     error: function () {
    //         console.log("Error occured")
    //     }
    // });

}


function loadUserSettings(username) {

    var postdata = {'id': username};
    // alert(JSON.stringify(postdata));
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        // dataType: 'json',
        url: '/setting/getusereditsettings',
        data: JSON.stringify(postdata),
        async: false,
        success: function (data) {
            document.getElementById('useredit_tab').innerHTML = data;

            var data = {};
            var data1 = {};
            $.ajax({
            type: "POST",
            contentType: 'application/json',
            dataType: 'json',
            url: "/settings/fetch_branches",
            data: JSON.stringify(data),
            async: false,
            success: function (data) {
             data1 = data;
                },
             error: function (data) {
             console.log('error occured ==> ' + data);
            }
            });

            console.log(data1);
            var html = "<select  id='branch' class='form-control'>";

             for (i = 0; i < data1.length; i++) {
              html = html + "<option value=" + data1[i].sol + ">" + data1[i].solDescription + "</option>";
             }
            html = html + "</select>";
            //document.getElementById("branchdata1").innerHTML = html;
           // document.getElementById("branchdata2").innerHTML = html;

            var role = {};
            var role1 = {};
            $.ajax({
                type: "POST",
                contentType: 'application/json',
                dataType: 'json',
                url: "/settings/fetch_roles",
                data: JSON.stringify(role),
                async: false,
                success: function (role) {
                    role1 = role;
                },
                error: function (role) {
                    console.log('error occured ==> ' + role);
                }
            });

            console.log(role1);
            var html1 = "<select  id='role1' class='form-control'>";

            for (i = 0; i < role1.length; i++) {
                html1 = html1 + "<option value=" + role1[i] + ">" + role1[i] + "</option>";
            }

            html1 = html1 + "</select>";
            document.getElementById("roledata").innerHTML = html1;
        },


        error: function () {
            console.log("Error occured");
        }
    });
}


function loadZsmUserSettings(username) {

    var postdata = {'id': username};
    // alert(JSON.stringify(postdata));
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        // dataType: 'json',
        url: '/setting/getzsmusereditsettings',
        data: JSON.stringify(postdata),
        async: false,
        success: function (data) {
            console.log('data ==> ' + data);
            document.getElementById('useredit_tab').innerHTML = data;

            // var data = {};
            // var data1 = {};
            // $.ajax({
            //     type: "POST",
            //     contentType: 'application/json',
            //     dataType: 'json',
            //     url: "/settings/fetch_branches",
            //     data: JSON.stringify(data),
            //     async: false,
            //     success: function (data) {
            //         data1 = data;
            //     },
            //     error: function (data) {
            //         console.log('error occured ==> ' + data);
            //     }
            // });

            // console.log(data1);
            // var html = "<select  id='branch' class='form-control'>";
            //
            // for (i = 0; i < data1.length; i++) {
            //     html = html + "<option value=" + data1[i].sol + ">" + data1[i].solDescription + "</option>";
            // }
            // html = html + "</select>";
            // document.getElementById("branchdata1").innerHTML = html;
            // document.getElementById("branchdata2").innerHTML = html;

            // var role = {};
            // var role1 = {};
            // $.ajax({
            //     type: "POST",
            //     contentType: 'application/json',
            //     dataType: 'json',
            //     url: "/settings/fetch_roles",
            //     data: JSON.stringify(role),
            //     async: false,
            //     success: function (role) {
            //         role1 = role;
            //     },
            //     error: function (role) {
            //         console.log('error occured ==> ' + role);
            //     }
            // });

            // console.log(role1);
            // var html1 = "<select  id='role1' class='form-control'>";
            //
            // for (i = 0; i < role1.length; i++) {
            //     html1 = html1 + "<option value=" + role1[i] + ">" + role1[i] + "</option>";
            // }
            //
            // html1 = html1 + "</select>";
            // document.getElementById("roledata").innerHTML = html1;
        },


        error: function () {
            console.log("Error occured");
        }
    });
}

function uploadusersettings( ){
    var role = $("#role2").val();

    if($('#branchzsmlist1').val() === null){
        document.getElementById("update_status").innerHTML = "<h5 style='color: red'>Please select a branch.</h5>";
        return false;
    }

    if(role !== 'ZONAL_SERVICE_MANAGER'){
        if($('#branchzsmlist1').val().length > 1){
            //if role is not a zsm, branch can not be more than 1
            document.getElementById("update_status").innerHTML = "<h5 style='color: red'>Multiple branch is not allowed for non zsm</h5>";
            return false;
        }

    }
    // // hideSpinner("customer");
    // console.log("data at uploadusersettings ==> " + data);
    var data2 = setCheckedValues( role);
    // //updateusersettings form id
    $.ajax({
        type :  "POST",
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data2),
        url  :  "/settings/updatesettings",
        success: function(data){
            console.log(data);
            document.getElementById("update_status").innerHTML = "<h5 style='color: green'>" +JSON.stringify(data) + "</h5>";
        }
    });

}

function setCheckedValues(role){
    var data3 = {};
    data3.isEnabled = document.getElementById('enabled').checked;
    data3.verifyLien = document.getElementById('verifylien').checked;
    data3.declineLien = document.getElementById('declinelien').checked;
    data3.cancelLien = document.getElementById('cancellien').checked;
    data3.modifyLien = document.getElementById('modifylien').checked;
    data3.submitLien = document.getElementById('submitlien').checked;
    data3.addFreeze = document.getElementById('addfreeze').checked;
    data3.submitFreeze = document.getElementById('submitfreeze').checked;
    data3.declineFreeze = document.getElementById('declinefreeze').checked;
    data3.verifyFreeze = document.getElementById('verifyfreeze').checked;
    data3.modifyFreeze = document.getElementById('modifyfreeze').checked;
    data3.cancelFreeze = document.getElementById('cancelfreeze').checked;
    data3.removeFreeze = document.getElementById('removefreeze').checked;
    data3.viewLink = document.getElementById('viewlink').checked;
    data3.modifyDocument = document.getElementById('modifydocument').checked;
    data3.chooseDocument = document.getElementById('choosedocument').checked;
    data3.submitDocument = document.getElementById('submitdocument').checked;
    data3.declineDocument = document.getElementById('declinedocument').checked;
    // data.verifyDocument = document.getElementById('verifydocument').checked;
    data3.addDocuments = document.getElementById('adddocument').checked;
    data3.modifyAccountDetails = document.getElementById('modifyaccountdetails').checked;
    data3.verfiyAccount = document.getElementById('verfiyaccount').checked;
    data3.declineAccount = document.getElementById('declineaccount').checked;
    data3.reactivateAccount = document.getElementById('reactivateaccount').checked;
    data3.cancelAccount = document.getElementById('cancelaccount').checked;
    data3.newMandate = document.getElementById('newmandate').checked;
    data3.viewMandate = document.getElementById('viewmandate').checked;
    data3.declineMandate = document.getElementById('declinemandate').checked;
    data3.verifyMandate = document.getElementById('verifymandate').checked;
    data3.modifyMandate = document.getElementById('modifymandate').checked;
    data3.submitMandate = document.getElementById('submitmandate').checked;
    data3.modifyCustomerDetails = document.getElementById('modifycustomerdetails').checked;
    data3.verifyCustomerDetails = document.getElementById('verifycustomerdetails').checked;
    data3.declineCustomerDetails = document.getElementById('declinecustomerdetails').checked;
    data3.cancelCustomerDetails = document.getElementById('cancelcustomerdetails').checked;
    data3.modifyCheque = document.getElementById('modifycheque').checked;
    data3.cancelCheque = document.getElementById('cancelcheque').checked;
    data3.verifyCheque = document.getElementById('verifycheque').checked;
    data3.declineCheque = document.getElementById('declinecheque').checked;
    data3.requestLetter = document.getElementById('requestletter').checked;
    data3.waivecharge = document.getElementById('waivecharge').checked;
    data3.verifyCharge = document.getElementById('verifycharge').checked;
    data3.declineCharge = document.getElementById('declinecharge').checked;

    var branches;

    data3.role = role;//role.options[role.selectedIndex].value;
    // if(data3.role === 'ZONAL_SERVICE_MANAGER'){
        branches = $('#branchzsmlist1').val();
        data3.strbranch = branches; //branches[1].value;//branches;
        

    // }else{
    //     branch = document.getElementById("branch1");
    //     data3.strbranch = branch.options[branch.selectedIndex].value;
    //     alert('data3.strbranch ==> ' + data3.strbranch);
    // }


    data3.originalrole = document.getElementById("myrole").value;
    data3.username = document.getElementById("userid").value;
    return data3;

}


function getlienCategory(){
    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        url: '/setting/json/liencategory',

        success: function(data) {
            $.each(data, function(i, value) {
                $("[name=liencategory]").append($('<option>').text(value).attr('value', value));
            });
        },
        error : function (data) {

        }
    });

}

function getFreezeType(){

    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        url: '/setting/json/freezetype ',

        success: function(data) {
            $.each(data, function(i, value) {
                $("[name=freezetype]").append($('<option>').text(value).attr('value', value));
            });
        },
        error : function (data) {

        }
    });

}

function getFreezeReason() {

    $.ajax({
        type:  'GET',
        contentType: 'application/json',
        url: '/setting/json/freezereason ',

        success: function(data) {

            $.each(data, function(i, value) {
                $("[name=freezereason]").append($('<option>').text(value).attr('value', value));
            });
        },
        error : function (data) {

        }
    });
}

function showDocumentPDF(theurl){
    //PDFObject.embed("/pdf/useropload.pdf", "#documentModal");
    PDFObject.embed("/file/load/"+theurl, "#pdfdoc");
    $('#documentModal').modal('show');
    
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
function ValidateImageInForm(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }

                if (!blnValid) {
                     return false;
                }
            }
        }
    }

    return true;
}

var _validPDFExtensions = [".pdf"];
function ValidatePDFInFOrm(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validPDFExtensions.length; j++) {
                    var sCurExtension = _validPDFExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }

                if (!blnValid) {
                     return false;
                }
            }
        }
    }

    return true;
}

function reloadDashboard(){
    //alert("id is "+$("#branchidfield").val());
    window.location.href = "/zsm/change/"+$("#branchidfield").val();
}

//UserPrivilege

$(function (){

       if  (($('#div1').is(":visible") == true) && ($('#modulepage').hasClass('col-lg-8')) ){
          $('.togglebutton').show();
          $('.togglebutton2').hide();
         // alert("i am toggle");
       }


       // alert("i am toggle1");
       //       if  (($('#div2').is(":visible") == true)){
          $('.togglebutton').click(function () {
                   $("#div1").hide();
                   $("#modulepage").removeClass('col-lg-8');
                   $("#modulepage").addClass('col-lg-12' );
                       // alert("i am toggle1");
          });
       //    }





       // get all divs
       var $elems = $('#modulepage').find('div');

       // count them
       var elemsCount = $elems.length;
//       alert('element count: '+elemsCount);

       // the loaded elements flag
       var loadedCount = 0;

       // attach the load event to elements
       $elems.on('load', function () {
           // increase the loaded count
           //loadedCount++;

           // if loaded count flag is equal to elements count
           //if (loadedCount == elemsCount) {
           if (elemsCount == 0) {
               // do what you want
               //
               // alert('No div loaded');
           }
       });
})

function checktransaction(module, link) {
    //call pace
    Pace.restart();

    url = '/account/checktransaction';
    var postInput = {
        module: module,
        linkId: link,
        using: "",
        value: "",
        from: $('#from').val(),
        to: $('#to').val(),
        filter: "DEFAULT",
        filtervalue: "",
        sortby: "",
        message: 'SEARCH',
        page: 1,
        acctid: 0,
    };
    post(url, postInput);
}

function getTransactionDetails(){
    var url = '/account/getTransaction';
    // alert("acct no "+ $("#taccno").val());
    var postdata = {"acctno":$("#taccno").val()};
    $.ajax({
       type:'POST',
       url:url,
       data:postdata,
       success:function(data){
            document.getElementById("detailsContent").innerHTML = data;
       },
        error:function(){
                console.log("Error occured when trying to fetch the current balance");
            document.getElementById("errorM").innerHTML = "<font color='red' face='Times New Roman'><h4>Error occured while trying to fetch record. Database could be down</h4></font>";
        }
    });
}

function newstatement(module, link) {
    //call pace
    Pace.restart();

    url = '/statement/new';
    var postInput = {
        module: module,
        linkId: link,
        using: "",
        value: "",
        from: $('#from').val(),
        to: $('#to').val(),
        filter: "DEFAULT",
        filtervalue: "",
        sortby: "",
        message: 'SEARCH',
        page: 1,
        acctid: 0,
    };
    post(url, postInput);
}


// function checkfile(sender) {
//     var validExts = new Array(".xlsx", ".xls");
//     var fileExt = sender.value;
//     fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
//     if (validExts.indexOf(fileExt) < 0) {
//         alert("Invalid file selected, valid files are of " +
//             validExts.toString() + " types.");
//         return false;
//     }
//     else return true;
// }

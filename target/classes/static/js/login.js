/**
 * Created by ccndu on 1/12/2017.
 */

function login() {
    setTimeout(function () {

    }, 5000);

    level = Number($("#level").val());
    $("#login_response").html("&nbsp;");
    if (level == 1) {
        if ($("#username").val().trim().length < 1) {
            $("#login_response").text("Username cannot be empty");
        }
        else {
            $("#spinner").show();
            $("#next_button").hide();
            $.post("/login/check_user",
                {
                    uname: $("#username").val()
                },
                function (data, status) {
                    $("#spinner").hide();
                    $("#next_button").show();
                    if (status == "success") {
                        if (data.status == "success") {
                            $("#username_div").hide();
                            $("#password_div").show();
                            $("#token_div").show();
                            $("#level").val(2);
                            $("#login_user").text(data.msg);
                        }
                        else {
                            $("#login_response").text(data.msg);
                        }
                    }

                });
        }
    }
    else if (level == 2) {
        if ($("#password").val().length < 1) {
            $("#login_response").text("Password cannot be empty");
        }
        else if ($("#token").val().length < 1) {
            $("#login_response").text("Token cannot be empty");
        }
        else {
            $("#spinner").show();
            $("#next_button").hide();
            $.post("/login/authenticate_user",
                {
                    uname: $("#username").val(),
                    upass: $("#password").val(),
                    token: $("#token").val()
                },
                function (data, status) {
                    if (status == "success") {
                        if (data.status == "success") {
                            window.location.replace("/homepage")
                            // window.location.replace("/dashboard")
                        }
                        else {
                            $("#spinner").hide();
                            $("#next_button").show();
                            $("#login_response").text(data.msg);
                        }
                    }
                });
        }
    }
    return false; //to prevent default form behaviour ie stop form from being submitted.
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});
});

